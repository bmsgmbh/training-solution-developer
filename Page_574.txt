OBJECT Page 574 Detailed Vendor Ledg. Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Detaillierte Kreditorenposten;
               ENU=Detailed Vendor Ledg. Entries];
    InsertAllowed=No;
    SourceTable=Table380;
    DataCaptionFields=Vendor Ledger Entry No.,Vendor No.;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 27      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 29      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Ausgleich aufheben;
                                 ENU=Unapply Entries];
                      ToolTipML=[DEU=Hebt die Auswahl eines oder mehrerer Posten auf, auf die dieser Datensatz nicht angewendet werden soll.;
                                 ENU=Unselect one or more ledger entries that you want to unapply this record.];
                      ApplicationArea=#Basic,#Suite;
                      Image=UnApply;
                      OnAction=VAR
                                 VendEntryApplyPostedEntries@1000 : Codeunit 227;
                               BEGIN
                                 VendEntryApplyPostedEntries.UnApplyDtldVendLedgEntry(Rec);
                               END;
                                }
      { 26      ;1   ;Action    ;
                      CaptionML=[DEU=&Navigate;
                                 ENU=&Navigate];
                      ToolTipML=[DEU=Sucht alle Posten und Belege, die f�r die Belegnummer und das Buchungsdatum auf dem ausgew�hlten Posten oder Beleg vorhanden sind.;
                                 ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate.SetDoc("Posting Date","Document No.");
                                 Navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Legt das Buchungsdatum des detaillierten Kreditorenpostens fest.;
                           ENU=Specifies the posting date of the detailed vendor ledger entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Postenart des detaillierten Kreditorenpostens fest.;
                           ENU=Specifies the entry type of the detailed vendor ledger entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry Type" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Belegart des detaillierten Kreditorenpostens fest.;
                           ENU=Specifies the document type of the detailed vendor ledger entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Type" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Belegnummer der Transaktion an, die den Posten erstellt hat.;
                           ENU=Specifies the document number of the transaction that created the entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No." }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kreditorkontos an, in dem der Posten gebucht wird.;
                           ENU=Specifies the number of the vendor account to which the entry is posted.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Vendor No." }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den globalen Dimensionscode 1 des urspr�nglichen Kreditorenpostens fest.;
                           ENU=Specifies the Global Dimension 1 code of the initial vendor ledger entry.];
                SourceExpr="Initial Entry Global Dim. 1";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den globalen Dimensionscode 2 des urspr�nglichen Kreditorenpostens fest.;
                           ENU=Specifies the Global Dimension 2 code of the initial vendor ledger entry.];
                SourceExpr="Initial Entry Global Dim. 2";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den Code f�r die W�hrung fest, wenn es sich um einen Betrag in Fremdw�hrung handelt.;
                           ENU=Specifies the code for the currency if the amount is in a foreign currency.];
                ApplicationArea=#Suite;
                SourceExpr="Currency Code" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den Betrag des detaillierten Kreditorenpostens fest.;
                           ENU=Specifies the amount of the detailed vendor ledger entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount;
                Editable=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag des Postens in MW an.;
                           ENU=Specifies the amount of the entry in LCY.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Amount (LCY)" }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Legt das Datum fest, an dem der Ursprungsposten zur Zahlung f�llig ist.;
                           ENU=Specifies the date on which the initial entry is due for payment.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Initial Entry Due Date" }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der den Posten erstellt hat.;
                           ENU=Specifies the ID of the user who created the entry.];
                SourceExpr="User ID";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Herkunftscode an, der angibt, wo der Posten erzeugt wurde.;
                           ENU=Specifies the source code that specifies where the entry was created.];
                SourceExpr="Source Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ursachencode an, einen zus�tzlichen Code, der Sie in die Lage versetzt, den Posten zu verfolgen.;
                           ENU=Specifies the reason code, a supplementary source code that enables you to trace the entry.];
                SourceExpr="Reason Code";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Legt fest, ob der Ausgleich f�r den Posten im Fenster "Kred.-Postenausgleich aufheben" aufgehoben wurde. Dabei wird die Postennummer im Feld "Ausgleich aufgehoben von Lfd. Nr." verwendet.;
                           ENU=Specifies whether the entry has been unapplied (undone) from the Unapply Vendor Entries window by the entry no. shown in the Unapplied by Entry No. field.];
                SourceExpr=Unapplied;
                Visible=FALSE }

    { 33  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Korrekturpostens an, wenn der Ausgleich f�r den urspr�nglichen Posten im Fenster "Kred.-Postenausgleich aufheben" aufgehoben wurde.;
                           ENU=Specifies the number of the correcting entry, if the original entry has been unapplied (undone) from the Unapply Vendor Entries window.];
                SourceExpr="Unapplied by Entry No.";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postennummer des Kreditorenpostens an, zu dem dieser detaillierte Kreditorenposten erzeugt wurde.;
                           ENU=Specifies the entry number of the vendor ledger entry that the detailed vendor ledger entry line was created for.];
                SourceExpr="Vendor Ledger Entry No.";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Postennummer des detaillierten Kreditorenpostens fest.;
                           ENU=Specifies the entry number of the detailed vendor ledger entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Navigate@1000 : Page 344;

    BEGIN
    END.
  }
}

