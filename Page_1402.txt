OBJECT Page 1402 Purchase No. Series Setup
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Einrichtung Einkaufsnummernserien;
               ENU=Purchase No. Series Setup];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table312;
    PageType=ListPlus;
    ActionList=ACTIONS
    {
      { 3       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 4       ;1   ;Action    ;
                      Name=Setup;
                      CaptionML=[DEU=Kreditoren & Einkauf Einr.;
                                 ENU=Purchases & Payables Setup];
                      ToolTipML=[DEU=Zeigen Sie Unternehmensrichtlinien f�r die Fakturierung von Eink�ufen und Reklamationen an und bieten Sie Aktionen zum Einrichten von in Kreditoren und Einkauf verwendeten Codes und Werten an.;
                                 ENU=View company policies for purchase invoicing and returns and offers actions to set up codes and values that you use in purchases and payables.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 460;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Setup;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 35  ;0   ;Container ;
                ContainerType=ContentArea }

    { 14  ;1   ;Group     ;
                CaptionML=[DEU=Nummerierung;
                           ENU=Numbering];
                InstructionalTextML=[DEU=Zum automatischen Ausf�llen des Belegnummernfelds m�ssen Sie eine Nummernserie einrichten.;
                                     ENU=To fill the Document No. field automatically, you must set up a number series.] }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Nummernserie an, die verwendet wird, um Einkaufsanfragen Nummern zuzuweisen. Um die bereits in der Tabelle "Nummernserie" eingerichteten Nummernserien anzuzeigen, klicken Sie auf das Feld.;
                           ENU=Specifies the code for the number series that will be used to assign numbers to purchase quotes. To see the number series that have been set up in the No. Series table, click the field.];
                ApplicationArea=#All;
                SourceExpr="Quote Nos.";
                Visible=QuoteNosVisible }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Nummernserie an, die verwendet wird, um Rahmenbestellungen Nummern zuzuweisen. Um die bereits in der Tabelle "Nummernserie" eingerichteten Nummernserien anzuzeigen, klicken Sie auf das Feld.;
                           ENU=Specifies the code for the number series that will be used to assign numbers to blanket purchase orders. To see the number series that have been set up in the No. Series table, click the field.];
                ApplicationArea=#All;
                SourceExpr="Blanket Order Nos.";
                Visible=BlanketOrderNosVisible }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Nummernserie an, die verwendet wird, um Bestellungen Nummern zuzuweisen. Um die bereits in der Tabelle "Nummernserie" eingerichteten Nummernserien anzuzeigen, klicken Sie auf das Feld.;
                           ENU=Specifies the code for the number series that will be used to assign numbers to purchase orders. To see the number series that have been set up in the No. Series table, click the field.];
                ApplicationArea=#All;
                SourceExpr="Order Nos.";
                Visible=OrderNosVisible }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummernserie an, die zum Zuweisen von Nummern zu neuen Einkaufsreklamationen verwendet wird.;
                           ENU=Specifies the number series that is used to assign numbers to new purchase return orders.];
                ApplicationArea=#All;
                SourceExpr="Return Order Nos.";
                Visible=ReturnOrderNosVisible }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Nummernserie an, die verwendet wird, um Einkaufsrechnungen Nummern zuzuweisen. Um die bereits in der Tabelle "Nummernserie" eingerichteten Nummernserien anzuzeigen, klicken Sie auf das Feld.;
                           ENU=Specifies the code for the number series that will be used to assign numbers to purchase invoices. To see the number series that have been set up in the No. Series table, click the field.];
                ApplicationArea=#All;
                SourceExpr="Invoice Nos.";
                Visible=InvoiceNosVisible }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Nummernserie an, die verwendet wird, um Einkaufsgutschriften Nummern zuzuweisen. Um die bereits in der Tabelle "Nummernserie" eingerichteten Nummernserien anzuzeigen, klicken Sie auf das Feld.;
                           ENU=Specifies the code for the number series that will be used to assign numbers to purchase credit memos. To see the number series that have been set up in the No. Series table, click the field.];
                ApplicationArea=#All;
                SourceExpr="Credit Memo Nos.";
                Visible=CreditMemoNosVisible }

  }
  CODE
  {
    VAR
      QuoteNosVisible@1000 : Boolean;
      BlanketOrderNosVisible@1001 : Boolean;
      OrderNosVisible@1002 : Boolean;
      ReturnOrderNosVisible@1003 : Boolean;
      InvoiceNosVisible@1004 : Boolean;
      CreditMemoNosVisible@1005 : Boolean;

    PROCEDURE SetFieldsVisibility@1(DocType@1000 : 'Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order');
    BEGIN
      QuoteNosVisible := (DocType = DocType::Quote);
      BlanketOrderNosVisible := (DocType = DocType::"Blanket Order");
      OrderNosVisible := (DocType = DocType::Order);
      ReturnOrderNosVisible := (DocType = DocType::"Return Order");
      InvoiceNosVisible := (DocType = DocType::Invoice);
      CreditMemoNosVisible := (DocType = DocType::"Credit Memo");
    END;

    BEGIN
    END.
  }
}

