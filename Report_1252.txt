OBJECT Report 1252 Match Bank Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Bankposten abstimmen;
               ENU=Match Bank Entries];
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 1   ;    ;DataItem;                    ;
               DataItemTable=Table273;
               DataItemTableView=SORTING(Bank Account No.,Statement No.);
               OnAfterGetRecord=BEGIN
                                  MatchSingle(DateRange);
                                END;
                                 }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
      { 1   ;    ;Container ;
                  Name=RequestPage;
                  ContainerType=ContentArea }

      { 3   ;1   ;Group     ;
                  GroupType=Group }

      { 2   ;2   ;Field     ;
                  Name=DateRange;
                  CaptionML=[DEU=Toleranz Buchungsdaten in Tagen;
                             ENU=Transaction Date Tolerance (Days)];
                  ToolTipML=[DEU=Legt die Anzahl der Tage vor und nach dem Buchungsdatum des Bankpostens fest, innerhalb derer die Funktion nach passenden Transaktionsdaten im Bankauszug sucht. Falls Sie 0 eingeben oder das Feld leer lassen, sucht die Funktion "Automatisch abgleichen" nur nach passenden Transaktionsdaten am Buchungsdatum des Bankposten.;
                             ENU=Specifies the span of days before and after the bank account ledger entry posting date within which the function will search for matching transaction dates in the bank statement. If you enter 0 or leave the field blank, then the Match Automatically function will only search for matching transaction dates on the bank account ledger entry posting date.];
                  ApplicationArea=#Basic,#Suite;
                  BlankZero=Yes;
                  SourceExpr=DateRange;
                  MinValue=0 }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      DateRange@1000 : Integer;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

