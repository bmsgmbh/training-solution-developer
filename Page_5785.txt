OBJECT Page 5785 Warehouse Activity Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Lageraktivit�tszeilen;
               ENU=Warehouse Activity Lines];
    SourceTable=Table5767;
    PageType=List;
    OnAfterGetCurrRecord=BEGIN
                           CurrPage.CAPTION := FormCaption;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 77      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 24      ;2   ;Action    ;
                      Name=Card;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Karte;
                                 ENU=Card];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=EditLines;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowActivityDoc;
                               END;
                                }
      { 78      ;2   ;Action    ;
                      CaptionML=[DEU=&Logistikbeleg anzeigen;
                                 ENU=Show &Whse. Document];
                      Promoted=Yes;
                      Image=ViewOrder;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowWhseDoc;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 58  ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Aktionsart f�r die Lageraktivit�tszeile fest.;
                           ENU=Specifies the action type for the warehouse activity line.];
                SourceExpr="Action Type";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Art der Lageraktivit�t f�r die Zeile fest.;
                           ENU=Specifies the type of warehouse activity for the line.];
                SourceExpr="Activity Type";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Lageraktivit�t an.;
                           ENU=Specifies the number of the warehouse activity.];
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Lageraktivit�tszeile an.;
                           ENU=Specifies the number of the warehouse activity line.];
                SourceExpr="Line No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Herkunftsbelegs an, auf den sich die Lagerbewegung bezieht, wie etwa Einkauf, Verkauf oder Fertigung.;
                           ENU=Specifies the type of source document to which the warehouse activity line relates, such as sales, purchase, and production.];
                SourceExpr="Source Type";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Herkunftsunterart des Belegs an, der sich auf die erwartete Lagerbewegung bezieht.;
                           ENU=Specifies the source subtype of the document related to the warehouse request.];
                SourceExpr="Source Subtype";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Quellnummer des Belegs an, von dem der Posten stammt.;
                           ENU=Specifies the source number of the document from which the entry originates.];
                SourceExpr="Source No." }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer im Feld "Zeilennr." der Wareneingangszeile an, die mit dieser Zuordnungsm�glichkeit verbunden ist.;
                           ENU=Specifies the number in the Line No. field on the warehouse receipt line related to this cross-dock opportunity.];
                SourceExpr="Source Line No." }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Herkunftsunterzeilennummer an.;
                           ENU=Specifies the source subline number.];
                SourceExpr="Source Subline No.";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt die Art des Beleges, auf den sich die Zeile bezieht, z.�B. Verkaufsauftrag.;
                           ENU=Specifies the type of document that the line relates to, such as a sales order.];
                SourceExpr="Source Document" }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerortcode an, an dem die Aktivit�t erfolgt.;
                           ENU=Specifies the code for the location where the activity occurs.];
                SourceExpr="Location Code" }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Zone an, in der sich der Lagerplatz dieser Zeile befindet.;
                           ENU=Specifies the zone code where the bin on this line is located.];
                SourceExpr="Zone Code";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerplatz an, an dem Artikel in der Zeile abgewickelt werden.;
                           ENU=Specifies the bin where items on the line are handled.];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Regalnummer des Artikels zu Informationszwecken an.;
                           ENU=Specifies the shelf number of the item for informational use.];
                SourceExpr="Shelf No." }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Artikelnummer des Artikels an, der bearbeitet (z. B. kommissioniert oder eingelagert) werden soll.;
                           ENU=Specifies the item number of the item to be handled, such as picked or put away.];
                SourceExpr="Item No." }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Variantencode des zu bearbeitenden Artikels an.;
                           ENU=Specifies the variant code of the item to be handled.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einheitencode des Artikels in der Zeile an.;
                           ENU=Specifies the unit of measure code of the item on the line.];
                SourceExpr="Unit of Measure Code" }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels in der Zeile pro Ma�einheit an.;
                           ENU=Specifies the quantity per unit of measure of the item on the line.];
                SourceExpr="Qty. per Unit of Measure" }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Artikels in der Zeile an.;
                           ENU=Specifies a description of the item on the line.];
                SourceExpr=Description }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Artikels in der Zeile an.;
                           ENU=Specifies a description of the item on the line.];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels an, der bearbeitet werden soll, z. B. empfangen, eingelagert oder zugewiesen.;
                           ENU=Specifies the quantity of the item to be handled, such as received, put-away, or assigned.];
                SourceExpr=Quantity }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels in der Basiseinheit an, die bewegt werden muss.;
                           ENU=Specifies the quantity of the item to be handled, in the base unit of measure.];
                SourceExpr="Qty. (Base)" }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Artikel an, die f�r diese Lageraktivit�tszeile noch nicht bearbeitet wurden.;
                           ENU=Specifies the number of items that have not yet been handled for this warehouse activity line.];
                SourceExpr="Qty. Outstanding" }

    { 48  ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Anzahl der Artikel in Basiseinheiten fest, die f�r diese Lageraktivit�tszeile noch nicht bearbeitet wurden.;
                           ENU=Specifies the number of items, expressed in the base unit of measure, that have not yet been handled for this warehouse activity line.];
                SourceExpr="Qty. Outstanding (Base)" }

    { 50  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viele Einheiten in dieser Lageraktivit�t bearbeitet werden.;
                           ENU=Specifies how many units to handle in this warehouse activity.];
                SourceExpr="Qty. to Handle" }

    { 52  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge der Artikel an, die in dieser Lageraktivit�t bearbeitet werden sollen.;
                           ENU=Specifies the quantity of items to be handled in this warehouse activity.];
                SourceExpr="Qty. to Handle (Base)" }

    { 54  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Artikel an, die f�r diese Lageraktivit�t bearbeitet wurden.;
                           ENU=Specifies the number of items on the line that have been handled in this warehouse activity.];
                SourceExpr="Qty. Handled" }

    { 56  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Artikel an, die f�r diese Lageraktivit�t bearbeitet wurden.;
                           ENU=Specifies the number of items on the line that have been handled in this warehouse activity.];
                SourceExpr="Qty. Handled (Base)" }

    { 72  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der erforderlichen Ausstattung zur Ausf�hrung der Aktion in dieser Zeile an.;
                           ENU=Specifies the code of the equipment required when you perform the action on the line.];
                SourceExpr="Special Equipment Code";
                Visible=FALSE }

    { 60  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Versandanweisung an, die informiert, ob Teillieferungen akzeptiert werden, kopiert aus dem Kopf des Herkunftsbelegs.;
                           ENU=Specifies the shipping advice, informing whether partial deliveries are acceptable, copied from the source document header.];
                SourceExpr="Shipping Advice" }

    { 66  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Lageraktivit�t abgeschlossen sein muss.;
                           ENU=Specifies the date when the warehouse activity must be completed.];
                SourceExpr="Due Date" }

    { 62  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Logistikbelegs an, aus dem die Zeile stammte.;
                           ENU=Specifies the type of warehouse document from which the line originated.];
                SourceExpr="Whse. Document Type";
                Visible=FALSE }

    { 64  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Logistikbelegs an, auf dem die Aktion dieser Zeile basiert.;
                           ENU=Specifies the number of the warehouse document that is the basis for the action on the line.];
                SourceExpr="Whse. Document No.";
                Visible=FALSE }

    { 68  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Zeile im Logistikbeleg an, auf dem die Aktion dieser Zeile basiert.;
                           ENU=Specifies the number of the line in the warehouse document that is the basis for the action on the line.];
                SourceExpr="Whse. Document Line No.";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1001 : TextConst 'DEU=Einlagerungszeilen;ENU=Warehouse Put-away Lines';
      Text001@1000 : TextConst 'DEU=Kommissionierzeilen;ENU=Warehouse Pick Lines';
      Text002@1002 : TextConst 'DEU=Lagerplatzumlagerungszeilen;ENU=Warehouse Movement Lines';
      Text003@1003 : TextConst 'DEU=Lageraktivit�tszeilen;ENU=Warehouse Activity Lines';
      Text004@1004 : TextConst 'DEU=Lagereinlagerungszeilen;ENU=Inventory Put-away Lines';
      Text005@1005 : TextConst 'DEU=Lagerkommissionierzeilen;ENU=Inventory Pick Lines';

    LOCAL PROCEDURE FormCaption@1() : Text[250];
    BEGIN
      CASE "Activity Type" OF
        "Activity Type"::"Put-away":
          EXIT(Text000);
        "Activity Type"::Pick:
          EXIT(Text001);
        "Activity Type"::Movement:
          EXIT(Text002);
        "Activity Type"::"Invt. Put-away":
          EXIT(Text004);
        "Activity Type"::"Invt. Pick":
          EXIT(Text005);
        ELSE
          EXIT(Text003);
      END;
    END;

    BEGIN
    END.
  }
}

