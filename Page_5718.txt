OBJECT Page 5718 Item Substitution Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Ersatzartikel;
               ENU=Item Substitution Entries];
    SourceTable=Table5715;
    DelayedInsert=Yes;
    DataCaptionFields=No.,Description;
    PageType=Worksheet;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 15      ;1   ;Action    ;
                      CaptionML=[DEU=B&edingung;
                                 ENU=&Condition];
                      RunObject=Page 5719;
                      RunPageLink=Type=FIELD(Type),
                                  No.=FIELD(No.),
                                  Variant Code=FIELD(Variant Code),
                                  Substitute Type=FIELD(Substitute Type),
                                  Substitute No.=FIELD(Substitute No.),
                                  Substitute Variant Code=FIELD(Substitute Variant Code);
                      Promoted=Yes;
                      Image=ViewComments;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 10  ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Variante an, die als Ersatzartikel verwendet werden kann.;
                           ENU=Specifies the code of the variant that can be used as a substitute.];
                SourceExpr="Variant Code" }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Warenausgangsdatum des Ersatzartikels an.;
                           ENU=Specifies the substitute item shipment date.];
                ApplicationArea=#Suite;
                SourceExpr="Shipment Date" }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an, der als Ersatz verwendet werden kann, falls der urspr�ngliche Artikel nicht verf�gbar ist.;
                           ENU=Specifies the number of the item that can be used as a substitute in case the original item is unavailable.];
                ApplicationArea=#Suite;
                SourceExpr="Substitute No." }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Variante an, die als Ersatzartikel verwendet werden kann.;
                           ENU=Specifies the code of the variant that can be used as a substitute.];
                SourceExpr="Substitute Variant Code";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Beschreibung des Ersatzartikels fest.;
                           ENU=Specifies the description of the substitute item.];
                ApplicationArea=#Suite;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viele Einheiten (z. B. St�ck, Schachteln oder Dosen) des Artikels verf�gbar sind.;
                           ENU=Specifies how many units (such as pieces, boxes, or cans) of the item are available.];
                ApplicationArea=#Suite;
                DecimalPlaces=0:5;
                SourceExpr=Inventory }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die verf�gbare Menge des Ersatzartikels am Warenausgangsdatum an.;
                           ENU=Specifies the substitute item quantity available on the shipment date.];
                ApplicationArea=#Suite;
                SourceExpr="Quantity Avail. on Shpt. Date" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass eine Bedingung f�r diesen Ersatz vorhanden ist.;
                           ENU=Specifies that a condition exists for this substitution.];
                SourceExpr=Condition }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

