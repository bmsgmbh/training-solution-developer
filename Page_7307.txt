OBJECT Page 7307 Bin Type List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Lagerplatzarten�bersicht;
               ENU=Bin Type List];
    SourceTable=Table7303;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen eindeutigen Code f�r die Lagerplatzart an.;
                           ENU=Specifies a unique code for the bin type.];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Lagerplatzart an.;
                           ENU=Specifies a description of the bin type.];
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Lagerplatz f�r Artikel verwendet wird, die gerade erst im Lager eingetroffen sind.;
                           ENU=Specifies to use the bin for items that have just arrived at the warehouse.];
                SourceExpr=Receive }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Lagerplatz f�r Artikel verwendet wird, die zur Auslieferung aus dem Lager anstehen.;
                           ENU=Specifies to use the bin for items that are about to be shipped out of the warehouse.];
                SourceExpr=Ship }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Lagerplatz f�r Artikel verwendet wird, die eingelagert werden, wie Lieferungen und interne Einlagerungsanforderungen.;
                           ENU=Specifies to use the bin for items that are being put away, such as receipts and internal put-always.];
                SourceExpr="Put Away" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Lagerplatz f�r Artikel verwendet wird, die f�r den Warenausgang, f�r interne Kommissionierungen und f�r die Produktion kommissioniert werden k�nnen.;
                           ENU=Specifies to use the bin for items that can be picked for shipment, internal picks, and production.];
                SourceExpr=Pick }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

