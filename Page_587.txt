OBJECT Page 587 XBRL Rollup Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=XBRL-Roll-up-Zeilen;
               ENU=XBRL Rollup Lines];
    SourceTable=Table398;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Nummer der XBRL-Zeile fest, von der diese XBRL-Zeile aufgerollt wird.;
                           ENU=Specifies the number of the XBRL line from which this XBRL line is rolled up.];
                SourceExpr="From XBRL Taxonomy Line No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Legt den Namen der XBRL-Zeile fest, von der diese XBRL-Zeile aufgerollt wird.;
                           ENU=Specifies the name of the XBRL line from which this XBRL line is rolled up.];
                SourceExpr="From XBRL Taxonomy Line Name";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Beschriftung der XBRL-Zeile fest, von der diese XBRL-Zeile aufgerollt wird.;
                           ENU=Specifies the label of the XBRL line from which this XBRL line is rolled up.];
                SourceExpr="From XBRL Taxonomy Line Label" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Beschriftung der XBRL-Zeile fest, von der diese XBRL-Zeile aufgerollt wird.;
                           ENU=Specifies the label of the XBRL line from which this XBRL line is rolled up.];
                SourceExpr=Weight }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

