OBJECT Page 9066 Serv Outbound Technician Act.
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Aktivit�ten;
               ENU=Activities];
    SourceTable=Table9052;
    PageType=CardPart;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;

                 SetRespCenterFilter;
                 SETRANGE("Date Filter",WORKDATE,WORKDATE);
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 9   ;1   ;Group     ;
                CaptionML=[DEU=Ausgehende Serviceauftr�ge;
                           ENU=Outbound Service Orders];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 15      ;0   ;Action    ;
                                  CaptionML=[DEU=Neuer Serviceauftrag;
                                             ENU=New Service Order];
                                  RunObject=Page 5900;
                                  Image=Document;
                                  RunPageMode=Create }
                  { 16      ;0   ;Action    ;
                                  CaptionML=[DEU=Servicearbeitsschein;
                                             ENU=Service Item Worksheet];
                                  RunObject=Report 5936;
                                  Image=ServiceItemWorksheet }
                }
                 }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl von Serviceauftr�gen an, die im Rollencenter im Servicestapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of in-service orders that are displayed in the Service Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Service Orders - Today";
                DrillDownPageID=Service Orders }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl von zum Nachfassen markierten Serviceauftr�gen an, die im Rollencenter im Servicestapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of service orders that have been marked for follow up that are displayed in the Service Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Service Orders - to Follow-up";
                DrillDownPageID=Service Orders }

  }
  CODE
  {

    BEGIN
    END.
  }
}

