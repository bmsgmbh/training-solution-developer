OBJECT Page 5625 Maintenance Registration
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Wartungsregistrierung;
               ENU=Maintenance Registration];
    SourceTable=Table5616;
    DataCaptionFields=FA No.;
    PageType=List;
    AutoSplitKey=Yes;
    OnInsertRecord=VAR
                     FixedAsset@1000 : Record 5600;
                   BEGIN
                     FixedAsset.GET("FA No.");
                     "Maintenance Vendor No." := FixedAsset."Maintenance Vendor No.";
                   END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 19  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Anlage des Wartungspostens an.;
                           ENU=Specifies the fixed asset number of the maintenance entry.];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA No.";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem an der jeweiligen Anlage Wartungsarbeiten durchgef�hrt werden.;
                           ENU=Specifies the date when the fixed asset is being serviced.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Service Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kreditors an, der die Anlage f�r diesen Posten wartet.;
                           ENU=Specifies the number of the vendor who services the fixed asset for this entry.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Maintenance Vendor No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Bemerkung zu den durchzuf�hrenden Service-, Reparatur- und Wartungsarbeiten an.;
                           ENU=Specifies a comment for the service, repairs or maintenance to be performed on the fixed asset.];
                SourceExpr=Comment }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Wartungsbeauftragten an, der die Wartung der Anlage durchf�hrt.;
                           ENU=Specifies the name of the service agent who is servicing the fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Service Agent Name" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Telefonnummer des Wartungsbeauftragten an, der die Wartung der Anlage durchf�hrt.;
                           ENU=Specifies the phone number of the service agent who is servicing the fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Service Agent Phone No." }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Mobiltelefonnummer des Wartungsbeauftragten an, der die Wartung der Anlage durchf�hrt.;
                           ENU=Specifies the mobile phone number of the service agent who is servicing the fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Service Agent Mobile Phone";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

