OBJECT Page 389 Bank Account Statement List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Bankkonto Auszugs�bersicht;
               ENU=Bank Account Statement List];
    SourceTable=Table275;
    PageType=List;
    CardPageID=Bank Account Statement;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Bankkontos an, das mit diesem Bankkontoauszug abgestimmt wurde.;
                           ENU=Specifies the number of the bank account that has been reconciled with this Bank Account Statement.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kontoauszugs an, der mit dem Bankkonto abgestimmt wurde.;
                           ENU=Specifies the number of the bank's statement that has been reconciled with the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Statement No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum des Kontoauszugs an, der mit dem Bankkonto abgestimmt wurde.;
                           ENU=Specifies the date on the bank's statement that has been reconciled with the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Statement Date" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Schlusssaldo des Bankkontoauszugs aus der letzten gebuchten Bankkontoabstimmung an.;
                           ENU=Specifies the ending balance on the bank account statement from the last posted bank account reconciliation.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Balance Last Statement" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Schlusssaldo des Kontoauszugs an, der mit dem Bankkonto abgestimmt wurde.;
                           ENU=Specifies the ending balance on the bank's statement that has been reconciled with the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Statement Ending Balance" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

