OBJECT Table 11014 Certificate
{
  OBJECT-PROPERTIES
  {
    Date=15.09.15;
    Time=12:00:00;
    Version List=NAVDACH9.00;
  }
  PROPERTIES
  {
    OnRename=BEGIN
               IF "User ID" <> xRec."User ID" THEN
                 ERROR(STRSUBSTNO(RenameNotAllowedErr,FIELDCAPTION("User ID")));
             END;

    CaptionML=[DEU=Zertifikat;
               ENU=Certificate];
  }
  FIELDS
  {
    { 1   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Benutzer-ID;
                                                              ENU=User ID] }
    { 2   ;   ;Certificate Type    ;Option        ;InitValue=Soft token;
                                                   OnValidate=BEGIN
                                                                IF "Certificate Type" <> "Certificate Type"::"Soft token" THEN
                                                                  ERROR(Text1140000,"Certificate Type");
                                                              END;

                                                   CaptionML=[DEU=Zertifikatstyp;
                                                              ENU=Certificate Type];
                                                   OptionCaptionML=[DEU=Softtoken,Hardwaretoken;
                                                                    ENU=Soft token,Hardware token];
                                                   OptionString=Soft token,Hardware token }
    { 4   ;   ;PFX File            ;BLOB          ;CaptionML=[DEU=PFX-Datei;
                                                              ENU=PFX File] }
    { 5   ;   ;Elster Certificate  ;BLOB          ;CaptionML=[DEU=Elster-Zertifikat;
                                                              ENU=Elster Certificate] }
    { 6   ;   ;PFX File Password   ;BLOB          ;CaptionML=[DEU=Kennwort der PFX-Datei;
                                                              ENU=PFX File Password] }
  }
  KEYS
  {
    {    ;User ID                                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text1140000@1140000 : TextConst 'DEU=Bisher werden nur Zertifikate vom Typ %1 unterst�tzt.;ENU=Only Certificates of type %1 are supported until now.';
      EncryptionManagement@1140001 : Codeunit 1266;
      EncryptionMustBeEnabledErr@1140002 : TextConst 'DEU=Sie m�ssen die Verschl�sselung aktivieren, bevor diese Aktion ausgef�hrt werden kann.;ENU=You must enable encryption before you can perform this action.';
      RenameNotAllowedErr@1140003 : TextConst '@@@="%1=Field name";DEU=Eine Bearbeitung von %1 ist nicht zul�ssig.;ENU=Modification of %1 is not allowed.';

    PROCEDURE SavePassword@1(PasswordText@1000 : Text);
    VAR
      TempBLOB@1140000 : Record 99008535;
      OutStream@1001 : OutStream;
    BEGIN
      IF NOT EncryptionManagement.IsEncryptionPossible THEN
        ERROR(EncryptionMustBeEnabledErr);
      PasswordText := EncryptionManagement.Encrypt(PasswordText);
      TempBLOB.Blob.CREATEOUTSTREAM(OutStream);
      OutStream.WRITE(PasswordText);

      "PFX File Password" := TempBLOB.Blob;
      MODIFY
    END;

    PROCEDURE GetPassword@2() : Text;
    VAR
      EncryptionManagement@1003 : Codeunit 1266;
      InStream@1000 : InStream;
      PasswordText@1001 : Text;
    BEGIN
      IF NOT EncryptionManagement.IsEncryptionPossible THEN
        ERROR(EncryptionMustBeEnabledErr);

      CALCFIELDS("PFX File Password");
      "PFX File Password".CREATEINSTREAM(InStream);
      InStream.READ(PasswordText);
      EXIT(EncryptionManagement.Decrypt(PasswordText));
    END;

    PROCEDURE SaveElsterCertificate@1140001(TempBlob@1140000 : Record 99008535);
    VAR
      EmptyTempBlob@1140002 : Record 99008535;
    BEGIN
      IF "Elster Certificate".HASVALUE THEN BEGIN
        EmptyTempBlob.INIT;
        "Elster Certificate" := EmptyTempBlob.Blob;
        MODIFY(TRUE);
      END;

      "Elster Certificate" := TempBlob.Blob;
      CALCFIELDS("Elster Certificate");
      MODIFY(TRUE);
    END;

    PROCEDURE SavePfxFile@1140000(TempBlob@1140000 : Record 99008535);
    VAR
      EmptyTempBlob@1140001 : Record 99008535;
    BEGIN
      IF "PFX File".HASVALUE THEN BEGIN
        EmptyTempBlob.INIT;
        "PFX File" := EmptyTempBlob.Blob;
        MODIFY(TRUE);
      END;

      "PFX File" := TempBlob.Blob;
      CALCFIELDS("PFX File");
      MODIFY(TRUE);
    END;

    BEGIN
    END.
  }
}

