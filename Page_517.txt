OBJECT Page 517 Requisition Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Bestellvorschlagszeilen;
               ENU=Requisition Lines];
    LinksAllowed=No;
    SourceTable=Table246;
    PageType=List;
    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                     END;

    OnNewRecord=BEGIN
                  CLEAR(ShortcutDimCode);
                END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 28      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 27      ;2   ;Action    ;
                      CaptionML=[DEU=Bestellvorschlag anzeigen;
                                 ENU=Show Worksheet];
                      Image=ViewWorksheet;
                      OnAction=BEGIN
                                 ReqWkshTmpl.GET("Worksheet Template Name");
                                 ReqLine := Rec;
                                 ReqLine.FILTERGROUP(2);
                                 ReqLine.SETRANGE("Worksheet Template Name","Worksheet Template Name");
                                 ReqLine.SETRANGE("Journal Batch Name","Journal Batch Name");
                                 ReqLine.FILTERGROUP(0);
                                 PAGE.RUN(ReqWkshTmpl."Page ID",ReqLine);
                               END;
                                }
      { 30      ;2   ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[DEU=Reservierungsposten;
                                 ENU=Reservation Entries];
                      Image=ReservationLedger;
                      OnAction=BEGIN
                                 ShowReservationEntries(TRUE);
                               END;
                                }
      { 52      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Anzeigen oder Bearbeiten von Dimensionen, wie etwa eines Bereichs, eines Projekts oder einer Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 6500    ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[DEU=Artikel&verfolgungszeilen;
                                 ENU=Item &Tracking Lines];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Bestellvorschlagsvorlage an.;
                           ENU=Specifies the name of the requisition worksheet template.];
                SourceExpr="Worksheet Template Name" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Bestellvorschlags an.;
                           ENU=Specifies the name of the requisition worksheet.];
                SourceExpr="Journal Batch Name" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Bestellvorschlagszeile an.;
                           ENU=Specifies the number of the requisition worksheet line.];
                SourceExpr="Line No.";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Bestellvorschlagszeile an, die Sie erstellen.;
                           ENU=Specifies the type of requisition worksheet line you are creating.];
                SourceExpr=Type }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos oder Artikels an, die in der Zeile eingegeben wird.;
                           ENU=Specifies the number of the general ledger account or item to be entered on the line.];
                SourceExpr="No." }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Variantencode f�r den Artikel an.;
                           ENU=Specifies a variant code for the item.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt beschreibenden Text f�r den Posten an.;
                           ENU=Specifies text that describes the entry.];
                SourceExpr=Description }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code f�r den Lagerort an, an dem die bestellten Artikel registriert werden.;
                           ENU=Specifies a code for an inventory location where the items that are being ordered will be registered.];
                SourceExpr="Location Code";
                Visible=TRUE }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Einheiten des Artikels an.;
                           ENU=Specifies the number of units of the item.];
                SourceExpr=Quantity }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viele Einheiten dieses Artikels reserviert wurden.;
                           ENU=Specifies how many units of this item have been reserved.];
                SourceExpr="Reserved Quantity" }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einheitencode an, der zum Ermitteln des VK-Preises verwendet wird.;
                           ENU=Specifies the unit of measure code used to determine the unit price.];
                SourceExpr="Unit of Measure Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kreditors an, der die Artikel in der Bestellung liefern wird.;
                           ENU=Specifies the number of the vendor who will ship the items in the purchase order.];
                SourceExpr="Vendor No." }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Debitors an, f�r den die Artikel in der Einkaufszeile bestellt werden, wenn es sich um eine Direktlieferung handelt.;
                           ENU=Specifies the number of the customer for whom the purchase line items will be ordered, if the line is a drop shipment.];
                SourceExpr="Sell-to Customer No.";
                Visible=FALSE }

    { 29  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Bestellvorschlagszeile verkn�pft werden soll.;
                           ENU=Specifies the dimension value code to be linked to the requisition worksheet line.];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Bestellvorschlagszeile verkn�pft werden soll.;
                           ENU=Specifies the dimension value code to be linked to the requisition worksheet line.];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 302 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 304 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 306 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 308 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 310 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Bestelldatum an, das f�r die Bestellvorschlagszeile gilt.;
                           ENU=Specifies the order date that will apply to the requisition worksheet line.];
                SourceExpr="Order Date";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das erwartete Lieferdatum der Artikel an.;
                           ENU=Specifies the date when you can expect to receive the items.];
                SourceExpr="Due Date" }

  }
  CODE
  {
    VAR
      ReqLine@1000 : Record 246;
      ReqWkshTmpl@1001 : Record 244;
      ShortcutDimCode@1002 : ARRAY [8] OF Code[20];

    BEGIN
    END.
  }
}

