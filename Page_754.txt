OBJECT Page 754 Standard Item Journal
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Standard Artikel Buch.-Blatt;
               ENU=Standard Item Journal];
    SourceTable=Table752;
    PageType=ListPlus;
    OnInsertRecord=BEGIN
                     IF xRec.Code = '' THEN
                       SETRANGE(Code,Code);
                   END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Datensatzes an.;
                           ENU=Specifies the code of the record.];
                ApplicationArea=#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Datensatz in der Zeile des Buch.-Blatts an.;
                           ENU=Specifies the record in the line of the journal.];
                ApplicationArea=#Suite;
                SourceExpr=Description }

    { 9   ;1   ;Part      ;
                Name=StdItemJnlLines;
                ApplicationArea=#Suite;
                SubPageLink=Journal Template Name=FIELD(Journal Template Name),
                            Standard Journal Code=FIELD(Code);
                PagePartID=Page755 }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

