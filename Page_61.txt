OBJECT Page 61 Applied Customer Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Ausgeglichene Debitorenposten;
               ENU=Applied Customer Entries];
    SourceTable=Table21;
    DataCaptionExpr=Heading;
    PageType=List;
    OnOpenPage=BEGIN
                 RESET;

                 IF "Entry No." <> 0 THEN BEGIN
                   CreateCustLedgEntry := Rec;
                   IF CreateCustLedgEntry."Document Type" = 0 THEN
                     Heading := Text000
                   ELSE
                     Heading := FORMAT(CreateCustLedgEntry."Document Type");
                   Heading := Heading + ' ' + CreateCustLedgEntry."Document No.";

                   FindApplnEntriesDtldtLedgEntry;
                   SETCURRENTKEY("Entry No.");
                   SETRANGE("Entry No.");

                   IF CreateCustLedgEntry."Closed by Entry No." <> 0 THEN BEGIN
                     "Entry No." := CreateCustLedgEntry."Closed by Entry No.";
                     MARK(TRUE);
                   END;

                   SETCURRENTKEY("Closed by Entry No.");
                   SETRANGE("Closed by Entry No.",CreateCustLedgEntry."Entry No.");
                   IF FIND('-') THEN
                     REPEAT
                       MARK(TRUE);
                     UNTIL NEXT = 0;

                   SETCURRENTKEY("Entry No.");
                   SETRANGE("Closed by Entry No.");
                 END;

                 MARKEDONLY(TRUE);
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 20      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Posten;
                                 ENU=Ent&ry];
                      Image=Entry }
      { 27      ;2   ;Action    ;
                      CaptionML=[DEU=Mahnungs-/Zinsrechnungsposten;
                                 ENU=Reminder/Fin. Charge Entries];
                      ToolTipML=[DEU=Zeigt Posten an, die bei der Registrierung von Zinsrechnungen und Mahnungen erstellt wurden.;
                                 ENU=View entries that were created when reminders and finance charge memos were issued.];
                      RunObject=Page 444;
                      RunPageView=SORTING(Customer Entry No.);
                      RunPageLink=Customer Entry No.=FIELD(Entry No.);
                      Image=Reminder }
      { 32      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Zeigt Dimensionen an oder bearbeitet sie, wie etwa einen Bereich, ein Projekt oder eine Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 35      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=Det&aillierte Posten;
                                 ENU=Detailed &Ledger Entries];
                      ToolTipML=[DEU=Zeigen Sie eine Zusammenfassung aller gebuchten Posten und Korrekturen eines bestimmten Debitorenpostens an.;
                                 ENU=View a summary of the all posted entries and adjustments related to a specific customer ledger entry.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 573;
                      RunPageView=SORTING(Cust. Ledger Entry No.,Posting Date);
                      RunPageLink=Cust. Ledger Entry No.=FIELD(Entry No.),
                                  Customer No.=FIELD(Customer No.);
                      Image=View }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 19      ;1   ;Action    ;
                      CaptionML=[DEU=&Navigate;
                                 ENU=&Navigate];
                      ToolTipML=[DEU=Sucht alle Posten und Belege, die f�r die Belegnummer und das Buchungsdatum auf dem ausgew�hlten Posten oder Beleg vorhanden sind.;
                                 ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate.SetDoc("Posting Date","Document No.");
                                 Navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt das Buchungsdatum des Debitorenpostens an.;
                           ENU=Specifies the customer entry's posting date.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Belegart an, zu der der Debitorenposten geh�rt.;
                           ENU=Specifies the document type that the customer entry belongs to.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Type" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt die Belegnummer des Postens an.;
                           ENU=Specifies the entry's document number.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Debitorenpostens an.;
                           ENU=Specifies a description of the customer entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 77  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit dem Posten verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the entry.];
                SourceExpr="Global Dimension 1 Code";
                Visible=FALSE }

    { 75  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit dem Posten verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the entry.];
                SourceExpr="Global Dimension 2 Code";
                Visible=FALSE }

    { 73  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Verk�ufers an, der mit dem Posten verkn�pft ist.;
                           ENU=Specifies the code for the salesperson whom the entry is linked to.];
                SourceExpr="Salesperson Code";
                Visible=FALSE }

    { 71  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den W�hrungscode f�r den Betrag in der Zeile an.;
                           ENU=Specifies the currency code for the amount on the line.];
                ApplicationArea=#Suite;
                SourceExpr="Currency Code" }

    { 33  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag des urspr�nglichen Postens an.;
                           ENU=Specifies the amount of the original entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Original Amount" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag des Postens an.;
                           ENU=Specifies the amount of the entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag an, mit dem der Posten endg�ltig ausgeglichen (d. h. geschlossen) wurde.;
                           ENU=Specifies the amount that the entry was finally applied to (closed) with.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Closed by Amount" }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den W�hrungscode des Postens an, mit dem dieser Debitorenposten ausgeglichen (und geschlossen) wurde.;
                           ENU=Specifies the code of the currency of the entry that was applied to (and closed) this customer ledger entry.];
                SourceExpr="Closed by Currency Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag an, mit dem dieser Debitorenposten endg�ltig ausgeglichen (und geschlossen) wurde.;
                           ENU=Specifies the amount that was finally applied to (and closed) this customer ledger entry.];
                SourceExpr="Closed by Currency Amount";
                AutoFormatType=1;
                AutoFormatExpr="Closed by Currency Code";
                Visible=FALSE }

    { 25  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der mit dem Posten verkn�pft ist.;
                           ENU=Specifies the ID of the user associated with the entry.];
                SourceExpr="User ID";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Herkunftscode an, der mit dem Posten verkn�pft ist.;
                           ENU=Specifies the source code that is linked to the entry.];
                SourceExpr="Source Code";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ursachencode des Postens an.;
                           ENU=Specifies the reason code on the entry.];
                SourceExpr="Reason Code";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postennummer an, die dem Posten zugeordnet ist.;
                           ENU=Specifies the entry number that is assigned to the entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'DEU=Beleg;ENU=Document';
      CreateCustLedgEntry@1001 : Record 21;
      Navigate@1002 : Page 344;
      Heading@1003 : Text[50];

    LOCAL PROCEDURE FindApplnEntriesDtldtLedgEntry@1();
    VAR
      DtldCustLedgEntry1@1001 : Record 379;
      DtldCustLedgEntry2@1000 : Record 379;
    BEGIN
      DtldCustLedgEntry1.SETCURRENTKEY("Cust. Ledger Entry No.");
      DtldCustLedgEntry1.SETRANGE("Cust. Ledger Entry No.",CreateCustLedgEntry."Entry No.");
      DtldCustLedgEntry1.SETRANGE(Unapplied,FALSE);
      IF DtldCustLedgEntry1.FIND('-') THEN
        REPEAT
          IF DtldCustLedgEntry1."Cust. Ledger Entry No." =
             DtldCustLedgEntry1."Applied Cust. Ledger Entry No."
          THEN BEGIN
            DtldCustLedgEntry2.INIT;
            DtldCustLedgEntry2.SETCURRENTKEY("Applied Cust. Ledger Entry No.","Entry Type");
            DtldCustLedgEntry2.SETRANGE(
              "Applied Cust. Ledger Entry No.",DtldCustLedgEntry1."Applied Cust. Ledger Entry No.");
            DtldCustLedgEntry2.SETRANGE("Entry Type",DtldCustLedgEntry2."Entry Type"::Application);
            DtldCustLedgEntry2.SETRANGE(Unapplied,FALSE);
            IF DtldCustLedgEntry2.FIND('-') THEN
              REPEAT
                IF DtldCustLedgEntry2."Cust. Ledger Entry No." <>
                   DtldCustLedgEntry2."Applied Cust. Ledger Entry No."
                THEN BEGIN
                  SETCURRENTKEY("Entry No.");
                  SETRANGE("Entry No.",DtldCustLedgEntry2."Cust. Ledger Entry No.");
                  IF FIND('-') THEN
                    MARK(TRUE);
                END;
              UNTIL DtldCustLedgEntry2.NEXT = 0;
          END ELSE BEGIN
            SETCURRENTKEY("Entry No.");
            SETRANGE("Entry No.",DtldCustLedgEntry1."Applied Cust. Ledger Entry No.");
            IF FIND('-') THEN
              MARK(TRUE);
          END;
        UNTIL DtldCustLedgEntry1.NEXT = 0;
    END;

    PROCEDURE SetTempCustLedgEntry@2(NewTempCustLedgEntryNo@1102601000 : Integer);
    BEGIN
      IF NewTempCustLedgEntryNo <> 0 THEN BEGIN
        SETRANGE("Entry No.",NewTempCustLedgEntryNo);
        FIND('-');
      END;
    END;

    BEGIN
    END.
  }
}

