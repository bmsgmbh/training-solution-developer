OBJECT Page 251 General Journal Batches
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Fibu Buch.-Blattnamen;
               ENU=General Journal Batches];
    SourceTable=Table232;
    DataCaptionExpr=DataCaption;
    PageType=List;
    OnOpenPage=BEGIN
                 GenJnlManagement.OpenJnlBatch(Rec);
                 ShowAllowPaymentExportForPaymentTemplate;
               END;

    OnNewRecord=BEGIN
                  SetupNewBatch;
                END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 29      ;1   ;Action    ;
                      Name=EditJournal;
                      ShortCutKey=Return;
                      CaptionML=[DEU=Buch.-Blatt bearbeiten;
                                 ENU=Edit Journal];
                      ToolTipML=[DEU=Bearbeiten Sie die Finanzbuchhaltung.;
                                 ENU=Edit the general journal.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=OpenJournal;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 GenJnlManagement.TemplateSelectionFromBatch(Rec);
                               END;
                                }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[DEU=Bu&chung;
                                 ENU=P&osting];
                      Image=Post }
      { 16      ;2   ;Action    ;
                      Name=TestReport;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Testbericht;
                                 ENU=Test Report];
                      ToolTipML=[DEU=Zeigt einen Testbericht an, um m�gliche Fehler vor dem tats�chlichen Buchen des Buch.-Blatts oder Belegs finden und korrigieren zu k�nnen.;
                                 ENU=View a test report so that you can find and correct any errors before you perform the actual posting of the journal or document.];
                      ApplicationArea=#Basic,#Suite;
                      Image=TestReport;
                      OnAction=BEGIN
                                 ReportPrint.PrintGenJnlBatch(Rec);
                               END;
                                }
      { 17      ;2   ;Action    ;
                      ShortCutKey=F9;
                      CaptionML=[DEU=Bu&chen;
                                 ENU=P&ost];
                      ToolTipML=[DEU=Schlie�t den Beleg oder das Buch.-Blatt durch Buchen der Betr�ge und Mengen auf den entsprechenden Konten in den Firmenb�chern ab.;
                                 ENU=Finalize the document or journal by posting the amounts and quantities to the related accounts in your company books.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 233;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process }
      { 18      ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      CaptionML=[DEU=Buchen und d&rucken;
                                 ENU=Post and &Print];
                      ToolTipML=[DEU=Schlie�t den Beleg oder das Buch.-Blatt ab und bereitet das Drucken vor. Die Werte und Mengen werden auf den entsprechenden Konten gebucht. Ein Berichtanforderungsfenster wird ge�ffnet, in dem Sie angeben k�nnen, was auf dem Ausdruck enthalten sein soll.;
                                 ENU=Finalize and prepare to print the document or journal. The values and quantities are posted to the related accounts. A report request window where you can specify what to include on the print-out.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 234;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process }
      { 26      ;2   ;Action    ;
                      Name=MarkedOnOff;
                      CaptionML=[DEU=Markiert Ein/Aus;
                                 ENU=Marked On/Off];
                      ToolTipML=[DEU=Zeigt alle Buch.-Blattnamen oder nur markierte Buch.-Blattnamen an. Ein Buch.-Blattname wird markiert bei einem Fehler beim Versuch zum Buchen des Buch.-Blatts.;
                                 ENU=View all journal batches or only marked journal batches. A journal batch is marked if an attempt to post the general journal fails.];
                      ApplicationArea=#Basic,#Suite;
                      Image=Change;
                      OnAction=BEGIN
                                 MARKEDONLY(NOT MARKEDONLY);
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 8       ;1   ;ActionGroup;
                      CaptionML=[DEU=Periodische Aktivit�ten;
                                 ENU=Periodic Activities] }
      { 7       ;2   ;Action    ;
                      CaptionML=[DEU=Wiederk. Fibu Buch.-Blatt;
                                 ENU=Recurring General Journal];
                      ToolTipML=[DEU=Definieren Sie, wie Sie Transaktionen, die mit wenigen oder keinen �nderungen an Sach-, Bank-, Debitoren-, Kreditoren- und Anlagekonten auftreten, buchen.;
                                 ENU=Define how to post transactions that recur with few or no changes to general ledger, bank, customer, vendor, and fixed assets accounts.];
                      ApplicationArea=#Suite;
                      RunObject=Page 283;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Journal;
                      PromotedCategory=Process;
                      PromotedOnly=Yes }
      { 9       ;2   ;Action    ;
                      CaptionML=[DEU=Fibujournal;
                                 ENU=G/L Register];
                      ToolTipML=[DEU=Zeigt Sachposten an.;
                                 ENU=View posted G/L entries.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 116;
                      Image=GLRegisters }
      { 22      ;    ;ActionContainer;
                      ActionContainerType=Reports }
      { 20      ;1   ;Action    ;
                      CaptionML=[DEU=Sachkonto - Kontoblatt;
                                 ENU=Detail Trial Balance];
                      ToolTipML=[DEU=Zeigt ausf�hrliche Sachkontosalden und -aktivit�ten an.;
                                 ENU=View detail general ledger account balances and activities.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 4;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report;
                      PromotedOnly=Yes }
      { 14      ;1   ;Action    ;
                      CaptionML=[DEU=Rohbilanz;
                                 ENU=Trial Balance];
                      ToolTipML=[DEU=Zeigt Sachkontosalden und -aktivit�ten an.;
                                 ENU=View general ledger account balances and activities.];
                      ApplicationArea=#Suite;
                      RunObject=Report 6;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report;
                      PromotedOnly=Yes }
      { 12      ;1   ;Action    ;
                      CaptionML=[DEU=Rohbilanz nach Periode;
                                 ENU=Trial Balance by Period];
                      ToolTipML=[DEU=Zeigt Sachkontosalden und -aktivit�ten innerhalb einer ausgew�hlten Periode an, drucken oder speichern Sie sie.;
                                 ENU=View general ledger account balances and activities within a selected period.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 38;
                      Image=Report;
                      PromotedCategory=Report }
      { 10      ;1   ;Action    ;
                      CaptionML=[DEU=Fibujournal;
                                 ENU=G/L Register];
                      ToolTipML=[DEU=Zeigt Sachposten an.;
                                 ENU=View posted G/L entries.];
                      ApplicationArea=#Suite;
                      RunObject=Report 3;
                      Image=GLRegisters }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Buch.-Blatts an, das Sie erstellen.;
                           ENU=Specifies the name of the journal you are creating.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine kurze Beschreibung des Buch.-Blattnamens an, den Sie erstellen.;
                           ENU=Specifies a brief description of the journal batch you are creating.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Wert an, den Sie im Feld "Gegenkontoart" in der Buch.-Blattvorlage ausgew�hlt haben. Dieser Wert kann ge�ndert werden.;
                           ENU=Specifies the value you have selected in the Bal. Account Type field on the journal template. You may change this value.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bal. Account Type" }

    { 13  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Kopie des Felds "Gegenkontonr." oder der Gegenkontonummer f�r diesen Fibu Buch.-Blattnamen an.;
                           ENU=Specifies a copy of the Bal. Account No. field, or the balancing account number for this general journal batch.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bal. Account No." }

    { 19  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Nummernserie an, die verwendet wird, um Buch.-Blattzeilen in diesem Buch.-Blattnamen Belegnummern zuzuweisen.;
                           ENU=Specifies the code for the number series that will be used to assign document numbers to journal lines in this journal batch.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. Series" }

    { 21  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Nummernserie an, aus der Belegnummern den Posten zugewiesen werden, die aus diesem Buch.-Blattnamen gebucht werden.;
                           ENU=Specifies the code for the number series that will be used to assign document numbers to ledger entries that are posted from this journal batch.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting No. Series" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ursachencode an, der mit der Fibu Buch.-Blattvorlage verkn�pft ist.;
                           ENU=Specifies the reason code that is linked to the general journal template.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reason Code" }

    { 25  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Anwendung MwSt. f�r Konten und Gegenkonten in der Buch.-Blattzeile des gew�hlten Buch.-Blattnamens berechnen soll.;
                           ENU=Specifies whether the program to calculate VAT for accounts and balancing accounts on the journal line of the selected journal batch.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Copy VAT Setup to Jnl. Lines" }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob der manuelle Ausgleich von MwSt.-Betr�gen in Buch.-Blattvorlagen erlaubt ist.;
                           ENU=Specifies whether to allow the manual adjustment of VAT amounts in journal templates.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Allow VAT Difference" }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob Sie Bankzahlungsdateien von Buch.-Blattzeilen mit diesem Fibu Buch.-Blattnamen exportieren k�nnen.;
                           ENU=Specifies if you can export bank payment files from payment journal lines using this general journal batch.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Allow Payment Export";
                Visible=IsPaymentTemplate }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob das Feld "Betrag" in Buch.-Blattzeilen f�r dieselbe Belegnummer automatisch mit dem Wert ausgef�llt ist, der f�r den Ausgleich des Belegs erforderlich ist.;
                           ENU=Specifies if the Amount field on journal lines for the same document number is automatically prefilled with the value that is required to balance the document.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Suggest Balancing Amount" }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Format der Bankauszugsdatei an, die in diesen Fibu Buch.-Blattnamen importiert werden kann.;
                           ENU=Specifies the format of the bank statement file that can be imported into this general journal batch.];
                SourceExpr="Bank Statement Import Format";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ReportPrint@1000 : Codeunit 228;
      GenJnlManagement@1003 : Codeunit 230;
      IsPaymentTemplate@1001 : Boolean;

    LOCAL PROCEDURE DataCaption@1() : Text[250];
    VAR
      GenJnlTemplate@1000 : Record 80;
    BEGIN
      IF NOT CurrPage.LOOKUPMODE THEN
        IF GETFILTER("Journal Template Name") <> '' THEN
          IF GETRANGEMIN("Journal Template Name") = GETRANGEMAX("Journal Template Name") THEN
            IF GenJnlTemplate.GET(GETRANGEMIN("Journal Template Name")) THEN
              EXIT(GenJnlTemplate.Name + ' ' + GenJnlTemplate.Description);
    END;

    LOCAL PROCEDURE ShowAllowPaymentExportForPaymentTemplate@2();
    VAR
      GenJournalTemplate@1000 : Record 80;
    BEGIN
      IF GenJournalTemplate.GET("Journal Template Name") THEN
        IsPaymentTemplate := GenJournalTemplate.Type = GenJournalTemplate.Type::Payments;
    END;

    BEGIN
    END.
  }
}

