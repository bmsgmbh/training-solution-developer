OBJECT Page 5005278 Deliv. Rem. Comment Line List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVDACH10.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Lieferanmahnungsbem.-�bersicht;
               ENU=Deliv. Rem. Comment Line List];
    SourceTable=Table5005275;
    DataCaptionFields=Document Type,No.;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1140000;1;Group     ;
                GroupType=Repeater }

    { 1140001;2;Field     ;
                ToolTipML=[DEU=Gibt die Belegnummer der Lieferanmahnung an, zu der die Bemerkung geh�rt.;
                           ENU=Specifies the document number of the delivery reminder to which the comment applies.];
                ApplicationArea=#All;
                SourceExpr="No." }

    { 1140003;2;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Bemerkung erstellt wurde.;
                           ENU=Specifies the date the comment was created.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Date }

    { 1140005;2;Field     ;
                ToolTipML=[DEU=Gibt die eigentliche Bemerkung an.;
                           ENU=Specifies the comment itself.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Comment }

  }
  CODE
  {

    BEGIN
    END.
  }
}

