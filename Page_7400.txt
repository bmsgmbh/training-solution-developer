OBJECT Page 7400 Internal Movement List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Interne Umlagerungsliste;
               ENU=Internal Movement List];
    SourceTable=Table7346;
    PageType=List;
    CardPageID=Internal Movement;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 17      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Interne Umlagerung;
                                 ENU=&Internal Movement];
                      Image=CreateMovement }
      { 19      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 5776;
                      RunPageLink=Table Name=CONST(Internal Movement),
                                  Type=CONST(" "),
                                  No.=FIELD(No.);
                      Image=ViewComments }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Es gibt mehrere Tabellen und Felder, die derzeit nicht dokumentiert sind. F�r diese Tabellen und Felder ist keine spezielle Hilfe verf�gbar.;
                           ENU=There are a number of tables and fields that are not currently documented. There is no specific Help for these tables and fields.];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, an dem die interne Umlagerung ausgef�hrt wird.;
                           ENU=Specifies the code of the location where the internal movement is being performed.];
                SourceExpr="Location Code" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerplatz an, an dem die Artikel dieser internen Umlagerung bei der Kommissionierung abgelegt werden sollen.;
                           ENU=Specifies the bin where you want items on this internal movement to be placed when they are picked.];
                SourceExpr="To Bin Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Es gibt mehrere Tabellen und Felder, die derzeit nicht dokumentiert sind. F�r diese Tabellen und Felder ist keine spezielle Hilfe verf�gbar.;
                           ENU=There are a number of tables and fields that are not currently documented. There is no specific Help for these tables and fields.];
                SourceExpr="Due Date" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der f�r den Beleg verantwortlich ist.;
                           ENU=Specifies the ID of the user who is responsible for the document.];
                SourceExpr="Assigned User ID" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Methode an, nach der die internen Umlagerungen sortiert werden.;
                           ENU=Specifies the method by which the internal movements are sorted.];
                SourceExpr="Sorting Method" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

