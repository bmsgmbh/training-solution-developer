OBJECT Page 5607 Fixed Asset Setup
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Anlageneinrichtung;
               ENU=Fixed Asset Setup];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table5603;
    PageType=Card;
    PromotedActionCategoriesML=[DEU=Neu,Prozess,Bericht,Allgemein,Abschreibung,Buchung,Buch.-Blattvorlagen;
                                ENU=New,Process,Report,General,Depreciation,Posting,Journal Templates];
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

    ActionList=ACTIONS
    {
      { 20      ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 19      ;1   ;Action    ;
                      CaptionML=[DEU=AfA-B�cher;
                                 ENU=Depreciation Books];
                      ToolTipML=[DEU=Richtet AfA-B�cher f�r verschiedene Abschreibungszwecke ein, z. B. f�r MwSt.-Abrechnung und Finanzberichte.;
                                 ENU=Set up depreciation books for various depreciation purposes, such as tax and financial statements.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5611;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=DepreciationBooks;
                      PromotedCategory=Category5 }
      { 18      ;1   ;Action    ;
                      CaptionML=[DEU=AfA-Tabellen;
                                 ENU=Depreciation Tables];
                      ToolTipML=[DEU=Richtet die verschiedenen AfA-Methoden ein, mit denen Sie Anlagen abschreiben.;
                                 ENU=Set up the different depreciation methods that you will use to depreciate fixed assets.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5663;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Table;
                      PromotedCategory=Category5 }
      { 14      ;1   ;Action    ;
                      CaptionML=[DEU=Anlagenklassen;
                                 ENU=FA Classes];
                      ToolTipML=[DEU=Richtet verschiedene Anlagenklassen wie "Materielle Anlagen" und "Immaterielle Anlagen" ein, um Ihre Anlagen nach Kategorien anzuordnen.;
                                 ENU=Set up different asset classes, such as Tangible Assets and Intangible Assets, to group your fixed assets by categories.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5615;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=FARegisters;
                      PromotedCategory=Category4 }
      { 16      ;1   ;Action    ;
                      CaptionML=[DEU=Anlagensachgruppen;
                                 ENU=FA Subclasses];
                      ToolTipML=[DEU=Richtet verschiedene Sachgruppen f�r Anlagen wie "Niederlassung und Besitz" oder "Maschinen und Ausr�stung" ein, die Sie Anlagen und Versicherungspolicen zuweisen k�nnen.;
                                 ENU=Set up different asset subclasses, such as Plant and Property and Machinery and Equipment, that you can assign to fixed assets and insurance policies.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5616;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=FARegisters;
                      PromotedCategory=Category4 }
      { 12      ;1   ;Action    ;
                      CaptionML=[DEU=Anlagenstandorte;
                                 ENU=FA Locations];
                      ToolTipML=[DEU=Richtet verschiedene Standorte wie ein Lager oder einen bestimmten Lagerort innerhalb des Lagers ein, den Sie Anlagen zuweisen k�nnen.;
                                 ENU=Set up different locations, such as a warehouse or a location within a warehouse, that you can assign to fixed assets.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5617;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=FixedAssets;
                      PromotedCategory=Category4 }
      { 10      ;1   ;ActionGroup;
                      CaptionML=[DEU=Buchung;
                                 ENU=Posting] }
      { 9       ;2   ;Action    ;
                      CaptionML=[DEU=Anlagenbuchungsart Einr.;
                                 ENU=FA Posting Type Setup];
                      ToolTipML=[DEU=Legt fest, wie die Buchungsarten f�r erh�hte Abschreibung, Zuschreibung und benutzerdefinierte Abschreibung behandelt werden, die Sie beim Buchen auf Anlagen verwenden.;
                                 ENU=Define how to handle the Write-Down, Appreciation, Custom 1, and Custom 2 posting types that you use when posting to fixed assets.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5608;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=GeneralPostingSetup;
                      PromotedCategory=Category6 }
      { 7       ;2   ;Action    ;
                      CaptionML=[DEU=Anlagenbuchungsgruppen;
                                 ENU=FA Posting Groups];
                      ToolTipML=[DEU=Richtet die Konten ein, auf die Transaktionen f�r Anlagen f�r die einzelnen Buchungsgruppen gebucht werden, sodass Sie sie den entsprechenden Anlagen zuweisen k�nnen.;
                                 ENU=Set up the accounts to which transactions are posted for fixed assets for each posting group, so that you can assign them to the relevant fixed assets.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5613;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=GeneralPostingSetup;
                      PromotedCategory=Category6 }
      { 5       ;2   ;Action    ;
                      CaptionML=[DEU=Anl. Buch.-Blattvorlagen;
                                 ENU=FA Journal Templates];
                      ToolTipML=[DEU=Richtet die Nummernserien und Ursachencodes in den f�r die Anlagenbuchung verwendeten Buch.-Bl�ttern ein. Mithilfe verschiedener Vorlagen k�nnen Sie Fenster mit unterschiedlichen Layouts erstellen und den einzelnen Vorlagen Verfolgungscodes, Nummernserien und Berichte zuweisen.;
                                 ENU=Set up number series and reason codes in the journals that you use for fixed asset posting. By using different templates you can design windows with different layouts and you can assign trace codes, number series, and reports to each template.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5630;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=JournalSetup;
                      PromotedCategory=Category7 }
      { 3       ;2   ;Action    ;
                      CaptionML=[DEU=Anlagen Umbuch.-Blattvorl.;
                                 ENU=FA Reclass. Journal Templates];
                      ToolTipML=[DEU=Richtet die Nummernserien und Ursachencodes im f�r die Anlagenumbuchung verwendeten Buch.-Blatt ein. Mithilfe verschiedener Vorlagen k�nnen Sie Fenster mit unterschiedlichen Layouts erstellen und den einzelnen Vorlagen Verfolgungscodes, Nummernserien und Berichte zuweisen.;
                                 ENU=Set up number series and reason codes in the journal that you use to reclassify fixed assets. By using different templates you can design windows with different layouts and you can assign trace codes, number series, and reports to each template.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5637;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=JournalSetup;
                      PromotedCategory=Category7 }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Standardabschreibungsbuch in Buch.-Blattzeilen und Einkaufszeilen sowie beim Ausf�hren von Batchauftr�gen und Berichten an.;
                           ENU=Specifies the default depreciation book on journal lines and purchase lines and when you run batch jobs and reports.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Default Depr. Book" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob Ihre Anlagen in Haupt- und Unteranlagen aufgeteilt sind und Sie direkt auf die Hauptanlagen buchen m�chten.;
                           ENU=Specifies whether you have split your fixed assets into main assets and components, and you want to be able to post directly to main assets.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allow Posting to Main Assets" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das fr�heste Datum an, an dem Buchungen in Anlagen zul�ssig sind.;
                           ENU=Specifies the earliest date when posting to the fixed assets is allowed.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allow FA Posting From" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das letzte Datum an, an dem Buchungen in Anlagen zul�ssig sind.;
                           ENU=Specifies the latest date when posting to the fixed assets is allowed.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allow FA Posting To" }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Abschreibungsbuchcode an. Nutzen Sie die Versicherungseinrichtungen, m�ssen Sie einen Code angeben, damit Sie Versicherungsposten buchen k�nnen.;
                           ENU=Specifies a depreciation book code. If you use the insurance facilities, you must enter a code to post insurance coverage ledger entries.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Insurance Depr. Book";
                Importance=Additional }

    { 13  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob bei der Buchung von Anschaffungskostenposten mit ausgef�lltem Feld "Versicherungsnummer" Versicherungsposten gebucht werden sollen.;
                           ENU=Specifies you want to post insurance coverage ledger entries when you post acquisition cost entries with the Insurance No. field filled in.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Automatic Insurance Posting";
                Importance=Additional }

    { 1904569201;1;Group  ;
                CaptionML=[DEU=Nummerierung;
                           ENU=Numbering] }

    { 15  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Nummernserie an, die zum Zuweisen von Nummern zu Anlagen verwendet wird.;
                           ENU=Specifies the code for the number series that will be used to assign numbers to fixed assets.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Fixed Asset Nos." }

    { 17  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Nummernseriencode an, der zum Zuweisen von Nummern zu Versicherungspolicen verwendet wird.;
                           ENU=Specifies the number series code that will be used to assign numbers to insurance policies.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Insurance Nos." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

