OBJECT Page 5771 Whse. Put-away Subform
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Zeilen;
               ENU=Lines];
    MultipleNewLines=Yes;
    InsertAllowed=No;
    LinksAllowed=No;
    SourceTable=Table5767;
    DelayedInsert=Yes;
    SourceTableView=WHERE(Activity Type=CONST(Put-away));
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnInit=BEGIN
             QtyToHandleEditable := TRUE;
             BinCodeEditable := TRUE;
             ZoneCodeEditable := TRUE;
           END;

    OnNewRecord=BEGIN
                  "Activity Type" := xRec."Activity Type";
                END;

    OnDeleteRecord=BEGIN
                     CurrPage.UPDATE(FALSE);
                   END;

    OnAfterGetCurrRecord=BEGIN
                           EnableZoneBin;
                         END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1906587504;1 ;ActionGroup;
                      CaptionML=[DEU=F&unktionen;
                                 ENU=F&unctions];
                      Image=Action }
      { 1901991804;2 ;Action    ;
                      ShortCutKey=Ctrl+F11;
                      CaptionML=[DEU=&Zeile aufteilen;
                                 ENU=&Split Line];
                      Image=Split;
                      OnAction=VAR
                                 WhseActivLine@1001 : Record 5767;
                               BEGIN
                                 WhseActivLine.COPY(Rec);
                                 SplitLine(WhseActivLine);
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 1901313404;2 ;Action    ;
                      Name=ChangeUnitOfMeasure;
                      Ellipsis=Yes;
                      CaptionML=[DEU=&Einheit �ndern;
                                 ENU=&Change Unit Of Measure];
                      Image=UnitConversions;
                      OnAction=BEGIN
                                 ChangeUOM;
                               END;
                                }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 1902759804;2 ;Action    ;
                      CaptionML=[DEU=Herkunftsbelegzeile;
                                 ENU=Source Document Line];
                      Image=SourceDocLine;
                      OnAction=BEGIN
                                 ShowSourceLine;
                               END;
                                }
      { 1901652504;2 ;Action    ;
                      CaptionML=[DEU=Logistikbelegzeile;
                                 ENU=Whse. Document Line];
                      Image=Line;
                      OnAction=BEGIN
                                 ShowWhseLine;
                               END;
                                }
      { 1900545304;2 ;Action    ;
                      CaptionML=[DEU=Lagerplatzinhalts�bersicht;
                                 ENU=Bin Contents List];
                      Image=BinContent;
                      OnAction=BEGIN
                                 ShowBinContents;
                               END;
                                }
      { 1903867104;2 ;ActionGroup;
                      CaptionML=[DEU=Artikelverf�gbarkeit nach;
                                 ENU=Item Availability by];
                      Image=ItemAvailability }
      { 5       ;3   ;Action    ;
                      CaptionML=[DEU=Ereignis;
                                 ENU=Event];
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailability(ItemAvailFormsMgt.ByEvent);
                               END;
                                }
      { 1901313504;3 ;Action    ;
                      CaptionML=[DEU=Periode;
                                 ENU=Period];
                      Image=Period;
                      OnAction=BEGIN
                                 ItemAvailability(ItemAvailFormsMgt.ByPeriod);
                               END;
                                }
      { 1900295904;3 ;Action    ;
                      CaptionML=[DEU=Variante;
                                 ENU=Variant];
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 ItemAvailability(ItemAvailFormsMgt.ByVariant);
                               END;
                                }
      { 1901742304;3 ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[DEU=Lagerort;
                                 ENU=Location];
                      Image=Warehouse;
                      OnAction=BEGIN
                                 ItemAvailability(ItemAvailFormsMgt.ByLocation);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Aktionsart f�r die Lageraktivit�tszeile fest.;
                           ENU=Specifies the action type for the warehouse activity line.];
                SourceExpr="Action Type";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt die Art des Beleges, auf den sich die Zeile bezieht, z.�B. Verkaufsauftrag.;
                           ENU=Specifies the type of document that the line relates to, such as a sales order.];
                OptionCaptionML=[DEU=,Auftrag,,,Verkaufsreklamation,Bestellung,,,Einkaufsreklamation,Eingeh. Umlagerung,,FA-Verbrauch;
                                 ENU=,Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,Inbound Transfer,,Prod. Consumption];
                SourceExpr="Source Document" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Quellnummer des Belegs an, von dem der Posten stammt.;
                           ENU=Specifies the source number of the document from which the entry originates.];
                SourceExpr="Source No." }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Artikelnummer des Artikels an, der bearbeitet (z. B. kommissioniert oder eingelagert) werden soll.;
                           ENU=Specifies the item number of the item to be handled, such as picked or put away.];
                SourceExpr="Item No." }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Variantencode des zu bearbeitenden Artikels an.;
                           ENU=Specifies the variant code of the item to be handled.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Artikels in der Zeile an.;
                           ENU=Specifies a description of the item on the line.];
                SourceExpr=Description }

    { 14  ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[DEU=Legt die Seriennummer f�r die Bewegung im Beleg fest.;
                           ENU=Specifies the serial number to handle in the document.];
                SourceExpr="Serial No.";
                Visible=FALSE;
                Editable=FALSE;
                OnValidate=BEGIN
                             SerialNoOnAfterValidate;
                           END;
                            }

    { 40  ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[DEU=Legt die Chargennummer f�r die Bewegung im Beleg fest.;
                           ENU=Specifies the lot number to handle in the document.];
                SourceExpr="Lot No.";
                Visible=FALSE;
                Editable=FALSE;
                OnValidate=BEGIN
                             LotNoOnAfterValidate;
                           END;
                            }

    { 1106000000;2;Field  ;
                ToolTipML=[DEU=Gibt beim Einlagern von Artikeln das Ablaufdatum der Serien-/Chargennummern an.;
                           ENU=Specifies the expiration date of the serial/lot numbers if you are putting items away.];
                SourceExpr="Expiration Date";
                Visible=FALSE;
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerortcode an, an dem die Aktivit�t erfolgt.;
                           ENU=Specifies the code for the location where the activity occurs.];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Zone an, in der sich der Lagerplatz dieser Zeile befindet.;
                           ENU=Specifies the zone code where the bin on this line is located.];
                SourceExpr="Zone Code";
                Visible=FALSE;
                Editable=ZoneCodeEditable }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerplatz an, an dem Artikel in der Zeile abgewickelt werden.;
                           ENU=Specifies the bin where items on the line are handled.];
                SourceExpr="Bin Code";
                Visible=FALSE;
                Editable=BinCodeEditable;
                OnValidate=BEGIN
                             BinCodeOnAfterValidate;
                           END;
                            }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Regalnummer des Artikels zu Informationszwecken an.;
                           ENU=Specifies the shelf number of the item for informational use.];
                SourceExpr="Shelf No.";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels an, der bearbeitet werden soll, z. B. empfangen, eingelagert oder zugewiesen.;
                           ENU=Specifies the quantity of the item to be handled, such as received, put-away, or assigned.];
                SourceExpr=Quantity }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels in der Basiseinheit an, die bewegt werden muss.;
                           ENU=Specifies the quantity of the item to be handled, in the base unit of measure.];
                SourceExpr="Qty. (Base)";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viele Einheiten in dieser Lageraktivit�t bearbeitet werden.;
                           ENU=Specifies how many units to handle in this warehouse activity.];
                SourceExpr="Qty. to Handle";
                Editable=QtyToHandleEditable;
                OnValidate=BEGIN
                             QtytoHandleOnAfterValidate;
                           END;
                            }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Artikel an, die f�r diese Lageraktivit�t bearbeitet wurden.;
                           ENU=Specifies the number of items on the line that have been handled in this warehouse activity.];
                SourceExpr="Qty. Handled" }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge der Artikel an, die in dieser Lageraktivit�t bearbeitet werden sollen.;
                           ENU=Specifies the quantity of items to be handled in this warehouse activity.];
                SourceExpr="Qty. to Handle (Base)";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Artikel an, die f�r diese Lageraktivit�t bearbeitet wurden.;
                           ENU=Specifies the number of items on the line that have been handled in this warehouse activity.];
                SourceExpr="Qty. Handled (Base)";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Artikel an, die f�r diese Lageraktivit�tszeile noch nicht bearbeitet wurden.;
                           ENU=Specifies the number of items that have not yet been handled for this warehouse activity line.];
                SourceExpr="Qty. Outstanding" }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Anzahl der Artikel in Basiseinheiten fest, die f�r diese Lageraktivit�tszeile noch nicht bearbeitet wurden.;
                           ENU=Specifies the number of items, expressed in the base unit of measure, that have not yet been handled for this warehouse activity line.];
                SourceExpr="Qty. Outstanding (Base)";
                Visible=FALSE }

    { 62  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Lageraktivit�t abgeschlossen sein muss.;
                           ENU=Specifies the date when the warehouse activity must be completed.];
                SourceExpr="Due Date" }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einheitencode des Artikels in der Zeile an.;
                           ENU=Specifies the unit of measure code of the item on the line.];
                SourceExpr="Unit of Measure Code" }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels in der Zeile pro Ma�einheit an.;
                           ENU=Specifies the quantity per unit of measure of the item on the line.];
                SourceExpr="Qty. per Unit of Measure" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt Informationen �ber die Art des Ziels (Debitor oder Kreditor) an, das mit dieser Lageraktivit�tszeile verkn�pft ist.;
                           ENU=Specifies information about the type of destination, such as customer or vendor, associated with the warehouse activity line.];
                SourceExpr="Destination Type";
                Visible=FALSE }

    { 56  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer oder den Code des Debitors, Kreditors oder des Lagerorts an, mit dem diese Aktivit�tszeile verkn�pft ist.;
                           ENU=Specifies the number or code of the customer, vendor or location related to the activity line.];
                SourceExpr="Destination No.";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Logistikbelegs an, aus dem die Zeile stammte.;
                           ENU=Specifies the type of warehouse document from which the line originated.];
                OptionCaptionML=[DEU=" ,Wareneingang,,Interne Einlag.-Anforderung";
                                 ENU=" ,Receipt,,Internal Put-away"];
                SourceExpr="Whse. Document Type";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Logistikbelegs an, auf dem die Aktion dieser Zeile basiert.;
                           ENU=Specifies the number of the warehouse document that is the basis for the action on the line.];
                SourceExpr="Whse. Document No.";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Zeile im Logistikbeleg an, auf dem die Aktion dieser Zeile basiert.;
                           ENU=Specifies the number of the line in the warehouse document that is the basis for the action on the line.];
                SourceExpr="Whse. Document Line No.";
                Visible=FALSE }

    { 54  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der erforderlichen Ausstattung zur Ausf�hrung der Aktion in dieser Zeile an.;
                           ENU=Specifies the code of the equipment required when you perform the action on the line.];
                SourceExpr="Special Equipment Code";
                Visible=FALSE }

    { 58  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Option f�r spezielle Informationen bez�glich der Zuordnungsaktivit�t an.;
                           ENU=Specifies an option for specific information regarding the cross-dock activity.];
                SourceExpr="Cross-Dock Information" }

  }
  CODE
  {
    VAR
      ItemAvailFormsMgt@1001 : Codeunit 353;
      WMSMgt@1000 : Codeunit 7302;
      ZoneCodeEditable@19015203 : Boolean INDATASET;
      BinCodeEditable@19056442 : Boolean INDATASET;
      QtyToHandleEditable@19012452 : Boolean INDATASET;

    LOCAL PROCEDURE ShowSourceLine@1();
    BEGIN
      WMSMgt.ShowSourceDocLine(
        "Source Type","Source Subtype","Source No.","Source Line No.","Source Subline No.");
    END;

    LOCAL PROCEDURE ItemAvailability@7(AvailabilityType@1000 : 'Date,Variant,Location,Bin,Event,BOM');
    BEGIN
      ItemAvailFormsMgt.ShowItemAvailFromWhseActivLine(Rec,AvailabilityType);
    END;

    LOCAL PROCEDURE ChangeUOM@5();
    VAR
      WhseActLine@1001 : Record 5767;
      WhseChangeOUM@1000 : Report 7314;
    BEGIN
      TESTFIELD("Action Type");
      TESTFIELD("Breakbulk No.",0);
      TESTFIELD("Action Type",2);
      WhseChangeOUM.DefWhseActLine(Rec);
      WhseChangeOUM.RUNMODAL;
      IF WhseChangeOUM.ChangeUOMCode(WhseActLine) = TRUE THEN
        ChangeUOMCode(Rec,WhseActLine);
      CLEAR(WhseChangeOUM);
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE RegisterPutAwayYesNo@3();
    VAR
      WhseActivLine@1001 : Record 5767;
    BEGIN
      WhseActivLine.COPY(Rec);
      WhseActivLine.FILTERGROUP(3);
      WhseActivLine.SETRANGE(Breakbulk);
      WhseActivLine.FILTERGROUP(0);
      CODEUNIT.RUN(CODEUNIT::"Whse.-Act.-Register (Yes/No)",WhseActivLine);
      RESET;
      SETCURRENTKEY("Activity Type","No.","Sorting Sequence No.");
      FILTERGROUP(4);
      SETRANGE("Activity Type","Activity Type");
      SETRANGE("No.","No.");
      FILTERGROUP(3);
      SETRANGE(Breakbulk,FALSE);
      FILTERGROUP(0);
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE AutofillQtyToHandle@10();
    VAR
      WhseActivLine@1000 : Record 5767;
    BEGIN
      WhseActivLine.COPY(Rec);
      AutofillQtyToHandle(WhseActivLine);
    END;

    PROCEDURE DeleteQtyToHandle@6();
    VAR
      WhseActivLine@1000 : Record 5767;
    BEGIN
      WhseActivLine.COPY(Rec);
      DeleteQtyToHandle(WhseActivLine);
    END;

    LOCAL PROCEDURE ShowBinContents@7301();
    VAR
      BinContent@1000 : Record 7302;
    BEGIN
      IF "Action Type" = "Action Type"::Place THEN
        BinContent.ShowBinContents("Location Code","Item No.","Variant Code",'')
      ELSE
        BinContent.ShowBinContents("Location Code","Item No.","Variant Code","Bin Code");
    END;

    LOCAL PROCEDURE ShowWhseLine@2();
    BEGIN
      WMSMgt.ShowWhseDocLine(
        "Whse. Document Type","Whse. Document No.","Whse. Document Line No.");
    END;

    LOCAL PROCEDURE EnableZoneBin@4();
    BEGIN
      ZoneCodeEditable :=
        ("Action Type" = "Action Type"::Place) AND ("Breakbulk No." = 0);
      BinCodeEditable :=
        ("Action Type" = "Action Type"::Place) AND ("Breakbulk No." = 0);
      QtyToHandleEditable :=
        ("Action Type" = "Action Type"::Take) OR ("Breakbulk No." = 0);
    END;

    LOCAL PROCEDURE SerialNoOnAfterValidate@19074494();
    VAR
      ItemTrackingMgt@1000 : Codeunit 6500;
      ExpDate@1106000002 : Date;
      EntriesExist@1106000000 : Boolean;
    BEGIN
      IF "Serial No." <> '' THEN
        ExpDate := ItemTrackingMgt.ExistingExpirationDate("Item No.","Variant Code",
            "Lot No.","Serial No.",FALSE,EntriesExist);

      IF ExpDate <> 0D THEN
        "Expiration Date" := ExpDate;
    END;

    LOCAL PROCEDURE LotNoOnAfterValidate@19045288();
    VAR
      ItemTrackingMgt@1000 : Codeunit 6500;
      ExpDate@1106000000 : Date;
      EntriesExist@1106000002 : Boolean;
    BEGIN
      IF "Lot No." <> '' THEN
        ExpDate := ItemTrackingMgt.ExistingExpirationDate("Item No.","Variant Code",
            "Lot No.","Serial No.",FALSE,EntriesExist);

      IF ExpDate <> 0D THEN
        "Expiration Date" := ExpDate;
    END;

    LOCAL PROCEDURE BinCodeOnAfterValidate@19073508();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE QtytoHandleOnAfterValidate@19067087();
    BEGIN
      CurrPage.UPDATE(TRUE);
    END;

    BEGIN
    END.
  }
}

