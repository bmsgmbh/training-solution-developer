OBJECT Page 5912 Service Ledger Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Serviceposten;
               ENU=Service Ledger Entries];
    SourceTable=Table5907;
    SourceTableView=SORTING(Entry No.)
                    ORDER(Descending);
    DataCaptionFields=Service Contract No.,Service Item No. (Serviced),Service Order No.;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 96      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Posten;
                                 ENU=&Entry];
                      Image=Entry }
      { 97      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Anzeigen oder Bearbeiten von Dimensionen, wie etwa eines Bereichs, eines Projekts oder einer Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 95      ;1   ;Action    ;
                      CaptionML=[DEU=&Navigate;
                                 ENU=&Navigate];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate.SetDoc("Posting Date","Document No.");
                                 Navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem dieser Posten gebucht wurde.;
                           ENU=Specifies the date when this entry was posted.];
                SourceExpr="Posting Date" }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art f�r diesen Posten an.;
                           ENU=Specifies the type for this entry.];
                SourceExpr="Entry Type" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Serviceauftrags an, wenn dieser Posten f�r einen Serviceauftrag erstellt wurde.;
                           ENU=Specifies the type of the service order if this entry was created for a service order.];
                SourceExpr="Service Order Type";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Servicevertrags an, wenn dieser Posten mit einem Servicevertrag verkn�pft ist.;
                           ENU=Specifies the number of the service contract, if this entry is linked to a service contract.];
                SourceExpr="Service Contract No." }

    { 81  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Serviceauftrags an, wenn dieser Posten f�r einen Serviceauftrag erstellt wurde.;
                           ENU=Specifies the number of the service order, if this entry was created for a service order.];
                SourceExpr="Service Order No." }

    { 83  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die diesem Posten zugewiesene Projektnummer an.;
                           ENU=Specifies the job number assigned to this entry.];
                SourceExpr="Job No.";
                Visible=FALSE }

    { 79  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die diesem Posten zugewiesene Projektaufgabennummer an.;
                           ENU=Specifies the job task number assigned to this entry.];
                SourceExpr="Job Task No.";
                Visible=FALSE }

    { 91  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Buch.-Blattzeile an, die in der Tabelle "Projektplanungszeile" erstellt wird und mit diesem Projektposten verkn�pft ist.;
                           ENU=Specifies the journal line type that is created in the Job Planning Line table and linked to this job ledger entry.];
                SourceExpr="Job Line Type";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Belegart des Servicepostens an.;
                           ENU=Specifies the document type of the service ledger entry.];
                SourceExpr="Document Type" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Belegs an, aus dem dieser Posten erstellt wurde.;
                           ENU=Specifies the number of the document from which this entry was created.];
                SourceExpr="Document No." }

    { 69  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Rech. an Deb.-Nr. dieses Postens an.;
                           ENU=Specifies the bill-to customer number of this entry.];
                SourceExpr="Bill-to Customer No.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des mit diesem Posten verkn�pften Debitors an.;
                           ENU=Specifies the number of the customer related to this entry.];
                SourceExpr="Customer No." }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lief. an Code f�r den Debitor an, der diesem Posten zugeordnet ist.;
                           ENU=Specifies the ship-to code for the customer associated with this entry.];
                SourceExpr="Ship-to Code";
                Visible=FALSE }

    { 43  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Serviceartikels an, der diesem Posten zugeordnet ist.;
                           ENU=Specifies the number of the serviced item associated with this entry.];
                SourceExpr="Service Item No. (Serviced)" }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Serviceartikels an, der diesem Posten zugeordnet ist.;
                           ENU=Specifies the number of the serviced item associated with this entry.];
                SourceExpr="Item No. (Serviced)" }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Seriennummer des Serviceartikels an, der diesem Posten zugeordnet ist.;
                           ENU=Specifies the serial number of the serviced item associated with this entry.];
                SourceExpr="Serial No. (Serviced)" }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Fakturierungsintervall des Vertrags an, wenn dieser Posten aus einem Servicevertrag stammt.;
                           ENU=Specifies the invoice period of that contract, if this entry originates from a service contract.];
                SourceExpr="Contract Invoice Period";
                Visible=FALSE }

    { 39  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den globalen Dimensionscode 1 an, der diesem Posten zugeordnet ist.;
                           ENU=Specifies the Global Dimension 1 code associated with this entry.];
                SourceExpr="Global Dimension 1 Code" }

    { 41  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den globalen Dimensionscode 2 an, der diesem Posten zugeordnet ist.;
                           ENU=Specifies the Global Dimension 2 code associated with this entry.];
                SourceExpr="Global Dimension 2 Code" }

    { 47  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Vertragsgruppencode des Servicevertrags an, dem dieser Posten zugeordnet ist.;
                           ENU=Specifies the contract group code of the service contract to which this entry is associated.];
                SourceExpr="Contract Group Code";
                Visible=FALSE }

    { 49  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Herkunftsart dieses Postens an.;
                           ENU=Specifies the type of origin of this entry.];
                SourceExpr=Type }

    { 51  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Vertragsnummer, den Standardtextcode, die Ressourcennummer, die Artikelnummer, den Servicekostencode oder das Sachkonto f�r diesen Posten an.;
                           ENU=Specifies the contract number, standard text code, resource number, item number, service cost code, or the general ledger account for this entry.];
                SourceExpr="No." }

    { 53  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einstandsbetrag (gesamt) dieses Postens an.;
                           ENU=Specifies the total cost amount of this entry.];
                SourceExpr="Cost Amount" }

    { 55  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den gesamten Rabattbetrag f�r diesen Posten an.;
                           ENU=Specifies the total discount amount on this entry.];
                SourceExpr="Discount Amount" }

    { 57  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einstandspreis dieses Postens an.;
                           ENU=Specifies the unit cost of this entry.];
                SourceExpr="Unit Cost" }

    { 59  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Einheiten in diesem Posten an.;
                           ENU=Specifies the number of units in this entry.];
                SourceExpr=Quantity }

    { 61  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Einheiten in diesem Artikel an, die fakturiert werden sollen.;
                           ENU=Specifies the number of units in this entry that should be invoiced.];
                SourceExpr="Charged Qty." }

    { 63  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Preis einer Einheit des Serviceartikels in diesem Posten an.;
                           ENU=Specifies the price of one unit of the service item in this entry.];
                SourceExpr="Unit Price" }

    { 65  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Rabattprozentsatz dieses Postens an.;
                           ENU=Specifies the discount percentage of this entry.];
                SourceExpr="Discount %" }

    { 67  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den gesamten Vertragsrabattbetrag dieses Postens an.;
                           ENU=Specifies the total contract discount amount of this entry.];
                SourceExpr="Contract Disc. Amount";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag dieses Postens an.;
                           ENU=Specifies the amount of this entry.];
                SourceExpr="Amount (LCY)" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass dieser Posten kein Vorauszahlungsposten eines Servicevertrags ist.;
                           ENU=Specifies that this entry is not a prepaid entry from a service contract.];
                SourceExpr="Moved from Prepaid Acc." }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Servicevertragskontengruppe an, dem der Servicevertrag zugeordnet ist, wenn dieser Posten in einem Servicevertrag enthalten ist.;
                           ENU=Specifies the service contract account group code the service contract is associated with, if this entry is included in a service contract.];
                SourceExpr="Serv. Contract Acc. Gr. Code" }

    { 71  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Problemursachencode f�r diesen Posten an.;
                           ENU=Specifies the fault reason code for this entry.];
                SourceExpr="Fault Reason Code" }

    { 73  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Ressource, des Artikels, der Kosten, des Standardtexts, des Sachkontos oder des Servicevertrags an, der diesem Posten zugeordnet ist.;
                           ENU=Specifies a description of the resource, item, cost, standard text, general ledger account, or service contract associated with this entry.];
                SourceExpr=Description }

    { 85  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Gesch�ftsbuchungsgruppe an, die diesem Posten zugeordnet ist.;
                           ENU=Specifies the code for the general business posting group associated with this entry.];
                SourceExpr="Gen. Bus. Posting Group" }

    { 87  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Produktbuchungsgruppe an, die diesem Posten zugeordnet ist.;
                           ENU=Specifies the code for the general production posting group associated with this entry.];
                SourceExpr="Gen. Prod. Posting Group" }

    { 89  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r den Lagerort an, der diesem Posten zugeordnet ist.;
                           ENU=Specifies the code for the location associated with this entry.];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 45  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerplatzcode f�r diesen Posten an.;
                           ENU=Specifies the bin code for this entry.];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob der Servicevertrag oder der vertragsbezogene Serviceauftrag vorausbezahlt wurde.;
                           ENU=Specifies whether the service contract or contract-related service order was prepaid.];
                SourceExpr=Prepaid;
                Visible=FALSE }

    { 93  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt vertragsbezogene Serviceposten an.;
                           ENU=Specifies contract-related service ledger entries.];
                SourceExpr=Open }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der den diesem Posten zugeordneten Beleg bearbeitet hat.;
                           ENU=Specifies the ID of the user who worked on the document associated with this entry.];
                SourceExpr="User ID" }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die dieser Postennummer zugewiesene Zeile an.;
                           ENU=Specifies this entry number assigned to this line.];
                SourceExpr="Entry No." }

    { 75  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Postens an, auf den dieser Posten angewendet wird, wenn ein Posten f�r eine Servicegutschrift erstellt wird.;
                           ENU=Specifies the number of the entry to which this entry is applied, if an entry is created for a service credit memo.];
                SourceExpr="Applies-to Entry No." }

    { 77  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag f�r diesen Posten an.;
                           ENU=Specifies the amount on this entry.];
                SourceExpr=Amount }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Navigate@1000 : Page 344;

    BEGIN
    END.
  }
}

