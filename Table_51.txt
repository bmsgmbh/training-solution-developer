OBJECT Table 51 User Time Register
{
  OBJECT-PROPERTIES
  {
    Date=07.09.12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Zeitprotokoll;
               ENU=User Time Register];
    LookupPageID=Page71;
  }
  FIELDS
  {
    { 1   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnValidate=VAR
                                                                UserMgt@1000 : Codeunit 418;
                                                              BEGIN
                                                                UserMgt.ValidateUserID("User ID");
                                                              END;

                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Benutzer-ID;
                                                              ENU=User ID];
                                                   NotBlank=Yes }
    { 2   ;   ;Date                ;Date          ;CaptionML=[DEU=Datum;
                                                              ENU=Date] }
    { 3   ;   ;Minutes             ;Decimal       ;CaptionML=[DEU=Minuten;
                                                              ENU=Minutes];
                                                   DecimalPlaces=0:0;
                                                   MinValue=0 }
  }
  KEYS
  {
    {    ;User ID,Date                            ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

