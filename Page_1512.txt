OBJECT Page 1512 Notification Setup
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Benachrichtigungseinrichtung;
               ENU=Notification Setup];
    SourceTable=Table1512;
    PageType=List;
    RefreshOnActivate=Yes;
    ShowFilter=No;
    OnOpenPage=BEGIN
                 IF NOT HASFILTER THEN
                   SETRANGE("User ID",'');
               END;

    OnNewRecord=BEGIN
                  "User ID" := CleanWebFilter(GETFILTER("User ID"));
                END;

    ActionList=ACTIONS
    {
      { 3       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 7       ;1   ;Action    ;
                      Name=Notification Schedule;
                      CaptionML=[DEU=Benachrichtigungsplan;
                                 ENU=Notification Schedule];
                      ToolTipML=[DEU=Gibt an, wann der Benutzer Benachrichtigungen erh�lt. Der Wert wird aus dem Feld "Wiederholung" im Fenster "Benachrichtigungsplan" �bernommen.;
                                 ENU=Specify when the user receives notifications. The value is copied from the Recurrence field in the Notification Schedule window.];
                      ApplicationArea=#Suite;
                      RunObject=Page 1513;
                      RunPageLink=User ID=FIELD(User ID),
                                  Notification Type=FIELD(Notification Type);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=DateRange;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, �ber welche Ereignisart die Benachrichtigung ist.;
                           ENU=Specifies what type of event the notification is about.];
                ApplicationArea=#Suite;
                SourceExpr="Notification Type" }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Benachrichtigungsmethode an, die zum Erstellen von Benachrichtigungen f�r den Benutzer verwendet wird.;
                           ENU=Specifies the code of the notification method that is used to create notifications for the user.];
                ApplicationArea=#Suite;
                SourceExpr="Notification Method" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wann der Benutzer Benachrichtigungen erh�lt. Der Wert wird aus dem Feld "Wiederholung" im Fenster "Benachrichtigungsplan" �bernommen.;
                           ENU=Specifies when the user receives notifications. The value is copied from the Recurrence field in the Notification Schedule window.];
                ApplicationArea=#Suite;
                SourceExpr=Schedule;
                Editable=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das bevorzugte Anzeigeziel der Benachrichtigung an.;
                           ENU=Specifies the preferred display target of the notification.];
                ApplicationArea=#Suite;
                SourceExpr="Display Target" }

  }
  CODE
  {

    LOCAL PROCEDURE CleanWebFilter@1(FilterString@1000 : Text) : Text[50];
    BEGIN
      EXIT(DELCHR(FilterString,'=','*|@|'''));
    END;

    BEGIN
    END.
  }
}

