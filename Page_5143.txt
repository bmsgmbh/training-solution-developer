OBJECT Page 5143 Segment Criteria
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Segmentkriterien;
               ENU=Segment Criteria];
    SourceTable=Table5097;
    DataCaptionFields=Segment No.;
    PageType=List;
    OnOpenPage=BEGIN
                 SETCURRENTKEY("Segment No.","Line No.");
                 SETRANGE(Type);
               END;

    OnAfterGetRecord=BEGIN
                       StyleIsStrong := Type = Type::Action;
                       IF Type <> Type::Action THEN
                         ActionTableIndent := 1
                       ELSE
                         ActionTableIndent := 0;
                     END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 13      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 14      ;2   ;Action    ;
                      CaptionML=[DEU=Speichern;
                                 ENU=Save];
                      ToolTipML=[DEU=Speichert die Segmentkriterien.;
                                 ENU=Save the segment criteria.];
                      ApplicationArea=#RelationshipMgmt;
                      Image=Save;
                      OnAction=VAR
                                 SegHeader@1001 : Record 5076;
                               BEGIN
                                 SegHeader.GET("Segment No.");
                                 SegHeader.SaveCriteria;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                IndentationColumnName=ActionTableIndent;
                IndentationControls=ActionTable;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Segmentkriterienzeile an.;
                           ENU=Specifies the number of the segment criteria line.];
                SourceExpr="Line No.";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Informationen an, die in der Zeile angezeigt werden. Es sind zwei Optionen verf�gbar: "Aktion" und "Filter".;
                           ENU=Specifies the type of information that the line shows. There are two options: Action or Filter.];
                SourceExpr=Type;
                Visible=FALSE;
                Style=Strong;
                StyleExpr=StyleIsStrong }

    { 6   ;2   ;Field     ;
                Name=ActionTable;
                CaptionML=[DEU=Aktion/Tabelle;
                           ENU=Action/Table];
                ToolTipML=[DEU=Gibt die Aktionen an (Hinzuf�gen oder Entfernen von Kontakten), die Sie zur Definition der Segmentkriterien durchgef�hrt haben. Die zugeh�rige Tabelle wird unter jeder Aktion angezeigt.;
                           ENU=Specifies the actions that you have performed (adding or removing contacts) in order to define the segment criteria. The related table is shown under each action.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=ActionTable;
                Style=Strong;
                StyleExpr=StyleIsStrong }

    { 11  ;2   ;Field     ;
                CaptionML=[DEU=Filter;
                           ENU=Filter];
                ToolTipML=[DEU=Gibt an, welche Segmentkriterien angezeigt werden.;
                           ENU=Specifies which segment criteria are shown.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Filter }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      StyleIsStrong@1000 : Boolean INDATASET;
      ActionTableIndent@19031239 : Integer INDATASET;

    BEGIN
    END.
  }
}

