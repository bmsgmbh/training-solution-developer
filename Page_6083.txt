OBJECT Page 6083 Serv. Price Adjmt. Detail
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Servicepreiskorrekturdetails;
               ENU=Serv. Price Adjmt. Detail];
    SourceTable=Table6083;
    DataCaptionExpr=FormCaption;
    PageType=List;
    OnInit=BEGIN
             ServPriceAdjmtGrCodeVisible := TRUE;
           END;

    OnOpenPage=VAR
                 ServPriceAdjmtGroup@1001 : Record 6082;
                 ShowColumn@1000 : Boolean;
               BEGIN
                 ShowColumn := TRUE;
                 IF GETFILTER("Serv. Price Adjmt. Gr. Code") <> '' THEN
                   IF ServPriceAdjmtGroup.GET("Serv. Price Adjmt. Gr. Code") THEN
                     ShowColumn := FALSE
                   ELSE
                     RESET;
                 ServPriceAdjmtGrCodeVisible := ShowColumn;
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Servicepreiskorrekturgruppe an, der mit der gebuchten Servicezeile verkn�pft ist.;
                           ENU=Specifies the code of the service price adjustment group that applies to the posted service line.];
                SourceExpr="Serv. Price Adjmt. Gr. Code";
                Visible=ServPriceAdjmtGrCodeVisible }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art f�r die zu regulierende Serviceartikelzeile an.;
                           ENU=Specifies the type for the service item line to be adjusted.];
                SourceExpr=Type }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Serviceartikels, der Ressource, der Ressourcengruppe oder der Servicekosten an, abh�ngig davon, welcher Wert im Feld "Art" ausgew�hlt ist.;
                           ENU=Specifies the number of the service item, resource, resource group, or service cost, depending on the value selected in the Type field.];
                SourceExpr="No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Arbeitstyp der Ressource an.;
                           ENU=Specifies the work type of the resource.];
                SourceExpr="Work Type" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Produktbuchungsgruppe an, die dem Artikel oder der Ressource in der Zeile zugeordnet ist.;
                           ENU=Specifies the code of the general product posting group associated with the item or resource on the line.];
                SourceExpr="Gen. Prod. Posting Group" }

    { 25  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Serviceartikel, die Ressource, die Ressourcengruppe oder die Servicekosten an, deren Preis basierend auf dem Wert im Feld "Art" reguliert wird.;
                           ENU=Specifies the service item, resource, resource group, or service cost, of which the price will be adjusted, based on the value selected in the Type field.];
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ServPriceAdjmtGrCodeVisible@19029165 : Boolean INDATASET;

    LOCAL PROCEDURE FormCaption@1() : Text[180];
    VAR
      ServPriceAdjmtGrp@1000 : Record 6082;
    BEGIN
      IF GETFILTER("Serv. Price Adjmt. Gr. Code") <> '' THEN
        IF ServPriceAdjmtGrp.GET("Serv. Price Adjmt. Gr. Code") THEN
          EXIT(STRSUBSTNO('%1 %2',"Serv. Price Adjmt. Gr. Code",ServPriceAdjmtGrp.Description));
    END;

    BEGIN
    END.
  }
}

