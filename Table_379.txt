OBJECT Table 379 Detailed Cust. Ledg. Entry
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Permissions=TableData 379=m;
    DataCaptionFields=Customer No.;
    OnInsert=BEGIN
               SetLedgerEntryAmount;
             END;

    CaptionML=[DEU=Detaillierte Debitorenposten;
               ENU=Detailed Cust. Ledg. Entry];
    LookupPageID=Page573;
    DrillDownPageID=Page573;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[DEU=Lfd. Nr.;
                                                              ENU=Entry No.] }
    { 2   ;   ;Cust. Ledger Entry No.;Integer     ;TableRelation="Cust. Ledger Entry";
                                                   CaptionML=[DEU=Debitorenposten Lfd. Nr.;
                                                              ENU=Cust. Ledger Entry No.] }
    { 3   ;   ;Entry Type          ;Option        ;CaptionML=[DEU=Postenart;
                                                              ENU=Entry Type];
                                                   OptionCaptionML=[DEU=,Urspr. Posten,Ausgleich,Unrealisierter Verlust,Unrealisierter Gewinn,Realisierter Verlust,Realisierter Gewinn,Skonto,Skonto (ohne MwSt.),Skonto (MwSt.-Regulierung),Ausgl. Rundung,Restbetrag Korrektur,Zahlungstoleranz,Skontotoleranz,Zahlungstoleranz (ohne MwSt.),Zahlungstoleranz (MwSt.-Regulierung),Skontotoleranz (ohne MwSt.),Skontotoleranz (MwSt.-Regulierung);
                                                                    ENU=,Initial Entry,Application,Unrealized Loss,Unrealized Gain,Realized Loss,Realized Gain,Payment Discount,Payment Discount (VAT Excl.),Payment Discount (VAT Adjustment),Appln. Rounding,Correction of Remaining Amount,Payment Tolerance,Payment Discount Tolerance,Payment Tolerance (VAT Excl.),Payment Tolerance (VAT Adjustment),Payment Discount Tolerance (VAT Excl.),Payment Discount Tolerance (VAT Adjustment)];
                                                   OptionString=,Initial Entry,Application,Unrealized Loss,Unrealized Gain,Realized Loss,Realized Gain,Payment Discount,Payment Discount (VAT Excl.),Payment Discount (VAT Adjustment),Appln. Rounding,Correction of Remaining Amount,Payment Tolerance,Payment Discount Tolerance,Payment Tolerance (VAT Excl.),Payment Tolerance (VAT Adjustment),Payment Discount Tolerance (VAT Excl.),Payment Discount Tolerance (VAT Adjustment) }
    { 4   ;   ;Posting Date        ;Date          ;CaptionML=[DEU=Buchungsdatum;
                                                              ENU=Posting Date] }
    { 5   ;   ;Document Type       ;Option        ;CaptionML=[DEU=Belegart;
                                                              ENU=Document Type];
                                                   OptionCaptionML=[DEU=" ,Zahlung,Rechnung,Gutschrift,Zinsrechnung,Mahnung,Erstattung";
                                                                    ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 6   ;   ;Document No.        ;Code20        ;CaptionML=[DEU=Belegnr.;
                                                              ENU=Document No.] }
    { 7   ;   ;Amount              ;Decimal       ;CaptionML=[DEU=Betrag;
                                                              ENU=Amount];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 8   ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[DEU=Betrag (MW);
                                                              ENU=Amount (LCY)];
                                                   AutoFormatType=1 }
    { 9   ;   ;Customer No.        ;Code20        ;TableRelation=Customer;
                                                   CaptionML=[DEU=Debitorennr.;
                                                              ENU=Customer No.] }
    { 10  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[DEU=W�hrungscode;
                                                              ENU=Currency Code] }
    { 11  ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Benutzer-ID;
                                                              ENU=User ID] }
    { 12  ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[DEU=Herkunftscode;
                                                              ENU=Source Code] }
    { 13  ;   ;Transaction No.     ;Integer       ;CaptionML=[DEU=Transaktionsnr.;
                                                              ENU=Transaction No.] }
    { 14  ;   ;Journal Batch Name  ;Code10        ;TestTableRelation=No;
                                                   CaptionML=[DEU=Buch.-Blattname;
                                                              ENU=Journal Batch Name] }
    { 15  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[DEU=Ursachencode;
                                                              ENU=Reason Code] }
    { 16  ;   ;Debit Amount        ;Decimal       ;CaptionML=[DEU=Sollbetrag;
                                                              ENU=Debit Amount];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 17  ;   ;Credit Amount       ;Decimal       ;CaptionML=[DEU=Habenbetrag;
                                                              ENU=Credit Amount];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 18  ;   ;Debit Amount (LCY)  ;Decimal       ;CaptionML=[DEU=Sollbetrag (MW);
                                                              ENU=Debit Amount (LCY)];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 19  ;   ;Credit Amount (LCY) ;Decimal       ;CaptionML=[DEU=Habenbetrag (MW);
                                                              ENU=Credit Amount (LCY)];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 20  ;   ;Initial Entry Due Date;Date        ;CaptionML=[DEU=Urspr. Posten F�lligk.-Datum;
                                                              ENU=Initial Entry Due Date] }
    { 21  ;   ;Initial Entry Global Dim. 1;Code20 ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[DEU=Urspr. Posten Globale Dim. 1;
                                                              ENU=Initial Entry Global Dim. 1] }
    { 22  ;   ;Initial Entry Global Dim. 2;Code20 ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[DEU=Urspr. Posten Globale Dim. 2;
                                                              ENU=Initial Entry Global Dim. 2] }
    { 24  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[DEU=Gesch�ftsbuchungsgruppe;
                                                              ENU=Gen. Bus. Posting Group] }
    { 25  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[DEU=Produktbuchungsgruppe;
                                                              ENU=Gen. Prod. Posting Group] }
    { 29  ;   ;Use Tax             ;Boolean       ;CaptionML=[DEU=Verbrauchssteuer;
                                                              ENU=Use Tax] }
    { 30  ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[DEU=MwSt.-Gesch�ftsbuchungsgruppe;
                                                              ENU=VAT Bus. Posting Group] }
    { 31  ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[DEU=MwSt.-Produktbuchungsgruppe;
                                                              ENU=VAT Prod. Posting Group] }
    { 35  ;   ;Initial Document Type;Option       ;CaptionML=[DEU=Urspr. Belegart;
                                                              ENU=Initial Document Type];
                                                   OptionCaptionML=[DEU=" ,Zahlung,Rechnung,Gutschrift,Zinsrechnung,Mahnung,Erstattung";
                                                                    ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 36  ;   ;Applied Cust. Ledger Entry No.;Integer;
                                                   CaptionML=[DEU=Ausgegl. Debitorenposten Lfd. Nr.;
                                                              ENU=Applied Cust. Ledger Entry No.] }
    { 37  ;   ;Unapplied           ;Boolean       ;CaptionML=[DEU=Ausgleich aufgehoben;
                                                              ENU=Unapplied] }
    { 38  ;   ;Unapplied by Entry No.;Integer     ;TableRelation="Detailed Cust. Ledg. Entry";
                                                   CaptionML=[DEU=Ausgleich aufgehoben von Lfd. Nr.;
                                                              ENU=Unapplied by Entry No.] }
    { 39  ;   ;Remaining Pmt. Disc. Possible;Decimal;
                                                   CaptionML=[DEU=Restskonto m�glich;
                                                              ENU=Remaining Pmt. Disc. Possible];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 40  ;   ;Max. Payment Tolerance;Decimal     ;CaptionML=[DEU=Max. Zahlungstoleranz;
                                                              ENU=Max. Payment Tolerance];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 41  ;   ;Tax Jurisdiction Code;Code10       ;TableRelation="Tax Jurisdiction";
                                                   CaptionML=[DEU=Steuerzust�ndigkeitscode;
                                                              ENU=Tax Jurisdiction Code];
                                                   Editable=No }
    { 42  ;   ;Application No.     ;Integer       ;CaptionML=[DEU=Ausgleichsnr.;
                                                              ENU=Application No.];
                                                   Editable=No }
    { 43  ;   ;Ledger Entry Amount ;Boolean       ;CaptionML=[DEU=Postenbetrag;
                                                              ENU=Ledger Entry Amount];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Cust. Ledger Entry No.,Posting Date      }
    {    ;Cust. Ledger Entry No.,Entry Type,Posting Date;
                                                   SumIndexFields=Amount (LCY);
                                                   MaintainSQLIndex=No }
    {    ;Ledger Entry Amount,Cust. Ledger Entry No.,Posting Date;
                                                   SumIndexFields=Amount,Amount (LCY),Debit Amount,Debit Amount (LCY),Credit Amount,Credit Amount (LCY);
                                                   MaintainSQLIndex=No;
                                                   MaintainSIFTIndex=No }
    {    ;Initial Document Type,Entry Type,Customer No.,Currency Code,Initial Entry Global Dim. 1,Initial Entry Global Dim. 2,Posting Date;
                                                   SumIndexFields=Amount,Amount (LCY);
                                                   MaintainSIFTIndex=No }
    {    ;Customer No.,Currency Code,Initial Entry Global Dim. 1,Initial Entry Global Dim. 2,Initial Entry Due Date,Posting Date;
                                                   SumIndexFields=Amount,Amount (LCY) }
    {    ;Document No.,Document Type,Posting Date  }
    {    ;Applied Cust. Ledger Entry No.,Entry Type }
    {    ;Transaction No.,Customer No.,Entry Type  }
    {    ;Application No.,Customer No.,Entry Type  }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Entry No.,Cust. Ledger Entry No.,Customer No.,Posting Date,Document Type,Document No. }
  }
  CODE
  {

    PROCEDURE UpdateDebitCredit@19(Correction@1000 : Boolean);
    BEGIN
      IF ((Amount > 0) OR ("Amount (LCY)" > 0)) AND NOT Correction OR
         ((Amount < 0) OR ("Amount (LCY)" < 0)) AND Correction
      THEN BEGIN
        "Debit Amount" := Amount;
        "Credit Amount" := 0;
        "Debit Amount (LCY)" := "Amount (LCY)";
        "Credit Amount (LCY)" := 0;
      END ELSE BEGIN
        "Debit Amount" := 0;
        "Credit Amount" := -Amount;
        "Debit Amount (LCY)" := 0;
        "Credit Amount (LCY)" := -"Amount (LCY)";
      END;
    END;

    PROCEDURE SetZeroTransNo@87(TransactionNo@1000 : Integer);
    VAR
      DtldCustLedgEntry@1001 : Record 379;
      ApplicationNo@1002 : Integer;
    BEGIN
      DtldCustLedgEntry.SETCURRENTKEY("Transaction No.");
      DtldCustLedgEntry.SETRANGE("Transaction No.",TransactionNo);
      IF DtldCustLedgEntry.FINDSET(TRUE) THEN BEGIN
        ApplicationNo := DtldCustLedgEntry."Entry No.";
        REPEAT
          DtldCustLedgEntry."Transaction No." := 0;
          DtldCustLedgEntry."Application No." := ApplicationNo;
          DtldCustLedgEntry.MODIFY;
        UNTIL DtldCustLedgEntry.NEXT = 0;
      END;
    END;

    LOCAL PROCEDURE SetLedgerEntryAmount@1();
    BEGIN
      "Ledger Entry Amount" :=
        NOT (("Entry Type" = "Entry Type"::Application) OR ("Entry Type" = "Entry Type"::"Appln. Rounding"));
    END;

    PROCEDURE GetUnrealizedGainLossAmount@2(EntryNo@1000 : Integer) : Decimal;
    BEGIN
      SETCURRENTKEY("Cust. Ledger Entry No.","Entry Type");
      SETRANGE("Cust. Ledger Entry No.",EntryNo);
      SETRANGE("Entry Type","Entry Type"::"Unrealized Loss","Entry Type"::"Unrealized Gain");
      CALCSUMS("Amount (LCY)");
      EXIT("Amount (LCY)");
    END;

    BEGIN
    END.
  }
}

