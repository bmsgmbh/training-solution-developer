OBJECT Page 962 Manager Time Sheet Arc. List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Arbeitszeittabelle f�r Manager - Archiv�bersicht;
               ENU=Manager Time Sheet Arc. List];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table954;
    SourceTableView=SORTING(Resource No.,Starting Date);
    PageType=List;
    OnOpenPage=BEGIN
                 IF UserSetup.GET(USERID) THEN
                   CurrPage.EDITABLE := UserSetup."Time Sheet Admin.";
                 TimeSheetMgt.FilterTimeSheetsArchive(Rec,FIELDNO("Approver User ID"));
               END;

    ActionList=ACTIONS
    {
      { 3       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 5       ;1   ;Action    ;
                      ShortCutKey=Return;
                      CaptionML=[DEU=Arbeitszeittabelle an&zeigen;
                                 ENU=&View Time Sheet];
                      ToolTipML=[DEU=�ffnet die Arbeitszeittabelle.;
                                 ENU=Open the time sheet.];
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=OpenJournal;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 EditTimeSheet;
                               END;
                                }
      { 7       ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 8       ;1   ;ActionGroup;
                      CaptionML=[DEU=Arbei&tszeittabelle;
                                 ENU=&Time Sheet];
                      Image=Timesheet }
      { 9       ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 963;
                      RunPageLink=No.=FIELD(No.),
                                  Time Sheet Line No.=CONST(0);
                      Image=ViewComments }
      { 10      ;2   ;Action    ;
                      CaptionML=[DEU=Buchu&ngsposten;
                                 ENU=Posting E&ntries];
                      ToolTipML=[DEU=Zeigt die Ressourcenposten an, die in Verbindung mit der Arbeitszeittabelle gebucht wurden.;
                                 ENU=View the resource ledger entries that have been posted in connection with the.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 958;
                      RunPageLink=Time Sheet No.=FIELD(No.);
                      Image=PostingEntries }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der archivierten Arbeitszeittabelle an.;
                           ENU=Specifies the number of the archived time sheet.];
                ApplicationArea=#Jobs;
                SourceExpr="No." }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Startdatum f�r die archivierte Arbeitszeittabelle an.;
                           ENU=Specifies the start date for the archived time sheet.];
                ApplicationArea=#Jobs;
                SourceExpr="Starting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Enddatum f�r eine archivierte Arbeitszeittabelle an.;
                           ENU=Specifies the end date for an archived time sheet.];
                ApplicationArea=#Jobs;
                SourceExpr="Ending Date" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Liste der Ressourcennummern an, die einer archivierten Arbeitszeittabelle zugeordnet sind.;
                           ENU=Specifies the list of resource numbers associated with an archived time sheet.];
                ApplicationArea=#Jobs;
                SourceExpr="Resource No." }

  }
  CODE
  {
    VAR
      UserSetup@1001 : Record 91;
      TimeSheetMgt@1000 : Codeunit 950;

    LOCAL PROCEDURE EditTimeSheet@1();
    VAR
      TimeSheetLineArchive@1001 : Record 955;
    BEGIN
      TimeSheetMgt.SetTimeSheetArchiveNo("No.",TimeSheetLineArchive);
      PAGE.RUN(PAGE::"Manager Time Sheet Archive",TimeSheetLineArchive);
    END;

    BEGIN
    END.
  }
}

