OBJECT Page 518 Purchase Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Einkaufszeilen;
               ENU=Purchase Lines];
    LinksAllowed=No;
    SourceTable=Table39;
    PageType=List;
    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                     END;

    OnNewRecord=BEGIN
                  CLEAR(ShortcutDimCode);
                END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 30      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 25      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Beleg anzeigen;
                                 ENU=Show Document];
                      ToolTipML=[DEU=�ffnet den Beleg, in dem die ausgew�hlte Zeile vorhanden ist.;
                                 ENU=Open the document that the selected line exists on.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=View;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 PageManagement@1000 : Codeunit 700;
                               BEGIN
                                 PurchHeader.GET("Document Type","Document No.");
                                 PageManagement.PageRun(PurchHeader);
                               END;
                                }
      { 32      ;2   ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[DEU=Reservierungsposten;
                                 ENU=Reservation Entries];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ReservationLedger;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowReservationEntries(TRUE);
                               END;
                                }
      { 6500    ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[DEU=Artikel&verfolgungszeilen;
                                 ENU=Item &Tracking Lines];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ItemTrackingLines;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Belegs an, den Sie gerade erstellen.;
                           ENU=Specifies the type of document that you are about to create.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Type" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Belegnummer an.;
                           ENU=Specifies the document number.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kreditors an, der die Artikel liefert.;
                           ENU=Specifies the number of the vendor who will delivers the items.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Buy-from Vendor No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Zeile an.;
                           ENU=Specifies the line's number.];
                SourceExpr="Line No.";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Zeilenart an.;
                           ENU=Specifies the line type.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Type }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer eines Sachkontos, eines Artikels, einer Ressource, eines Zu-/Abschlags oder einer Anlage an, je nach Inhalt im Feld "Art".;
                           ENU=Specifies the number of a general ledger account, item, resource, additional cost, or fixed asset, depending on the contents of the Type field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No." }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Variantencode f�r den Artikel an.;
                           ENU=Specifies a variant code for the item.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Postens an, die auf dem Inhalt der Felder "Art" und "Nr." basiert.;
                           ENU=Specifies a description of the entry, which is based on the contents of the Type and No. fields.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r den Lagerort an, an dem sich die Artikel in der Zeile befinden.;
                           ENU=Specifies the code for the location where the items on the line will be located.];
                SourceExpr="Location Code";
                Visible=TRUE }

    { 31  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Einheiten des Artikels an, die in der Zeile angegeben werden.;
                           ENU=Specifies the number of units of the item that will be specified on the line.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Quantity }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den Wert im Feld "Reservierte Menge" fest, in Basiseinheiten ausgedr�ckt.;
                           ENU=Specifies the value in the Reserved Quantity field, expressed in the base unit of measure.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reserved Qty. (Base)" }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einheitencode an, der f�r die Einkaufszeile gilt.;
                           ENU=Specifies the unit of measure code that is valid for the purchase line.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit of Measure Code" }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den EK-Preis des Artikels in der Zeile an.;
                           ENU=Specifies the direct unit cost of the item on the line.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Direct Unit Cost" }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Prozentsatz der indirekten Kosten des Artikels an.;
                           ENU=Specifies the item's indirect cost percentage.];
                SourceExpr="Indirect Cost %";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einstandspreis des Artikels in der Zeile an.;
                           ENU=Specifies the unit cost of the item on the line.];
                SourceExpr="Unit Cost (LCY)";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Preis f�r eine Einheit des Artikels an.;
                           ENU=Specifies the price for one unit of the item.];
                SourceExpr="Unit Price (LCY)";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Nettobetrag (vor Abzug des Rechnungsrabattbetrags) an, der f�r die Artikel in der Zeile gezahlt werden muss.;
                           ENU=Specifies the net amount (before subtracting the invoice discount amount) that must be paid for the items on the line.];
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Line Amount" }

    { 64  ;2   ;Field     ;
                ToolTipML=[DEU=Wenn Sie dieses Feld und das Feld "Projektaufgabennr." ausf�llen, wird ein Artikelposten zusammen mit der Bestellzeile gebucht.;
                           ENU=If you fill in this field and the Job Task No. field, then a job ledger entry will be posted together with the purchase order line.];
                SourceExpr="Job No.";
                Visible=FALSE }

    { 1006;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Projektaufgabe an, die dem Einkaufsbeleg (Rechnung oder Gutschrift) entspricht.;
                           ENU=Specifies the number of the job task that corresponds to the purchase document (invoice or credit memo).];
                SourceExpr="Job Task No.";
                Visible=FALSE }

    { 1008;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Projektplanungszeile zusammen mit der Buchung eines Projektpostens an.;
                           ENU=Specifies a Job Planning Line together with the posting of a job ledger entry.];
                SourceExpr="Job Line Type";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit dem Einkauf verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the purchase.];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit dem Einkauf verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the purchase.];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 302 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 304 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 306 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 308 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 310 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt das Datum, an dem die Artikel in Ihrem Lager voraussichtlich verf�gbar sind.;
                           ENU=Specifies the date that you expect the items to be available in your warehouse.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Expected Receipt Date" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Einheiten in der Auftragszeile an, die noch nicht geliefert wurden.;
                           ENU=Specifies how many units on the order line have not yet been received.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Outstanding Quantity" }

    { 1002;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag in MW f�r diejenigen Artikel der Bestellung an, die noch nicht geliefert wurden.;
                           ENU=Specifies the amount for the items on the order that have not yet been received in LCY.];
                SourceExpr="Outstanding Amount (LCY)";
                Visible=FALSE }

    { 1004;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag in MW f�r die bestellten Artikel an, die erhalten, aber noch nicht fakturiert wurden.;
                           ENU=Specifies the amount for the ordered items that have been received but not yet invoiced in LCY.];
                SourceExpr="Amt. Rcd. Not Invoiced (LCY)";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      PurchHeader@1000 : Record 38;
      ShortcutDimCode@1001 : ARRAY [8] OF Code[20];

    BEGIN
    END.
  }
}

