OBJECT Page 5406 Prod. Order Line List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=FA-Zeilen�bersicht;
               ENU=Prod. Order Line List];
    SourceTable=Table5406;
    PageType=List;
    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                     END;

    OnNewRecord=BEGIN
                  CLEAR(ShortcutDimCode);
                END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 16      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 17      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Beleg anzeigen;
                                 ENU=Show Document];
                      Image=View;
                      OnAction=VAR
                                 ProdOrder@1000 : Record 5405;
                               BEGIN
                                 ProdOrder.GET(Status,"Prod. Order No.");
                                 CASE Status OF
                                   Status::Planned:
                                     PAGE.RUN(PAGE::"Planned Production Order",ProdOrder);
                                   Status::"Firm Planned":
                                     PAGE.RUN(PAGE::"Firm Planned Prod. Order",ProdOrder);
                                   Status::Released:
                                     PAGE.RUN(PAGE::"Released Production Order",ProdOrder);
                                 END;
                               END;
                                }
      { 49      ;2   ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[DEU=Reservierungsposten;
                                 ENU=Reservation Entries];
                      Image=ReservationLedger;
                      OnAction=BEGIN
                                 ShowReservationEntries(TRUE);
                               END;
                                }
      { 50      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[DEU=Artikel&verfolgungszeilen;
                                 ENU=Item &Tracking Lines];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 45  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Wert an, der aus dem entsprechenden Feld im Fertigungsauftragskopf �bernommen wird.;
                           ENU=Specifies a value that is copied from the corresponding field on the production order header.];
                SourceExpr=Status }

    { 47  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Fertigungsauftragsnummer aus einer Fremdarbeitenvorschlagszeile an, wenn die Zeile gebucht wird.;
                           ENU=Specifies the production order number from a subcontracting worksheet line when the line is posted.];
                SourceExpr="Prod. Order No." }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an, der gefertigt werden soll.;
                           ENU=Specifies the number of the item that is to be produced.];
                SourceExpr="Item No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code an, wenn Sie im Fenster "Artikelvarianten" Variantencodes eingerichtet haben.;
                           ENU=Specifies a code if you have set up variant codes in the Item Variants window.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Wert des Felds "Beschreibung" auf der Artikelkarte an. Wenn Sie einen Variantencode eingeben, wird stattdessen die Beschreibung der Variante in dieses Feld �bernommen.;
                           ENU=Specifies the value of the Description field on the item card. If you enter a variant code, the variant description is copied to this field instead.];
                SourceExpr=Description }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine zus�tzliche Beschreibung an.;
                           ENU=Specifies an additional description.];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Dimensionswertcode f�r eine Dimension an.;
                           ENU=Specifies a dimension value code for a dimension.];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Dimensionswertcode f�r eine Dimension an.;
                           ENU=Specifies a dimension value code for a dimension.];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 302 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 304 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 306 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 308 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 310 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerortcode an, wenn die gefertigten Artikel in einem bestimmten Lagerort gelagert werden sollen.;
                           ENU=Specifies the location code, if the produced items should be stored in a specific location.];
                SourceExpr="Location Code";
                Visible=TRUE }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die zu fertigende Menge an, wenn Sie diese Zeile manuell ausf�llen.;
                           ENU=Specifies the quantity to be produced if you manually fill in this line.];
                SourceExpr=Quantity }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viel der Menge in dieser Zeile gefertigt wurde.;
                           ENU=Specifies how much of the quantity on this line has been produced.];
                SourceExpr="Finished Quantity" }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Differenz zwischen geplanter Menge und fertig gestellter Menge an oder Null, wenn die fertig gestellte Menge gr��er als die Restmenge ist.;
                           ENU=Specifies the difference between the finished and planned quantities, or zero if the finished quantity is greater than the remaining quantity.];
                SourceExpr="Remaining Quantity" }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=�bernimmt den Wert in diesem Feld aus dem Feld "Ausschuss %" auf der Artikelkarte, wenn das Feld "Artikelnr." ausgef�llt wird.;
                           ENU=Copies the value in this field from the Scrap Percentage field on the item card when the Item No. field is filled in.];
                SourceExpr="Scrap %";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=�bernimmt das Datum in diesem Feld aus dem entsprechendem Feld im Fertigungsauftragskopf.;
                           ENU=Copies the date in this field from the corresponding field on the production order header.];
                SourceExpr="Due Date" }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Startdatum des Postens an, das aus dem FA-Arbeitsplan abgerufen wird.;
                           ENU=Specifies the entry's starting date, which is retrieved from the production order routing.];
                SourceExpr="Starting Date" }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Startzeit des Postens an, die aus dem FA-Arbeitsplan abgerufen wird.;
                           ENU=Specifies the entry's starting time, which is retrieved from the production order routing.];
                SourceExpr="Starting Time";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Enddatum des Postens an, das aus dem FA-Arbeitsplan abgerufen wird.;
                           ENU=Specifies the entry's ending date, which is retrieved from the production order routing.];
                SourceExpr="Ending Date" }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Endzeit des Postens an, die aus dem FA-Arbeitsplan abgerufen wird.;
                           ENU=Specifies the entry's ending time, which is retrieved from the production order routing.];
                SourceExpr="Ending Time";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Fertigungsst�ckliste an, die die Basis f�r das Erstellen der FA-Komponenten�bersicht f�r diese Zeile ist.;
                           ENU=Specifies the number of the production BOM that is the basis for creating the Prod. Order Component list for this line.];
                SourceExpr="Production BOM No." }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Berechnet den Einstandspreis auf Basis der Kosten der Komponenten in der FA-Komponenten�bersicht, wenn die Lagerabgangsmethode nicht "Standard" ist.;
                           ENU=Calculates the unit cost, based on the cost of the components in the production order component list, and the routing, if the costing method is not standard.];
                SourceExpr="Unit Cost" }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Berechnet der Betrag durch Multiplizieren des Einstandspreises mit der Menge.;
                           ENU=Calculates the amount by multiplying the Unit Cost by the Quantity.];
                SourceExpr="Cost Amount" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ShortcutDimCode@1000 : ARRAY [8] OF Code[20];

    BEGIN
    END.
  }
}

