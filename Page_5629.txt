OBJECT Page 5629 Fixed Asset Journal
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Anlagen Buch.-Blatt;
               ENU=Fixed Asset Journal];
    SaveValues=Yes;
    SourceTable=Table5621;
    DelayedInsert=Yes;
    DataCaptionFields=Journal Batch Name;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnOpenPage=VAR
                 JnlSelected@1000 : Boolean;
               BEGIN
                 IF IsOpenedFromBatch THEN BEGIN
                   CurrentJnlBatchName := "Journal Batch Name";
                   FAJnlManagement.OpenJournal(CurrentJnlBatchName,Rec);
                   EXIT;
                 END;
                 FAJnlManagement.TemplateSelection(PAGE::"Fixed Asset Journal",FALSE,Rec,JnlSelected);
                 IF NOT JnlSelected THEN
                   ERROR('');
                 FAJnlManagement.OpenJournal(CurrentJnlBatchName,Rec);
               END;

    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                     END;

    OnNewRecord=BEGIN
                  SetUpNewLine(xRec);
                  CLEAR(ShortcutDimCode);
                END;

    OnAfterGetCurrRecord=BEGIN
                           FAJnlManagement.GetFA("FA No.",FADescription);
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 60      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 61      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Zeigt Dimensionen an oder bearbeitet sie, wie etwa einen Bereich, ein Projekt oder eine Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 44      ;1   ;ActionGroup;
                      CaptionML=[DEU=A&nlage;
                                 ENU=Fixed &Asset];
                      Image=FixedAssets }
      { 46      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=&Karte;
                                 ENU=&Card];
                      ToolTipML=[DEU=Zeigt detaillierte Informationen �ber die Anlage an oder bearbeitet diese.;
                                 ENU=View or edit detailed information about the fixed asset.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5600;
                      RunPageLink=No.=FIELD(FA No.);
                      Image=EditLines }
      { 47      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=&Posten;
                                 ENU=Ledger E&ntries];
                      ToolTipML=[DEU=Zeigt die Posten f�r die ausgew�hlte Anlage an.;
                                 ENU=View the ledger entries for the selected fixed asset.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Codeunit 5634;
                      Image=CustomerLedger }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 45      ;1   ;ActionGroup;
                      CaptionML=[DEU=Bu&chung;
                                 ENU=P&osting];
                      Image=Post }
      { 49      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=&Testbericht;
                                 ENU=&Test Report];
                      ToolTipML=[DEU=Zeigt eine Vorschau der resultierenden Anlagenposten an, um die Ergebnisse vor der tats�chlichen Buchung anzuzeigen.;
                                 ENU=Preview the resulting fixed asset entries to see the consequences before you perform the actual posting.];
                      ApplicationArea=#FixedAssets;
                      Image=TestReport;
                      OnAction=BEGIN
                                 ReportPrint.PrintFAJnlLine(Rec);
                               END;
                                }
      { 50      ;2   ;Action    ;
                      Name=Post;
                      ShortCutKey=F9;
                      CaptionML=[DEU=Bu&chen;
                                 ENU=P&ost];
                      ToolTipML=[DEU=Schlie�t den Beleg oder das Buch.-Blatt durch Buchen der Betr�ge und Mengen auf den entsprechenden Konten in den Firmenb�chern ab.;
                                 ENU=Finalize the document or journal by posting the amounts and quantities to the related accounts in your company books.];
                      ApplicationArea=#FixedAssets;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"FA. Jnl.-Post",Rec);
                                 CurrentJnlBatchName := GETRANGEMAX("Journal Batch Name");
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 4       ;2   ;Action    ;
                      Name=Preview;
                      CaptionML=[DEU=Buchungsvorschau;
                                 ENU=Preview Posting];
                      ToolTipML=[DEU=Pr�ft die verschiedenen Postenarten, die beim Buchen des Belegs oder Buch.-Blatts erstellt werden.;
                                 ENU=Review the different types of entries that will be created when you post the document or journal.];
                      ApplicationArea=#FixedAssets;
                      Image=ViewPostedOrder;
                      OnAction=VAR
                                 FAJnlPost@1000 : Codeunit 5636;
                               BEGIN
                                 FAJnlPost.Preview(Rec);
                               END;
                                }
      { 51      ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      CaptionML=[DEU=Buchen und d&rucken;
                                 ENU=Post and &Print];
                      ToolTipML=[DEU=Schlie�t den Beleg oder das Buch.-Blatt ab und bereitet das Drucken vor. Die Werte und Mengen werden auf den entsprechenden Konten gebucht. Ein Berichtanforderungsfenster wird ge�ffnet, in dem Sie angeben k�nnen, was auf dem Ausdruck enthalten sein soll.;
                                 ENU=Finalize and prepare to print the document or journal. The values and quantities are posted to the related accounts. A report request window where you can specify what to include on the print-out.];
                      ApplicationArea=#FixedAssets;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"FA. Jnl.-Post+Print",Rec);
                                 CurrentJnlBatchName := GETRANGEMAX("Journal Batch Name");
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 42  ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[DEU=Buch.-Blattname;
                           ENU=Batch Name];
                ToolTipML=[DEU=Gibt den Buch.-Blattnamen des Anlagenbuchungs-Blatts an.;
                           ENU=Specifies the name of the journal batch of the fixed asset journal.];
                ApplicationArea=#FixedAssets;
                SourceExpr=CurrentJnlBatchName;
                OnValidate=BEGIN
                             FAJnlManagement.CheckName(CurrentJnlBatchName,Rec);
                             CurrentJnlBatchNameOnAfterVali;
                           END;

                OnLookup=BEGIN
                           CurrPage.SAVERECORD;
                           FAJnlManagement.LookupName(CurrentJnlBatchName,Rec);
                           CurrPage.UPDATE(FALSE);
                         END;
                          }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, das als Buchungsdatum in Anlagenposten verwendet wird.;
                           ENU=Specifies the date that will be used as the posting date on FA ledger entries.];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Posting Date" }

    { 58  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt dasselbe Datum wie im Feld "Anlagedatum" an, wenn die Zeile gebucht wird.;
                           ENU=Specifies the same date as the FA Posting Date field when the line is posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die entsprechende Belegart f�r den Betrag an, den Sie buchen m�chten.;
                           ENU=Specifies the appropriate document type for the amount you want to post.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Document Type" }

    { 7   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Belegnummer f�r die Buch.-Blattzeile an.;
                           ENU=Specifies a document number for the journal line.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Document No." }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Ressource an, f�r die der Posten gebucht werden soll.;
                           ENU=Specifies the number of the resource you want to post an entry for.];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA No.";
                OnValidate=BEGIN
                             FAJnlManagement.GetFA("FA No.",FADescription);
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 17  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Abschreibungsbuches an, auf das diese Zeile gebucht wird.;
                           ENU=Specifies the code for the depreciation book to which the line will be posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Depreciation Book Code" }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die entsprechende Buchungsart f�r den Betrag an, den Sie buchen m�chten.;
                           ENU=Specifies the appropriate posting type for the amount you want to post.];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Posting Type" }

    { 13  ;2   ;Field     ;
                ToolTipML=[DEU=Ruft den Inhalt der Beschreibung automatisch aus der Anlagenkarte ab, wenn das Feld "Anlagennr." ausgef�llt ist.;
                           ENU=Automatically retrieves the description from the FA card when the FA No. field is filled in.];
                ApplicationArea=#FixedAssets;
                SourceExpr=Description }

    { 15  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtbetrag an, aus dem die Buch.-Blattzeile besteht.;
                           ENU=Specifies the total amount the journal line consists of.];
                ApplicationArea=#FixedAssets;
                SourceExpr=Amount }

    { 54  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Shortcutdimension 1 an.;
                           ENU=Specifies the code for Shortcut Dimension 1.];
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 56  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Shortcutdimension 2 an.;
                           ENU=Specifies the code for Shortcut Dimension 2.];
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Buch.-Blattzeile verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the journal line.];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(3,ShortcutDimCode[3]);
                           END;
                            }

    { 302 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Buch.-Blattzeile verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the journal line.];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(4,ShortcutDimCode[4]);
                           END;
                            }

    { 304 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Buch.-Blattzeile verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the journal line.];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(5,ShortcutDimCode[5]);
                           END;
                            }

    { 306 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Buch.-Blattzeile verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the journal line.];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(6,ShortcutDimCode[6]);
                           END;
                            }

    { 308 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Buch.-Blattzeile verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the journal line.];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(7,ShortcutDimCode[7]);
                           END;
                            }

    { 310 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Buch.-Blattzeile verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the journal line.];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(8,ShortcutDimCode[8]);
                           END;
                            }

    { 19  ;2   ;Field     ;
                ToolTipML=[DEU=Stellt den gesch�tzten verbleibenden Wert einer Anlage dar, der nicht mehr verwendet werden kann.;
                           ENU=Represents the estimated residual value of a fixed asset when it can no longer be used.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Salvage Value";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Abschreibungstage an, wenn im Feld "Anlagenbuchungsart" die Option "Abschreibung" oder "Sonderabschreibung" gew�hlt wurde.;
                           ENU=Specifies the number of depreciation days if you have selected the Depreciation or Custom 1 option in the FA Posting Type field.];
                ApplicationArea=#FixedAssets;
                SourceExpr="No. of Depreciation Days" }

    { 29  ;2   ;Field     ;
                ToolTipML=[DEU=Legt fest, ob die Abschreibung der vorhandenen (alten) Anlage automatisch gebucht werden soll.;
                           ENU=Specifies whether to automatically post depreciation of the existing (old) fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Depr. until FA Posting Date" }

    { 31  ;2   ;Field     ;
                ToolTipML=[DEU=Legt fest, ob zus�tzliche Anschaffungskosten und ein m�glicher Restwert zu einer angeschafften Anlage gebucht werden sollen.;
                           ENU=Specifies whether to post an additional acquisition cost and a possible salvage value to an acquired asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Depr. Acquisition Cost" }

    { 25  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Warnungscode an.;
                           ENU=Specifies a maintenance code.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Maintenance Code";
                Visible=FALSE;
                OnValidate=BEGIN
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Versicherungscode an, wenn Sie im Feld "Anlagenbuchungsart" die Option "Anschaffungskosten" ausgew�hlt haben.;
                           ENU=Specifies an insurance code if you have selected the Acquisition Cost option in the FA Posting Type field.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Insurance No.";
                Visible=FALSE;
                OnValidate=BEGIN
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Anlagennummer an.;
                           ENU=Specifies a fixed asset number.];
                ApplicationArea=#Suite;
                SourceExpr="Budgeted FA No." }

    { 52  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Abschreibungsbuchcode an, wenn die Buch.-Blattzeile in diesem Abschreibungsbuch gebucht werden sollen, sowie in das Abschreibungsbuch im Feld "Abschreibungsbuchcode".;
                           ENU=Specifies a depreciation book code if you want the journal line to be posted to that depreciation book, as well as to the depreciation book in the Depreciation Book Code field.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Duplicate in Depreciation Book" }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Zeile in allen Abschreibungsb�chern gebucht werden soll, die andere Buch.-Blattnamen aufweisen und bei denen das Feld "Kopien erm�glichen" aktiviert ist.;
                           ENU=Specifies whether the line is to be posted to all depreciation books, using different journal batches and with a check mark in the Part of Duplication List field.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Use Duplication List";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=[DEU=W�hlt das Feld automatisch aus, wenn der Posten aus einem Anlagenumbuchungsblatt generiert wurde.;
                           ENU=Automatically selects the field if the entry was generated from an FA reclassification journal.];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Reclassification Entry" }

    { 33  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer eines gebuchten Anlagenpostens an, der als Stornoposten gekennzeichnet werden soll.;
                           ENU=Specifies the number of a posted FA ledger entry to mark as an error entry.];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Error Entry No." }

    { 2   ;1   ;Group      }

    { 1900116601;2;Group  ;
                GroupType=FixedLayout }

    { 1901313201;3;Group  ;
                CaptionML=[DEU=Anlagenbeschreibung;
                           ENU=FA Description] }

    { 40  ;4   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Anlage an.;
                           ENU=Specifies a description of the fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr=FADescription;
                Editable=FALSE;
                ShowCaption=No }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      FAJnlManagement@1000 : Codeunit 5638;
      ReportPrint@1001 : Codeunit 228;
      CurrentJnlBatchName@1002 : Code[10];
      FADescription@1003 : Text[30];
      ShortcutDimCode@1004 : ARRAY [8] OF Code[20];

    LOCAL PROCEDURE CurrentJnlBatchNameOnAfterVali@19002411();
    BEGIN
      CurrPage.SAVERECORD;
      FAJnlManagement.SetName(CurrentJnlBatchName,Rec);
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

