OBJECT Page 5977 Posted Service Invoices
{
  OBJECT-PROPERTIES
  {
    Date=23.02.17;
    Time=12:00:00;
    Version List=NAVW110.00.00.15601;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Gebuchte Servicerechnungen;
               ENU=Posted Service Invoices];
    SourceTable=Table5992;
    PageType=List;
    CardPageID=Posted Service Invoice;
    OnOpenPage=BEGIN
                 SetSecurityFilterOnRespCenter;
               END;

    OnAfterGetRecord=VAR
                       ServiceInvoiceHeader@1000 : Record 5992;
                     BEGIN
                       DocExchStatusStyle := GetDocExchStatusStyle;

                       ServiceInvoiceHeader.COPYFILTERS(Rec);
                       ServiceInvoiceHeader.SETFILTER("Document Exchange Status",'<>%1',"Document Exchange Status"::"Not Sent");
                       DocExchStatusVisible := NOT ServiceInvoiceHeader.ISEMPTY;
                     END;

    OnAfterGetCurrRecord=BEGIN
                           DocExchStatusStyle := GetDocExchStatusStyle;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 19      ;1   ;ActionGroup;
                      CaptionML=[DEU=R&echnung;
                                 ENU=&Invoice];
                      Image=Invoice }
      { 31      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[DEU=Statistik;
                                 ENU=Statistics];
                      RunObject=Page 6033;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 32      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 5911;
                      RunPageLink=Table Name=CONST(Service Invoice Header),
                                  No.=FIELD(No.),
                                  Type=CONST(General);
                      Image=ViewComments }
      { 1102601000;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Anzeigen oder Bearbeiten von Dimensionen, wie etwa eines Bereichs, eines Projekts oder einer Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 5       ;1   ;Action    ;
                      Name=SendCustom;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Senden;
                                 ENU=Send];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=SendToMultiple;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ServiceInvHeader := Rec;
                                 CurrPage.SETSELECTIONFILTER(ServiceInvHeader);
                                 ServiceInvHeader.SendRecords;
                               END;
                                }
      { 20      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=D&rucken;
                                 ENU=&Print];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CurrPage.SETSELECTIONFILTER(ServiceInvHeader);
                                 ServiceInvHeader.PrintRecords(TRUE);
                               END;
                                }
      { 25      ;1   ;Action    ;
                      CaptionML=[DEU=&Navigate;
                                 ENU=&Navigate];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
      { 7       ;1   ;Action    ;
                      Name=ActivityLog;
                      CaptionML=[DEU=Aktivit�tsprotokoll;
                                 ENU=Activity Log];
                      Image=Log;
                      OnAction=BEGIN
                                 ShowActivityLog;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der gebuchten Rechnung an.;
                           ENU=Specifies the number of the posted invoice.];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Debitors an, dem die Artikel auf der Rechnung geh�ren.;
                           ENU=Specifies the number of the customer who owns the items on the invoice.];
                SourceExpr="Customer No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Debitors auf der Servicerechnung an.;
                           ENU=Specifies the name of the customer on the service invoice.];
                SourceExpr=Name }

    { 37  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den W�hrungscode f�r die Betr�ge auf der Rechnung an.;
                           ENU=Specifies the currency code for the amounts on the invoice.];
                SourceExpr="Currency Code" }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postleitzahl der Adresse an.;
                           ENU=Specifies the postal code of the address.];
                SourceExpr="Post Code";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den L�nder-/Regionscode der Debitorenadresse an.;
                           ENU=Specifies the country/region code of the customer address.];
                SourceExpr="Country/Region Code";
                Visible=FALSE }

    { 35  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Kontaktperson beim Debitorenunternehmen an.;
                           ENU=Specifies the name of the contact person at the customer company.];
                SourceExpr="Contact Name";
                Visible=FALSE }

    { 147 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Debitors an, an den die Rechnung gesendet wurde.;
                           ENU=Specifies the number of the customer to whom the invoice was sent.];
                SourceExpr="Bill-to Customer No.";
                Visible=FALSE }

    { 145 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Debitors an, an den Sie die Rechnung gesendet haben.;
                           ENU=Specifies the name of the customer to whom you sent the invoice.];
                SourceExpr="Bill-to Name";
                Visible=FALSE }

    { 33  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postleitzahl der Adresse an.;
                           ENU=Specifies the postal code of the address.];
                SourceExpr="Bill-to Post Code";
                Visible=FALSE }

    { 29  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den L�nder-/Regionscode der Rechnungsadresse des Debitors an.;
                           ENU=Specifies the country/region code of the customer's billing address.];
                SourceExpr="Bill-to Country/Region Code";
                Visible=FALSE }

    { 129 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Kontaktperson an, an die Sie die Rechnung gesendet haben.;
                           ENU=Specifies the name of the contact person to whom you sent the invoice.];
                SourceExpr="Bill-to Contact";
                Visible=FALSE }

    { 125 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Adresse des Debitors an.;
                           ENU=Specifies the code of the customer' s address.];
                SourceExpr="Ship-to Code";
                Visible=FALSE }

    { 123 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Debitors an, an den der Service auf der Rechnung geliefert wurde.;
                           ENU=Specifies the name of the customer to whom the service on the invoice was shipped.];
                SourceExpr="Ship-to Name";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postleitzahl der Adresse an.;
                           ENU=Specifies the postal code of the address.];
                SourceExpr="Ship-to Post Code";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den L�nder-/Regionscode der Adresse an.;
                           ENU=Specifies the code of the country/region of the address.];
                SourceExpr="Ship-to Country/Region Code";
                Visible=FALSE }

    { 113 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Kontaktperson am Lagerort an, an den der Service ausgeliefert wurde.;
                           ENU=Specifies the name of the contact person at the location where the service has been shipped to.];
                SourceExpr="Ship-to Contact";
                Visible=FALSE }

    { 109 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Rechnung gebucht wurde.;
                           ENU=Specifies the date when the invoice was posted.];
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 69  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Verk�ufers an, der mit der Rechnung verkn�pft ist.;
                           ENU=Specifies the code of the salesperson associated with the invoice.];
                SourceExpr="Salesperson Code";
                Visible=FALSE }

    { 91  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der dieser Rechnung zugeordnet ist.;
                           ENU=Specifies the dimension value code associated to this invoice.];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 89  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der dieser Rechnung zugeordnet ist.;
                           ENU=Specifies the dimension value code associated to this invoice.];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 93  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerort an, wie etwa Lager oder Verteilungszentrum, von dem aus der Service geliefert wurde.;
                           ENU=Specifies the location, such as warehouse or distribution center, from which the service was shipped.];
                SourceExpr="Location Code";
                Visible=TRUE }

    { 1102601001;2;Field  ;
                ToolTipML=[DEU=Gibt das Datum an, an dem Sie den Servicebeleg erstellt haben.;
                           ENU=Specifies the date when you created the service document.];
                SourceExpr="Document Date";
                Visible=FALSE }

    { 1102601003;2;Field  ;
                ToolTipML=[DEU=Legt das Datum fest, an dem die Rechnung zur Zahlung f�llig ist.;
                           ENU=Specifies the date when the invoice is due for payment.];
                SourceExpr="Due Date";
                Visible=FALSE }

    { 3   ;2   ;Field     ;
                SourceExpr="Document Exchange Status";
                Visible=DocExchStatusVisible;
                Editable=FALSE;
                StyleExpr=DocExchStatusStyle;
                OnDrillDown=VAR
                              DocExchServDocStatus@1000 : Codeunit 1420;
                            BEGIN
                              DocExchServDocStatus.DocExchStatusDrillDown(Rec);
                            END;
                             }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ServiceInvHeader@1000 : Record 5992;
      DocExchStatusStyle@1001 : Text;
      DocExchStatusVisible@1002 : Boolean;

    BEGIN
    END.
  }
}

