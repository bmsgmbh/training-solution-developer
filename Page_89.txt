OBJECT Page 89 Job List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Projekt�bersicht;
               ENU=Job List];
    SourceTable=Table167;
    PageType=List;
    CardPageID=Job Card;
    OnInit=BEGIN
             JobSimplificationAvailable := IsJobSimplificationAvailable;
           END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 30      ;1   ;ActionGroup;
                      CaptionML=[DEU=Pro&jekt;
                                 ENU=&Job];
                      Image=Job }
      { 37      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+T;
                      CaptionML=[DEU=Projektaufgabenzei&len;
                                 ENU=Job Task &Lines];
                      ToolTipML=[DEU=Plant, wie Sie Ihre Planungsinformationen einrichten m�chten. In diesem Fenster k�nnen Sie die Aufgaben eines Projekts angeben. Um mit der Planung eines Projekts beginnen oder den Verbrauch f�r ein Projekt buchen zu k�nnen, m�ssen Sie mindestens eine Projektaufgabe einrichten.;
                                 ENU=Plan how you want to set up your planning information. In this window you can specify the tasks involved in a job. To start planning a job or to post usage for a job, you must set up at least one job task.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 1002;
                      RunPageLink=Job No.=FIELD(No.);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=TaskList;
                      PromotedCategory=Process }
      { 13      ;2   ;ActionGroup;
                      CaptionML=[DEU=&Dimensionen;
                                 ENU=&Dimensions];
                      Image=Dimensions }
      { 84      ;3   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Zuordnung f�r aktuellen Daten&satz;
                                 ENU=Dimensions-&Single];
                      ToolTipML=[DEU=Zeigen Sie den f�r den ausgew�hlten Datensatz eingerichteten einzelnen Dimensionssatz an, oder bearbeiten Sie ihn.;
                                 ENU=View or edit the single set of dimensions that are set up for the selected record.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(167),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 12      ;3   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      CaptionML=[DEU=Zuordnung f�r &markierte Datens�tze;
                                 ENU=Dimensions-&Multiple];
                      ToolTipML=[DEU=Zeigt oder bearbeitet Standarddimensionen f�r eine Gruppe von Datens�tzen. Sie k�nnen Transaktionen Dimensionscodes zuweisen, um die Kosten zu verteilen und historische Informationen zu analysieren.;
                                 ENU=View or edit dimensions for a group of records. You can assign dimension codes to transactions to distribute costs and analyze historical information.];
                      ApplicationArea=#Jobs;
                      Image=DimensionSets;
                      OnAction=VAR
                                 Job@1001 : Record 167;
                                 DefaultDimensionsMultiple@1002 : Page 542;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(Job);
                                 DefaultDimensionsMultiple.SetMultiJob(Job);
                                 DefaultDimensionsMultiple.RUNMODAL;
                               END;
                                }
      { 33      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[DEU=&Statistik;
                                 ENU=&Statistics];
                      ToolTipML=[DEU=Zeigt die Statistik dieses Projekts an.;
                                 ENU=View this job's statistics.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 1025;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 14      ;2   ;Action    ;
                      Name=SalesInvoicesCreditMemos;
                      CaptionML=[DEU=&Verkaufsrechnungen/Gutschriften;
                                 ENU=Sales &Invoices/Credit Memos];
                      ToolTipML=[DEU=Zeigt Verkaufsrechnungen oder Verkaufsgutschriften an, die mit dem ausgew�hlten Projekt in Verbindung stehen.;
                                 ENU=View sales invoices or sales credit memos that are related to the selected job.];
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      Image=GetSourceDoc;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 JobInvoices@1000 : Page 1029;
                               BEGIN
                                 JobInvoices.SetPrJob(Rec);
                                 JobInvoices.RUNMODAL;
                               END;
                                }
      { 34      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      ToolTipML=[DEU=Zeigt die Bemerkungen f�r dieses Projekt an.;
                                 ENU=View the comment sheet for this job.];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(Job),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 150     ;1   ;ActionGroup;
                      CaptionML=[DEU=W&IP;
                                 ENU=W&IP];
                      Image=WIP }
      { 153     ;2   ;Action    ;
                      CaptionML=[DEU=&WIP-Posten;
                                 ENU=&WIP Entries];
                      ToolTipML=[DEU=Zeigt Posten f�r das Projekt an, die als Umlaufbestand gebucht werden.;
                                 ENU=View entries for the job that are posted as work in process.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 1008;
                      RunPageView=SORTING(Job No.,Job Posting Group,WIP Posting Date)
                                  ORDER(Descending);
                      RunPageLink=Job No.=FIELD(No.);
                      Image=WIPEntries }
      { 154     ;2   ;Action    ;
                      CaptionML=[DEU=WIP-Sac&hposten;
                                 ENU=WIP &G/L Entries];
                      ToolTipML=[DEU=Zeigt die WIP-Sachposten des Projekts an.;
                                 ENU=View the job's WIP G/L entries.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 1009;
                      RunPageView=SORTING(Job No.)
                                  ORDER(Descending);
                      RunPageLink=Job No.=FIELD(No.);
                      Image=WIPLedger }
      { 29      ;1   ;ActionGroup;
                      CaptionML=[DEU=VK-&Preise;
                                 ENU=&Prices];
                      Image=Price }
      { 38      ;2   ;Action    ;
                      CaptionML=[DEU=&Ressource;
                                 ENU=&Resource];
                      ToolTipML=[DEU=Zeigt die Ressourcen VK-Preise dieses Projekts an.;
                                 ENU=View this job's resource prices.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 1011;
                      RunPageLink=Job No.=FIELD(No.);
                      Image=Resource }
      { 39      ;2   ;Action    ;
                      CaptionML=[DEU=Art&ikel;
                                 ENU=&Item];
                      ToolTipML=[DEU=Zeigt die Artikelpreise dieses Projekts an.;
                                 ENU=View this job's item prices.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 1012;
                      RunPageLink=Job No.=FIELD(No.);
                      Image=Item }
      { 40      ;2   ;Action    ;
                      CaptionML=[DEU=&Sachkonto;
                                 ENU=&G/L Account];
                      ToolTipML=[DEU=Zeigt die Sachkontopreise dieses Projekts an.;
                                 ENU=View this job's G/L account prices.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 1013;
                      RunPageLink=Job No.=FIELD(No.);
                      Image=JobPrice }
      { 21      ;1   ;ActionGroup;
                      CaptionML=[DEU=P&lanung;
                                 ENU=Plan&ning];
                      Image=Planning }
      { 24      ;2   ;Action    ;
                      CaptionML=[DEU=Ressourcen pro &Projekt;
                                 ENU=Resource &Allocated per Job];
                      ToolTipML=[DEU=Zeigt die Ressourcenzuordnung dieses Projekts an.;
                                 ENU=View this job's resource allocation.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 221;
                      Image=ViewJob }
      { 27      ;2   ;Action    ;
                      CaptionML=[DEU=Ressourcengr. pro Pro&jekt;
                                 ENU=Res. Group All&ocated per Job];
                      ToolTipML=[DEU=Zeigt die Ressourcengruppenzuordnung des Projekts an.;
                                 ENU=View the job's resource group allocation.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 228;
                      Image=ViewJob }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[DEU=Historie;
                                 ENU=History];
                      Image=History }
      { 32      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=P&osten;
                                 ENU=Ledger E&ntries];
                      ToolTipML=[DEU=Zeigt die Historie der Transaktionen an, die f�r den ausgew�hlten Datensatz gebucht wurden.;
                                 ENU=View the history of transactions that have been posted for the selected record.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 92;
                      RunPageView=SORTING(Job No.,Job Task No.,Entry Type,Posting Date)
                                  ORDER(Descending);
                      RunPageLink=Job No.=FIELD(No.);
                      Promoted=Yes;
                      Image=CustomerLedger;
                      PromotedCategory=Process }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;ActionGroup;
                      Name=<Action9>;
                      CaptionML=[DEU=F&unktionen;
                                 ENU=F&unctions];
                      ActionContainerType=NewDocumentItems;
                      Image=Action }
      { 16      ;2   ;Action    ;
                      Name=CopyJob;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Projekt &kopieren;
                                 ENU=&Copy Job];
                      ToolTipML=[DEU=Kopiert ein Projekt und seine Projektaufgaben, Planungszeilen und Preise.;
                                 ENU=Copy a job and its job tasks, planning lines, and prices.];
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CopyFromTask;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 CopyJob@1000 : Page 1040;
                               BEGIN
                                 CopyJob.SetFromJob(Rec);
                                 CopyJob.RUNMODAL;
                               END;
                                }
      { 1903691404;2 ;Action    ;
                      CaptionML=[DEU=Projektverkauf&srechnung erstellen;
                                 ENU=Create Job &Sales Invoice];
                      ToolTipML=[DEU=Verwendet eine Stapelverarbeitung, um Projektverkaufsrechnungen f�r die beteiligten Projektplanzeilen zu erstellen.;
                                 ENU=Use a batch job to help you create job sales invoices for the involved job planning lines.];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1093;
                      Promoted=Yes;
                      Image=JobSalesInvoice;
                      PromotedCategory=Process }
      { 7       ;2   ;ActionGroup;
                      CaptionML=[DEU=W&IP;
                                 ENU=W&IP];
                      Image=WIP }
      { 5       ;3   ;Action    ;
                      Name=<Action151>;
                      Ellipsis=Yes;
                      CaptionML=[DEU=WIP bere&chnen;
                                 ENU=&Calculate WIP];
                      ToolTipML=[DEU=F�hrt die Stapelverarbeitung "WIP berechnen Projekt" aus.;
                                 ENU=Run the Job Calculate WIP batch job.];
                      ApplicationArea=#Jobs;
                      Image=CalculateWIP;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 Job@1002 : Record 167;
                               BEGIN
                                 TESTFIELD("No.");
                                 Job.COPY(Rec);
                                 Job.SETRANGE("No.","No.");
                                 REPORT.RUNMODAL(REPORT::"Job Calculate WIP",TRUE,FALSE,Job);
                               END;
                                }
      { 3       ;3   ;Action    ;
                      Name=<Action152>;
                      Ellipsis=Yes;
                      CaptionML=[DEU=WI&P auf Sachkonten buchen;
                                 ENU=&Post WIP to G/L];
                      ToolTipML=[DEU=F�hrt die Stapelverarbeitung "WIP nach Sachkonten Projekt" aus.;
                                 ENU=Run the Job Post WIP to G/L batch job.];
                      ApplicationArea=#Jobs;
                      Image=PostOrder;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 Job@1001 : Record 167;
                               BEGIN
                                 TESTFIELD("No.");
                                 Job.COPY(Rec);
                                 Job.SETRANGE("No.","No.");
                                 REPORT.RUNMODAL(REPORT::"Job Post WIP to G/L",TRUE,FALSE,Job);
                               END;
                                }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1903776506;1 ;Action    ;
                      CaptionML=[DEU=Budgetvergleich Projekt;
                                 ENU=Job Actual to Budget];
                      ToolTipML=[DEU=Vergleicht budgetierte Betr�ge und Verbrauchsbetr�ge f�r ausgew�hlte Projekte. In allen Zeilen des ausgew�hlten Projekts sind Menge, Einstandsbetrag und Zeilenbetrag angegeben.;
                                 ENU=Compare budgeted and usage amounts for selected jobs. All lines of the selected job show quantity, total cost, and line amount.];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1009;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1901542506;1 ;Action    ;
                      CaptionML=[DEU=Projektanalyse;
                                 ENU=Job Analysis];
                      ToolTipML=[DEU=Analysiert das Projekt, wie etwa die budgetierten Preise, VK-Preise Verbrauch und Vertragspreise, und vergleicht dann die drei Preiss�tze.;
                                 ENU=Analyze the job, such as the budgeted prices, usage prices, and contract prices, and then compares the three sets of prices.];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1008;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1902943106;1 ;Action    ;
                      CaptionML=[DEU=Projekt - Planzeilen;
                                 ENU=Job - Planning Lines];
                      ToolTipML=[DEU=Zeigt alle Planungszeilen f�r das Projekt an. Mithilfe dieses Fensters planen Sie, welche Artikel, Ressourcen und Aufwands-Finanzbuchhaltungsposten f�r ein Projekt verwendet werden sollen (Budget). Sie k�nnen aber auch angeben, was tats�chlich mit dem Debitor als f�r das Projekt zahlbar vereinbart wurde (Fakturierbar).;
                                 ENU=View all planning lines for the job. You use this window to plan what items, resources, and general ledger expenses that you expect to use on a job (budget) or you can specify what you actually agreed with your customer that he should pay for the job (billable).];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1006;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1903186006;1 ;Action    ;
                      CaptionML=[DEU=Projekt - Akontovorschlag;
                                 ENU=Job - Suggested Billing];
                      ToolTipML=[DEU=Zeigt eine Liste aller Projekte nach Debitor gruppiert an, welcher Betrag dem Debitor bereits in Rechnung gestellt wurde und welcher Restbetrag noch zu berechnen ist (Akontovorschlag).;
                                 ENU=View a list of all jobs, grouped by customer, how much the customer has already been invoiced, and how much remains to be invoiced, that is, the suggested billing.];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1011;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1900510006;1 ;Action    ;
                      CaptionML=[DEU=Projekte pro Debitor;
                                 ENU=Jobs per Customer];
                      ToolTipML=[DEU=F�hrt den Bericht "Projekte pro Debitor" aus.;
                                 ENU=Run the Jobs per Customer report.];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1012;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1905887906;1 ;Action    ;
                      CaptionML=[DEU=Artikel pro Projekt;
                                 ENU=Items per Job];
                      ToolTipML=[DEU=Zeigt, welche Artikel f�r ein bestimmtes Projekt verwendet werden.;
                                 ENU=View which items are used for a specific job.];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1013;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1906922906;1 ;Action    ;
                      CaptionML=[DEU=Projekte pro Artikel;
                                 ENU=Jobs per Item];
                      ToolTipML=[DEU=F�hrt den Bericht "Projekte pro Artikel" aus.;
                                 ENU=Run the Jobs per item report.];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1014;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 26      ;1   ;Action    ;
                      Name=Report Job Quote;
                      CaptionML=[DEU=Vorschau Projektangebot;
                                 ENU=Preview Job Quote];
                      ToolTipML=[DEU=�ffnet den Bericht "Projektangebot".;
                                 ENU=Open the Job Quote report.];
                      ApplicationArea=#Jobs;
                      Image=Report;
                      PromotedCategory=Report;
                      OnAction=VAR
                                 Job@1000 : Record 167;
                               BEGIN
                                 Job.SETCURRENTKEY("No.");
                                 Job.SETFILTER("No.","No.");
                                 REPORT.RUN(REPORT::"Job Quote",TRUE,FALSE,Job);
                               END;
                                }
      { 28      ;1   ;Action    ;
                      Name=Send Job Quote;
                      CaptionML=[DEU=Projektangebot senden;
                                 ENU=Send Job Quote];
                      ToolTipML=[DEU=Sendet das Projektangebot an den Debitor. Im erscheinenden Fenster k�nnen Sie �ndern, wie der Beleg gesendet wird.;
                                 ENU=Send the job quote to the customer. You can change the way that the document is sent in the window that appears.];
                      ApplicationArea=#Jobs;
                      Image=SendTo;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Jobs-Send",Rec);
                               END;
                                }
      { 20      ;1   ;ActionGroup;
                      CaptionML=[DEU=Finanzmanagement;
                                 ENU=Financial Management];
                      Image=Report }
      { 1907574906;2 ;Action    ;
                      CaptionML=[DEU=WIP nach Sachposten Projekt;
                                 ENU=Job WIP to G/L];
                      ToolTipML=[DEU=Zeigt den WIP-Wert f�r die ausgew�hlten Projekte im Vergleich zu dem Betrag, der in die Finanzbuchhaltung gebucht wurde.;
                                 ENU=View the value of work in process on the jobs that you select compared to the amount that has been posted in the general ledger.];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1010;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 23      ;1   ;ActionGroup;
                      CaptionML=[DEU=Historie;
                                 ENU=History];
                      Image=Report }
      { 1905285006;2 ;Action    ;
                      CaptionML=[DEU=Projekt - Kontoblatt;
                                 ENU=Jobs - Transaction Detail];
                      ToolTipML=[DEU=Zeigt alle Buchungen mit Posten f�r ein ausgew�hltes Projekt in einer bestimmten Periode an, die f�r ein bestimmtes Projekt gebucht wurden. Am Ende jeder Projekt�bersicht werden die Betr�ge nach Verkaufs- und Verbrauchsposten getrennt addiert.;
                                 ENU=View all postings with entries for a selected job for a selected period, which have been charged to a certain job. At the end of each job list, the amounts are totaled separately for the Sales and Usage entry types.];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1007;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1901294206;2 ;Action    ;
                      CaptionML=[DEU=Projektjournal;
                                 ENU=Job Register];
                      ToolTipML=[DEU=Zeigt ein oder mehrere ausgew�hlte Projektjournale an. Mithilfe eines Filters k�nnen Sie genau die Journalposten ausw�hlen, die angezeigt werden sollen. Wenn Sie keinen Filter festlegen, kann der Bericht unpraktisch sein, da eine zu gro�e Datenmenge enthalten ist. In der Projekt-Buchungsblattvorlage k�nnen Sie angeben, ob der Bericht beim Buchen gedruckt werden soll.;
                                 ENU=View one or more selected job registers. By using a filter, you can select only those register entries that you want to see. If you do not set a filter, the report can be impractical because it can contain a large amount of information. On the job journal template, you can indicate that you want the report to print when you post.];
                      ApplicationArea=#Jobs;
                      RunObject=Report 1015;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer f�r das Projekt an. Sie k�nnen eine der folgenden Methoden zum Eingeben der Nummer verwenden:;
                           ENU=Specifies the number for the job. You can use one of the following methods to fill in the number:];
                ApplicationArea=#Jobs;
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine kurze Beschreibung des Projekts an.;
                           ENU=Specifies a short description of the job.];
                ApplicationArea=#Jobs;
                SourceExpr=Description }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Debitors an, dem das Projekt in Rechnung gestellt werden soll.;
                           ENU=Specifies the number of the customer that the job should be billed to.];
                ApplicationArea=#Jobs;
                SourceExpr="Bill-to Customer No." }

    { 35  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Status f�r das aktuelle Projekt an. Der Status des Projekts kann w�hrend der Verarbeitung ge�ndert werden. Abschlussberechnungen k�nnen f�r abgeschlossene Projekte durchgef�hrt werden.;
                           ENU=Specifies a status for the current job. You can change the status for the job as it progresses. Final calculations can be made on completed jobs.];
                ApplicationArea=#Jobs;
                SourceExpr=Status }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Projektverantwortlichen an. Sie k�nnen einen Namen aus der im Fenster "Ressourcen�bersicht" verf�gbaren Liste der Ressourcen ausw�hlen. Der Name wird aus dem Feld "Nr." in der Tabelle "Ressource" �bernommen. Sie k�nnen das Feld ausw�hlen, um eine Ressourcen�bersicht anzuzeigen.;
                           ENU=Specifies the name of the person responsible for the job. You can select a name from the list of resources available in the Resource List window. The name is copied from the No. field in the Resource table. You can choose the field to see a list of resources.];
                ApplicationArea=#Jobs;
                SourceExpr="Person Responsible";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das n�chste Rechnungsdatum f�r das Projekt an.;
                           ENU=Specifies the next invoice date for the job.];
                ApplicationArea=#Jobs;
                SourceExpr="Next Invoice Date";
                Visible=FALSE }

    { 19  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Projektbuchungsgruppencode f�r ein Projekt an. W�hlen Sie das Feld aus, um die verf�gbaren Codes anzuzeigen.;
                           ENU=Specifies a job posting group code for a job. To see the available codes, choose the field.];
                ApplicationArea=#Jobs;
                SourceExpr="Job Posting Group";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den zus�tzlichen Namen f�r das Projekt an. Das Feld wird f�r Suchvorg�nge verwendet.;
                           ENU=Specifies the additional name for the job. The field is used for searching purposes.];
                ApplicationArea=#Jobs;
                SourceExpr="Search Description" }

    { 18  ;2   ;Field     ;
                Name=% of Overdue Planning Lines;
                CaptionML=[DEU=% der �berf�lligen Planzeilen;
                           ENU=% of Overdue Planning Lines];
                ToolTipML=[DEU=Gibt den Prozentsatz der Planungszeilen an, die f�r dieses Projekt �berf�llig sind.;
                           ENU=Specifies the percent of planning lines that are overdue for this job.];
                ApplicationArea=#Jobs;
                SourceExpr=PercentOverdue;
                Visible=FALSE;
                Editable=FALSE }

    { 17  ;2   ;Field     ;
                Name=% Completed;
                CaptionML=[DEU=% abgeschlossen;
                           ENU=% Completed];
                ToolTipML=[DEU=Gibt den Fertigstellungsprozentsatz f�r dieses Projekt an.;
                           ENU=Specifies the completion percentage for this job.];
                ApplicationArea=#Jobs;
                SourceExpr=PercentCompleted;
                Visible=FALSE;
                Editable=FALSE }

    { 15  ;2   ;Field     ;
                Name=% Invoiced;
                CaptionML=[DEU=% fakturiert;
                           ENU=% Invoiced];
                ToolTipML=[DEU=Gibt den Prozentsatz der Fakturierung f�r dieses Projekt an.;
                           ENU=Specifies the invoiced percentage for this job.];
                ApplicationArea=#Jobs;
                SourceExpr=PercentInvoiced;
                Visible=FALSE;
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1907234507;1;Part   ;
                ApplicationArea=#Jobs;
                SubPageLink=No.=FIELD(Bill-to Customer No.);
                PagePartID=Page9081;
                Visible=FALSE;
                PartType=Page }

    { 1902018507;1;Part   ;
                ApplicationArea=#Jobs;
                SubPageLink=No.=FIELD(Bill-to Customer No.);
                PagePartID=Page9082;
                Visible=FALSE;
                PartType=Page }

    { 1905650007;1;Part   ;
                ApplicationArea=#Jobs;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page9099;
                Visible=TRUE;
                PartType=Page }

    { 25  ;1   ;Part      ;
                CaptionML=[DEU=Projektdetails;
                           ENU=Job Details];
                ApplicationArea=#Jobs;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page1030;
                Visible=JobSimplificationAvailable;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      JobSimplificationAvailable@1000 : Boolean;

    BEGIN
    END.
  }
}

