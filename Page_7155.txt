OBJECT Page 7155 Sales Analysis View Card
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Verkaufsanalyseansichtskarte;
               ENU=Sales Analysis View Card];
    SourceTable=Table7152;
    SourceTableView=WHERE(Analysis Area=CONST(Sales));
    PageType=Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 6       ;1   ;ActionGroup;
                      CaptionML=[DEU=A&nalyseansicht;
                                 ENU=&Analysis];
                      Image=AnalysisView }
      { 38      ;2   ;Action    ;
                      CaptionML=[DEU=Filter;
                                 ENU=Filter];
                      RunObject=Page 7152;
                      RunPageLink=Analysis Area=FIELD(Analysis Area),
                                  Analysis View Code=FIELD(Code);
                      Image=Filter }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 7       ;1   ;Action    ;
                      CaptionML=[DEU=A&ktualisieren;
                                 ENU=&Update];
                      RunObject=Codeunit 7150;
                      Promoted=Yes;
                      Image=Refresh;
                      PromotedCategory=Process }
      { 5       ;1   ;Action    ;
                      CaptionML=[DEU=Update bei Buchung aktivieren;
                                 ENU=Enable Update on Posting];
                      Image=Apply;
                      OnAction=BEGIN
                                 SetUpdateOnPosting(TRUE);
                               END;
                                }
      { 3       ;1   ;Action    ;
                      CaptionML=[DEU=Update bei Buchung deaktivieren;
                                 ENU=Disable Update on Posting];
                      Image=UnApply;
                      OnAction=BEGIN
                                 SetUpdateOnPosting(FALSE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code f�r die Analyseansicht an.;
                           ENU=Specifies a code for the analysis view.];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Analyseansicht an.;
                           ENU=Specifies the name of the analysis view.];
                SourceExpr=Name }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Filter an, der die in der Analyseansicht ber�cksichtigten Posten festlegt.;
                           ENU=Specifies a filter to specify the items that will be included in an analysis view.];
                SourceExpr="Item Filter";
                OnLookup=VAR
                           ItemList@1002 : Page 31;
                         BEGIN
                           ItemList.LOOKUPMODE(TRUE);
                           IF ItemList.RUNMODAL = ACTION::LookupOK THEN BEGIN
                             Text := ItemList.GetSelectionFilter;
                             EXIT(TRUE);
                           END;
                         END;
                          }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Lagerortfilter an, mit dem festgelegt wird, dass nur an einem bestimmten Lagerort gebuchte Posten in der Analyseansicht ber�cksichtigt werden sollen.;
                           ENU=Specifies a location filter to specify that only entries posted to a particular location are to be included in an analysis view.];
                SourceExpr="Location Filter";
                OnLookup=VAR
                           LocList@1000 : Page 15;
                         BEGIN
                           LocList.LOOKUPMODE(TRUE);
                           IF LocList.RUNMODAL = ACTION::LookupOK THEN BEGIN
                             Text := LocList.GetSelectionFilter;
                             EXIT(TRUE);
                           END;
                         END;
                          }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Periode an, f�r die Posten kombiniert werden, um einen einzigen Posten f�r diesen Zeitraum zu erhalten.;
                           ENU=Specifies the period that the program will combine entries for, in order to create a single entry for that time period.];
                SourceExpr="Date Compression" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, ab dem Artikelposten in einer Analyseansicht ber�cksichtigt werden.;
                           ENU=Specifies the date from which item ledger entries will be included in an analysis view.];
                SourceExpr="Starting Date" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Analyseansicht zuletzt aktualisiert wurde.;
                           ENU=Specifies the date on which the analysis view was last updated.];
                SourceExpr="Last Date Updated" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des letzten Artikelpostens an, der vor der Aktualisierung der Analyseansicht gebucht wurde.;
                           ENU=Specifies the number of the last item ledger entry you posted, prior to updating the analysis view.];
                SourceExpr="Last Entry No." }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des letzten Artikelbudgetpostens an, der vor der Aktualisierung der Analyseansicht eingegeben wurde.;
                           ENU=Specifies the number of the last item budget entry you entered prior to updating the analysis view.];
                SourceExpr="Last Budget Entry No." }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Analyseansicht bei jeder Buchung eines Artikelpostens, beispielsweise aus einer Verkaufsrechnung, aktualisiert wird.;
                           ENU=Specifies if the analysis view is updated every time that you post an item ledger entry, for example from a sales invoice.];
                SourceExpr="Update on Posting" }

    { 39  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob beim Aktualisieren einer Analyseansicht eine Aktualisierung der Analyseansichtsbudgetposten eingeschlossen werden soll.;
                           ENU=Specifies whether to include an update of analysis view budget entries, when updating an analysis view.];
                SourceExpr="Include Budgets" }

    { 41  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Analyseansicht gesperrt ist, sodass sie nicht aktualisiert werden kann.;
                           ENU=Specifies if the analysis view is blocked so that it cannot be updated.];
                SourceExpr=Blocked }

    { 1900309501;1;Group  ;
                CaptionML=[DEU=Dimensionen;
                           ENU=Dimensions] }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine der drei Dimensionen an, die Sie in eine Analyseansicht aufnehmen k�nnen.;
                           ENU=Specifies one of the three dimensions that you can include in an analysis view.];
                SourceExpr="Dimension 1 Code" }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine der drei Dimensionen an, die Sie in eine Analyseansicht aufnehmen k�nnen.;
                           ENU=Specifies one of the three dimensions that you can include in an analysis view.];
                SourceExpr="Dimension 2 Code" }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine der drei Dimensionen an, die Sie in eine Analyseansicht aufnehmen k�nnen.;
                           ENU=Specifies one of the three dimensions that you can include in an analysis view.];
                SourceExpr="Dimension 3 Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

