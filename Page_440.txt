OBJECT Page 440 Issued Reminder List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Reg. Mahnungen �bersicht;
               ENU=Issued Reminder List];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table297;
    DataCaptionFields=Customer No.;
    PageType=List;
    CardPageID=Issued Reminder;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 8       ;1   ;ActionGroup;
                      CaptionML=[DEU=&Mahnung;
                                 ENU=&Reminder];
                      Image=Reminder }
      { 11      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 442;
                      RunPageLink=Type=CONST(Issued Reminder),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 10      ;2   ;Action    ;
                      CaptionML=[DEU=D&ebitor;
                                 ENU=C&ustomer];
                      RunObject=Page 22;
                      RunPageLink=No.=FIELD(Customer No.);
                      Image=Customer }
      { 27      ;2   ;Separator  }
      { 28      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[DEU=Statistik;
                                 ENU=Statistics];
                      RunObject=Page 441;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 16      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=D&rucken;
                                 ENU=&Print];
                      ToolTipML=[DEU=Bereiten Sie das Drucken des Belegs vor. Das Berichtanforderungsfenster f�r den Beleg wird ge�ffnet, in dem Sie angeben k�nnen, was auf dem Ausdruck enthalten sein soll.;
                                 ENU=Prepare to print the document. The report request window for the document opens where you can specify what to include on the print-out.];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 IssuedReminderHeader@1001 : Record 297;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(IssuedReminderHeader);
                                 IssuedReminderHeader.PrintRecords(TRUE,FALSE,FALSE);
                               END;
                                }
      { 5       ;1   ;Action    ;
                      CaptionML=[DEU=Per &E-Mail senden;
                                 ENU=Send by &Email];
                      ToolTipML=[DEU=Bereiten Sie sich vor, den Beleg per E-Mail zu senden. Das Fenster "E-Mail senden" wird mit bereits eingetragener E-Mail-Adresse des Debitors ge�ffnet. Sie k�nnen Informationen hinzuf�gen oder vorhandene Informationen �ndern, bevor Sie die E-Mail senden.;
                                 ENU=Prepare to send the document by email. The Send Email window opens prefilled for the customer where you can add or change information before you send the email.];
                      Promoted=Yes;
                      Image=Email;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 IssuedReminderHeader@1000 : Record 297;
                                 IssuedReminderHeader2@1001 : Record 297;
                                 PrevCustomerNo@1002 : Code[20];
                               BEGIN
                                 IssuedReminderHeader := Rec;
                                 CurrPage.SETSELECTIONFILTER(IssuedReminderHeader);
                                 CurrPage.SETSELECTIONFILTER(IssuedReminderHeader2);
                                 IssuedReminderHeader.SETCURRENTKEY("Customer No.");
                                 IF IssuedReminderHeader.FINDSET THEN
                                   REPEAT
                                     IF IssuedReminderHeader."Customer No." <> PrevCustomerNo THEN BEGIN
                                       IssuedReminderHeader2.SETRANGE("Customer No.",IssuedReminderHeader."Customer No.");
                                       IssuedReminderHeader2.PrintRecords(FALSE,TRUE,FALSE);
                                     END;
                                     PrevCustomerNo := IssuedReminderHeader."Customer No.";
                                   UNTIL IssuedReminderHeader.NEXT = 0;
                               END;
                                }
      { 55      ;1   ;Action    ;
                      CaptionML=[DEU=&Navigate;
                                 ENU=&Navigate];
                      ToolTipML=[DEU=Sucht alle Posten und Belege, die f�r die Belegnummer und das Buchungsdatum auf dem ausgew�hlten Posten oder Beleg vorhanden sind.;
                                 ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1904202406;1 ;Action    ;
                      CaptionML=[DEU=Mahnungsnummern;
                                 ENU=Reminder Nos.];
                      RunObject=Report 126;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1902299006;1 ;Action    ;
                      CaptionML=[DEU=Debitorenposten per;
                                 ENU=Customer - Balance to Date];
                      RunObject=Report 121;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1906871306;1 ;Action    ;
                      CaptionML=[DEU=Debitor - Kontoblatt;
                                 ENU=Customer - Detail Trial Bal.];
                      RunObject=Report 104;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die registrierte Mahnungsnummer an.;
                           ENU=Specifies the issued reminder number.];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Debitors an, f�r den die Mahnung bestimmt ist.;
                           ENU=Specifies the customer number the reminder is for.];
                SourceExpr="Customer No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Debitors an, f�r den die Mahnung bestimmt ist.;
                           ENU=Specifies the name of the customer the reminder is for.];
                SourceExpr=Name }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den W�hrungscode der ausgestellten Mahnung an.;
                           ENU=Specifies the currency code of the issued reminder.];
                SourceExpr="Currency Code" }

    { 21  ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[DEU=Gibt die Summe der Restbetr�ge der Mahnungszeilen an.;
                           ENU=Specifies the total of the remaining amounts on the reminder lines.];
                SourceExpr="Remaining Amount" }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie h�ufig die Mahnung gedruckt wurde.;
                           ENU=Specifies how many times the reminder has been printed.];
                SourceExpr="No. Printed";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postleitzahl der Adresse an.;
                           ENU=Specifies the postal code of the address.];
                SourceExpr="Post Code";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ort des Debitors an, f�r den die Mahnung bestimmt ist.;
                           ENU=Specifies the city name of the customer the reminder is for.];
                SourceExpr=City;
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Mahnung verkn�pft ist.;
                           ENU=Specifies the dimension value code associated with the reminder.];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Mahnung verkn�pft ist.;
                           ENU=Specifies the dimension value code associated with the reminder.];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

