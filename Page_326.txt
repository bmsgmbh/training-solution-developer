OBJECT Page 326 Intrastat Jnl. Template List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Intra. Buch.-Bl.-Vorl.-�bers.;
               ENU=Intrastat Jnl. Template List];
    SourceTable=Table261;
    PageType=List;
    RefreshOnActivate=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Intrastat Buch.-Blattvorlage an.;
                           ENU=Specifies the name of the Intrastat journal template.];
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Intrastat Buch.-Blattvorlage an.;
                           ENU=Specifies a description of the Intrastat journal template.];
                SourceExpr=Description }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Fensters (bzw. Formulars) an, in dem die Intrastat Buch.-Blattzeilen angezeigt werden.;
                           ENU=Specifies the number of the window (form) in which the program displays the Intrastat journal lines.];
                SourceExpr="Page ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Testbericht an, der gedruckt werden kann, wenn Sie im Fenster "Intrastat Buch.-Blatt" auf "Aktionen" und dann auf "Drucken" klicken und anschlie�end "Testbericht" ausw�hlen.;
                           ENU=Specifies the checklist that can be printed if you click Actions, Print in the intrastate journal window and then select Checklist Report.];
                SourceExpr="Checklist Report ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

