OBJECT Table 5972 Contract/Service Discount
{
  OBJECT-PROPERTIES
  {
    Date=07.09.12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Contract Type,Contract No.;
    OnInsert=BEGIN
               TESTFIELD("Contract No.");
               IF "Contract Type" = "Contract Type"::Contract THEN BEGIN
                 ServMgtSetup.GET;
                 IF ServMgtSetup."Register Contract Changes" THEN
                   ContractChangeLog.LogContractChange(
                     "Contract No.",2,STRSUBSTNO('%1 %2 %3',Type,"No.",FIELDCAPTION("No.")),1,
                     '',FORMAT("No."),'',0);
               END;
             END;

    OnModify=BEGIN
               IF "Contract Type" = "Contract Type"::Contract THEN BEGIN
                 ServMgtSetup.GET;
                 IF "Discount %" <> xRec."Discount %" THEN
                   IF ServMgtSetup."Register Contract Changes" THEN
                     ContractChangeLog.LogContractChange(
                       "Contract No.",2,STRSUBSTNO('%1 %2 %3',Type,"No.",FIELDCAPTION("Discount %")),0,
                       FORMAT(xRec."Discount %"),FORMAT("Discount %"),'',0);
               END;
             END;

    OnDelete=BEGIN
               IF "Contract Type" = "Contract Type"::Contract THEN BEGIN
                 ServMgtSetup.GET;
                 IF ServMgtSetup."Register Contract Changes" THEN
                   ContractChangeLog.LogContractChange(
                     "Contract No.",2,STRSUBSTNO('%1 %2 %3',Type,"No.",FIELDCAPTION("No.")),2,
                     FORMAT("No."),'','',0);
               END;
             END;

    OnRename=BEGIN
               ERROR(Text000);
             END;

    CaptionML=[DEU=Vertrags-/Servicerabatt;
               ENU=Contract/Service Discount];
    LookupPageID=Page6058;
  }
  FIELDS
  {
    { 1   ;   ;Contract Type       ;Option        ;CaptionML=[DEU=Vertragsart;
                                                              ENU=Contract Type];
                                                   OptionCaptionML=[DEU=Angebot,Vertrag,Vorlage;
                                                                    ENU=Quote,Contract,Template];
                                                   OptionString=Quote,Contract,Template }
    { 2   ;   ;Contract No.        ;Code20        ;TableRelation=IF (Contract Type=CONST(Template)) "Service Contract Template".No.
                                                                 ELSE IF (Contract Type=CONST(Contract)) "Service Contract Header"."Contract No." WHERE (Contract Type=CONST(Contract))
                                                                 ELSE IF (Contract Type=CONST(Quote)) "Service Contract Header"."Contract No." WHERE (Contract Type=CONST(Quote));
                                                   CaptionML=[DEU=Vertragsnr.;
                                                              ENU=Contract No.] }
    { 4   ;   ;Type                ;Option        ;CaptionML=[DEU=Art;
                                                              ENU=Type];
                                                   OptionCaptionML=[DEU=Serviceartikelgruppe,Ressourcengruppe,Kosten;
                                                                    ENU=Service Item Group,Resource Group,Cost];
                                                   OptionString=Service Item Group,Resource Group,Cost }
    { 5   ;   ;No.                 ;Code20        ;TableRelation=IF (Type=CONST(Service Item Group)) "Service Item Group".Code
                                                                 ELSE IF (Type=CONST(Resource Group)) "Resource Group".No.
                                                                 ELSE IF (Type=CONST(Cost)) "Service Cost".Code;
                                                   TestTableRelation=Yes;
                                                   CaptionML=[DEU=Nr.;
                                                              ENU=No.] }
    { 6   ;   ;Starting Date       ;Date          ;CaptionML=[DEU=Startdatum;
                                                              ENU=Starting Date] }
    { 7   ;   ;Discount %          ;Decimal       ;CaptionML=[DEU=Rabatt %;
                                                              ENU=Discount %];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
  }
  KEYS
  {
    {    ;Contract Type,Contract No.,Type,No.,Starting Date;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'DEU=Sie k�nnen den Datensatz nicht umbenennen.;ENU=You cannot rename the record.';
      ContractChangeLog@1001 : Record 5967;
      ServMgtSetup@1002 : Record 5911;

    BEGIN
    END.
  }
}

