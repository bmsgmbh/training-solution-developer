OBJECT Table 2000000165 Tenant Permission Set
{
  OBJECT-PROPERTIES
  {
    Date=01.07.17;
    Time=12:00:00;
    Version List=;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[DEU=Tenant-Berechtigungssatz;
               ENU=Tenant Permission Set];
  }
  FIELDS
  {
    { 1   ;   ;App ID              ;GUID          ;CaptionML=[DEU=App-ID;
                                                              ENU=App ID] }
    { 2   ;   ;Role ID             ;Code20        ;CaptionML=[DEU=Rollen-ID;
                                                              ENU=Role ID] }
    { 3   ;   ;Name                ;Text30        ;CaptionML=[DEU=Name;
                                                              ENU=Name] }
  }
  KEYS
  {
    {    ;App ID,Role ID                          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

