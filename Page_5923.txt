OBJECT Page 5923 Loaner List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Leihger�te�bersicht;
               ENU=Loaner List];
    SourceTable=Table5913;
    PageType=List;
    CardPageID=Loaner Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 20      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Leihger�t;
                                 ENU=L&oaner];
                      Image=Loaners }
      { 23      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 5911;
                      RunPageLink=Table Name=CONST(Loaner),
                                  Table Subtype=CONST(0),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 24      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=&Leihger�teposten;
                                 ENU=Loaner E&ntries];
                      RunObject=Page 5924;
                      RunPageView=SORTING(Loaner No.)
                                  ORDER(Ascending);
                      RunPageLink=Loaner No.=FIELD(No.);
                      Image=Entries }
      { 1900000005;0 ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 1906302005;1 ;Action    ;
                      CaptionML=[DEU=Neuer Serviceauftrag;
                                 ENU=New Service Order];
                      RunObject=Page 5900;
                      Promoted=Yes;
                      Image=Document;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 21      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 25      ;2   ;Action    ;
                      CaptionML=[DEU=R�cknahme;
                                 ENU=Receive];
                      Image=ReceiveLoaner;
                      OnAction=VAR
                                 ServLoanerMgt@1003 : Codeunit 5901;
                               BEGIN
                                 ServLoanerMgt.Receive(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Leihger�ts an.;
                           ENU=Specifies the number of the loaner.];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Leihger�ts an.;
                           ENU=Specifies a description of the loaner.];
                SourceExpr=Description }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine zus�tzliche Beschreibung des Leihger�ts an.;
                           ENU=Specifies an additional description of the loaner.];
                SourceExpr="Description 2" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass das Leihger�t an einen Debitor verliehen wurde.;
                           ENU=Specifies that the loaner has been lent to a customer.];
                SourceExpr=Lent }

    { 15  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt den Belegtyp des Leihger�tepostens an.;
                           ENU=Specifies the document type of the loaner entry.];
                SourceExpr="Document Type" }

    { 13  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Servicebelegs f�r den verliehenen Serviceartikel an.;
                           ENU=Specifies the number of the service document for the service item that was lent.];
                SourceExpr="Document No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

