OBJECT Page 7358 Whse. Internal Pick Line
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Zeilen;
               ENU=Lines];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table7334;
    DelayedInsert=Yes;
    PageType=ListPart;
    OnNewRecord=BEGIN
                  SetUpNewLine(xRec);
                END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 1901313504;2 ;Action    ;
                      CaptionML=[DEU=Lagerplatzinhalts�bersicht;
                                 ENU=Bin Contents List];
                      Image=BinContent;
                      OnAction=BEGIN
                                 ShowBinContents;
                               END;
                                }
      { 1902759804;2 ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[DEU=Artikel&verfolgungszeilen;
                                 ENU=Item &Tracking Lines];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an, der kommissioniert werden soll.;
                           ENU=Specifies the number of the item that should be picked.];
                SourceExpr="Item No.";
                OnValidate=BEGIN
                             ItemNoOnAfterValidate;
                           END;
                            }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Variante des Artikels in der Zeile an.;
                           ENU=Specifies the variant number of the item in the line.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des Artikels in der Zeile an.;
                           ENU=Specifies the description of the item in the line.];
                SourceExpr=Description }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerortcode der internen Kommissionierzeile an.;
                           ENU=Specifies the code of the location of the internal pick line.];
                SourceExpr="Location Code";
                Visible=FALSE;
                Editable=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den "Nach Zonencode" der Zone an, in der Artikel nach der Kommissionierung abgelegt werden sollen.;
                           ENU=Specifies the To Zone Code of the zone where items should be placed once they are picked.];
                SourceExpr="To Zone Code";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerplatzes an, an dem die Artikel nach der Kommissionierung abgelegt werden sollen.;
                           ENU=Specifies the code of the bin into which the items should be placed when they are picked.];
                SourceExpr="To Bin Code";
                Visible=FALSE;
                OnValidate=BEGIN
                             ToBinCodeOnAfterValidate;
                           END;
                            }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Regalnummer des Artikels zu Informationszwecken an.;
                           ENU=Specifies the shelf number of the item for informational use.];
                SourceExpr="Shelf No.";
                Visible=FALSE;
                OnValidate=BEGIN
                             ShelfNoOnAfterValidate;
                           END;
                            }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge an, die kommissioniert werden soll.;
                           ENU=Specifies the quantity that should be picked.];
                SourceExpr=Quantity }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge in der Basiseinheit an, die kommissioniert werden soll.;
                           ENU=Specifies the quantity that should be picked, in the base unit of measure.];
                SourceExpr="Qty. (Base)";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge an, die noch bewegt werden muss.;
                           ENU=Specifies the quantity that still needs to be handled.];
                SourceExpr="Qty. Outstanding";
                Visible=TRUE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge in der Basiseinheit an, die noch bewegt werden muss.;
                           ENU=Specifies the quantity that still needs to be handled, in the base unit of measure.];
                SourceExpr="Qty. Outstanding (Base)";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels aus Kommissionieranweisungen an, die f�r die Kommissionierung dieser Zeile zugewiesen wurde.;
                           ENU=Specifies the quantity of the item in pick instructions that are assigned to be picked for the line.];
                SourceExpr="Pick Qty.";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels in den Kommissionieranweisungen in der Basiseinheit an, die f�r die Kommissionierung f�r die Zeile zugewiesen wurde.;
                           ENU=Specifies the quantity of the item in pick instructions assigned to be picked for the line, in the base unit of measure.];
                SourceExpr="Pick Qty. (Base)";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge der Zeile an, die als kommissioniert registriert wurde.;
                           ENU=Specifies the quantity of the line that is registered as picked.];
                SourceExpr="Qty. Picked";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge der Zeile in der Basiseinheit an, die als kommissioniert registriert wurde.;
                           ENU=Specifies the quantity of the line that is registered as picked, in the base unit of measure.];
                SourceExpr="Qty. Picked (Base)";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das F�lligkeitsdatum der Zeile an.;
                           ENU=Specifies the due date of the line.];
                SourceExpr="Due Date";
                OnValidate=BEGIN
                             DueDateOnAfterValidate;
                           END;
                            }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ma�einheitencode der Zeile an.;
                           ENU=Specifies the unit of measure code of the line.];
                SourceExpr="Unit of Measure Code" }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Basiseinheiten an, die der Einheit des Artikels in dieser Zeile entsprechen.;
                           ENU=Specifies the number of base units of measure, that are in the unit of measure, specified for the item on the line.];
                SourceExpr="Qty. per Unit of Measure" }

  }
  CODE
  {
    VAR
      SortMethod@1000 : ' ,Item,Bin Code,Due Date';

    LOCAL PROCEDURE ShowBinContents@7300();
    VAR
      BinContent@1000 : Record 7302;
    BEGIN
      BinContent.ShowBinContents("Location Code","Item No.","Variant Code",'');
    END;

    PROCEDURE PickCreate@4();
    VAR
      WhseInternalPickHeader@1001 : Record 7333;
      WhseInternalPickLine@1000 : Record 7334;
      ReleaseWhseInternalPick@1002 : Codeunit 7315;
    BEGIN
      WhseInternalPickLine.COPY(Rec);
      WhseInternalPickHeader.GET(WhseInternalPickLine."No.");
      IF WhseInternalPickHeader.Status = WhseInternalPickHeader.Status::Open THEN
        ReleaseWhseInternalPick.Release(WhseInternalPickHeader);
      CreatePickDoc(WhseInternalPickLine,WhseInternalPickHeader);
    END;

    LOCAL PROCEDURE OpenItemTrackingLines@2();
    BEGIN
      OpenItemTrackingLines;
    END;

    LOCAL PROCEDURE GetActualSortMethod@1() : Decimal;
    VAR
      WhseInternalPickHeader@1000 : Record 7333;
    BEGIN
      IF WhseInternalPickHeader.GET("No.") THEN
        EXIT(WhseInternalPickHeader."Sorting Method");
      EXIT(0);
    END;

    LOCAL PROCEDURE ItemNoOnAfterValidate@19061248();
    BEGIN
      IF GetActualSortMethod = SortMethod::Item THEN
        CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE ToBinCodeOnAfterValidate@19037373();
    BEGIN
      IF GetActualSortMethod = SortMethod::"Bin Code" THEN
        CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE ShelfNoOnAfterValidate@19051954();
    BEGIN
      IF GetActualSortMethod = SortMethod::"Bin Code" THEN
        CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE DueDateOnAfterValidate@19011747();
    BEGIN
      IF GetActualSortMethod = SortMethod::"Due Date" THEN
        CurrPage.UPDATE;
    END;

    BEGIN
    END.
  }
}

