OBJECT Page 7707 Item Identifiers List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Artikelbarcodes�bersicht;
               ENU=Item Identifiers List];
    SourceTable=Table7704;
    DataCaptionFields=Code,Item No.;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen eindeutigen Code f�r einen bestimmten Artikel an, der f�r die automatische Datenerfassung hilfreich ist.;
                           ENU=Specifies a unique code for a particular item in terms that are useful for automatic data capture.];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an, der mit dem Barcode in der Zeile bestimmt werden soll.;
                           ENU=Specifies the number of the item to be identified by the identifier code on the line.];
                SourceExpr="Item No.";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Variantencode des Artikels an, der mit dem Barcode in der Zeile bestimmt werden soll.;
                           ENU=Specifies the variant code of the item to be identified by the identifier code on the line.];
                SourceExpr="Variant Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einheitencode des Artikels an, der mit dem Artikelbarcode in der Zeile bestimmt werden soll.;
                           ENU=Specifies the unit of measure code of the item to be identified by the identifier code on the line.];
                SourceExpr="Unit of Measure Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

