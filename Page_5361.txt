OBJECT Page 5361 Integration Field Mapping List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Integrationsfeld-Zuordnungsliste;
               ENU=Integration Field Mapping List];
    SourceTable=Table5336;
    DataCaptionExpr="Integration Table Mapping Name";
    PageType=List;
    OnAfterGetRecord=BEGIN
                       GetFieldCaptions;
                     END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Felds in Dynamics NAV an.;
                           ENU=Specifies the number of the field in Dynamics NAV.];
                BlankZero=Yes;
                SourceExpr="Field No." }

    { 3   ;2   ;Field     ;
                Name=FieldName;
                CaptionML=[DEU=Feldname;
                           ENU=Field Name];
                ToolTipML=[DEU=Gibt den Namen des Felds in Dynamics NAV an.;
                           ENU=Specifies the name of the field in Dynamics NAV.];
                SourceExpr=NAVFieldName }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Felds in Dynamics CRM an.;
                           ENU=Specifies the number of the field in Dynamics CRM.];
                BlankZero=Yes;
                SourceExpr="Integration Table Field No." }

    { 11  ;2   ;Field     ;
                Name=IntegrationFieldName;
                CaptionML=[DEU=Integrationsfeldname;
                           ENU=Integration Field Name];
                ToolTipML=[DEU=Gibt den Namen des Felds in Dynamics CRM an.;
                           ENU=Specifies the name of the field in Dynamics CRM.];
                SourceExpr=CRMFieldName }

    { 7   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Richtung der Synchronisierung an.;
                           ENU=Specifies the direction of the synchronization.];
                SourceExpr=Direction }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den konstanten Wert an, auf den das zugeordnete Feld festgelegt wird.;
                           ENU=Specifies the constant value that the mapped field will be set to.];
                SourceExpr="Constant Value" }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU="Gibt an, ob das Feld bei der Zuweisung in Dynamics NAV �berpr�ft werden soll. ";
                           ENU="Specifies if the field should be validated during assignment in Dynamics NAV. "];
                SourceExpr="Validate Field" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob das Integrationsfeld bei der Zuweisung in Dynamics CRM �berpr�ft werden soll.;
                           ENU=Specifies if the integration field should be validated during assignment in Dynamics CRM.];
                SourceExpr="Validate Integration Table Fld" }

  }
  CODE
  {
    VAR
      NAVFieldName@1000 : Text;
      CRMFieldName@1001 : Text;

    LOCAL PROCEDURE GetFieldCaptions@3();
    VAR
      IntegrationTableMapping@1003 : Record 5335;
    BEGIN
      IntegrationTableMapping.GET("Integration Table Mapping Name");
      NAVFieldName := GetFieldCaption(IntegrationTableMapping."Table ID","Field No.");
      CRMFieldName := GetFieldCaption(IntegrationTableMapping."Integration Table ID","Integration Table Field No.");
    END;

    LOCAL PROCEDURE GetFieldCaption@1(TableID@1000 : Integer;FieldID@1001 : Integer) : Text;
    VAR
      Field@1002 : Record 2000000041;
    BEGIN
      IF (TableID <> 0) AND (FieldID <> 0) THEN
        IF Field.GET(TableID,FieldID) THEN
          EXIT(Field."Field Caption");
      EXIT('');
    END;

    BEGIN
    END.
  }
}

