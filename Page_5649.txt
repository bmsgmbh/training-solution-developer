OBJECT Page 5649 Total Value Insured
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Versicherte Summe;
               ENU=Total Value Insured];
    SourceTable=Table5600;
    PageType=Document;
    OnAfterGetCurrRecord=BEGIN
                           CurrPage.TotalValue.PAGE.CreateTotalValue("No.");
                           FASetup.GET;
                           FADeprBook.INIT;
                           IF FASetup."Insurance Depr. Book" <> '' THEN
                             IF FADeprBook.GET("No.",FASetup."Insurance Depr. Book") THEN
                               FADeprBook.CALCFIELDS("Acquisition Cost");
                         END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Nummer f�r die Anlage an.;
                           ENU=Specifies a number for the fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Anlage an.;
                           ENU=Specifies a description of the fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr=Description }

    { 8   ;2   ;Field     ;
                CaptionML=[DEU=AfA-Buch zur Versicherung;
                           ENU=Insurance Depr. Book];
                ToolTipML=[DEU=Gibt den AfA Buchcode an, der im Fenster "Anlageneinrichtung" festgelegt ist.;
                           ENU=Specifies the depreciation book code that is specified in the Fixed Asset Setup window.];
                ApplicationArea=#FixedAssets;
                SourceExpr=FASetup."Insurance Depr. Book" }

    { 10  ;2   ;Field     ;
                CaptionML=[DEU=Anschaffungskosten;
                           ENU=Acquisition Cost];
                ToolTipML=[DEU=Gibt den Gesamtprozentsatz der Anschaffungskosten an, der beim Buchen der Anschaffungskosten verteilt werden kann.;
                           ENU=Specifies the total percentage of acquisition cost that can be allocated when acquisition cost is posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr=FADeprBook."Acquisition Cost" }

    { 7   ;1   ;Part      ;
                Name=TotalValue;
                ApplicationArea=#FixedAssets;
                PagePartID=Page5650 }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      FASetup@1000 : Record 5603;
      FADeprBook@1002 : Record 5612;

    BEGIN
    END.
  }
}

