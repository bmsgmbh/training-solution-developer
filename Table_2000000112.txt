OBJECT Table 2000000112 Server Instance
{
  OBJECT-PROPERTIES
  {
    Date=01.07.17;
    Time=12:00:00;
    Version List=;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[DEU=Serverinstanz;
               ENU=Server Instance];
  }
  FIELDS
  {
    { 1   ;   ;Server Instance ID  ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[DEU=Serverinstanz-ID;
                                                              ENU=Server Instance ID] }
    { 2   ;   ;Service Name        ;Text250       ;CaptionML=[DEU=Servicename;
                                                              ENU=Service Name] }
    { 3   ;   ;Server Computer Name;Text250       ;CaptionML=[DEU=Servercomputername;
                                                              ENU=Server Computer Name] }
    { 4   ;   ;Last Active         ;DateTime      ;Volatile=Yes;
                                                   CaptionML=[DEU=Zuletzt aktiv;
                                                              ENU=Last Active] }
    { 5   ;   ;Server Instance Name;Text250       ;CaptionML=[DEU=Serverinstanzname;
                                                              ENU=Server Instance Name] }
    { 6   ;   ;Server Port         ;Integer       ;CaptionML=[DEU=Serverport;
                                                              ENU=Server Port] }
    { 7   ;   ;Management Port     ;Integer       ;CaptionML=[DEU=Verwaltungsport;
                                                              ENU=Management Port] }
    { 8   ;   ;Status              ;Option        ;CaptionML=[DEU=Status;
                                                              ENU=Status];
                                                   OptionCaptionML=[DEU=Gestartet,Angehalten,Abgest�rzt;
                                                                    ENU=Started,Stopped,Crashed];
                                                   OptionString=Started,Stopped,Crashed }
  }
  KEYS
  {
    {    ;Server Instance ID                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

