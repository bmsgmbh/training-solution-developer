OBJECT Table 5066 Job Responsibility
{
  OBJECT-PROPERTIES
  {
    Date=07.09.12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Description;
    CaptionML=[DEU=Verantwortlichkeit;
               ENU=Job Responsibility];
    LookupPageID=Page5080;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[DEU=Code;
                                                              ENU=Code];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[DEU=Beschreibung;
                                                              ENU=Description] }
    { 3   ;   ;No. of Contacts     ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Contact Job Responsibility" WHERE (Job Responsibility Code=FIELD(Code)));
                                                   CaptionML=[DEU=Anzahl Kontakte;
                                                              ENU=No. of Contacts];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

