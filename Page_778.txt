OBJECT Page 778 Analysis Report Chart SubPage
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Analysebericht - Diagrammunterseite;
               ENU=Analysis Report Chart SubPage];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table771;
    PageType=ListPart;
    OnFindRecord=BEGIN
                   SetFilters(Rec);
                   EXIT(FINDSET);
                 END;

    ActionList=ACTIONS
    {
      { 8       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 10      ;1   ;Action    ;
                      Name=Edit;
                      CaptionML=[DEU=Bearbeiten;
                                 ENU=Edit];
                      ToolTipML=[DEU=Dient zum Bearbeiten des Diagramms.;
                                 ENU=Edit the chart.];
                      ApplicationArea=#Suite;
                      Image=EditLines;
                      OnAction=VAR
                                 AnalysisReportChartLine@1001 : Record 771;
                                 AnalysisReportChartLinePage@1002 : Page 777;
                                 AnalysisReportChartMatrix@1003 : Page 776;
                               BEGIN
                                 SetFilters(AnalysisReportChartLine);
                                 AnalysisReportChartLine.SETRANGE("Chart Type");
                                 CASE AnalysisReportChartSetup."Base X-Axis on" OF
                                   AnalysisReportChartSetup."Base X-Axis on"::Period:
                                     IF IsMeasure THEN BEGIN
                                       AnalysisReportChartMatrix.SetFilters(AnalysisReportChartSetup);
                                       AnalysisReportChartMatrix.RUNMODAL;
                                     END;
                                   AnalysisReportChartSetup."Base X-Axis on"::Line,
                                   AnalysisReportChartSetup."Base X-Axis on"::Column:
                                     BEGIN
                                       IF IsMeasure THEN
                                         AnalysisReportChartLinePage.SetViewAsMeasure(TRUE)
                                       ELSE
                                         AnalysisReportChartLinePage.SetViewAsMeasure(FALSE);
                                       AnalysisReportChartLinePage.SETTABLEVIEW(AnalysisReportChartLine);
                                       AnalysisReportChartLinePage.RUNMODAL;
                                     END;
                                 END;

                                 CurrPage.UPDATE;
                               END;
                                }
      { 9       ;1   ;Action    ;
                      Name=Delete;
                      CaptionML=[DEU=L�schen;
                                 ENU=Delete];
                      ToolTipML=[DEU=L�scht den Datensatz.;
                                 ENU=Delete the record.];
                      ApplicationArea=#Suite;
                      Image=Delete;
                      OnAction=VAR
                                 AnalysisReportChartLine@1001 : Record 771;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(AnalysisReportChartLine);
                                 AnalysisReportChartLine.MODIFYALL("Chart Type","Chart Type"::" ");
                                 CurrPage.UPDATE;
                               END;
                                }
      { 11      ;1   ;Action    ;
                      Name=Reset to default setup;
                      CaptionML=[DEU=Auf Standardeinrichtung zur�cksetzen;
                                 ENU=Reset to Default Setup];
                      ToolTipML=[DEU=Macht Ihre �nderung r�ckg�ngig und stellt die Standardeinrichtung wieder her.;
                                 ENU=Undo your change and return to the default setup.];
                      ApplicationArea=#Suite;
                      Image=Refresh;
                      OnAction=BEGIN
                                 AnalysisReportChartSetup.RefreshLines(FALSE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 7   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen an. Dieses Feld ist nur zur internen Verwendung vorgesehen.;
                           ENU=Specifies the name. This field is intended only for internal use.];
                SourceExpr="Analysis Line Template Name";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen an. Dieses Feld ist nur zur internen Verwendung vorgesehen.;
                           ENU=Specifies the name. This field is intended only for internal use.];
                SourceExpr="Analysis Column Template Name";
                Visible=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Analyseberichtsspalten oder -zeilen an, die Sie im Fenster "Analysebericht - Diagrammeinrichtung" zur Anzeige ausw�hlen.;
                           ENU=Specifies the analysis report columns or lines that you select to insert in the Analysis Report Chart Setup window.];
                SourceExpr="Original Measure Name";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Analyseberichtsspalten oder -zeilen an, auf denen die Kennzahlen auf der y-Achse im spezifischen Diagramm basieren.;
                           ENU=Specifies the analysis report columns or lines that the measures on the y-axis in the specific chart are based on.];
                ApplicationArea=#Suite;
                SourceExpr="Measure Name" }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie die Analyseberichtswerte im spezifischen Diagramm grafisch dargestellt werden.;
                           ENU=Specifies how the analysis report values are represented graphically in the specific chart.];
                ApplicationArea=#All;
                SourceExpr="Chart Type";
                Visible=IsMeasure;
                OnValidate=BEGIN
                             IF "Chart Type" = "Chart Type"::" " THEN
                               CurrPage.UPDATE;
                           END;
                            }

  }
  CODE
  {
    VAR
      AnalysisReportChartSetup@1001 : Record 770;
      IsMeasure@1000 : Boolean;

    PROCEDURE SetViewAsMeasure@1(Value@1000 : Boolean);
    BEGIN
      IsMeasure := Value;
    END;

    LOCAL PROCEDURE SetFilters@2(VAR AnalysisReportChartLine@1001 : Record 771);
    BEGIN
      WITH AnalysisReportChartLine DO BEGIN
        RESET;
        IF IsMeasure THEN
          AnalysisReportChartSetup.SetLinkToMeasureLines(AnalysisReportChartLine)
        ELSE
          AnalysisReportChartSetup.SetLinkToDimensionLines(AnalysisReportChartLine);
        SETFILTER("Chart Type",'<>%1',"Chart Type"::" ");
      END;
    END;

    PROCEDURE SetSetupRec@3(VAR NewAnalysisReportChartSetup@1000 : Record 770);
    BEGIN
      AnalysisReportChartSetup := NewAnalysisReportChartSetup;
    END;

    BEGIN
    END.
  }
}

