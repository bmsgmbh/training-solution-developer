OBJECT Page 1030 Job Cost Factbox
{
  OBJECT-PROPERTIES
  {
    Date=30.03.17;
    Time=12:00:00;
    Version List=NAVW110.00.00.16177;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Projektdetails;
               ENU=Job Details];
    LinksAllowed=No;
    SourceTable=Table167;
    PageType=CardPart;
    OnAfterGetCurrRecord=BEGIN
                           CLEAR(JobCalcStatistics);
                           JobCalcStatistics.JobCalculateCommonFilters(Rec);
                           JobCalcStatistics.CalculateAmounts;
                           JobCalcStatistics.GetLCYCostAmounts(CL);
                           JobCalcStatistics.GetLCYPriceAmounts(PL);
                         END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 11  ;1   ;Field     ;
                CaptionML=[DEU=Projektnr.;
                           ENU=Job No.];
                ToolTipML=[DEU=Gibt die Nummer des Projekts an.;
                           ENU=Specifies the job number.];
                ApplicationArea=#Jobs;
                SourceExpr="No.";
                OnDrillDown=BEGIN
                              ShowDetails;
                            END;
                             }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Budgetkosten;
                           ENU=Budget Cost];
                GroupType=Group }

    { 146 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt nichts an.;
                           ENU=Specifies nothing.];
                ApplicationArea=#Jobs;
                SourceExpr=PlaceHolderLbl;
                Visible=FALSE;
                Enabled=FALSE;
                Editable=FALSE }

    { 2   ;2   ;Field     ;
                Name=ScheduleCostLCY;
                CaptionML=[DEU=Ressource;
                           ENU=Resource];
                ToolTipML=[DEU=Gibt den budgetierten Einstandsbetrag der diesem Projekt zugeordneten Ressourcen an.;
                           ENU=Specifies the total budgeted cost of resources associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=CL[1];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowPlanningLine(1,1,TRUE);
                            END;
                             }

    { 3   ;2   ;Field     ;
                Name=ScheduleCostLCYItem;
                CaptionML=[DEU=Artikel;
                           ENU=Item];
                ToolTipML=[DEU=Gibt den budgetierten Einstandsbetrag der diesem Projekt zugeordneten Artikel an.;
                           ENU=Specifies the total budgeted cost of items associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=CL[2];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowPlanningLine(1,2,TRUE);
                            END;
                             }

    { 4   ;2   ;Field     ;
                Name=ScheduleCostLCYGLAcc;
                CaptionML=[DEU=Sachkonto;
                           ENU=G/L Account];
                ToolTipML=[DEU=Gibt den budgetierten Einstandsbetrag der diesem Projekt zugeordneten Fibu Buch.-Bl�tter an.;
                           ENU=Specifies the total budgeted cost of general journal entries associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=CL[3];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowPlanningLine(1,3,TRUE);
                            END;
                             }

    { 26  ;2   ;Field     ;
                Name=ScheduleCostLCYTotal;
                CaptionML=[DEU=Gesamt;
                           ENU=Total];
                ToolTipML=[DEU=Gibt die Summe der budgetierten Kosten f�r ein Projekt an.;
                           ENU=Specifies the total budget cost of a job.];
                ApplicationArea=#Jobs;
                SourceExpr=CL[4];
                Editable=FALSE;
                Style=Strong;
                StyleExpr=TRUE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowPlanningLine(1,0,TRUE);
                            END;
                             }

    { 10  ;1   ;Group     ;
                CaptionML=[DEU=Ist-Kosten;
                           ENU=Actual Cost];
                GroupType=Group }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt nichts an.;
                           ENU=Specifies nothing.];
                ApplicationArea=#Jobs;
                SourceExpr=PlaceHolderLbl;
                Visible=FALSE;
                Enabled=FALSE;
                Editable=FALSE }

    { 5   ;2   ;Field     ;
                Name=UsageCostLCY;
                CaptionML=[DEU=Ressource;
                           ENU=Resource];
                ToolTipML=[DEU=Gibt die Verbrauchskosten (Total) der diesem Projekt zugeordneten Ressourcen an.;
                           ENU=Specifies the total usage cost of resources associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=CL[5];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowLedgEntry(1,1,TRUE);
                            END;
                             }

    { 6   ;2   ;Field     ;
                Name=UsageCostLCYItem;
                CaptionML=[DEU=Artikel;
                           ENU=Item];
                ToolTipML=[DEU=Gibt die Verbrauchskosten (Total) der diesem Projekt zugeordneten Artikel an.;
                           ENU=Specifies the total usage cost of items associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=CL[6];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowLedgEntry(1,2,TRUE);
                            END;
                             }

    { 7   ;2   ;Field     ;
                Name=UsageCostLCYGLAcc;
                CaptionML=[DEU=Sachkonto;
                           ENU=G/L Account];
                ToolTipML=[DEU=Gibt die Verbrauchskosten (Total) der diesem Projekt zugeordneten Fibu Buch.-Bl�tter an.;
                           ENU=Specifies the total usage cost of general journal entries associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=CL[7];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowLedgEntry(1,3,TRUE);
                            END;
                             }

    { 8   ;2   ;Field     ;
                Name=UsageCostLCYTotal;
                CaptionML=[DEU=Gesamt;
                           ENU=Total];
                ToolTipML=[DEU=Gibt den f�r ein Projekt verwendeten Einstandsbetrag an.;
                           ENU=Specifies the total costs used for a job.];
                ApplicationArea=#Jobs;
                SourceExpr=CL[8];
                Editable=FALSE;
                Style=Strong;
                StyleExpr=TRUE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowLedgEntry(1,0,TRUE);
                            END;
                             }

    { 17  ;1   ;Group     ;
                CaptionML=[DEU=Fakturierbarer Preis;
                           ENU=Billable Price];
                GroupType=Group }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt nichts an.;
                           ENU=Specifies nothing.];
                ApplicationArea=#Jobs;
                SourceExpr=PlaceHolderLbl;
                Visible=FALSE;
                Enabled=FALSE;
                Editable=FALSE }

    { 15  ;2   ;Field     ;
                Name=BillablePriceLCY;
                CaptionML=[DEU=Ressource;
                           ENU=Resource];
                ToolTipML=[DEU=Gibt den fakturierbaren Verkaufsbetrag der diesem Projekt zugeordneten Ressourcen an.;
                           ENU=Specifies the total billable price of resources associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=PL[9];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowPlanningLine(1,1,FALSE);
                            END;
                             }

    { 14  ;2   ;Field     ;
                Name=BillablePriceLCYItem;
                CaptionML=[DEU=Artikel;
                           ENU=Item];
                ToolTipML=[DEU=Gibt den fakturierbaren Verkaufsbetrag der diesem Projekt zugeordneten Artikel an.;
                           ENU=Specifies the total billable price of items associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=PL[10];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowPlanningLine(1,2,FALSE);
                            END;
                             }

    { 13  ;2   ;Field     ;
                Name=BillablePriceLCYGLAcc;
                CaptionML=[DEU=Sachkonto;
                           ENU=G/L Account];
                ToolTipML=[DEU=Gibt den fakturierbaren Verkaufsbetrag f�r Projektplanungszeilen des Typs "Sachkonto" an.;
                           ENU=Specifies the total billable price for job planning lines of type G/L account.];
                ApplicationArea=#Jobs;
                SourceExpr=PL[11];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowPlanningLine(1,3,FALSE);
                            END;
                             }

    { 12  ;2   ;Field     ;
                Name=BillablePriceLCYTotal;
                CaptionML=[DEU=Summe;
                           ENU=Total];
                ToolTipML=[DEU=Gibt den fakturierbaren Verkaufsbetrag f�r ein Projekt an.;
                           ENU=Specifies the total billable price used for a job.];
                ApplicationArea=#Jobs;
                SourceExpr=PL[12];
                Editable=FALSE;
                Style=Strong;
                StyleExpr=TRUE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowPlanningLine(1,0,FALSE);
                            END;
                             }

    { 23  ;1   ;Group     ;
                CaptionML=[DEU=Fakturiert (VK-Preis);
                           ENU=Invoiced Price];
                GroupType=Group }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt nichts an.;
                           ENU=Specifies nothing.];
                ApplicationArea=#Jobs;
                SourceExpr=PlaceHolderLbl;
                Visible=FALSE;
                Enabled=FALSE;
                Editable=FALSE }

    { 21  ;2   ;Field     ;
                Name=InvoicedPriceLCY;
                CaptionML=[DEU=Ressource;
                           ENU=Resource];
                ToolTipML=[DEU=Gibt den fakturierten Verkaufsbetrag der diesem Projekt zugeordneten Ressourcen an.;
                           ENU=Specifies the total invoiced price of resources associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=PL[13];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowLedgEntry(1,1,FALSE);
                            END;
                             }

    { 20  ;2   ;Field     ;
                Name=InvoicedPriceLCYItem;
                CaptionML=[DEU=Artikel;
                           ENU=Item];
                ToolTipML=[DEU=Gibt den fakturierten Verkaufsbetrag der diesem Projekt zugeordneten Artikel an.;
                           ENU=Specifies the total invoiced price of items associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=PL[14];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowLedgEntry(1,2,FALSE);
                            END;
                             }

    { 19  ;2   ;Field     ;
                Name=InvoicedPriceLCYGLAcc;
                CaptionML=[DEU=Sachkonto;
                           ENU=G/L Account];
                ToolTipML=[DEU=Gibt den fakturierten Verkaufsbetrag der diesem Projekt zugeordneten Fibu Buch.-Bl�tter an.;
                           ENU=Specifies the total invoiced price of general journal entries associated with this job.];
                ApplicationArea=#Jobs;
                SourceExpr=PL[15];
                Editable=FALSE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowLedgEntry(1,3,FALSE);
                            END;
                             }

    { 18  ;2   ;Field     ;
                Name=InvoicedPriceLCYTotal;
                CaptionML=[DEU=Summe;
                           ENU=Total];
                ToolTipML=[DEU=Gibt den fakturierten Verkaufsbetrag f�r ein Projekt an.;
                           ENU=Specifies the total invoiced price of a job.];
                ApplicationArea=#Jobs;
                SourceExpr=PL[16];
                Editable=FALSE;
                Style=Strong;
                StyleExpr=TRUE;
                OnDrillDown=BEGIN
                              JobCalcStatistics.ShowLedgEntry(1,0,FALSE);
                            END;
                             }

  }
  CODE
  {
    VAR
      JobCalcStatistics@1001 : Codeunit 1008;
      PlaceHolderLbl@1002 : TextConst 'DEU=Platzhalter;ENU=Placeholder';
      CL@1005 : ARRAY [16] OF Decimal;
      PL@1000 : ARRAY [16] OF Decimal;

    LOCAL PROCEDURE ShowDetails@1();
    BEGIN
      PAGE.RUN(PAGE::"Job Card",Rec);
    END;

    BEGIN
    END.
  }
}

