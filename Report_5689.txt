OBJECT Report 5689 Create FA Depreciation Books
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Anlagen-AfA-Buch erstellen;
               ENU=Create FA Depreciation Books];
    ProcessingOnly=Yes;
    OnPreReport=BEGIN
                  DeprBook.GET(DeprBookCode);
                  Window.OPEN(
                    Text000 +
                    Text001);
                  IF FANo <> '' THEN
                    FADeprBook2.GET(FANo,DeprBookCode);
                END;

  }
  DATASET
  {
    { 3794;    ;DataItem;                    ;
               DataItemTable=Table5600;
               DataItemTableView=SORTING(No.);
               OnAfterGetRecord=BEGIN
                                  IF Inactive THEN
                                    CurrReport.SKIP;
                                  IF FADeprBook.GET("No.",DeprBookCode) THEN BEGIN
                                    Window.UPDATE(2,"No.");
                                    CurrReport.SKIP;
                                  END;
                                  Window.UPDATE(1,"No.");
                                  IF FANo <> '' THEN
                                    FADeprBook := FADeprBook2
                                  ELSE
                                    FADeprBook.INIT;
                                  FADeprBook."FA No." := "No.";
                                  FADeprBook."Depreciation Book Code" := DeprBookCode;
                                  FADeprBook.INSERT(TRUE);
                                END;

               ReqFilterFields=No.,FA Class Code,FA Subclass Code }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
      OnOpenPage=BEGIN
                   IF DeprBookCode = '' THEN BEGIN
                     FASetup.GET;
                     DeprBookCode := FASetup."Default Depr. Book";
                   END;
                 END;

    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[DEU=Optionen;
                             ENU=Options] }

      { 15  ;2   ;Field     ;
                  Name=DepreciationBook;
                  CaptionML=[DEU=AfA-Buch;
                             ENU=Depreciation Book];
                  ToolTipML=[DEU=Gibt den Code des AfA-Buchs an, das im Bericht oder in der Stapelverarbeitung ber�cksichtigt werden soll.;
                             ENU=Specifies the code for the depreciation book to be included in the report or batch job.];
                  ApplicationArea=#FixedAssets;
                  SourceExpr=DeprBookCode;
                  TableRelation="Depreciation Book";
                  OnValidate=BEGIN
                               CheckFADeprBook;
                             END;
                              }

      { 2   ;2   ;Field     ;
                  Name=CopyFromFANo;
                  CaptionML=[DEU=Kopiere von Anl.-Nr.;
                             ENU=Copy from FA No.];
                  ToolTipML=[DEU=Gibt die Nummer der Anlage an, aus der Sie kopieren m�chten.;
                             ENU=Specifies the number of the fixed asset that you want to copy from.];
                  ApplicationArea=#FixedAssets;
                  SourceExpr=FANo;
                  TableRelation="Fixed Asset";
                  OnValidate=BEGIN
                               CheckFADeprBook;
                             END;
                              }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'DEU=AfA-Buch erstellen            #1##########\;ENU=Creating fixed asset book     #1##########\';
      Text001@1001 : TextConst 'DEU=AfA-Buch nicht erstellen      #2##########;ENU=Not creating fixed asset book #2##########';
      FASetup@1002 : Record 5603;
      DeprBook@1003 : Record 5611;
      FADeprBook@1004 : Record 5612;
      FADeprBook2@1005 : Record 5612;
      Window@1006 : Dialog;
      DeprBookCode@1007 : Code[10];
      FANo@1008 : Code[20];

    LOCAL PROCEDURE CheckFADeprBook@1();
    VAR
      FADeprBook@1000 : Record 5612;
    BEGIN
      IF (DeprBookCode <> '') AND (FANo <> '') THEN
        FADeprBook.GET(FANo,DeprBookCode);
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

