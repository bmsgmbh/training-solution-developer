OBJECT Page 9091 Item Planning FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Artikeldetails - Planung;
               ENU=Item Details - Planning];
    SourceTable=Table27;
    PageType=CardPart;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 21  ;1   ;Field     ;
                CaptionML=[DEU=Artikelnr.;
                           ENU=Item No.];
                ToolTipML=[DEU=Gibt die Nummer des Artikels an.;
                           ENU=Specifies the number of the item.];
                SourceExpr="No.";
                OnDrillDown=BEGIN
                              ShowDetails;
                            END;
                             }

    { 19  ;1   ;Field     ;
                ToolTipML=[DEU=Gibt das Wiederbeschaffungsverfahren an, das zum Berechnen der Losgr��e pro Planungsperiode (Zeitrahmen) verwendet wird.;
                           ENU=Specifies the reordering policy that is used to calculate the lot size per planning period (time bucket).];
                SourceExpr="Reordering Policy" }

    { 7   ;1   ;Field     ;
                ToolTipML=[DEU=Gibt einen Bestand an, der den Lagerbestand festlegt, bei dessen Unterschreitung der Artikel beschafft werden muss.;
                           ENU=Specifies a stock quantity that sets the inventory below the level that you must replenish the item.];
                SourceExpr="Reorder Point" }

    { 9   ;1   ;Field     ;
                ToolTipML=[DEU=Gibt eine f�r alle Bestellvorschl�ge zu verwendende Standardlosgr��e an.;
                           ENU=Specifies a standard lot size quantity to be used for all order proposals.];
                SourceExpr="Reorder Quantity" }

    { 11  ;1   ;Field     ;
                ToolTipML=[DEU=Gibt eine Menge an, die Sie als maximalen Lagerbestand verwenden m�chten.;
                           ENU=Specifies a quantity that you want to use as a maximum inventory level.];
                SourceExpr="Maximum Inventory" }

    { 29  ;1   ;Field     ;
                ToolTipML=[DEU=Gibt eine Menge an, um die der voraussichtliche Lagerbestand den Minimalbestand �berschreiten darf, bevor das System vorschl�gt, die Beschaffungsauftr�ge zu vermindern.;
                           ENU=Specifies a quantity you allow projected inventory to exceed the reorder point, before the system suggests to decrease supply orders.];
                SourceExpr="Overflow Level" }

    { 1   ;1   ;Field     ;
                ToolTipML=[DEU=Gibt einen Zeitraum an, der den mit den Wiederbeschaffungsverfahren "Feste Bestellmenge" oder "Auff�llen auf Maximalbestand" verwendeten wiederkehrenden Planungszeitraum festlegt.;
                           ENU=Specifies a time period that defines the recurring planning horizon used with Fixed Reorder Qty. or Maximum Qty. reordering policies.];
                SourceExpr="Time Bucket" }

    { 25  ;1   ;Field     ;
                ToolTipML=[DEU=Definiert eine Periode, in der mehrere Bedarfsposten in einen Beschaffungsauftrag kumuliert werden, wenn Sie das Los-f�r-Los-Wiederbeschaffungsverfahren verwenden.;
                           ENU=Defines a period in which multiple demands are accumulated into one supply order when you use the Lot-for-Lot reordering policy.];
                SourceExpr="Lot Accumulation Period" }

    { 31  ;1   ;Field     ;
                ToolTipML=[DEU=Definiert eine Periode, innerhalb der jeder Vorschlag, ein Lieferdatum zu �ndern, immer aus einer Aktion "Neu berechnen" und nie aus einer Aktion "Abbrechen + Neu" besteht.;
                           ENU=Defines a period within which any suggestion to change a supply date always consists of a Reschedule action and never a Cancel + New action.];
                SourceExpr="Rescheduling Period" }

    { 3   ;1   ;Field     ;
                ToolTipML=[DEU=Definiert eine Datumsformel, die einen Sicherheitszuschlag Beschaffungszeit angibt, der als Pufferzeit f�r Verz�gerungen bei der Produktion und andere Verz�gerungen verwendet werden kann.;
                           ENU=Defines a date formula to indicate a safety lead time that can be used as a buffer period for production and other delays.];
                SourceExpr="Safety Lead Time" }

    { 5   ;1   ;Field     ;
                ToolTipML=[DEU=Definiert eine Menge, die immer im Lager verf�gbar sein soll, um gegen Schwankungen von Bedarf und Angebot w�hrend der Beschaffungszeit gesch�tzt zu sein.;
                           ENU=Defines a quantity of stock to have in inventory to protect against supply-and-demand fluctuations during replenishment lead time.];
                SourceExpr="Safety Stock Quantity" }

    { 13  ;1   ;Field     ;
                ToolTipML=[DEU=Legt eine minimal erlaubte Menge f�r einen Artikelauftragsvorschlag fest.;
                           ENU=Defines a minimum allowable quantity for an item order proposal.];
                SourceExpr="Minimum Order Quantity" }

    { 15  ;1   ;Field     ;
                ToolTipML=[DEU=Gibt eine maximal erlaubte Menge f�r einen Artikelauftragsvorschlag an.;
                           ENU=Specifies a maximum allowable quantity for an item order proposal.];
                SourceExpr="Maximum Order Quantity" }

    { 17  ;1   ;Field     ;
                ToolTipML=[DEU=Definiert einen Parameter, der vom Planungssystem verwendet wird, um die Menge geplanter Beschaffungsauftr�ge zu �ndern.;
                           ENU=Defines a parameter used by the planning system to modify the quantity of planned supply orders.];
                SourceExpr="Order Multiple" }

    { 23  ;1   ;Field     ;
                ToolTipML=[DEU=Gibt eine Zeitspanne an, innerhalb derer vom Planungssystem nicht vorgeschlagen werden soll, bestehende Beschaffungsauftr�ge in der Planung vorzuverlegen.;
                           ENU=Specifies a period of time during which you do not want the planning system to propose to reschedule existing supply orders.];
                SourceExpr="Dampener Period" }

    { 27  ;1   ;Field     ;
                ToolTipML=[DEU=Gibt eine Toleranzmenge an, um unwichtige �nderungsvorschl�ge f�r eine vorhandene Lieferung zu sperren, wenn die �nderungsmenge die Toleranzmenge nicht �bersteigt.;
                           ENU=Specifies a dampener quantity to block insignificant change suggestions for an existing supply, if the change quantity is lower than the dampener quantity.];
                SourceExpr="Dampener Quantity" }

  }
  CODE
  {

    LOCAL PROCEDURE ShowDetails@1102601000();
    BEGIN
      PAGE.RUN(PAGE::"Item Card",Rec);
    END;

    BEGIN
    END.
  }
}

