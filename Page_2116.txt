OBJECT Page 2116 O365 Customer Lookup
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Debitoren;
               ENU=Customers];
    SourceTable=Table18;
    SourceTableView=SORTING(Name);
    PageType=List;
    CardPageID=O365 Sales Customer Card;
  }
  CONTROLS
  {
    { 1900000001;;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU="";
                           ENU=""];
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Debitors an.;
                           ENU=Specifies the number of the customer.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No.";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Debitors an. Der Name wird auf allen Verkaufsbelegen f�r den Debitor angezeigt. Sie k�nnen bis zu 50 Zeichen, sowohl Ziffern als auch Buchstaben, eingeben.;
                           ENU=Specifies the customer's name. This name will appear on all sales documents for the customer. You can enter a maximum of 50 characters, both numbers and letters.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Telefonnummer des Debitors an.;
                           ENU=Specifies the customer's telephone number.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Phone No." }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Kontaktperson bei diesem Debitor an.;
                           ENU=Specifies the name of the person you regularly contact when you do business with this customer.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Contact }

    { 62  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Zahlungsbetrag an, den der Debitor f�r abgeschlossene Verk�ufe schuldet. Dieser Wert wird auch als der Saldo des Debitors bezeichnet.;
                           ENU=Specifies the payment amount that the customer owes for completed sales. This value is also known as the customer's balance.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Balance (LCY)";
                OnDrillDown=BEGIN
                              OpenCustomerLedgerEntries(FALSE);
                            END;
                             }

    { 59  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt Zahlungen des Debitors an, die nach dem heutigen Datum �berf�llig sind.;
                           ENU=Specifies payments from the customer that are overdue per today's date.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Balance Due (LCY)";
                OnDrillDown=BEGIN
                              OpenCustomerLedgerEntries(TRUE);
                            END;
                             }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den gesamten Nettobetrag der Verk�ufe an den Debitor in MW an.;
                           ENU=Specifies the total net amount of sales to the customer in LCY.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sales (LCY)" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

