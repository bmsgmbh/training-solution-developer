OBJECT Page 5660 Depreciation Table Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Zeilen;
               ENU=Lines];
    LinksAllowed=No;
    SourceTable=Table5643;
    DelayedInsert=Yes;
    PageType=ListPart;
    OnNewRecord=BEGIN
                  NewRecord;
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Abschreibungsperiode an, in der diese Zeile angewendet wird.;
                           ENU=Specifies the number of the depreciation period that this line applies to.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Period No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Abschreibungsprozentsatzes an, der auf die Periode f�r diese Zeile angewendet wird.;
                           ENU=Specifies the depreciation percentage to apply to the period for this line.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Period Depreciation %" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Einheiten an, die die Anlage in der Periode, in der diese Zeile gilt, produziert.;
                           ENU=Specifies the units produced by the asset this depreciation table applies to, during the period when this line applies.];
                ApplicationArea=#FixedAssets;
                SourceExpr="No. of Units in Period" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

