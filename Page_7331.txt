OBJECT Page 7331 Posted Whse. Receipt Subform
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Zeilen;
               ENU=Lines];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table7319;
    DelayedInsert=Yes;
    PageType=ListPart;
    AutoSplitKey=Yes;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 1900206304;2 ;Action    ;
                      CaptionML=[DEU=Geb. Herkunftsbeleg;
                                 ENU=Posted Source Document];
                      Image=PostedOrder;
                      OnAction=BEGIN
                                 ShowPostedSourceDoc;
                               END;
                                }
      { 1901742304;2 ;Action    ;
                      CaptionML=[DEU=Logistikbelegzeile;
                                 ENU=Whse. Document Line];
                      Image=Line;
                      OnAction=BEGIN
                                 ShowWhseLine;
                               END;
                                }
      { 1903867004;2 ;Action    ;
                      CaptionML=[DEU=Lagerplatzinhalts�bersicht;
                                 ENU=Bin Contents List];
                      Image=BinContent;
                      OnAction=BEGIN
                                 ShowBinContents;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Belegs an, auf den sich die Zeile bezieht.;
                           ENU=Specifies the type of document to which the line relates.];
                SourceExpr="Source Document" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Herkunftsnummer des Belegs an, aus dem die Zeile stammt.;
                           ENU=Specifies the source number of the document from which the line originates.];
                SourceExpr="Source No." }

    { 62  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum der f�lligen Lieferung f�r diese Zeile an.;
                           ENU=Specifies the date that the receipt line was due.];
                SourceExpr="Due Date" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Regalnummer des Artikels zu Informationszwecken an.;
                           ENU=Specifies the shelf number of the item for informational use.];
                SourceExpr="Shelf No.";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Zonencode dieser gebuchten Wareneingangszeile an.;
                           ENU=Specifies the code of the zone on this posted receipt line.];
                SourceExpr="Zone Code";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerplatzes der gebuchten Wareneingangszeile an.;
                           ENU=Specifies the code of the bin on the posted receipt line.];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Zonencode an, der bei der Erstellung der Zuordnungseinlagerung f�r diese Zeile verwendet wurde, als der Wareneingang gebucht wurde.;
                           ENU=Specifies the zone code used to create the cross-dock put-away for this line when the receipt was posted.];
                SourceExpr="Cross-Dock Zone Code";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerplatzcode an, der bei der Erstellung der Zuordnungseinlagerung f�r diese Zeile verwendet wurde, als der Wareneingang gebucht wurde.;
                           ENU=Specifies the bin code used to create the cross-dock put-away for this line when the receipt was posted.];
                SourceExpr="Cross-Dock Bin Code";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an, der geliefert und gebucht wurde.;
                           ENU=Specifies the number of the item that was received and posted.];
                SourceExpr="Item No." }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Variantennummer des Artikels in der Zeile an, wenn vorhanden.;
                           ENU=Specifies the variant number of the item in the line, if any.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des Artikels in der Zeile an.;
                           ENU=Specifies the description of the item in the line.];
                SourceExpr=Description }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine zweite Beschreibung des Artikels in der Zeile an, falls vorhanden.;
                           ENU=Specifies a second description of the item in the line, if any.];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge an, die eingegangen ist.;
                           ENU=Specifies the quantity that was received.];
                SourceExpr=Quantity }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge im Wareneingang in der Basiseinheit an.;
                           ENU=Specifies the quantity that was received, in the base unit of measure.];
                SourceExpr="Qty. (Base)";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge an, die eingelagert ist.;
                           ENU=Specifies the quantity that is put away.];
                SourceExpr="Qty. Put Away";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge der Artikel an, die sich in der Wareneingangszeile im Feld "Menge f�r Zuordnung" befand, als diese gebucht wurde.;
                           ENU=Specifies the quantity of items that was in the Qty. To Cross-Dock field on the warehouse receipt line when it was posted.];
                SourceExpr="Qty. Cross-Docked";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die eingelagerte Menge in der Basiseinheit an.;
                           ENU=Specifies the quantity that is put away, in the base unit of measure.];
                SourceExpr="Qty. Put Away (Base)";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Basismenge der Artikel an, die sich in der Wareneingangszeile im Feld "Menge f�r Zuordnung (Basis)" befand, als diese gebucht wurde.;
                           ENU=Specifies the base quantity of items in the Qty. To Cross-Dock (Base) field on the warehouse receipt line when it was posted.];
                SourceExpr="Qty. Cross-Docked (Base)";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge aus den Einlagerungsanweisungen an, die sich bereits im Einlagerungsprozess befindet.;
                           ENU=Specifies the quantity on put-away instructions in the process of being put away.];
                SourceExpr="Put-away Qty.";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge aus den Einlagerungsanweisungen, die sich bereits im Einlagerungsprozess befindet, in der Basiseinheit an.;
                           ENU=Specifies the quantity on put-away instructions, in the base unit of measure, in the process of being put away.];
                SourceExpr="Put-away Qty. (Base)";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Einheit f�r den Artikel in der Zeile an.;
                           ENU=Specifies the code of the unit of measure for the item on the line.];
                SourceExpr="Unit of Measure Code" }

  }
  CODE
  {
    VAR
      WMSMgt@1000 : Codeunit 7302;

    LOCAL PROCEDURE ShowPostedSourceDoc@1();
    BEGIN
      WMSMgt.ShowPostedSourceDoc("Posted Source Document","Posted Source No.");
    END;

    LOCAL PROCEDURE ShowBinContents@7301();
    VAR
      BinContent@1000 : Record 7302;
    BEGIN
      BinContent.ShowBinContents("Location Code","Item No.","Variant Code","Bin Code");
    END;

    LOCAL PROCEDURE ShowWhseLine@3();
    BEGIN
      WMSMgt.ShowWhseDocLine(0,"Whse. Receipt No.","Whse Receipt Line No.");
    END;

    PROCEDURE PutAwayCreate@2();
    VAR
      PostedWhseRcptHdr@1000 : Record 7318;
      PostedWhseRcptLine@1002 : Record 7319;
    BEGIN
      PostedWhseRcptHdr.GET("No.");
      PostedWhseRcptLine.COPY(Rec);
      CreatePutAwayDoc(PostedWhseRcptLine,PostedWhseRcptHdr."Assigned User ID");
    END;

    BEGIN
    END.
  }
}

