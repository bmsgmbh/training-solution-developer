OBJECT Page 9130 Contact Statistics FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Infobox 'Kontaktstatistik';
               ENU=Contact Statistics FactBox];
    SourceTable=Table5050;
    PageType=CardPart;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General];
                GroupType=Group }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gesamtkosten aller Aktivit�ten an, die sich auf den Kontakt beziehen. Dieses Feld kann nicht bearbeitet werden.;
                           ENU=Specifies the total cost of all the interactions involving the contact. The field is not editable.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Cost (LCY)" }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gesamtdauer aller Aktivit�ten an, die sich auf den Kontakt beziehen. Dieses Feld kann nicht bearbeitet werden.;
                           ENU=Specifies the total duration of all the interactions involving the contact. The field is not editable.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Duration (Min.)" }

    { 6   ;1   ;Group     ;
                CaptionML=[DEU=Verkaufschancen;
                           ENU=Opportunities];
                GroupType=Group }

    { 7   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der offenen Verkaufschancen an, die sich auf den Kontakt beziehen. Dieses Feld kann nicht bearbeitet werden.;
                           ENU=Specifies the number of open opportunities involving the contact. The field is not editable.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No. of Opportunities" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den erwarteten Gesamtwert aller Verkaufschancen an, die sich auf den Kontakt beziehen. Dieses Feld kann nicht bearbeitet werden.;
                           ENU=Specifies the total estimated value of all the opportunities involving the contact. The field is not editable.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Estimated Value (LCY)" }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den berechneten aktuellen Gesamtwert aller Verkaufschancen an, die sich auf den Kontakt beziehen. Dieses Feld kann nicht bearbeitet werden.;
                           ENU=Specifies the total calculated current value of all the opportunities involving the contact. The field is not editable.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Calcd. Current Value (LCY)" }

    { 10  ;1   ;Group     ;
                CaptionML=[DEU=Segmentierung;
                           ENU=Segmentation];
                GroupType=Group }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Verantwortlichkeiten f�r diesen Kontakt an. Dieses Feld gilt nur f�r Personen und kann nicht bearbeitet werden.;
                           ENU=Specifies the number of job responsibilities for this contact. This field is valid for persons only and is not editable.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No. of Job Responsibilities" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Branchen an, zu denen der Kontakt geh�rt. Wenn es sich bei dem Kontakt um eine Person handelt, enth�lt das Feld die Anzahl der Branchen, zu denen das Unternehmen des Kontakts geh�rt. Dieses Feld kann nicht bearbeitet werden.;
                           ENU=Specifies the number of industry groups to which the contact belongs. When the contact is a person, this field contains the number of industry groups for the contact's company. This field is not editable.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No. of Industry Groups" }

    { 13  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Gesch�ftsbeziehungen (z. B. Debitor, Kreditor, Berater, Wettbewerber usw.) an, die Ihr Unternehmen mit diesem Kontakt h�lt. Wenn es sich bei dem Kontakt um eine Person handelt, enth�lt das Feld die Anzahl der Gesch�ftsbeziehungen des Unternehmens Ihres Kontakts. Dieses Feld kann nicht bearbeitet werden.;
                           ENU=Specifies the number of business relations (for example, customer, vendor, consultant, competitor, and so on) your company has with this contact. When the contact is a person, this field contains the number of business relations for the contact's company. This field is not editable.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No. of Business Relations" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Verteiler f�r diesen Kontakt an.;
                           ENU=Specifies the number of mailing groups for this contact.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No. of Mailing Groups" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

