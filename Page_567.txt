OBJECT Page 567 Dimension Selection-Change
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Dimensionsauswahl;
               ENU=Dimension Selection];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table368;
    PageType=List;
    SourceTableTemporary=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=;
                ApplicationArea=#Suite;
                SourceExpr=Selected }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Dimension an.;
                           ENU=Specifies the code for the dimension.];
                ApplicationArea=#Suite;
                SourceExpr=Code;
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Dimension an.;
                           ENU=Specifies a description of the dimension.];
                ApplicationArea=#Suite;
                SourceExpr=Description;
                Editable=FALSE }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Legt einen Filter fest, nach dem Dimensionswerte angezeigt werden.;
                           ENU=Specifies a filter by which dimensions values will be shown.];
                ApplicationArea=#Suite;
                SourceExpr="Dimension Value Filter" }

    { 13  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den neuen Dimensionswert an, in den ge�ndert wird.;
                           ENU=Specifies the new dimension value to that you are changing to.];
                ApplicationArea=#Suite;
                SourceExpr="New Dimension Value Code" }

  }
  CODE
  {

    PROCEDURE GetDimSelBuf@7(VAR TheDimSelectionBuf@1000 : Record 368);
    BEGIN
      TheDimSelectionBuf.DELETEALL;
      IF FIND('-') THEN
        REPEAT
          TheDimSelectionBuf := Rec;
          TheDimSelectionBuf.INSERT;
        UNTIL NEXT = 0;
    END;

    PROCEDURE InsertDimSelBuf@11(NewSelected@1000 : Boolean;NewCode@1001 : Text[30];NewDescription@1002 : Text[30];NewNewDimValueCode@1003 : Code[20];NewDimValueFilter@1004 : Text[250]);
    VAR
      Dim@1005 : Record 348;
      GLAcc@1006 : Record 15;
      BusinessUnit@1007 : Record 220;
    BEGIN
      IF NewDescription = '' THEN BEGIN
        IF Dim.GET(NewCode) THEN
          NewDescription := Dim.Name;
      END;

      INIT;
      Selected := NewSelected;
      Code := NewCode;
      Description := NewDescription;
      IF NewSelected THEN BEGIN
        "New Dimension Value Code" := NewNewDimValueCode;
        "Dimension Value Filter" := NewDimValueFilter;
      END;
      CASE Code OF
        GLAcc.TABLECAPTION:
          "Filter Lookup Table No." := DATABASE::"G/L Account";
        BusinessUnit.TABLECAPTION:
          "Filter Lookup Table No." := DATABASE::"Business Unit";
      END;
      INSERT;
    END;

    BEGIN
    END.
  }
}

