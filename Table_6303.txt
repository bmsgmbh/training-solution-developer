OBJECT Table 6303 Azure AD Mgt. Setup
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Azure AD-Verwaltungseinrichtung;
               ENU=Azure AD Mgt. Setup];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[DEU=Hauptschl�ssel;
                                                              ENU=Primary Key] }
    { 2   ;   ;Auth Flow Codeunit ID;Integer      ;TableRelation="CodeUnit Metadata".ID;
                                                   CaptionML=[DEU=Auth.fluss-Codeunit-ID;
                                                              ENU=Auth Flow Codeunit ID] }
    { 3   ;   ;Azure AD User Mgt. Codeunit ID;Integer;
                                                   TableRelation="CodeUnit Metadata".ID;
                                                   CaptionML=[DEU=Azure AD-Benutzerverwaltungs-Codeunit-ID;
                                                              ENU=Azure AD User Mgt. Codeunit ID] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE ResetToDefault@1();
    BEGIN
      VALIDATE("Auth Flow Codeunit ID",CODEUNIT::"Azure AD Auth Flow");
      VALIDATE("Azure AD User Mgt. Codeunit ID",CODEUNIT::"Azure AD User Management");
    END;

    BEGIN
    END.
  }
}

