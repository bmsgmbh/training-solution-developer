OBJECT Table 9054 Finance Cue
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Finanzstapel;
               ENU=Finance Cue];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[DEU=Prim�rschl�ssel;
                                                              ENU=Primary Key] }
    { 2   ;   ;Overdue Sales Documents;Integer    ;FieldClass=FlowField;
                                                   CalcFormula=Count("Cust. Ledger Entry" WHERE (Document Type=FILTER(Invoice|Credit Memo),
                                                                                                 Due Date=FIELD(Overdue Date Filter),
                                                                                                 Open=CONST(Yes)));
                                                   CaptionML=[DEU=�berf�llige Verkaufsbelege;
                                                              ENU=Overdue Sales Documents] }
    { 3   ;   ;Purchase Documents Due Today;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Vendor Ledger Entry" WHERE (Document Type=FILTER(Invoice|Credit Memo),
                                                                                                  Due Date=FIELD(Due Date Filter),
                                                                                                  Open=CONST(Yes)));
                                                   CaptionML=[DEU=Heute f�llige Einkaufsbelege;
                                                              ENU=Purchase Documents Due Today] }
    { 4   ;   ;POs Pending Approval;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Purchase Header" WHERE (Document Type=CONST(Order),
                                                                                              Status=FILTER(Pending Approval)));
                                                   AccessByPermission=TableData 120=R;
                                                   CaptionML=[DEU=Bestellungen mit ausstehender Genehmigung;
                                                              ENU=POs Pending Approval] }
    { 5   ;   ;SOs Pending Approval;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=CONST(Order),
                                                                                           Status=FILTER(Pending Approval)));
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[DEU=Verkaufsauftr�ge mit ausstehender Genehmigung;
                                                              ENU=SOs Pending Approval] }
    { 6   ;   ;Approved Sales Orders;Integer      ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=CONST(Order),
                                                                                           Status=FILTER(Released|Pending Prepayment)));
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[DEU=Genehmigte Verkaufsauftr�ge;
                                                              ENU=Approved Sales Orders] }
    { 7   ;   ;Approved Purchase Orders;Integer   ;FieldClass=FlowField;
                                                   CalcFormula=Count("Purchase Header" WHERE (Document Type=CONST(Order),
                                                                                              Status=FILTER(Released|Pending Prepayment)));
                                                   AccessByPermission=TableData 120=R;
                                                   CaptionML=[DEU=Genehmigte Bestellungen;
                                                              ENU=Approved Purchase Orders] }
    { 8   ;   ;Vendors - Payment on Hold;Integer  ;FieldClass=FlowField;
                                                   CalcFormula=Count(Vendor WHERE (Blocked=FILTER(Payment)));
                                                   CaptionML=[DEU=Kreditoren - Zahlung abwarten;
                                                              ENU=Vendors - Payment on Hold] }
    { 9   ;   ;Purchase Return Orders;Integer     ;FieldClass=FlowField;
                                                   CalcFormula=Count("Purchase Header" WHERE (Document Type=CONST(Return Order)));
                                                   AccessByPermission=TableData 6650=R;
                                                   CaptionML=[DEU=Einkaufsreklamationen;
                                                              ENU=Purchase Return Orders] }
    { 10  ;   ;Sales Return Orders - All;Integer  ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=CONST(Return Order)));
                                                   AccessByPermission=TableData 6660=R;
                                                   CaptionML=[DEU=Verkaufsreklamationen - Alle;
                                                              ENU=Sales Return Orders - All] }
    { 11  ;   ;Customers - Blocked ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count(Customer WHERE (Blocked=FILTER(<>' ')));
                                                   CaptionML=[DEU=Debitoren - Gesperrt;
                                                              ENU=Customers - Blocked] }
    { 16  ;   ;Overdue Purchase Documents;Integer ;FieldClass=FlowField;
                                                   CalcFormula=Count("Vendor Ledger Entry" WHERE (Document Type=FILTER(Invoice|Credit Memo),
                                                                                                  Due Date=FIELD(Overdue Date Filter),
                                                                                                  Open=CONST(Yes)));
                                                   CaptionML=[DEU=�berf�llige Einkaufsbelege;
                                                              ENU=Overdue Purchase Documents] }
    { 17  ;   ;Purchase Discounts Next Week;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Vendor Ledger Entry" WHERE (Document Type=FILTER(Invoice|Credit Memo),
                                                                                                  Pmt. Discount Date=FIELD(Due Next Week Filter),
                                                                                                  Open=CONST(Yes)));
                                                   CaptionML=[DEU=Einkaufsrabatte n�chste Woche;
                                                              ENU=Purchase Discounts Next Week];
                                                   Editable=No }
    { 18  ;   ;Purch. Invoices Due Next Week;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Vendor Ledger Entry" WHERE (Document Type=FILTER(Invoice|Credit Memo),
                                                                                                  Due Date=FIELD(Due Next Week Filter),
                                                                                                  Open=CONST(Yes)));
                                                   CaptionML=[DEU=N�chste Woche f�llige Einkaufsrechnungen;
                                                              ENU=Purch. Invoices Due Next Week];
                                                   Editable=No }
    { 19  ;   ;Due Next Week Filter;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[DEU=Filter 'N�chste Woche f�llig';
                                                              ENU=Due Next Week Filter] }
    { 20  ;   ;Due Date Filter     ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[DEU=F�lligkeitsdatumsfilter;
                                                              ENU=Due Date Filter];
                                                   Editable=No }
    { 21  ;   ;Overdue Date Filter ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[DEU=�berf�lligkeits-Datumsfilter;
                                                              ENU=Overdue Date Filter] }
    { 22  ;   ;New Incoming Documents;Integer     ;FieldClass=FlowField;
                                                   CalcFormula=Count("Incoming Document" WHERE (Status=CONST(New)));
                                                   CaptionML=[DEU=Neue eingehende Dokumente;
                                                              ENU=New Incoming Documents] }
    { 23  ;   ;Approved Incoming Documents;Integer;FieldClass=FlowField;
                                                   CalcFormula=Count("Incoming Document" WHERE (Status=CONST(Released)));
                                                   CaptionML=[DEU=Genehmigte eingehende Dokumente;
                                                              ENU=Approved Incoming Documents] }
    { 24  ;   ;OCR Pending         ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Incoming Document" WHERE (OCR Status=FILTER(Ready|Sent|Awaiting Verification)));
                                                   CaptionML=[DEU=OCR offen;
                                                              ENU=OCR Pending] }
    { 25  ;   ;OCR Completed       ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Incoming Document" WHERE (OCR Status=CONST(Success)));
                                                   CaptionML=[DEU=OCR abgeschlossen;
                                                              ENU=OCR Completed] }
    { 26  ;   ;Requests to Approve ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Approval Entry" WHERE (Approver ID=FIELD(User ID Filter),
                                                                                             Status=FILTER(Open)));
                                                   CaptionML=[DEU=Zu genehmigende Anforderungen;
                                                              ENU=Requests to Approve] }
    { 27  ;   ;Requests Sent for Approval;Integer ;FieldClass=FlowField;
                                                   CalcFormula=Count("Approval Entry" WHERE (Sender ID=FIELD(User ID Filter),
                                                                                             Status=FILTER(Open)));
                                                   CaptionML=[DEU=Anforderungen zur Genehmigung gesendet;
                                                              ENU=Requests Sent for Approval] }
    { 28  ;   ;User ID Filter      ;Code50        ;FieldClass=FlowFilter;
                                                   CaptionML=[DEU=Benutzer-ID-Filter;
                                                              ENU=User ID Filter] }
    { 29  ;   ;Non-Applied Payments;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Bank Acc. Reconciliation" WHERE (Statement Type=CONST(Payment Application)));
                                                   CaptionML=[DEU=Nicht zugeordnete Zahlungen;
                                                              ENU=Non-Applied Payments] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

