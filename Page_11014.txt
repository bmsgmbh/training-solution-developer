OBJECT Page 11014 Data Export Record Types
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVDACH10.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Datenexport - Datensatztypen;
               ENU=Data Export Record Types];
    SourceTable=Table11007;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1140000;1;Group     ;
                GroupType=Repeater }

    { 1140001;2;Field     ;
                ToolTipML=[DEU=Legt einen Code f�r die Datensatzart eines Datenexports fest.;
                           ENU=Specifies a code for a data export record type.];
                ApplicationArea=#All;
                SourceExpr=Code }

    { 1140003;2;Field     ;
                ToolTipML=[DEU=Legt eine kurze Beschreibung f�r die Datensatzart eines Datenexports fest.;
                           ENU=Specifies a short description for a data export record type.];
                ApplicationArea=#All;
                SourceExpr=Description }

  }
  CODE
  {

    BEGIN
    END.
  }
}

