OBJECT Page 755 Standard Item Journal Subform
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Zeilen;
               ENU=Lines];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table753;
    DelayedInsert=Yes;
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                     END;

    OnNewRecord=BEGIN
                  "Entry Type" := xRec."Entry Type";
                  CLEAR(ShortcutDimCode);
                  "Source Code" := GetSourceCodeFromJnlTemplate;
                END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 1900206304;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Zeigt Dimensionen an oder bearbeitet sie, wie etwa einen Bereich, ein Projekt oder eine Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, aus welcher Art Transaktion der Posten erstellt wurde.;
                           ENU=Specifies which type of transaction that the entry is created from.];
                ApplicationArea=#Suite;
                SourceExpr="Entry Type" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels in der Buch.-Blattzeile an.;
                           ENU=Specifies the number of the item on the journal line.];
                ApplicationArea=#Suite;
                SourceExpr="Item No." }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Variante des Artikels in der Zeile an.;
                           ENU=Specifies the variant of the item on the line.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des Artikels in der Zeile an.;
                           ENU=Specifies the description of the item on the line.];
                ApplicationArea=#Suite;
                SourceExpr=Description }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, mit dem die Artikel Buch.-Blattzeile verkn�pft ist.;
                           ENU=Specifies the dimension value code that the item journal line is linked to.];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, mit dem die Artikel Buch.-Blattzeile verkn�pft ist.;
                           ENU=Specifies the dimension value code that the item journal line is linked to.];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(3,ShortcutDimCode[3]);
                           END;
                            }

    { 20  ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(4,ShortcutDimCode[4]);
                           END;
                            }

    { 22  ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(5,ShortcutDimCode[5]);
                           END;
                            }

    { 24  ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(6,ShortcutDimCode[6]);
                           END;
                            }

    { 26  ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(7,ShortcutDimCode[7]);
                           END;
                            }

    { 32  ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(8,ShortcutDimCode[8]);
                           END;
                            }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerortcode f�r den Artikel in der Zeile an.;
                           ENU=Specifies the location code for the item on the line.];
                SourceExpr="Location Code" }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerplatzcode f�r die Artikel in der Zeile an.;
                           ENU=Specifies the bin code for the items on the line.];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r den Eink�ufer oder Verk�ufer an, der mit dem Verkauf oder Einkauf in der Buch.-Blattzeile verkn�pft ist.;
                           ENU=Specifies the code for the salesperson or purchaser who is linked to the sale or purchase on the journal line.];
                SourceExpr="Salespers./Purch. Code";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Buchungsgruppe des Masterdatensatzes in der Buch.-Blattzeile an.;
                           ENU=Specifies the posting group of the master record on the journal line.];
                SourceExpr="Gen. Bus. Posting Group";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Produktkonto in der Finanzbuchhaltung an, auf das Transaktionen mit dem Artikel gebucht werden.;
                           ENU=Specifies the general product account in the general ledger to which transactions involving the item are posted.];
                SourceExpr="Gen. Prod. Posting Group";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels in der Buch.-Blattzeile an.;
                           ENU=Specifies the quantity of the item in the journal line.];
                ApplicationArea=#Suite;
                SourceExpr=Quantity }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Einheitencode an, der in der Tabelle "Einheit" eingerichtet wurde.;
                           ENU=Specifies a unit of measure code that has been set up in the Unit of Measure table.];
                ApplicationArea=#Suite;
                SourceExpr="Unit of Measure Code" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag der Einheit in der Zeile der Buch.-Blattzeile an.;
                           ENU=Specifies the amount of the unit in the line of the journal line.];
                ApplicationArea=#Suite;
                SourceExpr="Unit Amount" }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag der Einheit in der Zeile der Buch.-Blattzeile an.;
                           ENU=Specifies the amount of the unit in the line of the journal line.];
                ApplicationArea=#Suite;
                SourceExpr=Amount }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die indirekten Kosten des Artikels an.;
                           ENU=Specifies the item indirect cost.];
                SourceExpr="Indirect Cost %";
                Visible=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Kosten der Einheit in der Zeile der Buch.-Blattzeile an.;
                           ENU=Specifies the cost of the unit in the line of the journal line.];
                ApplicationArea=#Suite;
                SourceExpr="Unit Cost" }

    { 48  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Transaktionstyp der Artikel-Buch.-Blattzeile an.;
                           ENU=Specifies the transaction type of the item journal line.];
                SourceExpr="Transaction Type";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r den Verkehrszweig an, der f�r den Artikel verwendet wird.;
                           ENU=Specifies the code for the transport method used for the item.];
                SourceExpr="Transport Method";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den L�nder-/Regionscode des Artikels an.;
                           ENU=Specifies the country/region code of the item.];
                SourceExpr="Country/Region Code";
                Visible=FALSE }

    { 54  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Vorlage an, aus der die Buch.-Blattzeile stammt.;
                           ENU=Specifies the template that is the source of the journal line.];
                SourceExpr="Reason Code";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      ShortcutDimCode@1000 : ARRAY [8] OF Code[20];

    LOCAL PROCEDURE GetSourceCodeFromJnlTemplate@1() : Code[10];
    VAR
      ItemJnlTemplate@1000 : Record 82;
    BEGIN
      ItemJnlTemplate.GET("Journal Template Name");
      EXIT(ItemJnlTemplate."Source Code");
    END;

    LOCAL PROCEDURE ValidateSaveShortcutDimCode@5(FieldNumber@1001 : Integer;VAR ShortcutDimCode@1000 : Code[20]);
    BEGIN
      ValidateShortcutDimCode(FieldNumber,ShortcutDimCode);
      CurrPage.SAVERECORD;
    END;

    BEGIN
    END.
  }
}

