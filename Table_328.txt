OBJECT Table 328 Currency for Fin. Charge Terms
{
  OBJECT-PROPERTIES
  {
    Date=15.09.15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Fin. Charge Terms Code;
    CaptionML=[DEU=W�hrung f�r Zinskonditionen;
               ENU=Currency for Fin. Charge Terms];
    LookupPageID=Page477;
    DrillDownPageID=Page477;
  }
  FIELDS
  {
    { 1   ;   ;Fin. Charge Terms Code;Code10      ;TableRelation="Finance Charge Terms";
                                                   CaptionML=[DEU=Zinskonditionencode;
                                                              ENU=Fin. Charge Terms Code];
                                                   NotBlank=Yes;
                                                   Editable=No }
    { 2   ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[DEU=W�hrungscode;
                                                              ENU=Currency Code];
                                                   NotBlank=Yes }
    { 4   ;   ;Additional Fee      ;Decimal       ;CaptionML=[DEU=Zus�tzliche Geb�hr;
                                                              ENU=Additional Fee];
                                                   MinValue=0;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
  }
  KEYS
  {
    {    ;Fin. Charge Terms Code,Currency Code    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

