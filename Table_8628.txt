OBJECT Table 8628 Config. Field Mapping
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Feldzuordnung konfigurieren;
               ENU=Config. Field Mapping];
  }
  FIELDS
  {
    { 1   ;   ;Package Code        ;Code20        ;TableRelation="Config. Package";
                                                   CaptionML=[DEU=Paketcode;
                                                              ENU=Package Code];
                                                   NotBlank=Yes }
    { 2   ;   ;Table ID            ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Table));
                                                   CaptionML=[DEU=Tabellen-ID;
                                                              ENU=Table ID];
                                                   NotBlank=Yes }
    { 3   ;   ;Field ID            ;Integer       ;CaptionML=[DEU=Feld-ID;
                                                              ENU=Field ID];
                                                   NotBlank=Yes }
    { 4   ;   ;Field Name          ;Text30        ;CaptionML=[DEU=Feldname;
                                                              ENU=Field Name] }
    { 5   ;   ;Old Value           ;Text250       ;CaptionML=[DEU=Alter Wert;
                                                              ENU=Old Value] }
    { 6   ;   ;New Value           ;Text250       ;CaptionML=[DEU=Neuer Wert;
                                                              ENU=New Value] }
  }
  KEYS
  {
    {    ;Package Code,Table ID,Field ID,Old Value;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

