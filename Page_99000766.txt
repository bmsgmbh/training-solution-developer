OBJECT Page 99000766 Routing
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Arbeitsplan;
               ENU=Routing];
    SourceTable=Table99000763;
    PageType=ListPlus;
    OnAfterGetRecord=BEGIN
                       ActiveVersionCode :=
                         VersionMgt.GetRtngVersion("No.",WORKDATE,TRUE);
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 32      ;1   ;ActionGroup;
                      CaptionML=[DEU=A&rbeitsplan;
                                 ENU=&Routing];
                      Image=Route }
      { 34      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 99000784;
                      RunPageLink=Table Name=CONST(Routing Header),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 35      ;2   ;Action    ;
                      CaptionML=[DEU=&Versionen;
                                 ENU=&Versions];
                      RunObject=Page 99000808;
                      RunPageLink=Routing No.=FIELD(No.);
                      Promoted=Yes;
                      Image=RoutingVersions;
                      PromotedCategory=Process }
      { 41      ;2   ;Action    ;
                      CaptionML=[DEU=Verwendung;
                                 ENU=Where-used];
                      RunObject=Page 99000782;
                      RunPageView=SORTING(Routing No.);
                      RunPageLink=Routing No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Where-Used;
                      PromotedCategory=Process }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 18      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=&Arbeitsplan kopieren;
                                 ENU=Copy &Routing];
                      Promoted=Yes;
                      Image=CopyDocument;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 FromRtngHeader@1001 : Record 99000763;
                               BEGIN
                                 TESTFIELD("No.");
                                 IF PAGE.RUNMODAL(0,FromRtngHeader) = ACTION::LookupOK THEN
                                   CopyRouting.CopyRouting(FromRtngHeader."No.",'',Rec,'');
                               END;
                                }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1906688806;1 ;Action    ;
                      CaptionML=[DEU=Arbeitsplan;
                                 ENU=Routing Sheet];
                      RunObject=Report 99000787;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Arbeitsplans an.;
                           ENU=Specifies the routing number.];
                SourceExpr="No.";
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung f�r den Arbeitsplankopf an.;
                           ENU=Specifies a description for the routing header.];
                SourceExpr=Description }

    { 19  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, in welcher Reihenfolge Arbeitsg�nge im Arbeitsplan ausgef�hrt werden.;
                           ENU=Specifies in which order operations in the routing are performed.];
                SourceExpr=Type }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Status dieses Arbeitsplans an.;
                           ENU=Specifies the status of this routing.];
                SourceExpr=Status }

    { 13  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Suchbegriff an.;
                           ENU=Specifies a search description.];
                SourceExpr="Search Description" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummernserie an, die Sie zum Erstellen einer neuen Version dieses Arbeitsplans verwenden m�chten.;
                           ENU=Specifies the number series you want to use to create a new version of this routing.];
                SourceExpr="Version Nos." }

    { 21  ;2   ;Field     ;
                CaptionML=[DEU=Aktive Version;
                           ENU=Active Version];
                SourceExpr=ActiveVersionCode;
                Editable=FALSE;
                OnLookup=VAR
                           RtngVersion@1002 : Record 99000786;
                         BEGIN
                           RtngVersion.SETRANGE("Routing No.","No.");
                           RtngVersion.SETRANGE("Version Code",ActiveVersionCode);
                           PAGE.RUNMODAL(PAGE::"Routing Version",RtngVersion);
                           ActiveVersionCode := VersionMgt.GetRtngVersion("No.",WORKDATE,TRUE);
                         END;
                          }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wann die Arbeitsplankarte zuletzt ge�ndert wurde.;
                           ENU=Specifies when the routing card was last modified.];
                SourceExpr="Last Date Modified";
                OnValidate=BEGIN
                             LastDateModifiedOnAfterValidat;
                           END;
                            }

    { 7   ;1   ;Part      ;
                Name=RoutingLine;
                SubPageLink=Routing No.=FIELD(No.),
                            Version Code=CONST();
                PagePartID=Page99000765 }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      VersionMgt@1000 : Codeunit 99000756;
      CopyRouting@1001 : Codeunit 99000753;
      ActiveVersionCode@1002 : Code[20];

    LOCAL PROCEDURE LastDateModifiedOnAfterValidat@19040593();
    BEGIN
      CurrPage.UPDATE;
    END;

    BEGIN
    END.
  }
}

