OBJECT Table 9007 User Group Plan
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[DEU=Benutzergruppenplan;
               ENU=User Group Plan];
  }
  FIELDS
  {
    { 1   ;   ;Plan ID             ;GUID          ;TableRelation=Plan."Plan ID";
                                                   CaptionML=[DEU=Plan-ID;
                                                              ENU=Plan ID] }
    { 2   ;   ;User Group Code     ;Code20        ;TableRelation="User Group".Code;
                                                   CaptionML=[DEU=Benutzergruppencode;
                                                              ENU=User Group Code] }
    { 10  ;   ;Plan Name           ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Plan.Name WHERE (Plan ID=FIELD(Plan ID)));
                                                   CaptionML=[DEU=Planname;
                                                              ENU=Plan Name] }
    { 11  ;   ;User Group Name     ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("User Group".Name WHERE (Code=FIELD(User Group Code)));
                                                   CaptionML=[DEU=Name der Benutzergruppe;
                                                              ENU=User Group Name] }
  }
  KEYS
  {
    {    ;Plan ID,User Group Code                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

