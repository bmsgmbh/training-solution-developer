OBJECT Page 780 Certificates of Supply
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Gelangensbest�tigungen;
               ENU=Certificates of Supply];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table780;
    PageType=List;
    OnOpenPage=BEGIN
                 IF GETFILTERS = '' THEN
                   SETFILTER(Status,'<>%1',Status::"Not Applicable")
                 ELSE
                   InitRecord("Document Type","Document No.")
               END;

    ActionList=ACTIONS
    {
      { 13      ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 14      ;1   ;Action    ;
                      Name=PrintCertificateofSupply;
                      CaptionML=[DEU=Gelangensbest�tigung drucken;
                                 ENU=Print Certificate of Supply];
                      Promoted=Yes;
                      Image=PrintReport;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 CertificateOfSupply@1000 : Record 780;
                               BEGIN
                                 IF NOT ISEMPTY THEN BEGIN
                                   CertificateOfSupply.COPY(Rec);
                                   CertificateOfSupply.SETRANGE("Document Type","Document Type");
                                   CertificateOfSupply.SETRANGE("Document No.","Document No.");
                                 END;
                                 CertificateOfSupply.Print;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des gebuchten Belegs an, f�r den die Gelangensbest�tigung gilt.;
                           ENU=Specifies the type of the posted document to which the certificate of supply applies.];
                SourceExpr="Document Type";
                Editable=False }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Belegnummer des gebuchten Warenausgangsbelegs an, der der Gelangensbest�tigung zugeordnet ist.;
                           ENU=Specifies the document number of the posted shipment document associated with the certificate of supply.];
                SourceExpr="Document No.";
                Editable=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Status f�r Belege an, f�r die eine unterzeichnete Gelangensbest�tigung vom Debitor erforderlich ist.;
                           ENU=Specifies the status for documents where you must receive a signed certificate of supply from the customer.];
                SourceExpr=Status }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Best�tigung an.;
                           ENU=Specifies the number of the certificate.];
                SourceExpr="No." }

    { 7   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Eingangsdatum der unterzeichneten Gelangensbest�tigung an.;
                           ENU=Specifies the receipt date of the signed certificate of supply.];
                SourceExpr="Receipt Date" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Gelangensbest�tigung gedruckt und an den Debitor gesendet wurde.;
                           ENU=Specifies whether the certificate of supply has been printed and sent to the customer.];
                SourceExpr=Printed;
                Editable=FALSE }

    { 9   ;2   ;Field     ;
                Name=Customer/Vendor Name;
                ToolTipML=[DEU=Gibt den Namen des Debitors oder Kreditors an.;
                           ENU=Specifies the name of the customer or vendor.];
                SourceExpr="Customer/Vendor Name" }

    { 11  ;2   ;Field     ;
                Name=Shipment Date;
                ToolTipML=[DEU=Gibt das Datum an, an dem der gebuchte Warenausgang geliefert oder gebucht wurde.;
                           ENU=Specifies the date that the posted shipment was shipped or posted.];
                SourceExpr="Shipment/Posting Date" }

    { 12  ;2   ;Field     ;
                Name=Shipment Country;
                ToolTipML=[DEU=Gibt den L�nder-/Regionscode der Adresse an.;
                           ENU=Specifies the country/region code of the address.];
                SourceExpr="Ship-to Country/Region Code" }

    { 15  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Debitors oder Kreditors an.;
                           ENU=Specifies the number of the customer or vendor.];
                SourceExpr="Customer/Vendor No.";
                Editable=False }

    { 10  ;2   ;Field     ;
                Name=Shipment Method;
                ToolTipML=[DEU=Gibt den Code f�r die Lieferbedingung an. Der Code wird aus dem gebuchten Warenausgangsbeleg kopiert.;
                           ENU=Specifies the code for the shipment method. The code is copied from the posted shipment document.];
                SourceExpr="Shipment Method Code" }

    { 16  ;2   ;Field     ;
                SourceExpr="Vehicle Registration No." }

  }
  CODE
  {

    BEGIN
    END.
  }
}

