OBJECT Table 843 Cash Flow Setup
{
  OBJECT-PROPERTIES
  {
    Date=28.04.17;
    Time=12:00:00;
    Version List=NAVW110.00.00.16585;
  }
  PROPERTIES
  {
    Permissions=TableData 1261=rimd;
    CaptionML=[DEU=Cashfloweinrichtung;
               ENU=Cash Flow Setup];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[DEU=Prim�rschl�ssel;
                                                              ENU=Primary Key] }
    { 2   ;   ;Cash Flow Forecast No. Series;Code10;
                                                   TableRelation="No. Series";
                                                   CaptionML=[DEU=Nummernserien f�r Cashflowplanung;
                                                              ENU=Cash Flow Forecast No. Series] }
    { 3   ;   ;Receivables CF Account No.;Code20  ;TableRelation="Cash Flow Account";
                                                   OnValidate=BEGIN
                                                                CheckAccType("Receivables CF Account No.");
                                                              END;

                                                   CaptionML=[DEU=Debitoren - Cashflowkontonr.;
                                                              ENU=Receivables CF Account No.] }
    { 4   ;   ;Payables CF Account No.;Code20     ;TableRelation="Cash Flow Account";
                                                   OnValidate=BEGIN
                                                                CheckAccType("Payables CF Account No.");
                                                              END;

                                                   CaptionML=[DEU=Kreditoren - Cashflowkontonr.;
                                                              ENU=Payables CF Account No.] }
    { 5   ;   ;Sales Order CF Account No.;Code20  ;TableRelation="Cash Flow Account";
                                                   OnValidate=BEGIN
                                                                CheckAccType("Sales Order CF Account No.");
                                                              END;

                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[DEU=Auftrag - Cashflowkontonr.;
                                                              ENU=Sales Order CF Account No.] }
    { 6   ;   ;Purch. Order CF Account No.;Code20 ;TableRelation="Cash Flow Account";
                                                   OnValidate=BEGIN
                                                                CheckAccType("Purch. Order CF Account No.");
                                                              END;

                                                   AccessByPermission=TableData 120=R;
                                                   CaptionML=[DEU=Bestellung - Cashflowkontonr.;
                                                              ENU=Purch. Order CF Account No.] }
    { 8   ;   ;FA Budget CF Account No.;Code20    ;TableRelation="Cash Flow Account";
                                                   OnValidate=BEGIN
                                                                CheckAccType("FA Budget CF Account No.");
                                                              END;

                                                   AccessByPermission=TableData 5600=R;
                                                   CaptionML=[DEU=Anlagenbudget - Cashflowkontonr.;
                                                              ENU=FA Budget CF Account No.] }
    { 9   ;   ;FA Disposal CF Account No.;Code20  ;TableRelation="Cash Flow Account";
                                                   OnValidate=BEGIN
                                                                CheckAccType("FA Disposal CF Account No.");
                                                              END;

                                                   AccessByPermission=TableData 5600=R;
                                                   CaptionML=[DEU=Anlagenabgang - Cashflowkontonr.;
                                                              ENU=FA Disposal CF Account No.] }
    { 10  ;   ;Service CF Account No.;Code20      ;TableRelation="Cash Flow Account";
                                                   OnValidate=BEGIN
                                                                CheckAccType("Service CF Account No.");
                                                              END;

                                                   AccessByPermission=TableData 5900=R;
                                                   CaptionML=[DEU=Service - Cashflowkontonr.;
                                                              ENU=Service CF Account No.] }
    { 11  ;   ;CF No. on Chart in Role Center;Code20;
                                                   OnValidate=BEGIN
                                                                IF NOT ConfirmedChartRoleCenterCFNo("CF No. on Chart in Role Center") THEN
                                                                  "CF No. on Chart in Role Center" := xRec."CF No. on Chart in Role Center";
                                                              END;

                                                   CaptionML=[DEU=Cashflownr. in Diagramm im Rollencenter;
                                                              ENU=CF No. on Chart in Role Center] }
    { 12  ;   ;Job CF Account No.  ;Code20        ;TableRelation="Cash Flow Account";
                                                   OnValidate=BEGIN
                                                                CheckAccType("Job CF Account No.");
                                                              END;

                                                   AccessByPermission=TableData 1003=R;
                                                   CaptionML=[DEU=Projekt - Cashflowkontonr.;
                                                              ENU=Job CF Account No.] }
    { 13  ;   ;Automatic Update Frequency;Option  ;OnValidate=VAR
                                                                CashFlowManagement@1000 : Codeunit 841;
                                                              BEGIN
                                                                IF "Automatic Update Frequency" = xRec."Automatic Update Frequency" THEN
                                                                  EXIT;

                                                                CashFlowManagement.DeleteJobQueueEntries;
                                                                CashFlowManagement.CreateAndStartJobQueueEntry("Automatic Update Frequency");
                                                              END;

                                                   CaptionML=[DEU=H�ufigkeit f�r automatische Aktualisierungen;
                                                              ENU=Automatic Update Frequency];
                                                   OptionCaptionML=[DEU=Nie,T�glich,W�chentlich;
                                                                    ENU=Never,Daily,Weekly];
                                                   OptionString=Never,Daily,Weekly }
    { 14  ;   ;Tax CF Account No.  ;Code20        ;TableRelation="Cash Flow Account";
                                                   OnValidate=BEGIN
                                                                CheckAccType("Tax CF Account No.");
                                                              END;

                                                   AccessByPermission=TableData 254=R;
                                                   CaptionML=[DEU=Steuer - Cashflowkontonr.;
                                                              ENU=Tax CF Account No.] }
    { 19  ;   ;Taxable Period      ;Option        ;InitValue=Quarterly;
                                                   CaptionML=[DEU=Steuerperiode;
                                                              ENU=Taxable Period];
                                                   OptionCaptionML=[DEU=Monatlich,Viertelj�hrlich,Buchhaltungsperiode,J�hrlich;
                                                                    ENU=Monthly,Quarterly,Accounting Period,Yearly];
                                                   OptionString=Monthly,Quarterly,Accounting Period,Yearly }
    { 20  ;   ;Tax Payment Window  ;DateFormula   ;CaptionML=[DEU=Steuerzahlungsfenster;
                                                              ENU=Tax Payment Window] }
    { 21  ;   ;Tax Bal. Account Type;Option       ;OnValidate=BEGIN
                                                                EmptyTaxBalAccountIfTypeChanged(xRec."Tax Bal. Account Type");
                                                              END;

                                                   CaptionML=[DEU=Steuergegenkontoart;
                                                              ENU=Tax Bal. Account Type];
                                                   OptionCaptionML=[DEU=" ,Kreditor,Sachkonto";
                                                                    ENU=" ,Vendor,G/L Account"];
                                                   OptionString=[ ,Vendor,G/L Account] }
    { 22  ;   ;Tax Bal. Account No.;Code20        ;TableRelation=IF (Tax Bal. Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Tax Bal. Account Type=CONST(Vendor)) Vendor;
                                                   CaptionML=[DEU=Steuergegenkontonr.;
                                                              ENU=Tax Bal. Account No.] }
    { 23  ;   ;API Key             ;Text250       ;OnValidate=BEGIN
                                                                IF NOT ISNULLGUID("Service Pass API Key ID") THEN
                                                                  EnableEncryption;
                                                                SaveUserDefinedAPIKey("API Key");
                                                              END;

                                                   CaptionML=[DEU=API-Schl�ssel;
                                                              ENU=API Key] }
    { 24  ;   ;API URL             ;Text250       ;CaptionML=[DEU=API-URL;
                                                              ENU=API URL] }
    { 25  ;   ;Variance %          ;Integer       ;InitValue=35;
                                                   CaptionML=[DEU=Abweichung %;
                                                              ENU=Variance %];
                                                   MinValue=1;
                                                   MaxValue=100 }
    { 26  ;   ;Historical Periods  ;Integer       ;InitValue=24;
                                                   CaptionML=[DEU=Historische Perioden;
                                                              ENU=Historical Periods];
                                                   MinValue=5;
                                                   MaxValue=24 }
    { 27  ;   ;Horizon             ;Integer       ;InitValue=4;
                                                   CaptionML=[DEU=Horizont;
                                                              ENU=Horizon];
                                                   MinValue=3;
                                                   MaxValue=24 }
    { 28  ;   ;Period Type         ;Option        ;InitValue=Month;
                                                   CaptionML=[DEU=Periodentyp;
                                                              ENU=Period Type];
                                                   OptionCaptionML=[DEU=Tag,Woche,Monat,Quartal,Jahr;
                                                                    ENU=Day,Week,Month,Quarter,Year];
                                                   OptionString=Day,Week,Month,Quarter,Year }
    { 29  ;   ;TimeOut             ;Integer       ;InitValue=120;
                                                   CaptionML=[DEU=TimeOut;
                                                              ENU=TimeOut];
                                                   MinValue=1 }
    { 30  ;   ;Service Pass API Key ID;GUID       ;TableRelation="Service Password".Key;
                                                   CaptionML=[DEU=Dienstkennwort-ID f�r API-Schl�ssel;
                                                              ENU=Service Pass API Key ID];
                                                   Description=The Key for retrieving the API Key from the Service Password table. }
    { 31  ;   ;Cortana Intelligence Enabled;Boolean;
                                                   InitValue=No;
                                                   CaptionML=[DEU=Cortana Intelligence aktiviert;
                                                              ENU=Cortana Intelligence Enabled] }
    { 32  ;   ;Show Cortana Notification;Boolean  ;InitValue=Yes;
                                                   CaptionML=[DEU=Cortana-Benachrichtigung anzeigen;
                                                              ENU=Show Cortana Notification] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1000 : TextConst '@@@=Cash Flow <No.> <Description> is shown in the chart on the Role Center.;DEU=Im Diagramm im Rollencenter wird die Cashflowplanung ''%1 %2'' angezeigt. Soll stattdessen diese Cashflowplanung angezeigt werden?;ENU=Cash Flow Forecast %1 %2 is shown in the chart on the Role Center. Do you want to show this Cash Flow Forecast instead?';
      AzureKeyVaultManagement@1001 : Codeunit 2200;

    LOCAL PROCEDURE CheckAccType@1(Code@1000 : Code[20]);
    VAR
      CFAccount@1001 : Record 841;
    BEGIN
      IF Code <> '' THEN BEGIN
        CFAccount.GET(Code);
        CFAccount.TESTFIELD("Account Type",CFAccount."Account Type"::Entry);
      END;
    END;

    PROCEDURE SetChartRoleCenterCFNo@11(CashFlowNo@1000 : Code[20]);
    BEGIN
      GET;
      "CF No. on Chart in Role Center" := CashFlowNo;
      MODIFY;
    END;

    PROCEDURE GetChartRoleCenterCFNo@2() : Code[20];
    BEGIN
      GET;
      EXIT("CF No. on Chart in Role Center");
    END;

    LOCAL PROCEDURE ConfirmedChartRoleCenterCFNo@3(NewCashFlowNo@1000 : Code[20]) : Boolean;
    VAR
      CashFlowForecast@1001 : Record 840;
    BEGIN
      IF NewCashFlowNo = '' THEN
        EXIT(TRUE);

      IF NOT (xRec."CF No. on Chart in Role Center" IN ['',NewCashFlowNo]) THEN BEGIN
        CashFlowForecast.GET(xRec."CF No. on Chart in Role Center");
        EXIT(CONFIRM(STRSUBSTNO(Text001,CashFlowForecast."No.",CashFlowForecast.Description),TRUE));
      END;
      EXIT(TRUE);
    END;

    PROCEDURE GetTaxPaymentDueDate@4(ReferenceDate@1000 : Date) : Date;
    VAR
      EndOfTaxPeriod@1002 : Date;
    BEGIN
      GET;
      EndOfTaxPeriod := CalculateTaxableDate(ReferenceDate,TRUE);
      EXIT(CALCDATE("Tax Payment Window",EndOfTaxPeriod));
    END;

    PROCEDURE GetTaxPeriodStartEndDates@8(TaxDueDate@1000 : Date;VAR StartDate@1001 : Date;VAR EndDate@1002 : Date);
    BEGIN
      GET;
      EndDate := GetTaxPeriodEndDate(TaxDueDate);
      StartDate := CalculateTaxableDate(EndDate,FALSE);
    END;

    PROCEDURE GetTaxPaymentStartDate@5(TaxDueDate@1000 : Date) : Date;
    BEGIN
      GET;
      EXIT(CALCDATE('<1D>',GetTaxPeriodEndDate(TaxDueDate)));
    END;

    LOCAL PROCEDURE GetTaxPeriodEndDate@7(TaxDueDate@1000 : Date) : Date;
    VAR
      ReverseDateFormula@1001 : DateFormula;
    BEGIN
      GET;
      EVALUATE(ReverseDateFormula,ReverseDateFormulaAsText);
      EXIT(CALCDATE(ReverseDateFormula,TaxDueDate));
    END;

    PROCEDURE GetCurrentPeriodStartDate@12() : Date;
    BEGIN
      GET;
      EXIT(CalculateTaxableDate(WORKDATE,FALSE));
    END;

    PROCEDURE UpdateTaxPaymentInfo@29(NewTaxablePeriod@1002 : Option;NewPaymentWindow@1000 : DateFormula;NewTaxBalAccountType@1004 : Option;NewTaxBalAccountNum@1001 : Code[20]);
    VAR
      Modified@1003 : Boolean;
    BEGIN
      GET;
      IF "Taxable Period" <> NewTaxablePeriod THEN BEGIN
        "Taxable Period" := NewTaxablePeriod;
        Modified := TRUE;
      END;

      IF "Tax Payment Window" <> NewPaymentWindow THEN BEGIN
        "Tax Payment Window" := NewPaymentWindow;
        Modified := TRUE;
      END;

      IF "Tax Bal. Account Type" <> NewTaxBalAccountType THEN BEGIN
        "Tax Bal. Account Type" := NewTaxBalAccountType;
        Modified := TRUE;
      END;

      IF "Tax Bal. Account No." <> NewTaxBalAccountNum THEN BEGIN
        "Tax Bal. Account No." := NewTaxBalAccountNum;
        Modified := TRUE;
      END;

      IF Modified THEN
        MODIFY;
    END;

    LOCAL PROCEDURE CalculateTaxableDate@6(ReferenceDate@1000 : Date;FindLast@1001 : Boolean) Result : Date;
    VAR
      AccountingPeriod@1003 : Record 50;
    BEGIN
      CASE "Taxable Period" OF
        "Taxable Period"::Monthly:
          IF FindLast THEN
            Result := CALCDATE('<CM>',ReferenceDate)
          ELSE
            Result := CALCDATE('<-CM>',ReferenceDate);
        "Taxable Period"::Quarterly:
          IF FindLast THEN
            Result := CALCDATE('<CQ>',ReferenceDate)
          ELSE
            Result := CALCDATE('<-CQ>',ReferenceDate);
        "Taxable Period"::"Accounting Period":
          IF FindLast THEN BEGIN
            // The end of the current accounting period is the start of the next acc. period - 1 day
            AccountingPeriod.SETFILTER("Starting Date",'>%1',ReferenceDate);
            AccountingPeriod.FINDFIRST;
            Result := AccountingPeriod."Starting Date" - 1;
          END ELSE BEGIN
            // The end of the current accounting period is the start of the next acc. period - 1 day
            AccountingPeriod.SETFILTER("Starting Date",'<=%1',ReferenceDate);
            AccountingPeriod.FINDFIRST;
            Result := AccountingPeriod."Starting Date";
          END;
        "Taxable Period"::Yearly:
          IF FindLast THEN
            Result := CALCDATE('<CY>',ReferenceDate)
          ELSE
            Result := CALCDATE('<-CY>',ReferenceDate);
      END;
    END;

    LOCAL PROCEDURE ReverseDateFormulaAsText@15() Result : Text;
    VAR
      TempChar@1000 : Char;
    BEGIN
      Result := FORMAT("Tax Payment Window");
      IF Result = '' THEN
        EXIT('');

      IF NOT (COPYSTR(Result,1,1) IN ['+','-']) THEN
        Result := '+' + Result;

      TempChar := '#';
      Result := ReplaceCharInString(Result,'+',TempChar);
      Result := ReplaceCharInString(Result,'-','+');
      Result := ReplaceCharInString(Result,TempChar,'-');
    END;

    LOCAL PROCEDURE ReplaceCharInString@16(StringToReplace@1000 : Text;OldChar@1001 : Char;NewChar@1002 : Char) Result : Text;
    VAR
      Index@1005 : Integer;
      FirstPart@1003 : Text;
      LastPart@1004 : Text;
    BEGIN
      Index := STRPOS(StringToReplace,FORMAT(OldChar));
      Result := StringToReplace;
      WHILE Index > 0 DO BEGIN
        IF Index > 1 THEN
          FirstPart := COPYSTR(Result,1,Index - 1);
        IF Index < STRLEN(Result) THEN
          LastPart := COPYSTR(Result,Index + 1);
        Result := FirstPart + FORMAT(NewChar) + LastPart;
        Index := STRPOS(Result,FORMAT(OldChar));
      END;
    END;

    PROCEDURE HasValidTaxAccountInfo@9() : Boolean;
    BEGIN
      EXIT("Tax Bal. Account Type" <> "Tax Bal. Account Type"::" ");
    END;

    PROCEDURE EmptyTaxBalAccountIfTypeChanged@10(OldTypeValue@1000 : Option);
    BEGIN
      IF "Tax Bal. Account Type" <> OldTypeValue THEN
        "Tax Bal. Account No." := '';
    END;

    PROCEDURE SaveUserDefinedAPIKey@14(APIKeyValue@1000 : Text[250]);
    VAR
      ServicePassword@1002 : Record 1261;
    BEGIN
      IF ISNULLGUID("Service Pass API Key ID") OR NOT ServicePassword.GET("Service Pass API Key ID") THEN BEGIN
        ServicePassword.SavePassword(APIKeyValue);
        ServicePassword.INSERT(TRUE);
        "Service Pass API Key ID" := ServicePassword.Key;
      END ELSE BEGIN
        ServicePassword.SavePassword(APIKeyValue);
        ServicePassword.MODIFY;
      END;
    END;

    PROCEDURE GetMLCredentials@13(VAR APIURL@1003 : Text[250];VAR APIKey@1002 : Text[200];VAR LimitValue@1001 : Decimal) : Boolean;
    VAR
      ServicePassword@1000 : Record 1261;
      PermissionManager@1004 : Codeunit 9002;
    BEGIN
      // user-defined credentials
      IF IsAPIUserDefined THEN BEGIN
        ServicePassword.GET("Service Pass API Key ID");
        APIKey := COPYSTR(ServicePassword.GetPassword,1,200);
        IF (APIKey = '') OR ("API URL" = '') THEN
          EXIT(FALSE);
        APIURL := "API URL";
        EXIT(TRUE);
      END;

      // if credentials not user-defined retrieve it from Azure Key Vault
      IF PermissionManager.SoftwareAsAService THEN
        EXIT(RetrieveSaaSMLCredentials(APIURL,APIKey,LimitValue));
    END;

    LOCAL PROCEDURE RetrieveSaaSMLCredentials@55(VAR APIURL@1003 : Text[250];VAR APIKey@1002 : Text[200];VAR LimitValue@1001 : Decimal) : Boolean;
    BEGIN
      IF NOT AzureKeyVaultManagement.GetMachineLearningCredentials(APIURL,APIKey,LimitValue) THEN
        EXIT(FALSE);
      APIURL := APIURL + '/execute?api-version=2.0&details=true';
      EXIT(TRUE);
    END;

    LOCAL PROCEDURE EnableEncryption@56();
    VAR
      EncryptionManagement@1000 : Codeunit 1266;
    BEGIN
      IF NOT EncryptionManagement.IsEncryptionEnabled THEN
        EncryptionManagement.EnableEncryption;
    END;

    PROCEDURE GetUserDefinedAPIKey@18() : Text[200];
    VAR
      ServicePassword@1000 : Record 1261;
    BEGIN
      // user-defined credentials
      IF NOT ISNULLGUID("Service Pass API Key ID") THEN BEGIN
        ServicePassword.GET("Service Pass API Key ID");
        EXIT(COPYSTR(ServicePassword.GetPassword,1,200));
      END;
    END;

    PROCEDURE IsAPIUserDefined@99() : Boolean;
    BEGIN
      EXIT(NOT (ISNULLGUID("Service Pass API Key ID") OR ("API URL" = '')));
    END;

    BEGIN
    END.
  }
}

