OBJECT Page 9190 Delete Profile Configuration
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Profilkonfiguration l�schen;
               ENU=Delete Profile Configuration];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table2000000074;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1106000000;1;Group  ;
                GroupType=Repeater }

    { 1106000001;2;Field  ;
                CaptionML=[DEU=Profil-ID;
                           ENU=Profile ID];
                ToolTipML=[DEU=Gibt das Profil an, f�r das die Anpassung erstellt wurde.;
                           ENU=Specifies the profile for which the customization has been created.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Profile ID" }

    { 1106000003;2;Field  ;
                CaptionML=[DEU=Seiten-ID;
                           ENU=Page ID];
                ToolTipML=[DEU=Gibt die Nummer des Seitenobjekts an, das konfiguriert wurde.;
                           ENU=Specifies the number of the page object that has been configured.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Page ID" }

    { 1106000005;2;Field  ;
                CaptionML=[DEU=Beschreibung;
                           ENU=Description];
                ToolTipML=[DEU=Gibt eine Beschreibung der Anpassung an.;
                           ENU=Specifies a description of the customization.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 1106000007;2;Field  ;
                CaptionML=[DEU=Datum;
                           ENU=Date];
                ToolTipML=[DEU=Gibt das Datum der Anpassung an.;
                           ENU=Specifies the date of the customization.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Date }

    { 1106000009;2;Field  ;
                CaptionML=[DEU=Zeit;
                           ENU=Time];
                ToolTipML=[DEU=Gibt einen Zeitstempel f�r die Anpassung an.;
                           ENU=Specifies a timestamp for the customization.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Time }

  }
  CODE
  {

    BEGIN
    END.
  }
}

