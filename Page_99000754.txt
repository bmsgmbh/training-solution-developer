OBJECT Page 99000754 Work Center Card
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Arbeitsplatzgruppenkarte;
               ENU=Work Center Card];
    SourceTable=Table99000754;
    PageType=Card;
    OnInit=BEGIN
             FromProductionBinCodeEnable := TRUE;
             ToProductionBinCodeEnable := TRUE;
             OpenShopFloorBinCodeEnable := TRUE;
           END;

    OnOpenPage=BEGIN
                 OnActivateForm;
               END;

    OnAfterGetCurrRecord=BEGIN
                           UpdateEnabled;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 54      ;1   ;ActionGroup;
                      CaptionML=[DEU=Arbeitsp&latzgruppe;
                                 ENU=Wor&k Ctr.];
                      Image=WorkCenter }
      { 45      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=Kapa&zit�tsposten;
                                 ENU=Capacity Ledger E&ntries];
                      RunObject=Page 5832;
                      RunPageView=SORTING(Work Center No.,Work Shift Code,Posting Date);
                      RunPageLink=Work Center No.=FIELD(No.),
                                  Posting Date=FIELD(Date Filter);
                      Image=CapacityLedger }
      { 63      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Anzeigen oder Bearbeiten von Dimensionen, wie etwa eines Bereichs, eines Projekts oder einer Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(99000754),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 46      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 99000784;
                      RunPageView=WHERE(Table Name=CONST(Work Center));
                      RunPageLink=No.=FIELD(No.);
                      Image=ViewComments }
      { 48      ;2   ;Action    ;
                      CaptionML=[DEU=&Auslastung;
                                 ENU=Lo&ad];
                      RunObject=Page 99000887;
                      RunPageLink=No.=FIELD(No.),
                                  Work Shift Filter=FIELD(Work Shift Filter);
                      Promoted=Yes;
                      Image=WorkCenterLoad;
                      PromotedCategory=Process }
      { 47      ;2   ;Separator  }
      { 56      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[DEU=Statistik;
                                 ENU=Statistics];
                      RunObject=Page 99000756;
                      RunPageLink=No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter),
                                  Work Shift Filter=FIELD(Work Shift Filter);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 53      ;1   ;ActionGroup;
                      CaptionML=[DEU=Pla&nung;
                                 ENU=Pla&nning];
                      Image=Planning }
      { 59      ;2   ;Action    ;
                      CaptionML=[DEU=&Kalender;
                                 ENU=&Calendar];
                      RunObject=Page 99000769;
                      Promoted=Yes;
                      Image=MachineCenterCalendar;
                      PromotedCategory=Process }
      { 74      ;2   ;Action    ;
                      CaptionML=[DEU=&Fehlzeiten;
                                 ENU=A&bsence];
                      RunObject=Page 99000772;
                      RunPageView=SORTING(Capacity Type,No.,Date,Starting Time);
                      RunPageLink=Capacity Type=CONST(Work Center),
                                  No.=FIELD(No.),
                                  Date=FIELD(Date Filter);
                      Promoted=Yes;
                      Image=WorkCenterAbsence;
                      PromotedCategory=Process }
      { 36      ;2   ;Action    ;
                      CaptionML=[DEU=&Auftragsvorr�te;
                                 ENU=Ta&sk List];
                      RunObject=Page 99000915;
                      RunPageView=SORTING(Type,No.)
                                  WHERE(Type=CONST(Work Center),
                                        Status=FILTER(..Released),
                                        Routing Status=FILTER(<>Finished));
                      RunPageLink=No.=FIELD(No.);
                      Image=TaskList }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1906187306;1 ;Action    ;
                      CaptionML=[DEU=Fremdbearbeiter - Versandliste;
                                 ENU=Subcontractor - Dispatch List];
                      RunObject=Report 99000789;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Arbeitsplatzgruppe an.;
                           ENU=Specifies the number of the work center.];
                SourceExpr="No.";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Arbeitsplatzgruppe an.;
                           ENU=Specifies the name of the work center.];
                SourceExpr=Name;
                Importance=Promoted }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Abteilungscode an, sofern der Arbeitsplatz oder die zugrunde liegende Arbeitsplatzgruppe einer Abteilung zugeordnet wird.;
                           ENU=Specifies the work center group, if the work center or underlying machine center is assigned to a work center group.];
                SourceExpr="Work Center Group Code";
                Importance=Promoted }

    { 69  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine alternative Arbeitsplatzgruppe an.;
                           ENU=Specifies an alternate work center.];
                SourceExpr="Alternate Work Center" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Suchnamen f�r die Arbeitsplatzgruppe an.;
                           ENU=Specifies the search name for the work center.];
                SourceExpr="Search Name" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Arbeitsplatzgruppe gesperrt ist.;
                           ENU=Specifies whether the work center account is blocked.];
                SourceExpr=Blocked }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wann die Arbeitsplatzgruppenkarte zuletzt ge�ndert wurde.;
                           ENU=Specifies when the work center card was last modified.];
                SourceExpr="Last Date Modified" }

    { 1904784501;1;Group  ;
                CaptionML=[DEU=Buchung;
                           ENU=Posting] }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einkaufspreis der Arbeitsgruppe in einer Einheit an.;
                           ENU=Specifies the direct unit cost of the work center at one unit of measure.];
                SourceExpr="Direct Unit Cost";
                Importance=Promoted }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die indirekten Kosten f�r die Arbeitsplatzgruppe in Prozent an.;
                           ENU=Specifies the indirect costs of the work center, in percent.];
                SourceExpr="Indirect Cost %" }

    { 49  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gemeinkostensatz dieser Arbeitsplatzgruppe an.;
                           ENU=Specifies the overhead rate of this work center.];
                SourceExpr="Overhead Rate" }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einstandspreis in einer Einheit f�r den Arbeitsplatz an.;
                           ENU=Specifies the unit cost at one unit of measure for the machine center.];
                SourceExpr="Unit Cost" }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Einstandspreisberechnung an, die erfolgen soll.;
                           ENU=Specifies the unit cost calculation that is to be made.];
                SourceExpr="Unit Cost Calculation";
                Importance=Promoted }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wo der Einstandspreis definiert wird.;
                           ENU=Specifies where to define the unit costs.];
                SourceExpr="Specific Unit Cost" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Arbeitsplatzgruppe verkn�pft ist.;
                           ENU=Specifies the dimension value code linked to the work center.];
                SourceExpr="Global Dimension 1 Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, mit dem die Arbeitsplatzgruppe verkn�pft ist.;
                           ENU=Specifies the dimension value code that the work center is linked to.];
                SourceExpr="Global Dimension 2 Code" }

    { 50  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Nummer f�r den Kreditor an, der Fremdarbeit f�r diese Arbeitsplatzgruppe liefert.;
                           ENU=Specifies the number of a subcontractor who supplies this work center.];
                SourceExpr="Subcontractor No." }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Methode an, die verwendet werden soll, um die fertig gestellte Menge in dieser Arbeitsplatzgruppe zu berechnen.;
                           ENU=Specifies the method to use to calculate the output quantity at this work center.];
                SourceExpr="Flushing Method";
                Importance=Promoted }

    { 58  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Produktbuchungsgruppe an.;
                           ENU=Specifies a general product posting group.];
                SourceExpr="Gen. Prod. Posting Group";
                Importance=Promoted }

    { 1905773001;1;Group  ;
                CaptionML=[DEU=Planung;
                           ENU=Scheduling] }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Einheit f�r die Produktion in der Arbeitsplatzgruppe an.;
                           ENU=Specifies a unit for production at the work center.];
                SourceExpr="Unit of Measure Code";
                Importance=Promoted }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Kapazit�t der Arbeitsplatzgruppe an.;
                           ENU=Specifies the capacity of the work center.];
                SourceExpr=Capacity;
                Importance=Promoted }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Faktor der Effektivit�t (in Prozent) f�r diese Arbeitsplatzgruppe an.;
                           ENU=Specifies the efficiency factor as a percentage of the work center.];
                SourceExpr=Efficiency }

    { 37  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob der konsolidierte Kalender verwendet wird.;
                           ENU=Specifies whether the consolidated calendar is used.];
                SourceExpr="Consolidated Calendar" }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betriebskalendercode an, auf den sich die Planung dieser Arbeitsplatzgruppe bezieht.;
                           ENU=Specifies the shop calendar code that the planning of this work center refers to.];
                SourceExpr="Shop Calendar Code";
                Importance=Promoted }

    { 60  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Warteschlangenzeit der Arbeitsplatzgruppe an.;
                           ENU=Specifies the queue time of the work center.];
                SourceExpr="Queue Time" }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ma�einheitencode f�r die Warteschlangenzeit an.;
                           ENU=Specifies the queue time unit of measure code.];
                SourceExpr="Queue Time Unit of Meas. Code" }

    { 1907509201;1;Group  ;
                CaptionML=[DEU=Lager;
                           ENU=Warehouse] }

    { 64  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ort an, an dem die Arbeitsplatzgruppe standardm��ig Vorg�nge ausf�hrt.;
                           ENU=Specifies the location where the work center operates by default.];
                SourceExpr="Location Code";
                OnValidate=BEGIN
                             UpdateEnabled;
                           END;
                            }

    { 66  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerplatz an, der als standardm��iger Off. Fert.-Ber.-Lagerpl. an der Arbeitsplatzgruppe fungiert.;
                           ENU=Specifies the bin that functions as the default open shop floor bin at the work center.];
                SourceExpr="Open Shop Floor Bin Code";
                Enabled=OpenShopFloorBinCodeEnable }

    { 68  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerplatz im Fertigungsbereich an, wo f�r die Fertigung kommissionierte Komponenten standardm��ig platziert werden, bevor sie verbraucht werden k�nnen.;
                           ENU=Specifies the bin in the production area where components that are picked for production are placed by default before they can be consumed.];
                SourceExpr="To-Production Bin Code";
                Enabled=ToProductionBinCodeEnable }

    { 72  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerplatz im Fertigungsbereich an, wo fertige Endartikel standardm��ig entnommen werden, wenn der Vorgang Lageraktivit�t umfasst.;
                           ENU=Specifies the bin in the production area where finished end items are taken by default when the process involves warehouse activity.];
                SourceExpr="From-Production Bin Code";
                Enabled=FromProductionBinCodeEnable }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      OpenShopFloorBinCodeEnable@19054478 : Boolean INDATASET;
      ToProductionBinCodeEnable@19078604 : Boolean INDATASET;
      FromProductionBinCodeEnable@19048183 : Boolean INDATASET;

    LOCAL PROCEDURE UpdateEnabled@1();
    VAR
      Location@1000 : Record 14;
    BEGIN
      IF "Location Code" <> '' THEN
        Location.GET("Location Code");

      OpenShopFloorBinCodeEnable := Location."Bin Mandatory";
      ToProductionBinCodeEnable := Location."Bin Mandatory";
      FromProductionBinCodeEnable := Location."Bin Mandatory";
    END;

    LOCAL PROCEDURE OnActivateForm@19002417();
    BEGIN
      UpdateEnabled;
    END;

    BEGIN
    END.
  }
}

