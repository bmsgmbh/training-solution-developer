OBJECT Page 5430 Planning Error Log
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Planungsfehlerprotokoll;
               ENU=Planning Error Log];
    SourceTable=Table5430;
    DataCaptionExpr=Caption;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 2       ;1   ;Action    ;
                      CaptionML=[DEU=&Anzeigen;
                                 ENU=&Show];
                      Promoted=Yes;
                      Image=View;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowError;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Artikelnummer an, die mit diesem Posten verkn�pft ist.;
                           ENU=Specifies the item number associated with this entry.];
                SourceExpr="Item No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des Fehler in diesem Posten an.;
                           ENU=Specifies the description to the error in this entry.];
                SourceExpr="Error Description" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

