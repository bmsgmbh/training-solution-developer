OBJECT Table 7132 Item Budget Name
{
  OBJECT-PROPERTIES
  {
    Date=07.09.12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               ItemBudgetEntry@1000 : Record 7134;
               ItemAnalysisViewBudgetEntry@1001 : Record 7156;
             BEGIN
               ItemBudgetEntry.SETCURRENTKEY("Analysis Area","Budget Name");
               ItemBudgetEntry.SETRANGE("Analysis Area","Analysis Area");
               ItemBudgetEntry.SETRANGE("Budget Name",Name);
               ItemBudgetEntry.DELETEALL(TRUE);

               ItemAnalysisViewBudgetEntry.SETRANGE("Analysis Area","Analysis Area");
               ItemAnalysisViewBudgetEntry.SETRANGE("Budget Name",Name);
               ItemAnalysisViewBudgetEntry.DELETEALL;
             END;

    CaptionML=[DEU=Artikelbudgetname;
               ENU=Item Budget Name];
    LookupPageID=Page7132;
  }
  FIELDS
  {
    { 1   ;   ;Analysis Area       ;Option        ;CaptionML=[DEU=Analysebereich;
                                                              ENU=Analysis Area];
                                                   OptionCaptionML=[DEU=Verkauf,Einkauf;
                                                                    ENU=Sales,Purchase];
                                                   OptionString=Sales,Purchase }
    { 2   ;   ;Name                ;Code10        ;CaptionML=[DEU=Name;
                                                              ENU=Name];
                                                   NotBlank=Yes }
    { 3   ;   ;Description         ;Text80        ;CaptionML=[DEU=Beschreibung;
                                                              ENU=Description] }
    { 4   ;   ;Blocked             ;Boolean       ;CaptionML=[DEU=Gesperrt;
                                                              ENU=Blocked] }
    { 5   ;   ;Budget Dimension 1 Code;Code20     ;TableRelation=Dimension;
                                                   OnValidate=BEGIN
                                                                IF "Budget Dimension 1 Code" <> xRec."Budget Dimension 1 Code" THEN BEGIN
                                                                  IF Dim.CheckIfDimUsed("Budget Dimension 1 Code",17,Name,'',"Analysis Area") THEN
                                                                    ERROR(Text000,Dim.GetCheckDimErr);
                                                                  MODIFY;
                                                                END;
                                                              END;

                                                   CaptionML=[DEU=Budgetdimensionscode 1;
                                                              ENU=Budget Dimension 1 Code] }
    { 6   ;   ;Budget Dimension 2 Code;Code20     ;TableRelation=Dimension;
                                                   OnValidate=BEGIN
                                                                IF "Budget Dimension 2 Code" <> xRec."Budget Dimension 2 Code" THEN BEGIN
                                                                  IF Dim.CheckIfDimUsed("Budget Dimension 2 Code",18,Name,'',"Analysis Area") THEN
                                                                    ERROR(Text000,Dim.GetCheckDimErr);
                                                                  MODIFY;
                                                                END;
                                                              END;

                                                   CaptionML=[DEU=Budgetdimensionscode 2;
                                                              ENU=Budget Dimension 2 Code] }
    { 7   ;   ;Budget Dimension 3 Code;Code20     ;TableRelation=Dimension;
                                                   OnValidate=BEGIN
                                                                IF "Budget Dimension 3 Code" <> xRec."Budget Dimension 3 Code" THEN BEGIN
                                                                  IF Dim.CheckIfDimUsed("Budget Dimension 3 Code",19,Name,'',"Analysis Area") THEN
                                                                    ERROR(Text000,Dim.GetCheckDimErr);
                                                                  MODIFY;
                                                                END;
                                                              END;

                                                   CaptionML=[DEU=Budgetdimensionscode 3;
                                                              ENU=Budget Dimension 3 Code] }
  }
  KEYS
  {
    {    ;Analysis Area,Name                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'DEU=%1\Sie k�nnen in demselben Budget dieselbe Dimension nicht zweimal verwenden.;ENU=%1\You cannot use the same dimension twice in the same budget.';
      Dim@1002 : Record 348;

    BEGIN
    END.
  }
}

