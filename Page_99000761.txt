OBJECT Page 99000761 Machine Center List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Arbeitsplatz�bersicht;
               ENU=Machine Center List];
    SourceTable=Table99000758;
    PageType=List;
    CardPageID=Machine Center Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 18      ;1   ;ActionGroup;
                      CaptionML=[DEU=Arbeits&platz;
                                 ENU=&Mach. Ctr.];
                      Image=MachineCenter }
      { 11      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=Kapa&zit�tsposten;
                                 ENU=Capacity Ledger E&ntries];
                      RunObject=Page 5832;
                      RunPageView=SORTING(Type,No.,Work Shift Code,Item No.,Posting Date);
                      RunPageLink=Type=CONST(Machine Center),
                                  No.=FIELD(No.),
                                  Posting Date=FIELD(Date Filter);
                      Image=CapacityLedger }
      { 19      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 99000784;
                      RunPageLink=Table Name=CONST(Machine Center),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 13      ;2   ;Action    ;
                      CaptionML=[DEU=&Auslastung;
                                 ENU=Lo&ad];
                      RunObject=Page 99000889;
                      RunPageView=SORTING(No.);
                      RunPageLink=No.=FIELD(No.);
                      Image=WorkCenterLoad }
      { 10      ;2   ;Separator  }
      { 9       ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[DEU=Statistik;
                                 ENU=Statistics];
                      RunObject=Page 99000762;
                      RunPageLink=No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter),
                                  Work Shift Filter=FIELD(Work Shift Filter);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 17      ;1   ;ActionGroup;
                      CaptionML=[DEU=Pla&nung;
                                 ENU=Pla&nning];
                      Image=Planning }
      { 22      ;2   ;Action    ;
                      CaptionML=[DEU=&Kalender;
                                 ENU=&Calendar];
                      RunObject=Page 99000770;
                      Promoted=Yes;
                      Image=MachineCenterCalendar;
                      PromotedCategory=Process }
      { 30      ;2   ;Action    ;
                      CaptionML=[DEU=&Fehlzeiten;
                                 ENU=A&bsence];
                      RunObject=Page 99000772;
                      RunPageLink=Capacity Type=CONST(Machine Center),
                                  No.=FIELD(No.),
                                  Date=FIELD(Date Filter);
                      Promoted=Yes;
                      Image=WorkCenterAbsence;
                      PromotedCategory=Process }
      { 8       ;2   ;Action    ;
                      CaptionML=[DEU=&Auftragsvorr�te;
                                 ENU=Ta&sk List];
                      RunObject=Page 99000916;
                      RunPageView=SORTING(Type,No.)
                                  WHERE(Type=CONST(Machine Center),
                                        Status=FILTER(..Released),
                                        Routing Status=FILTER(<>Finished));
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=TaskList;
                      PromotedCategory=Process }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1907112806;1 ;Action    ;
                      CaptionML=[DEU=Arbeitsplatz�bersicht;
                                 ENU=Machine Center List];
                      RunObject=Report 99000760;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1907150206;1 ;Action    ;
                      CaptionML=[DEU=Arbeitsplatzauslastung;
                                 ENU=Machine Center Load];
                      RunObject=Report 99000784;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1906511306;1 ;Action    ;
                      CaptionML=[DEU=Arbeitsplatzauslast./Diagramm;
                                 ENU=Machine Center Load/Bar];
                      RunObject=Report 99000786;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Nummer der Arbeitsplatzgruppe fest.;
                           ENU=Specifies the number of the machine center.];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Namen f�r den Arbeitsplatz an.;
                           ENU=Specifies a name for the machine center.];
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Arbeitsplatzgruppe an, der dieser Arbeitsplatz zugeordnet werden soll.;
                           ENU=Specifies the number of the work center to assign this machine center to.];
                SourceExpr="Work Center No." }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Kapazit�t der Arbeitsplatzgruppe fest.;
                           ENU=Specifies the capacity of the machine center.];
                SourceExpr=Capacity }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Faktor der Effektivit�t (in Prozent) f�r diesen Arbeitsplatz an.;
                           ENU=Specifies the efficiency factor as a percentage of the machine center.];
                SourceExpr=Efficiency }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die minimale Effektivit�t dieses Arbeitsplatzes an.;
                           ENU=Specifies the minimum efficiency of this machine center.];
                SourceExpr="Minimum Efficiency";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die maximale Effektivit�t dieses Arbeitsplatzes an.;
                           ENU=Specifies the maximum efficiency of this machine center.];
                SourceExpr="Maximum Efficiency";
                Visible=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viel verf�gbare Kapazit�t gleichzeitig f�r einen Arbeitsgang an diesem Arbeitsplatz geplant werden muss.;
                           ENU=Specifies how much available capacity must be concurrently planned for one operation at this machine center.];
                SourceExpr="Concurrent Capacities";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Suchnamen f�r den Arbeitsplatz an.;
                           ENU=Specifies the search name for the machine center.];
                SourceExpr="Search Name" }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einkaufspreis des Arbeitsplatzes in einer Einheit an.;
                           ENU=Specifies the direct unit cost of the machine center at one unit of measure.];
                SourceExpr="Direct Unit Cost";
                Visible=FALSE }

    { 25  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die indirekten Kosten f�r den Arbeitsplatz in Prozent an.;
                           ENU=Specifies the indirect costs of the machine center in percent.];
                SourceExpr="Indirect Cost %";
                Visible=FALSE }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einstandspreis in einer Einheit f�r den Arbeitsplatz an.;
                           ENU=Specifies the unit cost at one unit of measure for the machine center.];
                SourceExpr="Unit Cost";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gemeinkostensatz dieses Arbeitsplatzes an.;
                           ENU=Specifies the overhead rate of this machine center.];
                SourceExpr="Overhead Rate";
                Visible=FALSE }

    { 1102601000;2;Field  ;
                ToolTipML=[DEU=Gibt an, wann die Arbeitsplatzkarte zuletzt ge�ndert wurde.;
                           ENU=Specifies when the machine center card was last modified.];
                SourceExpr="Last Date Modified";
                Visible=FALSE }

    { 1102601002;2;Field  ;
                ToolTipML=[DEU=Gibt die Methode an, die verwendet werden soll, um die fertig gestellte Menge des Artikels im Fertigungsprozess zu berechnen.;
                           ENU=Specifies the method to use to calculate the output quantity of the item in the production process.];
                SourceExpr="Flushing Method";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

