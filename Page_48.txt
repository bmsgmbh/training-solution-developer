OBJECT Page 48 Sales Orders
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Auftr�ge;
               ENU=Sales Orders];
    SourceTable=Table37;
    SourceTableView=WHERE(Document Type=FILTER(Order));
    DataCaptionFields=No.;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 32      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 33      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Beleg anzeigen;
                                 ENU=Show Order];
                      ToolTipML=[DEU=Zeigen Sie den ausgew�hlten Auftrag an.;
                                 ENU=View the selected sales order.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 42;
                      RunPageLink=Document Type=FIELD(Document Type),
                                  No.=FIELD(Document No.);
                      Image=ViewOrder }
      { 31      ;2   ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[DEU=Reservierungsposten;
                                 ENU=Reservation Entries];
                      Image=ReservationLedger;
                      OnAction=BEGIN
                                 ShowReservationEntries(TRUE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 21  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Einheit an, die f�r diese Verkaufszeile gebucht wird, wie Artikel, Ressource oder Sachkonto.;
                           ENU=Specifies the type of entity that will be posted for this sales line, such as Item, Resource, or G/L Account.];
                SourceExpr=Type }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer eines Sachkontos, eines Artikels, einer Ressource, eines Zu-/Abschlags oder einer Anlage an, je nach der Auswahl im Feld "Art".;
                           ENU=Specifies the number of a general ledger account, an item, a resource, an additional cost or a fixed asset, depending on what you selected in the Type field.];
                ApplicationArea=#All;
                SourceExpr="No." }

    { 25  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Postens an. Die Beschreibung h�ngt von der im Feld "Art" gew�hlten Option ab. Wenn Sie nicht "Leer" ausgew�hlt haben, wird dieses Feld bei einer Eingabe in das Feld "Nr." automatisch ausgef�llt.;
                           ENU=Specifies a description of the entry. The description depends on what you chose in the Type field. If you did not choose Blank, the program will fill in the field when you enter something in the No. field.];
                ApplicationArea=#All;
                SourceExpr=Description }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem sich die Artikel in der Zeile im Lager befinden und zur Kommissionierung zur Verf�gung stehen.;
                           ENU=Specifies the date that the items on the line are in inventory and available to be picked.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Shipment Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Debitors an, an den die Artikel im Auftrag geliefert werden.;
                           ENU=Specifies the number of the customer to whom the items in the sales order will be shipped.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Customer No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Belegnummer an.;
                           ENU=Specifies the document number.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No." }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den W�hrungscode f�r den Betrag in dieser Zeile an.;
                           ENU=Specifies the currency code for the amount on this line.];
                ApplicationArea=#Suite;
                SourceExpr="Currency Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viele Einheiten verkauft werden.;
                           ENU=Specifies how many units are being sold.];
                SourceExpr=Quantity }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der noch nicht gelieferten Einheiten in der Auftragszeile an.;
                           ENU=Specifies how many units on the order line have not yet been shipped.];
                SourceExpr="Outstanding Quantity" }

    { 29  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Einheit an, die zum Bestimmen des Werts im Feld "VK-Preis" in der Verkaufszeile verwendet wird.;
                           ENU=Specifies the unit of measure that is used to determine the value in the Unit Price field on the sales line.];
                SourceExpr="Unit of Measure Code" }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Geh�rt zum Anwendungsbereich "Projekte".;
                           ENU=Belongs to the Job application area.];
                SourceExpr="Work Type Code";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Summe der Betr�ge im Feld "Zeilenbetrag" in den Auftragszeilen an.;
                           ENU=Specifies the sum of amounts in the Line Amount field on the sales order lines.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Preis einer Einheit in der Verkaufszeile an.;
                           ENU=Specifies the price for one unit on the sales line.];
                SourceExpr="Unit Price" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den prozentualen Zeilenrabatt f�r die Artikelmenge in der Zeile an.;
                           ENU=Specifies the line discount percentage that is valid for the item quantity on the line.];
                SourceExpr="Line Discount %" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

