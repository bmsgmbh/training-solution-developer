OBJECT Page 9510 Event Subscriptions
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Ereignisabonnements;
               ENU=Event Subscriptions];
    SourceTable=Table2000000140;
    PageType=List;
    OnAfterGetRecord=VAR
                       AllObj@1001 : Record 2000000038;
                       CodeUnitMetadata@1000 : Record 2000000137;
                     BEGIN
                       IF CodeUnitMetadata.GET("Subscriber Codeunit ID") THEN
                         CodeunitName := CodeUnitMetadata.Name;

                       AllObj.SETRANGE("Object Type","Publisher Object Type");
                       AllObj.SETRANGE("Object ID","Publisher Object ID");
                       IF AllObj.FINDFIRST THEN
                         PublisherName := AllObj."Object Name";
                     END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID der Codeunit an, die die Ereignisabonnentenfunktion enth�lt.;
                           ENU=Specifies the ID of codeunit that contains the event subscriber function.];
                ApplicationArea=#All;
                SourceExpr="Subscriber Codeunit ID" }

    { 12  ;2   ;Field     ;
                Name=CodeunitName;
                CaptionML=[DEU=Codeunit-Name des Abonnenten;
                           ENU=Subscriber Codeunit Name];
                ToolTipML=[DEU=Gibt den Namen der Codeunit an, die die Ereignisabonnentenfunktion enth�lt.;
                           ENU=Specifies the name of the codeunit that contains the event subscriber function.];
                ApplicationArea=#All;
                SourceExpr=CodeunitName }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Ereignisabonnentenfunktion in der Abonnenten-Codeunit an, die das Ereignis abonniert hat.;
                           ENU=Specifies the event subscriber function in the subscriber codeunit that subscribes to the event.];
                ApplicationArea=#All;
                SourceExpr="Subscriber Function" }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Ereignisart an, wobei es sich um Gesch�ft, Integration oder Trigger handeln kann.;
                           ENU=Specifies the event type, which can be Business, Integration, or Trigger.];
                ApplicationArea=#All;
                SourceExpr="Event Type" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Objekts an, das die Ereignisherausgeberfunktion enth�lt, die das Ereignis ver�ffentlicht.;
                           ENU=Specifies the type of object that contains the event publisher function that publishes the event.];
                ApplicationArea=#All;
                SourceExpr="Publisher Object Type" }

    { 7   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Objekts an, das die Ereignisherausgeberfunktion enth�lt, die das Ereignis ver�ffentlicht.;
                           ENU=Specifies the ID of the object that contains the event publisher function that publishes the event.];
                ApplicationArea=#All;
                SourceExpr="Publisher Object ID" }

    { 13  ;2   ;Field     ;
                Name=PublisherName;
                CaptionML=[DEU=Herausgeber-Objektname;
                           ENU=Publisher Object Name];
                ToolTipML=[DEU=Gibt den Namen des Objekts an, das die Ereignisherausgeberfunktion enth�lt, die das Ereignis ver�ffentlicht.;
                           ENU=Specifies the name of the object that contains the event publisher function that publishes the event.];
                ApplicationArea=#All;
                SourceExpr=PublisherName }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Ereignisherausgeberfunktion im Herausgeberobjekt an, das die Ereignisabonnentenfunktion abonniert hat.;
                           ENU=Specifies the name of the event publisher function in the publisher object that the event subscriber function subscribes to.];
                ApplicationArea=#All;
                SourceExpr="Published Function" }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Legt fest, ob das Ereignisabonnement aktiv ist.;
                           ENU=Specifies if the event subscription is active.];
                ApplicationArea=#All;
                SourceExpr=Active }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie oft die Ereignisabonnentenfunktion aufgerufen wurde. Die Ereignisabonnentenfunktion wird aufgerufen, wenn ein ver�ffentlichtes Ereignis in der Anwendung ausgel�st wird.;
                           ENU=Specifies how many times the event subscriber function has been called. The event subscriber function is called when the published event is raised in the application.];
                ApplicationArea=#All;
                SourceExpr="Number of Calls" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Ereignisabonnement an.;
                           ENU=Specifies the event subscription.];
                ApplicationArea=#All;
                SourceExpr="Subscriber Instance" }

    { 15  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt manuelle Ereignisabonnements an, die aktiv sind.;
                           ENU=Specifies manual event subscriptions that are active.];
                ApplicationArea=#All;
                SourceExpr="Active Manual Instances" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Legt das Objekt fest, das das Ereignis ausl�st.;
                           ENU=Specifies the object that triggers the event.];
                ApplicationArea=#All;
                SourceExpr="Originating App Name" }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Fehler an, der f�r das Ereignisabonnement aufgetreten ist.;
                           ENU=Specifies an error that occurred for the event subscription.];
                ApplicationArea=#All;
                SourceExpr="Error Information" }

  }
  CODE
  {
    VAR
      CodeunitName@1000 : Text;
      PublisherName@1001 : Text;

    BEGIN
    END.
  }
}

