OBJECT Page 9552 Document Service Acc. Pwd.
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Kennwort f�r Belegdienstkonto;
               ENU=Document Service Acc. Pwd.];
    PageType=StandardDialog;
    OnQueryClosePage=BEGIN
                       IF CloseAction = ACTION::OK THEN
                         IF PasswordField <> ConfirmPasswordField THEN
                           ERROR(PasswordValidationErr);
                     END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                GroupType=Group;
                InstructionalTextML=[DEU=Geben Sie das Kennwort des Speicherkontos f�r Ihr Onlinedokument ein.;
                                     ENU=Enter the password for your online document storage account.] }

    { 3   ;2   ;Field     ;
                Name=PasswordField;
                ExtendedDatatype=Masked;
                CaptionML=[DEU=Kennwort festlegen;
                           ENU=Set Password];
                ToolTipML=[DEU=Legt das Kennwort f�r Ihr Onlinespeicherkonto fest.;
                           ENU=Specifies the password for your online storage account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PasswordField;
                ShowCaption=Yes }

    { 4   ;2   ;Field     ;
                Name=ConfirmPasswordField;
                ExtendedDatatype=Masked;
                CaptionML=[DEU=Kennwort best�tigen;
                           ENU=Confirm Password];
                ToolTipML=[DEU=Gibt die Wiederholung des Kennworts an.;
                           ENU=Specifies the password repeated.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=ConfirmPasswordField;
                ShowCaption=Yes }

  }
  CODE
  {
    VAR
      PasswordField@1000 : Text[80];
      ConfirmPasswordField@1001 : Text[80];
      PasswordValidationErr@1002 : TextConst 'DEU=Die eingegebenen Kennw�rter stimmen nicht �berein.;ENU=The passwords that you entered do not match.';

    PROCEDURE GetData@1() : Text[80];
    BEGIN
      EXIT(PasswordField);
    END;

    BEGIN
    END.
  }
}

