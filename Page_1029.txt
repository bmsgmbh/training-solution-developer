OBJECT Page 1029 Job Invoices
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Projektrechnungen;
               ENU=Job Invoices];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table1022;
    PageType=List;
    SourceTableTemporary=Yes;
    RefreshOnActivate=Yes;
    OnInit=BEGIN
             ShowDetails := TRUE;
           END;

    OnOpenPage=VAR
                 JobCreateInvoice@1000 : Codeunit 1002;
               BEGIN
                 JobCreateInvoice.FindInvoices(Rec,JobNo,JobTaskNo,JobPlanningLineNo,DetailLevel);
               END;

    ActionList=ACTIONS
    {
      { 12      ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 13      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktionen;
                                 ENU=F&unctions];
                      Image=Action }
      { 14      ;2   ;Action    ;
                      Name=OpenSalesInvoiceCreditMemo;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Verkaufsrechnung/Gutschrift �ffnen;
                                 ENU=Open Sales Invoice/Credit Memo];
                      ToolTipML=[DEU=�ffnet die Verkaufsrechnung oder Verkaufsgutschrift f�r die ausgew�hlte Zeile.;
                                 ENU=Open the sales invoice or sales credit memo for the selected line.];
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=GetSourceDoc;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 JobCreateInvoice@1001 : Codeunit 1002;
                               BEGIN
                                 JobCreateInvoice.OpenSalesInvoice(Rec);
                                 JobCreateInvoice.FindInvoices(Rec,JobNo,JobTaskNo,JobPlanningLineNo,DetailLevel);
                                 IF GET("Job No.","Job Task No.","Job Planning Line No.","Document Type","Document No.","Line No.") THEN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Informationen �ber die Belegart an. Es sind vier Optionen verf�gbar:;
                           ENU=Specifies the information about the type of document. There are four options:];
                ApplicationArea=#Jobs;
                SourceExpr="Document Type" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die dem Beleg zugeordnete Nummer an. Wenn Sie z. B. eine Rechnung erstellt haben, gibt das Feld die Rechnungsnummer an.;
                           ENU=Specifies the number associated with the document. For example, if you have created an invoice, the field Specifies the invoice number.];
                ApplicationArea=#Jobs;
                SourceExpr="Document No." }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die mit dem Beleg verkn�pfte Zeilennummer an. Die Nummern werden fortlaufend erstellt.;
                           ENU=Specifies the line number that is linked to the document. Numbers are created sequentially.];
                ApplicationArea=#All;
                SourceExpr="Line No.";
                Visible=ShowDetails }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die von der Projektplanungszeile in die Rechnung oder Gutschrift �bertragene Menge.;
                           ENU=Specifies the quantity transferred from the job planning line to the invoice or credit memo.];
                ApplicationArea=#Jobs;
                SourceExpr="Quantity Transferred" }

    { 7   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Rechnung oder der Gutschriftbeleg erstellt wurde. Das Datum wird auf das Buchungsdatum festgelegt, das Sie beim Erstellen der Rechnung oder Gutschrift angegeben haben.;
                           ENU=Specifies the date on which the invoice or credit document was created. The date is set to the posting date you specified when you created the invoice or credit memo.];
                ApplicationArea=#All;
                SourceExpr="Transferred Date";
                Visible=ShowDetails }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Rechnung oder Gutschrift gebucht wurde.;
                           ENU=Specifies the date on which the invoice or credit memo was posted.];
                ApplicationArea=#All;
                SourceExpr="Invoiced Date";
                Visible=ShowDetails }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag (MW) an, der aus der Rechnung oder Gutschrift gebucht wurde. Der Betrag wird auf Grundlage von Menge, Zeilenrabatt % und VK-Preis berechnet.;
                           ENU=Specifies the amount (LCY) that was posted from the invoice or credit memo. The amount is calculated based on Quantity, Line Discount %, and Unit Price.];
                ApplicationArea=#Jobs;
                SourceExpr="Invoiced Amount (LCY)" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Betrag der Einstandspreise an, der aus der Rechnung oder Gutschrift gebucht wurde. Der Betrag wird auf Grundlage von Menge, Einstandspreis und Zeilenrabatt % berechnet.;
                           ENU=Specifies the amount of the unit costs that has been posted from the invoice or credit memo. The amount is calculated based on Quantity, Unit Cost, and Line Discount %.];
                ApplicationArea=#Jobs;
                SourceExpr="Invoiced Cost Amount (LCY)" }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Link zum Projektposten an, der bei der Buchung des Belegs erstellt wurde.;
                           ENU=Specifies a link to the job ledger entry that was created when the document was posted.];
                ApplicationArea=#All;
                SourceExpr="Job Ledger Entry No.";
                Visible=ShowDetails }

  }
  CODE
  {
    VAR
      JobNo@1002 : Code[20];
      JobTaskNo@1003 : Code[20];
      JobPlanningLineNo@1004 : Integer;
      DetailLevel@1001 : 'All,Per Job,Per Job Task,Per Job Planning Line';
      ShowDetails@1006 : Boolean;

    PROCEDURE SetPrJob@1(Job@1000 : Record 167);
    BEGIN
      DetailLevel := DetailLevel::"Per Job";
      JobNo := Job."No.";
      ShowDetails := FALSE;
    END;

    PROCEDURE SetPrJobTask@2(JobTask@1000 : Record 1001);
    BEGIN
      DetailLevel := DetailLevel::"Per Job Task";
      JobNo := JobTask."Job No.";
      JobTaskNo := JobTask."Job Task No.";
      ShowDetails := FALSE;
    END;

    PROCEDURE SetPrJobPlanningLine@3(JobPlanningLine@1000 : Record 1003);
    BEGIN
      DetailLevel := DetailLevel::"Per Job Planning Line";
      JobNo := JobPlanningLine."Job No.";
      JobTaskNo := JobPlanningLine."Job Task No.";
      JobPlanningLineNo := JobPlanningLine."Line No.";
      ShowDetails := FALSE;
    END;

    BEGIN
    END.
  }
}

