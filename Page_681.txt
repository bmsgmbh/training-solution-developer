OBJECT Page 681 Report Inbox Part
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Berichtseingang;
               ENU=Report Inbox];
    SourceTable=Table477;
    SourceTableView=SORTING(User ID,Created Date-Time)
                    ORDER(Descending);
    PageType=ListPart;
    OnOpenPage=BEGIN
                 SETRANGE("User ID",USERID);
                 SETAUTOCALCFIELDS;
                 ShowAll := TRUE;
                 UpdateVisibility;
                 AddInReady := FALSE;
               END;

    OnQueryClosePage=BEGIN
                       IF AddInReady THEN
                         CurrPage.PingPong.Stop;
                       EXIT(TRUE);
                     END;

    ActionList=ACTIONS
    {
      { 7       ;    ;ActionContainer;
                      CaptionML=[DEU=Bericht;
                                 ENU=Report];
                      ActionContainerType=ActionItems }
      { 8       ;1   ;Action    ;
                      Name=Show;
                      ShortCutKey=Return;
                      CaptionML=[DEU=Anzeigen;
                                 ENU=Show];
                      ToolTipML=[DEU=�ffnet Ihren Berichtseingang.;
                                 ENU=Open your report inbox.];
                      ApplicationArea=#Basic,#Suite;
                      Image=Report;
                      OnAction=BEGIN
                                 ShowReport;
                                 CurrPage.UPDATE;
                               END;
                                }
      { 11      ;1   ;Separator  }
      { 12      ;1   ;Action    ;
                      Name=Unread;
                      CaptionML=[DEU=Ungelesene Berichte;
                                 ENU=Unread Reports];
                      ToolTipML=[DEU=Zeigt nur nicht gelesene Berichte im Eingang an.;
                                 ENU=Show only unread reports in your inbox.];
                      ApplicationArea=#Basic,#Suite;
                      Enabled=ShowAll;
                      Image=FilterLines;
                      OnAction=BEGIN
                                 ShowAll := FALSE;
                                 UpdateVisibility;
                               END;
                                }
      { 13      ;1   ;Action    ;
                      Name=All;
                      CaptionML=[DEU=Alle Berichte;
                                 ENU=All Reports];
                      ToolTipML=[DEU=Zeigt alle Berichte im Eingang an.;
                                 ENU=View all reports in your inbox.];
                      ApplicationArea=#Basic,#Suite;
                      Enabled=NOT ShowAll;
                      Image=AllLines;
                      OnAction=BEGIN
                                 ShowAll := TRUE;
                                 UpdateVisibility;
                               END;
                                }
      { 14      ;1   ;Separator  }
      { 9       ;1   ;Action    ;
                      Name=Delete;
                      CaptionML=[DEU=L�schen;
                                 ENU=Delete];
                      ToolTipML=[DEU=L�scht den Datensatz.;
                                 ENU=Delete the record.];
                      ApplicationArea=#Basic,#Suite;
                      Image=Delete;
                      OnAction=VAR
                                 ReportInbox@1000 : Record 477;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(ReportInbox);
                                 ReportInbox.DELETEALL;
                               END;
                                }
      { 18      ;1   ;Separator  }
      { 19      ;1   ;Action    ;
                      Name=ShowQueue;
                      CaptionML=[DEU=Warteschlange anzeigen;
                                 ENU=Show Queue];
                      ToolTipML=[DEU=Zeigt geplante Berichte an.;
                                 ENU=Show scheduled reports.];
                      ApplicationArea=#Basic,#Suite;
                      Image=List;
                      OnAction=VAR
                                 JobQueueEntry@1000 : Record 472;
                               BEGIN
                                 JobQueueEntry.FILTERGROUP(2);
                                 JobQueueEntry.SETRANGE("User ID",USERID);
                                 JobQueueEntry.FILTERGROUP(0);
                                 PAGE.RUN(PAGE::"Job Queue Entries",JobQueueEntry);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Microsoft Dynamics NAV-Benutzer an, der die Ausf�hrung des Berichts geplant hat.;
                           ENU=Specifies the Microsoft Dynamics NAV user who scheduled the report to run.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User ID";
                Visible=false;
                Style=Strong;
                StyleExpr=NOT Read }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum und die Uhrzeit der Verarbeitung des geplanten Berichts aus der Aufgabenwarteschlange an.;
                           ENU=Specifies the date and time that the scheduled report was processed from the job queue.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Created Date-Time";
                Style=Strong;
                StyleExpr=NOT Read }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Berichts an, der geplant und von der Aufgabenwarteschlange ausgef�hrt wurde.;
                           ENU=Specifies the ID of the report that was scheduled and processed by job queue.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Report ID";
                Visible=false;
                Style=Strong;
                StyleExpr=NOT Read }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des geplanten Berichts an, der von der Aufgabenwarteschlange verarbeitet wurde.;
                           ENU=Specifies the name of the scheduled report that was processed from the job queue.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Report Name";
                Visible=FALSE;
                Style=Strong;
                StyleExpr=NOT Read;
                OnDrillDown=BEGIN
                              ShowReport;
                              CurrPage.UPDATE;
                            END;
                             }

    { 15  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des geplanten Berichts an, der von der Aufgabenwarteschlange verarbeitet wurde.;
                           ENU=Specifies the description of the scheduled report that was processed from the job queue.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description;
                Style=Strong;
                StyleExpr=NOT Read;
                OnDrillDown=BEGIN
                              ShowReport;
                              CurrPage.UPDATE;
                            END;
                             }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Ausgabeart des geplanten Berichts an.;
                           ENU=Specifies the output type of the scheduled report.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Output Type";
                Style=Strong;
                StyleExpr=NOT Read }

    { 16  ;1   ;Group     ;
                GroupType=Group }

    { 17  ;2   ;Field     ;
                Name=PingPong;
                ApplicationArea=#Basic,#Suite;
                ControlAddIn=[Microsoft.Dynamics.Nav.Client.PingPong;PublicKeyToken=31bf3856ad364e35] }

  }
  CODE
  {
    VAR
      ShowAll@1000 : Boolean;
      PrevNumberOfRecords@1001 : Integer;
      AddInReady@1002 : Boolean;

    LOCAL PROCEDURE UpdateVisibility@1();
    BEGIN
      IF ShowAll THEN
        SETRANGE(Read)
      ELSE
        SETRANGE(Read,FALSE);
      IF FINDFIRST THEN;
      CurrPage.UPDATE(FALSE);
    END;

    EVENT PingPong@-17::AddInReady@2();
    BEGIN
      AddInReady := TRUE;
      PrevNumberOfRecords := COUNT;
      CurrPage.PingPong.Ping(10000);
    END;

    EVENT PingPong@-17::Pong@3();
    VAR
      CurrNumberOfRecords@1000 : Integer;
    BEGIN
      CurrNumberOfRecords := COUNT;
      IF PrevNumberOfRecords <> CurrNumberOfRecords THEN
        CurrPage.UPDATE(FALSE);
      PrevNumberOfRecords := CurrNumberOfRecords;
      CurrPage.PingPong.Ping(10000);
    END;

    BEGIN
    END.
  }
}

