OBJECT Page 586 XBRL G/L Map Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=XBRL-Finanzb.-Abbildungszeilen;
               ENU=XBRL G/L Map Lines];
    SourceTable=Table397;
    DataCaptionExpr=GetCaption;
    PageType=List;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Sachkonten fest, die zum Generieren der exportierten Daten verwendet werden, die im Instance Document enthalten sind. Es werden nur Sachkonten verwendet.;
                           ENU=Specifies the general ledger accounts that will be used to generate the exported data contained in the instance document. Only posting accounts will be used.];
                SourceExpr="G/L Account Filter";
                OnLookup=VAR
                           GLAccList@1000 : Page 18;
                         BEGIN
                           GLAccList.LOOKUPMODE(TRUE);
                           IF NOT (GLAccList.RUNMODAL = ACTION::LookupOK) THEN
                             EXIT(FALSE);

                           Text := GLAccList.GetSelectionFilter;
                           EXIT(TRUE);
                         END;
                          }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Gesch�ftseinheiten fest, die verwendet werden, um die exportierten Daten zu generieren, die im Instance Document enthalten sind.;
                           ENU=Specifies the business units that will be used to generate the exported data that is contained in the instance document.];
                SourceExpr="Business Unit Filter";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Dimensionen fest, die verwendet werden, um die exportierten Daten zu generieren, die im Instance Document enthalten sind.;
                           ENU=Specifies the dimensions that will be used to generate the exported data that is contained in the instance document.];
                SourceExpr="Global Dimension 1 Filter";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Dimensionen fest, die verwendet werden, um die exportierten Daten zu generieren, die im Instance Document enthalten sind.;
                           ENU=Specifies the dimensions that will be used to generate the exported data that is contained in the instance document.];
                SourceExpr="Global Dimension 2 Filter";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Legt zusammen mit dem Startdatum, der Periodenl�nge und der Anzahl der Perioden fest, welcher Datumsbereich auf die f�r diese Zeile exportierten Finanzbuchhaltungsdaten angewendet wird.;
                           ENU=Specifies, along with the starting date, period length, and number of periods, what date range will be applied to the general ledger data exported for this line.];
                SourceExpr="Timeframe Type" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Legt fest, welche Sachposten in der Summe f�r den Export in das Instance Document ber�cksichtigt werden.;
                           ENU=Specifies which general ledger entries will be included in the total calculated for export to the instance document.];
                SourceExpr="Amount Type" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Legt Soll oder Haben fest. Dies legt fest, wie w�hrend der Berechnung mit dem Saldo verfahren wird. Dadurch k�nnen Salden, die mit der Standardsaldoart �bereinstimmen, als positive Werte exportiert werden Wenn Sie beispielsweise m�chten, dass das Instance Document positive Zahlen enthalten soll, dann muss f�r alle Sachkonten mit einem normalen Habensaldo f�r dieses Feld Haben gew�hlt werden.;
                           ENU=Specifies either debit or credit. This determines how the balance will be handled during calculation, allowing balances consistent with the Normal Balance type to be exported as positive values. For example, if you want the instance document to contain positive numbers, all G/L Accounts with a normal credit balance will need to have Credit selected for this field.];
                SourceExpr="Normal Balance" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    LOCAL PROCEDURE GetCaption@1() : Text[250];
    VAR
      XBRLLine@1000 : Record 395;
    BEGIN
      IF NOT XBRLLine.GET("XBRL Taxonomy Name","XBRL Taxonomy Line No.") THEN
        EXIT('');

      COPYFILTER("Label Language Filter",XBRLLine."Label Language Filter");
      XBRLLine.CALCFIELDS(Label);
      IF XBRLLine.Label = '' THEN
        XBRLLine.Label := XBRLLine.Name;
      EXIT(XBRLLine.Label);
    END;

    BEGIN
    END.
  }
}

