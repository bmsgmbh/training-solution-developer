OBJECT Page 805 Online Map Parameter FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Online Map-Parameter (Infobox);
               ENU=Online Map Parameter FactBox];
    PageType=CardPart;
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                Name=Container;
                ContainerType=ContentArea }

    { 2   ;1   ;Field     ;
                CaptionML=[DEU={1};
                           ENU={1}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text001 }

    { 3   ;1   ;Field     ;
                CaptionML=[DEU={2};
                           ENU={2}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text002 }

    { 4   ;1   ;Field     ;
                CaptionML=[DEU={3};
                           ENU={3}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text003 }

    { 5   ;1   ;Field     ;
                CaptionML=[DEU={4};
                           ENU={4}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text004 }

    { 6   ;1   ;Field     ;
                CaptionML=[DEU={5};
                           ENU={5}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text005 }

    { 7   ;1   ;Field     ;
                CaptionML=[DEU={6};
                           ENU={6}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text006 }

    { 8   ;1   ;Field     ;
                CaptionML=[DEU={7};
                           ENU={7}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text007 }

    { 9   ;1   ;Field     ;
                CaptionML=[DEU={8};
                           ENU={8}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text008 }

    { 10  ;1   ;Field     ;
                CaptionML=[DEU={9};
                           ENU={9}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text009 }

    { 11  ;1   ;Field     ;
                CaptionML=[DEU={10};
                           ENU={10}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=LatitudeLbl }

    { 12  ;1   ;Field     ;
                CaptionML=[DEU={11};
                           ENU={11}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=LongitudeLbl }

  }
  CODE
  {
    VAR
      Text001@1000 : TextConst 'DEU=Stra�e (Adresse�1);ENU=Street (Address1)';
      Text002@1001 : TextConst 'DEU=Ort;ENU=City';
      Text003@1002 : TextConst 'DEU=Bundesland (Landkreis);ENU=State (County)';
      Text004@1003 : TextConst 'DEU=PLZ-Code;ENU=Post Code/ZIP Code';
      Text005@1004 : TextConst 'DEU=L�nder-/Regionscode;ENU=Country/Region Code';
      Text006@1008 : TextConst 'DEU=L�nder-/Regionsname;ENU=Country/Region Name';
      Text007@1005 : TextConst 'DEU=Kulturinformationen (beispielsweise ''de-de'');ENU=Culture Information, e.g., en-us';
      Text008@1006 : TextConst 'DEU=Entfernung in (Meilen/Kilometern);ENU=Distance in (Miles/Kilometers)';
      Text009@1007 : TextConst 'DEU=Route (schnellste/k�rzeste);ENU=Route (Quickest/Shortest)';
      LatitudeLbl@1009 : TextConst 'DEU=GPS-Breitengrad;ENU=GPS Latitude';
      LongitudeLbl@1010 : TextConst 'DEU=GPS-L�ngengrad;ENU=GPS Longitude';

    BEGIN
    END.
  }
}

