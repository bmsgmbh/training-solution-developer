OBJECT Page 6004 Res. Availability (Service)
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Res.-Verf�gbarkeit (Service);
               ENU=Res. Availability (Service)];
    SaveValues=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table156;
    DataCaptionExpr='';
    PageType=Card;
    OnOpenPage=BEGIN
                 ServMgtSetup.GET;
                 ServHeader.GET(CurrentDocumentType,CurrentDocumentNo);
                 SetColumns(SetWanted::Initial);
                 UpdateFields;
               END;

    OnAfterGetRecord=BEGIN
                       IF ServHeader.GET(CurrentDocumentType,CurrentDocumentNo) THEN
                         "Service Zone Filter" := ServHeader."Service Zone Code"
                       ELSE
                         "Service Zone Filter" := '';

                       CALCFIELDS("In Customer Zone");
                     END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 77      ;1   ;Action    ;
                      Name=ShowMatrix;
                      CaptionML=[DEU=&Matrix anzeigen;
                                 ENU=&Show Matrix];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ShowMatrix;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 MatrixForm@1117 : Page 9229;
                               BEGIN
                                 MatrixForm.SetData(CurrentDocumentType,CurrentDocumentNo,CurrentServItemLineNo,CurrentEntryNo,
                                   MatrixColumnCaptions,MatrixRecords,PeriodType);
                                 MatrixForm.SETTABLEVIEW(Rec);
                                 MatrixForm.RUNMODAL;
                               END;
                                }
      { 6       ;1   ;Action    ;
                      CaptionML=[DEU=N�chster Satz;
                                 ENU=Next Set];
                      ToolTipML=[DEU=Wechselt zum n�chsten Datensatz.;
                                 ENU=Go to the next set of data.];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=NextSet;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SetColumns(SetWanted::Next);
                               END;
                                }
      { 3       ;1   ;Action    ;
                      CaptionML=[DEU=Vorheriger Satz;
                                 ENU=Previous Set];
                      ToolTipML=[DEU=Wechselt zum vorherigen Datensatz.;
                                 ENU=Go to the previous set of data.];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PreviousSet;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SetColumns(SetWanted::Previous);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 22  ;1   ;Group     ;
                CaptionML=[DEU=Matrixoptionen;
                           ENU=Matrix Options] }

    { 4   ;2   ;Field     ;
                CaptionML=[DEU=Anzeigen nach;
                           ENU=View by];
                ToolTipML=[DEU=Legt fest, bis zu welchem Zeitraum die Betr�ge angezeigt werden.;
                           ENU=Specifies by which period amounts are displayed.];
                OptionCaptionML=[DEU=Tag,Woche,Monat,Quartal,Jahr,Buchhaltungsperiode;
                                 ENU=Day,Week,Month,Quarter,Year,Accounting Period];
                SourceExpr=PeriodType;
                OnValidate=BEGIN
                             DateControl;
                             SetColumns(SetWanted::Initial);
                           END;
                            }

    { 5   ;2   ;Field     ;
                CaptionML=[DEU=Datumsfilter;
                           ENU=Date Filter];
                SourceExpr=DateFilter;
                OnValidate=BEGIN
                             DateControl;
                             SetColumns(SetWanted::Initial);
                           END;
                            }

    { 2   ;2   ;Field     ;
                CaptionML=[DEU=Spaltensatz;
                           ENU=Column set];
                SourceExpr=ColumnsSet;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      MatrixRecords@1021 : ARRAY [32] OF Record 2000000007;
      ResRec2@1041 : Record 156;
      ServMgtSetup@1001 : Record 5911;
      ServHeader@1002 : Record 5900;
      ApplicationManagement@1039 : Codeunit 1;
      CurrentDocumentType@1019 : Integer;
      CurrentDocumentNo@1007 : Code[20];
      CurrentServItemLineNo@1008 : Integer;
      CurrentEntryNo@1009 : Integer;
      PeriodType@1012 : 'Day,Week,Month,Quarter,Year,Accounting Period';
      DateFilter@1035 : Text;
      SetWanted@1048 : 'Initial,Previous,Same,Next';
      PKFirstRecInCurrSet@1047 : Text[1024];
      MatrixColumnCaptions@1046 : ARRAY [32] OF Text[100];
      ColumnsSet@1045 : Text[1024];
      CurrSetLength@1044 : Integer;

    PROCEDURE SetData@2(DocumentType@1003 : Integer;DocumentNo@1000 : Code[20];ServItemLineNo@1001 : Integer;EntryNo@1002 : Integer);
    BEGIN
      CurrentDocumentType := DocumentType;
      CurrentDocumentNo := DocumentNo;
      CurrentServItemLineNo := ServItemLineNo;
      CurrentEntryNo := EntryNo;
    END;

    LOCAL PROCEDURE UpdateFields@3();
    BEGIN
    END;

    LOCAL PROCEDURE DateControl@6();
    BEGIN
      IF ApplicationManagement.MakeDateFilter(DateFilter) = 0 THEN;
      ResRec2.SETFILTER("Date Filter",DateFilter);
      DateFilter := ResRec2.GETFILTER("Date Filter");
    END;

    PROCEDURE SetColumns@11(SetWanted@1001 : 'Initial,Previous,Same,Next');
    VAR
      MatrixMgt@1000 : Codeunit 9200;
    BEGIN
      MatrixMgt.GeneratePeriodMatrixData(SetWanted,32,FALSE,PeriodType,DateFilter,
        PKFirstRecInCurrSet,MatrixColumnCaptions,ColumnsSet,CurrSetLength,MatrixRecords);
    END;

    BEGIN
    END.
  }
}

