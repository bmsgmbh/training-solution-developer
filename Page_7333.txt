OBJECT Page 7333 Posted Whse. Receipt List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Geb. Wareneingangs�bersicht;
               ENU=Posted Whse. Receipt List];
    SourceTable=Table7318;
    DataCaptionFields=No.;
    PageType=List;
    CardPageID=Posted Whse. Receipt;
    OnOpenPage=BEGIN
                 ErrorIfUserIsNotWhseEmployee;
               END;

    OnFindRecord=BEGIN
                   EXIT(FindFirstAllowedRec(Which));
                 END;

    OnNextRecord=BEGIN
                   EXIT(FindNextAllowedRec(Steps));
                 END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102601002;1 ;ActionGroup;
                      CaptionML=[DEU=&Wareneingang;
                                 ENU=&Receipt];
                      Image=Receipt }
      { 1102601003;2 ;Action    ;
                      ShortCutKey=Shift+Ctrl+L;
                      CaptionML=[DEU=�bersicht;
                                 ENU=List];
                      Image=OpportunitiesList;
                      OnAction=BEGIN
                                 LookupPostedWhseRcptHeader(Rec);
                               END;
                                }
      { 1102601004;2 ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 5776;
                      RunPageLink=Table Name=CONST(Posted Whse. Receipt),
                                  Type=CONST(" "),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 1102601005;2 ;Action    ;
                      CaptionML=[DEU=Einlagerungszeilen;
                                 ENU=Put-away Lines];
                      RunObject=Page 5785;
                      RunPageView=SORTING(Whse. Document No.,Whse. Document Type,Activity Type)
                                  WHERE(Activity Type=CONST(Put-away));
                      RunPageLink=Whse. Document Type=CONST(Receipt),
                                  Whse. Document No.=FIELD(No.);
                      Image=PutawayLines }
      { 1102601006;2 ;Action    ;
                      CaptionML=[DEU=Registr. Einlagerungszeilen;
                                 ENU=Registered Put-away Lines];
                      RunObject=Page 7364;
                      RunPageView=SORTING(Whse. Document Type,Whse. Document No.,Whse. Document Line No.)
                                  WHERE(Activity Type=CONST(Put-away));
                      RunPageLink=Whse. Document Type=CONST(Receipt),
                                  Whse. Document No.=FIELD(No.);
                      Image=RegisteredDocs }
      { 17      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 19      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Karte;
                                 ENU=Card];
                      Image=EditLines;
                      OnAction=BEGIN
                                 PAGE.RUN(PAGE::"Posted Whse. Receipt",Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des gebuchten Wareneingangs an.;
                           ENU=Specifies the number of the posted warehouse receipt.];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, an dem die Artikel eingegangen sind.;
                           ENU=Specifies the code of the location where the items were received.];
                SourceExpr="Location Code" }

    { 15  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der f�r den Beleg verantwortlich ist.;
                           ENU=Specifies the ID of the user who is responsible for the document.];
                SourceExpr="Assigned User ID" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Nummernseriencode f�r den Datensatz an, der beim Buchen eines Wareneingangs erstellt wird.;
                           ENU=Specifies the number series code to use for the record it creates when you post a receipt.];
                SourceExpr="No. Series" }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des zum gebuchten Beleg geh�rigen Wareneingangs an.;
                           ENU=Specifies the number of the warehouse receipt that the posted warehouse receipt concerns.];
                SourceExpr="Whse. Receipt No." }

    { 1102601000;2;Field  ;
                ToolTipML=[DEU=Gibt den Zonencode dieses gebuchten Wareneingangskopfs an.;
                           ENU=Specifies the code of the zone on this posted receipt header.];
                SourceExpr="Zone Code";
                Visible=FALSE }

    { 1102601007;2;Field  ;
                ToolTipML=[DEU=Gibt den Code des Lagerplatzes des gebuchten Wareneingangskopfs an.;
                           ENU=Specifies the code of the bin on the posted receipt header.];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 1102601009;2;Field  ;
                ToolTipML=[DEU=Gibt den Status des gebuchten Wareneingangs an.;
                           ENU=Specifies the status of the posted warehouse receipt.];
                SourceExpr="Document Status";
                Visible=FALSE }

    { 1102601011;2;Field  ;
                ToolTipML=[DEU=Gibt das Buchungsdatum des Wareneingangs an.;
                           ENU=Specifies the posting date of the receipt.];
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 1102601013;2;Field  ;
                ToolTipML=[DEU=Gibt das Datum an, an dem der Wareneingang dem Benutzer zugewiesen wurde.;
                           ENU=Specifies the date on which the receipt was assigned to the user.];
                SourceExpr="Assignment Date";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

