OBJECT Page 1011 Job Resource Prices
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Res.-VK-Preise Projekt;
               ENU=Job Resource Prices];
    SourceTable=Table1012;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Projekts an, f�r das dieser Ressourcen-VK-Preis gilt.;
                           ENU=Specifies the number of the job to which this resource price applies.];
                ApplicationArea=#Jobs;
                SourceExpr="Job No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Projektaufgabe an, wenn der Ressourcen-VK-Preis nur f�r eine bestimmte Projektaufgabe gelten soll.;
                           ENU=Specifies the number of the job task if the resource price should only apply to a specific job task.];
                ApplicationArea=#Jobs;
                SourceExpr="Job Task No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob der f�r das Projekt eingerichtete Preis f�r eine Ressource, eine Ressourcengruppe oder f�r alle Ressourcen und Ressourcengruppen gelten soll.;
                           ENU=Specifies whether the price that you are setting up for the job should apply to a resource, to a resource group, or to all resources and resource groups.];
                ApplicationArea=#Jobs;
                SourceExpr=Type }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Ressource oder Ressourcengruppe an, f�r die dieser Preis gelten soll. Die Nummer muss der Auswahl im Feld "Art" entsprechen.;
                           ENU=Specifies the resource or resource group that this price applies to. The No. must correspond to your selection in the Type field.];
                ApplicationArea=#Jobs;
                SourceExpr=Code }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, zu welchem Arbeitstyp die Ressource geh�rt. Die Preise werden auf Grundlage dieses Postens aktualisiert.;
                           ENU=Specifies which work type the resource applies to. Prices are updated based on this entry.];
                ApplicationArea=#Jobs;
                SourceExpr="Work Type Code" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die W�hrung des Verkaufspreises an, wenn der Preis, den Sie in dieser Zeile eingerichtet haben, in einer Fremdw�hrung angegeben wird. W�hlen Sie das Feld aus, um die verf�gbaren W�hrungscodes anzuzeigen.;
                           ENU=Specifies the code for the currency of the sales price if the price that you have set up in this line is in a foreign currency. Choose the field to see the available currency codes.];
                ApplicationArea=#Jobs;
                SourceExpr="Currency Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Verkaufspreis an, der f�r diese Zeile gilt. Dieser Preis wird in der W�hrung angegeben, die durch den Code im Feld "W�hrungscode" in dieser Zeile dargestellt wird.;
                           ENU=Specifies the sales price that applies to this line. This price is in the currency represented by the code in the Currency Code field on this line.];
                ApplicationArea=#Jobs;
                SourceExpr="Unit Price" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einstandspreisfaktor an. Wenn Sie mit dem Debitor vereinbart haben, dass dieser f�r einen bestimmten Ressourcenverbrauch nach Einstandswert plus einem bestimmten Prozentsatz zahlen soll, um den Gemeinkostenaufwand zu decken, k�nnen Sie in diesem Feld einen Einstandspreisfaktor einrichten.;
                           ENU=Specifies the unit cost factor. If you have agreed with you customer that he should pay for certain resource usage by cost value plus a certain percent value to cover your overhead expenses, you can set up a unit cost factor in this field.];
                ApplicationArea=#Jobs;
                SourceExpr="Unit Cost Factor" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Prozentsatz f�r den Zeilenrabatt f�r diese Ressource oder Ressourcengruppe an. Dies ist z. B. dann n�tzlich, wenn in den Rechnungszeilen f�r das Projekt ein Rabattprozentsatz angegeben sein soll.;
                           ENU=Specifies a line discount percent that applies to this resource, or resource group. This is useful, for example if you want invoice lines for the job to show a discount percent.];
                ApplicationArea=#Jobs;
                SourceExpr="Line Discount %" }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung der Ressource oder Ressourcengruppe an, die Sie in das Feld "Code" eingegeben haben.;
                           ENU=Specifies the description of the resource, or resource group, you have entered in the Code field.];
                ApplicationArea=#Jobs;
                SourceExpr=Description }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob ein Rabatt auf das Projekt angewendet werden soll. W�hlen Sie dieses Feld aus, wenn der Rabattprozentsatz f�r diese Ressource oder Ressourcengruppe auch dann f�r das Projekt gelten soll, wenn der Rabattprozentsatz den Wert Null aufweist.;
                           ENU=Specifies whether to apply a discount to the job. Select this field if the discount percent for this resource or resource group should apply to the job, even if the discount percent is zero.];
                ApplicationArea=#Jobs;
                SourceExpr="Apply Job Discount";
                Visible=FALSE }

    { 25  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob der Preis f�r diese Ressource oder Ressourcengruppe auch dann f�r das Projekt gelten soll, wenn der Preis den Wert Null aufweist.;
                           ENU=Specifies whether the price for this resource, or resource group, should apply to the job, even if the price is zero.];
                ApplicationArea=#Jobs;
                SourceExpr="Apply Job Price";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

