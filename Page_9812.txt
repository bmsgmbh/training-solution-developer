OBJECT Page 9812 Set Web Service Access Key
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Webdienst-Zugriffsschl�ssel festlegen;
               ENU=Set Web Service Access Key];
    SourceTable=Table2000000120;
    DataCaptionExpr="Full Name";
    PageType=StandardDialog;
    InstructionalTextML=[DEU=Webdienst-Zugriffsschl�ssel festlegen;
                         ENU=Set Web Service Access Key];
    OnQueryClosePage=BEGIN
                       IF CloseAction = ACTION::OK THEN BEGIN
                         IF NeverExpires THEN
                           IdentityManagement.CreateWebServicesKeyNoExpiry("User Security ID")
                         ELSE
                           IdentityManagement.CreateWebServicesKey("User Security ID",ExpirationDate);
                       END;
                     END;

  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                Name=General;
                ContainerType=ContentArea }

    { 5   ;1   ;Group     ;
                Name=somegroup;
                CaptionML=[DEU=Wenn Sie einen neuen Webdienstschl�ssel festlegen, wird der alte Schl�ssel ung�ltig.;
                           ENU=Setting a new Web Service key makes the old key not valid.];
                GroupType=Group }

    { 4   ;1   ;Field     ;
                CaptionML=[DEU=Schl�ssel l�uft niemals ab;
                           ENU=Key Never Expires];
                ToolTipML=[DEU=Gibt an, dass der Webdienst-Zugriffsschl�ssel nicht ablaufen kann.;
                           ENU=Specifies that the web service access key cannot expire.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NeverExpires }

    { 3   ;1   ;Field     ;
                CaptionML=[DEU=Schl�sselablaufdatum;
                           ENU=Key Expiration Date];
                ToolTipML=[DEU=Gibt an, wann der Webdienst-Zugriffsschl�ssel abl�uft.;
                           ENU=Specifies when the web service access key expires.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=ExpirationDate;
                Editable=NOT NeverExpires }

  }
  CODE
  {
    VAR
      IdentityManagement@1004 : Codeunit 9801;
      ExpirationDate@1001 : DateTime;
      NeverExpires@1002 : Boolean;

    BEGIN
    END.
  }
}

