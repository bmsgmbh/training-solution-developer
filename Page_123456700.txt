OBJECT Page 123456700 Seminar Card
{
  OBJECT-PROPERTIES
  {
    Date=07.12.17;
    Time=09:57:02;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Seminar Karte;
               ENU=Seminar Card];
    SourceTable=Table123456700;
    PageType=Card;
    ActionList=ACTIONS
    {
      { 1000000020;0 ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 1000000021;1 ;Action    ;
                      Name=Seminar Registration;
                      CaptionML=[DEU=Seminar Registrierung;
                                 ENU=Seminar Registration];
                      RunObject=Page 123456710;
                      RunPageLink=Seminar No.=FIELD(No.);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=NewTimesheet;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1000000017;  ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1000000018;1 ;ActionGroup;
                      CaptionML=[DEU=Seminar;
                                 ENU=Seminar] }
      { 1000000024;2 ;Action    ;
                      Name=Seminar Ledger Entries;
                      ShortCutKey=Ctrl+F7;
                      RunObject=Page 123456721;
                      RunPageLink=Seminar No.=FIELD(No.);
                      PromotedIsBig=Yes;
                      Image=WarrantyLedger;
                      PromotedCategory=Process }
      { 1000000019;2 ;Action    ;
                      Name=Comments;
                      CaptionML=[DEU=Bemerkungen;
                                 ENU=Comments];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(Seminar),
                                  No.=FIELD(No.) }
      { 1000000022;1 ;ActionGroup;
                      Name=Registrations Group;
                      CaptionML=DEU=Registrations }
      { 1000000023;2 ;Action    ;
                      Name=Registrations;
                      RunObject=Page 123456713;
                      RunPageLink=Seminar No.=FIELD(No.);
                      Image=Timesheet;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1000000000;0;Container;
                ContainerType=ContentArea }

    { 1000000001;1;Group  ;
                Name=Allgemein;
                GroupType=Group }

    { 1000000002;2;Field  ;
                SourceExpr="No.";
                OnAssistEdit=BEGIN
                               IF AssistEdit THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 1000000003;2;Field  ;
                SourceExpr=Name }

    { 1000000004;2;Field  ;
                SourceExpr="Minimum Participants" }

    { 1000000005;2;Field  ;
                SourceExpr="Maximum Participants" }

    { 1000000006;2;Field  ;
                SourceExpr="Search Name" }

    { 1000000007;2;Field  ;
                SourceExpr="Seminar Duration" }

    { 1000000008;2;Field  ;
                SourceExpr=Blocked }

    { 1000000009;2;Field  ;
                SourceExpr="Last Date Modified" }

    { 1000000010;1;Group  ;
                Name=Invoicing;
                GroupType=Group }

    { 1000000011;2;Field  ;
                SourceExpr="Gen. Prod. Posting Group" }

    { 1000000012;2;Field  ;
                SourceExpr="VAT Prod. Posting Group" }

    { 1000000013;2;Field  ;
                SourceExpr="Seminar Price" }

    { 1000000014;0;Container;
                ContainerType=FactBoxArea }

    { 1000000015;1;Part   ;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1000000016;1;Part   ;
                PartType=System;
                SystemPartID=MyNotes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

