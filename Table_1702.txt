OBJECT Table 1702 Deferral Line
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IF "Posting Date" = 0D THEN
                 ERROR(InvalidDeferralLineDateErr);
             END;

    CaptionML=[DEU=Abgrenzungszeile;
               ENU=Deferral Line];
  }
  FIELDS
  {
    { 1   ;   ;Deferral Doc. Type  ;Option        ;TableRelation="Deferral Header"."Deferral Doc. Type";
                                                   CaptionML=[DEU=Abgrenzungsbelegtyp;
                                                              ENU=Deferral Doc. Type];
                                                   OptionCaptionML=[DEU=Einkauf,Verkauf,Sachkonto;
                                                                    ENU=Purchase,Sales,G/L];
                                                   OptionString=Purchase,Sales,G/L }
    { 2   ;   ;Gen. Jnl. Template Name;Code10     ;TableRelation="Deferral Header"."Gen. Jnl. Template Name";
                                                   CaptionML=[DEU=Fibu Buch.-Bl.-Vorlagenname;
                                                              ENU=Gen. Jnl. Template Name] }
    { 3   ;   ;Gen. Jnl. Batch Name;Code10        ;TableRelation="Deferral Header"."Gen. Jnl. Batch Name";
                                                   CaptionML=[DEU=Fibu Buch.-Blattname;
                                                              ENU=Gen. Jnl. Batch Name] }
    { 4   ;   ;Document Type       ;Integer       ;TableRelation="Deferral Header"."Document Type";
                                                   CaptionML=[DEU=Belegart;
                                                              ENU=Document Type] }
    { 5   ;   ;Document No.        ;Code20        ;TableRelation="Deferral Header"."Document No.";
                                                   CaptionML=[DEU=Belegnr.;
                                                              ENU=Document No.] }
    { 6   ;   ;Line No.            ;Integer       ;TableRelation="Deferral Header"."Line No.";
                                                   CaptionML=[DEU=Zeilennr.;
                                                              ENU=Line No.] }
    { 7   ;   ;Posting Date        ;Date          ;OnValidate=VAR
                                                                AccountingPeriod@1000 : Record 50;
                                                              BEGIN
                                                                IF GenJnlCheckLine.DateNotAllowed("Posting Date") THEN
                                                                  ERROR(InvalidPostingDateErr,"Posting Date");

                                                                AccountingPeriod.SETFILTER("Starting Date",'>=%1',"Posting Date");
                                                                IF AccountingPeriod.ISEMPTY THEN
                                                                  ERROR(DeferSchedOutOfBoundsErr);
                                                              END;

                                                   CaptionML=[DEU=Buchungsdatum;
                                                              ENU=Posting Date] }
    { 8   ;   ;Description         ;Text50        ;CaptionML=[DEU=Beschreibung;
                                                              ENU=Description] }
    { 9   ;   ;Amount              ;Decimal       ;OnValidate=BEGIN
                                                                IF Amount = 0 THEN
                                                                  ERROR(ZeroAmountToDeferErr);

                                                                IF DeferralHeader.GET("Deferral Doc. Type","Gen. Jnl. Template Name","Gen. Jnl. Batch Name","Document Type","Document No.","Line No.") THEN BEGIN
                                                                  IF DeferralHeader."Amount to Defer" > 0 THEN
                                                                    IF Amount < 0 THEN
                                                                      ERROR(AmountToDeferPositiveErr);
                                                                  IF DeferralHeader."Amount to Defer" < 0 THEN
                                                                    IF Amount > 0 THEN
                                                                      ERROR(AmountToDeferNegativeErr);
                                                                END;
                                                              END;

                                                   CaptionML=[DEU=Betrag;
                                                              ENU=Amount];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 10  ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[DEU=Betrag (MW);
                                                              ENU=Amount (LCY)];
                                                   AutoFormatType=1 }
    { 11  ;   ;Currency Code       ;Code10        ;CaptionML=[DEU=W�hrungscode;
                                                              ENU=Currency Code] }
  }
  KEYS
  {
    {    ;Deferral Doc. Type,Gen. Jnl. Template Name,Gen. Jnl. Batch Name,Document Type,Document No.,Line No.,Posting Date;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      DeferralHeader@1005 : Record 1701;
      GenJnlCheckLine@1002 : Codeunit 11;
      InvalidPostingDateErr@1003 : TextConst '@@@="%1=The date passed in for the posting date.";DEU=%1 liegt nicht innerhalb des Buchungszeitraums Ihres Unternehmens.;ENU=%1 is not within the range of posting dates for your company.';
      DeferSchedOutOfBoundsErr@1001 : TextConst 'DEU=Das Rechnungsabgrenzungsschema liegt au�erhalb der festgelegten Buchhaltungsperioden des Unternehmens.;ENU=The deferral schedule falls outside the accounting periods that have been set up for the company.';
      InvalidDeferralLineDateErr@1004 : TextConst 'DEU=Das Buchungsdatum f�r diesen Abgrenzungsschemazeile ist nicht g�ltig.;ENU=The posting date for this deferral schedule line is not valid.';
      ZeroAmountToDeferErr@1006 : TextConst 'DEU=Der Abgrenzungsbetrag darf nicht 0 sein.;ENU=The deferral amount cannot be 0.';
      AmountToDeferPositiveErr@1007 : TextConst 'DEU=Der Abgrenzungsbetrag muss positiv sein.;ENU=The deferral amount must be positive.';
      AmountToDeferNegativeErr@1008 : TextConst 'DEU=Der Abgrenzungsbetrag muss negativ sein.;ENU=The deferral amount must be negative.';

    BEGIN
    END.
  }
}

