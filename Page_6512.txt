OBJECT Page 6512 Item Tracking Code Card
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Artikelverfolgungskarte;
               ENU=Item Tracking Code Card];
    SourceTable=Table6502;
    PageType=Card;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Datensatzes an.;
                           ENU=Specifies the code of the record.];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Artikelverfolgungscodes an.;
                           ENU=Specifies a description of the item tracking code.];
                SourceExpr=Description }

    { 1907140601;1;Group  ;
                CaptionML=[DEU=Seriennr.;
                           ENU=Serial No.] }

    { 64  ;2   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 6   ;3   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass Sie f�r eine ausgehende Einheit des betreffenden Artikels immer angeben m�ssen, welche vorhandene Seriennummer betroffen sein soll.;
                           ENU=Specifies that when handling an outbound unit of the item in question, you must always specify which existing serial number to handle.];
                SourceExpr="SN Specific Tracking" }

    { 20  ;2   ;Group     ;
                CaptionML=[DEU=Eingang;
                           ENU=Inbound] }

    { 56  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Inform. erforderlich;
                           ENU=SN No. Info. Must Exist];
                ToolTipML=[DEU=Gibt an, dass f�r Seriennummern in eingehenden Belegzeilen ein Informationsdatensatz in dem Fenster "Seriennr.-Informationskarte" vorhanden sein muss.;
                           ENU=Specifies that serial numbers on inbound document lines must have an information record in the Serial No. Information Card.];
                SourceExpr="SN Info. Inbound Must Exist" }

    { 10  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Einkauf;
                           ENU=SN Purchase Tracking];
                ToolTipML=[DEU=Gibt an, dass eingehende Einkaufsbelegzeilen Seriennummern ben�tigen.;
                           ENU=Specifies that inbound purchase document lines require serial numbers.];
                SourceExpr="SN Purchase Inbound Tracking" }

    { 12  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Verkauf;
                           ENU=SN Sales Tracking];
                ToolTipML=[DEU=Gibt an, dass eingehende Verkaufsbelegzeilen Seriennummern ben�tigen.;
                           ENU=Specifies that inbound sales document lines require serial numbers.];
                SourceExpr="SN Sales Inbound Tracking" }

    { 14  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Zugang;
                           ENU=SN Positive Adjmt. Tracking];
                ToolTipML=[DEU=Gibt an, dass die eingehenden Artikel Buch.-Blattzeilen der Art "Zugang" Seriennummern ben�tigen.;
                           ENU=Specifies that inbound item journal lines of type positive entry require serial numbers.];
                SourceExpr="SN Pos. Adjmt. Inb. Tracking" }

    { 16  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Abgang;
                           ENU=SN Negative Adjmt. Tracking];
                ToolTipML=[DEU=Gibt an, dass die eingehenden Artikel Buch.-Blattzeilen der Art "Abgang" Seriennummern ben�tigen.;
                           ENU=Specifies that inbound item journal lines of type negative entry require serial numbers.];
                SourceExpr="SN Neg. Adjmt. Inb. Tracking" }

    { 9   ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Montage;
                           ENU=SN Assembly Tracking];
                ToolTipML=[DEU=Gibt an, dass Seriennummern f�r eingehende Buchungen aus Montageauftr�gen ben�tigt werden.;
                           ENU=Indicates that serial numbers are required with inbound posting from assembly orders.];
                SourceExpr="SN Assembly Inbound Tracking" }

    { 29  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Produktion;
                           ENU=SN Manufacturing Tracking];
                ToolTipML=[DEU=Gibt an, dass Seriennummern f�r eingehende Buchungen aus der Fertigung ben�tigt werden, also in der Regel Istmeldungen.;
                           ENU=Specifies that serial numbers are required with inbound posting from production - typically output.];
                SourceExpr="SN Manuf. Inbound Tracking" }

    { 82  ;2   ;Group      }

    { 31  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Lager;
                           ENU=SN Warehouse Tracking];
                ToolTipML=[DEU=Gibt an, dass Logistikbelegzeilen Seriennummern ben�tigen.;
                           ENU=Specifies that warehouse document lines require serial numbers.];
                SourceExpr="SN Warehouse Tracking" }

    { 18  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Umlagerung;
                           ENU=SN Transfer Tracking];
                ToolTipML=[DEU=Gibt an, dass Umlagerungsauftragszeilen Seriennummern ben�tigen.;
                           ENU=Specifies that transfer order lines require serial numbers.];
                SourceExpr="SN Transfer Tracking" }

    { 21  ;2   ;Group     ;
                CaptionML=[DEU=Ausgang;
                           ENU=Outbound] }

    { 59  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Inform. erforderlich;
                           ENU=SN No. Info. Must Exist];
                ToolTipML=[DEU=Gibt an, dass f�r Seriennummern in ausgehenden Belegzeilen ein Informationsdatensatz in dem Fenster "Seriennr.-Informationskarte" vorhanden sein muss.;
                           ENU=Specifies that serial numbers on outbound document lines must have an information record in the Serial No. Information Card.];
                SourceExpr="SN Info. Outbound Must Exist" }

    { 22  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Einkauf;
                           ENU=SN Purchase Tracking];
                ToolTipML=[DEU=Gibt an, dass ausgehende Einkaufsbelegzeilen Seriennummern ben�tigen.;
                           ENU=Specifies that outbound purchase document lines require serial numbers.];
                SourceExpr="SN Purchase Outbound Tracking" }

    { 24  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Verkauf;
                           ENU=SN Sales Tracking];
                ToolTipML=[DEU=Gibt an, dass ausgehende Verkaufsbelegzeilen Seriennummern ben�tigen.;
                           ENU=Specifies that outbound sales document lines require serial numbers.];
                SourceExpr="SN Sales Outbound Tracking" }

    { 26  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Zugang;
                           ENU=SN Positive Adjmt. Tracking];
                ToolTipML=[DEU=Gibt an, dass die ausgehenden Artikel Buch.-Blattzeilen der Art "Zugang" Seriennummern ben�tigen.;
                           ENU=Specifies that outbound item journal lines of type positive entry require serial numbers.];
                SourceExpr="SN Pos. Adjmt. Outb. Tracking" }

    { 28  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Abgang;
                           ENU=SN Negative Adjmt. Tracking];
                ToolTipML=[DEU=Gibt an, dass die ausgehenden Artikel Buch.-Blattzeilen der Art "Abgang" Seriennummern ben�tigen.;
                           ENU=Specifies that outbound item journal lines of type negative entry require serial numbers.];
                SourceExpr="SN Neg. Adjmt. Outb. Tracking" }

    { 3   ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Montage;
                           ENU=SN Assembly Tracking];
                ToolTipML=[DEU=Gibt an, dass Seriennummern f�r ausgehende Buchungen aus Montageauftr�gen ben�tigt werden.;
                           ENU=Indicates that serial numbers are required with outbound posting from assembly orders.];
                SourceExpr="SN Assembly Outbound Tracking" }

    { 67  ;3   ;Field     ;
                CaptionML=[DEU=Seriennr.-Verf. Produktion;
                           ENU=SN Manufacturing Tracking];
                ToolTipML=[DEU=Gibt an, dass Seriennummern f�r ausgehende Buchungen aus der Fertigung ben�tigt werden, also in der Regel Verbrauchsbuchungen.;
                           ENU=Specifies that serial numbers are required with outbound posting from production - typically consumption.];
                SourceExpr="SN Manuf. Outbound Tracking" }

    { 1903605001;1;Group  ;
                CaptionML=[DEU=Chargennr.;
                           ENU=Lot No.] }

    { 74  ;2   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 33  ;3   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass Sie bei der Bearbeitung einer ausgehenden Einheit immer angeben m�ssen, welche vorhandene Chargennummer betroffen sein soll.;
                           ENU=Specifies that when handling an outbound unit, always specify which existing lot number to handle.];
                SourceExpr="Lot Specific Tracking" }

    { 47  ;2   ;Group     ;
                CaptionML=[DEU=Eingang;
                           ENU=Inbound] }

    { 61  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Inform. erforderlich;
                           ENU=Lot No. Info. Must Exist];
                ToolTipML=[DEU=Gibt an, dass f�r Chargennummern in eingehenden Belegzeilen ein Informationsdatensatz in dem Fenster "Chargennr.-Informationskarte" vorhanden sein muss.;
                           ENU=Specifies that lot numbers on inbound document lines must have an information record in the Lot No. Information Card.];
                SourceExpr="Lot Info. Inbound Must Exist" }

    { 37  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Einkauf;
                           ENU=Lot Purchase Tracking];
                ToolTipML=[DEU=Gibt an, dass eingehende Einkaufsbelegzeilen eine Chargennummer ben�tigen.;
                           ENU=Specifies that inbound purchase document lines require a lot number.];
                SourceExpr="Lot Purchase Inbound Tracking" }

    { 39  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Verkauf;
                           ENU=Lot Sales Tracking];
                ToolTipML=[DEU=Gibt an, dass eingehende Verkaufsbelegzeilen eine Chargennummer ben�tigen.;
                           ENU=Specifies that inbound sales document lines require a lot number.];
                SourceExpr="Lot Sales Inbound Tracking" }

    { 41  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Zugang;
                           ENU=Lot Positive Adjmt. Tracking];
                ToolTipML=[DEU=Gibt an, dass die eingehenden Artikel Buch.-Blattzeilen der Art "Zugang" eine Chargennummer ben�tigen.;
                           ENU=Specifies that inbound item journal lines of type positive entry require a lot number.];
                SourceExpr="Lot Pos. Adjmt. Inb. Tracking" }

    { 43  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Abgang;
                           ENU=Lot Negative Adjmt. Tracking];
                ToolTipML=[DEU=Gibt an, dass die eingehenden Artikel Buch.-Blattzeilen der Art "Abgang" eine Chargennummer ben�tigen.;
                           ENU=Specifies that inbound item journal lines of type negative entry require a lot number.];
                SourceExpr="Lot Neg. Adjmt. Inb. Tracking" }

    { 5   ;3   ;Field     ;
                CaptionML=[DEU=Losmontageverfolgung;
                           ENU=Lot Assembly Tracking];
                ToolTipML=[DEU=Gibt an, dass Chargennummern f�r eingehende Buchungen aus Montageauftr�gen ben�tigt werden.;
                           ENU=Indicates that lot numbers are required with inbound posting from assembly orders.];
                SourceExpr="Lot Assembly Inbound Tracking" }

    { 69  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Produktion;
                           ENU=Lot Manufacturing Tracking];
                ToolTipML=[DEU=Gibt an, dass Chargennummern f�r ausgehende Buchungen aus der Fertigung ben�tigt werden, also in der Regel Istmeldungen.;
                           ENU=Specifies that lot numbers are required with outbound posting from production - typically output.];
                SourceExpr="Lot Manuf. Inbound Tracking" }

    { 81  ;2   ;Group      }

    { 72  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Lager;
                           ENU=Lot Warehouse Tracking];
                ToolTipML=[DEU=Gibt an, dass Logistikbelegzeilen eine Chargennummer ben�tigen.;
                           ENU=Specifies that warehouse document lines require a lot number.];
                SourceExpr="Lot Warehouse Tracking" }

    { 45  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Umlagerung;
                           ENU=Lot Transfer Tracking];
                ToolTipML=[DEU=Gibt an, dass Umlagerungsauftragszeilen eine Chargennummer ben�tigen.;
                           ENU=Specifies that transfer order lines require a lot number.];
                SourceExpr="Lot Transfer Tracking" }

    { 48  ;2   ;Group     ;
                CaptionML=[DEU=Ausgang;
                           ENU=Outbound] }

    { 63  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Inform. erforderlich;
                           ENU=Lot No. Info. Must Exist];
                ToolTipML=[DEU=Gibt an, dass f�r Chargennummern in ausgehenden Belegzeilen ein Informationsdatensatz in dem Fenster "Chargennr.-Informationskarte" vorhanden sein muss.;
                           ENU=Specifies that lot numbers on outbound document lines must have an information record in the Lot No. Information Card.];
                SourceExpr="Lot Info. Outbound Must Exist" }

    { 49  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Einkauf;
                           ENU=Lot Purchase Tracking];
                ToolTipML=[DEU=Gibt an, dass ausgehende Einkaufsbelegzeilen eine Chargennummer ben�tigen.;
                           ENU=Specifies that outbound purchase document lines require a lot number.];
                SourceExpr="Lot Purchase Outbound Tracking" }

    { 51  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Verkauf;
                           ENU=Lot Sales Tracking];
                ToolTipML=[DEU=Gibt an, dass ausgehende Verkaufsbelegzeilen eine Chargennummer ben�tigen.;
                           ENU=Specifies that outbound sales document lines require a lot number.];
                SourceExpr="Lot Sales Outbound Tracking" }

    { 53  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Zugang;
                           ENU=Lot Positive Adjmt. Tracking];
                ToolTipML=[DEU=Gibt an, dass die ausgehenden Artikel Buch.-Blattzeilen der Art "Zugang" eine Chargennummer ben�tigen.;
                           ENU=Specifies that outbound item journal lines of type positive entry require a lot number.];
                SourceExpr="Lot Pos. Adjmt. Outb. Tracking" }

    { 55  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Abgang;
                           ENU=Lot Negative Adjmt. Tracking];
                ToolTipML=[DEU=Gibt an, dass die ausgehenden Artikel Buch.-Blattzeilen der Art "Abgang" eine Chargennummer ben�tigen.;
                           ENU=Specifies that outbound item journal lines of type negative entry require a lot number.];
                SourceExpr="Lot Neg. Adjmt. Outb. Tracking" }

    { 7   ;3   ;Field     ;
                CaptionML=[DEU=Losmontageverfolgung;
                           ENU=Lot Assembly Tracking];
                ToolTipML=[DEU=Gibt an, dass Chargennummern f�r ausgehende Buchungen aus Montageauftr�gen ben�tigt werden.;
                           ENU=Indicates that lot numbers are required with outbound posting from assembly orders.];
                SourceExpr="Lot Assembly Outbound Tracking" }

    { 70  ;3   ;Field     ;
                CaptionML=[DEU=Chargennr.-Verf. Produktion;
                           ENU=Lot Manufacturing Tracking];
                ToolTipML=[DEU=Gibt an, dass Chargennummern f�r ausgehende Buchungen aus der Fertigung ben�tigt werden, also in der Regel Verbrauchsbuchungen.;
                           ENU=Specifies that lot numbers are required with outbound posting from production - typically consumption.];
                SourceExpr="Lot Manuf. Outbound Tracking" }

    { 1905489801;1;Group  ;
                CaptionML=[DEU=Sonst.;
                           ENU=Misc.] }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Formel fest, die das Garantiedatum berechnet, das in der Artikelverfolgungszeile in das Feld "Garantiedatum" eingegeben wurde.;
                           ENU=Specifies the formula that calculates the warranty date entered in the Warranty Date field on item tracking line.];
                SourceExpr="Warranty Date Formula" }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass das Garantiedatum manuell eingegeben werden muss.;
                           ENU=Specifies that a warranty date must be entered manually.];
                SourceExpr="Man. Warranty Date Entry Reqd." }

    { 52  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass Sie in der Artikelverfolgungszeile manuell ein Ablaufdatum eingeben m�ssen.;
                           ENU=Specifies that you must manually enter an expiration date on the item tracking line.];
                SourceExpr="Man. Expir. Date Entry Reqd." }

    { 65  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass ein Ablaufdatum, das der Artikelverfolgungsnummer beim Wareneingang zugewiesen wurde, beim Warenausgang ber�cksichtigt werden muss.;
                           ENU=Specifies that an expiration date assigned to the item tracking number as it entered inventory must be respected when it exits inventory.];
                SourceExpr="Strict Expiration Posting" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

