OBJECT Page 449 Finance Charge Memo Statistics
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Zinsrechnungsstatistik;
               ENU=Finance Charge Memo Statistics];
    LinksAllowed=No;
    SourceTable=Table302;
    PageType=Card;
    OnAfterGetRecord=VAR
                       CustPostingGr@1005 : Record 92;
                       GLAcc@1004 : Record 15;
                       VATPostingSetup@1003 : Record 325;
                       VATInterest@1001 : Decimal;
                     BEGIN
                       CALCFIELDS("Interest Amount","VAT Amount");
                       FinChrgMemoTotal := "Additional Fee" + "Interest Amount" + "VAT Amount";
                       IF "Customer No." <> '' THEN BEGIN
                         CustPostingGr.GET("Customer Posting Group");
                         CustPostingGr.TESTFIELD("Interest Account");
                         GLAcc.GET(CustPostingGr."Interest Account");
                         VATPostingSetup.GET("VAT Bus. Posting Group",GLAcc."VAT Prod. Posting Group");
                         VATInterest := VATPostingSetup."VAT %";
                         GLAcc.GET(CustPostingGr."Additional Fee Account");
                         VATPostingSetup.GET("VAT Bus. Posting Group",GLAcc."VAT Prod. Posting Group");
                         Interest := (FinChrgMemoTotal - "Additional Fee" * (VATPostingSetup."VAT %" / 100 + 1)) /
                           (VATInterest / 100 + 1);
                         VatAmount := Interest * VATInterest / 100 +
                           "Additional Fee" * VATPostingSetup."VAT %" / 100;
                       END;
                       IF Cust.GET("Customer No.") THEN
                         Cust.CALCFIELDS("Balance (LCY)")
                       ELSE
                         CLEAR(Cust);
                       IF Cust."Credit Limit (LCY)" = 0 THEN
                         CreditLimitLCYExpendedPct := 0
                       ELSE
                         CreditLimitLCYExpendedPct := ROUND(Cust."Balance (LCY)" / Cust."Credit Limit (LCY)" * 10000,1);
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 3   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 13  ;2   ;Field     ;
                DrillDown=No;
                CaptionML=[DEU=Zinsbetrag;
                           ENU=Interest Amount];
                SourceExpr=Interest }

    { 1   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[DEU=Legt die Summe der Zusatzgeb�hren der Zinsrechnungszeilen fest.;
                           ENU=Specifies the total of the additional fee amounts on the finance charge memo lines.];
                SourceExpr="Additional Fee" }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                CaptionML=[DEU=MwSt.-Betrag;
                           ENU=VAT Amount];
                SourceExpr=VatAmount }

    { 6   ;2   ;Field     ;
                CaptionML=[DEU=Gesamt;
                           ENU=Total];
                SourceExpr=FinChrgMemoTotal;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1903289601;1;Group  ;
                CaptionML=[DEU=Debitor;
                           ENU=Customer] }

    { 11  ;2   ;Field     ;
                CaptionML=[DEU=Saldo (MW);
                           ENU=Balance (LCY)];
                SourceExpr=Cust."Balance (LCY)";
                AutoFormatType=1 }

    { 8   ;2   ;Field     ;
                CaptionML=[DEU=Kreditlimit (MW);
                           ENU=Credit Limit (LCY)];
                SourceExpr=Cust."Credit Limit (LCY)";
                AutoFormatType=1 }

    { 10  ;2   ;Field     ;
                ExtendedDatatype=Ratio;
                CaptionML=[DEU=Kreditlimit (MW) �berschritten %;
                           ENU=Expended % of Credit Limit (LCY)];
                ToolTipML=[DEU=Gibt den erweiterten Prozentsatz des Kreditlimits in MW an.;
                           ENU=Specifies the expended percentage of the credit limit in (LCY).];
                SourceExpr=CreditLimitLCYExpendedPct }

  }
  CODE
  {
    VAR
      Cust@1000 : Record 18;
      FinChrgMemoTotal@1001 : Decimal;
      CreditLimitLCYExpendedPct@1002 : Decimal;
      Interest@1003 : Decimal;
      VatAmount@1004 : Decimal;

    BEGIN
    END.
  }
}

