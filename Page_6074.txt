OBJECT Page 6074 Filed Service Contract Subform
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Zeilen;
               ENU=Lines];
    MultipleNewLines=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    LinksAllowed=No;
    SourceTable=Table5971;
    DelayedInsert=Yes;
    PageType=ListPart;
    AutoSplitKey=No;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                Editable=TRUE;
                GroupType=Repeater }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Serviceartikels in der archivierten Servicevertragszeile an.;
                           ENU=Specifies the number of the service item on the filed service contract line.];
                SourceExpr="Service Item No." }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Serviceartikelgruppe an, der die archivierte Serviceartikelzeile zugeordnet ist.;
                           ENU=Specifies a description of the service item group associated with the filed service item line.];
                SourceExpr=Description }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Seriennummer des Serviceartikels in der archivierten Artikelzeile an.;
                           ENU=Specifies the serial number of the service item on the filed service item line.];
                SourceExpr="Serial No." }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die mit dem Serviceartikel im archivierten Vertrag verkn�pfte Artikelnummer an.;
                           ENU=Specifies the item number linked to the service item in the filed contract.];
                SourceExpr="Item No." }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Serviceartikelgruppe an, der diesem Serviceartikel zugeordnet ist.;
                           ENU=Specifies the code for the service item group associated with this service item.];
                SourceExpr="Service Item Group Code" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Einheit an, die verwendet wurde, als dieser Serviceartikel verkauft wurde.;
                           ENU=Specifies the code of the unit of measure used when the service item was sold.];
                SourceExpr="Unit of Measure Code";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das gesch�tzte Zeitintervall an, nach dem die Arbeit am Serviceauftrag beginnt.;
                           ENU=Specifies the estimated time interval after work on the service order starts.];
                SourceExpr="Response Time (Hours)" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Zeileneinstandsbetrag der Artikelzeile im archivierten Servicevertrag oder archivierten Servicevertragsangebot an.;
                           ENU=Specifies the calculated cost of the item line in the filed service contract or filed service contract quote.];
                SourceExpr="Line Cost" }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Wert der Serviceartikelzeile in diesem Servicevertrag an.;
                           ENU=Specifies the value on the service item line in the service contract.];
                SourceExpr="Line Value" }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den prozentualen Zeilenrabatt an, der in der Serviceartikelzeile angegeben wird.;
                           ENU=Specifies the line discount percentage provided on the service item line.];
                SourceExpr="Line Discount %" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Rabattbetrag der Vertragszeile im archivierten Servicevertrag oder Vertragsangebot an.;
                           ENU=Specifies the amount of the discount on the contract line in the filed service contract or filed contract quote.];
                SourceExpr="Line Discount Amount" }

    { 48  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Nettobetrag (vor Abzug des Rechnungsrabatts) der Serviceartikelzeile an.;
                           ENU=Specifies the net amount (excluding the invoice discount amount) of the service item line.];
                SourceExpr="Line Amount" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Deckungsbeitrag der Vertragszeile im archivierten Servicevertrag oder Vertragsangebot an.;
                           ENU=Specifies the profit on the contract line in the filed service contract or filed contract quote.];
                SourceExpr=Profit }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem der Vertrag zuletzt fakturiert wurde.;
                           ENU=Specifies the date when the contract was last invoiced.];
                SourceExpr="Invoiced to Date" }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die gesch�tzte Zeit an, die verstreicht, bis der Service f�r diesen Serviceartikel beginnt.;
                           ENU=Specifies the estimated time that elapses until service starts on the service item.];
                SourceExpr="Service Period" }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum des letzten geplanten Service in diesem Artikel an.;
                           ENU=Specifies the date of the last planned service on this item.];
                SourceExpr="Last Planned Service Date" }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum des n�chsten geplanten Service an diesem Artikel an.;
                           ENU=Specifies the date of the next planned service on this item.];
                SourceExpr="Next Planned Service Date" }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum des letzten tats�chlichen Service an diesem Artikel an.;
                           ENU=Specifies the date of the last actual service on this item.];
                SourceExpr="Last Service Date" }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum der letzten durchgef�hrten pr�ventiven Serviceleistung f�r diesen Artikel an.;
                           ENU=Specifies the date when the last time preventative service was performed on this item.];
                SourceExpr="Last Preventive Maint. Date" }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Erstellungsdatum der Gutschrift f�r den Artikel an, der aus dem Servicevertrag entfernt werden muss.;
                           ENU=Specifies the date when you can create a credit memo for the item that needs to be removed from the service contract.];
                SourceExpr="Credit Memo Date" }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem der Serviceartikel aus dem Servicevertrag entfernt werden soll.;
                           ENU=Specifies the date when the service item should be removed from the service contract.];
                SourceExpr="Contract Expiration Date" }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass diese Servicevertragszeile eine neue Zeile ist.;
                           ENU=Specifies that this service contract line is a new line.];
                SourceExpr="New Line" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

