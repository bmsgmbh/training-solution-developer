OBJECT Page 167 Item Ledger Entries Preview
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Artikelposten-Vorschau;
               ENU=Item Ledger Entries Preview];
    SourceTable=Table32;
    DataCaptionFields=Item No.;
    PageType=List;
    SourceTableTemporary=Yes;
    OnAfterGetRecord=BEGIN
                       CalcAmounts;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 60      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Posten;
                                 ENU=Ent&ry];
                      Image=Entry }
      { 61      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Zeigt Dimensionen an oder bearbeitet sie, wie etwa einen Bereich, ein Projekt oder eine Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 64      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=&Wertposten;
                                 ENU=&Value Entries];
                      ToolTipML=[DEU=Zeigt Betr�ge an, die sich auf einen Artikel beziehen. Wenn Sie �nderungen an einem Wert f�r Artikel im Bestand vornehmen, z. B. einen Auftrag buchen, wird mindestens ein Wertposten hinzugef�gt.;
                                 ENU=View amounts that relate to an item. Whenever you do something that changes a value for items in the inventory, like post an order, one or more value entries are added.];
                      RunObject=Page 5802;
                      RunPageView=SORTING(Item Ledger Entry No.);
                      RunPageLink=Item Ledger Entry No.=FIELD(Entry No.);
                      Image=ValueLedger }
      { 30      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Ausgleich;
                                 ENU=&Application];
                      Image=Apply }
      { 58      ;2   ;Action    ;
                      CaptionML=[DEU=&Ausgeglichene Posten;
                                 ENU=Applied E&ntries];
                      ToolTipML=[DEU=Zeigt die Posten an, die auf diesen Datensatz angewendet wurden.;
                                 ENU=View the ledger entries that have been applied to this record.];
                      ApplicationArea=#Basic,#Suite;
                      Image=Approve;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Show Applied Entries",Rec);
                               END;
                                }
      { 56      ;2   ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[DEU=Reservierungsposten;
                                 ENU=Reservation Entries];
                      ToolTipML=[DEU=Zeigen Sie alle Reservierungen f�r den Artikel an. Die Elemente k�nnen beispielsweise f�r Fertigungsauftr�ge reserviert werden.;
                                 ENU=View all reservations for the item. For example, items can be reserved for production orders or production orders.];
                      ApplicationArea=#Basic,#Suite;
                      Image=ReservationLedger;
                      OnAction=BEGIN
                                 ShowReservationEntries(TRUE);
                               END;
                                }
      { 86      ;2   ;Action    ;
                      CaptionML=[DEU=Ausgleichsvorschlag;
                                 ENU=Application Worksheet];
                      ToolTipML=[DEU=Zeigen Sie Artikelausgleiche an, die automatisch zwischen Artikelposten w�hrend Artikeltransaktionen erstellt werden.;
                                 ENU=View item applications that are automatically created between item ledger entries during item transactions.];
                      ApplicationArea=#Basic,#Suite;
                      Image=ApplicationWorksheet;
                      OnAction=VAR
                                 ApplicationWorksheet@1000 : Page 521;
                               BEGIN
                                 CLEAR(ApplicationWorksheet);
                                 ApplicationWorksheet.SetRecordToShow(Rec);
                                 ApplicationWorksheet.RUN;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 57      ;1   ;ActionGroup;
                      CaptionML=[DEU=Fun&ktionen;
                                 ENU=F&unctions];
                      Image=Action }
      { 59      ;2   ;Action    ;
                      CaptionML=[DEU=&Bedarfsverursacher;
                                 ENU=Order &Tracking];
                      ToolTipML=[DEU=Verfolgt die Verbindung eines Vorrats zum entsprechenden Bedarf. So k�nnen Sie den urspr�nglichen Bedarf finden, anhand dessen ein spezifischer Fertigungsauftrag oder eine Bestellung erstellt wurde.;
                                 ENU=Tracks the connection of a supply to its corresponding demand. This can help you find the original demand that created a specific production order or purchase order.];
                      ApplicationArea=#Basic,#Suite;
                      Image=OrderTracking;
                      OnAction=VAR
                                 OrderTrackingForm@1001 : Page 99000822;
                               BEGIN
                                 OrderTrackingForm.SetItemLedgEntry(Rec);
                                 OrderTrackingForm.RUNMODAL;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Buchungsdatum des Postens an.;
                           ENU=Specifies the entry's posting date.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, aus welcher Art Transaktion der Posten erstellt wurde.;
                           ENU=Specifies which type of transaction the entry is created from.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry Type" }

    { 76  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, welche Belegart zum Erstellen des Artikelpostens gebucht wurde.;
                           ENU=Specifies what type of document was posted to create the item ledger entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Type" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Belegnummer des Postens an. Der Beleg ist das Dokument, das die Basis des Postens darstellte, z.�B. ein Lieferschein.;
                           ENU=Specifies the document number on the entry. The document is the voucher that the entry was based on, for example, a receipt.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No." }

    { 78  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Zeile des gebuchten Belegs an, die dem Artikelposten entspricht.;
                           ENU=Specifies the number of the line on the posted document that corresponds to the item ledger entry.];
                SourceExpr="Document Line No.";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels im Posten an.;
                           ENU=Specifies the number of the item in the entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Item No." }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt den Variantencode f�r die Artikel an.;
                           ENU=Shows the variant code for the items.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Postens an.;
                           ENU=Specifies a description of the entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt einen Code, der erkl�rt, warum der Artikel zur�ckgegeben wird.;
                           ENU=Contains a code that explains why the item is returned.];
                SourceExpr="Return Reason Code";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt den Dimensionswertcode an, mit dem der Posten verkn�pft ist.;
                           ENU=Shows the dimension value code that the entry is linked to.];
                SourceExpr="Global Dimension 1 Code";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt den Dimensionswertcode an, mit dem der Posten verkn�pft ist.;
                           ENU=Shows the dimension value code that the entry is linked to.];
                SourceExpr="Global Dimension 2 Code";
                Visible=FALSE }

    { 82  ;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt das letzte Datum, an dem der Artikel in der Zeile verwendet werden kann.;
                           ENU=Contains the last date that the item on the line can be used.];
                SourceExpr="Expiration Date";
                Visible=FALSE }

    { 68  ;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt eine Seriennummer, wenn es f�r den gebuchten Artikel eine derartige Nummer gibt.;
                           ENU=Contains a serial number if the posted item carries such a number.];
                SourceExpr="Serial No.";
                Visible=FALSE }

    { 70  ;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt eine Chargennummer, wenn es f�r den gebuchten Artikel eine derartige Nummer gibt.;
                           ENU=Contains a lot number if the posted item carries such a number.];
                SourceExpr="Lot No.";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt den Code des Lagerorts an, mit dem der Posten verbunden ist.;
                           ENU=Shows the code for the location that the entry is linked to.];
                SourceExpr="Location Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Einheiten des Artikels im Artikelposten an.;
                           ENU=Specifies the number of units of the item in the item entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Quantity }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viele Einheiten des Artikels in der Zeile fakturiert wurden.;
                           ENU=Specifies how many units of the item on the line have been invoiced.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Invoiced Quantity";
                Visible=TRUE }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt die im Lager verbleibende Menge im Feld "Menge" an, wenn der Posten einen Zugang darstellt (einen Einkauf oder Zugang). Wenn der Posten einen Abgang darstellt (einen Verkauf oder Abgang), wird in diesem Feld die Menge angezeigt, die noch mit einem Lagerzugangsposten ausgeglichen werden kann.;
                           ENU=Specifies the quantity that remains in inventory in the Quantity field if the entry is an increase (a purchase or positive adjustment). If the entry is a decrease (a sale or negative adjustment), the field shows the quantity that remains to be applied to by an increase entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Remaining Quantity";
                Visible=TRUE }

    { 84  ;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt f�r diesen Artikelposten die Menge, die geliefert, aber noch nicht zur�ckgesendet wurde.;
                           ENU=Contains the quantity for this item ledger entry that was shipped and has not yet been returned.];
                SourceExpr="Shipped Qty. Not Returned";
                Visible=FALSE }

    { 117 ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt an, wie viele Einheiten des Artikels in der Zeile reserviert wurden.;
                           ENU=Shows how many units of the item on the line have been reserved.];
                SourceExpr="Reserved Quantity";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt die Menge pro Artikeleinheit an.;
                           ENU=Shows the quantity per item unit of measure.];
                SourceExpr="Qty. per Unit of Measure";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                CaptionML=[DEU=Verkaufsbetrag (erwartet);
                           ENU=Sales Amount (Expected)];
                SourceExpr=SalesAmountExpected;
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                CaptionML=[DEU=Verkaufsbetrag (tats�chl.);
                           ENU=Sales Amount (Actual)];
                ToolTipML=[DEU=Gibt die Summe der Ist-Verkaufsbetr�ge bei der Buchung an.;
                           ENU=Specifies the sum of the actual sales amounts if you post.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=SalesAmountActual }

    { 31  ;2   ;Field     ;
                CaptionML=[DEU=Einstandsbetrag (erwartet);
                           ENU=Cost Amount (Expected)];
                SourceExpr=CostAmountExpected;
                Visible=FALSE }

    { 41  ;2   ;Field     ;
                CaptionML=[DEU=Einstandsbetrag (tats�chl.);
                           ENU=Cost Amount (Actual)];
                ToolTipML=[DEU=Gibt die Summe der Ist-Einstandsbetr�ge bei der Buchung an.;
                           ENU=Specifies the sum of the actual cost amounts if you post.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=CostAmountActual }

    { 47  ;2   ;Field     ;
                CaptionML=[DEU=Einst.-Betr. (lagerwertunabh.);
                           ENU=Cost Amount (Non-Invtbl.)];
                ToolTipML=[DEU=Gibt die Summe der lagerwertunabh�ngigen Ist-Einstandsbetr�ge bei der Buchung an. Typische lagerwertunabh�ngige Kosten stammen aus Artikel Zu-/Abschl�gen.;
                           ENU=Specifies the sum of the actual non-inventoriable cost amounts if you post. Typical non-inventoriable costs come from item charges.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=CostAmountNonInvtbl }

    { 49  ;2   ;Field     ;
                Name=CostAmountExpectedACY;
                CaptionML=[DEU=Einstandsbetrag (erw.) (BW);
                           ENU=Cost Amount (Expected) (ACY)];
                SourceExpr=CostAmountExpectedACY;
                Visible=FALSE }

    { 72  ;2   ;Field     ;
                Name=CostAmountActualACY;
                CaptionML=[DEU=Einst.-Betrag (tats�chl.) (BW);
                           ENU=Cost Amount (Actual) (ACY)];
                SourceExpr=CostAmountActualACY;
                Visible=FALSE }

    { 74  ;2   ;Field     ;
                CaptionML=[DEU=Einst.-B. (lagerw.-unabh.) (BW);
                           ENU=Cost Amount (Non-Invtbl.) (ACY)];
                SourceExpr=CostAmountNonInvtblACY;
                Visible=FALSE }

    { 66  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt an, ob der Posten komplett fakturiert wurde oder ob weitere gebuchte Rechnungen erwartet werden. Nur komplett fakturierte Posten k�nnen neu bewertet werden.;
                           ENU=Shows if the entry has been fully invoiced or if more posted invoices are expected. Only completely invoiced entries can be revalued.];
                SourceExpr="Completely Invoiced";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob der Posten vollst�ndig ausgeglichen wurde.;
                           ENU=Specifies if the entry has been fully applied to.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Open }

    { 80  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt an, ob die Artikel in der Zeile direkt an den Debitor geliefert wurden.;
                           ENU=Shows whether the items on the line have been shipped directly to the customer.];
                SourceExpr="Drop Shipment";
                Visible=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Buchung einen Auftragsmontageverkauf darstellt.;
                           ENU=Specifies if the posting represents an assemble-to-order sale.];
                SourceExpr="Assemble to Order";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob es einen oder mehrere ausgeglichene Posten gibt, die reguliert werden m�ssen.;
                           ENU=Specifies whether there is one or more applied entries, which need to be adjusted.];
                SourceExpr="Applied Entry to Adjust";
                Visible=FALSE }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, aus welcher Art Transaktion der Posten erstellt wurde.;
                           ENU=Specifies which type of transaction the entry is created from.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Order Type" }

    { 54  ;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt die Nummer des Auftrags, der den Posten erstellt hat.;
                           ENU=Contains the number of the order that created the entry.];
                SourceExpr="Order No.";
                Visible=FALSE }

    { 51  ;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt die Zeilennummer des Auftrags, der den Posten erzeugt hat.;
                           ENU=Contains the line number of the order that created the entry.];
                SourceExpr="Order Line No.";
                Visible=FALSE }

    { 63  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt die Zeilennummer der FA-Komponente an.;
                           ENU=Shows the line number of the production order component.];
                SourceExpr="Prod. Order Comp. Line No.";
                Visible=FALSE }

    { 1000;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt die Nummer des Projekts, das mit dem Posten verkn�pft ist.;
                           ENU=Contains the number of the job associated with the entry.];
                SourceExpr="Job No.";
                Visible=FALSE }

    { 1002;2   ;Field     ;
                ToolTipML=[DEU=Enth�lt die Nummer der Projektaufgabe, die mit dem Posten verkn�pft ist.;
                           ENU=Contains the number of the job task associated with the entry.];
                SourceExpr="Job Task No.";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      TempValueEntry@1009 : TEMPORARY Record 5802;
      SalesAmountExpected@1000 : Decimal;
      SalesAmountActual@1001 : Decimal;
      CostAmountExpected@1002 : Decimal;
      CostAmountActual@1003 : Decimal;
      CostAmountNonInvtbl@1004 : Decimal;
      CostAmountExpectedACY@1005 : Decimal;
      CostAmountActualACY@1006 : Decimal;
      CostAmountNonInvtblACY@1007 : Decimal;

    PROCEDURE Set@3(VAR TempItemLedgerEntry2@1000 : TEMPORARY Record 32;VAR TempValueEntry2@1001 : TEMPORARY Record 5802);
    BEGIN
      IF TempItemLedgerEntry2.FINDSET THEN
        REPEAT
          Rec := TempItemLedgerEntry2;
          INSERT;
        UNTIL TempItemLedgerEntry2.NEXT = 0;

      IF TempValueEntry2.FINDSET THEN
        REPEAT
          TempValueEntry := TempValueEntry2;
          TempValueEntry.INSERT;
        UNTIL TempValueEntry2.NEXT = 0;
    END;

    LOCAL PROCEDURE CalcAmounts@5();
    BEGIN
      SalesAmountExpected := 0;
      SalesAmountActual := 0;
      CostAmountExpected := 0;
      CostAmountActual := 0;
      CostAmountNonInvtbl := 0;
      CostAmountExpectedACY := 0;
      CostAmountActualACY := 0;
      CostAmountNonInvtblACY := 0;

      TempValueEntry.SETFILTER("Item Ledger Entry No.",'%1',"Entry No.");
      IF TempValueEntry.FINDSET THEN
        REPEAT
          SalesAmountExpected += TempValueEntry."Sales Amount (Expected)";
          SalesAmountActual += TempValueEntry."Sales Amount (Actual)";
          CostAmountExpected += TempValueEntry."Cost Amount (Expected)";
          CostAmountActual += TempValueEntry."Cost Amount (Actual)";
          CostAmountNonInvtbl += TempValueEntry."Cost Amount (Non-Invtbl.)";
          CostAmountExpectedACY += TempValueEntry."Cost Amount (Expected) (ACY)";
          CostAmountActualACY += TempValueEntry."Cost Amount (Actual) (ACY)";
          CostAmountNonInvtblACY += TempValueEntry."Cost Amount (Non-Invtbl.)(ACY)";
        UNTIL TempValueEntry.NEXT = 0;
    END;

    BEGIN
    END.
  }
}

