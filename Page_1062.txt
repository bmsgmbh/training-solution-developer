OBJECT Page 1062 Select Payment Service Type
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Zahlungsverkehrtyp ausw�hlen;
               ENU=Select Payment Service Type];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table1060;
    DataCaptionExpr='';
    PageType=StandardDialog;
    SourceTableTemporary=Yes;
    OnOpenPage=BEGIN
                 OnRegisterPaymentServiceProviders(Rec);
               END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 4   ;1   ;Group     ;
                Editable=FALSE;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Zahlungsservicetyps an.;
                           ENU=Specifies the name of the payment service type.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des Zahlungsservices an.;
                           ENU=Specifies the description of the payment service.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

  }
  CODE
  {

    BEGIN
    END.
  }
}

