OBJECT Page 5080 Job Responsibilities
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Verantwortlichkeiten;
               ENU=Job Responsibilities];
    SourceTable=Table5066;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Verantwortlichkeit;
                                 ENU=&Job Responsibility];
                      Image=Job }
      { 12      ;2   ;Action    ;
                      CaptionML=[DEU=&Kontakte;
                                 ENU=C&ontacts];
                      ToolTipML=[DEU=Zeigen Sie eine Liste der Kontakte an, die der spezifischen Verantwortlichkeit zugeordnet sind.;
                                 ENU=View a list of contacts that are associated with the specific job responsibility.];
                      ApplicationArea=#RelationshipMgmt;
                      RunObject=Page 5083;
                      RunPageLink=Job Responsibility Code=FIELD(Code);
                      Image=CustomerContact }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Verantwortlichkeit an.;
                           ENU=Specifies the code for the job responsibility.];
                ApplicationArea=#All;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung der Verantwortlichkeit an.;
                           ENU=Specifies the description of the job responsibility.];
                ApplicationArea=#All;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Kontakte an, denen die Verantwortlichkeit zugeordnet wurde.;
                           ENU=Specifies the number of contacts that have been assigned the job responsibility.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No. of Contacts";
                DrillDownPageID=Job Responsibility Contacts }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

