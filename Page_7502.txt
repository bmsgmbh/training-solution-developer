OBJECT Page 7502 Item Attribute Translations
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Artikelattribut�bersetzungen;
               ENU=Item Attribute Translations];
    SourceTable=Table7502;
    DataCaptionFields=Attribute ID;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Sprachcode an.;
                           ENU=Specifies a language code.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Language Code";
                LookupPageID=Languages }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den �bersetzten Namen des Artikelattributs an.;
                           ENU=Specifies the translated name of the item attribute.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

  }
  CODE
  {

    BEGIN
    END.
  }
}

