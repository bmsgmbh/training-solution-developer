OBJECT Page 5204 Alternative Address List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Alternative Adressen�bersicht;
               ENU=Alternative Address List];
    SourceTable=Table5201;
    DataCaptionFields=Employee No.;
    PageType=List;
    CardPageID=Alternative Address Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 28      ;1   ;ActionGroup;
                      CaptionML=[DEU=Ad&resse;
                                 ENU=&Address];
                      Image=Addresses }
      { 27      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 5222;
                      RunPageLink=Table Name=CONST(Alternative Address),
                                  No.=FIELD(Employee No.),
                                  Alternative Address Code=FIELD(Code);
                      Image=ViewComments }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code f�r die alternative Adresse des Mitarbeiters an.;
                           ENU=Specifies a code for the employee's alternate address.];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Nachnamen des Mitarbeiters an.;
                           ENU=Specifies the employee's last name.];
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Vornamen des Mitarbeiters oder einen alternativen Namen an.;
                           ENU=Specifies the employee's first name, or an alternate name.];
                SourceExpr="Name 2";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine alternative Adresse f�r den Mitarbeiter an.;
                           ENU=Specifies an alternate address for the employee.];
                SourceExpr=Address }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine weitere Zeile einer alternativen Adresse f�r den Mitarbeiter an.;
                           ENU=Specifies another line of an alternate address for the employee.];
                SourceExpr="Address 2";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ort der alternativen Adresse an.;
                           ENU=Specifies the city of the alternate address.];
                SourceExpr=City;
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postleitzahl der alternativen Adresse an.;
                           ENU=Specifies the postal code of the alternate address.];
                SourceExpr="Post Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Bundesregion der alternativen Adresse des Mitarbeiters an.;
                           ENU=Specifies the county of the employee's alternate address.];
                SourceExpr=County;
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Telefonnummer des Mitarbeiters an der alternativen Adresse an.;
                           ENU=Specifies the employee's telephone number at the alternate address.];
                SourceExpr="Phone No." }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Faxnummer des Mitarbeiters an der alternativen Adresse an.;
                           ENU=Specifies the employee's fax number at the alternate address.];
                SourceExpr="Fax No.";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die alternative E-Mail-Adresse des Mitarbeiters an.;
                           ENU=Specifies the employee's alternate email address.];
                SourceExpr="E-Mail";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob eine Bemerkung f�r diesen Posten eingegeben wurde.;
                           ENU=Specifies if a comment was entered for this entry.];
                SourceExpr=Comment }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

