OBJECT Page 1805 Email Setup Wizard
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=E-Mail-Einrichtung;
               ENU=Email Setup];
    SourceTable=Table409;
    PageType=NavigatePage;
    SourceTableTemporary=Yes;
    OnInit=BEGIN
             LoadTopBanners;
           END;

    OnOpenPage=VAR
                 SMTPMailSetup@1000 : Record 409;
                 CompanyInformation@1001 : Record 79;
               BEGIN
                 INIT;
                 IF SMTPMailSetup.GET THEN BEGIN
                   TRANSFERFIELDS(SMTPMailSetup);
                   EmailProvider := EmailProvider::Other;
                 END ELSE BEGIN
                   SMTPMail.ApplyOffice365Smtp(Rec);
                   EmailProvider := EmailProvider::"Office 365";
                   IF CompanyInformation.GET THEN
                     "User ID" := CompanyInformation."E-Mail";
                 END;
                 INSERT;
                 IF SMTPMailSetup.HasPassword THEN
                   Password := DummyPasswordTxt;

                 Step := Step::Start;
                 EnableControls;
               END;

    OnQueryClosePage=BEGIN
                       IF CloseAction = ACTION::OK THEN
                         IF AssistedSetup.GetStatus(PAGE::"Email Setup Wizard") = AssistedSetup.Status::"Not Completed" THEN
                           IF NOT CONFIRM(NAVNotSetUpQst,FALSE) THEN
                             ERROR('');
                     END;

    ActionList=ACTIONS
    {
      { 10      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 15      ;1   ;Action    ;
                      Name=ActionBack;
                      CaptionML=[DEU=Zur�ck;
                                 ENU=Back];
                      ApplicationArea=#Basic,#Suite;
                      Enabled=BackActionEnabled;
                      InFooterBar=Yes;
                      Image=PreviousRecord;
                      OnAction=BEGIN
                                 NextStep(TRUE);
                               END;
                                }
      { 14      ;1   ;Action    ;
                      Name=ActionNext;
                      CaptionML=[DEU=Weiter;
                                 ENU=Next];
                      ApplicationArea=#Basic,#Suite;
                      Enabled=NextActionEnabled;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 CASE Step OF
                                   Step::Settings:
                                     IF (Authentication = Authentication::Basic) AND (("User ID" = '') OR (Password = '')) THEN
                                       ERROR(EmailPasswordMissingErr);
                                 END;

                                 NextStep(FALSE);
                               END;
                                }
      { 16      ;1   ;Action    ;
                      Name=ActionSendTestEmail;
                      CaptionML=[DEU=Test-E-Mail senden;
                                 ENU=Send Test Email];
                      ApplicationArea=#Basic,#Suite;
                      Enabled=FinishActionEnabled;
                      InFooterBar=Yes;
                      Image=Email;
                      OnAction=BEGIN
                                 SendTestEmailAction;
                               END;
                                }
      { 11      ;1   ;Action    ;
                      Name=ActionFinish;
                      CaptionML=[DEU=Fertig stellen;
                                 ENU=Finish];
                      ApplicationArea=#Basic,#Suite;
                      Enabled=FinishActionEnabled;
                      InFooterBar=Yes;
                      Image=Approve;
                      OnAction=BEGIN
                                 FinishAction;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 96  ;1   ;Group     ;
                Visible=TopBannerVisible AND NOT FinalStepVisible;
                Editable=FALSE;
                GroupType=Group }

    { 97  ;2   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaRepositoryStandard.Image;
                Editable=FALSE;
                ShowCaption=No }

    { 98  ;1   ;Group     ;
                Visible=TopBannerVisible AND FinalStepVisible;
                Editable=FALSE;
                GroupType=Group }

    { 99  ;2   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaRepositoryDone.Image;
                Editable=FALSE;
                ShowCaption=No }

    { 20  ;1   ;Group     ;
                Visible=FirstStepVisible;
                GroupType=Group }

    { 13  ;2   ;Group     ;
                CaptionML=[DEU=Willkommen bei der E-Mail-Einrichtung;
                           ENU=Welcome to Email Setup];
                Visible=FirstStepVisible;
                GroupType=Group }

    { 18  ;3   ;Group     ;
                GroupType=Group;
                InstructionalTextML=[DEU=Um E-Mails anhand von Aktionen auf Belegen zu senden, z.�B. im Fenster f�r die Verkaufsrechnung, m�ssen Sie sich mit dem entsprechenden E-Mail-Konto anmelden.;
                                     ENU=To send email messages using actions on documents, such as the Sales Invoice window, you must log on to the relevant email account.] }

    { 19  ;3   ;Group     ;
                GroupType=Group;
                InstructionalTextML=[DEU=E-Mails k�nnen dann direkt an Debitoren und zwischen Genehmigungsworkflowbenutzern gesendet werden.;
                                     ENU=Email messages can then be sent directly to customers and between approval workflow users.] }

    { 21  ;2   ;Group     ;
                CaptionML=[DEU=Los geht's!;
                           ENU=Let's go!];
                GroupType=Group }

    { 22  ;3   ;Group     ;
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie 'Weiter', damit Sie das Senden von E-Mails �ber Belege einrichten k�nnen.;
                                     ENU=Choose Next so you can set up email sending from documents.] }

    { 2   ;1   ;Group     ;
                Visible=ProviderStepVisible;
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie Ihren E-Mail-Anbieter.;
                                     ENU=Choose your email provider.] }

    { 3   ;2   ;Field     ;
                Name=Email Provider;
                CaptionML=[DEU=E-Mail-Anbieter;
                           ENU=Email Provider];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=EmailProvider;
                OnValidate=BEGIN
                             IF EmailProvider = EmailProvider::"Office 365" THEN
                               SMTPMail.ApplyOffice365Smtp(Rec)
                             ELSE
                               "SMTP Server" := '';
                             EnableControls;
                           END;
                            }

    { 12  ;1   ;Group     ;
                Visible=SettingsStepVisible;
                GroupType=Group }

    { 27  ;2   ;Group     ;
                Visible=AdvancedSettingsVisible;
                GroupType=Group;
                InstructionalTextML=[DEU=Geben Sie die SMTP-Serverdetails ein.;
                                     ENU=Enter the SMTP Server Details.] }

    { 9   ;3   ;Field     ;
                Name=Authentication;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Authentication;
                OnValidate=BEGIN
                             EnableControls;
                           END;
                            }

    { 8   ;3   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des SMTP-Servers an.;
                           ENU=Specifies the name of the SMTP server.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="SMTP Server";
                OnValidate=BEGIN
                             EnableControls;
                           END;
                            }

    { 7   ;3   ;Field     ;
                ToolTipML=[DEU=Legt den Port des SMTP-Servers fest. Die Standardeinstellung lautet 25.;
                           ENU=Specifies the port of the SMTP server. The default setting is 25.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="SMTP Server Port" }

    { 6   ;3   ;Field     ;
                Name=Secure Connection;
                ToolTipML=[DEU=Gibt an, ob Ihre SMTP-Mailservereinrichtung eine sichere Verbindung erfordert, die ein Kryptografie- oder Sicherheitsprotokoll verwendet, z. B. Secure Socket Layers (SSL). Deaktivieren Sie das Kontrollk�stchen, falls diese Sicherheitseinstellung nicht aktiviert werden soll.;
                           ENU=Specifies if your SMTP mail server setup requires a secure connection that uses a cryptography or security protocol, such as secure socket layers (SSL). Clear the check box if you do not want to enable this security setting.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Secure Connection" }

    { 26  ;2   ;Group     ;
                Visible=MailSettingsVisible;
                GroupType=Group;
                InstructionalTextML=[DEU=Geben Sie die Anmeldeinformationen f�r das Konto ein, die zum Senden von E-Mails verwendet werden.;
                                     ENU=Enter the credentials for the account, which will be used for sending emails.] }

    { 5   ;3   ;Field     ;
                Name=Email;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User ID";
                OnValidate=BEGIN
                             EnableControls;
                           END;
                            }

    { 4   ;3   ;Field     ;
                Name=Password;
                ExtendedDatatype=Masked;
                CaptionML=[DEU=Kennwort;
                           ENU=Password];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Password;
                OnValidate=BEGIN
                             EnableControls;
                           END;
                            }

    { 17  ;1   ;Group     ;
                Visible=FinalStepVisible;
                GroupType=Group }

    { 23  ;2   ;Group     ;
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie 'Test-E-Mail senden', um zu pr�fen, ob die angegebene E-Mail-Einrichtung erfolgreich war.;
                                     ENU=To verify that the specified email setup works, choose Send Test Email.] }

    { 24  ;2   ;Group     ;
                CaptionML=[DEU=Das war's schon!;
                           ENU=That's it!];
                GroupType=Group }

    { 25  ;3   ;Group     ;
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie 'Fertig stellen', um das direkte Senden von E-Mails �ber Belege zu aktivieren.;
                                     ENU=To enable email sending directly from documents, choose Finish.] }

  }
  CODE
  {
    VAR
      AssistedSetup@1005 : Record 1803;
      MediaRepositoryStandard@1040 : Record 9400;
      MediaRepositoryDone@1041 : Record 9400;
      SMTPMail@1008 : Codeunit 400;
      Step@1012 : 'Start,Provider,Settings,Finish';
      TopBannerVisible@1042 : Boolean;
      FirstStepVisible@1000 : Boolean;
      ProviderStepVisible@1010 : Boolean;
      SettingsStepVisible@1011 : Boolean;
      AdvancedSettingsVisible@1003 : Boolean;
      MailSettingsVisible@1002 : Boolean;
      FinalStepVisible@1015 : Boolean;
      EmailProvider@1001 : 'Office 365,Other';
      FinishActionEnabled@1009 : Boolean;
      BackActionEnabled@1013 : Boolean;
      NextActionEnabled@1014 : Boolean;
      NAVNotSetUpQst@1004 : TextConst 'DEU=Die E-Mail wurde noch nicht eingerichtet.\\M�chten Sie den Assistenten wirklich beenden?;ENU=Email has not been set up.\\Are you sure you want to exit?';
      EmailPasswordMissingErr@1006 : TextConst 'DEU=Geben Sie eine g�ltige E-Mail-Adresse und ein Kennwort ein.;ENU=Please enter a valid email address and password.';
      Password@1007 : Text[250];
      DummyPasswordTxt@1017 : TextConst '@@@={Locked};DEU=***;ENU=***';

    LOCAL PROCEDURE EnableControls@2();
    BEGIN
      ResetControls;

      CASE Step OF
        Step::Start:
          ShowStartStep;
        Step::Provider:
          ShowProviderStep;
        Step::Settings:
          ShowSettingsStep;
        Step::Finish:
          ShowFinishStep;
      END;
    END;

    LOCAL PROCEDURE StoreSMTPSetup@31();
    VAR
      SMTPMailSetup@1000 : Record 409;
    BEGIN
      IF NOT SMTPMailSetup.GET THEN BEGIN
        SMTPMailSetup.INIT;
        SMTPMailSetup.INSERT;
      END;

      SMTPMailSetup.TRANSFERFIELDS(Rec,FALSE);
      IF Password <> DummyPasswordTxt THEN
        SMTPMailSetup.SetPassword(Password);
      SMTPMailSetup.MODIFY(TRUE);
      COMMIT;
    END;

    LOCAL PROCEDURE SendTestEmailAction@10();
    BEGIN
      StoreSMTPSetup;
      CODEUNIT.RUN(CODEUNIT::"SMTP Test Mail");
    END;

    LOCAL PROCEDURE FinishAction@12();
    BEGIN
      StoreSMTPSetup;
      AssistedSetup.SetStatus(PAGE::"Email Setup Wizard",AssistedSetup.Status::Completed);
      CurrPage.CLOSE;
    END;

    LOCAL PROCEDURE NextStep@41(Backwards@1000 : Boolean);
    BEGIN
      IF Backwards THEN
        Step := Step - 1
      ELSE
        Step := Step + 1;

      EnableControls;
    END;

    LOCAL PROCEDURE ShowStartStep@24();
    BEGIN
      FirstStepVisible := TRUE;
      FinishActionEnabled := FALSE;
      BackActionEnabled := FALSE;
    END;

    LOCAL PROCEDURE ShowProviderStep@33();
    BEGIN
      ProviderStepVisible := TRUE;
    END;

    LOCAL PROCEDURE ShowSettingsStep@34();
    BEGIN
      SettingsStepVisible := TRUE;
      AdvancedSettingsVisible := EmailProvider = EmailProvider::Other;
      MailSettingsVisible := Authentication = Authentication::Basic;
    END;

    LOCAL PROCEDURE ShowFinishStep@35();
    BEGIN
      FinalStepVisible := TRUE;
      NextActionEnabled := FALSE;
    END;

    LOCAL PROCEDURE ResetControls@1();
    BEGIN
      FinishActionEnabled := "SMTP Server" <> '';
      IF (Authentication = Authentication::Basic) AND (("User ID" = '') OR (Password = '')) THEN
        FinishActionEnabled := FALSE;
      BackActionEnabled := TRUE;
      NextActionEnabled := TRUE;

      FirstStepVisible := FALSE;
      ProviderStepVisible := FALSE;
      SettingsStepVisible := FALSE;
      FinalStepVisible := FALSE;
    END;

    LOCAL PROCEDURE LoadTopBanners@40();
    BEGIN
      IF MediaRepositoryStandard.GET('AssistedSetup-NoText-400px.png',FORMAT(CURRENTCLIENTTYPE)) AND
         MediaRepositoryDone.GET('AssistedSetupDone-NoText-400px.png',FORMAT(CURRENTCLIENTTYPE))
      THEN
        TopBannerVisible := MediaRepositoryDone.Image.HASVALUE;
    END;

    BEGIN
    END.
  }
}

