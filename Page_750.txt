OBJECT Page 750 Standard General Journals
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Standard Fibu Buch.-Bl�tter;
               ENU=Standard General Journals];
    SaveValues=Yes;
    SourceTable=Table750;
    DataCaptionFields=Journal Template Name;
    PageType=List;
    CardPageID=Standard General Journal;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Standard;
                                 ENU=&Standard];
                      Image=Journal }
      { 12      ;2   ;Action    ;
                      Name=ShowJournal;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=&Buch.-Blatt anzeigen;
                                 ENU=&Show Journal];
                      ToolTipML=[DEU=�ffnet ein Buchungsblatt auf Grundlage des ausgew�hlten Buchungsblattnamens.;
                                 ENU=Open a journal based on the journal batch that you selected.];
                      ApplicationArea=#Suite;
                      Image=Journal;
                      OnAction=VAR
                                 StdGenJnl@1001 : Record 750;
                               BEGIN
                                 StdGenJnl.SETRANGE("Journal Template Name","Journal Template Name");
                                 StdGenJnl.SETRANGE(Code,Code);

                                 PAGE.RUN(PAGE::"Standard General Journal",StdGenJnl);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code zum Identifizieren des Standard-Fibu-Buch.-Blatts an, das Sie speichern m�chten.;
                           ENU=Specifies a code to identify the standard general journal that you are about to save.];
                ApplicationArea=#Suite;
                SourceExpr=Code }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Text an, der den Zweck des Standard-Fibu-Buch.-Blatts beschreibt.;
                           ENU=Specifies a text that indicates the purpose of the standard general journal.];
                ApplicationArea=#Suite;
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

