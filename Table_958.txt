OBJECT Table 958 Time Sheet Posting Entry
{
  OBJECT-PROPERTIES
  {
    Date=15.09.15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Buchungsposten f�r Arbeitszeittabelle;
               ENU=Time Sheet Posting Entry];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[DEU=Lfd. Nr.;
                                                              ENU=Entry No.] }
    { 2   ;   ;Time Sheet No.      ;Code20        ;TableRelation="Time Sheet Header";
                                                   CaptionML=[DEU=Arbeitszeittabelle - Nr.;
                                                              ENU=Time Sheet No.] }
    { 3   ;   ;Time Sheet Line No. ;Integer       ;CaptionML=[DEU=Arbeitszeittabelle - Zeilennr.;
                                                              ENU=Time Sheet Line No.] }
    { 4   ;   ;Time Sheet Date     ;Date          ;CaptionML=[DEU=Arbeitszeittabelle - Datum;
                                                              ENU=Time Sheet Date] }
    { 5   ;   ;Quantity            ;Decimal       ;CaptionML=[DEU=Menge;
                                                              ENU=Quantity];
                                                   Editable=No }
    { 6   ;   ;Document No.        ;Code20        ;CaptionML=[DEU=Belegnr.;
                                                              ENU=Document No.] }
    { 7   ;   ;Posting Date        ;Date          ;CaptionML=[DEU=Buchungsdatum;
                                                              ENU=Posting Date] }
    { 10  ;   ;Description         ;Text50        ;CaptionML=[DEU=Beschreibung;
                                                              ENU=Description] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Time Sheet No.,Time Sheet Line No.,Time Sheet Date;
                                                   SumIndexFields=Quantity }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

