OBJECT Page 5305 Outlook Synch. User Setup
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Outlook-Synch.-Benutzereinrichtung;
               ENU=Outlook Synch. User Setup];
    SourceTable=Table5305;
    PageType=List;
    RefreshOnActivate=Yes;
    OnOpenPage=VAR
                 OutlookSynchSetupDefaults@1000 : Codeunit 5312;
               BEGIN
                 OutlookSynchSetupDefaults.InsertOSynchDefaults;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 16      ;1   ;ActionGroup;
                      Name=Setup;
                      CaptionML=[DEU=&Einrichtung;
                                 ENU=&Setup];
                      Image=Setup }
      { 17      ;2   ;Action    ;
                      CaptionML=[DEU=S&ynch.-Elemente;
                                 ENU=S&ynch. Elements];
                      RunObject=Page 5310;
                      RunPageLink=User ID=FIELD(User ID),
                                  Synch. Entity Code=FIELD(Synch. Entity Code),
                                  Outlook Collection=FILTER(<>'');
                      Image=Hierarchy;
                      OnAction=BEGIN
                                 CALCFIELDS("No. of Elements");
                               END;
                                }
      { 19      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=In der Einrichtung des �nderung&sprotokolls erfassen;
                                 ENU=Register in Change Log &Setup];
                      Image=ImportLog;
                      OnAction=BEGIN
                                 OSynchEntity.GET("Synch. Entity Code");
                                 OSynchEntity.SETRECFILTER;
                                 REPORT.RUN(REPORT::"Outlook Synch. Change Log Set.",TRUE,FALSE,OSynchEntity);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID eines Benutzers an, der die Windows-Serverauthentifizierung verwendet, um sich bei Microsoft Dynamics NAV anzumelden, damit er auf die aktuelle Datenbank zugreifen kann. In Microsoft Dynamics NAV besteht die Benutzer-ID nur aus einem Benutzernamen.;
                           ENU=Specifies the ID of a user who uses the Windows Server Authentication to log on to Microsoft Dynamics NAV to access the current database. In Microsoft Dynamics NAV the user ID consists of only a user name.];
                SourceExpr="User ID" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Synchronisierungseinheit an. Dieser Code wurde aus dem Feld "Code" der Tabelle "Outlook-Synch.-Einheit" �bernommen.;
                           ENU=Specifies the code of the synchronization entity. The program copied this code from the Code field of the Outlook Synch. Entity table.];
                SourceExpr="Synch. Entity Code" }

    { 6   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[DEU=Gibt eine kurze Beschreibung der Synchronisierungseinheit an. Diese Beschreibung wird aus dem Feld "Beschreibung" der Tabelle "Outlook-Synch.-Einheit" �bernommen. Dieses Feld wird ausgef�llt, wenn Sie einen Code in das Feld "Synch.-Einheitscode" eingeben.;
                           ENU=Specifies a brief description of the synchronization entity. The program copies this description from the Description field of the Outlook Synch. Entity table. This field is filled in when you enter a code in the Synch. Entity Code field.];
                SourceExpr=Description }

    { 15  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Sammlungen an, die f�r die Synchronisierung ausgew�hlt wurden. Der Benutzer definiert diese Sammlungen auf der Seite "Outlook-Synch.-Einrichtungsdetails".;
                           ENU=Specifies the number of the collections which were selected for the synchronization. The user defines these collections on the Outlook Synch. Setup Details page.];
                SourceExpr="No. of Elements" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Kriterien zum Definieren eines Satzes bestimmter Posten an, die im Synchronisierungsprozess verwendet werden sollen. Dieser Filter wird auf die von Ihnen im Feld "Tabellennr." angegebene Tabelle angewendet. F�r diesen Filter k�nnen Sie nur die Optionen CONST und FILTER verwenden.;
                           ENU=Specifies the criteria for defining a set of specific entries to use in the synchronization process. This filter is applied to the table you specified in the Table No. field. For this filter you can use only the CONST and FILTER options.];
                SourceExpr=Condition;
                OnAssistEdit=BEGIN
                               OSynchEntity.GET("Synch. Entity Code");
                               Condition := COPYSTR(OSynchSetupMgt.ShowOSynchFiltersForm("Record GUID",OSynchEntity."Table No.",0),1,MAXSTRLEN(Condition));
                             END;
                              }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Richtung der Synchronisierung f�r den aktuellen Posten an. Die folgenden Optionen sind verf�gbar:;
                           ENU=Specifies the direction of the synchronization for the current entry. The following options are available:];
                SourceExpr="Synch. Direction" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      OSynchEntity@1001 : Record 5300;
      OSynchSetupMgt@1000 : Codeunit 5300;

    BEGIN
    END.
  }
}

