OBJECT Page 5005361 Posted Phys. Invt. Order Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVDACH10.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Geb. Inventurauftragszeilen;
               ENU=Posted Phys. Invt. Order Lines];
    SourceTable=Table5005355;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1140040 ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 1140041 ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=&Karte;
                                 ENU=Show &Card];
                      RunObject=Page 5005358;
                      RunPageView=SORTING(No.);
                      RunPageLink=No.=FIELD(Document No.) }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1140000;1;Group     ;
                GroupType=Repeater }

    { 1140001;2;Field     ;
                ToolTipML=[DEU=Enth�lt die Belegnummer der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Document No. of the table physical inventory order line.];
                SourceExpr="Document No." }

    { 1140003;2;Field     ;
                ToolTipML=[DEU=Enth�lt die Lfd. Nr. der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Line No. of the table physical inventory order line.];
                SourceExpr="Line No.";
                Visible=FALSE }

    { 1140005;2;Field     ;
                ToolTipML=[DEU=Enth�lt die Artikelnummer der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Item No. of the table physical inventory order line.];
                SourceExpr="Item No." }

    { 1140007;2;Field     ;
                ToolTipML=[DEU=Enth�lt den Variantencode der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Variant Code of the table physical inventory order line.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 1140009;2;Field     ;
                ToolTipML=[DEU=Enth�lt die Beschreibung der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Description of the table physical inventory order line.];
                SourceExpr=Description }

    { 1140011;2;Field     ;
                ToolTipML=[DEU=Enth�lt die Beschreibung 2 der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Description 2 of the table physical inventory order line.];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 1140013;2;Field     ;
                ToolTipML=[DEU=Enth�lt den Lagerortcode der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Location Code of the table physical inventory order line.];
                SourceExpr="Location Code" }

    { 1140015;2;Field     ;
                ToolTipML=[DEU=Enth�lt die Einheit der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Unit of Measure of the table physical inventory order line.];
                SourceExpr="Unit of Measure";
                Visible=FALSE }

    { 1140017;2;Field     ;
                ToolTipML=[DEU=Enth�lt den Basiseinheitencode der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Base Unit of Measure Code of the table physical inventory order line.];
                SourceExpr="Base Unit of Measure Code" }

    { 1140019;2;Field     ;
                ToolTipML=[DEU=Enth�lt die "Erwartete Menge (Basis)" der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Qty. Expected (Base) of the table physical inventory order line.];
                SourceExpr="Qty. Expected (Base)" }

    { 1140021;2;Field     ;
                ToolTipML=[DEU=Enth�lt die "Erfasste Menge (Basis)" der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Qty. Recorded (Base) of the table physical inventory order line.];
                SourceExpr="Qty. Recorded (Base)" }

    { 1140023;2;Field     ;
                ToolTipML=[DEU=Enth�lt die Postenart der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Entry Type of the table physical inventory order line.];
                SourceExpr="Entry Type" }

    { 1140025;2;Field     ;
                ToolTipML=[DEU=Enth�lt die "Menge (Basis)" der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Quantity (Base) of the table physical inventory order line.];
                SourceExpr="Quantity (Base)" }

    { 1140027;2;Field     ;
                ToolTipML=[DEU=Enth�lt "Anzahl beendete Erfassungszeilen" der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the No. Finished Rec.-Lines of the table physical inventory order line.];
                SourceExpr="No. Finished Rec.-Lines" }

    { 1140029;2;Field     ;
                ToolTipML=[DEU=Enth�lt die "Ohne Auftrag erfasst" der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Recorded without Order of the table physical inventory order line.];
                SourceExpr="Recorded without Order";
                Visible=FALSE }

    { 1140031;2;Field     ;
                ToolTipML=[DEU=Enth�lt den St�ckpreis der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Unit Amount of the table physical inventory order line.];
                SourceExpr="Unit Amount" }

    { 1140033;2;Field     ;
                ToolTipML=[DEU=Enth�lt den Einstandspreis der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Unit Cost of the table physical inventory order line.];
                SourceExpr="Unit Cost";
                Visible=FALSE }

    { 1140035;2;Field     ;
                ToolTipML=[DEU=Enth�lt die Regalnummer oder den Lagerplatz der Tabelle "Inventurauftragszeile".;
                           ENU=Contains the Shelf/Bin No. of the table physical inventory order line.];
                SourceExpr="Shelf/Bin No.";
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

