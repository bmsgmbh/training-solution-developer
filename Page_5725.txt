OBJECT Page 5725 Nonstock Item Card
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Katalogartikelkarte;
               ENU=Nonstock Item Card];
    SourceTable=Table5718;
    PageType=Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 24      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Katalogartikel;
                                 ENU=Nonstoc&k Item];
                      Image=NonStockItem }
      { 33      ;2   ;Action    ;
                      CaptionML=[DEU=Ersat&zartikel;
                                 ENU=Substituti&ons];
                      ApplicationArea=#Suite;
                      RunObject=Page 5716;
                      RunPageLink=Type=CONST(Nonstock Item),
                                  No.=FIELD(Entry No.);
                      Image=ItemSubstitution }
      { 44      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(Nonstock Item),
                                  No.=FIELD(Entry No.);
                      Image=ViewComments }
      { 1900000005;0 ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 1900294905;1 ;Action    ;
                      CaptionML=[DEU=Neuer Artikel;
                                 ENU=New Item];
                      RunObject=Page 30;
                      Promoted=Yes;
                      Image=NewItem;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 39      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 41      ;2   ;Action    ;
                      CaptionML=[DEU=&Artikel erstellen;
                                 ENU=&Create Item];
                      ToolTipML=[DEU=Konvertieren Sie die Katalogartikelkarte in einer normale Artikelkarte, entsprechend einer von Ihnen gew�hlten Artikelvorlage.;
                                 ENU=Convert the nonstock item card to a normal item card, according to an item template that you choose.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=NewItemNonStock;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 NonstockItemMgt.NonstockAutoItem(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer an, die dem Katalogartikel zugeordnet ist, wenn Sie einen Artikel in eine Katalogartikelkarte eingeben.;
                           ENU=Specifies the number assigned to the nonstock item, when you enter an item on a nonstock item card.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry No.";
                Importance=Additional;
                OnAssistEdit=BEGIN
                               IF AssistEdit THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code f�r den Hersteller des Katalogartikels an.;
                           ENU=Specifies a code for the manufacturer of the nonstock item.];
                SourceExpr="Manufacturer Code" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kreditors an, von dem Sie den Katalogartikel kaufen k�nnen.;
                           ENU=Specifies the number of the vendor from whom you can purchase the nonstock item.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Vendor No.";
                Importance=Promoted }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer an, die der Kreditor verwendet, um den Katalogartikel zu kennzeichnen.;
                           ENU=Specifies the number the vendor uses to identify the nonstock item.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Vendor Item No.";
                Importance=Promoted }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Artikelnummer an, die die Anwendung f�r diesen Katalogartikel erzeugt hat.;
                           ENU=Specifies the item number that the program has generated for this nonstock item.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Item No.";
                Importance=Additional }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Katalogartikels an.;
                           ENU=Specifies a description of the nonstock item.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Einheit an, in der der Katalogartikel verkauft wird.;
                           ENU=Specifies the code for the unit of measure in which the nonstock item is sold.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit of Measure";
                Importance=Promoted }

    { 35  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Katalogartikelkarte zuletzt ge�ndert wurde.;
                           ENU=Specifies the date on which the nonstock item card was last modified.];
                SourceExpr="Last Date Modified" }

    { 1905885101;1;Group  ;
                CaptionML=[DEU=Fakturierung;
                           ENU=Invoicing] }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Listen-EK-Preis des Lieferanten des Katalogartikels an.;
                           ENU=Specifies the published cost or vendor list price for the nonstock item.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Published Cost";
                Importance=Additional }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Preis an, den Sie mit dem Lieferanten f�r den Katalogartikel vereinbart haben.;
                           ENU=Specifies the price you negotiated to pay for the nonstock item.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Negotiated Cost";
                Importance=Additional }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den VK-Preis des Katalogartikels in Mandantenw�hrung (MW) an.;
                           ENU=Specifies the unit price of the nonstock item in the local currency (LCY).];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit Price";
                Importance=Promoted }

    { 29  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Bruttogewicht des Katalogartikels an, inklusive Verpackung.;
                           ENU=Specifies the gross weight, including the weight of any packaging, of the nonstock item.];
                SourceExpr="Gross Weight" }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Nettogewicht des Artikels an. Das Gewicht der Verpackung ist darin nicht enthalten.;
                           ENU=Specifies the net weight of the item. The weight of packaging materials is not included.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Net Weight" }

    { 31  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Barcode des Katalogartikels an.;
                           ENU=Specifies the bar code of the nonstock item.];
                SourceExpr="Bar Code" }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Artikelvorlage an, die f�r diesen Katalogartikel verwendet wurde.;
                           ENU=Specifies the code for the item template used for this nonstock item.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Item Template Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      NonstockItemMgt@1000 : Codeunit 5703;

    BEGIN
    END.
  }
}

