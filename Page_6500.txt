OBJECT Page 6500 Item Tracking Summary
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Artikelverf.-Zusammenfassung;
               ENU=Item Tracking Summary];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table338;
    PageType=Worksheet;
    SourceTableTemporary=Yes;
    OnInit=BEGIN
             Undefined1Visible := TRUE;
             Selected1Visible := TRUE;
             MaxQuantity1Visible := TRUE;
             BinContentVisible := TRUE;
           END;

    OnOpenPage=BEGIN
                 UpdateSelectedQuantity;

                 BinContentVisible := CurrBinCode <> '';

                 IF SelectedQuantityVisible THEN BEGIN
                 END
                 ELSE
                   IF "Serial No." <> '' THEN;
               END;

    OnAfterGetCurrRecord=BEGIN
                           UpdateIfFiltersHaveChanged;
                         END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Chargennummer an, f�r die die Verf�gbarkeit im Fenster "Artikelverf.-Zusammenfassung" angezeigt wird.;
                           ENU=Specifies the lot number for which availability is presented in the Item Tracking Summary window.];
                SourceExpr="Lot No.";
                Editable=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Seriennummer an, f�r die die Verf�gbarkeit im Fenster "Artikelverf.-Zusammenfassung" angezeigt wird.;
                           ENU=Specifies the serial number for which availability is presented in the Item Tracking Summary window.];
                SourceExpr="Serial No.";
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Garantieablaufdatum des Artikels mit der Artikelverfolgungsnummer an.;
                           ENU=Specifies the warranty expiration date, if any, of the item carrying the item tracking number.];
                SourceExpr="Warranty Date";
                Visible=FALSE;
                Editable=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Ablaufdatum des Artikels mit der Artikelverfolgungsnummer an.;
                           ENU=Specifies the expiration date, if any, of the item carrying the item tracking number.];
                SourceExpr="Expiration Date";
                Visible=FALSE;
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                DrillDown=Yes;
                ToolTipML=[DEU=Gibt die Gesamtmenge des Artikels im Lager an.;
                           ENU=Specifies the total quantity of the item in inventory.];
                SourceExpr="Total Quantity";
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DrillDownEntries(FIELDNO("Total Quantity"));
                            END;
                             }

    { 24  ;2   ;Field     ;
                DrillDown=Yes;
                ToolTipML=[DEU=Gibt die Gesamtmenge der angeforderten Los- oder Seriennummer in allen Belegen an.;
                           ENU=Specifies the total quantity of the lot or serial number that is requested in all documents.];
                SourceExpr="Total Requested Quantity";
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DrillDownEntries(FIELDNO("Total Requested Quantity"));
                            END;
                             }

    { 31  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge aus der Artikelverfolgungszeile an, die im Beleg ausgew�hlt ist, aber noch nicht in die Datenbank �bernommen wurde.;
                           ENU=Specifies the quantity from the item tracking line that is selected on the document but not yet committed to the database.];
                SourceExpr="Current Pending Quantity";
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge an, die der Benutzer in Posten der Art in der Zeile anfordern kann.;
                           ENU=Specifies the quantity available for the user to request, in entries of the type on the line.];
                SourceExpr="Total Available Quantity";
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge der im Posten enthaltenen Artikel an, die f�r die Zeile reserviert sind, von der aus das Fenster "Reservierung" ge�ffnet wird.;
                           ENU=Specifies the quantity of items in the entry that are reserved for the line that the Reservation window is opened from.];
                SourceExpr="Current Reserved Quantity";
                Visible=FALSE;
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gesamtmenge des relevanten Artikels an, die in Belegen oder Posten der Art in der Zeile reserviert ist.;
                           ENU=Specifies the total quantity of the relevant item that is reserved on documents or entries of the type on the line.];
                SourceExpr="Total Reserved Quantity";
                Visible=FALSE;
                Editable=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels an, der sich am in der Belegzeile angegebenen Lagerplatz befindet.;
                           ENU=Specifies the quantity of the item in the bin specified in the document line.];
                SourceExpr="Bin Content";
                Visible=BinContentVisible;
                OnDrillDown=BEGIN
                              DrillDownBinContent(FIELDNO("Bin Content"));
                            END;
                             }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge f�r die einzelnen Chargen- oder Seriennummern an, die sie verwenden m�chten, um den Bedarf f�r die Transaktion zu erf�llen.;
                           ENU=Specifies the quantity of each lot or serial number that you want to use to fulfill the demand for the transaction.];
                SourceExpr="Selected Quantity";
                Visible=SelectedQuantityVisible;
                Editable=SelectedQuantityEditable;
                Style=Strong;
                StyleExpr=TRUE;
                OnValidate=BEGIN
                             SelectedQuantityOnAfterValidat;
                           END;
                            }

    { 50  ;1   ;Group      }

    { 1901775901;2;Group  ;
                GroupType=FixedLayout }

    { 1900545401;3;Group  ;
                CaptionML=[DEU=Ausw�hlbar;
                           ENU=Selectable] }

    { 27  ;4   ;Field     ;
                Name=MaxQuantity1;
                CaptionML=[DEU=Ausw�hlbar;
                           ENU=Selectable];
                ToolTipML=[DEU=Gibt den Wert aus dem Feld "Undefiniert" im Fenster "Artikelverfolgungszeilen" an. Dieser Wert gibt an, wie viel ausgew�hlt werden kann.;
                           ENU=Specifies the value from the Undefined field on the Item Tracking Lines window. This value indicates how much can be selected.];
                DecimalPlaces=0:5;
                SourceExpr=MaxQuantity;
                Visible=MaxQuantity1Visible;
                Editable=FALSE }

    { 1900724401;3;Group  ;
                CaptionML=[DEU=Ausgew�hlt;
                           ENU=Selected] }

    { 5   ;4   ;Field     ;
                Name=Selected1;
                CaptionML=[DEU=Ausgew�hlt;
                           ENU=Selected];
                ToolTipML=[DEU=Gibt die Gesamtmenge an, die Sie ausgew�hlt haben. Diese gibt die Summe der Mengen in den Feldern "Ausgew�hlte Menge" an.;
                           ENU=Specifies the sum of the quantity that you have selected. It Specifies a total of the quantities in the Selected Quantity fields.];
                DecimalPlaces=0:5;
                SourceExpr=SelectedQuantity;
                Visible=Selected1Visible;
                Editable=FALSE }

    { 1900724101;3;Group  ;
                CaptionML=[DEU=Undefiniert;
                           ENU=Undefined] }

    { 6   ;4   ;Field     ;
                Name=Undefined1;
                CaptionML=[DEU=Undefiniert;
                           ENU=Undefined];
                ToolTipML=[DEU=Gibt den Unterschied zwischen der Menge, die f�r die Belegzeile ausgew�hlt werden kann, und der in diesem Fenster ausgew�hlten Menge an.;
                           ENU=Specifies the difference between the quantity that can be selected for the document line, and the quantity selected in this window.];
                DecimalPlaces=2:5;
                BlankZero=Yes;
                SourceExpr=MaxQuantity - SelectedQuantity;
                Visible=Undefined1Visible;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      CurrItemTrackingCode@1001 : Record 6502;
      TempReservEntry@1000 : TEMPORARY Record 337;
      xFilterRec@1006 : Record 338;
      ItemTrackingDataCollection@1004 : Codeunit 6501;
      MaxQuantity@1002 : Decimal;
      SelectedQuantity@1003 : Decimal;
      CurrBinCode@1005 : Code[20];
      SelectedQuantityVisible@19045813 : Boolean INDATASET;
      BinContentVisible@19045641 : Boolean INDATASET;
      MaxQuantity1Visible@19054264 : Boolean INDATASET;
      Selected1Visible@19001414 : Boolean INDATASET;
      Undefined1Visible@19007737 : Boolean INDATASET;
      SelectedQuantityEditable@19031275 : Boolean INDATASET;

    PROCEDURE SetSources@1(VAR ReservEntry@1000 : Record 337;VAR EntrySummary@1001 : Record 338);
    VAR
      xEntrySummary@1002 : Record 338;
    BEGIN
      TempReservEntry.RESET;
      TempReservEntry.DELETEALL;
      IF ReservEntry.FIND('-') THEN
        REPEAT
          TempReservEntry := ReservEntry;
          TempReservEntry.INSERT;
        UNTIL ReservEntry.NEXT = 0;

      xEntrySummary.SETVIEW(GETVIEW);
      RESET;
      DELETEALL;
      IF EntrySummary.FINDSET THEN
        REPEAT
          IF EntrySummary.HasQuantity THEN BEGIN
            Rec := EntrySummary;
            INSERT;
          END;
        UNTIL EntrySummary.NEXT = 0;
      SETVIEW(xEntrySummary.GETVIEW);
      UpdateSelectedQuantity;
    END;

    PROCEDURE SetSelectionMode@2(SelectionMode@1000 : Boolean);
    BEGIN
      SelectedQuantityVisible := SelectionMode;
      SelectedQuantityEditable := SelectionMode;
      MaxQuantity1Visible := SelectionMode;
      Selected1Visible := SelectionMode;
      Undefined1Visible := SelectionMode;
    END;

    PROCEDURE SetMaxQuantity@3(MaxQty@1000 : Decimal);
    BEGIN
      MaxQuantity := MaxQty;
    END;

    PROCEDURE SetCurrentBinAndItemTrkgCode@8(BinCode@1000 : Code[20];ItemTrackingCode@1001 : Record 6502);
    BEGIN
      ItemTrackingDataCollection.SetCurrentBinAndItemTrkgCode(BinCode,ItemTrackingCode);
      BinContentVisible := BinCode <> '';
      CurrBinCode := BinCode;
      CurrItemTrackingCode := ItemTrackingCode;
    END;

    PROCEDURE AutoSelectLotSerialNo@6();
    BEGIN
      ItemTrackingDataCollection.AutoSelectLotSerialNo(Rec,MaxQuantity);
    END;

    LOCAL PROCEDURE UpdateSelectedQuantity@4();
    VAR
      xEntrySummary@1000 : Record 338;
    BEGIN
      IF NOT SelectedQuantityVisible THEN
        EXIT;
      IF MODIFY THEN; // Ensure that changes to current rec are included in CALCSUMS
      xEntrySummary := Rec;
      CALCSUMS("Selected Quantity");
      SelectedQuantity := "Selected Quantity";
      Rec := xEntrySummary;
    END;

    PROCEDURE GetSelected@5(VAR EntrySummary@1000 : Record 338);
    BEGIN
      EntrySummary.RESET;
      EntrySummary.DELETEALL;
      SETFILTER("Selected Quantity",'<>%1',0);
      IF FINDSET THEN
        REPEAT
          EntrySummary := Rec;
          EntrySummary.INSERT;
        UNTIL NEXT = 0;
    END;

    LOCAL PROCEDURE DrillDownEntries@7(FieldNumber@1001 : Integer);
    VAR
      TempReservEntry2@1000 : TEMPORARY Record 337;
    BEGIN
      TempReservEntry.RESET;
      TempReservEntry.SETCURRENTKEY(
        "Item No.","Source Type","Source Subtype","Reservation Status",
        "Location Code","Variant Code","Shipment Date","Expected Receipt Date","Serial No.","Lot No.");

      TempReservEntry.SETRANGE("Lot No.","Lot No.");
      IF "Serial No." <> '' THEN
        TempReservEntry.SETRANGE("Serial No.","Serial No.");

      CASE FieldNumber OF
        FIELDNO("Total Quantity"):
          BEGIN
            // An Item Ledger Entry will in itself be represented with a surplus TempReservEntry. Order tracking
            // and reservations against Item Ledger Entries are therefore kept out, as these quantities would
            // otherwise be represented twice in the drill down.

            TempReservEntry.SETRANGE(Positive,TRUE);
            TempReservEntry2.COPY(TempReservEntry);  // Copy key
            IF TempReservEntry.FINDSET THEN
              REPEAT
                TempReservEntry2 := TempReservEntry;
                IF TempReservEntry."Source Type" = DATABASE::"Item Ledger Entry" THEN BEGIN
                  IF TempReservEntry."Reservation Status" = TempReservEntry."Reservation Status"::Surplus THEN
                    TempReservEntry2.INSERT;
                END ELSE
                  TempReservEntry2.INSERT;
              UNTIL TempReservEntry.NEXT = 0;
            TempReservEntry2.ASCENDING(FALSE);
            PAGE.RUNMODAL(PAGE::"Avail. - Item Tracking Lines",TempReservEntry2);
          END;
        FIELDNO("Total Requested Quantity"):
          BEGIN
            TempReservEntry.SETRANGE(Positive,FALSE);
            TempReservEntry.ASCENDING(FALSE);
            PAGE.RUNMODAL(PAGE::"Avail. - Item Tracking Lines",TempReservEntry);
          END;
      END;
    END;

    LOCAL PROCEDURE DrillDownBinContent@11(FieldNumber@1000 : Integer);
    VAR
      BinContent@1001 : Record 7302;
    BEGIN
      IF CurrBinCode = '' THEN
        EXIT;
      TempReservEntry.RESET;
      IF NOT TempReservEntry.FINDFIRST THEN
        EXIT;

      CurrItemTrackingCode.TESTFIELD(Code);

      BinContent.SETRANGE("Location Code",TempReservEntry."Location Code");
      BinContent.SETRANGE("Item No.",TempReservEntry."Item No.");
      BinContent.SETRANGE("Variant Code",TempReservEntry."Variant Code");
      IF CurrItemTrackingCode."Lot Warehouse Tracking" THEN
        IF "Lot No." <> '' THEN
          BinContent.SETRANGE("Lot No. Filter","Lot No.");
      IF CurrItemTrackingCode."SN Warehouse Tracking" THEN
        IF "Serial No." <> '' THEN
          BinContent.SETRANGE("Serial No. Filter","Serial No.");

      IF FieldNumber = FIELDNO("Bin Content") THEN
        BinContent.SETRANGE("Bin Code",CurrBinCode);

      PAGE.RUNMODAL(PAGE::"Bin Content",BinContent);
    END;

    LOCAL PROCEDURE UpdateIfFiltersHaveChanged@9();
    BEGIN
      // In order to update Selected Quantity when filters have been changed on the form.
      IF GETFILTERS = xFilterRec.GETFILTERS THEN
        EXIT;

      UpdateSelectedQuantity;
      xFilterRec.COPY(Rec);
    END;

    LOCAL PROCEDURE SelectedQuantityOnAfterValidat@19049896();
    BEGIN
      UpdateSelectedQuantity;
      CurrPage.UPDATE;
    END;

    BEGIN
    END.
  }
}

