OBJECT Page 5743 Posted Transfer Shipment
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Geb. Umlag.-Ausgang;
               ENU=Posted Transfer Shipment];
    InsertAllowed=No;
    SourceTable=Table5744;
    PageType=Document;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 50      ;1   ;ActionGroup;
                      CaptionML=[DEU=A&usgang;
                                 ENU=&Shipment];
                      Image=Shipment }
      { 56      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[DEU=Statistik;
                                 ENU=Statistics];
                      RunObject=Page 5756;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 57      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 5750;
                      RunPageLink=Document Type=CONST(Posted Transfer Shipment),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 58      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Anzeigen oder Bearbeiten von Dimensionen, wie etwa eines Bereichs, eines Projekts oder einer Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 51      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=D&rucken;
                                 ENU=&Print];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 TransShptHeader@1001 : Record 5744;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(TransShptHeader);
                                 TransShptHeader.PrintRecords(TRUE);
                               END;
                                }
      { 52      ;1   ;Action    ;
                      CaptionML=[DEU=&Navigate;
                                 ENU=&Navigate];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Umlagerungsausgangs an.;
                           ENU=Specifies the number of the transfer shipment.];
                SourceExpr="No.";
                Importance=Promoted;
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, von dem Sie Artikel umlagern.;
                           ENU=Specifies the code of the location that you are transferring items from.];
                SourceExpr="Transfer-from Code";
                Importance=Promoted;
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, an den Sie Artikel umlagern.;
                           ENU=Specifies the code of the location that you are transferring items to.];
                SourceExpr="Transfer-to Code";
                Importance=Promoted;
                Editable=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den In Transit Code an, der f�r diese Umlagerung verwendet wird.;
                           ENU=Specifies the in-transit code that is used for this transfer.];
                SourceExpr="In-Transit Code";
                Editable=FALSE }

    { 53  ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[DEU=Gibt die Nummer des Umlagerungsauftrags an, der die Grundlage der Umlagerungsauslieferung darstellt.;
                           ENU=Specifies the number of the transfer order on which the transfer shipment was based.];
                SourceExpr="Transfer Order No.";
                Importance=Promoted;
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem der Umlagerungsauftrag erstellt wurde.;
                           ENU=Specifies the date on which the transfer order was created.];
                SourceExpr="Transfer Order Date";
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Buchungsdatum f�r diesen Beleg an.;
                           ENU=Specifies the posting date for this document.];
                SourceExpr="Posting Date";
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode f�r die Dimension an, die als Globale Dimension 1 ausgew�hlt wurde.;
                           ENU=Specifies the dimension value code for the dimension that has been chosen as Global Dimension 1.];
                SourceExpr="Shortcut Dimension 1 Code";
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode f�r die Dimension an, die als Globale Dimension 2 ausgew�hlt wurde.;
                           ENU=Specifies the dimension value code for the dimension that has been chosen as Global Dimension 2.];
                SourceExpr="Shortcut Dimension 2 Code";
                Editable=FALSE }

    { 49  ;1   ;Part      ;
                Name=TransferShipmentLines;
                SubPageLink=Document No.=FIELD(No.);
                PagePartID=Page5744 }

    { 1904655901;1;Group  ;
                CaptionML=[DEU=Umlag. von;
                           ENU=Transfer-from] }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Lagerorts an, von dem Sie Artikel umlagern.;
                           ENU=Specifies the name of the location that you are transferring items from.];
                SourceExpr="Transfer-from Name";
                Editable=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen zus�tzlichen Teil des Namens des Lagerorts an, von dem Sie Artikel umlagern.;
                           ENU=Specifies an additional part of the name of the location that you are transferring items from.];
                SourceExpr="Transfer-from Name 2";
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Adresse des Lagerorts an, von dem Sie Artikel umlagern.;
                           ENU=Specifies the address of the location that you are transferring items from.];
                SourceExpr="Transfer-from Address";
                Editable=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen zus�tzlichen Teil der Adresse des Lagerorts an.;
                           ENU=Specifies an additional part of the address of the location.];
                SourceExpr="Transfer-from Address 2";
                Editable=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postleitzahl des Lagerorts an, von dem Sie Artikel umlagern.;
                           ENU=Specifies the postal code of the location that you are transferring items from.];
                SourceExpr="Transfer-from Post Code";
                Editable=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ort des Lagerorts an, von dem Sie Artikel umlagern.;
                           ENU=Specifies the city of the location that you are transferring items from.];
                SourceExpr="Transfer-from City";
                Editable=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Kontaktperson am Umlag. von Lagerort an.;
                           ENU=Specifies the name of the contact person at the transfer-from location.];
                SourceExpr="Transfer-from Contact";
                Editable=FALSE }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Warenausgangsdatum des Umlagerungsauftrags an.;
                           ENU=Specifies the shipment date of the transfer order.];
                SourceExpr="Shipment Date";
                Importance=Promoted;
                Editable=FALSE }

    { 67  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code an, der f�r die Lieferbedingung steht.;
                           ENU=Specifies a code that represents the shipment method.];
                SourceExpr="Shipment Method Code";
                Editable=FALSE }

    { 69  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r den Zusteller an, den Sie f�r diese Umlagerungsauslieferung in Anspruch genommen haben.;
                           ENU=Specifies the code for the shipping agent you have used for this transfer shipment.];
                SourceExpr="Shipping Agent Code";
                Editable=FALSE }

    { 71  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Zustellertransportart an, die Sie f�r diese Umlagerungsauslieferung in Anspruch genommen haben.;
                           ENU=Specifies the code for the shipping agent service you have used for this transfer shipment.];
                SourceExpr="Shipping Agent Service Code";
                Importance=Promoted;
                Editable=FALSE }

    { 1901454601;1;Group  ;
                CaptionML=[DEU=Umlag. nach;
                           ENU=Transfer-to] }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Lagerorts an, an den Sie Artikel umlagern.;
                           ENU=Specifies the name of the location that you are transferring items to.];
                SourceExpr="Transfer-to Name";
                Editable=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen zus�tzlichen Teil des Namens des Lagerorts an, an den Sie Artikel umlagern.;
                           ENU=Specifies an additional part of the name of the location that you are transferring items to.];
                SourceExpr="Transfer-to Name 2";
                Editable=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Adresse des Lagerorts an, an den Sie Artikel umlagern.;
                           ENU=Specifies the address of the location that you are transferring items to.];
                SourceExpr="Transfer-to Address";
                Editable=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen zus�tzlichen Teil der Adresse des Lagerorts an.;
                           ENU=Specifies an additional part of the address of the location.];
                SourceExpr="Transfer-to Address 2";
                Editable=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postleitzahl des Lagerorts an.;
                           ENU=Specifies the postal code of the location.];
                SourceExpr="Transfer-to Post Code";
                Editable=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ort des Lagerorts an, an den die Artikel umgelagert werden.;
                           ENU=Specifies the city of the location to which items are transferred.];
                SourceExpr="Transfer-to City";
                Editable=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Kontaktperson am Umlag. nach Lagerort an.;
                           ENU=Specifies the name of the contact person at the transfer-to location.];
                SourceExpr="Transfer-to Contact";
                Editable=FALSE }

    { 29  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Eingangsdatum des Umlagerungsauftrags an.;
                           ENU=Specifies the receipt date of the transfer order.];
                SourceExpr="Receipt Date";
                Importance=Promoted;
                Editable=FALSE }

    { 1907468901;1;Group  ;
                CaptionML=[DEU=Au�enhandel;
                           ENU=Foreign Trade] }

    { 63  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Transaktionstyp der Umlagerung an.;
                           ENU=Specifies the transaction type of the transfer.];
                SourceExpr="Transaction Type";
                Importance=Promoted;
                Editable=FALSE }

    { 65  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Spezifikationscode f�r das Verfahren an, das bei der Umlagerung verwendet wurde.;
                           ENU=Specifies the transaction specification code that was used in the transfer.];
                SourceExpr="Transaction Specification";
                Editable=FALSE }

    { 74  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r den Verkehrszweig an, der f�r den Artikel in der Zeile verwendet wird.;
                           ENU=Specifies the code for the transport method used for the item on this line.];
                SourceExpr="Transport Method";
                Importance=Promoted;
                Editable=FALSE }

    { 76  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r einen Bereich beim Debitor oder Kreditor an, mit dem Sie die Artikel in der Zeile handeln.;
                           ENU=Specifies the code for an area at the customer or vendor with which you are trading the items on the line.];
                SourceExpr=Area;
                Editable=FALSE }

    { 78  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Entladehafens an, an dem die Artikel in Ihr Land/Ihre Region oder in den Einladehafen gekommen sind.;
                           ENU=Specifies the code of either the port of entry at which the items passed into your country/region, or the port of exit.];
                SourceExpr="Entry/Exit Point";
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

