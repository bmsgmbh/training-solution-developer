OBJECT Page 5151 Contact Salutations
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Kontaktanreden;
               ENU=Contact Salutations];
    SourceTable=Table5069;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Sprachcode f�r die Anredeformel an.;
                           ENU=Specifies the language code for the salutation formula.];
                ApplicationArea=#All;
                SourceExpr="Language Code" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Anrede formell oder informell ist. W�hlen Sie eine Option aus, indem Sie auf das Feld klicken.;
                           ENU=Specifies whether the salutation is formal or informal. Make your selection by clicking the field.];
                ApplicationArea=#All;
                SourceExpr="Salutation Type" }

    { 9   ;2   ;Field     ;
                CaptionML=[DEU=Anrede;
                           ENU=Salutation];
                ToolTipML=[DEU=Gibt eine Anrede an. Verwenden Sie einen Code, den Sie schnell mit der jeweiligen Anrede in Verbindung bringen, z. B. M-TITEL f�r "M�nnliche Person mit Titel".;
                           ENU=Specifies a salutation. Use a code that makes it easy for you to remember the salutation, for example, M-JOB for "Male person with a job title".];
                ApplicationArea=#All;
                SourceExpr=GetContactSalutation }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

