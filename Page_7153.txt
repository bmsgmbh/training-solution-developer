OBJECT Page 7153 Item Analysis View Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Artikelanalyseansichtsposten;
               ENU=Analysis View Entries];
    SourceTable=Table7154;
    DataCaptionFields=Analysis View Code;
    PageType=List;
    OnAfterGetCurrRecord=BEGIN
                           IF "Analysis View Code" <> xRec."Analysis View Code" THEN;
                         END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, aus welcher Art Transaktion der Posten erstellt wurde.;
                           ENU=Specifies which type of transaction that the entry is created from.];
                SourceExpr="Item Ledger Entry Type" }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Wertpostenart f�r einen Analyseansichtsposten an.;
                           ENU=Specifies the value entry type for an analysis view entry.];
                SourceExpr="Entry Type" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Artikelnummer an, unter der ein Artikelposten in einem Analyseansichtsposten gebucht wurde.;
                           ENU=Specifies the item number to which the item ledger entry in an analysis view entry was posted.];
                SourceExpr="Item No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, an dem ein Artikelposten in einem Analyseansichtsposten gebucht wurde.;
                           ENU=Specifies the code of the location to which the item ledger entry in an analysis view entry was posted.];
                SourceExpr="Location Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswert an, den Sie f�r die Analyseansichtsdimension ausgew�hlt haben, die auf der Analyseansichtskarte als Dimension 1 definiert wurde.;
                           ENU=Specifies the dimension value you selected for the analysis view dimension that you defined as Dimension 1 on the analysis view card.];
                SourceExpr="Dimension 1 Value Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswert an, den Sie f�r die Analyseansichtsdimension ausgew�hlt haben, die auf der Analyseansichtskarte als Dimension 2 definiert wurde.;
                           ENU=Specifies the dimension value you selected for the analysis view dimension that you defined as Dimension 2 on the analysis view card.];
                SourceExpr="Dimension 2 Value Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswert an, den Sie f�r die Analyseansichtsdimension ausgew�hlt haben, die auf der Analyseansichtskarte als Dimension 3 definiert wurde.;
                           ENU=Specifies the dimension value you selected for the analysis view dimension that you defined as Dimension 3 on the analysis view card.];
                SourceExpr="Dimension 3 Value Code" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem der Artikelposten in einem Analyseansichtsposten gebucht wurde.;
                           ENU=Specifies the date when the item ledger entry in an analysis view entry was posted.];
                SourceExpr="Posting Date" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Summe der gebuchten Ist-Verkaufsbetr�ge f�r die Artikelposten im Analyseansichtsposten an.;
                           ENU=Specifies the sum of the actual sales amounts posted for the item ledger entries included in the analysis view entry.];
                SourceExpr="Sales Amount (Actual)";
                OnDrillDown=BEGIN
                              DrillDown;
                            END;
                             }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Summe der erwarteten Ist-Verkaufsbetr�ge f�r die Artikelposten im Analyseansichtsposten an.;
                           ENU=Specifies the sum of the expected sales amounts posted for the item ledger entries, included in the analysis view entry.];
                SourceExpr="Sales Amount (Expected)";
                OnDrillDown=BEGIN
                              DrillDown;
                            END;
                             }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Summe der gebuchten Ist-Einstandsbetr�ge f�r die Artikelposten im Analyseansichtsposten an.;
                           ENU=Specifies the sum of the actual cost amounts posted for the item ledger entries included in the analysis view entry.];
                SourceExpr="Cost Amount (Actual)";
                OnDrillDown=BEGIN
                              DrillDown;
                            END;
                             }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Summe der erwarteten Einstandsbetr�ge f�r die Artikelposten im Analyseansichtsposten an.;
                           ENU=Specifies the sum of the expected cost amounts posted for the item ledger entries included in the analysis view entry.];
                SourceExpr="Cost Amount (Expected)";
                OnDrillDown=BEGIN
                              DrillDown;
                            END;
                             }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Summe der lagerwertunabh�ngigen Einstandsbetr�ge f�r die Artikelposten im Analyseansichtsposten an.;
                           ENU=Specifies the sum of the non-inventoriable cost amounts posted for the item ledger entries included in the analysis view entry.];
                SourceExpr="Cost Amount (Non-Invtbl.)";
                OnDrillDown=BEGIN
                              DrillDown;
                            END;
                             }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Summe der Mengen f�r die Artikelposten im Analyseansichtsposten an.;
                           ENU=Specifies the sum of the quantity for the item ledger entries included in the analysis view entry.];
                SourceExpr=Quantity;
                OnDrillDown=BEGIN
                              DrillDown;
                            END;
                             }

    { 33  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Summe der fakturierten Menge f�r die Artikelposten im Analyseansichtsposten an.;
                           ENU=Specifies the sum of the quantity invoiced for the item ledger entries included in the analysis view entry.];
                SourceExpr="Invoiced Quantity";
                OnDrillDown=BEGIN
                              DrillDown;
                            END;
                             }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      TempValueEntry@1000 : TEMPORARY Record 5802;

    LOCAL PROCEDURE DrillDown@1();
    BEGIN
      SetAnalysisViewEntry(Rec);
      TempValueEntry.FILTERGROUP(DATABASE::"Item Analysis View Entry"); // Trick: FILTERGROUP is used to transfer an integer value
      PAGE.RUNMODAL(PAGE::"Value Entries",TempValueEntry);
    END;

    PROCEDURE SetAnalysisViewEntry@2(ItemAnalysisViewEntry@1000 : Record 7154);
    VAR
      ItemAViewEntryToValueEntries@1001 : Codeunit 7151;
    BEGIN
      TempValueEntry.RESET;
      TempValueEntry.DELETEALL;
      ItemAViewEntryToValueEntries.GetValueEntries(ItemAnalysisViewEntry,TempValueEntry);
    END;

    BEGIN
    END.
  }
}

