OBJECT Page 99000811 Prod. BOM Where-Used
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Fert.-St�cklistenverwendung;
               ENU=Prod. BOM Where-Used];
    SourceTable=Table99000790;
    DataCaptionExpr=SetCaption;
    PageType=Worksheet;
    SourceTableTemporary=Yes;
    OnOpenPage=BEGIN
                 BuildForm;
               END;

    OnFindRecord=BEGIN
                   EXIT(WhereUsedMgt.FindRecord(Which,Rec));
                 END;

    OnNextRecord=BEGIN
                   EXIT(WhereUsedMgt.NextRecord(Steps,Rec));
                 END;

    OnAfterGetRecord=BEGIN
                       DescriptionIndent := 0;
                       DescriptionOnFormat;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 15  ;1   ;Group     ;
                CaptionML=[DEU=Optionen;
                           ENU=Options] }

    { 16  ;2   ;Field     ;
                CaptionML=[DEU=Berechnungsdatum;
                           ENU=Calculation Date];
                ToolTipML=[DEU=Legt das Datum fest, f�r das die Verwendungsliste angezeigt werden soll.;
                           ENU=Specifies a date for which you want to show the where-used lines.];
                SourceExpr=CalculateDate;
                OnValidate=BEGIN
                             CalculateDateOnAfterValidate;
                           END;
                            }

    { 2   ;2   ;Field     ;
                CaptionML=[DEU=Ebenen;
                           ENU=Levels];
                ToolTipML=[DEU=Gibt den Detailgrad f�r die Verwendungszeilen an.;
                           ENU=Specifies the level of detail for the where-used lines.];
                OptionCaptionML=[DEU=Eine,Mehrere;
                                 ENU=Single,Multi];
                SourceExpr=ShowLevel;
                OnValidate=BEGIN
                             ShowLevelOnAfterValidate;
                           END;
                            }

    { 1   ;1   ;Group     ;
                Editable=FALSE;
                IndentationColumnName=DescriptionIndent;
                IndentationControls=Description;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an, dem der Basisartikel oder die Fertigungsst�ckliste zugeordnet ist.;
                           ENU=Specifies the number of the item that the base item or production BOM is assigned to.];
                SourceExpr="Item No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Versionscode der Fertigungsst�ckliste an, dem der Artikel oder die Fertigungsst�ckliste zugeordnet ist.;
                           ENU=Specifies the version code of the production BOM that the item or production BOM component is assigned to.];
                SourceExpr="Version Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des Artikels an, dem der Artikel oder die Fertigungsst�ckliste zugeordnet ist.;
                           ENU=Specifies the description of the item to which the item or production BOM component is assigned.];
                SourceExpr=Description }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels oder der Fertigungsst�cklistenkomponente an, die f�r den zugeordneten Artikel ben�tigt wird.;
                           ENU=Specifies the quantity of the item or the production BOM component that is needed for the assigned item.];
                SourceExpr="Quantity Needed" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Item@1000 : Record 27;
      ProdBOM@1001 : Record 99000771;
      WhereUsedMgt@1002 : Codeunit 99000770;
      ShowLevel@1003 : 'Single,Multi';
      CalculateDate@1004 : Date;
      DescriptionIndent@19057867 : Integer INDATASET;

    PROCEDURE SetProdBOM@2(NewProdBOM@1000 : Record 99000771;NewCalcDate@1001 : Date);
    BEGIN
      ProdBOM := NewProdBOM;
      CalculateDate := NewCalcDate;
    END;

    PROCEDURE SetItem@4(NewItem@1000 : Record 27;NewCalcDate@1001 : Date);
    BEGIN
      Item := NewItem;
      CalculateDate := NewCalcDate;
    END;

    LOCAL PROCEDURE BuildForm@3();
    BEGIN
      IF ProdBOM."No." <> '' THEN
        WhereUsedMgt.WhereUsedFromProdBOM(ProdBOM,CalculateDate,ShowLevel = ShowLevel::Multi)
      ELSE
        WhereUsedMgt.WhereUsedFromItem(Item,CalculateDate,ShowLevel = ShowLevel::Multi);
    END;

    PROCEDURE SetCaption@1() : Text[80];
    BEGIN
      IF ProdBOM."No." <> '' THEN
        EXIT(ProdBOM."No." + ' ' + ProdBOM.Description);

      EXIT(Item."No." + ' ' + Item.Description);
    END;

    LOCAL PROCEDURE CalculateDateOnAfterValidate@19026152();
    BEGIN
      BuildForm;
      CurrPage.UPDATE(FALSE);
    END;

    LOCAL PROCEDURE ShowLevelOnAfterValidate@19042710();
    BEGIN
      BuildForm;
      CurrPage.UPDATE(FALSE);
    END;

    LOCAL PROCEDURE DescriptionOnFormat@19023855();
    BEGIN
      DescriptionIndent := "Level Code" - 1;
    END;

    BEGIN
    END.
  }
}

