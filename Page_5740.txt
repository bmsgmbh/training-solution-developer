OBJECT Page 5740 Transfer Order
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Umlagerungsauftrag;
               ENU=Transfer Order];
    SourceTable=Table5740;
    PageType=Document;
    RefreshOnActivate=Yes;
    OnDeleteRecord=BEGIN
                     TESTFIELD(Status,Status::Open);
                   END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 57      ;1   ;ActionGroup;
                      CaptionML=[DEU=Auf&trag;
                                 ENU=O&rder];
                      Image=Order }
      { 27      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[DEU=Statistik;
                                 ENU=Statistics];
                      RunObject=Page 5755;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 28      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 5750;
                      RunPageLink=Document Type=CONST(Transfer Order),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 65      ;2   ;Action    ;
                      Name=Dimensions;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Anzeigen oder Bearbeiten von Dimensionen, wie etwa eines Bereichs, eines Projekts oder einer Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 5       ;1   ;ActionGroup;
                      CaptionML=[DEU=Belege;
                                 ENU=Documents];
                      Image=Documents }
      { 81      ;2   ;Action    ;
                      CaptionML=[DEU=&Geb. Umlag.-Ausg�nge;
                                 ENU=S&hipments];
                      RunObject=Page 5752;
                      RunPageLink=Transfer Order No.=FIELD(No.);
                      Image=Shipment }
      { 82      ;2   ;Action    ;
                      CaptionML=[DEU=Geb. &Umlag.-Eing�nge;
                                 ENU=Re&ceipts];
                      RunObject=Page 5753;
                      RunPageLink=Transfer Order No.=FIELD(No.);
                      Image=PostedReceipts }
      { 7       ;1   ;ActionGroup;
                      CaptionML=[DEU=Lager;
                                 ENU=Warehouse];
                      Image=Warehouse }
      { 98      ;2   ;Action    ;
                      CaptionML=[DEU=Waren&ausg�nge;
                                 ENU=Whse. Shi&pments];
                      RunObject=Page 7341;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.,Source Line No.);
                      RunPageLink=Source Type=CONST(5741),
                                  Source Subtype=CONST(0),
                                  Source No.=FIELD(No.);
                      Image=Shipment }
      { 97      ;2   ;Action    ;
                      CaptionML=[DEU=Waren&eing�nge;
                                 ENU=&Whse. Receipts];
                      RunObject=Page 7342;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.,Source Line No.);
                      RunPageLink=Source Type=CONST(5741),
                                  Source Subtype=CONST(1),
                                  Source No.=FIELD(No.);
                      Image=Receipt }
      { 85      ;2   ;Action    ;
                      CaptionML=[DEU=Lager&belegzeilen;
                                 ENU=In&vt. Put-away/Pick Lines];
                      RunObject=Page 5774;
                      RunPageView=SORTING(Source Document,Source No.,Location Code);
                      RunPageLink=Source Document=FILTER(Inbound Transfer|Outbound Transfer),
                                  Source No.=FIELD(No.);
                      Image=PickLines }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 69      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=D&rucken;
                                 ENU=&Print];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 DocPrint@1001 : Codeunit 229;
                               BEGIN
                                 DocPrint.PrintTransferHeader(Rec);
                               END;
                                }
      { 3       ;1   ;ActionGroup;
                      CaptionML=[DEU=Freigeben;
                                 ENU=Release];
                      Image=ReleaseDoc }
      { 59      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F9;
                      CaptionML=[DEU=&Freigeben;
                                 ENU=Re&lease];
                      RunObject=Codeunit 5708;
                      Promoted=Yes;
                      Image=ReleaseDoc;
                      PromotedCategory=Process }
      { 48      ;2   ;Action    ;
                      CaptionML=[DEU=Stat&us zur�cksetzen;
                                 ENU=Reo&pen];
                      Promoted=Yes;
                      Image=ReOpen;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ReleaseTransferDoc@1001 : Codeunit 5708;
                               BEGIN
                                 ReleaseTransferDoc.Reopen(Rec);
                               END;
                                }
      { 58      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 5778    ;2   ;Action    ;
                      AccessByPermission=TableData 7320=R;
                      CaptionML=[DEU=Waren&ausgang erstellen;
                                 ENU=Create Whse. S&hipment];
                      Image=NewShipment;
                      OnAction=VAR
                                 GetSourceDocOutbound@1001 : Codeunit 5752;
                               BEGIN
                                 GetSourceDocOutbound.CreateFromOutbndTransferOrder(Rec);
                               END;
                                }
      { 84      ;2   ;Action    ;
                      AccessByPermission=TableData 7316=R;
                      CaptionML=[DEU=&Wareneingang erstellen;
                                 ENU=Create &Whse. Receipt];
                      Image=NewReceipt;
                      OnAction=VAR
                                 GetSourceDocInbound@1001 : Codeunit 5751;
                               BEGIN
                                 GetSourceDocInbound.CreateFromInbndTransferOrder(Rec);
                               END;
                                }
      { 94      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=La&gerbelege erstellen;
                                 ENU=Create Inventor&y Put-away/Pick];
                      Image=CreateInventoryPickup;
                      OnAction=BEGIN
                                 CreateInvtPutAwayPick;
                               END;
                                }
      { 95      ;2   ;Action    ;
                      AccessByPermission=TableData 7302=R;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Lagerplatzinhalt holen;
                                 ENU=Get Bin Content];
                      Image=GetBinContent;
                      OnAction=VAR
                                 BinContent@1002 : Record 7302;
                                 GetBinContent@1000 : Report 7391;
                               BEGIN
                                 BinContent.SETRANGE("Location Code","Transfer-from Code");
                                 GetBinContent.SETTABLEVIEW(BinContent);
                                 GetBinContent.InitializeTransferHeader(Rec);
                                 GetBinContent.RUNMODAL;
                               END;
                                }
      { 62      ;1   ;ActionGroup;
                      CaptionML=[DEU=Bu&chung;
                                 ENU=P&osting];
                      Image=Post }
      { 66      ;2   ;Action    ;
                      Name=Post;
                      ShortCutKey=F9;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Bu&chen;
                                 ENU=P&ost];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"TransferOrder-Post (Yes/No)",Rec);
                               END;
                                }
      { 67      ;2   ;Action    ;
                      Name=PostAndPrint;
                      ShortCutKey=Shift+F9;
                      CaptionML=[DEU=Buchen und d&rucken;
                                 ENU=Post and &Print];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"TransferOrder-Post + Print",Rec);
                               END;
                                }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1901320106;1 ;Action    ;
                      CaptionML=[DEU=Lager - Eingehende Umlagerung;
                                 ENU=Inventory - Inbound Transfer];
                      RunObject=Report 5702;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Umlagerungsauftrags an.;
                           ENU=Specifies the number of the transfer order.];
                SourceExpr="No.";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, von dem aus die Artikel umgelagert werden.;
                           ENU=Specifies the code of the location from which items are transferred.];
                SourceExpr="Transfer-from Code";
                Importance=Promoted }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, an den Sie Artikel umlagern.;
                           ENU=Specifies the code of the location that you are transferring items to.];
                SourceExpr="Transfer-to Code";
                Importance=Promoted }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den In Transit Code an, der diese Umlagerung kennzeichnet.;
                           ENU=Specifies the in-transit code that identifies this transfer.];
                SourceExpr="In-Transit Code" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Buchungsdatum des Umlagerungsauftrags an.;
                           ENU=Specifies the posting date of the transfer order.];
                SourceExpr="Posting Date";
                OnValidate=BEGIN
                             PostingDateOnAfterValidate;
                           END;
                            }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode f�r die Dimension an, die als Globale Dimension 1 ausgew�hlt wurde.;
                           ENU=Specifies the dimension value code for the dimension that has been chosen as Global Dimension 1.];
                SourceExpr="Shortcut Dimension 1 Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode f�r die Dimension an, die als Globale Dimension 2 ausgew�hlt wurde.;
                           ENU=Specifies the dimension value code for the dimension that has been chosen as Global Dimension 2.];
                SourceExpr="Shortcut Dimension 2 Code" }

    { 106 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der f�r den Beleg verantwortlich ist.;
                           ENU=Specifies the ID of the user who is responsible for the document.];
                SourceExpr="Assigned User ID" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob der Umlagerungsauftrag offen ist oder f�r den n�chsten Bearbeitungsschritt freigegeben wurde.;
                           ENU=Indicates whether the transfer order is open or has been released for the next stage of processing.];
                SourceExpr=Status;
                Importance=Promoted }

    { 55  ;1   ;Part      ;
                Name=TransferLines;
                SubPageLink=Document No.=FIELD(No.),
                            Derived From Line No.=CONST(0);
                PagePartID=Page5741 }

    { 1904655901;1;Group  ;
                CaptionML=[DEU=Umlag. von;
                           ENU=Transfer-from] }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Lagerorts an, von dem aus die Artikel umgelagert werden.;
                           ENU=Specifies the name of the location from which items are transferred.];
                SourceExpr="Transfer-from Name" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen zus�tzlichen Teil des Namens des Lagerorts an, von dem aus die Artikel umgelagert werden.;
                           ENU=Specifies an additional part of the name of the location from which items are transferred.];
                SourceExpr="Transfer-from Name 2" }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Adresse des Lagerorts an, von dem aus die Artikel umgelagert werden.;
                           ENU=Specifies the address of the location from which items are transferred.];
                SourceExpr="Transfer-from Address" }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen zus�tzlichen Teil der Adresse an.;
                           ENU=Specifies an additional part of the address.];
                SourceExpr="Transfer-from Address 2" }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postleitzahl des Lagerorts an, von dem aus die Artikel umgelagert werden.;
                           ENU=Specifies the postal code of the location from which items are transferred.];
                SourceExpr="Transfer-from Post Code" }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ort des Lagerorts an, von dem aus die Artikel umgelagert werden.;
                           ENU=Specifies the city of the location from which items are transferred.];
                SourceExpr="Transfer-from City" }

    { 49  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Kontaktperson am Umlag. von Lagerort an.;
                           ENU=Specifies the name of the contact person at the transfer-from location.];
                SourceExpr="Transfer-from Contact" }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem der Auftrag ausgeliefert werden soll.;
                           ENU=Specifies the date the order is expected to be shipped.];
                SourceExpr="Shipment Date";
                Importance=Promoted;
                OnValidate=BEGIN
                             ShipmentDateOnAfterValidate;
                           END;
                            }

    { 89  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Zeit an, die ben�tigt wird, um die Lieferung aus dem Umlag. von Lagerort in den Umlag. nach Lagerort vorzubereiten.;
                           ENU=Specifies the time it takes for the Transfer-from location to prepare the shipment to the Transfer-to location.];
                SourceExpr="Outbound Whse. Handling Time";
                OnValidate=BEGIN
                             OutboundWhseHandlingTimeOnAfte;
                           END;
                            }

    { 79  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lieferbedingungscode an, den Sie f�r diesen Auftrag angegeben haben.;
                           ENU=Specifies the shipment method code that you have entered for this order.];
                SourceExpr="Shipment Method Code" }

    { 72  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r den Zusteller an, den Sie beauftragen, die Artikel dieses Umlagerungsauftrags auszuliefern.;
                           ENU=Specifies the code for the shipping agent you are using to ship the items on this transfer order.];
                SourceExpr="Shipping Agent Code";
                Importance=Promoted;
                OnValidate=BEGIN
                             ShippingAgentCodeOnAfterValida;
                           END;
                            }

    { 74  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Zustellertransportart an, die Sie beauftragen, die Artikel dieses Umlagerungsauftrags auszuliefern.;
                           ENU=Specifies the code for the shipping agent service that you are using to ship the items on this transfer order.];
                SourceExpr="Shipping Agent Service Code";
                OnValidate=BEGIN
                             ShippingAgentServiceCodeOnAfte;
                           END;
                            }

    { 76  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Transportzeit zur Berechnung des Wareneingangsdatums an.;
                           ENU=Specifies the shipping time, used to calculate the receipt date.];
                SourceExpr="Shipping Time";
                OnValidate=BEGIN
                             ShippingTimeOnAfterValidate;
                           END;
                            }

    { 70  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt Anweisungen f�r das Lager, das die Artikel sendet, ob auch eine Teillieferung akzeptabel ist.;
                           ENU=Specifies advice for the warehouse sending the items, about whether a partial delivery is acceptable.];
                SourceExpr="Shipping Advice";
                Importance=Promoted;
                OnValidate=BEGIN
                             IF "Shipping Advice" <> xRec."Shipping Advice" THEN
                               IF NOT CONFIRM(Text000,FALSE,FIELDCAPTION("Shipping Advice")) THEN
                                 ERROR('');
                           END;
                            }

    { 1901454601;1;Group  ;
                CaptionML=[DEU=Umlag. nach;
                           ENU=Transfer-to] }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Lagerorts an, an den Sie Artikel umlagern.;
                           ENU=Specifies the name of the location that you are transferring items to.];
                SourceExpr="Transfer-to Name" }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen zus�tzlichen Teil des "Umlag. nach Namen" an, an den Sie Artikel umlagern.;
                           ENU=Specifies an additional part of the transfer-to name of the location that you are transferring items to.];
                SourceExpr="Transfer-to Name 2" }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Adresse des Lagerorts an, an den Sie Artikel umlagern.;
                           ENU=Specifies the address of the location that you are transferring items to.];
                SourceExpr="Transfer-to Address" }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen zus�tzlichen Teil der Adresse des Lagerorts an, an den die Artikel umgelagert werden.;
                           ENU=Specifies an additional part of the address of the location to which items are transferred.];
                SourceExpr="Transfer-to Address 2" }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postleitzahl des Lagerorts an, an den Sie Artikel umlagern.;
                           ENU=Specifies the postal code of the location that you are transferring items to.];
                SourceExpr="Transfer-to Post Code" }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ort des Lagerorts an, an den Sie Artikel umlagern.;
                           ENU=Specifies the city of the location that you are transferring items to.];
                SourceExpr="Transfer-to City" }

    { 51  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Kontaktperson am Umlag. nach Lagerort an.;
                           ENU=Specifies the name of the contact person at the transfer-to location.];
                SourceExpr="Transfer-to Contact" }

    { 52  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem Sie die Lieferung am Lagerort von "Umlag. nach Lagerort" erwarten.;
                           ENU=Specifies the date that you expect the transfer-to location to receive the shipment.];
                SourceExpr="Receipt Date";
                OnValidate=BEGIN
                             ReceiptDateOnAfterValidate;
                           END;
                            }

    { 91  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ben�tigte Zeit an, um Artikel dem verf�gbaren Lagerbestand hinzuzuf�gen, nachdem die Artikel als "geliefert" gebucht wurden.;
                           ENU=Specifies the time it takes to make items part of available inventory, after the items have been posted as received.];
                SourceExpr="Inbound Whse. Handling Time";
                OnValidate=BEGIN
                             InboundWhseHandlingTimeOnAfter;
                           END;
                            }

    { 1907468901;1;Group  ;
                CaptionML=[DEU=Au�enhandel;
                           ENU=Foreign Trade] }

    { 104 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Transaktionstyp der Umlagerung an.;
                           ENU=Specifies the transaction type of the transfer.];
                SourceExpr="Transaction Type";
                Importance=Promoted }

    { 102 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Spezifikationscode f�r das Verfahren an, das bei der Umlagerung verwendet wurde.;
                           ENU=Specifies the transaction specification code that was used in the transfer.];
                SourceExpr="Transaction Specification" }

    { 100 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r den Verkehrszweig an, der f�r den Artikel in der Zeile verwendet wird.;
                           ENU=Specifies the code for the transport method used for the item on this line.];
                SourceExpr="Transport Method";
                Importance=Promoted }

    { 96  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r einen Bereich beim Debitor oder Kreditor an, mit dem Sie die Artikel in der Zeile handeln.;
                           ENU=Specifies the code for an area at the customer or vendor with which you are trading the items on the line.];
                SourceExpr=Area }

    { 83  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Entladehafens an, an dem die Artikel in Ihr Land/Ihre Region oder in den Einladehafen gekommen sind.;
                           ENU=Specifies the code of either the port of entry at which the items passed into your country/region, or the port of exit.];
                SourceExpr="Entry/Exit Point" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'DEU=M�chten Sie %1 in allen verkn�pften Datens�tzen im Lager �ndern?;ENU=Do you want to change %1 in all related records in the warehouse?';

    LOCAL PROCEDURE PostingDateOnAfterValidate@19003005();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE ShipmentDateOnAfterValidate@19068710();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE ShippingAgentServiceCodeOnAfte@19046563();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE ShippingAgentCodeOnAfterValida@19008956();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE ShippingTimeOnAfterValidate@19029567();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE OutboundWhseHandlingTimeOnAfte@19078070();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE ReceiptDateOnAfterValidate@19074743();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE InboundWhseHandlingTimeOnAfter@19076948();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    BEGIN
    END.
  }
}

