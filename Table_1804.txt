OBJECT Table 1804 Approval Workflow Wizard
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Genehmigungsworkflow-Assistent;
               ENU=Approval Workflow Wizard];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[DEU=Prim�rschl�ssel;
                                                              ENU=Primary Key] }
    { 2   ;   ;Approver ID         ;Code50        ;TableRelation="User Setup"."User ID";
                                                   CaptionML=[DEU=Genehmiger-ID;
                                                              ENU=Approver ID] }
    { 3   ;   ;Sales Invoice App. Workflow;Boolean;CaptionML=[DEU=Workflow Verkaufsrechnungsgenehmigung;
                                                              ENU=Sales Invoice App. Workflow] }
    { 4   ;   ;Sales Amount Approval Limit;Integer;CaptionML=[DEU=Grenzbetrag f�r Verkauf;
                                                              ENU=Sales Amount Approval Limit];
                                                   MinValue=0 }
    { 5   ;   ;Purch Invoice App. Workflow;Boolean;CaptionML=[DEU=Genehmigungsworkflow Einkaufsrechnung;
                                                              ENU=Purch Invoice App. Workflow] }
    { 6   ;   ;Purch Amount Approval Limit;Integer;CaptionML=[DEU=Grenzbetrag f�r Einkauf;
                                                              ENU=Purch Amount Approval Limit];
                                                   MinValue=0 }
    { 7   ;   ;Use Exist. Approval User Setup;Boolean;
                                                   CaptionML=[DEU=Vorhandene Genehmigungsbenutzereinrichtung verwenden;
                                                              ENU=Use Exist. Approval User Setup] }
    { 10  ;   ;Field               ;Integer       ;TableRelation=Field.No. WHERE (TableNo=CONST(18));
                                                   CaptionML=[DEU=Feld;
                                                              ENU=Field] }
    { 11  ;   ;TableNo             ;Integer       ;CaptionML=[DEU=Tabellennummer;
                                                              ENU=TableNo] }
    { 12  ;   ;Field Caption       ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Field."Field Caption" WHERE (TableNo=FIELD(TableNo),
                                                                                                   No.=FIELD(Field)));
                                                   CaptionML=[DEU=Feldbezeichnung;
                                                              ENU=Field Caption] }
    { 13  ;   ;Custom Message      ;Text250       ;CaptionML=[DEU=Benutzerdefinierte Nachricht;
                                                              ENU=Custom Message] }
    { 14  ;   ;App. Trigger        ;Option        ;CaptionML=[DEU=Genehmigungstrigger;
                                                              ENU=App. Trigger];
                                                   OptionCaptionML=[DEU=Der Benutzer sendet eine Genehmigungsanforderung manuell,Der Benutzer �ndert ein spezifisches Feld;
                                                                    ENU=The user sends an approval requests manually,The user changes a specific field];
                                                   OptionString=The user sends an approval requests manually,The user changes a specific field }
    { 15  ;   ;Field Operator      ;Option        ;CaptionML=[DEU=Feldoperator;
                                                              ENU=Field Operator];
                                                   OptionCaptionML=[DEU=Erh�ht,Gefallen,Ge�ndert;
                                                                    ENU=Increased,Decreased,Changed];
                                                   OptionString=Increased,Decreased,Changed }
    { 38  ;   ;Journal Batch Name  ;Code10        ;TableRelation="Gen. Journal Batch".Name;
                                                   CaptionML=[DEU=Buch.-Blattname;
                                                              ENU=Journal Batch Name] }
    { 39  ;   ;For All Batches     ;Boolean       ;CaptionML=[DEU=F�r alle Chargen;
                                                              ENU=For All Batches] }
    { 40  ;   ;Journal Template Name;Code10       ;CaptionML=[DEU=Buch.-Blattvorlagenname;
                                                              ENU=Journal Template Name] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

