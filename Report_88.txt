OBJECT Report 88 VAT- VIES Declaration Disk
{
  OBJECT-PROPERTIES
  {
    Date=28.01.17;
    Time=12:00:00;
    Version List=NAVW110.00.00.15140,NAVDE10.00.00.15140;
  }
  PROPERTIES
  {
    Permissions=TableData 254=imd;
    CaptionML=[DEU=Zusammenfassende Meldung Disk;
               ENU=VAT- VIES Declaration Disk];
    ProcessingOnly=Yes;
    OnPreReport=BEGIN
                  FileName := FileManagement.ServerTempFileName('txt');
                END;

    OnPostReport=BEGIN
                   IF NOT HideFileDialog THEN BEGIN
                     FileManagement.DownloadHandler(FileName ,'','',FileManagement.GetToFilterText('',FileName),ToFileNameTxt);
                     FileManagement.DeleteServerFile(FileName);
                   END
                 END;

  }
  DATASET
  {
    { 7612;    ;DataItem;                    ;
               DataItemTable=Table254;
               DataItemTableView=SORTING(Type,Country/Region Code,VAT Registration No.,VAT Bus. Posting Group,VAT Prod. Posting Group,Posting Date)
                                 WHERE(Type=CONST(Sale));
               OnPreDataItem=BEGIN
                               IF FileVersion = '' THEN
                                 ERROR(FileVersionNotDefinedErr);
                               IF FileVersion2 = '' THEN
                                 ERROR(FileVersionElsterOnlineNotDefinedErr);
                               CLEAR(VATFile);
                               VATFile.TEXTMODE := TRUE;
                               VATFile.WRITEMODE := TRUE;
                               VATFile.CREATE(FileName);

                               CompanyInfo.GET;
                               VATRegNo := CONVERTSTR(CompanyInfo."VAT Registration No.",Text001,'    ');
                               VATFile.WRITE('#v' + FileVersion);
                               VATFile.WRITE('#ve' + FileVersion2);
                               VATFile.WRITE('Laenderkennzeichen,USt-IdNr.,Betrag(Euro),Art der Leistung,Importmeldung');
                               NoOfGrTotal := 0;
                               Period := GETRANGEMAX("Posting Date");
                               InternalReferenceNo := FORMAT(Period,4,2) + '000000';
                             END;

               OnAfterGetRecord=VAR
                                  VATEntry@1001 : Record 254;
                                  TotalValueOfItemSupplies@1003 : Decimal;
                                  TotalValueOfServiceSupplies@1002 : Decimal;
                                  GroupTotal@1000 : Boolean;
                                BEGIN
                                  IF "EU Service" THEN BEGIN
                                    HasServiceSupplies := TRUE;
                                    IF UseAmtsInAddCurr THEN
                                      TotalValueOfServiceSupplies := "Additional-Currency Base"
                                    ELSE
                                      TotalValueOfServiceSupplies := Base;
                                  END ELSE BEGIN
                                    HasItemSupplies := TRUE;
                                    IF UseAmtsInAddCurr THEN
                                      TotalValueOfItemSupplies := "Additional-Currency Base"
                                    ELSE
                                      TotalValueOfItemSupplies := Base;
                                  END;

                                  IF "EU 3-Party Trade" THEN BEGIN
                                    HasEU3PartyTrade := TRUE;
                                    EU3PartyItemTradeAmt := EU3PartyItemTradeAmt + TotalValueOfItemSupplies;
                                    EU3PartyServiceTradeAmt := EU3PartyServiceTradeAmt + TotalValueOfServiceSupplies;
                                  END ELSE BEGIN
                                    TotalValueofItemSuppliesTotal += TotalValueOfItemSupplies;
                                    TotalValueofServiceSuppliesTot += TotalValueOfServiceSupplies;
                                  END;

                                  VATEntry.COPY("VAT Entry");
                                  IF VATEntry.NEXT = 1 THEN BEGIN
                                    IF (VATEntry."Country/Region Code" <> "Country/Region Code") OR
                                       (VATEntry."VAT Registration No." <> "VAT Registration No.")
                                    THEN
                                      GroupTotal := TRUE;
                                  END ELSE
                                    GroupTotal := TRUE;

                                  IF GroupTotal THEN BEGIN
                                    WriteGrTotalsToFile(TotalValueofServiceSuppliesTot,TotalValueofItemSuppliesTotal,
                                      EU3PartyServiceTradeAmt,EU3PartyItemTradeAmt,
                                      HasEU3PartyTrade,HasItemSupplies,HasServiceSupplies);
                                    EU3PartyItemTradeTotalAmt += EU3PartyItemTradeAmt;
                                    EU3PartyServiceTradeTotalAmt += EU3PartyServiceTradeAmt;

                                    TotalValueofItemSuppliesTotal := 0;
                                    TotalValueofServiceSuppliesTot := 0;

                                    EU3PartyItemTradeAmt := 0;
                                    EU3PartyServiceTradeAmt := 0;

                                    HasEU3PartyTrade := FALSE;
                                    HasItemSupplies := FALSE;
                                    HasServiceSupplies := FALSE;
                                  END;
                                END;

               OnPostDataItem=BEGIN
                                VATFile.CLOSE;
                              END;

               ReqFilterFields=VAT Bus. Posting Group,VAT Prod. Posting Group,Posting Date }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[DEU=Optionen;
                             ENU=Options] }

      { 1   ;2   ;Field     ;
                  CaptionML=[DEU=Betr�ge in Berichtsw�hrung ausgeben;
                             ENU=Show Amounts in Add. Reporting Currency];
                  SourceExpr=UseAmtsInAddCurr;
                  MultiLine=Yes }

      { 3   ;2   ;Field     ;
                  Name=FileVersion;
                  ToolTipML=[DEU=Gibt die Dateiversion an. Weitere Informationen finden Sie unter "https://www.elsteronline.de/hilfe/eop/private/formulare/leitfaden/zm_import.html".;
                             ENU=Specifies the file version. For more information, see https://www.elsteronline.de/hilfe/eop/private/formulare/leitfaden/zm_import.html];
                  OptionCaptionML=[DEU=Dateiversion (Elster online);
                                   ENU=File version (Elster online)];
                  SourceExpr=FileVersion }

      { 4   ;2   ;Field     ;
                  Name=FileVersion 2;
                  CaptionML=[DEU=Dateiversion 2 (Elster online);
                             ENU=File version 2 (Elster online)];
                  ToolTipML=[DEU=Gibt die Version von ElsterOnline an. Weitere Informationen finden Sie unter "https://www.elsteronline.de/hilfe/eop/private/formulare/leitfaden/zm_import.html".;
                             ENU=Specifies the version of ElsterOnline. For more information, see https://www.elsteronline.de/hilfe/eop/private/formulare/leitfaden/zm_import.html];
                  SourceExpr=FileVersion2 }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Text001@1001 : TextConst 'DEU=WwWw;ENU=WwWw';
      Text003@1003 : TextConst 'DEU="%1 wurde nicht in allen MwSt.-Posten ausgef�llt, wo %2 = %3 ist";ENU="%1 was not filled in for all VAT entries in which %2 = %3."';
      CompanyInfo@1005 : Record 79;
      Country@1006 : Record 9;
      Cust@1007 : Record 18;
      FileManagement@1000 : Codeunit 419;
      VATFile@1008 : File;
      TotalValueofServiceSuppliesTot@1021 : Decimal;
      TotalValueofItemSuppliesTotal@1013 : Decimal;
      EU3PartyServiceTradeAmt@1022 : Decimal;
      EU3PartyItemTradeAmt@1014 : Decimal;
      EU3PartyItemTradeTotalAmt@1016 : Decimal;
      EU3PartyServiceTradeTotalAmt@1023 : Decimal;
      NoOfGrTotal@1020 : Integer;
      FileName@1009 : Text;
      VATRegNo@1010 : Code[20];
      InternalReferenceNo@1011 : Text[10];
      Period@1012 : Date;
      UseAmtsInAddCurr@1015 : Boolean;
      ToFileNameTxt@1018 : TextConst 'DEU=Default.csv;ENU=Default.csv';
      HideFileDialog@1024 : Boolean;
      FileVersion@1026 : Text[30];
      FileVersion2@1025 : Text[30];
      HasEU3PartyTrade@1010000 : Boolean;
      HasItemSupplies@1010001 : Boolean;
      HasServiceSupplies@1010002 : Boolean;
      FileVersionNotDefinedErr@1140000 : TextConst 'DEU=Sie m�ssen die Dateiversion angeben.;ENU=You must specify file version.';
      FileVersionElsterOnlineNotDefinedErr@1140001 : TextConst 'DEU=Sie m�ssen die Dateiversion (ElsterOnline) angeben.;ENU=You must specify file version (Elster online).';

    LOCAL PROCEDURE WriteGrTotalsToFile@4(TotalValueofServiceSupplies@1003 : Decimal;TotalValueofItemSupplies@1002 : Decimal;EU3PartyServiceTradeAmt@1001 : Decimal;EU3PartyItemTradeAmt@1000 : Decimal;HasEU3Party@1010004 : Boolean;HasItem@1010005 : Boolean;HasService@1010006 : Boolean);
    BEGIN
      WITH "VAT Entry" DO BEGIN
        IF "VAT Registration No." = '' THEN BEGIN
          Type := Type::Sale;
          ERROR(
            Text003,
            FIELDCAPTION("VAT Registration No."),FIELDCAPTION(Type),Type);
        END;

        Cust.GET("Bill-to/Pay-to No.");
        Cust.TESTFIELD("Country/Region Code");
        Country.GET(Cust."Country/Region Code");
        Cust.TESTFIELD("VAT Registration No.");
        Country.GET("Country/Region Code");
        Country.TESTFIELD("EU Country/Region Code");
        NoOfGrTotal := NoOfGrTotal + 1;

        InternalReferenceNo := INCSTR(InternalReferenceNo);
        SETRANGE("Country/Region Code","Country/Region Code");
        SETRANGE("Bill-to/Pay-to No.","Bill-to/Pay-to No.");
        MODIFYALL("Internal Ref. No.",InternalReferenceNo);
        SETRANGE("Country/Region Code");
        SETRANGE("Bill-to/Pay-to No.");

        IF HasItem THEN
          WriteLineToFile(-TotalValueofItemSupplies,'L');
        IF HasService THEN
          WriteLineToFile(-TotalValueofServiceSupplies,'S');
        IF HasEU3Party THEN
          WriteLineToFile(-(EU3PartyItemTradeAmt + EU3PartyServiceTradeAmt),'D');
      END;
    END;

    PROCEDURE GetFileName@6() : Text[1024];
    BEGIN
      EXIT(FileName);
    END;

    PROCEDURE InitializeRequest@5(NewHideFileDialog@1000 : Boolean);
    BEGIN
      HideFileDialog := NewHideFileDialog;
    END;

    PROCEDURE WriteLineToFile@1010000(ExportAmount@1010000 : Decimal;DLS@1010001 : Text[1]);
    VAR
      VATRegNo@1010002 : Text[20];
    BEGIN
      WITH "VAT Entry" DO BEGIN
        VATRegNo := "VAT Registration No.";
        IF COPYSTR(VATRegNo,1,STRLEN(Country."EU Country/Region Code")) = Country."EU Country/Region Code" THEN
          VATRegNo := COPYSTR(VATRegNo,STRLEN(Country."EU Country/Region Code") + 1);
        VATFile.WRITE(
          FORMAT(Country."EU Country/Region Code",2) + ',' +
          FORMAT(VATRegNo) + ',' +
          DELCHR(FORMAT(ROUND(ExportAmount,1)),'=','.,') + ',' +
          DLS);
      END;
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

