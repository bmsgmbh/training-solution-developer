OBJECT Page 5986 Service Item Component List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Serviceartikelkomponenten;
               ENU=Service Item Component List];
    SourceTable=Table5941;
    DelayedInsert=Yes;
    DataCaptionFields=Parent Service Item No.,Line No.;
    PageType=List;
    AutoSplitKey=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 31      ;1   ;ActionGroup;
                      CaptionML=[DEU=K&omponente;
                                 ENU=Com&ponent];
                      Image=Components }
      { 32      ;2   ;Action    ;
                      CaptionML=[DEU=&Aus St�ckliste �bernehmen;
                                 ENU=&Copy from BOM];
                      Image=CopyFromBOM;
                      OnAction=BEGIN
                                 ServItem.GET("Parent Service Item No.");
                                 CODEUNIT.RUN(CODEUNIT::"ServComponent-Copy from BOM",ServItem);
                               END;
                                }
      { 33      ;2   ;ActionGroup;
                      CaptionML=[DEU=&Ersetzte Komponenten;
                                 ENU=&Replaced List];
                      Image=ItemSubstitution }
      { 34      ;3   ;Action    ;
                      Name=ThisLine;
                      CaptionML=[DEU=Diese Zeile;
                                 ENU=This Line];
                      RunObject=Page 5987;
                      RunPageView=SORTING(Active,Parent Service Item No.,From Line No.);
                      RunPageLink=Active=CONST(No),
                                  Parent Service Item No.=FIELD(Parent Service Item No.),
                                  From Line No.=FIELD(Line No.);
                      Image=Line }
      { 35      ;3   ;Action    ;
                      Name=AllLines;
                      CaptionML=[DEU=Alle Zeilen;
                                 ENU=All Lines];
                      RunObject=Page 5987;
                      RunPageView=SORTING(Active,Parent Service Item No.,From Line No.);
                      RunPageLink=Active=CONST(No),
                                  Parent Service Item No.=FIELD(Parent Service Item No.);
                      Image=AllLines }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Serviceartikels an, der die Komponente enth�lt.;
                           ENU=Specifies the number of the service item in which the component is included.];
                SourceExpr="Parent Service Item No.";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Zeile an.;
                           ENU=Specifies the number of the line.];
                SourceExpr="Line No.";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass die Komponente verwendet wird.;
                           ENU=Specifies that the component is in use.];
                SourceExpr=Active;
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Komponentenart an.;
                           ENU=Specifies the component type.];
                SourceExpr=Type }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt auf Grundlage des Werts im Feld "Art" die Nummer eines Artikels oder eines Serviceartikels an.;
                           ENU=Specifies the number of an item or a service item, based on the value in the Type field.];
                SourceExpr="No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Variantencode der Komponente an.;
                           ENU=Specifies the variant code of the component.];
                SourceExpr="Variant Code" }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Komponente an.;
                           ENU=Specifies a description of the component.];
                SourceExpr=Description }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Seriennummer der Komponente an.;
                           ENU=Specifies the serial number of the component.];
                SourceExpr="Serial No.";
                OnAssistEdit=BEGIN
                               AssistEditSerialNo;
                             END;
                              }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Komponente installiert wurde.;
                           ENU=Specifies the date when the component was installed.];
                SourceExpr="Date Installed" }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Zeilennummer an, die der Komponente zugeordnet ist, sofern es sich um eine aktive Komponente des Serviceartikels handelt.;
                           ENU=Specifies the line number assigned to the component when it was an active component of the service item.];
                SourceExpr="From Line No.";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Komponente zuletzt ge�ndert wurde.;
                           ENU=Specifies the date when the component was last modified.];
                SourceExpr="Last Date Modified";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ServItem@1000 : Record 5940;

    BEGIN
    END.
  }
}

