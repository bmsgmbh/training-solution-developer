OBJECT Page 9191 Delete User Personalization
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Benutzeranpassung l�schen;
               ENU=Delete User Personalization];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table2000000075;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1106000000;1;Group  ;
                GroupType=Repeater }

    { 1106000001;2;Field  ;
                CaptionML=[DEU=Benutzer-SID;
                           ENU=User SID];
                ToolTipML=[DEU=Gibt die Sicherheits-ID (SID) des Benutzers an, der die Anpassung durchgef�hrt hat.;
                           ENU=Specifies the security identifier (SID) of the user who performed the personalization.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User SID" }

    { 1106000003;2;Field  ;
                CaptionML=[DEU=Benutzer-ID;
                           ENU=User ID];
                ToolTipML=[DEU=Gibt die Benutzer-ID des Benutzers an, der die Anpassung durchgef�hrt hat.;
                           ENU=Specifies the user ID of the user who performed the personalization.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User ID" }

    { 1106000005;2;Field  ;
                CaptionML=[DEU=Seiten-ID;
                           ENU=Page ID];
                ToolTipML=[DEU=Gibt die Nummer des Seitenobjekts an, das angepasst wurde.;
                           ENU=Specifies the number of the page object that has been personalized.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Page ID" }

    { 1106000007;2;Field  ;
                CaptionML=[DEU=Beschreibung;
                           ENU=Description];
                ToolTipML=[DEU=Gibt eine Beschreibung der Anpassung an.;
                           ENU=Specifies a description of the personalization.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 1106000009;2;Field  ;
                CaptionML=[DEU=Datum;
                           ENU=Date];
                ToolTipML=[DEU=Gibt das Datum der Anpassung an.;
                           ENU=Specifies the date of the personalization.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Date }

    { 1106000011;2;Field  ;
                CaptionML=[DEU=Zeit;
                           ENU=Time];
                ToolTipML=[DEU=Gibt den Zeitstempel f�r die Anpassung an.;
                           ENU=Specifies the timestamp for the personalization.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Time }

  }
  CODE
  {

    BEGIN
    END.
  }
}

