OBJECT Table 1705 Posted Deferral Line
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Gebuchte Abgrenzungszeile;
               ENU=Posted Deferral Line];
  }
  FIELDS
  {
    { 1   ;   ;Deferral Doc. Type  ;Option        ;TableRelation="Posted Deferral Header"."Deferral Doc. Type";
                                                   CaptionML=[DEU=Abgrenzungsbelegtyp;
                                                              ENU=Deferral Doc. Type];
                                                   OptionCaptionML=[DEU=Einkauf,Verkauf,Sachkonto;
                                                                    ENU=Purchase,Sales,G/L];
                                                   OptionString=Purchase,Sales,G/L }
    { 2   ;   ;Gen. Jnl. Document No.;Code20      ;TableRelation="Posted Deferral Header"."Gen. Jnl. Document No.";
                                                   CaptionML=[DEU=Fibu Buch.-Bl.-Belegnnr.;
                                                              ENU=Gen. Jnl. Document No.] }
    { 3   ;   ;Account No.         ;Code20        ;TableRelation="Posted Deferral Header"."Account No.";
                                                   CaptionML=[DEU=Kontonr.;
                                                              ENU=Account No.] }
    { 4   ;   ;Document Type       ;Integer       ;TableRelation="Posted Deferral Header"."Document Type";
                                                   CaptionML=[DEU=Belegart;
                                                              ENU=Document Type] }
    { 5   ;   ;Document No.        ;Code20        ;TableRelation="Posted Deferral Header"."Document No.";
                                                   CaptionML=[DEU=Belegnr.;
                                                              ENU=Document No.] }
    { 6   ;   ;Line No.            ;Integer       ;TableRelation="Posted Deferral Header"."Line No.";
                                                   CaptionML=[DEU=Zeilennr.;
                                                              ENU=Line No.] }
    { 7   ;   ;Posting Date        ;Date          ;CaptionML=[DEU=Buchungsdatum;
                                                              ENU=Posting Date] }
    { 8   ;   ;Description         ;Text50        ;CaptionML=[DEU=Beschreibung;
                                                              ENU=Description] }
    { 9   ;   ;Amount              ;Decimal       ;CaptionML=[DEU=Betrag;
                                                              ENU=Amount];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 10  ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[DEU=Betrag (MW);
                                                              ENU=Amount (LCY)];
                                                   AutoFormatType=1 }
    { 11  ;   ;Currency Code       ;Code10        ;CaptionML=[DEU=W�hrungscode;
                                                              ENU=Currency Code] }
    { 12  ;   ;Deferral Account    ;Code20        ;TableRelation="G/L Account" WHERE (Account Type=CONST(Posting),
                                                                                      Blocked=CONST(No));
                                                   CaptionML=[DEU=Abgrenzungskonto;
                                                              ENU=Deferral Account];
                                                   NotBlank=Yes }
  }
  KEYS
  {
    {    ;Deferral Doc. Type,Gen. Jnl. Document No.,Account No.,Document Type,Document No.,Line No.,Posting Date;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE InitFromDeferralLine@4(DeferralLine@1000 : Record 1702;GenJnlDocNo@1001 : Code[20];AccountNo@1006 : Code[20];NewDocumentType@1005 : Integer;NewDocumentNo@1004 : Code[20];NewLineNo@1003 : Integer;DeferralAccount@1007 : Code[20]);
    BEGIN
      INIT;
      TRANSFERFIELDS(DeferralLine);
      "Gen. Jnl. Document No." := GenJnlDocNo;
      "Account No." := AccountNo;
      "Document Type" := NewDocumentType;
      "Document No." := NewDocumentNo;
      "Line No." := NewLineNo;
      "Deferral Account" := DeferralAccount;
      INSERT;
    END;

    BEGIN
    END.
  }
}

