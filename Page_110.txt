OBJECT Page 110 Customer Posting Groups
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Debitorenbuchungsgruppen;
               ENU=Customer Posting Groups];
    SourceTable=Table92;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Debitorenbuchungsgruppencode an.;
                           ENU=Specifies a customer posting group code.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von Forderungen von Debitoren in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post receivables from customers in this posting group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Receivables Account" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von Servicegeb�hren f�r Debitoren in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post service charges for customers in this posting group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Service Charge Acc." }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von Debitoren gew�hrten Rabatten in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post payment discounts granted to customers in this posting group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Payment Disc. Debit Acc." }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von Debitoren gew�hrten reduzierten Skonti in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post reductions in payment discounts granted to customers in this posting group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Payment Disc. Credit Acc." }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von Zinsen aus Mahnungen und Zinsrechnungen f�r Debitoren in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post interest from reminders and finance charge memos for customers in this posting group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Interest Account" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von Geb�hren aus Mahnungen und Zinsrechnungen f�r Debitoren in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post additional fees from reminders and finance charge memos for customers in this posting group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Additional Fee Account" }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Konto an, auf das Geb�hren gebucht werden.;
                           ENU=Specifies the account that additional fees are posted to.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Add. Fee per Line Account" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, auf welches Konto Betr�ge aus Rechnungsrundungen gebucht werden sollen, wenn Sie Transaktionen mit Debitoren buchen.;
                           ENU=Specifies to which account to post amounts resulting from invoice rounding when you post transactions involving customers.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Invoice Rounding Account" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos an, auf das Rundungsdifferenzen gebucht werden, die auftreten k�nnen, wenn Sie Posten in unterschiedlichen W�hrungen miteinander ausgleichen.;
                           ENU=Specifies the general ledger account number to post rounding differences that can occur when you apply entries in different currencies to one another.];
                ApplicationArea=#Suite;
                SourceExpr="Debit Curr. Appln. Rndg. Acc." }

    { 21  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos an, auf das Rundungsdifferenzen gebucht werden, die auftreten k�nnen, wenn Sie Posten in unterschiedlichen W�hrungen miteinander ausgleichen.;
                           ENU=Specifies the general ledger account number to post rounding differences that can occur when you apply entries in different currencies to one another.];
                ApplicationArea=#Suite;
                SourceExpr="Credit Curr. Appln. Rndg. Acc." }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt die Nummer des Sachkontos zum Buchen von Rundungsdifferenzen aus dem Restbetrag an.;
                           ENU=Specifies the general ledger account number to post rounding differences from remaining amount.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Debit Rounding Account" }

    { 25  ;2   ;Field     ;
                ToolTipML=[DEU=Zeigt die Nummer des Sachkontos zum Buchen von Rundungsdifferenzen aus dem Restbetrag an.;
                           ENU=Specifies the general ledger account number to post rounding differences from remaining amount.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Credit Rounding Account" }

    { 29  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos an, auf das die Zahlungstoleranz f�r diese Kombination aus Gesch�fts- und Produktgruppe beim Buchen von Zahlungen f�r Verk�ufe gebucht wird.;
                           ENU=Specifies the general ledger account number to post payment tolerance when you post payments for sales with this particular combination of business group and product group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Payment Tolerance Debit Acc." }

    { 31  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos an, auf das die Zahlungstoleranz f�r diese Kombination aus Gesch�fts- und Produktgruppe beim Buchen von Zahlungen f�r Verk�ufe gebucht wird.;
                           ENU=Specifies the general ledger account number to post payment tolerance when you post payments for sales with this particular combination of business group and product group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Payment Tolerance Credit Acc." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

