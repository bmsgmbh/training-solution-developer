OBJECT Page 17 G/L Account Card
{
  OBJECT-PROPERTIES
  {
    Date=30.03.17;
    Time=12:00:00;
    Version List=NAVW110.00.00.16177;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Sachkontokarte;
               ENU=G/L Account Card];
    SourceTable=Table15;
    PageType=Card;
    RefreshOnActivate=Yes;
    PromotedActionCategoriesML=[DEU=Neu,Verarbeiten,Melden,Konto,Saldo;
                                ENU=New,Process,Report,Account,Balance];
    OnAfterGetRecord=BEGIN
                       CALCFIELDS("Account Subcategory Descript.");
                       SubCategoryDescription := "Account Subcategory Descript.";
                     END;

    OnNewRecord=BEGIN
                  SetupNewGLAcc(xRec,BelowxRec);
                END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 36      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Konto;
                                 ENU=A&ccount];
                      Image=ChartOfAccounts }
      { 41      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=&Posten;
                                 ENU=Ledger E&ntries];
                      ToolTipML=[DEU=Zeigt die Historie der Transaktionen an, die f�r den ausgew�hlten Datensatz gebucht wurden.;
                                 ENU=View the history of transactions that have been posted for the selected record.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 20;
                      RunPageView=SORTING(G/L Account No.)
                                  ORDER(Descending);
                      RunPageLink=G/L Account No.=FIELD(No.);
                      Promoted=Yes;
                      Image=GLRegisters;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes }
      { 38      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      ToolTipML=[DEU=Zeigen Sie Bemerkungen an, oder f�gen Sie sie dem Konto hinzu.;
                                 ENU=View or add comments to the account.];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(G/L Account),
                                  No.=FIELD(No.);
                      Promoted=Yes;
                      Image=ViewComments;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes }
      { 84      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Zeigt Dimensionen an oder bearbeitet sie, wie etwa einen Bereich, ein Projekt oder eine Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      ApplicationArea=#Suite;
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(15),
                                  No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Dimensions;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes }
      { 166     ;2   ;Action    ;
                      CaptionML=[DEU=&Textbausteine;
                                 ENU=E&xtended Texts];
                      ToolTipML=[DEU=Richten Sie einen zus�tzlichen Text f�r die Beschreibung des ausgew�hlten Artikels ein. Erweiterter Text kann im Feld "Beschreibung" in Belegzeilen f�r den Artikel eingef�gt werden.;
                                 ENU=Set up additional text for the description of the selected item. Extended text can be inserted under the Description field on document lines for the item.];
                      ApplicationArea=#Suite;
                      RunObject=Page 391;
                      RunPageView=SORTING(Table Name,No.,Language Code,All Language Codes,Starting Date,Ending Date);
                      RunPageLink=Table Name=CONST(G/L Account),
                                  No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Text;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes }
      { 40      ;2   ;Action    ;
                      CaptionML=[DEU=Liquidit�t;
                                 ENU=Receivables-Payables];
                      ToolTipML=[DEU=Zeigen Sie eine �bersicht �ber die Forderungen und Verbindlichkeiten f�r das Konto an, einschlie�lich f�lliger Debitorensaldo- und Kreditorensaldobetr�ge.;
                                 ENU=View a summary of the receivables and payables for the account, including customer and vendor balance due amounts.];
                      ApplicationArea=#Suite;
                      RunObject=Page 159;
                      Promoted=Yes;
                      Image=ReceivablesPayables;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes }
      { 47      ;2   ;Action    ;
                      CaptionML=[DEU=Verwendungs�bersicht;
                                 ENU=Where-Used List];
                      ToolTipML=[DEU=Zeigen Sie die Einrichtungstabellen an, in denen ein Sachkonto verwendet wird.;
                                 ENU=View setup tables where a general ledger account is used.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Track;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 CalcGLAccWhereUsed@1000 : Codeunit 100;
                               BEGIN
                                 CalcGLAccWhereUsed.CheckGLAcc("No.");
                               END;
                                }
      { 136     ;1   ;ActionGroup;
                      CaptionML=[DEU=&Saldo;
                                 ENU=&Balance];
                      Image=Balance }
      { 46      ;2   ;Action    ;
                      CaptionML=[DEU=Sachk&ontensaldo;
                                 ENU=G/L &Account Balance];
                      ToolTipML=[DEU=Zeigen Sie eine Zusammenfassung der Soll- und Habensalden f�r verschiedene Zeitperioden f�r das im Kontenplan ausgew�hlte Konto an.;
                                 ENU=View a summary of the debit and credit balances for different time periods, for the account that you select in the chart of accounts.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 415;
                      RunPageLink=No.=FIELD(No.),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Business Unit Filter=FIELD(Business Unit Filter);
                      Promoted=Yes;
                      Image=GLAccountBalance;
                      PromotedCategory=Category5;
                      PromotedOnly=Yes }
      { 154     ;2   ;Action    ;
                      CaptionML=[DEU=&Saldo;
                                 ENU=G/L &Balance];
                      ToolTipML=[DEU=Zeigen Sie eine Zusammenfassung der Soll- und Habensalden f�r alle Konten im Kontenplan f�r die ausgew�hlte Zeitperiode an, in der Sie bl�ttern k�nnen.;
                                 ENU=View a scrollable summary of the debit and credit balances for all the accounts in the chart of accounts, for the time period that you select.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 414;
                      RunPageOnRec=Yes;
                      Promoted=Yes;
                      Image=GLBalance;
                      PromotedCategory=Category5;
                      PromotedOnly=Yes }
      { 138     ;2   ;Action    ;
                      CaptionML=[DEU=Saldo nach &Dimension;
                                 ENU=G/L Balance by &Dimension];
                      ToolTipML=[DEU=Zeigt eine Zusammenfassung der Soll- und Habensalden nach Dimensionen f�r das aktuelle Konto an.;
                                 ENU=View a summary of the debit and credit balances by dimensions for the current account.];
                      ApplicationArea=#Suite;
                      RunObject=Page 408;
                      Promoted=Yes;
                      Image=GLBalanceDimension;
                      PromotedCategory=Category5;
                      PromotedOnly=Yes }
      { 1906497203;1 ;Action    ;
                      CaptionML=[DEU=Buchungsmatrix Einrichtung;
                                 ENU=General Posting Setup];
                      ToolTipML=[DEU=Zeigt die Methode zum Einrichten von Kombinationen aus Gesch�ftsbuchungs- und Produktbuchungsgruppen an oder bearbeitet sie.;
                                 ENU=View or edit how you want to set up combinations of general business and general product posting groups.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 314;
                      Promoted=Yes;
                      Image=GeneralPostingSetup;
                      PromotedCategory=Process;
                      PromotedOnly=Yes }
      { 1900660103;1 ;Action    ;
                      CaptionML=[DEU=MwSt.-Buchungsmatrix Einr.;
                                 ENU=VAT Posting Setup];
                      ToolTipML=[DEU=Zeigen Sie Kombinationen aus MwSt.-Gesch�ftsbuchungsgruppen und MwSt.-Produktbuchungsgruppen an, oder bearbeiten Sie sie.;
                                 ENU=View or edit combinations of Tax business posting groups and Tax product posting groups.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 472;
                      Promoted=Yes;
                      Image=VATPostingSetup;
                      PromotedCategory=Process;
                      PromotedOnly=Yes }
      { 1900210203;1 ;Action    ;
                      CaptionML=[DEU=Fibujournal;
                                 ENU=G/L Register];
                      ToolTipML=[DEU=Zeigt Sachposten an.;
                                 ENU=View posted G/L entries.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 116;
                      Promoted=Yes;
                      Image=GLRegisters;
                      PromotedCategory=Process;
                      PromotedOnly=Yes }
      { 15      ;1   ;Action    ;
                      Name=DocsWithoutIC;
                      CaptionML=[DEU=Gebuchte Belege ohne eingehenden Beleg;
                                 ENU=Posted Documents without Incoming Document];
                      ToolTipML=[DEU=Zeigt eine Liste gebuchter Einkaufs- und Verkaufsbelege unter dem Sachkonto an, die nicht �ber entsprechende Eingangsbelege verf�gen.;
                                 ENU=Show a list of posted purchase and sales documents under the G/L account that do not have related incoming document records.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Documents;
                      PromotedCategory=Process;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 PostedDocsWithNoIncBuf@1001 : Record 134;
                               BEGIN
                                 IF "Account Type" = "Account Type"::Posting THEN
                                   PostedDocsWithNoIncBuf.SETRANGE("G/L Account No. Filter","No.")
                                 ELSE
                                   IF Totaling <> '' THEN
                                     PostedDocsWithNoIncBuf.SETFILTER("G/L Account No. Filter",Totaling)
                                   ELSE
                                     EXIT;
                                 PAGE.RUN(PAGE::"Posted Docs. With No Inc. Doc.",PostedDocsWithNoIncBuf);
                               END;
                                }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1900670506;1 ;Action    ;
                      CaptionML=[DEU=Sachkonto - Kontoblatt;
                                 ENU=Detail Trial Balance];
                      ToolTipML=[DEU=Zeigt ausf�hrliche Sachkontosalden und -aktivit�ten f�r alle ausgew�hlten Konten an, eine Transaktion pro Zeile.;
                                 ENU=View detail general ledger account balances and activities for all the selected accounts, one transaction per line.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 4;
                      Image=Report;
                      PromotedCategory=Report }
      { 1904082706;1 ;Action    ;
                      CaptionML=[DEU=Rohbilanz;
                                 ENU=Trial Balance];
                      ToolTipML=[DEU=Zeigt Sachkontosalden und -aktivit�ten f�r alle ausgew�hlten Konten an, eine Transaktion pro Zeile.;
                                 ENU=View general ledger account balances and activities for all the selected accounts, one transaction per line.];
                      ApplicationArea=#Suite;
                      RunObject=Report 6;
                      Image=Report;
                      PromotedCategory=Report }
      { 1902174606;1 ;Action    ;
                      CaptionML=[DEU=Rohbilanz nach Periode;
                                 ENU=Trial Balance by Period];
                      ToolTipML=[DEU=Zeigt Sachkontosalden und -aktivit�ten f�r alle ausgew�hlten Konten an, eine Transaktion pro Zeile in einem ausgew�hlten Zeitraum.;
                                 ENU=View general ledger account balances and activities for all the selected accounts, one transaction per line for a selected period.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 38;
                      Image=Report;
                      PromotedCategory=Report }
      { 1900210206;1 ;Action    ;
                      CaptionML=[DEU=Fibujournal;
                                 ENU=G/L Register];
                      ToolTipML=[DEU=Zeigt Sachposten an.;
                                 ENU=View posted G/L entries.];
                      ApplicationArea=#Suite;
                      RunObject=Report 3;
                      Image=GLRegisters;
                      PromotedCategory=Report }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 61      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktionen;
                                 ENU=F&unctions];
                      Image=Action }
      { 62      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Vorlage anwenden;
                                 ENU=Apply Template];
                      ToolTipML=[DEU=W�hlen Sie eine Konfigurationsvorlage aus, um schnell ein Sachkonto zu erstellen.;
                                 ENU=Select a configuration template to quickly create a general ledger account.];
                      Image=ApplyTemplate;
                      OnAction=VAR
                                 ConfigTemplateMgt@1000 : Codeunit 8612;
                                 RecRef@1001 : RecordRef;
                               BEGIN
                                 RecRef.GETTABLE(Rec);
                                 ConfigTemplateMgt.UpdateFromTemplateSelection(RecRef);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nr. des Sachkontos an, das Sie einrichten.;
                           ENU=Specifies the No. of the G/L Account you are setting up.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No.";
                Importance=Promoted }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Sachkontos an.;
                           ENU=Specifies the name of the general ledger account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name;
                Importance=Promoted }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob es sich bei dem Sachkonto um ein GuV-Konto oder ein Bilanzkonto handelt.;
                           ENU=Specifies whether a general ledger account is an income statement account or a balance sheet account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Income/Balance";
                Importance=Promoted }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Kategorie des Sachkontos an.;
                           ENU=Specifies the category of the G/L account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Account Category";
                OnLookup=BEGIN
                           CurrPage.UPDATE;
                         END;
                          }

    { 13  ;2   ;Field     ;
                Name=SubCategoryDescription;
                CaptionML=[DEU=Kontounterkategorie;
                           ENU=Account Subcategory];
                ToolTipML=[DEU=Gibt die Unterkategorie der Kontokategorie des Sachkontos an.;
                           ENU=Specifies the subcategory of the account category of the G/L account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=SubCategoryDescription;
                OnValidate=BEGIN
                             ValidateAccountSubCategory(SubCategoryDescription);
                             CurrPage.UPDATE;
                           END;

                OnLookup=BEGIN
                           LookupAccountSubCategory;
                           CurrPage.UPDATE;
                         END;
                          }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Posten an, die in der Regel auf dieses Sachkonto gebucht werden.;
                           ENU=Specifies the type of entries that will normally be posted to this general ledger account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Debit/Credit" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Zweck des Kontos an. Neu erstellten Konten wird automatisch die Kontoart "Buchen" zugewiesen, die Sie jedoch manuell �ndern k�nnen.;
                           ENU=Specifies the purpose of the account. Newly created accounts are automatically assigned the Posting account type, but you can change this.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Account Type" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt ein Kontointervall oder eine Liste von Kontonummern an.;
                           ENU=Specifies an account interval or a list of account numbers.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Totaling;
                OnLookup=VAR
                           GLAccountList@1000 : Page 18;
                           OldText@1002 : Text;
                         BEGIN
                           OldText := Text;
                           GLAccountList.LOOKUPMODE(TRUE);
                           IF NOT (GLAccountList.RUNMODAL = ACTION::LookupOK) THEN
                             EXIT(FALSE);

                           Text := OldText + GLAccountList.GetSelectionFilter;
                           EXIT(TRUE);
                         END;
                          }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl von Leerzeilen an, die vor diesem Konto im Kontenplan eingef�gt werden sollen.;
                           ENU=Specifies the number of blank lines that you want inserted before this account in the chart of accounts.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Blank Lines";
                Importance=Additional }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob beim Ausdruck des Kontenplans eine neue Seite nach diesem Sachkonto begonnen werden soll. W�hlen Sie dieses Feld aus, um nach diesem Sachkonto mit einer neuen Seite zu beginnen.;
                           ENU=Specifies whether you want a new page to start immediately after this general ledger account when you print the chart of accounts. Select this field to�start a new page after this�general ledger�account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="New Page";
                Importance=Additional }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Suchbegriff an.;
                           ENU=Specifies a search name.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Search Name";
                Importance=Additional }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Saldo f�r dieses Konto an.;
                           ENU=Specifies the balance on this account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Balance;
                Importance=Promoted }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob dieses Sachkonto in das Fenster "Abstimmung" des Fibu Buch.-Blattes aufgenommen werden soll. Wenn das Sachkonto mit einbezogen werden soll, versehen Sie das Kontrollk�stchen mit einem H�kchen. Sie k�nnen das Fenster "Abstimmung" aufrufen, indem Sie im Fenster "Fibu Buch.-Blatt" auf "Aktionen", "Buchen" klicken.;
                           ENU=Specifies whether this general ledger account will be included in the Reconciliation window in the general journal. To have the G/L account included in the window, place a check mark in the check box. You can find the Reconciliation window by clicking Actions, Posting in the General Journal window.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reconciliation Account" }

    { 57  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass ein Textbaustein automatisch dem Konto hinzugef�gt wird.;
                           ENU=Specifies that an extended text will be added automatically to the account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Automatic Ext. Texts" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob Buchungen direkt oder indirekt auf dieses Sachkonto vorgenommen werden k�nnen. Um Direktbuchungen auf ein Sachkonto zu erm�glichen, versehen Sie dieses Kontrollk�stchen mit einem H�kchen.;
                           ENU=Specifies whether you will be able to post directly or only indirectly to this general ledger account. To allow Direct Posting to the G/L account, place a check mark in the check box.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Direct Posting" }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass Posten nicht im Sachkonto gebucht werden k�nnen.;
                           ENU=Specifies that entries cannot be posted to the G/L account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Blocked }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das letzte �nderungsdatum des Sachkontos an.;
                           ENU=Specifies when the G/L account was last modified.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Last Date Modified" }

    { 7   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Standardbeschreibung automatisch in das Feld "Beschreibung" der Buch.-Blattzeilen eingef�gt wird, die f�r dieses Sachkonto erstellt werden.;
                           ENU=Specifies if the default description is automatically inserted in the Description field on journal lines created for this general ledger account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Omit Default Descr. in Jnl." }

    { 1904784501;1;Group  ;
                CaptionML=[DEU=Buchung;
                           ENU=Posting] }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die allgemeine Buchungsart an, die beim Buchen auf dieses Konto verwendet wird.;
                           ENU=Specifies the general posting type to use when posting to this account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Gen. Posting Type" }

    { 55  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gesch�ftsbuchungsgruppe an, die f�r den Posten gilt.;
                           ENU=Specifies the general business posting group that applies to the entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Gen. Bus. Posting Group";
                Importance=Promoted }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code einer Produktbuchungsgruppe an.;
                           ENU=Specifies a general product posting group code.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Gen. Prod. Posting Group";
                Importance=Promoted }

    { 39  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine MwSt.-Gesch�ftsbuchungsgruppe an.;
                           ENU=Specifies a VAT Bus. Posting Group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Bus. Posting Group";
                Importance=Promoted }

    { 54  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen MwSt.-Produktbuchungsgruppencode an.;
                           ENU=Specifies a VAT Prod. Posting Group code.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Prod. Posting Group";
                Importance=Promoted }

    { 59  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt Konten an, die Sie h�ufig im Feld "Gegenkontonr." in IC-Buch.-Blatt- oder Belegzeilen eingeben.;
                           ENU=Specifies accounts that you often enter in the Bal. Account No. field on intercompany journal or document lines.];
                SourceExpr="Default IC Partner G/L Acc. No" }

    { 9   ;2   ;Field     ;
                CaptionML=[DEU=Standard-Abgrenzungsvorlage;
                           ENU=Default Deferral Template];
                ToolTipML=[DEU=Gibt die Standardabgrenzungsvorlage an, die regelt, wie Einnahmen und Ausgaben von den Perioden des Auftretens abgegrenzt werden.;
                           ENU=Specifies the default deferral template that governs how to defer revenues and expenses to the periods when they occurred.];
                ApplicationArea=#Suite;
                SourceExpr="Default Deferral Template Code" }

    { 1904602201;1;Group  ;
                CaptionML=[DEU=Konsolidierung;
                           ENU=Consolidation] }

    { 50  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Kontonummer in einem Konsolidierungsmandant an, auf den ein auf diesem Konto vorhandener Sollsaldo �bertragen werden soll.;
                           ENU=Specifies the number of the account in a consolidated company to which to transfer debit balances on this account.];
                SourceExpr="Consol. Debit Acc.";
                Importance=Promoted }

    { 52  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Kontonummer in einem Konsolidierungsmandant an, auf den ein auf diesem Konto vorhandener Habensaldo �bertragen werden soll.;
                           ENU=Specifies the number of the account in a consolidated company to which to transfer credit balances on this account.];
                SourceExpr="Consol. Credit Acc.";
                Importance=Promoted }

    { 48  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Konsolidierungsumrechnungsmethode des Kontos an, die den auf das Konto anzuwendenden W�hrungsumrechnungskurs bestimmt.;
                           ENU=Specifies the account's consolidation translation method, which identifies the currency translation rate to be applied to the account.];
                SourceExpr="Consol. Translation Method";
                Importance=Promoted }

    { 1904488501;1;Group  ;
                CaptionML=[DEU=Berichtswesen;
                           ENU=Reporting] }

    { 64  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie Sachkonten bei Kursschwankungen zwischen Mandantenw�hrung und Berichtsw�hrung reguliert werden.;
                           ENU=Specifies how general ledger accounts will be adjusted for exchange rate fluctuations between LCY and the additional reporting currency.];
                ApplicationArea=#Suite;
                SourceExpr="Exchange Rate Adjustment";
                Importance=Promoted }

    { 3   ;1   ;Group     ;
                CaptionML=[DEU=Kostenrechnung;
                           ENU=Cost Accounting];
                GroupType=Group }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Kostenartnummer an, um festzulegen, zu welcher Kostenart ein Sachkonto geh�rt.;
                           ENU=Specifies a cost type number to establish which cost type a general ledger account belongs to.];
                SourceExpr="Cost Type No.";
                Importance=Promoted }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1905532107;1;Part   ;
                SubPageLink=Table ID=CONST(15),
                            No.=FIELD(No.);
                PagePartID=Page9083;
                Visible=FALSE;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      SubCategoryDescription@1000 : Text[80];

    BEGIN
    END.
  }
}

