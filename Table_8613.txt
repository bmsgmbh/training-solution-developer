OBJECT Table 8613 Config. Package Table
{
  OBJECT-PROPERTIES
  {
    Date=30.06.17;
    Time=12:00:00;
    Version List=NAVW110.00.00.17501;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               InitPackageFields;
             END;

    OnDelete=VAR
               ConfigLine@1003 : Record 8622;
               ConfigPackageField@1001 : Record 8616;
               ConfigPackageFilter@1000 : Record 8626;
               ConfigTableProcessingRule@1002 : Record 8631;
             BEGIN
               DeletePackageData;

               ConfigPackageField.SETRANGE("Package Code","Package Code");
               ConfigPackageField.SETRANGE("Table ID","Table ID");
               ConfigPackageField.DELETEALL(TRUE);

               ConfigPackageFilter.SETRANGE("Package Code","Package Code");
               ConfigPackageFilter.SETRANGE("Table ID","Table ID");
               ConfigPackageFilter.DELETEALL;

               ConfigTableProcessingRule.SETRANGE("Package Code","Package Code");
               ConfigTableProcessingRule.SETRANGE("Table ID","Table ID");
               ConfigTableProcessingRule.DELETEALL(TRUE);

               ConfigLine.SETRANGE("Package Code","Package Code");
               ConfigLine.SETRANGE("Table ID","Table ID");
               IF ConfigLine.FINDSET(TRUE) THEN
                 REPEAT
                   ConfigLine."Package Code" := '';
                   ConfigLine."Dimensions as Columns" := FALSE;
                   ConfigLine.MODIFY;
                 UNTIL ConfigLine.NEXT = 0;
             END;

    OnRename=BEGIN
               ERROR(Text004);
             END;

    CaptionML=[DEU=Pakettabelle konfigurieren;
               ENU=Config. Package Table];
  }
  FIELDS
  {
    { 1   ;   ;Package Code        ;Code20        ;TableRelation="Config. Package";
                                                   CaptionML=[DEU=Paketcode;
                                                              ENU=Package Code] }
    { 2   ;   ;Table ID            ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Table));
                                                   OnValidate=BEGIN
                                                                IF ConfigMgt.IsSystemTable("Table ID") THEN
                                                                  ERROR(Text001,"Table ID");

                                                                IF "Table ID" <> xRec."Table ID" THEN
                                                                  "Page ID" := ConfigMgt.FindPage("Table ID");
                                                              END;

                                                   OnLookup=BEGIN
                                                              ConfigValidateMgt.LookupTable("Table ID");
                                                              IF "Table ID" <> 0 THEN
                                                                VALIDATE("Table ID");
                                                            END;

                                                   CaptionML=[DEU=Tabellen-ID;
                                                              ENU=Table ID];
                                                   NotBlank=Yes }
    { 3   ;   ;Table Name          ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Name" WHERE (Object Type=CONST(Table),
                                                                                                             Object ID=FIELD(Table ID)));
                                                   CaptionML=[DEU=Tabellenname;
                                                              ENU=Table Name];
                                                   Editable=No }
    { 4   ;   ;No. of Package Records;Integer     ;FieldClass=FlowField;
                                                   CalcFormula=Count("Config. Package Record" WHERE (Package Code=FIELD(Package Code),
                                                                                                     Table ID=FIELD(Table ID)));
                                                   CaptionML=[DEU=Anzahl der Paketdatens�tze;
                                                              ENU=No. of Package Records];
                                                   Editable=No }
    { 5   ;   ;No. of Package Errors;Integer      ;FieldClass=FlowField;
                                                   CalcFormula=Count("Config. Package Error" WHERE (Package Code=FIELD(Package Code),
                                                                                                    Table ID=FIELD(Table ID)));
                                                   CaptionML=[DEU=Anzahl der Paketfehler;
                                                              ENU=No. of Package Errors];
                                                   Editable=No }
    { 7   ;   ;Imported Date and Time;DateTime    ;CaptionML=[DEU=Datum/Uhrzeit Import;
                                                              ENU=Imported Date and Time];
                                                   Editable=No }
    { 8   ;   ;Exported Date and Time;DateTime    ;CaptionML=[DEU=Datum/Uhrzeit Export;
                                                              ENU=Exported Date and Time];
                                                   Editable=No }
    { 9   ;   ;Comments            ;Text250       ;CaptionML=[DEU=Bemerkungen;
                                                              ENU=Comments] }
    { 10  ;   ;Created Date and Time;DateTime     ;CaptionML=[DEU=Datum/Uhrzeit Erstellung;
                                                              ENU=Created Date and Time] }
    { 11  ;   ;Company Filter (Source Table);Text30;
                                                   FieldClass=FlowFilter;
                                                   TableRelation=Company;
                                                   CaptionML=[DEU=Mandantenfilter (Ursp.Tabelle);
                                                              ENU=Company Filter (Source Table)] }
    { 12  ;   ;Table Caption       ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Caption" WHERE (Object Type=CONST(Table),
                                                                                                                Object ID=FIELD(Table ID)));
                                                   CaptionML=[DEU=Tabellencaption;
                                                              ENU=Table Caption];
                                                   Editable=No }
    { 13  ;   ;Data Template       ;Code10        ;TableRelation="Config. Template Header";
                                                   CaptionML=[DEU=Datenvorlage;
                                                              ENU=Data Template] }
    { 14  ;   ;Package Processing Order;Integer   ;CaptionML=[DEU=Paketverarbeitungsauftrag;
                                                              ENU=Package Processing Order];
                                                   Editable=No }
    { 15  ;   ;Page ID             ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Page));
                                                   OnLookup=BEGIN
                                                              ConfigValidateMgt.LookupPage("Page ID");
                                                              VALIDATE("Page ID");
                                                            END;

                                                   CaptionML=[DEU=Seiten-ID;
                                                              ENU=Page ID] }
    { 16  ;   ;Processing Order    ;Integer       ;CaptionML=[DEU=Verarbeitungsauftrag;
                                                              ENU=Processing Order] }
    { 17  ;   ;No. of Fields Included;Integer     ;FieldClass=FlowField;
                                                   CalcFormula=Count("Config. Package Field" WHERE (Package Code=FIELD(Package Code),
                                                                                                    Table ID=FIELD(Table ID),
                                                                                                    Include Field=CONST(Yes)));
                                                   CaptionML=[DEU=Anz. der eingeschlossenen Felder;
                                                              ENU=No. of Fields Included];
                                                   Editable=No }
    { 18  ;   ;No. of Fields Available;Integer    ;FieldClass=FlowField;
                                                   CalcFormula=Count("Config. Package Field" WHERE (Package Code=FIELD(Package Code),
                                                                                                    Table ID=FIELD(Table ID)));
                                                   CaptionML=[DEU=Anz. der verf�gbaren Felder;
                                                              ENU=No. of Fields Available];
                                                   Editable=No }
    { 19  ;   ;No. of Fields to Validate;Integer  ;FieldClass=FlowField;
                                                   CalcFormula=Count("Config. Package Field" WHERE (Package Code=FIELD(Package Code),
                                                                                                    Table ID=FIELD(Table ID),
                                                                                                    Validate Field=CONST(Yes)));
                                                   CaptionML=[DEU=Anzahl der zu pr�fenden Felder;
                                                              ENU=No. of Fields to Validate];
                                                   Editable=No }
    { 20  ;   ;Package Caption     ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Config. Package"."Package Name" WHERE (Code=FIELD(Package Code)));
                                                   CaptionML=[DEU=Paketbeschriftung;
                                                              ENU=Package Caption];
                                                   Editable=No }
    { 21  ;   ;Imported by User ID ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserManagement@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserManagement.LookupUserID("Imported by User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Importiert von Benutzer-ID;
                                                              ENU=Imported by User ID];
                                                   Editable=No }
    { 22  ;   ;Created by User ID  ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserManagement@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserManagement.LookupUserID("Created by User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Erstellt von Benutzer-ID;
                                                              ENU=Created by User ID];
                                                   Editable=No }
    { 24  ;   ;Dimensions as Columns;Boolean      ;OnValidate=BEGIN
                                                                IF "Dimensions as Columns" THEN BEGIN
                                                                  InitDimensionFields;
                                                                  UpdateDimensionsPackageData;
                                                                END ELSE
                                                                  DeleteDimensionFields;
                                                              END;

                                                   CaptionML=[DEU=Dimensionen als Spalten;
                                                              ENU=Dimensions as Columns] }
    { 25  ;   ;Filtered            ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Config. Package Filter" WHERE (Package Code=FIELD(Package Code),
                                                                                                     Table ID=FIELD(Table ID)));
                                                   CaptionML=[DEU=Gefiltert;
                                                              ENU=Filtered];
                                                   Editable=No }
    { 26  ;   ;Skip Table Triggers ;Boolean       ;CaptionML=[DEU=Tabellentrigger �berspringen;
                                                              ENU=Skip Table Triggers] }
    { 27  ;   ;Delete Recs Before Processing;Boolean;
                                                   CaptionML=[DEU=Datens�tze vor der Verarbeitung l�schen;
                                                              ENU=Delete Recs Before Processing] }
    { 28  ;   ;Processing Report ID;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Report));
                                                   CaptionML=[DEU=Verarbeitungsberichts-ID;
                                                              ENU=Processing Report ID] }
    { 29  ;   ;Parent Table ID     ;Integer       ;TableRelation="Config. Package Table"."Table ID" WHERE (Package Code=FIELD(Package Code),
                                                                                                           Parent Table ID=CONST(0));
                                                   OnValidate=VAR
                                                                ConfigPackageTableChild@1000 : Record 8613;
                                                              BEGIN
                                                                IF "Table ID" = "Parent Table ID" THEN
                                                                  FIELDERROR("Parent Table ID",STRSUBSTNO(CannotBeEqualToErr,FIELDCAPTION("Table ID")));

                                                                ConfigPackageTableChild.SETRANGE("Package Code","Package Code");
                                                                ConfigPackageTableChild.SETRANGE("Parent Table ID","Table ID");
                                                                IF NOT ConfigPackageTableChild.ISEMPTY THEN
                                                                  FIELDERROR("Parent Table ID",STRSUBSTNO(CannotBeSetDueToRelatedTablesErr,"Table ID"));
                                                              END;

                                                   CaptionML=[DEU=�bergeordnete Tabellen-ID;
                                                              ENU=Parent Table ID] }
    { 30  ;   ;Validated           ;Boolean       ;CaptionML=[DEU=�berpr�ft;
                                                              ENU=Validated] }
  }
  KEYS
  {
    {    ;Package Code,Table ID                   ;Clustered=Yes }
    {    ;Package Processing Order,Processing Order }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1001 : TextConst 'DEU=Die Systemtabelle ''%1'' im Paket kann nicht verwendet werden.;ENU=You cannot use system table %1 in the package.';
      Text002@1005 : TextConst 'DEU=Sie k�nnen die Funktion ''Dimensionen als Spalten'' nicht f�r Tabelle %1 verwenden.;ENU=You cannot use the Dimensions as Columns function for table %1.';
      Text003@1006 : TextConst 'DEU=Die Tabellen ''Vorgabedimension'' und ''Dimensionswert'' m�ssen im Paket %1 enthalten sein, um diese Option zu aktivieren. Die fehlenden Tabellen werden dem Paket hinzugef�gt. Soll der Vorgang fortgesetzt werden?;ENU=The Default Dimension and Dimension Value tables must be included in the package %1 to enable this option. The missing tables will be added to the package. Do you want to continue?';
      Text004@1008 : TextConst 'DEU=Sie k�nnen die Konfigurationspakettabelle nicht umbenennen.;ENU=You cannot rename the configuration package table.';
      Text005@1009 : TextConst 'DEU=Die Einrichtung von ''Dimensionen als Spalten'' wurde storniert.;ENU=The setup of Dimensions as Columns was canceled.';
      Text010@1003 : TextConst 'DEU=Definieren Sie die Drilldownseite im Feld %1.;ENU=Define the drill-down page in the %1 field.';
      ConfigValidateMgt@1000 : Codeunit 8617;
      ConfigMgt@1002 : Codeunit 8616;
      ConfigPackageMgt@1004 : Codeunit 8611;
      HideValidationDialog@1010 : Boolean;
      i@1007 : Integer;
      CannotBeEqualToErr@1011 : TextConst '@@@=%1 - name of the field "Table ID";DEU=darf nicht mit %1 identisch sein.;ENU=cannot be equal to %1.';
      CannotBeSetDueToRelatedTablesErr@1012 : TextConst '@@@=%1 - an integer value;DEU=muss 0 entsprechen, da zugeh�rige Tabellen mit der �bergeordneten Tabellen-ID %1 vorhanden sind.;ENU=must be 0 because there are related tables, where Parent Table ID is %1.';

    LOCAL PROCEDURE InitPackageFields@1() : Boolean;
    VAR
      ConfigPackageField@1001 : Record 8616;
      ConfigLine@1004 : Record 8622;
      Field@1000 : Record 2000000041;
      ProcessingOrder@1002 : Integer;
      FieldsAdded@1003 : Boolean;
    BEGIN
      FieldsAdded := FALSE;
      ConfigPackageMgt.SetFieldFilter(Field,"Table ID",0);
      IF Field.FINDSET THEN
        REPEAT
          IF NOT ConfigPackageField.GET("Package Code","Table ID",Field."No.") AND
             NOT ConfigPackageMgt.IsDimSetIDField("Table ID",Field."No.")
          THEN BEGIN
            ConfigPackageMgt.InsertPackageField(
              ConfigPackageField,"Package Code","Table ID",Field."No.",Field.FieldName,Field."Field Caption",
              TRUE,TRUE,FALSE,FALSE);
            ConfigPackageField.SETRANGE("Package Code","Package Code");
            ConfigPackageField.SETRANGE("Table ID","Table ID");
            ConfigPackageField.SETRANGE("Field ID",Field."No.");
            ConfigPackageMgt.SelectAllPackageFields(ConfigPackageField,TRUE);
            FieldsAdded := TRUE;
          END;
        UNTIL Field.NEXT = 0;

      IF FieldsAdded THEN BEGIN
        ProcessingOrder := 0;
        SetProcessingOrderPrimaryKey("Package Code","Table ID",ProcessingOrder);
        ConfigPackageField.RESET;
        ConfigPackageField.SETRANGE("Package Code","Package Code");
        ConfigPackageField.SETRANGE("Table ID","Table ID");
        ConfigPackageField.SETRANGE("Primary Key",FALSE);
        IF "Table ID" <> DATABASE::"Config. Line" THEN
          SetProcessingOrderFields(ConfigPackageField,ProcessingOrder)
        ELSE BEGIN
          ConfigPackageField.SETRANGE("Field ID",ConfigLine.FIELDNO("Line Type"),ConfigLine.FIELDNO("Table ID"));
          SetProcessingOrderFields(ConfigPackageField,ProcessingOrder);
          // package code must be processed just after table ID!
          ConfigPackageField.SETRANGE("Field ID",ConfigLine.FIELDNO("Package Code"));
          SetProcessingOrderFields(ConfigPackageField,ProcessingOrder);
          ConfigPackageField.SETRANGE("Field ID",ConfigLine.FIELDNO(Name),ConfigLine.FIELDNO("Package Code") - 1);
          SetProcessingOrderFields(ConfigPackageField,ProcessingOrder);
          ConfigPackageField.SETFILTER("Field ID",'%1..',ConfigLine.FIELDNO("Package Code") + 1);
          SetProcessingOrderFields(ConfigPackageField,ProcessingOrder);
        END;
      END;

      EXIT(FieldsAdded);
    END;

    LOCAL PROCEDURE SetProcessingOrderPrimaryKey@17(PackageCode@1006 : Code[20];TableID@1007 : Integer;VAR ProcessingOrder@1000 : Integer);
    VAR
      ConfigPackageField@1001 : Record 8616;
      RecRef@1004 : RecordRef;
      KeyRef@1003 : KeyRef;
      FieldRef@1002 : FieldRef;
      KeyFieldCount@1005 : Integer;
    BEGIN
      RecRef.OPEN(TableID);
      KeyRef := RecRef.KEYINDEX(1);
      FOR KeyFieldCount := 1 TO KeyRef.FIELDCOUNT DO BEGIN
        FieldRef := KeyRef.FIELDINDEX(KeyFieldCount);
        ConfigPackageField.GET(PackageCode,TableID,FieldRef.NUMBER);
        ProcessingOrder += 1;
        ConfigPackageField."Processing Order" := ProcessingOrder;
        ConfigPackageField.MODIFY;
      END;
    END;

    LOCAL PROCEDURE SetProcessingOrderFields@9(VAR ConfigPackageField@1000 : Record 8616;VAR ProcessingOrder@1002 : Integer);
    BEGIN
      IF ConfigPackageField.FINDSET THEN
        REPEAT
          ProcessingOrder += 1;
          ConfigPackageField."Processing Order" := ProcessingOrder;
          ConfigPackageField.MODIFY;
        UNTIL ConfigPackageField.NEXT = 0;
    END;

    PROCEDURE InitDimensionFields@3();
    VAR
      Dimension@1002 : Record 348;
      ConfigPackageField@1000 : Record 8616;
      ConfigPackageTable@1001 : Record 8613;
      Confirmed@1004 : Boolean;
    BEGIN
      IF NOT (ConfigMgt.IsDimSetIDTable("Table ID") OR ConfigMgt.IsDefaultDimTable("Table ID")) THEN
        ERROR(Text002,"Table ID");

      IF ConfigMgt.IsDefaultDimTable("Table ID") THEN BEGIN
        Confirmed :=
          (ConfigPackageTable.GET("Package Code",DATABASE::"Dimension Value") AND
           ConfigPackageTable.GET("Package Code",DATABASE::"Default Dimension")) OR
          (HideValidationDialog OR NOT GUIALLOWED);
        IF NOT Confirmed THEN
          Confirmed := CONFIRM(Text003,TRUE,"Package Code");
        IF Confirmed THEN BEGIN
          ConfigPackageMgt.InsertPackageTable(ConfigPackageTable,"Package Code",DATABASE::"Dimension Value");
          ConfigPackageMgt.InsertPackageTable(ConfigPackageTable,"Package Code",DATABASE::"Default Dimension");
        END ELSE
          ERROR(Text005);
      END;

      i := 0;
      IF Dimension.FINDSET THEN
        REPEAT
          i := i + 1;
          ConfigPackageMgt.InsertPackageField(
            ConfigPackageField,"Package Code","Table ID",ConfigMgt.DimensionFieldID + i,
            Dimension.Code,Dimension."Code Caption",TRUE,FALSE,FALSE,TRUE);
        UNTIL Dimension.NEXT = 0;
    END;

    PROCEDURE DeletePackageData@8();
    VAR
      ConfigPackageRecord@1001 : Record 8614;
    BEGIN
      ConfigPackageRecord.SETRANGE("Package Code","Package Code");
      ConfigPackageRecord.SETRANGE("Table ID","Table ID");
      ConfigPackageRecord.DELETEALL(TRUE);
    END;

    LOCAL PROCEDURE DeleteDimensionFields@7();
    VAR
      ConfigPackageField@1000 : Record 8616;
      ConfigPackageData@1002 : Record 8615;
    BEGIN
      ConfigPackageData.SETRANGE("Package Code","Package Code");
      ConfigPackageData.SETRANGE("Table ID","Table ID");
      ConfigPackageData.SETRANGE("Field ID",ConfigMgt.DimensionFieldID,ConfigMgt.DimensionFieldID + 999);
      ConfigPackageData.DELETEALL;

      ConfigPackageField.SETRANGE("Package Code","Package Code");
      ConfigPackageField.SETRANGE("Table ID","Table ID");
      ConfigPackageField.SETRANGE(Dimension,TRUE);
      ConfigPackageField.DELETEALL;
    END;

    PROCEDURE DimensionFieldsCount@2() : Integer;
    VAR
      ConfigPackageField@1000 : Record 8616;
    BEGIN
      ConfigPackageField.SETRANGE("Package Code","Package Code");
      ConfigPackageField.SETRANGE("Table ID","Table ID");
      ConfigPackageField.SETRANGE(Dimension,TRUE);
      EXIT(ConfigPackageField.COUNT);
    END;

    PROCEDURE DimensionPackageDataExist@6() : Boolean;
    VAR
      ConfigPackageData@1000 : Record 8615;
    BEGIN
      ConfigPackageData.SETRANGE("Package Code","Package Code");
      ConfigPackageData.SETRANGE("Table ID","Table ID");
      ConfigPackageData.SETRANGE("Field ID",ConfigMgt.DimensionFieldID,ConfigMgt.DimensionFieldID + 999);
      EXIT(NOT ConfigPackageData.ISEMPTY);
    END;

    PROCEDURE ShowPackageRecords@5(Show@1004 : 'Records,Errors,All';ShowDim@1003 : Boolean);
    VAR
      ConfigPackageField@1000 : Record 8616;
      ConfigPackageRecord@1001 : Record 8614;
      ConfigPackageRecords@1002 : Page 8626;
      MatrixColumnCaptions@1005 : ARRAY [1000] OF Text[100];
    BEGIN
      ConfigPackageField.SETRANGE("Package Code","Package Code");
      ConfigPackageField.SETRANGE("Table ID","Table ID");
      ConfigPackageField.SETRANGE("Include Field",TRUE);
      IF NOT ShowDim THEN
        ConfigPackageField.SETRANGE(Dimension,FALSE);
      i := 1;
      CLEAR(MatrixColumnCaptions);
      IF ConfigPackageField.FINDSET THEN
        REPEAT
          MatrixColumnCaptions[i] := ConfigPackageField."Field Name";
          i := i + 1;
        UNTIL ConfigPackageField.NEXT = 0;

      CALCFIELDS("Table Caption");
      CLEAR(ConfigPackageRecords);
      ConfigPackageRecord.SETRANGE("Package Code","Package Code");
      ConfigPackageRecord.SETRANGE("Table ID","Table ID");
      CASE Show OF
        Show::Records:
          ConfigPackageRecord.SETRANGE(Invalid,FALSE);
        Show::Errors:
          ConfigPackageRecord.SETRANGE(Invalid,TRUE);
      END;
      ConfigPackageRecords.SETTABLEVIEW(ConfigPackageRecord);
      ConfigPackageRecords.LOOKUPMODE(TRUE);
      ConfigPackageRecords.Load(MatrixColumnCaptions,"Table Caption","Package Code","Table ID",ShowDim);
      ConfigPackageRecords.RUNMODAL;
    END;

    PROCEDURE ShowDatabaseRecords@4();
    VAR
      ConfigLine@1000 : Record 8622;
    BEGIN
      IF "Page ID" <> 0 THEN
        PAGE.RUN("Page ID")
      ELSE BEGIN
        ConfigLine.SETRANGE("Package Code","Package Code");
        ConfigLine.SETRANGE("Table ID","Table ID");
        IF ConfigLine.FINDFIRST AND (ConfigLine."Page ID" > 0) THEN
          PAGE.RUN(ConfigLine."Page ID")
        ELSE
          ERROR(Text010,FIELDCAPTION("Page ID"));
      END;
    END;

    PROCEDURE ShowPackageFields@11();
    BEGIN
      ShowFilteredPackageFields('');
    END;

    PROCEDURE ShowFilteredPackageFields@19(FilterValue@1002 : Text);
    VAR
      ConfigPackageField@1000 : Record 8616;
      ConfigPackageFields@1001 : Page 8624;
    BEGIN
      IF InitPackageFields THEN
        COMMIT;

      IF "Dimensions as Columns" THEN
        IF NOT DimensionPackageDataExist THEN BEGIN
          IF DimensionFieldsCount > 0 THEN
            DeleteDimensionFields;
          InitDimensionFields;
          COMMIT;
        END;

      ConfigPackageField.FILTERGROUP(2);
      ConfigPackageField.SETRANGE("Package Code","Package Code");
      ConfigPackageField.SETRANGE("Table ID","Table ID");
      IF FilterValue <> '' THEN
        ConfigPackageField.SETFILTER("Field ID",FilterValue);
      ConfigPackageField.FILTERGROUP(0);
      ConfigPackageFields.SETTABLEVIEW(ConfigPackageField);
      ConfigPackageFields.RUNMODAL;
      CLEAR(ConfigPackageFields);
    END;

    PROCEDURE ShowPackageCard@14(PackageCode@1002 : Code[20]);
    VAR
      ConfigPackage@1000 : Record 8623;
      ConfigPackageCard@1001 : Page 8614;
    BEGIN
      ConfigPackage.FILTERGROUP(2);
      ConfigPackage.SETRANGE(Code,PackageCode);
      ConfigPackage.FILTERGROUP(0);
      ConfigPackageCard.SETTABLEVIEW(ConfigPackage);
      ConfigPackageCard.RUNMODAL;
      CLEAR(ConfigPackageCard);
    END;

    PROCEDURE SetFieldStyle@12(FieldNumber@1000 : Integer) : Text;
    BEGIN
      CASE FieldNumber OF
        FIELDNO("No. of Package Records"):
          BEGIN
            CALCFIELDS("No. of Package Records");
            IF "No. of Package Records" > 0 THEN
              EXIT('Strong');
          END;
        FIELDNO("No. of Package Errors"):
          BEGIN
            CALCFIELDS("No. of Package Errors");
            IF "No. of Package Errors" > 0 THEN
              EXIT('Unfavorable');
          END;
      END;

      EXIT('');
    END;

    PROCEDURE ShowFilters@10();
    VAR
      ConfigPackageFilter@1001 : Record 8626;
      ConfigPackageFilters@1000 : Page 8623;
    BEGIN
      ConfigPackageFilter.FILTERGROUP(2);
      ConfigPackageFilter.SETRANGE("Package Code","Package Code");
      ConfigPackageFilter.SETRANGE("Table ID","Table ID");
      ConfigPackageFilter.SETRANGE("Processing Rule No.",0);
      ConfigPackageFilter.FILTERGROUP(0);
      ConfigPackageFilters.SETTABLEVIEW(ConfigPackageFilter);
      ConfigPackageFilters.RUNMODAL;
      CLEAR(ConfigPackageFilters);
    END;

    PROCEDURE ShowProcessingRules@18();
    VAR
      ConfigTableProcessingRule@1001 : Record 8631;
      ConfigTableProcessingRules@1000 : Page 8640;
    BEGIN
      ConfigTableProcessingRule.FILTERGROUP(2);
      ConfigTableProcessingRule.SETRANGE("Package Code","Package Code");
      ConfigTableProcessingRule.SETRANGE("Table ID","Table ID");
      ConfigTableProcessingRule.FILTERGROUP(0);
      ConfigTableProcessingRules.SETTABLEVIEW(ConfigTableProcessingRule);
      ConfigTableProcessingRules.RUNMODAL;
      CLEAR(ConfigTableProcessingRules);
    END;

    LOCAL PROCEDURE UpdateDimensionsPackageData@13();
    VAR
      ConfigPackageField@1000 : Record 8616;
      ConfigPackageRecord@1001 : Record 8614;
      ConfigPackageData@1002 : Record 8615;
    BEGIN
      ConfigPackageRecord.SETRANGE("Package Code","Package Code");
      ConfigPackageRecord.SETRANGE("Table ID","Table ID");
      IF ConfigPackageRecord.FINDSET THEN
        REPEAT
          ConfigPackageField.SETRANGE("Package Code","Package Code");
          ConfigPackageField.SETRANGE("Table ID","Table ID");
          ConfigPackageField.SETRANGE(Dimension,TRUE);
          IF ConfigPackageField.FINDSET THEN
            REPEAT
              ConfigPackageMgt.InsertPackageData(
                ConfigPackageData,"Package Code","Table ID",ConfigPackageRecord."No.",
                ConfigPackageField."Field ID",'',ConfigPackageRecord.Invalid);
            UNTIL ConfigPackageField.NEXT = 0;
        UNTIL ConfigPackageRecord.NEXT = 0;
    END;

    PROCEDURE SetHideValidationDialog@16(NewHideValidationDialog@1000 : Boolean);
    BEGIN
      HideValidationDialog := NewHideValidationDialog;
    END;

    PROCEDURE GetNoOfDatabaseRecords@15() : Integer;
    VAR
      ConfigXMLExchange@1002 : Codeunit 8614;
      RecRef@1000 : RecordRef;
    BEGIN
      IF "Table ID" = 0 THEN
        EXIT(0);

      IF NOT ConfigXMLExchange.TableObjectExists("Table ID") THEN
        EXIT(0);

      RecRef.OPEN("Table ID",FALSE,"Company Filter (Source Table)");
      EXIT(RecRef.COUNT);
    END;

    BEGIN
    END.
  }
}

