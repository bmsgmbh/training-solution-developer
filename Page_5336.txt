OBJECT Page 5336 CRM Coupling Record
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=CRM-Kopplungsdatensatz;
               ENU=CRM Coupling Record];
    SourceTable=Table5332;
    PageType=StandardDialog;
    SourceTableTemporary=Yes;
    OnAfterGetRecord=BEGIN
                       RefreshFields
                     END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 11  ;1   ;Group     ;
                GroupType=Group }

    { 2   ;2   ;Group     ;
                CaptionML=[DEU=Kopplung;
                           ENU=Coupling];
                GroupType=GridLayout;
                Layout=Columns }

    { 3   ;3   ;Group     ;
                CaptionML=[DEU=Dynamics NAV;
                           ENU=Dynamics NAV];
                GroupType=Group }

    { 4   ;4   ;Field     ;
                Name=NAVName;
                CaptionML=[DEU=Dynamics NAV-Name;
                           ENU=Dynamics NAV Name];
                ToolTipML=[DEU=Gibt den Namen des Datensatzes in Dynamics NAV zur Kopplung mit einem vorhandenen Dynamics CRM-Datensatz an.;
                           ENU=Specifies the name of the record in Dynamics NAV to couple to an existing Dynamics CRM record.];
                ApplicationArea=#Suite;
                SourceExpr="NAV Name";
                Editable=FALSE;
                ShowCaption=No }

    { 13  ;4   ;Group     ;
                GroupType=Group }

    { 5   ;5   ;Field     ;
                Name=SyncActionControl;
                CaptionML=[DEU=Nach Kopplung synchronisieren;
                           ENU=Synchronize After Coupling];
                ToolTipML=[DEU=Gibt an, ob Daten im Datensatz in Dynamics NAV und im Datensatz in Dynamics CRM synchronisiert werden.;
                           ENU=Specifies whether to synchronize the data in the record in Dynamics NAV and the record in Dynamics CRM.];
                OptionCaptionML=[DEU=Nein,Ja - Dynamics NAV-Daten verwenden,Ja - Dynamics CRM-Daten verwenden;
                                 ENU=No,Yes - Use the Dynamics NAV data,Yes - Use the Dynamics CRM data];
                ApplicationArea=#Suite;
                SourceExpr="Sync Action";
                Enabled=NOT "Create New" }

    { 6   ;3   ;Group     ;
                CaptionML=[DEU=Dynamics CRM;
                           ENU=Dynamics CRM];
                GroupType=Group }

    { 7   ;4   ;Field     ;
                Name=CRMName;
                CaptionML=[DEU=Dynamics CRM-Name;
                           ENU=Dynamics CRM Name];
                ToolTipML=[DEU=Gibt den Namen des Datensatzes in Dynamics CRM an, der mit dem Datensatz in Dynamics NAV gekoppelt wird.;
                           ENU=Specifies the name of the record in Dynamics CRM that is coupled to the record in Dynamics NAV.];
                ApplicationArea=#Suite;
                SourceExpr="CRM Name";
                Enabled=NOT "Create New";
                OnValidate=BEGIN
                             RefreshFields
                           END;

                ShowCaption=No }

    { 15  ;4   ;Group     ;
                GroupType=Group }

    { 8   ;5   ;Field     ;
                Name=CreateNewControl;
                CaptionML=[DEU=Neu erstellen;
                           ENU=Create New];
                ToolTipML=[DEU=Gibt an, ob ein neuer Datensatz in Dynamics CRM automatisch erstellt und mit dem dazugeh�rigen Datensatz in Dynamics NAV gekoppelt wird.;
                           ENU=Specifies if a new record in Dynamics CRM is automatically created and coupled to the related record in Dynamics NAV.];
                ApplicationArea=#Suite;
                SourceExpr="Create New" }

    { 12  ;1   ;Part      ;
                Name=CoupledFields;
                CaptionML=[DEU=Felder;
                           ENU=Fields];
                ApplicationArea=#Suite;
                PagePartID=Page5337;
                PartType=Page;
                ShowFilter=No }

  }
  CODE
  {

    PROCEDURE GetCRMId@1() : GUID;
    BEGIN
      EXIT("CRM ID");
    END;

    PROCEDURE GetPerformInitialSynchronization@2() : Boolean;
    BEGIN
      EXIT(Rec.GetPerformInitialSynchronization);
    END;

    PROCEDURE GetInitialSynchronizationDirection@3() : Integer;
    BEGIN
      EXIT(Rec.GetInitialSynchronizationDirection);
    END;

    LOCAL PROCEDURE RefreshFields@5();
    BEGIN
      CurrPage.CoupledFields.PAGE.SetSourceRecord(Rec);
    END;

    PROCEDURE SetSourceRecordID@4(RecordID@1001 : RecordID);
    BEGIN
      Initialize(RecordID);
      INSERT;
    END;

    BEGIN
    END.
  }
}

