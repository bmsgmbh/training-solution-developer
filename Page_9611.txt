OBJECT Page 9611 XML Schema Restrictions Part
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Einschr�nkungsteil XML-Schema;
               ENU=XML Schema Restrictions Part];
    SourceTable=Table9611;
    PageType=ListPart;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Wert des importierten Datensatzes an.;
                           ENU=Specifies the value of the imported record.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Value }

  }
  CODE
  {

    BEGIN
    END.
  }
}

