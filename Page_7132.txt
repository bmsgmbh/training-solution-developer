OBJECT Page 7132 Item Budget Names
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Artikelbudgetnamen;
               ENU=Item Budget Names];
    SourceTable=Table7132;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Artikelbudgets an.;
                           ENU=Specifies the name of the item budget.];
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Artikelbudgets an.;
                           ENU=Specifies a description of the item budget.];
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob das Artikelbudget gesperrt ist.;
                           ENU=Specifies whether the item budget is blocked.];
                SourceExpr=Blocked }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Dimensionscode f�r Artikelbudgetdimension 1 an.;
                           ENU=Specifies a dimension code for Item Budget Dimension 1.];
                SourceExpr="Budget Dimension 1 Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Dimensionscode f�r Artikelbudgetdimension 2 an.;
                           ENU=Specifies a dimension code for Item Budget Dimension 2.];
                SourceExpr="Budget Dimension 2 Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Dimensionscode f�r Artikelbudgetdimension 3 an.;
                           ENU=Specifies a dimension code for Item Budget Dimension 3.];
                SourceExpr="Budget Dimension 3 Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

