OBJECT Page 1293 Pmt. Rec. Journals Overview
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Nicht verarbeitete Zahlungen;
               ENU=Unprocessed Payments];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    LinksAllowed=No;
    SourceTable=Table273;
    SourceTableView=WHERE(Statement Type=CONST(Payment Application));
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 6       ;1   ;Action    ;
                      CaptionML=[DEU=Bankkontokarte;
                                 ENU=Bank Account Card];
                      ToolTipML=[DEU=Zeigen Sie Informationen �ber das mit dem Zahlungsabstimmungsbuch.-Blatt verkn�pfte Bankkonto an, oder bearbeiten Sie sie.;
                                 ENU=View or edit information about the bank account that is related to the payment reconciliation journal.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 1283;
                      RunPageLink=No.=FIELD(Bank Account No.);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=BankAccount;
                      PromotedCategory=Process }
      { 12      ;1   ;Action    ;
                      Name=ViewJournal;
                      ShortCutKey=Return;
                      CaptionML=[DEU=Buch.-Blatt anzeigen;
                                 ENU=View Journal];
                      ToolTipML=[DEU=Zeigen Sie die Zahlungsabstimmungszeilen aus dem Bankauszug f�r das Konto an. Diese Informationen k�nnen beim Buchen der von der Bank erfassten Transaktionen n�tzlich sein, die noch nicht erfasst wurden.;
                                 ENU=View the payment reconciliation lines from the bank statement for the account. This information can help when posting the transactions recorded by the bank that have not yet been recorded.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=OpenWorksheet;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 BankAccReconciliation@1001 : Record 273;
                               BEGIN
                                 IF NOT BankAccReconciliation.GET("Statement Type","Bank Account No.","Statement No.") THEN
                                   EXIT;

                                 OpenList(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Nummer des Bankkontos fest, das mit dem Bankkontoauszug abgestimmt werden soll.;
                           ENU=Specifies the number of the bank account that you want to reconcile with the bank's statement.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No." }

    { 5   ;2   ;Field     ;
                CaptionML=[DEU=Auszugleichender Restbetrag;
                           ENU=Remaining Amount to Apply];
                ToolTipML=[DEU=Gibt die Summe der Werte im Feld "Differenz" in allen Zeilen im Fenster "Bankkontoabstimmung" an, die zur Bankkontoabstimmung geh�ren.;
                           ENU=Specifies the sum of values in the Difference field on all lines in the Bank Acc. Reconciliation window that belong to the bank account reconciliation.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Total Difference" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

