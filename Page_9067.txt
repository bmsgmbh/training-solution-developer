OBJECT Page 9067 Resource Manager Activities
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Aktivit�ten;
               ENU=Activities];
    SourceTable=Table9057;
    PageType=CardPart;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;

                 SETRANGE("Date Filter",WORKDATE,WORKDATE);
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 4   ;1   ;Group     ;
                CaptionML=[DEU=Verteilung;
                           ENU=Allocation];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 9       ;0   ;Action    ;
                                  CaptionML=[DEU=Ressourcenkapazit�t;
                                             ENU=Resource Capacity];
                                  ToolTipML=[DEU=Zeigt die Kapazit�t der Ressourcen an.;
                                             ENU=View the capacity of the resource.];
                                  ApplicationArea=#Jobs;
                                  RunObject=Page 213;
                                  Image=Capacity }
                  { 10      ;0   ;Action    ;
                                  CaptionML=[DEU=Ressourcengr.-Kapazit�t;
                                             ENU=Resource Group Capacity];
                                  ToolTipML=[DEU=Zeigt die Kapazit�t von Ressourcengruppen an.;
                                             ENU=View the capacity of resource groups.];
                                  ApplicationArea=#Jobs;
                                  RunObject=Page 214 }
                }
                 }

    { 1   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl verf�gbarer Ressourcen an, die im Rollencenter im Projektstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of available resources that are displayed in the Job Cue on the Role Center. The documents are filtered by today's date.];
                ApplicationArea=#Jobs;
                SourceExpr="Available Resources";
                DrillDownPageID=Resource List }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Projekte ohne zugewiesene Ressource an, die im Rollencenter im Projektstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of jobs without an assigned resource that are displayed in the Job Cue on the Role Center. The documents are filtered by today's date.];
                ApplicationArea=#Jobs;
                SourceExpr="Jobs w/o Resource";
                DrillDownPageID=Job List }

    { 7   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl nicht zugewiesener Ressourcengruppen an, die im Rollencenter im Projektstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of unassigned resource groups that are displayed in the Job Cue on the Role Center. The documents are filtered by today's date.];
                ApplicationArea=#Jobs;
                SourceExpr="Unassigned Resource Groups";
                DrillDownPageID=Resource Groups }

  }
  CODE
  {

    BEGIN
    END.
  }
}

