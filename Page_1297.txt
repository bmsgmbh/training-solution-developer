OBJECT Page 1297 Transfer Difference to Account
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Differenz auf Konto buchen;
               ENU=Transfer Difference to Account];
    SourceTable=Table81;
    DataCaptionExpr='';
    PageType=StandardDialog;
    OnOpenPage=BEGIN
                 DescriptionTxt := Description;
                 CurrPage.EDITABLE := TRUE;
               END;

    OnQueryClosePage=BEGIN
                       IF CloseAction = ACTION::LookupOK THEN
                         VALIDATE(Description,DescriptionTxt)
                     END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=General;
                CaptionML=[DEU=Allgemein;
                           ENU=General];
                GroupType=Group }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtbetrag (inkl. MwSt.) f�r die Buch.-Blattzeile an.;
                           ENU=Specifies the total amount (including VAT) that the journal line consists of.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount;
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Kontoart an, auf die der Posten der Buch.-Blattzeile gebucht wird.;
                           ENU=Specifies the type of account that the entry on the journal line will be posted to.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Account Type";
                ValuesAllowed=[G/L Account;Customer;Vendor;Bank Account] }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Kontonummer an, auf die der Posten der Buch.-Blattzeile gebucht wird.;
                           ENU=Specifies the account number that the entry on the journal line will be posted to.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Account No." }

    { 7   ;2   ;Field     ;
                CaptionML=[DEU=Description;
                           ENU=Description];
                ToolTipML=[DEU=Gibt einen beschreibenden Text f�r diese Direktzahlungsbuchung an. Standardm��ig wird der Text im Feld "Transaktionstext" eingef�gt.;
                           ENU=Specifies text that describes this direct payment posting. By default, the text in the Transaction Text field is inserted.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DescriptionTxt }

  }
  CODE
  {
    VAR
      DescriptionTxt@1000 : Text[50];

    BEGIN
    END.
  }
}

