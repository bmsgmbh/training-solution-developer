OBJECT Page 5804 Applied Item Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Ausgeglichene Artikelposten;
               ENU=Applied Item Entries];
    SourceTable=Table32;
    PageType=List;
    OnAfterGetRecord=BEGIN
                       GetApplQty;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 60      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Posten;
                                 ENU=Ent&ry];
                      Image=Entry }
      { 61      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Zeigt Dimensionen an oder bearbeitet sie, wie etwa einen Bereich, ein Projekt oder eine Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 64      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=&Wertposten;
                                 ENU=&Value Entries];
                      ToolTipML=[DEU=Zeigt Betr�ge an, die sich auf einen Artikel beziehen. Wenn Sie �nderungen an einem Wert f�r Artikel im Bestand vornehmen, z. B. einen Auftrag buchen, wird mindestens ein Wertposten hinzugef�gt.;
                                 ENU=View amounts that relate to an item. Whenever you do something that changes a value for items in the inventory, like post an order, one or more value entries are added.];
                      RunObject=Page 5802;
                      RunPageView=SORTING(Item Ledger Entry No.);
                      RunPageLink=Item Ledger Entry No.=FIELD(Entry No.);
                      Image=ValueLedger }
      { 30      ;1   ;ActionGroup;
                      CaptionML=[DEU=Au&sgleich;
                                 ENU=&Application];
                      Image=Apply }
      { 58      ;2   ;Action    ;
                      CaptionML=[DEU=&Ausgeglichene Posten;
                                 ENU=Applied E&ntries];
                      ToolTipML=[DEU=Zeigt die Posten an, die auf diesen Datensatz angewendet wurden.;
                                 ENU=View the ledger entries that have been applied to this record.];
                      ApplicationArea=#Basic,#Suite;
                      Image=Approve;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Show Applied Entries",Rec);
                               END;
                                }
      { 56      ;2   ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[DEU=Reservierungsposten;
                                 ENU=Reservation Entries];
                      ToolTipML=[DEU=Zeigt Informationen zu Reservierung und Artikelverfolgung an.;
                                 ENU=View information about reservation and item tracking.];
                      ApplicationArea=#Basic,#Suite;
                      Image=ReservationLedger;
                      OnAction=BEGIN
                                 ShowReservationEntries(TRUE);
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 57      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 59      ;2   ;Action    ;
                      CaptionML=[DEU=&Bedarfsverursacher;
                                 ENU=Order &Tracking];
                      ToolTipML=[DEU=Verfolgt die Verbindung eines Vorrats zum entsprechenden Bedarf. So k�nnen Sie den urspr�nglichen Bedarf finden, anhand dessen ein spezifischer Fertigungsauftrag oder eine Bestellung erstellt wurde.;
                                 ENU=Tracks the connection of a supply to its corresponding demand. This can help you find the original demand that created a specific production order or purchase order.];
                      ApplicationArea=#Basic,#Suite;
                      Image=OrderTracking;
                      OnAction=VAR
                                 TrackingForm@1001 : Page 99000822;
                               BEGIN
                                 TrackingForm.SetItemLedgEntry(Rec);
                                 TrackingForm.RUNMODAL;
                               END;
                                }
      { 32      ;1   ;Action    ;
                      CaptionML=[DEU=&Navigate;
                                 ENU=&Navigate];
                      ToolTipML=[DEU=Sucht alle Posten und Belege, die f�r die Belegnummer und das Buchungsdatum auf dem ausgew�hlten Posten oder Beleg vorhanden sind.;
                                 ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate.SetDoc("Posting Date","Document No.");
                                 Navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Buchungsdatum des Postens an.;
                           ENU=Specifies the entry's posting date.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, aus welcher Art Transaktion der Posten erstellt wurde.;
                           ENU=Specifies which type of transaction that the entry is created from.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry Type" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Belegnummer des Postens an. Der Beleg ist das Dokument, das die Basis des Postens darstellte, z.�B. ein Lieferschein.;
                           ENU=Specifies the document number on the entry. The document is the voucher that the entry was based on, for example, a receipt.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels im Posten an.;
                           ENU=Specifies the number of the item in the entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Item No." }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Variantencode f�r die Artikel an.;
                           ENU=Specifies the variant code for the items.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Postens an.;
                           ENU=Specifies a description of the entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, mit dem der Posten verkn�pft ist.;
                           ENU=Specifies the dimension value code that the entry is linked to.];
                SourceExpr="Global Dimension 1 Code";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, mit dem der Posten verkn�pft ist.;
                           ENU=Specifies the dimension value code that the entry is linked to.];
                SourceExpr="Global Dimension 2 Code";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, mit dem der Posten verkn�pft ist.;
                           ENU=Specifies the code for the location that the entry is linked to.];
                SourceExpr="Location Code" }

    { 20  ;2   ;Field     ;
                CaptionML=[DEU=Ausgeglichene Menge;
                           ENU=Applied Quantity];
                ToolTipML=[DEU=Gibt an, wie viele Einheiten des Artikels ausgeglichen wurden.;
                           ENU=Specifies how many units of the item that have been applied.];
                ApplicationArea=#Basic,#Suite;
                DecimalPlaces=0:5;
                SourceExpr=ApplQty }

    { 12  ;2   ;Field     ;
                CaptionML=[DEU=Menge;
                           ENU=Quantity];
                ToolTipML=[DEU=Gibt die Gesamtmenge der ausgeglichenen Artikel an.;
                           ENU=Specifies the total quantity of items that have been applied.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Qty }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viele Einheiten des Artikels in der Zeile fakturiert wurden.;
                           ENU=Specifies how many units of the item on the line have been invoiced.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Invoiced Quantity";
                Visible=TRUE }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die im Lager verbleibende Menge im Feld "Menge" an, wenn der Posten einen Zugang darstellt (einen Einkauf oder Zugang).;
                           ENU=Specifies the quantity that remains in inventory in the Quantity field if the entry is an increase (a purchase or positive adjustment).];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Remaining Quantity";
                Visible=TRUE }

    { 117 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viele Einheiten des Artikels in der Zeile reserviert wurden.;
                           ENU=Specifies how many units of the item on the line have been reserved.];
                SourceExpr="Reserved Quantity";
                Visible=FALSE;
                OnDrillDown=BEGIN
                              ShowReservationEntries(TRUE);
                            END;
                             }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob der Posten vollst�ndig ausgeglichen wurde.;
                           ENU=Specifies whether the entry has been fully applied to.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Open }

    { 52  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge pro Artikeleinheit an.;
                           ENU=Specifies the quantity per item unit of measure.];
                SourceExpr="Qty. per Unit of Measure";
                Visible=FALSE }

    { 80  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Artikel in der Zeile direkt an den Debitor geliefert wurden.;
                           ENU=Specifies whether the items on the line have been shipped directly to the customer.];
                SourceExpr="Drop Shipment";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob es einen oder mehrere ausgeglichene Posten gibt, die reguliert werden m�ssen.;
                           ENU=Specifies whether there is one or more applied entries, which need to be adjusted.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Applied Entry to Adjust" }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, in welcher Auftragsart der Posten erstellt wurde.;
                           ENU=Specifies which type of order that the entry was created in.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Order Type" }

    { 54  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Auftrags an, der den Posten erstellt hat.;
                           ENU=Specifies the number of the order that created the entry.];
                SourceExpr="Order No.";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postennummer f�r den Posten an.;
                           ENU=Specifies the entry number for the entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Navigate@1000 : Page 344;
      ApplQty@1001 : Decimal;
      Qty@1002 : Decimal;

    LOCAL PROCEDURE GetApplQty@1();
    VAR
      ItemLedgEntry@1000 : Record 32;
    BEGIN
      ItemLedgEntry.GET("Entry No.");
      ApplQty := Quantity;
      Qty := ItemLedgEntry.Quantity;
    END;

    BEGIN
    END.
  }
}

