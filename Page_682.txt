OBJECT Page 682 Schedule a Report
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Bericht planen;
               ENU=Schedule a Report];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table472;
    DataCaptionExpr=Description;
    PageType=StandardDialog;
    SourceTableTemporary=Yes;
    OnOpenPage=BEGIN
                 IF NOT FINDFIRST THEN BEGIN
                   INIT;
                   ReportEditable := TRUE;
                   OutPutEditable := TRUE;
                   Status := Status::"On Hold";
                   VALIDATE("Object Type to Run","Object Type to Run"::Report);
                   INSERT(TRUE);
                 END ELSE
                   OutPutEditable := REPORT.DEFAULTLAYOUT("Object ID to Run") <> DEFAULTLAYOUT::None; // Processing Only
               END;

    OnQueryClosePage=VAR
                       JobQueueEntry@1000 : Record 472;
                     BEGIN
                       IF CloseAction <> ACTION::OK THEN
                         EXIT(TRUE);

                       IF "Object ID to Run" = 0 THEN BEGIN
                         MESSAGE(NoIdMsg);
                         EXIT(FALSE);
                       END;

                       CALCFIELDS(XML);
                       JobQueueEntry := Rec;
                       JobQueueEntry."Run in User Session" := TRUE;
                       IF JobQueueEntry.Description = '' THEN
                         JobQueueEntry.Description := COPYSTR("Object Caption to Run",1,MAXSTRLEN(JobQueueEntry.Description));
                       CODEUNIT.RUN(CODEUNIT::"Job Queue - Enqueue",JobQueueEntry);
                       IF JobQueueEntry.IsToReportInbox THEN
                         MESSAGE(ReportScheduledMsg);
                       EXIT(TRUE);
                     END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 3   ;1   ;Field     ;
                CaptionML=[DEU=Berichts-ID;
                           ENU=Report ID];
                ToolTipML=[DEU=Gibt die ID des f�r den Aufgabenwarteschlangenposten auszuf�hrenden Objekts an. Sie k�nnen im Feld "Art des auszuf�hrenden Objekts" eine ID der angegebenen Objektart ausw�hlen.;
                           ENU=Specifies the ID of the object that is to be run for this job. You can select an ID that is of the object type that you have specified in the Object Type to Run field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Object ID to Run";
                Editable=ReportEditable;
                OnValidate=BEGIN
                             IF "Object ID to Run" <> 0 THEN
                               RunReportRequestPage;
                             OutPutEditable := REPORT.DEFAULTLAYOUT("Object ID to Run") <> DEFAULTLAYOUT::None; // Processing Only
                           END;

                OnLookup=VAR
                           NewObjectID@1000 : Integer;
                         BEGIN
                           IF LookupObjectID(NewObjectID) THEN BEGIN
                             Text := FORMAT(NewObjectID);
                             EXIT(TRUE);
                           END;
                           EXIT(FALSE);
                         END;
                          }

    { 4   ;1   ;Field     ;
                CaptionML=[DEU=Berichtsname;
                           ENU=Report Name];
                ToolTipML=[DEU=Gibt den Namen des im Feld "ID des auszuf�hrenden Objekts" ausgew�hlten Objekts an.;
                           ENU=Specifies the name of the object that is selected in the Object ID to Run field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Object Caption to Run";
                Enabled=FALSE }

    { 10  ;1   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Aufgabenwarteschlangenpostens an. Sie k�nnen die Beschreibung auf der Karte f�r den Aufgabenwarteschlangenposten bearbeiten und aktualisieren. Die Beschreibung wird auch im Fenster "Aufgabenwarteschlangenposten" angezeigt, kann dort aber nicht aktualisiert werden. Sie k�nnen bis zu 50�Zeichen (sowohl Ziffern als auch Buchstaben) eingeben.;
                           ENU=Specifies a description of the job queue entry. You can edit and update the description on the job queue entry card. The description is also displayed in the Job Queue Entries window, but it cannot be updated there. You can enter a maximum of 50 characters, both numbers and letters.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 5   ;1   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob Optionen auf der Berichtanforderungsseite f�r den geplanten Berichtsauftrag festgelegt wurden. Wenn das Kontrollk�stchen aktiviert ist, wurden Optionen f�r den geplanten Bericht festgelegt.;
                           ENU=Specifies whether options on the report request page have been set for scheduled report job. If the check box is selected, then options have been set for the scheduled report.];
                ApplicationArea=#All;
                SourceExpr="Report Request Page Options";
                Visible=ReportEditable }

    { 6   ;1   ;Field     ;
                ToolTipML=[DEU=Gibt die Ausgabe des geplanten Berichts an.;
                           ENU=Specifies the output of the scheduled report.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Report Output Type";
                Enabled=OutPutEditable }

    { 7   ;1   ;Field     ;
                ToolTipML=[DEU=Gibt den Drucker an, auf dem der geplante Bericht gedruckt werden soll.;
                           ENU=Specifies the printer to use to print the scheduled report.];
                SourceExpr="Printer Name";
                Importance=Additional;
                Enabled="Report Output Type" = "Report Output Type"::Print }

    { 8   ;1   ;Field     ;
                ToolTipML=[DEU=Gibt das fr�heste Datum und die fr�heste Uhrzeit f�r die Ausf�hrung des Aufgabenwarteschlangenpostens an.;
                           ENU=Specifies the earliest date and time when the job queue entry should be run.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Earliest Start Date/Time";
                Importance=Additional }

    { 9   ;1   ;Field     ;
                ToolTipML=[DEU=Gibt an, wann (Datum und Uhrzeit) der Aufgabenwarteschlangenposten abl�uft. Nach diesem Zeitpunkt wird der Aufgabenwarteschlangenposten nicht mehr ausgef�hrt.;
                           ENU=Specifies the date and time when the job queue entry is to expire, after which the job queue entry will not be run.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Expiration Date/Time";
                Importance=Additional }

  }
  CODE
  {
    VAR
      NoIdMsg@1000 : TextConst 'DEU=Sie m�ssen eine Berichtnummer angeben.;ENU=You must specify a report number.';
      ReportEditable@1001 : Boolean;
      OutPutEditable@1002 : Boolean;
      ReportScheduledMsg@1003 : TextConst 'DEU=Der Bericht wurde geplant. Er erscheint im Teil "Berichtseingang", wenn er fertig ist.;ENU=The report has been scheduled. It will appear in the Report Inbox part when it is completed.';

    PROCEDURE ScheduleAReport@1(ReportId@1000 : Integer;RequestPageXml@1001 : Text) : Boolean;
    VAR
      ScheduleAReport@1002 : Page 682;
    BEGIN
      ScheduleAReport.SetParameters(ReportId,RequestPageXml);
      EXIT(ScheduleAReport.RUNMODAL = ACTION::OK);
    END;

    PROCEDURE SetParameters@2(ReportId@1000 : Integer;RequestPageXml@1001 : Text);
    BEGIN
      INIT;
      Status := Status::"On Hold";
      VALIDATE("Object Type to Run","Object Type to Run"::Report);
      VALIDATE("Object ID to Run",ReportId);
      INSERT(TRUE);
      SetReportParameters(RequestPageXml);
    END;

    BEGIN
    END.
  }
}

