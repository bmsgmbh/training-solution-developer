OBJECT Page 6511 Posted Item Tracking Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Geb. Artikelverfolgungszeilen;
               ENU=Posted Item Tracking Lines];
    SourceTable=Table32;
    PageType=List;
    OnOpenPage=VAR
                 CaptionText1@1000 : Text[100];
                 CaptionText2@1001 : Text[100];
               BEGIN
                 CaptionText1 := "Item No.";
                 IF CaptionText1 <> '' THEN BEGIN
                   CaptionText2 := CurrPage.CAPTION;
                   CurrPage.CAPTION := STRSUBSTNO(Text001,CaptionText1,CaptionText2);
                 END;
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Seriennummer an, wenn es f�r den gebuchten Artikel eine derartige Nummer gibt.;
                           ENU=Specifies a serial number if the posted item carries such a number.];
                SourceExpr="Serial No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Chargennummer an, wenn es f�r den gebuchten Artikel eine derartige Nummer gibt.;
                           ENU=Specifies a lot number if the posted item carries such a number.];
                SourceExpr="Lot No." }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Einheiten des Artikels im Artikelposten an.;
                           ENU=Specifies the number of units of the item in the item entry.];
                SourceExpr=Quantity }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt f�r diesen Artikelposten die Menge an, die geliefert, aber noch nicht zur�ckgesendet wurde.;
                           ENU=Specifies the quantity for this item ledger entry that was shipped and has not yet been returned.];
                SourceExpr="Shipped Qty. Not Returned";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt letzte Garantiedatum f�r den Artikel in der Zeile an.;
                           ENU=Specifies the last day of warranty for the item on the line.];
                SourceExpr="Warranty Date" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das letzte Datum an, an dem der Artikel in der Zeile verwendet werden kann.;
                           ENU=Specifies the last date that the item on the line can be used.];
                SourceExpr="Expiration Date" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text001@1000 : TextConst '@@@={Locked};DEU=%1 - %2;ENU=%1 - %2';

    BEGIN
    END.
  }
}

