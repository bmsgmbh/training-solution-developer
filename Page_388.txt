OBJECT Page 388 Bank Acc. Reconciliation List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Bankkontoabstimmungs�bersicht;
               ENU=Bank Acc. Reconciliation List];
    SourceTable=Table273;
    SourceTableView=WHERE(Statement Type=CONST(Bank Reconciliation));
    PageType=List;
    CardPageID=Bank Acc. Reconciliation;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1102601000;1 ;ActionGroup;
                      CaptionML=[DEU=Bu&chung;
                                 ENU=P&osting];
                      Image=Post }
      { 1102601002;2 ;Action    ;
                      Name=Post;
                      ShortCutKey=F9;
                      CaptionML=[DEU=Bu&chen;
                                 ENU=P&ost];
                      ToolTipML=[DEU=Schlie�t den Beleg oder das Buch.-Blatt durch Buchen der Betr�ge und Mengen auf den entsprechenden Konten in den Firmenb�chern ab.;
                                 ENU=Finalize the document or journal by posting the amounts and quantities to the related accounts in your company books.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 371;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process }
      { 1102601003;2 ;Action    ;
                      Name=PostAndPrint;
                      ShortCutKey=Shift+F9;
                      CaptionML=[DEU=Buchen und d&rucken;
                                 ENU=Post and &Print];
                      ToolTipML=[DEU=Schlie�t den Beleg oder das Buch.-Blatt ab und bereitet das Drucken vor. Die Werte und Mengen werden auf den entsprechenden Konten gebucht. Ein Berichtanforderungsfenster wird ge�ffnet, in dem Sie angeben k�nnen, was auf dem Ausdruck enthalten sein soll.;
                                 ENU=Finalize and prepare to print the document or journal. The values and quantities are posted to the related accounts. A report request window where you can specify what to include on the print-out.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 372;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                Name=BankAccountNo;
                ToolTipML=[DEU=Legt die Nummer des Bankkontos fest, das mit dem Bankkontoauszug abgestimmt werden soll.;
                           ENU=Specifies the number of the bank account that you want to reconcile with the bank's statement.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No." }

    { 4   ;2   ;Field     ;
                Name=StatementNo;
                ToolTipML=[DEU=Gibt die Nummer des Bankkontoauszugs an.;
                           ENU=Specifies the number of the bank account statement.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Statement No." }

    { 6   ;2   ;Field     ;
                Name=StatementDate;
                ToolTipML=[DEU=Gibt das Datum des Bankkontoauszugs an.;
                           ENU=Specifies the date on the bank account statement.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Statement Date" }

    { 8   ;2   ;Field     ;
                Name=BalanceLastStatement;
                ToolTipML=[DEU=Gibt den auf dem letzten Bankkontoauszug angegebenen Schlusssaldo an, der bei der zuletzt gebuchten Bankabstimmung dieses Bankkontos verwendet wurde.;
                           ENU=Specifies the ending balance shown on the last bank statement, which was used in the last posted bank reconciliation for this bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Balance Last Statement" }

    { 10  ;2   ;Field     ;
                Name=StatementEndingBalance;
                ToolTipML=[DEU=Legt den Schlusssaldo des Bankkontoauszugs fest, der mit dem Bankkonto abgestimmt werden soll.;
                           ENU=Specifies the ending balance shown on the bank's statement that you want to reconcile with the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Statement Ending Balance" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

