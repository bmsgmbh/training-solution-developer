OBJECT Page 7508 Select Item Attribute Value
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Artikelattributwert ausw�hlen;
               ENU=Select Item Attribute Value];
    SourceTable=Table7501;
    DataCaptionExpr='';
    PageType=StandardDialog;
    OnQueryClosePage=BEGIN
                       CLEAR(DummySelectedItemAttributeValue);
                       CurrPage.SETSELECTIONFILTER(DummySelectedItemAttributeValue);
                     END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Wert der Option an.;
                           ENU=Specifies the value of the option.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Value }

  }
  CODE
  {
    VAR
      DummySelectedItemAttributeValue@1000 : Record 7501;

    PROCEDURE GetSelectedValue@2(VAR ItemAttributeValue@1000 : Record 7501);
    BEGIN
      ItemAttributeValue.COPY(DummySelectedItemAttributeValue);
    END;

    BEGIN
    END.
  }
}

