OBJECT Page 7396 Posted Invt. Put-away Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Geb. Lagereinlagerungszeilen;
               ENU=Posted Invt. Put-away Lines];
    LinksAllowed=No;
    SourceTable=Table7341;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 49      ;1   ;ActionGroup;
                      CaptionML=[DEU=Zei&le;
                                 ENU=&Line];
                      Image=Line }
      { 50      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Beleg anzeigen;
                                 ENU=Show Document];
                      Image=View;
                      OnAction=BEGIN
                                 PostedInvtPutAway.GET("No.");
                                 PAGE.RUN(PAGE::"Posted Invt. Put-away",PostedInvtPutAway);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des gebuchten Lagereinlagerungskopfs an.;
                           ENU=Specifies the number of the posted inventory put-away header.];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der gebuchten Lagereinlagerungszeile an.;
                           ENU=Specifies the number of the posted inventory put-away line.];
                SourceExpr="Line No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Aktionsart f�r die Lagereinlagerungszeile fest.;
                           ENU=Specifies the action type for the inventory put-away line.];
                SourceExpr="Action Type";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Belegs an, auf den sich die Zeile bezieht (beispielsweise Verkaufsauftrag, Einkaufsbestellung oder Umlagerungsauftrag).;
                           ENU=Specifies the type of document (for example, sales order, purchase order or transfer) to which the line relates.];
                SourceExpr="Source Document" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Herkunftsbelegs an, aus dem die Lagereinlagerungszeile stammt.;
                           ENU=Specifies the number of the source document from which the inventory put-away line originated.];
                SourceExpr="Source No." }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an, der eingelagert wurde.;
                           ENU=Specifies the number of the item that was put away.];
                SourceExpr="Item No." }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Variantencode des Artikels an, der eingelagert wurde.;
                           ENU=Specifies the variant code of the item that was put away.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Artikels an, der eingelagert wurde.;
                           ENU=Specifies a description of the item that was put away.];
                SourceExpr=Description }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Seriennummer des Artikels an, der eingelagert wurde.;
                           ENU=Specifies the serial number for the item that was put away.];
                SourceExpr="Serial No.";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Chargennummer des Artikels an, der eingelagert wurde.;
                           ENU=Specifies the lot number for the item that was put away.];
                SourceExpr="Lot No.";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Ablaufdatum des Artikels an, der eingelagert wurde.;
                           ENU=Specifies the expiration date for the item that was put away.];
                SourceExpr="Expiration Date";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerortcode an, an dem die Artikel eingelagert wurden.;
                           ENU=Specifies the code for the location where the items were put away.];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerplatzes an, an dem der Artikel eingelagert wurde.;
                           ENU=Specifies the code for the bin in which the item was put away.];
                SourceExpr="Bin Code" }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Regalnummer des Artikels zu Informationszwecken an.;
                           ENU=Specifies the shelf number of the item for informational use.];
                SourceExpr="Shelf No.";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels an, die eingelagert wurde.;
                           ENU=Specifies the quantity of the item that was put away.];
                SourceExpr=Quantity }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels, der eingelagert wurde, in der Basiseinheit an.;
                           ENU=Specifies the quantity, in the base unit of measure, of the item that was put away.];
                SourceExpr="Qty. (Base)";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Einlagerung abgeschlossen sein h�tte m�ssen.;
                           ENU=Specifies the date when the put-away should have been completed.];
                SourceExpr="Due Date" }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einheitencode des Artikels an, der eingelagert wurde.;
                           ENU=Specifies the unit of measure code of the item that was put away.];
                SourceExpr="Unit of Measure Code" }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge pro Einheit des Artikels an, der eingelagert wurde.;
                           ENU=Specifies the quantity per unit of measure of the item that was put away.];
                SourceExpr="Qty. per Unit of Measure";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Ziels an, das mit der gebuchten Lagereinlagerungszeile verkn�pft ist.;
                           ENU=Specifies the type of destination associated with the posted inventory put-away line.];
                SourceExpr="Destination Type";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer oder den Code des Debitors oder Kreditors an, mit dem diese Zeile verkn�pft ist.;
                           ENU=Specifies the number or code of the customer or vendor that the line is linked to.];
                SourceExpr="Destination No.";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerhilfsmittelcode des Artikels an, der eingelagert wurde.;
                           ENU=Specifies the special equipment code for the item that was put away.];
                SourceExpr="Special Equipment Code";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      PostedInvtPutAway@1000 : Record 7340;

    BEGIN
    END.
  }
}

