OBJECT Table 2000000162 NAV App Capabilities
{
  OBJECT-PROPERTIES
  {
    Date=01.07.17;
    Time=12:00:00;
    Version List=;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[DEU=NAV-App-Funktionen;
               ENU=NAV App Capabilities];
  }
  FIELDS
  {
    { 1   ;   ;Package ID          ;GUID          ;CaptionML=[DEU=Paket-ID;
                                                              ENU=Package ID] }
    { 2   ;   ;Capability ID       ;Integer       ;CaptionML=[DEU=Funktions-ID;
                                                              ENU=Capability ID] }
  }
  KEYS
  {
    {    ;Package ID,Capability ID                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

