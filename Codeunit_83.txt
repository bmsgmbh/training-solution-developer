OBJECT Codeunit 83 Sales-Quote to Order (Yes/No)
{
  OBJECT-PROPERTIES
  {
    Date=28.04.17;
    Time=12:00:00;
    Version List=NAVW110.00.00.16585;
  }
  PROPERTIES
  {
    TableNo=36;
    OnRun=VAR
            OfficeMgt@1000 : Codeunit 1630;
            SalesOrder@1002 : Page 42;
            OpenPage@1001 : Boolean;
          BEGIN
            TESTFIELD("Document Type","Document Type"::Quote);
            IF GUIALLOWED THEN
              IF NOT CONFIRM(ConfirmConvertToOrderQst,FALSE) THEN
                EXIT;

            IF CheckCustomerCreated(TRUE) THEN
              GET("Document Type"::Quote,"No.")
            ELSE
              EXIT;

            SalesQuoteToOrder.RUN(Rec);
            SalesQuoteToOrder.GetSalesOrderHeader(SalesHeader2);
            COMMIT;

            IF GUIALLOWED THEN
              IF OfficeMgt.AttachAvailable THEN
                OpenPage := TRUE
              ELSE
                OpenPage := CONFIRM(STRSUBSTNO(OpenNewInvoiceQst,SalesHeader2."No."),TRUE);
            IF OpenPage THEN BEGIN
              CLEAR(SalesOrder);
              SalesOrder.CheckNotificationsOnce;
              SalesHeader2.SETRECFILTER;
              SalesOrder.SETTABLEVIEW(SalesHeader2);
              SalesOrder.RUN;
            END;
          END;

  }
  CODE
  {
    VAR
      ConfirmConvertToOrderQst@1000 : TextConst 'DEU=M�chten Sie das Angebot in einen Auftrag �bernehmen?;ENU=Do you want to convert the quote to an order?';
      OpenNewInvoiceQst@1004 : TextConst '@@@="%1 = No. of the new sales order document.";DEU=Das Angebot wurde in den Auftrag %1 umgewandelt. M�chten Sie den neuen Auftrag �ffnen?;ENU=The quote has been converted to order %1. Do you want to open the new order?';
      SalesHeader2@1002 : Record 36;
      SalesQuoteToOrder@1003 : Codeunit 86;

    BEGIN
    END.
  }
}

