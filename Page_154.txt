OBJECT Page 154 G/L Account Balance/Budget
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Sachkontensaldo/Budget;
               ENU=G/L Account Balance/Budget];
    SaveValues=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table15;
    PageType=ListPlus;
    OnOpenPage=BEGIN
                 CODEUNIT.RUN(CODEUNIT::"GLBudget-Open",Rec);
               END;

    OnAfterGetRecord=BEGIN
                       UpdateSubForm;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 2       ;1   ;ActionGroup;
                      CaptionML=[DEU=&Konto;
                                 ENU=A&ccount];
                      Image=ChartOfAccounts }
      { 20      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Karte;
                                 ENU=Card];
                      ToolTipML=[DEU=�ffnet die Sachkontokarte f�r den ausgew�hlten Datensatz.;
                                 ENU=Open the G/L account card for the selected record.];
                      ApplicationArea=#Suite;
                      RunObject=Page 17;
                      RunPageLink=No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Budget Filter=FIELD(Budget Filter),
                                  Business Unit Filter=FIELD(Business Unit Filter);
                      Image=EditLines }
      { 22      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=&Posten;
                                 ENU=Ledger E&ntries];
                      ToolTipML=[DEU=Zeigt die Historie der Transaktionen an, die f�r den ausgew�hlten Datensatz gebucht wurden.;
                                 ENU=View the history of transactions that have been posted for the selected record.];
                      ApplicationArea=#Suite;
                      RunObject=Page 20;
                      RunPageView=SORTING(G/L Account No.);
                      RunPageLink=G/L Account No.=FIELD(No.);
                      Promoted=No;
                      Image=GLRegisters;
                      PromotedCategory=Process }
      { 23      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      ToolTipML=[DEU=Zeigt Bemerkungen an oder f�gt diese hinzu.;
                                 ENU=View or add comments.];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(G/L Account),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 184     ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Anzeigen oder Bearbeiten von Dimensionen, wie etwa eines Bereichs, eines Projekts oder einer Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      ApplicationArea=#Suite;
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(15),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 24      ;2   ;Action    ;
                      CaptionML=[DEU=&Textbausteine;
                                 ENU=E&xtended Texts];
                      ToolTipML=[DEU=Zeigt zus�tzliche Informationen an, die der Beschreibung f�r das aktuelle Konto hinzugef�gt wurden.;
                                 ENU=View additional information that has been added to the description for the current account.];
                      ApplicationArea=#Suite;
                      RunObject=Page 391;
                      RunPageView=SORTING(Table Name,No.,Language Code,All Language Codes,Starting Date,Ending Date);
                      RunPageLink=Table Name=CONST(G/L Account),
                                  No.=FIELD(No.);
                      Image=Text }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 3       ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 28      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Budget kopieren;
                                 ENU=Copy Budget];
                      ToolTipML=[DEU=Erstellen Sie eine Kopie des aktuellen Budgets.;
                                 ENU=Create a copy of the current budget.];
                      ApplicationArea=#Suite;
                      RunObject=Report 96;
                      Image=CopyBudget }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 14  ;1   ;Group     ;
                CaptionML=[DEU=Optionen;
                           ENU=Options] }

    { 19  ;2   ;Field     ;
                CaptionML=[DEU=Ultimoposten;
                           ENU=Closing Entries];
                ToolTipML=[DEU=Legt fest, ob im angezeigten Saldo die Ultimoposten enthalten sein sollen. Wenn Sie Betr�ge f�r GuV Konten aus abgeschlossenen Gesch�ftsjahren einsehen m�chten, m�ssen Sie die Ultimoposten ausschlie�en.;
                           ENU=Specifies whether the balance shown will include closing entries. If you want to see the amounts on income statement accounts in closed years, you must exclude closing entries.];
                OptionCaptionML=[DEU=Einschlie�lich,Ausschlie�lich;
                                 ENU=Include,Exclude];
                ApplicationArea=#Suite;
                SourceExpr=ClosingEntryFilter;
                OnValidate=BEGIN
                             UpdateSubForm;
                           END;
                            }

    { 6   ;2   ;Field     ;
                CaptionML=[DEU=Anzeigen nach;
                           ENU=View by];
                ToolTipML=[DEU=Legt fest, bis zu welchem Zeitraum die Betr�ge angezeigt werden.;
                           ENU=Specifies by which period amounts are displayed.];
                OptionCaptionML=[DEU=Tag,Woche,Monat,Quartal,Jahr,Buchhaltungsperiode;
                                 ENU=Day,Week,Month,Quarter,Year,Accounting Period];
                ApplicationArea=#Suite;
                SourceExpr=PeriodType;
                OnValidate=BEGIN
                             IF PeriodType = PeriodType::"Accounting Period" THEN
                               AccountingPerioPeriodTypeOnVal;
                             IF PeriodType = PeriodType::Year THEN
                               YearPeriodTypeOnValidate;
                             IF PeriodType = PeriodType::Quarter THEN
                               QuarterPeriodTypeOnValidate;
                             IF PeriodType = PeriodType::Month THEN
                               MonthPeriodTypeOnValidate;
                             IF PeriodType = PeriodType::Week THEN
                               WeekPeriodTypeOnValidate;
                             IF PeriodType = PeriodType::Day THEN
                               DayPeriodTypeOnValidate;
                           END;
                            }

    { 1   ;2   ;Field     ;
                CaptionML=[DEU=Anzeigen als;
                           ENU=View as];
                ToolTipML=[DEU=Gibt an, wie Betr�ge angezeigt werden. Bewegung: Die Bewegung im Saldo f�r den ausgew�hlten Zeitraum an. Saldo bis Datum: Der Saldo am letzten Tag im ausgew�hlten Zeitraum.;
                           ENU=Specifies how amounts are displayed. Net Change: The net change in the balance for the selected period. Balance at Date: The balance as of the last day in the selected period.];
                OptionCaptionML=[DEU=Bewegung,Saldo bis Datum;
                                 ENU=Net Change,Balance at Date];
                ApplicationArea=#Suite;
                SourceExpr=AmountType;
                OnValidate=BEGIN
                             IF AmountType = AmountType::"Balance at Date" THEN
                               BalanceatDateAmountTypeOnValid;
                             IF AmountType = AmountType::"Net Change" THEN
                               NetChangeAmountTypeOnValidate;
                           END;
                            }

    { 5   ;1   ;Part      ;
                Name=GLBalanceLines;
                ApplicationArea=#Suite;
                PagePartID=Page350 }

  }
  CODE
  {
    VAR
      PeriodType@1000 : 'Day,Week,Month,Quarter,Year,Accounting Period';
      AmountType@1001 : 'Net Change,Balance at Date';
      ClosingEntryFilter@1002 : 'Include,Exclude';

    LOCAL PROCEDURE UpdateSubForm@1();
    BEGIN
      CurrPage.GLBalanceLines.PAGE.Set(Rec,PeriodType,AmountType,ClosingEntryFilter);
    END;

    LOCAL PROCEDURE DayPeriodTypeOnPush@19008851();
    BEGIN
      UpdateSubForm;
    END;

    LOCAL PROCEDURE WeekPeriodTypeOnPush@19046063();
    BEGIN
      UpdateSubForm;
    END;

    LOCAL PROCEDURE MonthPeriodTypeOnPush@19047374();
    BEGIN
      UpdateSubForm;
    END;

    LOCAL PROCEDURE QuarterPeriodTypeOnPush@19018850();
    BEGIN
      UpdateSubForm;
    END;

    LOCAL PROCEDURE YearPeriodTypeOnPush@19051042();
    BEGIN
      UpdateSubForm;
    END;

    LOCAL PROCEDURE AccountingPerioPeriodTypOnPush@19038761();
    BEGIN
      UpdateSubForm;
    END;

    LOCAL PROCEDURE NetChangeAmountTypeOnPush@19074855();
    BEGIN
      UpdateSubForm;
    END;

    LOCAL PROCEDURE BalanceatDateAmountTypeOnPush@19049003();
    BEGIN
      UpdateSubForm;
    END;

    LOCAL PROCEDURE DayPeriodTypeOnValidate@19012979();
    BEGIN
      DayPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE WeekPeriodTypeOnValidate@19058475();
    BEGIN
      WeekPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE MonthPeriodTypeOnValidate@19021027();
    BEGIN
      MonthPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE QuarterPeriodTypeOnValidate@19015346();
    BEGIN
      QuarterPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE YearPeriodTypeOnValidate@19064743();
    BEGIN
      YearPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE AccountingPerioPeriodTypeOnVal@19058901();
    BEGIN
      AccountingPerioPeriodTypOnPush;
    END;

    LOCAL PROCEDURE NetChangeAmountTypeOnValidate@19062218();
    BEGIN
      NetChangeAmountTypeOnPush;
    END;

    LOCAL PROCEDURE BalanceatDateAmountTypeOnValid@19007073();
    BEGIN
      BalanceatDateAmountTypeOnPush;
    END;

    BEGIN
    END.
  }
}

