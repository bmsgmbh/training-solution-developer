OBJECT Page 1527 Workflow Change List FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Zu genehmigende �nderungen;
               ENU=Changes to Approve];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table1525;
    PageType=ListPart;
    ShowFilter=No;
    OnAfterGetRecord=BEGIN
                       NewValue := GetFormattedNewValue(TRUE);
                       OldValue := GetFormattedOldValue(TRUE);
                     END;

  }
  CONTROLS
  {
    { 5   ;0   ;Container ;
                ContainerType=ContentArea }

    { 4   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                Name=Field;
                ToolTipML=[DEU=Gibt die Caption des Felds an, das ge�ndert wird.;
                           ENU=Specifies the caption of the field that changes.];
                ApplicationArea=#Suite;
                SourceExpr="Field Caption";
                Style=Strong }

    { 1   ;2   ;Field     ;
                CaptionML=[DEU=Neuer Wert;
                           ENU=New Value];
                ToolTipML=[DEU=Gibt den Feldwert nach dem �ndern des Felds an.;
                           ENU=Specifies the field value after the field is changed.];
                ApplicationArea=#Suite;
                SourceExpr=NewValue;
                Style=StrongAccent;
                StyleExpr=TRUE }

    { 2   ;2   ;Field     ;
                CaptionML=[DEU=Alter Wert;
                           ENU=Old Value];
                ToolTipML=[DEU=Gibt den Feldwert vor dem �ndern des Felds an.;
                           ENU=Specifies the field value before the field is changed.];
                ApplicationArea=#Suite;
                SourceExpr=OldValue }

  }
  CODE
  {
    VAR
      NewValue@1000 : Text;
      OldValue@1001 : Text;

    PROCEDURE SetFilterFromApprovalEntry@1(ApprovalEntry@1000 : Record 454) ReturnValue : Boolean;
    BEGIN
      SETRANGE("Record ID",ApprovalEntry."Record ID to Approve");
      SETRANGE("Workflow Step Instance ID",ApprovalEntry."Workflow Step Instance ID");
      ReturnValue := FINDSET;
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

