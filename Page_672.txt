OBJECT Page 672 Job Queue Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Aufgabenwarteschlangenposten;
               ENU=Job Queue Entries];
    SourceTable=Table472;
    SourceTableView=SORTING(Priority,Last Ready State);
    PageType=List;
    CardPageID=Job Queue Entry Card;
    OnAfterGetRecord=VAR
                       User@1000 : Record 2000000120;
                     BEGIN
                       UserDoesNotExist := FALSE;
                       IF "User ID" = USERID THEN
                         EXIT;
                       IF User.ISEMPTY THEN
                         EXIT;
                       User.SETRANGE("User Name","User ID");
                       UserDoesNotExist := User.ISEMPTY;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;  ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 10      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Aufgabenwarteschlange;
                                 ENU=Job &Queue];
                      Image=CheckList }
      { 45      ;2   ;Action    ;
                      Name=ResetStatus;
                      CaptionML=[DEU=Status auf 'Bereit' festlegen;
                                 ENU=Set Status to Ready];
                      ToolTipML=[DEU=�ndert den Status des ausgew�hlten Postens.;
                                 ENU=Change the status of the selected entry.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=ResetStatus;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SetStatus(Status::Ready);
                               END;
                                }
      { 46      ;2   ;Action    ;
                      Name=Suspend;
                      CaptionML=[DEU=Auf 'Abwarten' setzen;
                                 ENU=Set On Hold];
                      ToolTipML=[DEU=�ndert den Status des ausgew�hlten Postens.;
                                 ENU=Change the status of the selected entry.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Pause;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SetStatus(Status::"On Hold");
                               END;
                                }
      { 5       ;2   ;Action    ;
                      Name=ShowError;
                      CaptionML=[DEU=Fehler anzeigen;
                                 ENU=Show Error];
                      ToolTipML=[DEU=Zeigt die Fehlermeldung an, durch die der Posten beendet wurde.;
                                 ENU=Show the error message that has stopped the entry.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Error;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowErrorMessage;
                               END;
                                }
      { 7       ;2   ;Action    ;
                      Name=Restart;
                      CaptionML=[DEU=Neu starten;
                                 ENU=Restart];
                      ToolTipML=[DEU=Beendet und startet den ausgew�hlten Posten.;
                                 ENU=Stop and start the selected entry.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Start;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Restart;
                               END;
                                }
      { 17      ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Aufgabenwarteschlange;
                                 ENU=Job &Queue];
                      Image=CheckList }
      { 19      ;2   ;Action    ;
                      Name=LogEntries;
                      CaptionML=[DEU=Protokolleintr�ge;
                                 ENU=Log Entries];
                      ToolTipML=[DEU=Zeigt die Aufgabenwarteschlangen-Protokolleintr�ge an.;
                                 ENU=View the job queue log entries.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 674;
                      RunPageLink=ID=FIELD(ID);
                      Promoted=Yes;
                      Image=Log;
                      PromotedCategory=Process }
      { 21      ;2   ;Action    ;
                      Name=ShowRecord;
                      CaptionML=[DEU=Datensatz anzeigen;
                                 ENU=Show Record];
                      ToolTipML=[DEU=Zeigt den Datensatz f�r den ausgew�hlten Posten an.;
                                 ENU=Show the record for the selected entry.];
                      ApplicationArea=#Basic,#Suite;
                      Image=ViewDetails;
                      OnAction=BEGIN
                                 LookupRecordToProcess;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                Editable=FALSE;
                GroupType=Repeater }

    { 43  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Status des Aufgabenwarteschlangenpostens an. Wenn Sie einen Aufgabenwarteschlangenposten erstellen, wird der Status auf "Abwarten" gesetzt. Sie k�nnen den Status in "Bereit" und wieder in "Abwarten" �ndern. Andernfalls werden die Statusinformationen in diesem Feld automatisch aktualisiert.;
                           ENU=Specifies the status of the job queue entry. When you create a job queue entry, its status is set to On Hold. You can set the status to Ready and back to On Hold. Otherwise, status information in this field is updated automatically.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Status }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Benutzer-ID des Benutzers an, der den Aufgabenwarteschlangenposten in die Warteschlange eingef�gt hat.;
                           ENU=Specifies the user ID of the user who has inserted the job queue entry in the queue.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User ID";
                Style=Unfavorable;
                StyleExpr=UserDoesNotExist }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des bzw. der f�r den Aufgabenwarteschlangenposten auszuf�hrenden Objekts, Berichts oder Codeunit an. Nachdem Sie eine Art angegeben haben, w�hlen Sie eine Objekt-ID dieser Art im Feld "ID des auszuf�hrenden Objekts" aus.;
                           ENU=Specifies the type of the object, report or codeunit, that is to be run for the job queue entry. After you specify a type, you then select an object ID of that type in the Object ID to Run field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Object Type to Run" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des f�r den Aufgabenwarteschlangenposten auszuf�hrenden Objekts an. Sie k�nnen im Feld "Art des auszuf�hrenden Objekts" eine ID der angegebenen Objektart ausw�hlen.;
                           ENU=Specifies the ID of the object that is to be run for this job. You can select an ID that is of the object type that you have specified in the Object Type to Run field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Object ID to Run" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des im Feld "ID des auszuf�hrenden Objekts" ausgew�hlten Objekts an.;
                           ENU=Specifies the name of the object that is selected in the Object ID to Run field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Object Caption to Run" }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Aufgabenwarteschlangenpostens an. Sie k�nnen die Beschreibung auf der Karte f�r den Aufgabenwarteschlangenposten bearbeiten und aktualisieren. Die Beschreibung wird auch im Fenster "Aufgabenwarteschlangenposten" angezeigt, kann dort aber nicht aktualisiert werden. Sie k�nnen bis zu 50�Zeichen (sowohl Ziffern als auch Buchstaben) eingeben.;
                           ENU=Specifies a description of the job queue entry. You can edit and update the description on the job queue entry card. The description is also displayed in the Job Queue Entries window, but it cannot be updated there. You can enter a maximum of 50 characters, both numbers and letters.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Aufgabenwarteschlangenkategorie an, zu der der Aufgabenwarteschlangenposten geh�rt. W�hlen Sie das Feld aus, um einen Code in der Liste auszuw�hlen.;
                           ENU=Specifies the code of the job queue category to which the job queue entry belongs. Choose the field to select a code from the list.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Job Queue Category Code" }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum und die Uhrzeit des Starts einer Benutzersitzung an.;
                           ENU=Specifies the date and time that a user session started.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User Session Started" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Textzeichenfolge an, die bei der Ausf�hrung von der Aufgabenwarteschlange als Parameter verwendet wird.;
                           ENU=Specifies a text string that is used as a parameter by the job queue when it is run.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Parameter String";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das fr�heste Datum und die fr�heste Uhrzeit f�r die Ausf�hrung des Aufgabenwarteschlangenpostens an.;
                           ENU=Specifies the earliest date and time when the job queue entry should be run.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Earliest Start Date/Time" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die zugewiesene Priorit�t eines Aufgabenwarteschlangenpostens an. Anhand der Priorit�t k�nnen Sie die Reihenfolge feststellen, in der Aufgabenwarteschlangenposten ausgef�hrt werden.;
                           ENU=Specifies the assigned priority of a job queue entry. You can use priority to determine the order in which job queue entries are run.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Scheduled;
                Style=Unfavorable;
                StyleExpr=NOT Scheduled }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob der Aufgabenwarteschlangenposten wiederkehrend ist. Wenn das Kontrollk�stchen "Wiederkehrende Aufgabe" aktiviert ist, handelt es sich um einen wiederkehrenden Aufgabenwarteschlangenposten. Ist das Kontrollk�stchen deaktiviert, ist der Aufgabenwarteschlangenposten nicht wiederkehrend. Nachdem Sie angegeben haben, dass ein Aufgabenwarteschlangenposten wiederkehrend ist, m�ssen Sie die Wochentage festlegen, an denen er ausgef�hrt werden soll. Optional k�nnen Sie auch eine Uhrzeit f�r die Ausf�hrung der Aufgabe und die Anzahl von Minuten zwischen Ausf�hrungen angeben.;
                           ENU=Specifies if the job queue entry is recurring. If the Recurring Job check box is selected, then the job queue entry is recurring. If the check box is cleared, the job queue entry is not recurring. After you specify that a job queue entry is a recurring one, you must specify on which days of the week the job queue entry is to run. Optionally, you can also specify a time of day for the job to run and how many minutes should elapse between runs.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Recurring Job" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Mindestanzahl von Minuten an, die zwischen Ausf�hrungen eines Aufgabenwarteschlangenposten verstreichen m�ssen. Dieses Feld ist nur relevant, wenn der Aufgabenwarteschlangenpostens als wiederkehrende Aufgabe festgelegt ist.;
                           ENU=Specifies the minimum number of minutes that are to elapse between runs of a job queue entry. This field only has meaning if the job queue entry is set to be a recurring job.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Minutes between Runs" }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Aufgabenwarteschlangenposten an Montagen ausgef�hrt wird.;
                           ENU=Specifies that the job queue entry runs on Mondays.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Run on Mondays";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Aufgabenwarteschlangenposten an Dienstagen ausgef�hrt wird.;
                           ENU=Specifies that the job queue entry runs on Tuesdays.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Run on Tuesdays";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Aufgabenwarteschlangenposten mittwochs ausgef�hrt wird.;
                           ENU=Specifies that the job queue entry runs on Wednesdays.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Run on Wednesdays";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Aufgabenwarteschlangenposten an Donnerstagen ausgef�hrt wird.;
                           ENU=Specifies that the job queue entry runs on Thursdays.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Run on Thursdays";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Aufgabenwarteschlangenposten an Freitagen ausgef�hrt wird.;
                           ENU=Specifies that the job queue entry runs on Fridays.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Run on Fridays";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Aufgabenwarteschlangenposten an Samstagen ausgef�hrt wird.;
                           ENU=Specifies that the job queue entry runs on Saturdays.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Run on Saturdays";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Aufgabenwarteschlangenposten an Sonntagen ausgef�hrt wird.;
                           ENU=Specifies that the job queue entry runs on Sundays.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Run on Sundays";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die fr�heste Uhrzeit f�r die Ausf�hrung des wiederkehrenden Aufgabenwarteschlangenpostens an.;
                           ENU=Specifies the earliest time of the day that the recurring job queue entry is to be run.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Starting Time";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die sp�teste Uhrzeit f�r die Ausf�hrung des wiederkehrenden Aufgabenwarteschlangenpostens an.;
                           ENU=Specifies the latest time of the day that the recurring job queue entry is to be run.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Ending Time";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      UserDoesNotExist@1000 : Boolean;

    BEGIN
    END.
  }
}

