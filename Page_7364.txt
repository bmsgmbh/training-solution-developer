OBJECT Page 7364 Registered Whse. Act.-Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Reg. Lageraktivit�tszeilen;
               ENU=Registered Whse. Act.-Lines];
    SourceTable=Table5773;
    PageType=List;
    OnAfterGetCurrRecord=BEGIN
                           CurrPage.CAPTION := FormCaption;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 77      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 24      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Registrierten Beleg anzeigen;
                                 ENU=Show Registered Document];
                      Image=ViewRegisteredOrder;
                      OnAction=BEGIN
                                 ShowRegisteredActivityDoc;
                               END;
                                }
      { 78      ;2   ;Action    ;
                      CaptionML=[DEU=&Logistikbeleg anzeigen;
                                 ENU=Show &Whse. Document];
                      Image=ViewOrder;
                      OnAction=BEGIN
                                 ShowWhseDoc;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 58  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Aktion an, die f�r die Artikel dieser Zeile ausgef�hrt werden muss.;
                           ENU=Specifies the action you must perform for the items on the line.];
                SourceExpr="Action Type";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Aktivit�t an, die das Lager in der Zeile ausgef�hrt hat, wie etwa Einlagerung, Kommissionierung oder Umlagerung.;
                           ENU=Specifies the type of activity that the warehouse performed on the line, such as put-away, pick, or movement.];
                SourceExpr="Activity Type";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des registrierten Lageraktivit�tskopfs an.;
                           ENU=Specifies the number of the registered warehouse activity header.];
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der registrierten Lageraktivit�tszeile an.;
                           ENU=Specifies the number of the registered warehouse activity line.];
                SourceExpr="Line No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Transaktion an, der der Herkunftsbeleg zugeordnet ist, wie etwa Einkauf, Verkauf oder Fertigung.;
                           ENU=Specifies the type of transaction the source document is associated with, such as sales, purchase, and production.];
                SourceExpr="Source Type";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Herkunftsunterart des Belegs an, der sich auf die registrierte Aktivit�tszeile bezieht.;
                           ENU=Specifies the source subtype of the document related to the registered activity line.];
                SourceExpr="Source Subtype";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Quellbelegs an, von der die Aktivit�tszeile stammte.;
                           ENU=Specifies the number of the source document from which the activity line originated.];
                SourceExpr="Source No." }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Herkunftsbelegs an, auf den sich die Aktivit�tszeile bezieht.;
                           ENU=Specifies the number of the source document that relates to this activity line.];
                SourceExpr="Source Line No." }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Unterzeile des Herkunftsbelegs an, auf den sich die Aktivit�tszeile bezieht.;
                           ENU=Specifies the number of the source document subline related to this activity line.];
                SourceExpr="Source Subline No.";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Belegs an, wie etwa Verkaufsauftrag, auf den sich die Zeile bezieht.;
                           ENU=Specifies the type of document, such as sales order, to which the line relates.];
                SourceExpr="Source Document" }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerortcode an, an dem die Aktivit�t erfolgt.;
                           ENU=Specifies the code for the location at which the activity occurs.];
                SourceExpr="Location Code" }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Zone an, in der sich der Lagerplatz dieser Zeile befindet.;
                           ENU=Specifies the code of the zone in which the bin on this line is located.];
                SourceExpr="Zone Code";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerplatzes in der registrierten Lageraktivit�tszeile an.;
                           ENU=Specifies the code of the bin on the registered warehouse activity line.];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Regalnummer des Artikels in der Zeile zu Informationszwecken an.;
                           ENU=Specifies the shelf number of the item on the line for information use.];
                SourceExpr="Shelf No." }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Artikelnummer des Artikels an, der bearbeitet (z. B. kommissioniert oder eingelagert) werden soll.;
                           ENU=Specifies the item number of the item to be handled, such as picked or put away.];
                SourceExpr="Item No." }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Variantencode des zu bearbeitenden Artikels an.;
                           ENU=Specifies the variant code of the item to be handled.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einheitencode des Artikels in der Zeile an.;
                           ENU=Specifies the unit of measure code of the item on the line.];
                SourceExpr="Unit of Measure Code" }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels in der Zeile pro Ma�einheit an.;
                           ENU=Specifies the quantity per unit of measure of the item on the line.];
                SourceExpr="Qty. per Unit of Measure" }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Artikels in der Zeile an.;
                           ENU=Specifies a description of the item on the line.];
                SourceExpr=Description }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Artikels in der Zeile an.;
                           ENU=Specifies a description of the item on the line.];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels an, die eingelagert, kommissioniert oder zwischen Lagerpl�tzen umgelagert wurde.;
                           ENU=Specifies the quantity of the item that was put-away, picked or moved.];
                SourceExpr=Quantity }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels an, die eingelagert, kommissioniert oder zwischen Lagerpl�tzen umgelagert wurde.;
                           ENU=Specifies the quantity of the item that was put-away, picked or moved.];
                SourceExpr="Qty. (Base)";
                Visible=FALSE }

    { 72  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der erforderlichen Ausstattung zur Ausf�hrung der Aktion in dieser Zeile an.;
                           ENU=Specifies the code of the equipment required when you perform the action on the line.];
                SourceExpr="Special Equipment Code";
                Visible=FALSE }

    { 60  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Versandanweisung an, die anzeigt, ob der Empf�nger auch eine Teillieferung annimmt.;
                           ENU=Specifies the shipping advice about whether a partial delivery was acceptable to the order recipient.];
                SourceExpr="Shipping Advice" }

    { 66  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Aktivit�t abgeschlossen sein h�tte m�ssen.;
                           ENU=Specifies the date when the activity should have been completed.];
                SourceExpr="Due Date" }

    { 62  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Belegs an, aus dem die Zeile stammte.;
                           ENU=Specifies the type of document that the line originated from.];
                SourceExpr="Whse. Document Type";
                Visible=FALSE }

    { 64  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Logistikbelegs an, auf dem die Aktion dieser Zeile basiert.;
                           ENU=Specifies the number of the warehouse document that is the basis for the action on the line.];
                SourceExpr="Whse. Document No.";
                Visible=FALSE }

    { 68  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Zeile im Logistikbeleg an, auf dem die Aktion dieser Zeile basiert.;
                           ENU=Specifies the number of the line in the warehouse document that is the basis for the action on the line.];
                SourceExpr="Whse. Document Line No.";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1001 : TextConst 'DEU=Registr. Einlagerungszeilen;ENU=Registered Whse. Put-away Lines';
      Text001@1000 : TextConst 'DEU=Registr. Kommissionierzeilen;ENU=Registered Whse. Pick Lines';
      Text002@1002 : TextConst 'DEU=Reg. Lag.-Pl.-Umlag.-Zeilen;ENU=Registered Whse. Movement Lines';
      Text003@1003 : TextConst 'DEU=Registr. Lageraktivit�tszeilen;ENU=Registered Whse. Activity Lines';

    LOCAL PROCEDURE FormCaption@1() : Text[250];
    BEGIN
      CASE "Activity Type" OF
        "Activity Type"::"Put-away":
          EXIT(Text000);
        "Activity Type"::Pick:
          EXIT(Text001);
        "Activity Type"::Movement:
          EXIT(Text002);
        ELSE
          EXIT(Text003);
      END;
    END;

    BEGIN
    END.
  }
}

