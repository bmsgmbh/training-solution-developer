OBJECT Page 9104 Approval Comments FactBox
{
  OBJECT-PROPERTIES
  {
    Date=23.01.17;
    Time=12:00:00;
    Version List=NAVW110.00.00.15052;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Bemerkungen;
               ENU=Comments];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table455;
    PageType=ListPart;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 21  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Bemerkung an. Sie k�nnen bis zu 250 Zeichen, sowohl Ziffern als auch Buchstaben, eingeben.;
                           ENU=Specifies the comment. You can enter a maximum of 250 characters, both numbers and letters.];
                ApplicationArea=#Suite;
                SourceExpr=Comment }

    { 15  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der diese Genehmigungsbemerkung erstellt hat.;
                           ENU=Specifies the ID of the user who created this approval comment.];
                ApplicationArea=#Suite;
                SourceExpr="User ID" }

    { 17  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum und die Uhrzeit der Erstellung des Kommentars an.;
                           ENU=Specifies the date and time when the comment was made.];
                ApplicationArea=#Suite;
                SourceExpr="Date and Time" }

  }
  CODE
  {

    PROCEDURE SetFilterFromApprovalEntry@1(ApprovalEntry@1000 : Record 454) : Boolean;
    BEGIN
      SETRANGE("Record ID to Approve",ApprovalEntry."Record ID to Approve");
      SETRANGE("Workflow Step Instance ID",ApprovalEntry."Workflow Step Instance ID");
      CurrPage.UPDATE(FALSE);
      EXIT(NOT ISEMPTY);
    END;

    BEGIN
    END.
  }
}

