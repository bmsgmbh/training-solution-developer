OBJECT Table 1523 Workflow Step Argument
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Permissions=TableData 1523=rim;
    DataCaptionFields=General Journal Template Name,General Journal Batch Name,Notification User ID;
    OnInsert=BEGIN
               ID := CREATEGUID;
             END;

    OnModify=BEGIN
               CheckEditingIsAllowed;
             END;

    OnDelete=BEGIN
               CheckEditingIsAllowed;
             END;

    OnRename=BEGIN
               CheckEditingIsAllowed;
             END;

    CaptionML=[DEU=Workflowschritt-Argument;
               ENU=Workflow Step Argument];
    LookupPageID=Page1523;
  }
  FIELDS
  {
    { 1   ;   ;ID                  ;GUID          ;CaptionML=[DEU=ID;
                                                              ENU=ID] }
    { 2   ;   ;Type                ;Option        ;TableRelation="Workflow Step".Type WHERE (Argument=FIELD(ID));
                                                   CaptionML=[DEU=Art;
                                                              ENU=Type];
                                                   OptionCaptionML=[DEU=Ereignis,Reaktion;
                                                                    ENU=Event,Response];
                                                   OptionString=Event,Response }
    { 3   ;   ;General Journal Template Name;Code10;
                                                   TableRelation="Gen. Journal Template".Name;
                                                   CaptionML=[DEU=Fibu Buch.-Blattvorlagenname;
                                                              ENU=General Journal Template Name] }
    { 4   ;   ;General Journal Batch Name;Code10  ;TableRelation="Gen. Journal Batch".Name WHERE (Journal Template Name=FIELD(General Journal Template Name));
                                                   CaptionML=[DEU=Fibu Buch.-Blattname;
                                                              ENU=General Journal Batch Name] }
    { 5   ;   ;Notification User ID;Code50        ;TableRelation="User Setup"."User ID";
                                                   CaptionML=[DEU=Benachrichtigungsbenutzer-ID;
                                                              ENU=Notification User ID] }
    { 6   ;   ;Notification User License Type;Option;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Lookup(User."License Type" WHERE (User Name=FIELD(Notification User ID)));
                                                   CaptionML=[DEU=Lizenztyp des Benachrichtigungsbenutzers;
                                                              ENU=Notification User License Type];
                                                   OptionCaptionML=[DEU=Vollst�ndiger Benutzer,Eingeschr�nkter Benutzer,Einziger Benutzer des Ger�ts,Windows-Gruppe,Externer Benutzer;
                                                                    ENU=Full User,Limited User,Device Only User,Windows Group,External User];
                                                   OptionString=Full User,Limited User,Device Only User,Windows Group,External User }
    { 7   ;   ;Response Function Name;Code128     ;TableRelation="Workflow Response"."Function Name";
                                                   CaptionML=[DEU=Funktionsname Reaktion;
                                                              ENU=Response Function Name] }
    { 9   ;   ;Link Target Page    ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Page));
                                                   CaptionML=[DEU=Link zur Zielseite;
                                                              ENU=Link Target Page] }
    { 10  ;   ;Custom Link         ;Text250       ;OnValidate=VAR
                                                                WebRequestHelper@1000 : Codeunit 1299;
                                                              BEGIN
                                                                WebRequestHelper.IsValidUri("Custom Link");
                                                              END;

                                                   ExtendedDatatype=URL;
                                                   CaptionML=[DEU=Benutzerdefinierter Link;
                                                              ENU=Custom Link] }
    { 11  ;   ;Event Conditions    ;BLOB          ;CaptionML=[DEU=Ereignisbedingungen;
                                                              ENU=Event Conditions] }
    { 12  ;   ;Approver Type       ;Option        ;CaptionML=[DEU=Genehmigertyp;
                                                              ENU=Approver Type];
                                                   OptionCaptionML=[DEU=Verk�ufer/Eink�ufer,Genehmiger,Workflow-Benutzergruppe;
                                                                    ENU=Salesperson/Purchaser,Approver,Workflow User Group];
                                                   OptionString=Salesperson/Purchaser,Approver,Workflow User Group }
    { 13  ;   ;Approver Limit Type ;Option        ;CaptionML=[DEU=Einschr�nkungsart Genehmiger;
                                                              ENU=Approver Limit Type];
                                                   OptionCaptionML=[DEU=Genehmigerkette,Direkter Genehmiger,Erster qualifizierter Genehmiger,Spezifischer Genehmiger;
                                                                    ENU=Approver Chain,Direct Approver,First Qualified Approver,Specific Approver];
                                                   OptionString=Approver Chain,Direct Approver,First Qualified Approver,Specific Approver }
    { 14  ;   ;Workflow User Group Code;Code20    ;TableRelation="Workflow User Group".Code;
                                                   CaptionML=[DEU=Workflow-Benutzergruppencode;
                                                              ENU=Workflow User Group Code] }
    { 15  ;   ;Due Date Formula    ;DateFormula   ;OnValidate=BEGIN
                                                                IF COPYSTR(FORMAT("Due Date Formula"),1,1) = '-' THEN
                                                                  ERROR(STRSUBSTNO(NoNegValuesErr,FIELDCAPTION("Due Date Formula")));
                                                              END;

                                                   CaptionML=[DEU=F�lligkeitsdatumsformel;
                                                              ENU=Due Date Formula] }
    { 16  ;   ;Message             ;Text250       ;CaptionML=[DEU=Meldung;
                                                              ENU=Message] }
    { 17  ;   ;Delegate After      ;Option        ;CaptionML=[DEU=Delegieren nach;
                                                              ENU=Delegate After];
                                                   OptionCaptionML=[DEU=Niemals,1 Tag,2 Tage,5 Tage;
                                                                    ENU=Never,1 day,2 days,5 days];
                                                   OptionString=Never,1 day,2 days,5 days }
    { 18  ;   ;Show Confirmation Message;Boolean  ;CaptionML=[DEU=Best�tigungsmeldung anzeigen;
                                                              ENU=Show Confirmation Message] }
    { 19  ;   ;Table No.           ;Integer       ;CaptionML=[DEU=Tabellennr.;
                                                              ENU=Table No.] }
    { 20  ;   ;Field No.           ;Integer       ;CaptionML=[DEU=Feldnr.;
                                                              ENU=Field No.] }
    { 21  ;   ;Field Caption       ;Text80        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Field."Field Caption" WHERE (TableNo=FIELD(Table No.),
                                                                                                   No.=FIELD(Field No.)));
                                                   CaptionML=[DEU=Feldbezeichnung;
                                                              ENU=Field Caption];
                                                   Editable=No }
    { 22  ;   ;Approver User ID    ;Code50        ;TableRelation="User Setup"."User ID";
                                                   OnLookup=VAR
                                                              UserSetup@1000 : Record 91;
                                                            BEGIN
                                                              IF PAGE.RUNMODAL(PAGE::"Approval User Setup",UserSetup) = ACTION::LookupOK THEN
                                                                VALIDATE("Approver User ID",UserSetup."User ID");
                                                            END;

                                                   CaptionML=[DEU=Benutzer-ID des Genehmigers;
                                                              ENU=Approver User ID] }
    { 100 ;   ;Response Option Group;Code20       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Workflow Response"."Response Option Group" WHERE (Function Name=FIELD(Response Function Name)));
                                                   CaptionML=[DEU=Reaktionsoptionsgruppe;
                                                              ENU=Response Option Group];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;ID                                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      NoNegValuesErr@1000 : TextConst 'DEU=%1 muss ein positiver Wert sein.;ENU=%1 must be a positive value.';

    PROCEDURE Clone@1() : GUID;
    VAR
      WorkflowStepArgument@1001 : Record 1523;
    BEGIN
      CALCFIELDS("Event Conditions");
      WorkflowStepArgument.COPY(Rec);
      WorkflowStepArgument.INSERT(TRUE);
      EXIT(WorkflowStepArgument.ID);
    END;

    PROCEDURE Equals@9(WorkflowStepArgument@1000 : Record 1523;SkipBlob@1004 : Boolean) : Boolean;
    VAR
      TypeHelper@1003 : Codeunit 10;
      OtherRecRef@1002 : RecordRef;
      ThisRecRef@1001 : RecordRef;
    BEGIN
      ThisRecRef.GETTABLE(Rec);
      OtherRecRef.GETTABLE(WorkflowStepArgument);

      IF NOT TypeHelper.Equals(ThisRecRef,OtherRecRef,SkipBlob) THEN
        EXIT(FALSE);

      EXIT(TRUE);
    END;

    PROCEDURE GetEventFilters@8() Filters : Text;
    VAR
      FiltersInStream@1002 : InStream;
    BEGIN
      IF "Event Conditions".HASVALUE THEN BEGIN
        CALCFIELDS("Event Conditions");
        "Event Conditions".CREATEINSTREAM(FiltersInStream);
        FiltersInStream.READ(Filters);
      END;
    END;

    PROCEDURE SetEventFilters@2(Filters@1000 : Text);
    VAR
      FiltersOutStream@1001 : OutStream;
    BEGIN
      "Event Conditions".CREATEOUTSTREAM(FiltersOutStream);
      FiltersOutStream.WRITE(Filters);
      MODIFY(TRUE);
    END;

    LOCAL PROCEDURE CheckEditingIsAllowed@12();
    VAR
      Workflow@1000 : Record 1501;
      WorkflowStep@1001 : Record 1502;
    BEGIN
      IF ISNULLGUID(ID) THEN
        EXIT;

      WorkflowStep.SETRANGE(Argument,ID);
      IF WorkflowStep.FINDFIRST THEN BEGIN
        Workflow.GET(WorkflowStep."Workflow Code");
        Workflow.CheckEditingIsAllowed;
      END;
    END;

    PROCEDURE HideExternalUsers@5();
    VAR
      PermissionManager@1001 : Codeunit 9002;
      OriginalFilterGroup@1000 : Integer;
    BEGIN
      IF NOT PermissionManager.SoftwareAsAService THEN
        EXIT;

      OriginalFilterGroup := FILTERGROUP;
      FILTERGROUP := 2;
      CALCFIELDS("Notification User License Type");
      SETFILTER("Notification User License Type",'<>%1',"Notification User License Type"::"External User");
      FILTERGROUP := OriginalFilterGroup;
    END;

    BEGIN
    END.
  }
}

