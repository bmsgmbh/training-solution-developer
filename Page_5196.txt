OBJECT Page 5196 To-do Interaction Languages
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Aufgabensprachen;
               ENU=To-do Interaction Languages];
    SourceTable=Table5196;
    PageType=List;
    OnFindRecord=VAR
                   RecordsFound@1001 : Boolean;
                 BEGIN
                   RecordsFound := FIND(Which);
                   CurrPage.EDITABLE := ("To-do No." <> '');
                   IF Todo.GET("To-do No.") THEN
                     CurrPage.EDITABLE := NOT Todo.Closed;

                   EXIT(RecordsFound);
                 END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 11      ;1   ;ActionGroup;
                      Name=Attachment;
                      CaptionML=[DEU=Da&teianhang;
                                 ENU=&Attachment];
                      Image=Attachments }
      { 13      ;2   ;Action    ;
                      ShortCutKey=Return;
                      CaptionML=[DEU=�ffnen;
                                 ENU=Open];
                      ToolTipML=[DEU=�ffnet den Dateianhang.;
                                 ENU=Open the attachment.];
                      Image=Edit;
                      OnAction=BEGIN
                                 OpenAttachment(("To-do No." = '') OR Todo.Closed);
                               END;
                                }
      { 14      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Erstellen;
                                 ENU=Create];
                      ToolTipML=[DEU=Erstellt einen Dateianhang.;
                                 ENU=Create an attachment.];
                      Image=New;
                      OnAction=BEGIN
                                 CreateAttachment(("To-do No." = '') OR Todo.Closed);
                               END;
                                }
      { 15      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=&Kopieren von;
                                 ENU=Copy &from];
                      ToolTipML=[DEU=Kopiert aus einem Dateianhang.;
                                 ENU=Copy from an attachment.];
                      Image=Copy;
                      OnAction=BEGIN
                                 CopyFromAttachment;
                               END;
                                }
      { 16      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Importieren;
                                 ENU=Import];
                      ToolTipML=[DEU=Importiert einen Dateianhang.;
                                 ENU=Import an attachment.];
                      Image=Import;
                      OnAction=BEGIN
                                 ImportAttachment;
                               END;
                                }
      { 17      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=E&xportieren;
                                 ENU=E&xport];
                      ToolTipML=[DEU=Exportiert einen Dateianhang.;
                                 ENU=Export an attachment.];
                      Image=Export;
                      OnAction=BEGIN
                                 ExportAttachment;
                               END;
                                }
      { 18      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=L�schen;
                                 ENU=Remove];
                      ToolTipML=[DEU=Entfernt einen Dateianhang.;
                                 ENU=Remove an attachment.];
                      Image=Cancel;
                      OnAction=BEGIN
                                 RemoveAttachment(TRUE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Sprachcode f�r die Aktivit�tenvorlage an.;
                           ENU=Specifies the language code for the interaction template.];
                SourceExpr="Language Code" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung der Aktivit�tenvorlage an, die Sie f�r die Aufgabe ausgew�hlt haben.;
                           ENU=Specifies the description of the interaction template that you have chosen for the to-do.];
                SourceExpr=Description }

    { 9   ;2   ;Field     ;
                AssistEdit=Yes;
                CaptionML=[DEU=Dateianhang;
                           ENU=Attachment];
                SourceExpr="Attachment No." > 0;
                OnAssistEdit=BEGIN
                               IF "Attachment No." = 0 THEN
                                 CreateAttachment(("To-do No." = '') OR Todo.Closed)
                               ELSE
                                 OpenAttachment(("To-do No." = '') OR Todo.Closed);
                               CurrPage.UPDATE;
                             END;
                              }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Todo@1000 : Record 5080;

    BEGIN
    END.
  }
}

