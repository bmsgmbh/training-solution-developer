OBJECT Page 5349 CRM Case List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Microsoft Dynamics CRM-Anfragen;
               ENU=Microsoft Dynamics CRM Cases];
    InsertAllowed=No;
    LinksAllowed=No;
    SourceTable=Table5349;
    SourceTableView=SORTING(Title);
    PageType=List;
    PromotedActionCategoriesML=[DEU=Neu,Verarbeiten,Berichte,Dynamics CRM;
                                ENU=New,Process,Reports,Dynamics CRM];
    OnInit=BEGIN
             CODEUNIT.RUN(CODEUNIT::"CRM Integration Management");
           END;

    ActionList=ACTIONS
    {
      { 9       ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 8       ;1   ;ActionGroup;
                      Name=ActionGroupCRM;
                      CaptionML=[DEU=Dynamics CRM;
                                 ENU=Dynamics CRM] }
      { 7       ;2   ;Action    ;
                      Name=CRMGoToCase;
                      CaptionML=[DEU=Anfrage;
                                 ENU=Case];
                      ApplicationArea=#Suite;
                      Promoted=Yes;
                      Image=CoupledOrder;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 CRMIntegrationManagement@1000 : Codeunit 5330;
                               BEGIN
                                 HYPERLINK(CRMIntegrationManagement.GetCRMEntityUrlFromCRMID(DATABASE::"CRM Incident",IncidentId));
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                CaptionML=[DEU=Anfragetitel;
                           ENU=Case Title];
                ApplicationArea=#Suite;
                SourceExpr=Title }

    { 4   ;2   ;Field     ;
                CaptionML=[DEU=Status;
                           ENU=Status];
                OptionCaptionML=[DEU=Aktiv,Gel�st,Storniert;
                                 ENU=Active,Resolved,Canceled];
                ApplicationArea=#Suite;
                SourceExpr=StateCode }

    { 5   ;2   ;Field     ;
                CaptionML=[DEU=Anfragenummer;
                           ENU=Case Number];
                ApplicationArea=#Suite;
                SourceExpr=TicketNumber }

    { 6   ;2   ;Field     ;
                CaptionML=[DEU=Erstellt am;
                           ENU=Created On];
                ToolTipML=[DEU=Gibt an, wann der Verkaufsauftrag erstellt wurde.;
                           ENU=Specifies when the sales order was created.];
                ApplicationArea=#Suite;
                SourceExpr=CreatedOn }

  }
  CODE
  {

    BEGIN
    END.
  }
}

