OBJECT Table 5770 Warehouse Comment Line
{
  OBJECT-PROPERTIES
  {
    Date=07.09.12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Logistikbemerkungszeile;
               ENU=Warehouse Comment Line];
    LookupPageID=Page5777;
    DrillDownPageID=Page5777;
  }
  FIELDS
  {
    { 1   ;   ;Table Name          ;Option        ;CaptionML=[DEU=Tabellenname;
                                                              ENU=Table Name];
                                                   OptionCaptionML=[DEU=Lageraktivit�tskopf,Wareneingang,Warenausgang,Interne Einlag.-Anforderung,Interne Kommiss.-Anforderung,Reg. Lageraktivit�tskopf,Geb. Wareneingang,Geb. Warenausgang,Geb. Lagereinlagerung,Geb. Lagerkommissionierung,Reg. Lagerbestandsumlagerung,Interne Umlagerung;
                                                                    ENU=Whse. Activity Header,Whse. Receipt,Whse. Shipment,Internal Put-away,Internal Pick,Rgstrd. Whse. Activity Header,Posted Whse. Receipt,Posted Whse. Shipment,Posted Invt. Put-Away,Posted Invt. Pick,Registered Invt. Movement,Internal Movement];
                                                   OptionString=Whse. Activity Header,Whse. Receipt,Whse. Shipment,Internal Put-away,Internal Pick,Rgstrd. Whse. Activity Header,Posted Whse. Receipt,Posted Whse. Shipment,Posted Invt. Put-Away,Posted Invt. Pick,Registered Invt. Movement,Internal Movement }
    { 2   ;   ;Type                ;Option        ;CaptionML=[DEU=Art;
                                                              ENU=Type];
                                                   OptionCaptionML=[DEU=" ,Einlagerung,Kommissionierung,Umlagerung,Lagereinlagerung,Lagerkommissionierung,Lagerbestandsumlagerung";
                                                                    ENU=" ,Put-away,Pick,Movement,Invt. Put-away,Invt. Pick,Invt. Movement"];
                                                   OptionString=[ ,Put-away,Pick,Movement,Invt. Put-away,Invt. Pick,Invt. Movement] }
    { 3   ;   ;No.                 ;Code20        ;CaptionML=[DEU=Nr.;
                                                              ENU=No.] }
    { 4   ;   ;Line No.            ;Integer       ;CaptionML=[DEU=Zeilennr.;
                                                              ENU=Line No.] }
    { 5   ;   ;Date                ;Date          ;CaptionML=[DEU=Datum;
                                                              ENU=Date] }
    { 6   ;   ;Code                ;Code10        ;CaptionML=[DEU=Code;
                                                              ENU=Code] }
    { 7   ;   ;Comment             ;Text80        ;CaptionML=[DEU=Bemerkung;
                                                              ENU=Comment] }
  }
  KEYS
  {
    {    ;Table Name,Type,No.,Line No.            ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE SetUpNewLine@1();
    VAR
      WhseCommentLine@1000 : Record 5770;
    BEGIN
      WhseCommentLine.SETRANGE("Table Name","Table Name");
      WhseCommentLine.SETRANGE(Type,Type);
      WhseCommentLine.SETRANGE("No.","No.");
      WhseCommentLine.SETRANGE(Date,WORKDATE);
      IF WhseCommentLine.ISEMPTY THEN
        Date := WORKDATE;
    END;

    PROCEDURE FormCaption@2() : Text[250];
    BEGIN
      IF ("Table Name" = "Table Name"::"Whse. Activity Header") OR
         ("Table Name" = "Table Name"::"Rgstrd. Whse. Activity Header")
      THEN
        EXIT(FORMAT(Type) + ' ' + "No.");

      EXIT(FORMAT("Table Name") + ' ' + "No.");
    END;

    BEGIN
    END.
  }
}

