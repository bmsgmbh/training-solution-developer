OBJECT Page 9047 Machine Operator Activities
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Aktivit�ten;
               ENU=Activities];
    SourceTable=Table9056;
    PageType=CardPart;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;

                 SETFILTER("Date Filter",'<=%1',WORKDATE)
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 4   ;1   ;Group     ;
                CaptionML=[DEU=Fertigungsauftr�ge;
                           ENU=Production Orders];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 10      ;0   ;Action    ;
                                  CaptionML=[DEU=FA-Verb. Buch.-Blatt;
                                             ENU=Consumption Journal];
                                  RunObject=Page 99000846;
                                  Image=ConsumptionJournal }
                  { 11      ;0   ;Action    ;
                                  CaptionML=[DEU=FA-Istmld. Buch.-Bl.;
                                             ENU=Output Journal];
                                  RunObject=Page 99000823;
                                  Image=OutputJournal }
                }
                 }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl freigegebener Fertigungsauftr�ge an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of released production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Released Prod. Orders - All";
                DrillDownPageID=Released Production Orders }

    { 1   ;2   ;Field     ;
                CaptionML=[DEU=Freigegebene FA bis heute;
                           ENU=Released Prod. Orders Until Today];
                ToolTipML=[DEU=Gibt die Anzahl freigegebener Fertigungsauftr�ge an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of released production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Rlsd. Prod. Orders Until Today";
                DrillDownPageID=Released Production Orders }

    { 5   ;1   ;Group     ;
                CaptionML=[DEU=Arbeitsg�nge;
                           ENU=Operations];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 16      ;0   ;Action    ;
                                  CaptionML=[DEU=Fehlzeit erfassen - Arbeitsplatz;
                                             ENU=Register Absence - Machine Center];
                                  RunObject=Report 99003800;
                                  Image=CalendarMachine }
                  { 17      ;0   ;Action    ;
                                  CaptionML=[DEU=Fehlzeit erfassen - Arbeitsplatzgruppe;
                                             ENU=Register Absence - Work Center];
                                  RunObject=Report 99003805;
                                  Image=CalendarWorkcenter }
                  { 25      ;0   ;Action    ;
                                  CaptionML=[DEU=FA-Arbeitsschein;
                                             ENU=Prod. Order - Job Card];
                                  RunObject=Report 99000762;
                                  Image=Report }
                }
                 }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl von FA-Arbeitspl�nen in Warteschlange an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of production order routings in queue that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Prod. Orders Routings-in Queue";
                DrillDownPageID=Prod. Order Routing }

    { 13  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl inaktiver Serviceauftr�ge an, die im Rollencenter im Servicestapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of inactive service orders that are displayed in the Service Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Prod. Orders Routings-in Prog.";
                DrillDownPageID=Prod. Order Routing }

    { 8   ;1   ;Group     ;
                CaptionML=[DEU=Logistikbelege;
                           ENU=Warehouse Documents];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 23      ;0   ;Action    ;
                                  CaptionML=[DEU=Neue Lagerkommissionierung;
                                             ENU=New Inventory Pick];
                                  RunObject=Page 7377;
                                  RunPageMode=Create }
                  { 24      ;0   ;Action    ;
                                  CaptionML=[DEU=Neue Lagereinlagerung;
                                             ENU=New Inventory Put-away];
                                  RunObject=Page 7375;
                                  RunPageMode=Create }
                }
                 }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl von Lagerkommissionierungen an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of inventory picks that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Invt. Picks to Production";
                DrillDownPageID=Inventory Picks }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl von Lagereinlagerungen von Fertigung an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of inventory put-always from production that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Invt. Put-aways from Prod.";
                DrillDownPageID=Inventory Put-aways }

  }
  CODE
  {

    BEGIN
    END.
  }
}

