OBJECT Page 7362 Posted Whse. Shipment Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Geb. Warenausgangszeilen;
               ENU=Posted Whse. Shipment Lines];
    SourceTable=Table7323;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 6       ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 7       ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Geb. Logistikbeleg anzeigen;
                                 ENU=Show Posted Whse. Document];
                      Image=ViewPostedOrder;
                      OnAction=VAR
                                 PostedWhseShptHeader@1000 : Record 7322;
                               BEGIN
                                 PostedWhseShptHeader.GET("No.");
                                 PAGE.RUN(PAGE::"Posted Whse. Shipment",PostedWhseShptHeader);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Belegs an, auf den sich die Zeile bezieht.;
                           ENU=Specifies the type of document to which the line relates.];
                SourceExpr="Source Document";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Herkunftsnummer des Belegs an, aus dem die Zeile stammt.;
                           ENU=Specifies the source number of the document from which the line originates.];
                SourceExpr="Source No." }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Herkunftszeilennummer des Belegs an, aus dem der Posten stammt.;
                           ENU=Specifies the source line number of the document from which the entry originates.];
                SourceExpr="Source Line No." }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Ziels an, das mit der gebuchten Warenausgangszeile verkn�pft ist.;
                           ENU=Specifies the type of destination associated with the posted warehouse shipment line.];
                SourceExpr="Destination Type";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Debitors, Kreditors oder Lagerorts an, an den die Artikel geliefert wurden.;
                           ENU=Specifies the number of the customer, vendor, or location to which the items have been shipped.];
                SourceExpr="Destination No.";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, von dem aus die Artikel in der Zeile geliefert wurden.;
                           ENU=Specifies the code of the location from which the items on the line were shipped.];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code der Zone an, in der sich der Lagerplatz f�r diese gebuchte Lieferzeile befindet.;
                           ENU=Specifies the code of the zone where the bin on this posted shipment line is located.];
                SourceExpr="Zone Code";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerplatzes der gebuchten Warenausgangszeile an.;
                           ENU=Specifies the code of the bin on the posted warehouse shipment line.];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Regalnummer des Artikels zu Informationszwecken an.;
                           ENU=Specifies the shelf number of the item for informational use.];
                SourceExpr="Shelf No.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an, die versendet wurde.;
                           ENU=Specifies the number of the item that has been shipped.];
                SourceExpr="Item No." }

    { 50  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Variantennummer des Artikels in der Zeile an, wenn vorhanden.;
                           ENU=Specifies the variant number of the item on the line, if any.];
                SourceExpr="Variant Code" }

    { 52  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des Artikels in der Zeile an.;
                           ENU=Specifies the description of the item on the line.];
                SourceExpr=Description }

    { 54  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die zweite Beschreibung des Artikels in der Zeile an, falls vorhanden.;
                           ENU=Specifies the a second description of the item on the line, if any.];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge an, die geliefert wurde.;
                           ENU=Specifies the quantity that was shipped.];
                SourceExpr=Quantity }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge im Warenausgang in der Basiseinheit an.;
                           ENU=Specifies the quantity that was shipped, in the base unit of measure.];
                SourceExpr="Qty. (Base)";
                Visible=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ma�einheitencode der Zeile an.;
                           ENU=Specifies the unit of measure code of the line.];
                SourceExpr="Unit of Measure Code" }

    { 48  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Basiseinheiten an, die der Einheit des Artikels in dieser Zeile entsprechen.;
                           ENU=Specifies the number of base units of measure, that are in the unit of measure, specified for the item on the line.];
                SourceExpr="Qty. per Unit of Measure" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Herkunftsbelegs an, der der Zeile zugeordnet ist.;
                           ENU=Specifies the type of source document associated with the line.];
                SourceExpr="Posted Source Document" }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Belegnummer des gebuchten Herkunftsbelegs an.;
                           ENU=Specifies the document number of the posted source document.];
                SourceExpr="Posted Source No." }

    { 58  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das F�lligkeitsdatum der Zeile an.;
                           ENU=Specifies the due date of the line.];
                SourceExpr="Due Date" }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Belegnummer an.;
                           ENU=Specifies the document number.];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Zeile an.;
                           ENU=Specifies the number of the line.];
                SourceExpr="Line No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

