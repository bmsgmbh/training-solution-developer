OBJECT Page 597 XBRL Comment Lines Part
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=XBRL-Bemerkungszeilen (Teil);
               ENU=XBRL Comment Lines Part];
    SourceTable=Table396;
    PageType=ListPart;
  }
  CONTROLS
  {
    { 21  ;0   ;Container ;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Bemerkung an, die die Zeile enth�lt. Info: Eine Bemerkung, die beim Import der Taxonomie aus der Schemadatei importiert wurde. Hinweis: Eine Bemerkung, die mit den anderen finanziellen Informationen exportiert wird. Referenz: Eine Bemerkung, die beim Import der Taxonomie aus der Referenz Linkbase importiert wird.;
                           ENU=Specifies the type of comment that the line contains. Info: a comment imported from the schema file when you imported the taxonomy. Note: A comment that will be exported with the other financial information. Reference: A comment imported from the reference linkbase when you imported the taxonomy.];
                SourceExpr="Comment Type";
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt ein Datum f�r die Bemerkung an. Wenn Sie den Bericht 'XBRL-Instance exp. - Spez. 2' ausf�hren, enth�lt er Bemerkungen innerhalb des Berichtszeitraums sowie Bemerkungen ohne Datum.;
                           ENU=Specifies a date for the comment. When you run the XBRL Export Instance - Spec. 2 report, it includes comments that dates within the period of the report, as well as comments that do not have a date.];
                SourceExpr=Date;
                Visible=false }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Bemerkung an. Wenn die Bemerkungsart Information ist, dann wurde die Bemerkung mit der Taxonomie importiert und kann nicht editiert werden. Wenn die Bemerkungsart Notiz ist, k�nnen Sie maximal 80 Zeichen, sowohl Buchstaben als auch Ziffern, eingeben. Diese wird dann mit dem Rest der Finanzdaten exportiert.;
                           ENU=Specifies the comment. If the comment type is Info, this comment was imported with the taxonomy and cannot be edited. If the comment type is Note, you can enter a maximum of 80 characters for each, both numbers and letters, and it will be exported with the rest of the financial information.];
                SourceExpr=Comment }

  }
  CODE
  {

    BEGIN
    END.
  }
}

