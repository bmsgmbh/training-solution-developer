OBJECT Page 5106 Team Salespeople
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Team Verk�ufer;
               ENU=Team Salespeople];
    SourceTable=Table5084;
    DataCaptionFields=Team Code;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Verk�ufers an, den Sie als Teil des Teams registrieren m�chten.;
                           ENU=Specifies the code of the salesperson you want to register as part of the team.];
                SourceExpr="Salesperson Code" }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[DEU=Gibt den Namen des Verk�ufers an, den Sie als Teil des Teams registrieren m�chten.;
                           ENU=Specifies the name of the salesperson you want to register as part of the team.];
                SourceExpr="Salesperson Name" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

