OBJECT Page 5135 Customer Link
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Debitorenverkn�pfung;
               ENU=Customer Link];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table5054;
    PageType=Card;
    OnQueryClosePage=BEGIN
                       IF ("No." <> '') AND (CloseAction = ACTION::LookupOK) THEN BEGIN
                         ContBusRel := Rec;
                         ContBusRel.INSERT(TRUE);

                         CASE CurrMasterFields OF
                           CurrMasterFields::Contact:
                             BEGIN
                               Cont.GET(ContBusRel."Contact No.");
                               UpdateCustVendBank.UpdateCustomer(Cont,ContBusRel);
                             END;
                           CurrMasterFields::Customer:
                             BEGIN
                               Cust.GET(ContBusRel."No.");
                               UpdateContFromCust.OnModify(Cust);
                             END;
                         END;
                       END;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                CaptionML=[DEU=Debitorennr.;
                           ENU=Customer No.];
                ToolTipML=[DEU=Gibt die Nummer an, die dem Kunden in der Tabelle "Debitor", "Kreditor" oder "Bankkonto" zugeordnet ist. Dieses Feld gilt nur f�r Kontakte, die als Debitor, Kreditor oder Bankkonto erfasst sind.;
                           ENU=Specifies the number assigned to the contact in the Customer, Vendor, or Bank Account table. This field is only valid for contacts recorded as customer, vendor or bank accounts.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No." }

    { 5   ;2   ;Field     ;
                CaptionML=[DEU=Felder �bernehmen von;
                           ENU=Current Master Fields];
                ToolTipML=[DEU=Gibt an, welche Felder zum Zuweisen einer Priorit�t im Konfliktfall zwischen gemeinsamen Feldern der Kontaktkarte und der Bankkontokarte verwendet werden sollen.;
                           ENU=Specifies which fields should be used to prioritize in case there is conflicting information in fields common to the contact card and the bank account card.];
                OptionCaptionML=[DEU=Kontakt,Debitor;
                                 ENU=Contact,Customer];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=CurrMasterFields }

  }
  CODE
  {
    VAR
      ContBusRel@1000 : Record 5054;
      Cont@1001 : Record 5050;
      Cust@1002 : Record 18;
      UpdateCustVendBank@1003 : Codeunit 5055;
      UpdateContFromCust@1004 : Codeunit 5056;
      CurrMasterFields@1005 : 'Contact,Customer';

    BEGIN
    END.
  }
}

