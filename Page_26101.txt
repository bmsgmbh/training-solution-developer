OBJECT Page 26101 Report Selection - VAT
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVDACH10.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Berichtsauswahl - MwSt.;
               ENU=Report Selection - VAT];
    SaveValues=Yes;
    SourceTable=Table26100;
    PageType=Worksheet;
    OnOpenPage=BEGIN
                 SetUsageFilter;
               END;

    OnNewRecord=BEGIN
                  NewRecord;
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 13  ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 11  ;2   ;Field     ;
                CaptionML=[DEU=Verwendung;
                           ENU=Usage];
                OptionCaptionML=[DEU=MwSt.-Abrechnung,USt.-Kontennachweis,MwSt.-Abrechnungsschema;
                                 ENU=VAT Statement,Sales VAT Adv. Not. Acc. Proof,VAT Statement Schedule];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=ReportUsage2;
                OnValidate=BEGIN
                             SetUsageFilter;
                             ReportUsage2OnAfterValidate;
                           END;
                            }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wo in der Druckreihenfolge ein Bericht steht.;
                           ENU=Specifies where a report is in the printing order.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Sequence }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Berichts an, der f�r diese Belegart gedruckt wird.;
                           ENU=Specifies the ID of the report that prints for this document type.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Report ID";
                LookupPageID=Objects }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[DEU=Gibt den Namen des Berichts an, der f�r diese Belegart gedruckt wird.;
                           ENU=Specifies the name of the report that prints for this document type.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Report Name";
                LookupPageID=Objects }

  }
  CODE
  {
    VAR
      ReportUsage2@1000 : 'VAT Statement,Sales VAT Adv. Not. Acc. Proof,VAT Statement Schedule';

    LOCAL PROCEDURE SetUsageFilter@1();
    BEGIN
      FILTERGROUP(2);
      CASE ReportUsage2 OF
        ReportUsage2::"VAT Statement":
          SETRANGE(Usage,Usage::"VAT Statement");
        ReportUsage2::"Sales VAT Adv. Not. Acc. Proof":
          SETRANGE(Usage,Usage::"Sales VAT Acc. Proof");
        ReportUsage2::"VAT Statement Schedule":
          SETRANGE(Usage,Usage::"VAT Statement Schedule");
      END;
      FILTERGROUP(0);
    END;

    LOCAL PROCEDURE ReportUsage2OnAfterValidate@19038799();
    BEGIN
      CurrPage.UPDATE;
    END;

    BEGIN
    END.
  }
}

