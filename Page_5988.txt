OBJECT Page 5988 Service Items
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Serviceartikel;
               ENU=Service Items];
    SourceTable=Table5940;
    DataCaptionExpr=GetCaption;
    PageType=List;
    CardPageID=Service Item Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 8       ;1   ;ActionGroup;
                      CaptionML=[DEU=&Serviceartikel;
                                 ENU=&Serv. Item];
                      Image=ServiceItem }
      { 15      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 5911;
                      RunPageLink=Table Name=CONST(Service Item),
                                  Table Subtype=CONST(0),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 18      ;2   ;Separator  }
      { 19      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=Servicep&osten;
                                 ENU=Service Ledger E&ntries];
                      RunObject=Page 5912;
                      RunPageView=SORTING(Service Item No. (Serviced),Entry Type,Moved from Prepaid Acc.,Type,Posting Date);
                      RunPageLink=Service Item No. (Serviced)=FIELD(No.),
                                  Service Order No.=FIELD(Service Order Filter),
                                  Service Contract No.=FIELD(Contract Filter),
                                  Posting Date=FIELD(Date Filter);
                      Image=ServiceLedger }
      { 20      ;2   ;Action    ;
                      CaptionML=[DEU=Gara&ntieposten;
                                 ENU=&Warranty Ledger Entries];
                      RunObject=Page 5913;
                      RunPageView=SORTING(Service Item No. (Serviced),Posting Date,Document No.);
                      RunPageLink=Service Item No. (Serviced)=FIELD(No.);
                      Image=WarrantyLedger }
      { 21      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[DEU=Statistik;
                                 ENU=Statistics];
                      RunObject=Page 5982;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 24      ;2   ;Action    ;
                      CaptionML=[DEU=&Trendscape;
                                 ENU=&Trendscape];
                      RunObject=Page 5983;
                      RunPageLink=No.=FIELD(No.);
                      Image=Trendscape }
      { 35      ;2   ;Action    ;
                      CaptionML=[DEU=&Protokoll;
                                 ENU=L&og];
                      RunObject=Page 5989;
                      RunPageLink=Service Item No.=FIELD(No.);
                      Image=Approve }
      { 36      ;2   ;Separator  }
      { 37      ;2   ;Action    ;
                      CaptionML=[DEU=Kompon&enten;
                                 ENU=Com&ponents];
                      RunObject=Page 5986;
                      RunPageView=SORTING(Active,Parent Service Item No.,Line No.);
                      RunPageLink=Active=CONST(Yes),
                                  Parent Service Item No.=FIELD(No.);
                      Image=Components }
      { 38      ;2   ;Separator  }
      { 43      ;2   ;ActionGroup;
                      CaptionML=[DEU=&L�sungsanleitung Einrichtung;
                                 ENU=Trou&bleshooting  Setup];
                      Image=Troubleshoot }
      { 44      ;3   ;Action    ;
                      Name=ServiceItemGroup;
                      CaptionML=[DEU=Serviceartikelgruppe;
                                 ENU=Service Item Group];
                      RunObject=Page 5993;
                      RunPageLink=Type=CONST(Service Item Group),
                                  No.=FIELD(Service Item Group Code);
                      Image=ServiceItemGroup }
      { 45      ;3   ;Action    ;
                      Name=ServiceItem;
                      CaptionML=[DEU=Serviceartikel;
                                 ENU=Service Item];
                      RunObject=Page 5993;
                      RunPageLink=Type=CONST(Service Item),
                                  No.=FIELD(No.);
                      Image=Report }
      { 46      ;3   ;Action    ;
                      Name=Item;
                      CaptionML=[DEU=Artikel;
                                 ENU=Item];
                      RunObject=Page 5993;
                      RunPageLink=Type=CONST(Item),
                                  No.=FIELD(Item No.);
                      Image=Item }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                Editable=FALSE;
                GroupType=Repeater }

    { 39  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an.;
                           ENU=Specifies the number of the item.];
                SourceExpr="No." }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung dieses Artikels an.;
                           ENU=Specifies a description of this item.];
                SourceExpr=Description }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die mit dem Serviceartikel verkn�pfte Artikelnummer an.;
                           ENU=Specifies the item number linked to the service item.];
                SourceExpr="Item No." }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Seriennummer dieses Artikels an.;
                           ENU=Specifies the serial number of this item.];
                SourceExpr="Serial No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Debitors an, dem dieser Artikel geh�rt.;
                           ENU=Specifies the number of the customer who owns this item.];
                SourceExpr="Customer No." }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lief. an Code des Debitors an, dem dieser Artikel geh�rt.;
                           ENU=Specifies the ship-to code for the customer who owns this item.];
                SourceExpr="Ship-to Code" }

    { 49  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Startdatum der Ersatzteilgarantie f�r diesen Artikel an.;
                           ENU=Specifies the starting date of the spare parts warranty for this item.];
                SourceExpr="Warranty Starting Date (Parts)" }

    { 51  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Enddatum der Ersatzteilgarantie f�r diesen Artikel an.;
                           ENU=Specifies the ending date of the spare parts warranty for this item.];
                SourceExpr="Warranty Ending Date (Parts)" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Startdatum der Garantie (Arbeit) f�r diesen Artikel an.;
                           ENU=Specifies the starting date of the labor warranty for this item.];
                SourceExpr="Warranty Starting Date (Labor)" }

    { 47  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Enddatum der Garantie (Arbeit) f�r diesen Artikel an.;
                           ENU=Specifies the ending date of the labor warranty for this item.];
                SourceExpr="Warranty Ending Date (Labor)" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst '@@@="%1=Cust.""No.""  %2=Cust.Name";DEU=%1 %2;ENU=%1 %2';
      Text001@1001 : TextConst '@@@="%1 = Item no, %2 = Item description";DEU=%1 %2;ENU=%1 %2';

    LOCAL PROCEDURE GetCaption@1() : Text[80];
    VAR
      Cust@1000 : Record 18;
      Item@1001 : Record 27;
    BEGIN
      CASE TRUE OF
        GETFILTER("Customer No.") <> '':
          BEGIN
            IF Cust.GET(GETRANGEMIN("Customer No.")) THEN
              EXIT(STRSUBSTNO(Text000,Cust."No.",Cust.Name));
            EXIT(STRSUBSTNO('%1 %2',GETRANGEMIN("Customer No.")));
          END;
        GETFILTER("Item No.") <> '':
          BEGIN
            IF Item.GET(GETRANGEMIN("Item No.")) THEN
              EXIT(STRSUBSTNO(Text001,Item."No.",Item.Description));
            EXIT(STRSUBSTNO('%1 %2',GETRANGEMIN("Item No.")));
          END;
        ELSE
          EXIT('');
      END;
    END;

    BEGIN
    END.
  }
}

