OBJECT Page 473 VAT Posting Setup Card
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=MwSt.Buchungsmatrixkarte Einr.;
               ENU=VAT Posting Setup Card];
    SourceTable=Table325;
    DataCaptionFields=VAT Bus. Posting Group,VAT Prod. Posting Group;
    PageType=Card;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 6       ;1   ;Action    ;
                      Name=Copy;
                      Ellipsis=Yes;
                      CaptionML=[DEU=&Kopie;
                                 ENU=&Copy];
                      ToolTipML=[DEU=Kopieren Sie einen Datensatz mit ausgew�hlten oder allen Feldern aus der "MwSt.-Buchungsmatrix Einr." in einen neuen Datensatz. Zuvor m�ssen Sie jedoch den neuen Datensatz erstellen.;
                                 ENU=Copy a record with selected fields or all fields from the Tax posting setup to a new record. Before you start to copy you have to create the new record.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Copy;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CurrPage.SAVERECORD;
                                 CopyVATPostingSetup.SetVATSetup(Rec);
                                 CopyVATPostingSetup.RUNMODAL;
                                 CLEAR(CopyVATPostingSetup);
                                 CurrPage.UPDATE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen MwSt.-Gesch�ftsbuchungsgruppencode an.;
                           ENU=Specifies a VAT business posting group code.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Bus. Posting Group" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen MwSt.-Produktbuchungsgruppencode an.;
                           ENU=Specifies a VAT product posting group code.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Prod. Posting Group" }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Legt fest, wie die Mehrwertsteuer f�r den Einkauf und Verkauf von Artikeln mit dieser Kombination aus MwSt.-Gesch�fts- und MwSt.-Produktbuchungsgruppe berechnet werden soll.;
                           ENU=Specifies how VAT will be calculated for purchases or sales of items with this particular combination of VAT business posting group and VAT product posting group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Calculation Type" }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den relevanten Mehrwertsteuersatz f�r die Kombination aus MwSt.-Gesch�fts- und MwSt.-Produktbuchungsgruppe fest. Geben Sie kein Prozentzeichen, sondern nur den Wert ein. Wenn der Mehrwertsteuerprozentsatz zum Beispiel 25�% betr�gt, dann geben Sie in dieses Feld 25 ein.;
                           ENU=Specifies the relevant VAT rate for the particular combination of VAT business posting group and VAT product posting group. Do not enter the percent sign, only the number. For example, if the VAT rate is 25 %, enter 25 in this field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT %" }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Legt fest, wie unrealisierte Mehrwertsteuer gehandhabt wird. Dabei handelt es sich um eine Mehrwertsteuer, die zwar berechnet wird, aber erst beim Zahlen der Rechnung f�llig wird.;
                           ENU=Specifies how to handle unrealized VAT, which is VAT that is calculated but not due until the invoice is paid.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unrealized VAT Type" }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Legt einen Code fest, um mehrere MwSt.-Buchungsmatrix Einrichtungen mit �hnlichen Eigenschaften, zum Beispiel MwSt.-Prozentsatz, zu gruppieren.;
                           ENU=Specifies a code to group various VAT posting setups with similar attributes, for example VAT percentage.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Identifier" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den MwSt.-Klauselcode fest, der der MwSt.-Buchungsmatrix Einr. zugeordnet ist.;
                           ENU=Specifies the VAT Clause Code that is associated with the VAT Posting Setup.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Clause Code" }

    { 7   ;2   ;Field     ;
                ToolTipML=[DEU=Legt fest, ob diese Kombination aus MwSt.-Gesch�ftsbuchungsgruppe und MwSt.-Produktbuchungsgruppe in den regelm��igen MwSt.-Erkl�rungen als Dienstleistung aufgef�hrt werden soll.;
                           ENU=Specifies if this combination of VAT business posting group and VAT product posting group are to be reported as services in the periodic VAT reports.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="EU Service" }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob Mehrwertsteuerbetr�ge neu berechnet werden sollen, wenn Sie Zahlungen buchen, die Skonti ausl�sen.;
                           ENU=Specifies whether to recalculate VAT amounts when you post payments that trigger payment discounts.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Adjust for Payment Discount" }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Legt fest, ob Belege, die diese Kombination aus MwSt.-Gesch�ftsbuchungsgruppe und MwSt.-Produktbuchungsgruppe verwenden, eine Gelangensbest�tigung erfordern.;
                           ENU=Specifies if documents that use this combination of VAT business posting group and VAT product posting group require a certificate of supply.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Certificate of Supply Required" }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die MwSt.-Kategorie in Verbindung mit dem Senden von elektronischen Belegen an. Wenn Sie beispielsweise Verkaufsbelege durch den PEPPOL-Service senden, wird der Wert in diesem Feld verwendet, um mehrere Felder auszuf�llen, wie das Element "TaxApplied" in der Gruppe "Lieferant". Die Nummer basiert auf dem UNCL5305-Standard.;
                           ENU=Specifies the VAT category in connection with electronic document sending. For example, when you send sales documents through the PEPPOL service, the value in this field is used to populate the TaxApplied element in the Supplier group. The number is based on the UNCL5305 standard.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Tax Category" }

    { 1904305601;1;Group  ;
                CaptionML=[DEU=Verkauf;
                           ENU=Sales] }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos an, auf das die Umsatzsteuer f�r diese spezielle Kombination aus MwSt.-Gesch�ftsbuchungsgruppe und MwSt.-Produktbuchungsgruppe gebucht wird.;
                           ENU=Specifies the general ledger account number to which to post sales VAT for the particular combination of VAT business posting group and VAT product posting group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sales VAT Account" }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos an, auf das f�r diese Kombination aus MwSt.-Gesch�fts- und MwSt.-Produktbuchungsgruppe die unrealisierte Umsatzsteuer (wie sie bei der Buchung von Verkaufsrechnungen berechnet wird) gebucht werden soll.;
                           ENU=Specifies the general ledger account number to which to post unrealized sales VAT (as calculated when you post sales invoices) using this particular combination of VAT business posting group and VAT product posting group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sales VAT Unreal. Account" }

    { 1907458401;1;Group  ;
                CaptionML=[DEU=Einkauf;
                           ENU=Purchases] }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos an, auf das f�r diese Kombination aus Gesch�fts- und Produktbuchungsgruppe beim Buchen der Erwerbssteuer gebucht werden soll.;
                           ENU=Specifies the general ledger account number to which to post purchase VAT for the particular combination of business group and product group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Purchase VAT Account" }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos an, auf das f�r diese Kombination aus MwSt.-Gesch�fts- und MwSt.-Produktbuchungsgruppe die unrealisierte Erwerbssteuer (wie sie bei der Buchung von Einkaufsrechnungen berechnet wird) gebucht werden soll.;
                           ENU=Specifies the general ledger account number to which to post unrealized purchase VAT (as calculated when you post purchase invoices) using this particular combination of VAT business posting group and VAT product posting group.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Purch. VAT Unreal. Account" }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos an, auf das f�r diese Kombination aus MwSt.-Gesch�fts- und MwSt.-Produktbuchungsgruppe die Erwerbssteuer gebucht werden soll, wenn Sie im Feld "MwSt.-Berechnungsart" die Erwerbsbesteuerung gew�hlt haben.;
                           ENU=Specifies the general ledger account number to which you want to post reverse charge VAT (purchase VAT) for this combination of VAT business posting group and VAT product posting group, if you have selected the Reverse Charge VAT option in the VAT Calculation Type field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reverse Chrg. VAT Acc." }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos an, auf das f�r diese Kombination aus MwSt.-Gesch�fts- und MwSt.-Produktbuchungsgruppe die Betr�ge f�r die unrealisierte Erwerbssteuer gebucht werden sollen, wenn Sie im Feld "MwSt.-Berechnungsart" die Erwerbsbesteuerung gew�hlt haben.;
                           ENU=Specifies the general ledger account number to which you want to post amounts for unrealized reverse charge VAT (purchase VAT) for this combination of VAT business posting group and VAT product posting group, if you have selected the Reverse Charge VAT option in the VAT Calculation Type field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reverse Chrg. VAT Unreal. Acc." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      CopyVATPostingSetup@1000 : Report 85;

    BEGIN
    END.
  }
}

