OBJECT Page 5929 Fault Reason Codes
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Problemursachen;
               ENU=Fault Reason Codes];
    SourceTable=Table5917;
    PageType=List;
    OnInit=BEGIN
             CurrPage.LOOKUPMODE := FALSE;
           END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Problemursache;
                                 ENU=&Fault];
                      Image=Error }
      { 12      ;2   ;Action    ;
                      CaptionML=[DEU=Serv&icezeilen�bersicht;
                                 ENU=Serv&ice Line List];
                      RunObject=Page 5904;
                      RunPageView=SORTING(Fault Reason Code);
                      RunPageLink=Fault Reason Code=FIELD(Code);
                      Image=ServiceLines }
      { 13      ;2   ;Action    ;
                      CaptionML=[DEU=Serviceartikelzeilen�bersicht;
                                 ENU=Service Item Line List];
                      RunObject=Page 5903;
                      RunPageView=SORTING(Fault Reason Code);
                      RunPageLink=Fault Reason Code=FIELD(Code);
                      Image=ServiceItem }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code f�r die Problemursache an.;
                           ENU=Specifies a code for the fault reason.];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Problemursachencodes an.;
                           ENU=Specifies a description of the fault reason code.];
                SourceExpr=Description }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass Sie einen Garantierabatt f�r den Serviceartikel ausschlie�en m�chten, der diesem Problemursachencode zugeordnet ist.;
                           ENU=Specifies that you want to exclude a warranty discount for the service item assigned this fault reason code.];
                SourceExpr="Exclude Warranty Discount" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass Sie einen Vertrags-/Servicerabatt f�r den Serviceartikel ausschlie�en m�chten, der diesem Problemursachencode zugeordnet ist.;
                           ENU=Specifies that you want to exclude a contract/service discount for the service item assigned this fault reason code.];
                SourceExpr="Exclude Contract Discount" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

