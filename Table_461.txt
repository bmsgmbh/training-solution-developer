OBJECT Table 461 Prepayment Inv. Line Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Zeilenpuffer Vorauszahlungsrechnung;
               ENU=Prepayment Inv. Line Buffer];
  }
  FIELDS
  {
    { 1   ;   ;G/L Account No.     ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[DEU=Sachkontonr.;
                                                              ENU=G/L Account No.] }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[DEU=Zeilennr.;
                                                              ENU=Line No.] }
    { 3   ;   ;Amount              ;Decimal       ;CaptionML=[DEU=Betrag;
                                                              ENU=Amount];
                                                   AutoFormatType=2 }
    { 4   ;   ;Description         ;Text50        ;CaptionML=[DEU=Beschreibung;
                                                              ENU=Description] }
    { 5   ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[DEU=Gesch�ftsbuchungsgruppe;
                                                              ENU=Gen. Bus. Posting Group] }
    { 6   ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[DEU=Produktbuchungsgruppe;
                                                              ENU=Gen. Prod. Posting Group] }
    { 7   ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[DEU=MwSt.-Gesch�ftsbuchungsgruppe;
                                                              ENU=VAT Bus. Posting Group] }
    { 8   ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[DEU=MwSt.-Produktbuchungsgruppe;
                                                              ENU=VAT Prod. Posting Group] }
    { 9   ;   ;VAT Amount          ;Decimal       ;CaptionML=[DEU=MwSt.-Betrag;
                                                              ENU=VAT Amount];
                                                   AutoFormatType=1 }
    { 10  ;   ;VAT Calculation Type;Option        ;CaptionML=[DEU=MwSt.-Berechnungsart;
                                                              ENU=VAT Calculation Type];
                                                   OptionCaptionML=[DEU=Normale MwSt.,Erwerbsbesteuerung,Nur MwSt.,Verkaufssteuer;
                                                                    ENU=Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax];
                                                   OptionString=Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax }
    { 11  ;   ;VAT Base Amount     ;Decimal       ;CaptionML=[DEU=MwSt.-Bemessungsgrundlage;
                                                              ENU=VAT Base Amount];
                                                   AutoFormatType=1 }
    { 12  ;   ;Amount (ACY)        ;Decimal       ;CaptionML=[DEU=Betrag (BW);
                                                              ENU=Amount (ACY)];
                                                   AutoFormatType=1 }
    { 13  ;   ;VAT Amount (ACY)    ;Decimal       ;CaptionML=[DEU=MwSt.-Betrag (BW);
                                                              ENU=VAT Amount (ACY)];
                                                   AutoFormatType=1 }
    { 14  ;   ;VAT Base Amount (ACY);Decimal      ;CaptionML=[DEU=MwSt.-Bemessungsgrundlage (BW);
                                                              ENU=VAT Base Amount (ACY)];
                                                   AutoFormatType=1 }
    { 15  ;   ;VAT Difference      ;Decimal       ;CaptionML=[DEU=MwSt.-Differenz;
                                                              ENU=VAT Difference];
                                                   AutoFormatType=1 }
    { 16  ;   ;VAT %               ;Decimal       ;CaptionML=[DEU=MwSt. %;
                                                              ENU=VAT %];
                                                   DecimalPlaces=1:1 }
    { 17  ;   ;VAT Identifier      ;Code10        ;CaptionML=[DEU=MwSt.-Kennzeichen;
                                                              ENU=VAT Identifier];
                                                   Editable=No }
    { 19  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[DEU=Globaler Dimensionscode 1;
                                                              ENU=Global Dimension 1 Code];
                                                   CaptionClass='1,1,1' }
    { 20  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[DEU=Globaler Dimensionscode 2;
                                                              ENU=Global Dimension 2 Code];
                                                   CaptionClass='1,1,2' }
    { 21  ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[DEU=Projektnr.;
                                                              ENU=Job No.] }
    { 22  ;   ;Amount Incl. VAT    ;Decimal       ;CaptionML=[DEU=Betrag inkl. MwSt.;
                                                              ENU=Amount Incl. VAT];
                                                   AutoFormatType=1 }
    { 24  ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[DEU=Steuergebietscode;
                                                              ENU=Tax Area Code] }
    { 25  ;   ;Tax Liable          ;Boolean       ;CaptionML=[DEU=Steuerpflichtig;
                                                              ENU=Tax Liable] }
    { 26  ;   ;Tax Group Code      ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[DEU=Steuergruppencode;
                                                              ENU=Tax Group Code] }
    { 27  ;   ;Invoice Rounding    ;Boolean       ;CaptionML=[DEU=Rechnungsrundung;
                                                              ENU=Invoice Rounding] }
    { 28  ;   ;Adjustment          ;Boolean       ;CaptionML=[DEU=Regulierung;
                                                              ENU=Adjustment] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=[DEU=Dimensionssatz-ID;
                                                              ENU=Dimension Set ID];
                                                   Editable=No }
    { 1001;   ;Job Task No.        ;Code20        ;TableRelation="Job Task";
                                                   CaptionML=[DEU=Projektaufgabennr.;
                                                              ENU=Job Task No.] }
  }
  KEYS
  {
    {    ;G/L Account No.,Job No.,Tax Area Code,Tax Liable,Tax Group Code,Invoice Rounding,Adjustment,Line No.,Dimension Set ID;
                                                   SumIndexFields=Amount,Amount Incl. VAT;
                                                   Clustered=Yes }
    {    ;Adjustment                               }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE IncrAmounts@1(PrepmtInvLineBuf@1001 : Record 461);
    BEGIN
      Amount := Amount + PrepmtInvLineBuf.Amount;
      "Amount Incl. VAT" := "Amount Incl. VAT" + PrepmtInvLineBuf."Amount Incl. VAT";
      "VAT Amount" := "VAT Amount" + PrepmtInvLineBuf."VAT Amount";
      "VAT Base Amount" := "VAT Base Amount" + PrepmtInvLineBuf."VAT Base Amount";
      "Amount (ACY)" := "Amount (ACY)" + PrepmtInvLineBuf."Amount (ACY)";
      "VAT Amount (ACY)" := "VAT Amount (ACY)" + PrepmtInvLineBuf."VAT Amount (ACY)";
      "VAT Base Amount (ACY)" := "VAT Base Amount (ACY)" + PrepmtInvLineBuf."VAT Base Amount (ACY)";
      "VAT Difference" := "VAT Difference" + PrepmtInvLineBuf."VAT Difference";
    END;

    PROCEDURE ReverseAmounts@2();
    BEGIN
      Amount := -Amount;
      "Amount Incl. VAT" := -"Amount Incl. VAT";
      "VAT Amount" := -"VAT Amount";
      "VAT Base Amount" := -"VAT Base Amount";
      "Amount (ACY)" := -"Amount (ACY)";
      "VAT Amount (ACY)" := -"VAT Amount (ACY)";
      "VAT Base Amount (ACY)" := -"VAT Base Amount (ACY)";
      "VAT Difference" := -"VAT Difference";
    END;

    PROCEDURE SetAmounts@12(AmountLCY@1000 : Decimal;AmountInclVAT@1001 : Decimal;VATBaseAmount@1002 : Decimal;AmountACY@1003 : Decimal;VATBaseAmountACY@1004 : Decimal;VATDifference@1005 : Decimal);
    BEGIN
      Amount := AmountLCY;
      "Amount Incl. VAT" := AmountInclVAT;
      "VAT Base Amount" := VATBaseAmount;
      "Amount (ACY)" := AmountACY;
      "VAT Base Amount (ACY)" := VATBaseAmountACY;
      "VAT Difference" := VATDifference;
    END;

    PROCEDURE InsertInvLineBuffer@3(PrepmtInvLineBuf2@1000 : Record 461);
    BEGIN
      Rec := PrepmtInvLineBuf2;
      IF FIND THEN BEGIN
        IncrAmounts(PrepmtInvLineBuf2);
        MODIFY;
      END ELSE
        INSERT;
    END;

    PROCEDURE CopyWithLineNo@4(PrepmtInvLineBuf@1001 : Record 461;LineNo@1002 : Integer);
    BEGIN
      Rec := PrepmtInvLineBuf;
      "Line No." := LineNo;
      INSERT;
    END;

    PROCEDURE CopyFromPurchLine@11(PurchLine@1000 : Record 39);
    BEGIN
      "Gen. Prod. Posting Group" := PurchLine."Gen. Prod. Posting Group";
      "VAT Prod. Posting Group" := PurchLine."VAT Prod. Posting Group";
      "Gen. Bus. Posting Group" := PurchLine."Gen. Bus. Posting Group";
      "VAT Bus. Posting Group" := PurchLine."VAT Bus. Posting Group";
      "VAT Calculation Type" := PurchLine."Prepmt. VAT Calc. Type";
      "VAT Identifier" := PurchLine."Prepayment VAT Identifier";
      "VAT %" := PurchLine."VAT %";
      "Global Dimension 1 Code" := PurchLine."Shortcut Dimension 1 Code";
      "Global Dimension 2 Code" := PurchLine."Shortcut Dimension 2 Code";
      "Dimension Set ID" := PurchLine."Dimension Set ID";
      "Job No." := PurchLine."Job No.";
      "Job Task No." := PurchLine."Job Task No.";
      "Tax Area Code" := PurchLine."Tax Area Code";
      "Tax Liable" := PurchLine."Tax Liable";
      "Tax Group Code" := PurchLine."Tax Group Code";
    END;

    PROCEDURE CopyFromSalesLine@13(SalesLine@1000 : Record 37);
    BEGIN
      "Gen. Prod. Posting Group" := SalesLine."Gen. Prod. Posting Group";
      "VAT Prod. Posting Group" := SalesLine."VAT Prod. Posting Group";
      "Gen. Bus. Posting Group" := SalesLine."Gen. Bus. Posting Group";
      "VAT Bus. Posting Group" := SalesLine."VAT Bus. Posting Group";
      "VAT Calculation Type" := SalesLine."Prepmt. VAT Calc. Type";
      "VAT Identifier" := SalesLine."Prepayment VAT Identifier";
      "VAT %" := SalesLine."VAT %";
      "Global Dimension 1 Code" := SalesLine."Shortcut Dimension 1 Code";
      "Global Dimension 2 Code" := SalesLine."Shortcut Dimension 2 Code";
      "Dimension Set ID" := SalesLine."Dimension Set ID";
      "Job No." := SalesLine."Job No.";
      "Job Task No." := SalesLine."Job Task No.";
      "Tax Area Code" := SalesLine."Tax Area Code";
      "Tax Liable" := SalesLine."Tax Liable";
      "Tax Group Code" := SalesLine."Tax Group Code";
    END;

    PROCEDURE SetFilterOnPKey@5(PrepmtInvLineBuf@1001 : Record 461);
    BEGIN
      RESET;
      SETRANGE("G/L Account No.",PrepmtInvLineBuf."G/L Account No.");
      SETRANGE("Dimension Set ID",PrepmtInvLineBuf."Dimension Set ID");
      SETRANGE("Job No.",PrepmtInvLineBuf."Job No.");
      SETRANGE("Tax Area Code",PrepmtInvLineBuf."Tax Area Code");
      SETRANGE("Tax Liable",PrepmtInvLineBuf."Tax Liable");
      SETRANGE("Tax Group Code",PrepmtInvLineBuf."Tax Group Code");
      SETRANGE("Invoice Rounding",PrepmtInvLineBuf."Invoice Rounding");
      SETRANGE(Adjustment,PrepmtInvLineBuf.Adjustment);
      IF PrepmtInvLineBuf."Line No." <> 0 THEN
        SETRANGE("Line No.",PrepmtInvLineBuf."Line No.");
    END;

    PROCEDURE FillAdjInvLineBuffer@6(PrepmtInvLineBuf@1000 : Record 461;GLAccountNo@1003 : Code[20];CorrAmount@1002 : Decimal;CorrAmountACY@1001 : Decimal);
    BEGIN
      INIT;
      Adjustment := TRUE;
      "G/L Account No." := GLAccountNo;
      Amount := CorrAmount;
      "Amount Incl. VAT" := CorrAmount;
      "Amount (ACY)" := CorrAmountACY;
      "Line No." := PrepmtInvLineBuf."Line No.";
      "Global Dimension 1 Code" := PrepmtInvLineBuf."Global Dimension 1 Code";
      "Global Dimension 2 Code" := PrepmtInvLineBuf."Global Dimension 2 Code";
      "Dimension Set ID" := PrepmtInvLineBuf."Dimension Set ID";
      Description := PrepmtInvLineBuf.Description;
    END;

    PROCEDURE FillFromGLAcc@7(CompressPrepayment@1002 : Boolean);
    VAR
      GLAcc@1001 : Record 15;
    BEGIN
      GLAcc.GET("G/L Account No.");
      "Gen. Prod. Posting Group" := GLAcc."Gen. Prod. Posting Group";
      "VAT Prod. Posting Group" := GLAcc."VAT Prod. Posting Group";
      IF CompressPrepayment THEN
        Description := GLAcc.Name;
    END;

    PROCEDURE AdjustVATBase@8(VATAdjustment@1002 : ARRAY [2] OF Decimal);
    BEGIN
      IF Amount <> "Amount Incl. VAT" THEN BEGIN
        Amount := Amount + VATAdjustment[1];
        "VAT Base Amount" := Amount;
        "VAT Amount" := "VAT Amount" + VATAdjustment[2];
        "Amount Incl. VAT" := Amount + "VAT Amount";
      END;
    END;

    PROCEDURE AmountsToArray@9(VAR VATAmount@1001 : ARRAY [2] OF Decimal);
    BEGIN
      VATAmount[1] := Amount;
      VATAmount[2] := "Amount Incl. VAT" - Amount;
    END;

    PROCEDURE CompressBuffer@10();
    VAR
      TempPrepmtInvLineBuffer2@1000 : TEMPORARY Record 461;
    BEGIN
      FIND('-');
      REPEAT
        TempPrepmtInvLineBuffer2 := Rec;
        TempPrepmtInvLineBuffer2."Line No." := 0;
        IF TempPrepmtInvLineBuffer2.FIND THEN BEGIN
          TempPrepmtInvLineBuffer2.IncrAmounts(Rec);
          TempPrepmtInvLineBuffer2.MODIFY;
        END ELSE
          TempPrepmtInvLineBuffer2.INSERT;
      UNTIL NEXT = 0;

      DELETEALL;

      TempPrepmtInvLineBuffer2.FIND('-');
      REPEAT
        Rec := TempPrepmtInvLineBuffer2;
        INSERT;
      UNTIL TempPrepmtInvLineBuffer2.NEXT = 0;
    END;

    PROCEDURE UpdateVATAmounts@14();
    VAR
      GLSetup@1000 : Record 98;
      Currency@1002 : Record 4;
      VATPostingSetup@1001 : Record 325;
    BEGIN
      GLSetup.GET;
      Currency.Initialize(GLSetup."Additional Reporting Currency");
      VATPostingSetup.GET("VAT Bus. Posting Group","VAT Prod. Posting Group");
      "VAT Amount" := ROUND(Amount * VATPostingSetup."VAT %" / 100);
      "VAT Amount (ACY)" := ROUND("Amount (ACY)" * VATPostingSetup."VAT %" / 100,Currency."Amount Rounding Precision");
    END;

    BEGIN
    END.
  }
}

