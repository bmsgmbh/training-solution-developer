OBJECT Page 506 Item Application Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Artikelausgleichsposten;
               ENU=Item Application Entries];
    SourceTable=Table339;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Buchungsdatum an, das dem Buchungsdatum des Artikelpostens entspricht, f�r den der Artikelausgleichsposten erstellt wurde.;
                           ENU=Specifies the posting date that corresponds to the posting date of the item ledger entry, for which this item application entry was created.];
                ApplicationArea=#Suite;
                SourceExpr="Posting Date" }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=F�r jede Lagertransaktion, die gebucht wurde, wird mindestens ein Artikelausgleichsposten erfasst.;
                           ENU=Specifies one or more item application entries for each inventory transaction that is posted.];
                ApplicationArea=#Suite;
                SourceExpr="Item Ledger Entry No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikelpostens an, der dem Lagerzugang oder der positiven Menge im Lager entspricht.;
                           ENU=Specifies the number of the item ledger entry corresponding to the inventory increase or positive quantity in inventory.];
                ApplicationArea=#Suite;
                SourceExpr="Inbound Item Entry No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikelpostens an, der dem Lagerabgang dieses Postens entspricht.;
                           ENU=Specifies the number of the item ledger entry corresponding to the inventory decrease for this entry.];
                ApplicationArea=#Suite;
                SourceExpr="Outbound Item Entry No." }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels an, die aus dem Lagerabgang im Feld "Ausgeh. Artikelposten Lfd. Nr." mit dem Lagerzugang im Feld "Eingeh. Artikelposten Lfd. Nr." ausgeglichen wird.;
                           ENU=Specifies the quantity of the item that is being applied from the inventory decrease in the Outbound Item Entry No. field, to the inventory increase in the Inbound Item Entry No. field.];
                ApplicationArea=#Suite;
                SourceExpr=Quantity }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine eindeutige laufende Nummer f�r jeden Artikelausgleichsposten an.;
                           ENU=Specifies a unique entry number for each item application entry.];
                ApplicationArea=#Suite;
                SourceExpr="Entry No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

