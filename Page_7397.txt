OBJECT Page 7397 Posted Invt. Pick Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Geb. Lagerkommissionierzeilen;
               ENU=Posted Invt. Pick Lines];
    LinksAllowed=No;
    SourceTable=Table7343;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 49      ;1   ;ActionGroup;
                      CaptionML=[DEU=Zei&le;
                                 ENU=&Line];
                      Image=Line }
      { 50      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Beleg anzeigen;
                                 ENU=Show Document];
                      Image=View;
                      OnAction=BEGIN
                                 PostedInvtPickHeader.GET("No.");
                                 PAGE.RUN(PAGE::"Posted Invt. Pick",PostedInvtPickHeader);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des gebuchten Lagerkommissionierkopfs an.;
                           ENU=Specifies the number of the posted inventory pick header.];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der gebuchten Lagerkommissionierzeile an.;
                           ENU=Specifies the number of the posted inventory pick line.];
                SourceExpr="Line No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Aktionsart f�r die Lagerkommissionierzeile fest. F�r gebuchte Lagerkommissionierzeilen wird bei der Aktionsart stets Lagerentnahme angegeben, um anzuzeigen, dass die Artikel aus dem Lagerplatz entnommen wurden.;
                           ENU=Specifies the action type for the inventory pick line. For posted inventory pick lines, the action type is always Take, meaning that the items are being taken out of the bin.];
                SourceExpr="Action Type";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Belegs an, auf den sich die Zeile bezieht (beispielsweise Verkaufsauftrag, Einkaufsbestellung oder Umlagerungsauftrag).;
                           ENU=Specifies the type of document (for example, sales order, purchase order or transfer) to which the line relates.];
                SourceExpr="Source Document" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Herkunftsbelegs an, aus dem die Lagerkommissionierzeile stammt.;
                           ENU=Specifies the number of the source document from which the inventory pick line originated.];
                SourceExpr="Source No." }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an, der kommissioniert wurde.;
                           ENU=Specifies the number of the item that was picked.];
                SourceExpr="Item No." }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Variantencode des Artikels an, der kommissioniert wurde.;
                           ENU=Specifies the variant code of the item that was picked.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Artikels an, der kommissioniert wurde.;
                           ENU=Specifies a description of the item that was picked.];
                SourceExpr=Description }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Seriennummer des Artikels an, der kommissioniert wurde.;
                           ENU=Specifies the serial number for the item that was picked.];
                SourceExpr="Serial No.";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Chargennummer des Artikels an, der kommissioniert wurde.;
                           ENU=Specifies the lot number for the item that was picked.];
                SourceExpr="Lot No.";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Ablaufdatum des Artikels an, der kommissioniert wurde.;
                           ENU=Specifies the expiration date for the item that was picked.];
                SourceExpr="Expiration Date";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, an dem die Artikel kommissioniert wurden.;
                           ENU=Specifies the code for the location at which the items were pick.];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerplatzes an, an dem der Artikel kommissioniert wurde.;
                           ENU=Specifies the code for the bin in which item was picked.];
                SourceExpr="Bin Code" }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Regalnummer des Artikels zu Informationszwecken an.;
                           ENU=Specifies the shelf number of the item for informational use.];
                SourceExpr="Shelf No.";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels an, die kommissioniert wurde.;
                           ENU=Specifies the quantity of the item that was picked.];
                SourceExpr=Quantity }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge des Artikels, der kommissioniert wurde, in der Basiseinheit an.;
                           ENU=Specifies the quantity, in the base unit of measure, of the item that was picked.];
                SourceExpr="Qty. (Base)";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Aktivit�t kommissioniert sein h�tte m�ssen.;
                           ENU=Specifies the date when the pick should have been completed.];
                SourceExpr="Due Date" }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einheitencode des Artikels an, der kommissioniert wurde.;
                           ENU=Specifies the unit of measure code of the item that was picked.];
                SourceExpr="Unit of Measure Code" }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge pro Einheit des Artikels an, der kommissioniert wurde.;
                           ENU=Specifies the quantity per unit of measure of the item that was picked.];
                SourceExpr="Qty. per Unit of Measure";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Ziels an, das mit der gebuchten Lagerkommissionierzeile verkn�pft ist.;
                           ENU=Specifies the type of destination associated with the posted inventory pick line.];
                SourceExpr="Destination Type";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer oder den Code des Debitors oder Kreditors an, der mit dieser Zeile verkn�pft ist.;
                           ENU=Specifies the number or the code of the customer or vendor linked to the line.];
                SourceExpr="Destination No.";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerhilfsmittelcode des Artikels an, der kommissioniert wurde.;
                           ENU=Specifies the special equipment code for the item that was picked.];
                SourceExpr="Special Equipment Code";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      PostedInvtPickHeader@1000 : Record 7342;

    BEGIN
    END.
  }
}

