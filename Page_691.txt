OBJECT Page 691 Send-to Programs
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Zielprogramme;
               ENU=Send-to Programs];
    SourceTable=Table2000000065;
    DelayedInsert=Yes;
    PageType=List;
    OnNewRecord=BEGIN
                  IF ISNULLGUID("Program ID") THEN
                    CreateNewGUID;
                  Parameter := '%1';
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                CaptionML=[DEU=Programm-ID;
                           ENU=Program ID];
                ToolTipML=[DEU=Gibt die ID des Programms an, an das Daten aus Microsoft Dynamics NAV gesendet werden sollen.;
                           ENU=Specifies the ID of the program to send data to from Microsoft Dynamics NAV.];
                SourceExpr="Program ID";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                CaptionML=[DEU=Programmdatei;
                           ENU=Executable];
                ToolTipML=[DEU=Gibt den Namen der ausf�hrbaren Datei an, die das Programm startet.;
                           ENU=Specifies the name of the executable file that launches the program.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Executable;
                OnValidate=BEGIN
                             TESTFIELD(Executable);
                             CreateNewGUID;
                             IF Name = '' THEN
                               Name := Executable;
                           END;
                            }

    { 8   ;2   ;Field     ;
                CaptionML=[DEU=Name;
                           ENU=Name];
                ToolTipML=[DEU=Gibt den Namen des Programms an, an das Daten aus Dynamics NAV gesendet werden sollen.;
                           ENU=Specifies the name of the program to send data to from Dynamics NAV.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                CaptionML=[DEU=Parameter;
                           ENU=Parameter];
                ToolTipML=[DEU=Gibt den Parameter an, der aus Microsoft Dynamics NAV an das Programm gesendet werden soll.;
                           ENU=Specifies the parameter to send to the program from Microsoft Dynamics NAV.];
                SourceExpr=Parameter;
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    LOCAL PROCEDURE CreateNewGUID@6();
    BEGIN
      CASE UPPERCASE(Executable) OF
        'WINWORD.EXE':
          "Program ID" := '{000209FF-0000-0000-C000-000000000046}';  // defined in fin.stx
        'EXCEL.EXE':
          "Program ID" := '{00024500-0000-0000-C000-000000000046}';  // defined in fin.stx
        ELSE
          "Program ID" := CREATEGUID;
      END;
    END;

    BEGIN
    END.
  }
}

