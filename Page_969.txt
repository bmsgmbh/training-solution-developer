OBJECT Page 969 Time Sheet Line Absence Detail
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Arbeitszeittabellenzeile - Abwesenheitsdetails;
               ENU=Time Sheet Line Absence Detail];
    SourceTable=Table951;
    PageType=StandardDialog;
    SourceTableTemporary=Yes;
    OnAfterGetCurrRecord=BEGIN
                           AllowEdit := GetAllowEdit(0,ManagerRole);
                         END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General];
                GroupType=Group }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Liste der Standardabwesenheitscodes an, aus denen Sie einen ausw�hlen k�nnen.;
                           ENU=Specifies a list of standard absence codes, from which you may select one.];
                ApplicationArea=#Jobs;
                SourceExpr="Cause of Absence Code";
                Editable=AllowEdit }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Arbeitszeittabellenzeile an.;
                           ENU=Specifies a description of the time sheet line.];
                ApplicationArea=#Jobs;
                SourceExpr=Description;
                Editable=AllowEdit }

  }
  CODE
  {
    VAR
      ManagerRole@1000 : Boolean;
      AllowEdit@1001 : Boolean;

    PROCEDURE SetParameters@2(TimeSheetLine@1000 : Record 951;NewManagerRole@1001 : Boolean);
    BEGIN
      Rec := TimeSheetLine;
      INSERT;
      ManagerRole := NewManagerRole;
    END;

    BEGIN
    END.
  }
}

