OBJECT Table 6705 Booking Staff
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    TableType=Exchange;
    ExternalName=BookingStaff;
    CaptionML=[DEU=Bookings-Personal;
               ENU=Booking Staff];
  }
  FIELDS
  {
    { 1   ;   ;SMTP Address        ;Text250       ;ExternalName=SmtpAddress;
                                                   CaptionML=[DEU=SMTP-Adresse;
                                                              ENU=SMTP Address] }
    { 2   ;   ;Display Name        ;Text250       ;ExternalName=DisplayName;
                                                   CaptionML=[DEU=Anzeigename;
                                                              ENU=Display Name] }
    { 3   ;   ;Permission          ;Option        ;ExternalName=Permission;
                                                   CaptionML=[DEU=Zugriffsrecht;
                                                              ENU=Permission];
                                                   OptionCaptionML=[DEU=Ung�ltig,Administrator,Viewer,Gast;
                                                                    ENU=Invalid,Administrator,Viewer,Guest];
                                                   OptionString=Invalid,Administrator,Viewer,Guest }
  }
  KEYS
  {
    {    ;SMTP Address                            ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

