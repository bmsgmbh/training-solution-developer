OBJECT Page 8616 Config. Package Errors
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Paketfehler konfigurieren;
               ENU=Config. Package Errors];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table8617;
    DataCaptionExpr=FormCaption;
    PageType=List;
    OnAfterGetRecord=BEGIN
                       RecordIDValue := FORMAT("Record ID");
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Text des Fehlers im Migrationsfeld an. Sie k�nnen die im Fehlertext enthaltenen Informationen zum Beheben von Migrationsproblemen verwenden, bevor Sie Migrationsdaten in die Datenbank �bernehmen.;
                           ENU=Specifies the text of the error in the migration field. You can use information contained in the error text to fix migration problems before you attempt to apply migration data to the database.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Error Text" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Caption des Migrationsfelds an, auf das sich der Fehler bezieht.;
                           ENU=Specifies the caption of the migration field to which the error applies.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Field Caption" }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Felds in der Migrationstabelle an, auf das sich der Fehler bezieht.;
                           ENU=Specifies the name of the field in the migration table to which the error applies.];
                SourceExpr="Field Name";
                Visible=FALSE }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Datensatz in der Migrationstabelle an, auf den sich der Fehler bezieht.;
                           ENU=Specifies the record in the migration table to which the error applies.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=RecordIDValue;
                CaptionClass=FIELDCAPTION("Record ID");
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      RecordIDValue@1000 : Text;

    LOCAL PROCEDURE FormCaption@1() : Text[1024];
    VAR
      ConfigPackageTable@1000 : Record 8613;
    BEGIN
      ConfigPackageTable.SETRANGE("Package Code","Package Code");
      ConfigPackageTable.SETRANGE("Table ID","Table ID");
      IF ConfigPackageTable.FINDFIRST THEN
        ConfigPackageTable.CALCFIELDS("Table Caption");

      EXIT(ConfigPackageTable."Table Caption");
    END;

    BEGIN
    END.
  }
}

