OBJECT Table 9183 Generic Chart Query Column
{
  OBJECT-PROPERTIES
  {
    Date=09.09.14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Generisches Diagramm - Abfragespalte;
               ENU=Generic Chart Query Column];
  }
  FIELDS
  {
    { 1   ;   ;Query No.           ;Integer       ;CaptionML=[DEU=Abfragenr.;
                                                              ENU=Query No.] }
    { 2   ;   ;Query Column No.    ;Integer       ;CaptionML=[DEU=Abfragespaltennr.;
                                                              ENU=Query Column No.] }
    { 3   ;   ;Column Name         ;Text50        ;CaptionML=[DEU=Spaltenname;
                                                              ENU=Column Name] }
    { 4   ;   ;Column Data Type    ;Option        ;CaptionML=[DEU=Spaltendatentyp;
                                                              ENU=Column Data Type];
                                                   OptionCaptionML=[DEU=Datum,Uhrzeit,Datumsformel,Dezimalzahl,Text,Code,Bin�r,Boolesch,Ganzzahl,Option,BigInteger,DateTime;
                                                                    ENU=Date,Time,DateFormula,Decimal,Text,Code,Binary,Boolean,Integer,Option,BigInteger,DateTime];
                                                   OptionString=Date,Time,DateFormula,Decimal,Text,Code,Binary,Boolean,Integer,Option,BigInteger,DateTime }
    { 5   ;   ;Column Type         ;Option        ;CaptionML=[DEU=Spaltenart;
                                                              ENU=Column Type];
                                                   OptionCaptionML=[DEU=Filterspalte,Spalte;
                                                                    ENU=Filter Column,Column];
                                                   OptionString=Filter Column,Column }
    { 6   ;   ;Entry No.           ;Integer       ;CaptionML=[DEU=Lfd. Nr.;
                                                              ENU=Entry No.] }
    { 7   ;   ;Aggregation Type    ;Option        ;CaptionML=[DEU=Aggregationsart;
                                                              ENU=Aggregation Type];
                                                   OptionCaptionML=[DEU=Kein,Anzahl,Summe,Min,Max,Durchschn;
                                                                    ENU=None,Count,Sum,Min,Max,Avg];
                                                   OptionString=None,Count,Sum,Min,Max,Avg }
  }
  KEYS
  {
    {    ;Query No.,Query Column No.,Entry No.    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst '@@@=NONE;DEU=KEINE;ENU=NONE';
      Text001@1001 : TextConst '@@@=COUNT;DEU=ANZAHL;ENU=COUNT';
      Text002@1002 : TextConst '@@@=SUM;DEU=SUMME;ENU=SUM';
      Text003@1003 : TextConst '@@@=MIN;DEU=MIN;ENU=MIN';
      Text004@1004 : TextConst '@@@=MAX;DEU=MAX;ENU=MAX';
      Text005@1005 : TextConst '@@@=AVERAGE;DEU=DURCHSCHNITT;ENU=AVERAGE';

    PROCEDURE SetAggregationType@34(InputTxt@1000 : Text);
    BEGIN
      CASE UPPERCASE(InputTxt) OF
        Text000:
          "Aggregation Type" := "Aggregation Type"::None;
        Text001:
          "Aggregation Type" := "Aggregation Type"::Count;
        Text002:
          "Aggregation Type" := "Aggregation Type"::Sum;
        Text003:
          "Aggregation Type" := "Aggregation Type"::Min;
        Text004:
          "Aggregation Type" := "Aggregation Type"::Max;
        Text005:
          "Aggregation Type" := "Aggregation Type"::Avg;
      END;
    END;

    PROCEDURE SetColumnDataType@1(FieldType@1001 : Option);
    VAR
      Field@1000 : Record 2000000041;
    BEGIN
      CASE FieldType OF
        Field.Type::Date:
          "Column Data Type" := "Column Data Type"::Date;
        Field.Type::Time:
          "Column Data Type" := "Column Data Type"::Time;
        Field.Type::DateFormula:
          "Column Data Type" := "Column Data Type"::DateFormula;
        Field.Type::Decimal:
          "Column Data Type" := "Column Data Type"::Decimal;
        Field.Type::Text:
          "Column Data Type" := "Column Data Type"::Text;
        Field.Type::Code:
          "Column Data Type" := "Column Data Type"::Code;
        Field.Type::Binary:
          "Column Data Type" := "Column Data Type"::Binary;
        Field.Type::Boolean:
          "Column Data Type" := "Column Data Type"::Boolean;
        Field.Type::Integer:
          "Column Data Type" := "Column Data Type"::Integer;
        Field.Type::Option:
          "Column Data Type" := "Column Data Type"::Option;
        Field.Type::BigInteger:
          "Column Data Type" := "Column Data Type"::BigInteger;
        Field.Type::DateTime:
          "Column Data Type" := "Column Data Type"::DateTime;
      END;
    END;

    BEGIN
    END.
  }
}

