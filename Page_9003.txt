OBJECT Page 9003 Acc. Receivables Adm. RC
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Rollencenter;
               ENU=Role Center];
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 9       ;1   ;Action    ;
                      CaptionML=[DEU=De&bitor - Liste;
                                 ENU=C&ustomer - List];
                      RunObject=Report 101;
                      Image=Report }
      { 13      ;1   ;Action    ;
                      CaptionML=[DEU=Debitorenposten &per;
                                 ENU=Customer - &Balance to Date];
                      RunObject=Report 121;
                      Image=Report }
      { 16      ;1   ;Action    ;
                      CaptionML=[DEU=Debitor - Salden&r�ckblick;
                                 ENU=Aged &Accounts Receivable];
                      RunObject=Report 120;
                      Image=Report }
      { 17      ;1   ;Action    ;
                      CaptionML=[DEU=Debitor - &Altersvert.-Saldo;
                                 ENU=Customer - &Summary Aging Simp.];
                      RunObject=Report 109;
                      Image=Report }
      { 18      ;1   ;Action    ;
                      CaptionML=[DEU=Debitor - Sald&enliste;
                                 ENU=Customer - Trial Balan&ce];
                      RunObject=Report 129;
                      Image=Report }
      { 19      ;1   ;Action    ;
                      CaptionML=[DEU=&Debitor/Artikel Statistik;
                                 ENU=Cus&tomer/Item Sales];
                      RunObject=Report 113;
                      Image=Report }
      { 20      ;1   ;Separator  }
      { 22      ;1   ;Action    ;
                      CaptionML=[DEU=Debitorenbe&legnummern;
                                 ENU=Customer &Document Nos.];
                      RunObject=Report 128;
                      Image=Report }
      { 23      ;1   ;Action    ;
                      CaptionML=[DEU=Verkau&fsrechnungsnummern;
                                 ENU=Sales &Invoice Nos.];
                      RunObject=Report 124;
                      Image=Report }
      { 24      ;1   ;Action    ;
                      CaptionML=[DEU=Verkaufsgutschriftsn&ummern;
                                 ENU=Sa&les Credit Memo Nos.];
                      RunObject=Report 125;
                      Image=Report }
      { 27      ;1   ;Action    ;
                      CaptionML=[DEU=Mahnungsnu&mmern;
                                 ENU=Re&minder Nos.];
                      RunObject=Report 126;
                      Image=Report }
      { 28      ;1   ;Action    ;
                      CaptionML=[DEU=Zinsrechnung&snummern;
                                 ENU=Finance Cha&rge Memo Nos.];
                      RunObject=Report 127;
                      Image=Report }
      { 1900000011;0 ;ActionContainer;
                      ActionContainerType=HomeItems }
      { 12      ;1   ;Action    ;
                      Name=Customers;
                      CaptionML=[DEU=Debitoren;
                                 ENU=Customers];
                      RunObject=Page 22;
                      Image=Customer }
      { 2       ;1   ;Action    ;
                      Name=CustomersBalance;
                      CaptionML=[DEU=Saldo;
                                 ENU=Balance];
                      RunObject=Page 22;
                      RunPageView=WHERE(Balance (LCY)=FILTER(<>0));
                      Image=Balance }
      { 14      ;1   ;Action    ;
                      CaptionML=[DEU=Verkaufsauftr�ge;
                                 ENU=Sales Orders];
                      RunObject=Page 9305;
                      Image=Order }
      { 92      ;1   ;Action    ;
                      CaptionML=[DEU=Verkaufsrechnungen;
                                 ENU=Sales Invoices];
                      RunObject=Page 9301;
                      Image=Invoice }
      { 3       ;1   ;Action    ;
                      CaptionML=[DEU=Verkaufsreklamationen;
                                 ENU=Sales Return Orders];
                      RunObject=Page 9304;
                      Image=ReturnOrder }
      { 5       ;1   ;Action    ;
                      CaptionML=[DEU=Bankkonten;
                                 ENU=Bank Accounts];
                      RunObject=Page 371;
                      Image=BankAccount }
      { 6       ;1   ;Action    ;
                      CaptionML=[DEU=Mahnungen;
                                 ENU=Reminders];
                      RunObject=Page 436;
                      Image=Reminder }
      { 7       ;1   ;Action    ;
                      CaptionML=[DEU=Zinsrechnungen;
                                 ENU=Finance Charge Memos];
                      RunObject=Page 448;
                      Image=FinChargeMemo }
      { 8       ;1   ;Action    ;
                      CaptionML=[DEU=Artikel;
                                 ENU=Items];
                      RunObject=Page 31;
                      Image=Item }
      { 31      ;1   ;Action    ;
                      Name=SalesJournals;
                      CaptionML=[DEU=Verkaufs Buch.-Bl�tter;
                                 ENU=Sales Journals];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Sales),
                                        Recurring=CONST(No)) }
      { 32      ;1   ;Action    ;
                      Name=CashReceiptJournals;
                      CaptionML=[DEU=Zlg.-Eing. Buch.-Bl�tter;
                                 ENU=Cash Receipt Journals];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Cash Receipts),
                                        Recurring=CONST(No));
                      Image=Journals }
      { 33      ;1   ;Action    ;
                      Name=GeneralJournals;
                      CaptionML=[DEU=Fibu Buch.-Bl�tter;
                                 ENU=General Journals];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(General),
                                        Recurring=CONST(No));
                      Image=Journal }
      { 4       ;1   ;Action    ;
                      CaptionML=[DEU=Lastschriften;
                                 ENU=Direct Debit Collections];
                      RunObject=Page 1207 }
      { 1900000012;0 ;ActionContainer;
                      ActionContainerType=ActivityButtons }
      { 39      ;1   ;ActionGroup;
                      CaptionML=[DEU=Gebuchte Belege;
                                 ENU=Posted Documents];
                      Image=FiledPosted }
      { 11      ;2   ;Action    ;
                      CaptionML=[DEU=Geb. Verkaufslieferungen;
                                 ENU=Posted Sales Shipments];
                      RunObject=Page 142;
                      Image=PostedShipment }
      { 25      ;2   ;Action    ;
                      CaptionML=[DEU=Geb. Verkaufsrechnungen;
                                 ENU=Posted Sales Invoices];
                      RunObject=Page 143;
                      Image=PostedOrder }
      { 10      ;2   ;Action    ;
                      CaptionML=[DEU=Gebuchte R�cksendungen;
                                 ENU=Posted Return Receipts];
                      RunObject=Page 6662;
                      Image=PostedReturnReceipt }
      { 26      ;2   ;Action    ;
                      CaptionML=[DEU=Geb. Verkaufsgutschriften;
                                 ENU=Posted Sales Credit Memos];
                      RunObject=Page 144;
                      Image=PostedOrder }
      { 34      ;2   ;Action    ;
                      CaptionML=[DEU=Geb. Einkaufsrechnungen;
                                 ENU=Posted Purchase Invoices];
                      RunObject=Page 146 }
      { 35      ;2   ;Action    ;
                      CaptionML=[DEU=Geb. Einkaufsgutschriften;
                                 ENU=Posted Purchase Credit Memos];
                      RunObject=Page 147 }
      { 29      ;2   ;Action    ;
                      CaptionML=[DEU=Registrierte Mahnungen;
                                 ENU=Issued Reminders];
                      RunObject=Page 440;
                      Image=OrderReminder }
      { 30      ;2   ;Action    ;
                      CaptionML=[DEU=Registrierte Zinsrechnungen;
                                 ENU=Issued Fin. Charge Memos];
                      RunObject=Page 452;
                      Image=PostedMemo }
      { 102     ;2   ;Action    ;
                      CaptionML=[DEU=Fibujournale;
                                 ENU=G/L Registers];
                      RunObject=Page 116;
                      Image=GLRegisters }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 64      ;1   ;Separator ;
                      CaptionML=[DEU=Neu;
                                 ENU=New];
                      IsHeader=Yes }
      { 103     ;1   ;Action    ;
                      CaptionML=[DEU=D&ebitor;
                                 ENU=C&ustomer];
                      RunObject=Page 21;
                      Promoted=No;
                      Image=Customer;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 36      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Verkauf;
                                 ENU=&Sales];
                      Image=Sales }
      { 37      ;2   ;Action    ;
                      CaptionML=[DEU=Verkaufsau&ftrag;
                                 ENU=Sales &Order];
                      RunObject=Page 42;
                      Promoted=No;
                      Image=Document;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 104     ;2   ;Action    ;
                      CaptionML=[DEU=Verkauf&srechnung;
                                 ENU=Sales &Invoice];
                      RunObject=Page 43;
                      Promoted=No;
                      Image=NewSalesInvoice;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 65      ;2   ;Action    ;
                      CaptionML=[DEU=Verkaufs&gutschrift;
                                 ENU=Sales &Credit Memo];
                      RunObject=Page 44;
                      Promoted=No;
                      Image=CreditMemo;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 105     ;2   ;Action    ;
                      CaptionML=[DEU=Verkaufs&zinsrechnung;
                                 ENU=Sales &Fin. Charge Memo];
                      RunObject=Page 446;
                      Promoted=No;
                      Image=FinChargeMemo;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 106     ;2   ;Action    ;
                      CaptionML=[DEU=Verkaufsma&hnung;
                                 ENU=Sales &Reminder];
                      RunObject=Page 434;
                      Promoted=No;
                      Image=Reminder;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 67      ;1   ;Separator ;
                      CaptionML=[DEU=Aufgaben;
                                 ENU=Tasks];
                      IsHeader=Yes }
      { 74      ;1   ;Action    ;
                      CaptionML=[DEU=Zahlungseingang&s Buch.-Blatt;
                                 ENU=Cash Receipt &Journal];
                      RunObject=Page 255;
                      Image=CashReceiptJournal }
      { 111     ;1   ;Separator  }
      { 112     ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Sammelrec&hnung;
                                 ENU=Combine Shi&pments];
                      RunObject=Report 295;
                      Image=Action }
      { 113     ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Sammelr�c&klieferungen;
                                 ENU=Combine Return S&hipments];
                      RunObject=Report 6653;
                      Image=Action }
      { 15      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Wiederkehrende Rechnungen erstellen;
                                 ENU=Create Recurring Invoices];
                      RunObject=Report 172;
                      Image=CreateDocument }
      { 84      ;1   ;Separator ;
                      CaptionML=[DEU=Verwaltung;
                                 ENU=Administration];
                      IsHeader=Yes }
      { 86      ;1   ;Action    ;
                      CaptionML=[DEU=Debitoren && Verka&uf Einr.;
                                 ENU=Sales && Recei&vables Setup];
                      RunObject=Page 459;
                      Image=Setup }
      { 89      ;1   ;Separator ;
                      CaptionML=[DEU=Historie;
                                 ENU=History];
                      IsHeader=Yes }
      { 90      ;1   ;Action    ;
                      CaptionML=[DEU=Navi&gate;
                                 ENU=Navi&gate];
                      RunObject=Page 344;
                      Image=Navigate }
    }
  }
  CONTROLS
  {
    { 1900000008;0;Container;
                ContainerType=RoleCenterArea }

    { 1900724808;1;Group   }

    { 1902899408;2;Part   ;
                PagePartID=Page9034;
                PartType=Page }

    { 1900724708;1;Group   }

    { 1907692008;2;Part   ;
                PagePartID=Page9150;
                PartType=Page }

    { 1905989608;2;Part   ;
                PagePartID=Page9152;
                Visible=FALSE;
                PartType=Page }

    { 38  ;2   ;Part      ;
                PagePartID=Page681;
                PartType=Page }

    { 1   ;2   ;Part      ;
                PagePartID=Page675;
                Visible=false;
                PartType=Page }

    { 1903012608;2;Part   ;
                PagePartID=Page9175;
                Visible=FALSE;
                PartType=Page }

    { 1901377608;2;Part   ;
                PartType=System;
                SystemPartID=MyNotes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

