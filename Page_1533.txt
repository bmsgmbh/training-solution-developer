OBJECT Page 1533 Workflow User Groups
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Workflow-Benutzergruppen;
               ENU=Workflow User Groups];
    SourceTable=Table1540;
    PageType=List;
    CardPageID=Workflow User Group;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Workflow-Benutzergruppe an.;
                           ENU=Specifies the workflow user group.];
                ApplicationArea=#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Workflow-Benutzergruppe an.;
                           ENU=Specifies the workflow user group.];
                ApplicationArea=#Suite;
                SourceExpr=Description }

  }
  CODE
  {

    BEGIN
    END.
  }
}

