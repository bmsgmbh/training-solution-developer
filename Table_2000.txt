OBJECT Table 2000 Time Series Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Zeitreihenpuffer;
               ENU=Time Series Buffer];
  }
  FIELDS
  {
    { 1   ;   ;Group ID            ;Code50        ;CaptionML=[DEU=Gruppen-ID;
                                                              ENU=Group ID] }
    { 2   ;   ;Period No.          ;Integer       ;CaptionML=[DEU=Periodennr.;
                                                              ENU=Period No.] }
    { 3   ;   ;Period Start Date   ;Date          ;CaptionML=[DEU=Periodenstartdatum;
                                                              ENU=Period Start Date] }
    { 4   ;   ;Value               ;Decimal       ;CaptionML=[DEU=Wert;
                                                              ENU=Value] }
  }
  KEYS
  {
    {    ;Group ID,Period No.                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

