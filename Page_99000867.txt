OBJECT Page 99000867 Finished Production Order
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Beendeter FA;
               ENU=Finished Production Order];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table5405;
    SourceTableView=WHERE(Status=CONST(Finished));
    PageType=Document;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 54      ;1   ;ActionGroup;
                      CaptionML=[DEU=A&uftrag;
                                 ENU=O&rder];
                      Image=Order }
      { 48      ;2   ;ActionGroup;
                      CaptionML=[DEU=&Posten;
                                 ENU=E&ntries];
                      Image=Entries }
      { 49      ;3   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=&Artikelposten;
                                 ENU=Item Ledger E&ntries];
                      RunObject=Page 38;
                      RunPageView=SORTING(Order Type,Order No.);
                      RunPageLink=Order Type=CONST(Production),
                                  Order No.=FIELD(No.);
                      Image=ItemLedger }
      { 66      ;3   ;Action    ;
                      CaptionML=[DEU=Kapazit�tsposten;
                                 ENU=Capacity Ledger Entries];
                      RunObject=Page 5832;
                      RunPageView=SORTING(Order Type,Order No.);
                      RunPageLink=Order Type=CONST(Production),
                                  Order No.=FIELD(No.);
                      Image=CapacityLedger }
      { 76      ;3   ;Action    ;
                      CaptionML=[DEU=Wertposten;
                                 ENU=Value Entries];
                      RunObject=Page 5802;
                      RunPageView=SORTING(Order Type,Order No.);
                      RunPageLink=Order Type=CONST(Production),
                                  Order No.=FIELD(No.);
                      Image=ValueLedger }
      { 7300    ;3   ;Action    ;
                      CaptionML=[DEU=&Lagerplatzposten;
                                 ENU=&Warehouse Entries];
                      RunObject=Page 7318;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.);
                      RunPageLink=Source Type=FILTER(83|5407),
                                  Source Subtype=FILTER(3|4|5),
                                  Source No.=FIELD(No.);
                      Image=BinLedger }
      { 69      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 99000838;
                      RunPageLink=Status=FIELD(Status),
                                  Prod. Order No.=FIELD(No.);
                      Image=ViewComments }
      { 162     ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Anzeigen oder Bearbeiten von Dimensionen, wie etwa eines Bereichs, eines Projekts oder einer Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                               END;
                                }
      { 70      ;2   ;Separator  }
      { 71      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[DEU=Statistik;
                                 ENU=Statistics];
                      RunObject=Page 99000816;
                      RunPageLink=Status=FIELD(Status),
                                  No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 20      ;2   ;Separator  }
      { 7301    ;2   ;Action    ;
                      CaptionML=[DEU=&Registrierte Kommissionierzeilen;
                                 ENU=Registered P&ick Lines];
                      RunObject=Page 7364;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.,Source Line No.,Source Subline No.);
                      RunPageLink=Source Type=CONST(5407),
                                  Source Subtype=CONST(3),
                                  Source No.=FIELD(No.);
                      Image=RegisteredDocs }
      { 2       ;2   ;Action    ;
                      Name=<Action2>;
                      CaptionML=[DEU=Registrierte Lagerbestandsu&mlagerungszeilen;
                                 ENU=Registered Invt. M&ovement Lines];
                      RunObject=Page 7387;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.,Source Line No.,Source Subline No.);
                      RunPageLink=Source Type=CONST(5407),
                                  Source Subtype=CONST(3),
                                  Source No.=FIELD(No.);
                      Image=RegisteredDocs }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 16  ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[DEU=Gibt die Nummer des Fertigungsauftrags an.;
                           ENU=Specifies the number of the production order.];
                SourceExpr="No.";
                Importance=Promoted;
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des Fertigungsauftrags an.;
                           ENU=Specifies the description of the production order.];
                SourceExpr=Description;
                Importance=Promoted;
                Editable=FALSE }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen zus�tzlichen Teil der Beschreibung des Fertigungsauftrags an.;
                           ENU=Specifies an additional part of the production order description.];
                SourceExpr="Description 2";
                Editable=FALSE }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Herkunftsart des Fertigungsauftrags an.;
                           ENU=Specifies the source type of the production order.];
                SourceExpr="Source Type";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Herkunftsnummer des Fertigungsauftrags an.;
                           ENU=Specifies the source number of the production order.];
                SourceExpr="Source No.";
                Editable=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Suchbegriff an.;
                           ENU=Specifies the search description.];
                SourceExpr="Search Description";
                Editable=FALSE }

    { 19  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Artikeleinheiten oder Fertigungsfamilien an, die Sie produzieren m�chten (Fertigungsmenge).;
                           ENU=Specifies how many units of the item or the family to produce (production quantity).];
                SourceExpr=Quantity;
                Importance=Promoted;
                Editable=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das F�lligkeitsdatum des Fertigungsauftrags an.;
                           ENU=Specifies the due date of the production order.];
                SourceExpr="Due Date";
                Editable=FALSE }

    { 45  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wann die Fertigungsauftragskarte zuletzt ge�ndert wurde.;
                           ENU=Specifies when the production order card was last modified.];
                SourceExpr="Last Date Modified";
                Editable=FALSE }

    { 26  ;1   ;Part      ;
                Name=ProdOrderLines;
                SubPageLink=Prod. Order No.=FIELD(No.);
                PagePartID=Page99000868 }

    { 1907170701;1;Group  ;
                CaptionML=[DEU=Plan;
                           ENU=Schedule] }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Startzeit des Fertigungsauftrags an.;
                           ENU=Specifies the starting time of the production order.];
                SourceExpr="Starting Time";
                Importance=Promoted;
                Editable=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Startdatum des Fertigungsauftrags an.;
                           ENU=Specifies the starting date of the production order.];
                SourceExpr="Starting Date";
                Importance=Promoted;
                Editable=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Endzeit des Fertigungsauftrags an.;
                           ENU=Specifies the ending time of the production order.];
                SourceExpr="Ending Time";
                Importance=Promoted;
                Editable=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Enddatum des Fertigungsauftrags an.;
                           ENU=Specifies the ending date of the production order.];
                SourceExpr="Ending Date";
                Importance=Promoted;
                Editable=FALSE }

    { 1904784501;1;Group  ;
                CaptionML=[DEU=Buchung;
                           ENU=Posting] }

    { 81  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Lagerbuchungsgruppe an, damit die WIP dem richtigen Sachkonto zugeordnet wird.;
                           ENU=Specifies the inventory posting group in order to assign the WIP to the correct general ledger account.];
                SourceExpr="Inventory Posting Group";
                Importance=Promoted;
                Editable=FALSE }

    { 83  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Produktbuchungsgruppe an, die den gefertigten Artikeln dieses Fertigungsauftrags zugeordnet ist.;
                           ENU=Specifies a product posting group associated with manufactured items in this production order.];
                SourceExpr="Gen. Prod. Posting Group";
                Editable=FALSE }

    { 85  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Gesch�ftsbuchungsgruppe an.;
                           ENU=Specifies a business posting group.];
                SourceExpr="Gen. Bus. Posting Group";
                Editable=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Dimension an, die mit dem Fertigungsauftrag verkn�pft ist.;
                           ENU=Specifies the code for the dimension associated with the production order.];
                SourceExpr="Shortcut Dimension 1 Code";
                Editable=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Dimension an, die mit dem Fertigungsauftrag verkn�pft ist.;
                           ENU=Specifies the code for the dimension associated with the production order.];
                SourceExpr="Shortcut Dimension 2 Code";
                Editable=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r den Lagerort an, an dem die zu produzierenden Artikel gelagert werden sollen.;
                           ENU=Specifies the location code to which you want to post the finished product from this production order.];
                SourceExpr="Location Code";
                Importance=Promoted;
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

