OBJECT Page 859 Cash Flow Manual Expenses
{
  OBJECT-PROPERTIES
  {
    Date=28.01.17;
    Time=12:00:00;
    Version List=NAVW110.00.00.15140;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Neutrale Cashflowausgaben;
               ENU=Cash Flow Manual Expenses];
    SourceTable=Table850;
    SourceTableView=SORTING(Starting Date)
                    ORDER(Ascending);
    PageType=List;
    OnAfterGetRecord=BEGIN
                       GetRecord;
                     END;

    OnNewRecord=BEGIN
                  InitNewRecord;
                END;

    OnInsertRecord=BEGIN
                     EnableControls;
                   END;

    OnDeleteRecord=BEGIN
                     CurrPage.SAVERECORD;
                   END;

    OnAfterGetCurrRecord=BEGIN
                           GetRecord;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1022    ;1   ;ActionGroup;
                      CaptionML=[DEU=&Ausgaben;
                                 ENU=&Expenses];
                      Image=ProjectExpense }
      { 1023    ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Anzeigen oder Bearbeiten von Dimensionen, wie etwa eines Bereichs, eines Projekts oder einer Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      ApplicationArea=#Suite;
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(850),
                                  No.=FIELD(Code);
                      Image=Dimensions }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1000;1   ;Group     ;
                GroupType=Repeater }

    { 1001;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Datensatzes an.;
                           ENU=Specifies the code of the record.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code;
                Visible=FALSE }

    { 1003;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Cashflowkontos an, in dem der Posten in der Zeile mit den neutralen Einnahmen registriert ist.;
                           ENU=Specifies the number of the cash flow account that the entry on the manual revenue line is registered to.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Cash Flow Account No.";
                Visible=FALSE }

    { 1005;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung des Cashflow-Planungspostens an.;
                           ENU=Specifies a description of the cash flow forecast entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 1007;2   ;Field     ;
                CaptionML=[DEU=Datum;
                           ENU=Date];
                ToolTipML=[DEU=Gibt das Datum des Cashflowpostens an.;
                           ENU=Specifies the date of the cash flow entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Starting Date";
                ShowMandatory=TRUE }

    { 1013;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtbetrag in MW an, aus dem die neutralen Ausgaben bestehen. Der eingegebene Betrag ist der Betrag, der innerhalb des angegebenen Zeitraums pro Wiederholungsh�ufigkeit registriert wird.;
                           ENU=Specifies the total amount in LCY that the manual expense consists of. The entered amount is the amount that is registered in the given time period per recurring frequency.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount }

    { 2   ;2   ;Field     ;
                CaptionML=[DEU=Wiederholung;
                           ENU=Recurrence];
                ToolTipML=[DEU=Gibt eine Datumsformel f�r die Berechnung der Periodenl�nge an. Der Inhalt des Felds bestimmt, wie oft der Posten in der Zeile der neutralen Ausgaben registriert ist. Beispiel: Falls die Zeile jeden Monat registriert werden muss, k�nnen Sie 1M eingeben.;
                           ENU=Specifies a date formula for calculating the period length. The content of the field determines how often the entry on the manual expense line is registered. For example, if the line must be registered every month, you can enter 1M.];
                OptionCaptionML=[DEU=" ,T�glich,W�chentlich,Monatlich,Viertelj�hrlich,J�hrlich";
                                 ENU=" ,Daily,Weekly,Monthly,Quarterly,Yearly"];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Recurrence;
                OnValidate=VAR
                             RecurringFrequency@1000 : Text;
                           BEGIN
                             RecurringFrequency := CashFlowManagement.RecurrenceToRecurringFrequency(Recurrence);
                             EVALUATE("Recurring Frequency",RecurringFrequency);
                             EnableControls;
                           END;
                            }

    { 1011;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie oft der Posten in der Zeile mit den neutralen Ausgaben registriert ist, falls f�r die Buch.-Blattvorlage "wiederkehrend" eingerichtet ist;
                           ENU=Specifies how often the entry on the manual expense line is registered, if the journal template used is set up to be recurring];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Recurring Frequency";
                Visible=FALSE;
                OnValidate=BEGIN
                             CashFlowManagement.RecurringFrequencyToRecurrence("Recurring Frequency",Recurrence);
                           END;
                            }

    { 1009;2   ;Field     ;
                CaptionML=[DEU=Beenden um;
                           ENU=End By];
                ToolTipML=[DEU=Gibt das letzte Datum an, ab dem die neutralen Ausgaben f�r die Cashflow-Planung registriert werden sollen.;
                           ENU=Specifies the last date from which manual expenses should be registered for the cash flow forecast.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Ending Date";
                Enabled=EndingDateEnabled }

    { 1015;2   ;Field     ;
                SourceExpr="Global Dimension 1 Code";
                Visible=FALSE }

    { 1017;2   ;Field     ;
                SourceExpr="Global Dimension 2 Code";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      CashFlowManagement@1004 : Codeunit 841;
      Recurrence@1000 : ' ,Daily,Weekly,Monthly,Quarterly,Yearly';
      EndingDateEnabled@1001 : Boolean;

    LOCAL PROCEDURE GetRecord@2();
    BEGIN
      EnableControls;
      CashFlowManagement.RecurringFrequencyToRecurrence("Recurring Frequency",Recurrence);
    END;

    LOCAL PROCEDURE EnableControls@3();
    BEGIN
      EndingDateEnabled := (Recurrence <> Recurrence::" ");
      IF NOT EndingDateEnabled THEN
        "Ending Date" := 0D;
    END;

    BEGIN
    END.
  }
}

