OBJECT Table 7360 Reservation Entry Buffer
{
  OBJECT-PROPERTIES
  {
    Date=07.09.12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Reservierungspostenpuffer;
               ENU=Reservation Entry Buffer];
  }
  FIELDS
  {
    { 4   ;   ;Quantity (Base)     ;Decimal       ;CaptionML=[DEU=Menge (Basis);
                                                              ENU=Quantity (Base)];
                                                   DecimalPlaces=0:5 }
    { 10  ;   ;Source Type         ;Integer       ;CaptionML=[DEU=Herkunftsart;
                                                              ENU=Source Type] }
    { 11  ;   ;Source Subtype      ;Option        ;CaptionML=[DEU=Herkunftsunterart;
                                                              ENU=Source Subtype];
                                                   OptionCaptionML=[DEU=0,1,2,3,4,5,6,7,8,9,10;
                                                                    ENU=0,1,2,3,4,5,6,7,8,9,10];
                                                   OptionString=0,1,2,3,4,5,6,7,8,9,10 }
    { 12  ;   ;Source ID           ;Code20        ;CaptionML=[DEU=Herkunfts-ID;
                                                              ENU=Source ID] }
    { 13  ;   ;Source Batch Name   ;Code10        ;CaptionML=[DEU=Herkunftsbuch.-Blattname;
                                                              ENU=Source Batch Name] }
    { 14  ;   ;Source Prod. Order Line;Integer    ;CaptionML=[DEU=Herkunfts-FA-Zeile;
                                                              ENU=Source Prod. Order Line] }
    { 15  ;   ;Source Ref. No.     ;Integer       ;CaptionML=[DEU=Herkunftsref.-Nr.;
                                                              ENU=Source Ref. No.] }
  }
  KEYS
  {
    {    ;Source Type,Source Subtype,Source ID,Source Batch Name,Source Prod. Order Line,Source Ref. No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

