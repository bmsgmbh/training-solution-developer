OBJECT Page 1607 Office Update Available Dlg
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Update f�r Office-Add-In verf�gbar;
               ENU=Office Add-in Update Available];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table1610;
    DataCaptionExpr='Office Add-in Update Available';
    SourceTableTemporary=Yes;
    OnInit=BEGIN
             UserCanUpdate := NOT IsAdminDeployed;
             UserCanContinue := NOT Breaking;
           END;

    OnQueryClosePage=BEGIN
                       IF DontShowAgain THEN BEGIN
                         IF UserCanUpdate THEN
                           MESSAGE(DontDisplayAgainMsg);
                         InstructionMgt.DisableMessageForCurrentUser(InstructionMgt.OfficeUpdateNotificationCode);
                       END;

                       IF Breaking THEN
                         EXIT(FALSE);
                     END;

  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                Name=Content;
                ContainerType=ContentArea }

    { 7   ;1   ;Field     ;
                Name=Empty;
                CaptionML=[DEU="";
                           ENU=""];
                ToolTipML=[DEU=Gibt Optionen zum Aktualisieren des Add-Ins an.;
                           ENU=Specifies options for updating the add-in.];
                ApplicationArea=#Basic,#Suite;
                Enabled=FALSE;
                Editable=FALSE;
                HideValue=TRUE;
                ShowCaption=No }

    { 5   ;1   ;Field     ;
                Name=AdminNonBreaking;
                CaptionML=[DEU=Ein Update ist f�r dieses Outlook-Add-In verf�gbar. Zum Aktualisieren des Add-ins wenden Sie sich an Ihren Systemadministrator.;
                           ENU=An update is available for this Outlook add-in. To update the add-in, please contact your system administrator.];
                ToolTipML=[DEU=Gibt eine Option zum Aktualisieren des Add-Ins an.;
                           ENU=Specifies an option for updating the add-in.];
                ApplicationArea=#Basic,#Suite;
                Visible=NOT UserCanUpdate AND UserCanContinue;
                HideValue=True }

    { 8   ;1   ;Field     ;
                Name=AdminBreaking;
                CaptionML=[DEU=Ein Update ist f�r dieses Outlook-Add-In verf�gbar. Zur weiteren Nutzung des Add-Ins wenden Sie sich an Ihren Systemadministrator.;
                           ENU=An update is available for this Outlook add-in. To continue using the add-in, please contact your system administrator.];
                ToolTipML=[DEU=Gibt eine Option zum Aktualisieren des Add-Ins an.;
                           ENU=Specifies an option for updating the add-in.];
                ApplicationArea=#Basic,#Suite;
                Visible=NOT UserCanUpdate AND NOT UserCanContinue }

    { 9   ;1   ;Field     ;
                Name=UserNonBreaking;
                CaptionML=[DEU=Ein Update ist f�r dieses Outlook-Add-In verf�gbar. M�chten Sie das Update jetzt ausf�hren?;
                           ENU=An update is available for this Outlook add-in. Do you want to apply the update now?];
                ToolTipML=[DEU=Gibt eine Option zum Aktualisieren des Add-Ins an.;
                           ENU=Specifies an option for updating the add-in.];
                ApplicationArea=#Basic,#Suite;
                Visible=UserCanContinue AND UserCanUpdate }

    { 10  ;1   ;Field     ;
                Name=UserBreaking;
                CaptionML=[DEU=Ein Update ist f�r dieses Outlook-Add-In verf�gbar. Zur weiteren Nutzung des Add-Ins m�ssen Sie das Update ausf�hren.;
                           ENU=An update is available for this Outlook add-in. To continue using the add-in, you must apply the update.];
                ToolTipML=[DEU=Gibt eine Option zum Aktualisieren des Add-Ins an.;
                           ENU=Specifies an option for updating the add-in.];
                ApplicationArea=#Basic,#Suite;
                Visible=NOT UserCanContinue AND UserCanUpdate }

    { 3   ;1   ;Field     ;
                Name=UpgradeNow;
                ToolTipML=[DEU=Legt fest, dass das Add-In jetzt aktualisiert werden muss.;
                           ENU=Specifies that the add-in must be updated now.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=UpgradeNowLbl;
                Visible=UserCanUpdate;
                Editable=False;
                OnDrillDown=BEGIN
                              IF ExchangeAddinSetup.PromptForCredentials THEN BEGIN
                                ExchangeAddinSetup.DeployAddin(Rec);
                                MESSAGE(RestartClientMsg);
                                CurrPage.CLOSE;
                              END;
                            END;

                ShowCaption=No }

    { 4   ;1   ;Field     ;
                Name=UpgradeLater;
                ToolTipML=[DEU=Gibt an, dass Sie das Add-In weiter verwenden und sp�ter aktualisieren m�chten.;
                           ENU=Specifies that you want to continue using the add-in and update it later.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=GetLaterLabel;
                Visible=UserCanContinue;
                Editable=False;
                OnDrillDown=BEGIN
                              CurrPage.CLOSE;
                            END;

                ShowCaption=No }

    { 6   ;1   ;Field     ;
                Name=DontShowAgain;
                CaptionML=[DEU=Diese Meldung nicht mehr anzeigen;
                           ENU=Do not show this message again];
                ToolTipML=[DEU=Gibt an, ob Sie diese Nachricht noch einmal sehen m�chten.;
                           ENU=Specifies if you want to not see this message again.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DontShowAgain;
                Visible=UserCanContinue }

  }
  CODE
  {
    VAR
      DontDisplayAgainMsg@1006 : TextConst 'DEU=Zur sp�teren Aktualisierung des Add-Ins m�ssen Sie die unterst�tzte Einrichtung f�r das Office-Add-In verwenden.;ENU=To update the add-in later, you must use the Office Add-In assisted setup guide.';
      RestartClientMsg@1009 : TextConst 'DEU=Das Add-In wurde aktualisiert. Schlie�en Sie Outlook und �ffnen Sie es wieder.;ENU=The add-in has been updated. Please close and reopen Outlook.';
      ContinueLbl@1014 : TextConst 'DEU=Fortfahren;ENU=Continue';
      UpgradeNowLbl@1003 : TextConst 'DEU=Upgrade jetzt durchf�hren;ENU=Upgrade Now';
      UpgradeLaterLbl@1004 : TextConst 'DEU=Upgrade sp�ter durchf�hren;ENU=Upgrade Later';
      ExchangeAddinSetup@1008 : Codeunit 5323;
      InstructionMgt@1007 : Codeunit 1330;
      DontShowAgain@1005 : Boolean;
      UserCanContinue@1011 : Boolean INDATASET;
      UserCanUpdate@1013 : Boolean;

    LOCAL PROCEDURE GetLaterLabel@38() : Text;
    BEGIN
      CASE TRUE OF
        UserCanContinue AND NOT UserCanUpdate:
          EXIT(ContinueLbl);
        ELSE
          EXIT(UpgradeLaterLbl);
      END;
    END;

    BEGIN
    END.
  }
}

