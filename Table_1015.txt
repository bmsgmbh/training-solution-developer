OBJECT Table 1015 Job Entry No.
{
  OBJECT-PROPERTIES
  {
    Date=24.11.16;
    Time=12:00:00;
    Version List=NAVW110.00.00.14199;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Projektpostennr.;
               ENU=Job Entry No.];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[DEU=Prim�rschl�ssel;
                                                              ENU=Primary Key];
                                                   Editable=No }
    { 2   ;   ;Entry No.           ;Integer       ;CaptionML=[DEU=Lfd. Nr.;
                                                              ENU=Entry No.];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

