OBJECT Page 5210 Causes of Absence
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Gr�nde Abwesenheit;
               ENU=Causes of Absence];
    SourceTable=Table5206;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code Abwesenheitsgrund an.;
                           ENU=Specifies a cause of absence code.];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung f�r den Abwesenheitsgrund an.;
                           ENU=Specifies a description for the cause of absence.];
                SourceExpr=Description }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gesamtanzahl der Abwesenheiten (in Tagen oder Stunden) f�r alle Mitarbeiter an.;
                           ENU=Specifies the total number of absences (calculated in days or hours) for all employees.];
                SourceExpr="Total Absence (Base)" }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Einheit an, die zur Berechnung der Abwesenheiten verwendet wird.;
                           ENU=Specifies the unit of measure to be used for calculating absences.];
                SourceExpr="Unit of Measure Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

