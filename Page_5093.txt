OBJECT Page 5093 Segment List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Segmente�bersicht;
               ENU=Segment List];
    SourceTable=Table5076;
    DataCaptionFields=Campaign No.,Salesperson Code;
    PageType=List;
    CardPageID=Segment;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Segment;
                                 ENU=&Segment];
                      Image=Segment }
      { 17      ;2   ;Action    ;
                      CaptionML=[DEU=Aufga&ben;
                                 ENU=T&o-dos];
                      ToolTipML=[DEU=Zeigt die Aufgaben an, die Verk�ufern oder Teams zugewiesen wurden. Aufgaben k�nnen mit Kontakten und/oder Kampagnen verkn�pft werden.;
                                 ENU=View the to-dos that have been assigned to salespeople or teams. To-dos can be linked to contacts and/or campaigns.];
                      RunObject=Page 5096;
                      RunPageView=SORTING(Segment No.);
                      RunPageLink=Segment No.=FIELD(No.);
                      Image=TaskList }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Segments an.;
                           ENU=Specifies the number of the segment.];
                ApplicationArea=#All;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des Segments an.;
                           ENU=Specifies the description of the segment.];
                ApplicationArea=#All;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Kampagne an, f�r die das Segment erstellt wurde.;
                           ENU=Specifies the number of the campaign for which the segment has been created.];
                SourceExpr="Campaign No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Verk�ufers an, der f�r dieses Segment und/oder die Aktivit�t verantwortlich ist.;
                           ENU=Specifies the code of the salesperson responsible for this segment and/or interaction.];
                SourceExpr="Salesperson Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem das Segment erstellt wurde.;
                           ENU=Specifies the date that the segment was created.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Date }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

