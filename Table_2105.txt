OBJECT Table 2105 O365 Payment History Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=O365-Zahlungsverlaufspuffer;
               ENU=O365 Payment History Buffer];
  }
  FIELDS
  {
    { 1   ;   ;Ledger Entry No.    ;Integer       ;TableRelation="G/L Entry";
                                                   CaptionML=[DEU=Postennr.;
                                                              ENU=Ledger Entry No.] }
    { 2   ;   ;Type                ;Option        ;CaptionML=[DEU=Typ;
                                                              ENU=Type];
                                                   OptionCaptionML=[DEU=" ,Zahlung,Rechnung,Gutschrift,Zinsrechnung,Mahnung,Erstattung";
                                                                    ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 3   ;   ;Amount              ;Decimal       ;CaptionML=[DEU=Betrag;
                                                              ENU=Amount] }
    { 4   ;   ;Date Received       ;Date          ;CaptionML=[DEU=Empfangsdatum;
                                                              ENU=Date Received] }
  }
  KEYS
  {
    {    ;Ledger Entry No.                        ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;Brick               ;Date Received,Type,Amount                }
  }
  CODE
  {
    VAR
      CanOnlyCancelPaymentsErr@1002 : TextConst 'DEU=Nur Zahlungen k�nnen als nicht bezahlt gekennzeichnet werden.;ENU=Only payments can be marked as unpaid.';
      CanOnlyCancelLastPaymentErr@1001 : TextConst 'DEU=Nur die letzte Zahlung kann als nicht bezahlt gekennzeichnet werden.;ENU=Only the last payment can be marked as unpaid.';
      DevMsgNotTemporaryErr@1000 : TextConst 'DEU=Diese Funktion kann nur verwendet werden, wenn es sich um einen tempor�ren Datensatz handelt.;ENU=This function can only be used when the record is temporary.';
      O365SalesInvoicePayment@1003 : Codeunit 2105;

    PROCEDURE FillPaymentHistory@1(SalesInvoiceDocumentNo@1000 : Code[20];IncludeSalesInvoice@1001 : Boolean);
    VAR
      InvoiceCustLedgerEntry@1002 : Record 21;
      PaymentCustLedgerEntry@1003 : Record 21;
    BEGIN
      IF NOT ISTEMPORARY THEN
        ERROR(DevMsgNotTemporaryErr);

      RESET;
      DELETEALL;
      InvoiceCustLedgerEntry.SETRANGE("Document Type",InvoiceCustLedgerEntry."Document Type"::Invoice);
      InvoiceCustLedgerEntry.SETRANGE("Document No.",SalesInvoiceDocumentNo);
      IF NOT InvoiceCustLedgerEntry.FINDFIRST THEN
        EXIT;

      IF IncludeSalesInvoice THEN
        CopyFromCustomerLedgerEntry(InvoiceCustLedgerEntry);

      IF PaymentCustLedgerEntry.GET(InvoiceCustLedgerEntry."Closed by Entry No.") THEN
        CopyFromCustomerLedgerEntry(PaymentCustLedgerEntry);

      PaymentCustLedgerEntry.SETCURRENTKEY("Closed by Entry No.");
      PaymentCustLedgerEntry.SETRANGE("Closed by Entry No.",InvoiceCustLedgerEntry."Entry No.");
      IF PaymentCustLedgerEntry.FINDSET THEN
        REPEAT
          CopyFromCustomerLedgerEntry(PaymentCustLedgerEntry);
        UNTIL PaymentCustLedgerEntry.NEXT = 0;
    END;

    LOCAL PROCEDURE CopyFromCustomerLedgerEntry@6(CustLedgerEntry@1000 : Record 21);
    BEGIN
      CustLedgerEntry.CALCFIELDS("Amount (LCY)");
      INIT;
      "Ledger Entry No." := CustLedgerEntry."Entry No.";
      Type := CustLedgerEntry."Document Type";
      Amount := CustLedgerEntry."Amount (LCY)";
      IF Type = Type::Payment THEN
        Amount := -Amount;
      "Date Received" := CustLedgerEntry."Posting Date";
      INSERT(TRUE);
    END;

    PROCEDURE CancelPayment@2();
    BEGIN
      IF Type <> Type::Payment THEN
        ERROR(CanOnlyCancelPaymentsErr);
      SETFILTER("Ledger Entry No.",'>%1',"Ledger Entry No.");
      IF NOT ISEMPTY THEN
        ERROR(CanOnlyCancelLastPaymentErr);
      O365SalesInvoicePayment.CancelCustLedgerEntry("Ledger Entry No.");
    END;

    BEGIN
    END.
  }
}

