OBJECT Page 5005276 Deliv. Reminder Ledger Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVDACH10.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Lieferanmahnungsposten;
               ENU=Deliv. Reminder Ledger Entries];
    SourceTable=Table5005274;
    DataCaptionExpr=CaptionString;
    PageType=List;
    OnFindRecord=BEGIN
                   CaptionString := '';

                   CurrentFilter := GETFILTER("Order No.");
                   IF CurrentFilter <> '' THEN
                     CaptionString :=
                       Text1140000 + CurrentFilter;

                   CurrentFilter := GETFILTER("Vendor No.");
                   IF CurrentFilter <> '' THEN BEGIN
                     IF CaptionString <> '' THEN
                       CaptionString := CaptionString + Text1140001;
                     CaptionString :=
                       CaptionString + ' ' + CurrentFilter;
                   END;

                   EXIT(FIND(Which));
                 END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1140036 ;1   ;Action    ;
                      CaptionML=[DEU=&Navigate;
                                 ENU=&Navigate];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate.SetDoc("Posting Date","Reminder No.");
                                 Navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1140000;1;Group     ;
                GroupType=Repeater }

    { 1140001;2;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Lieferanmahnung an.;
                           ENU=Specifies the number of the delivery reminder.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reminder No." }

    { 1140003;2;Field     ;
                ToolTipML=[DEU=Gibt die Nummer aus dem Nummernfeld der Lieferanmahnungszeile an.;
                           ENU=Specifies the number from the No. field on the delivery reminder line.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reminder Line No." }

    { 1140005;2;Field     ;
                ToolTipML=[DEU=Gibt die Mahnstufe in der Lieferanmahnungszeile an.;
                           ENU=Specifies the reminder level on the delivery reminder line.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reminder Level" }

    { 1140007;2;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der �berf�lligen Tage in der Lieferanmahnungszeile an.;
                           ENU=Specifies the day overdue on the delivery reminder line.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Days overdue" }

    { 1140009;2;Field     ;
                ToolTipML=[DEU=Legt die Kreditorennummer f�r den Lieferanmahnungsposten fest.;
                           ENU=Specifies the vendor number for the delivery reminder ledger entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Vendor No." }

    { 1140011;2;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der angemahnten Bestellung an.;
                           ENU=Specifies the number of the reminded purchase order.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Order No." }

    { 1140013;2;Field     ;
                ToolTipML=[DEU=Enth�lt die Nummer der angemahnten Bestellzeile.;
                           ENU=Contains the number of the reminded purchase order line.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Order Line No." }

    { 1140015;2;Field     ;
                ToolTipML=[DEU=Gibt die Postenart an.;
                           ENU=Specifies the entry type.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Type }

    { 1140017;2;Field     ;
                ToolTipML=[DEU=Gibt eine Nummer f�r den Artikel oder das Sachkonto an, die beim Buchen der Zeile verwendet wird.;
                           ENU=Specifies a number that identifies the item, or a number that identifies the G/L account, used when posting the line.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No." }

    { 1140019;2;Field     ;
                ToolTipML=[DEU=Gibt die angemahnte Menge an.;
                           ENU=Specifies the reminded quantity.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Quantity }

    { 1140021;2;Field     ;
                ToolTipML=[DEU=Gibt die Bestellmenge an.;
                           ENU=Specifies the reorder quantity.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reorder Quantity" }

    { 1140023;2;Field     ;
                ToolTipML=[DEU=Gibt die Restmenge an.;
                           ENU=Specifies the remaining quantity.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Remaining Quantity" }

    { 1140025;2;Field     ;
                ToolTipML=[DEU=Gibt das Buchungsdatum der Lieferanmahnung an.;
                           ENU=Specifies the posting date of the delivery reminder.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date" }

    { 1140027;2;Field     ;
                ToolTipML=[DEU=Gibt das Belegdatum der Lieferanmahnung an.;
                           ENU=Specifies the document date of the delivery reminder.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Date" }

    { 1140029;2;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der die Lieferanmahnung gebucht hat.;
                           ENU=Specifies the ID of the user who posted the delivery reminder.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User ID" }

    { 1140031;2;Field     ;
                ToolTipML=[DEU=Gibt eine fortlaufende Nummer f�r jeden neuen Posten an.;
                           ENU=Specifies a consecutive number for each new entry.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry No." }

  }
  CODE
  {
    VAR
      Text1140000@1140000 : TextConst 'DEU="Bestellung ";ENU="PurchOrder "';
      Text1140001@1140001 : TextConst 'DEU=" Debitor";ENU=" Customer"';
      Navigate@1140002 : Page 344;
      CaptionString@1140003 : Text[100];
      CurrentFilter@1140004 : Text[30];

    BEGIN
    END.
  }
}

