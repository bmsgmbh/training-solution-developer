OBJECT Page 791 G/L Accounts ListPart
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Sachkonten-ListPart;
               ENU=G/L Accounts ListPart];
    SourceTable=Table15;
    SourceTableView=WHERE(Account Type=CONST(Posting));
    PageType=ListPart;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Datensatzes an.;
                           ENU=Specifies the number of the record.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Datensatzes an.;
                           ENU=Specifies the name of the record.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 5   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob es sich bei dem Sachkonto um ein GuV-Konto oder ein Bilanzkonto handelt.;
                           ENU=Specifies is the general ledger account is an income statement account or a balance sheet account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Income/Balance" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

