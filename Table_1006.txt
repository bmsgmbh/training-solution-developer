OBJECT Table 1006 Job WIP Method
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               JobWIPEntry@1000 : Record 1004;
               JobWIPGLEntry@1001 : Record 1005;
               JobsSetup@1002 : Record 315;
             BEGIN
               IF "System Defined" THEN
                 ERROR(Text001,FIELDCAPTION("System Defined"));

               JobWIPEntry.SETRANGE("WIP Method Used",Code);
               JobWIPGLEntry.SETRANGE("WIP Method Used",Code);
               IF NOT (JobWIPEntry.ISEMPTY AND JobWIPGLEntry.ISEMPTY) THEN
                 ERROR(Text004,JobWIPEntry.TABLECAPTION,JobWIPGLEntry.TABLECAPTION);

               JobsSetup.SETRANGE("Default WIP Method",Code);
               IF NOT JobsSetup.ISEMPTY THEN
                 ERROR(Text006);
             END;

    CaptionML=[DEU=WIP-Methode f�r Projekt;
               ENU=Job WIP Method];
    LookupPageID=Page1010;
    DrillDownPageID=Page1010;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;OnValidate=BEGIN
                                                                ValidateModification;
                                                              END;

                                                   CaptionML=[DEU=Code;
                                                              ENU=Code];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;OnValidate=BEGIN
                                                                ValidateModification;
                                                              END;

                                                   CaptionML=[DEU=Beschreibung;
                                                              ENU=Description] }
    { 3   ;   ;WIP Cost            ;Boolean       ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                ValidateModification;
                                                                IF "Recognized Costs" <> "Recognized Costs"::"Usage (Total Cost)" THEN
                                                                  ERROR(Text003,FIELDCAPTION("Recognized Costs"),"Recognized Costs");
                                                              END;

                                                   CaptionML=[DEU=WIP-Kosten;
                                                              ENU=WIP Cost] }
    { 4   ;   ;WIP Sales           ;Boolean       ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                ValidateModification;
                                                                IF "Recognized Sales" <> "Recognized Sales"::"Contract (Invoiced Price)" THEN
                                                                  ERROR(Text003,FIELDCAPTION("Recognized Sales"),"Recognized Sales");
                                                              END;

                                                   CaptionML=[DEU=WIP-Verkauf;
                                                              ENU=WIP Sales] }
    { 5   ;   ;Recognized Costs    ;Option        ;OnValidate=BEGIN
                                                                ValidateModification;
                                                                IF "Recognized Costs" <> "Recognized Costs"::"Usage (Total Cost)" THEN
                                                                  "WIP Cost" := TRUE;
                                                              END;

                                                   CaptionML=[DEU=Deklarierte Kosten;
                                                              ENU=Recognized Costs];
                                                   OptionCaptionML=[DEU=Bei Abschluss,Vertriebskosten,Einstandswert,Vertrag (Einstandsbetrag fakturiert),Verbrauch (Einstandsbetrag);
                                                                    ENU=At Completion,Cost of Sales,Cost Value,Contract (Invoiced Cost),Usage (Total Cost)];
                                                   OptionString=At Completion,Cost of Sales,Cost Value,Contract (Invoiced Cost),Usage (Total Cost) }
    { 6   ;   ;Recognized Sales    ;Option        ;OnValidate=BEGIN
                                                                ValidateModification;
                                                                IF "Recognized Sales" <> "Recognized Sales"::"Contract (Invoiced Price)" THEN
                                                                  "WIP Sales" := TRUE;
                                                              END;

                                                   CaptionML=[DEU=Deklarierte Verk�ufe;
                                                              ENU=Recognized Sales];
                                                   OptionCaptionML=[DEU=Bei Abschluss,Vertrag (Preis fakturiert),Verbrauch (Einstandsbetrag),Verbrauch (Verkaufsbetrag),Prozentsatz der Fertigung,Verkaufswert;
                                                                    ENU=At Completion,Contract (Invoiced Price),Usage (Total Cost),Usage (Total Price),Percentage of Completion,Sales Value];
                                                   OptionString=At Completion,Contract (Invoiced Price),Usage (Total Cost),Usage (Total Price),Percentage of Completion,Sales Value }
    { 7   ;   ;Valid               ;Boolean       ;InitValue=Yes;
                                                   OnValidate=VAR
                                                                JobsSetup@1000 : Record 315;
                                                              BEGIN
                                                                JobsSetup.SETRANGE("Default WIP Method",Code);
                                                                IF NOT JobsSetup.ISEMPTY THEN
                                                                  ERROR(Text007,JobsSetup.FIELDCAPTION("Default WIP Method"));
                                                              END;

                                                   CaptionML=[DEU=G�ltig;
                                                              ENU=Valid] }
    { 8   ;   ;System Defined      ;Boolean       ;InitValue=No;
                                                   CaptionML=[DEU=Systemdefiniert;
                                                              ENU=System Defined];
                                                   Editable=No }
    { 9   ;   ;System-Defined Index;Integer       ;CaptionML=[DEU=Systemdefinierter Index;
                                                              ENU=System-Defined Index];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
    {    ;Valid                                    }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1000 : TextConst 'DEU=Methoden mit folgendem Status k�nnen nicht gel�scht werden: %1.;ENU=You cannot delete methods that are %1.';
      Text002@1001 : TextConst 'DEU=Methoden mit folgendem Status k�nnen nicht ge�ndert werden: %1.;ENU=You cannot modify methods that are %1.';
      Text003@1002 : TextConst 'DEU=Dieses Feld kann nicht ge�ndert werden, wenn ''%1'' auf ''%2'' festgelegt ist.;ENU=You cannot modify this field when %1 is %2.';
      Text004@1003 : TextConst 'DEU=Methoden mit Posten in ''%1'' oder ''%2'' k�nnen nicht gel�scht werden.;ENU=You cannot delete methods that have entries in %1 or %2.';
      Text005@1004 : TextConst 'DEU=Methoden mit Posten in ''%1'' oder ''%2'' k�nnen nicht ge�ndert werden.;ENU=You cannot modify methods that have entries in %1 or %2.';
      Text006@1005 : TextConst 'DEU=Die Standardmethode kann nicht gel�scht werden.;ENU=You cannot delete the default method.';
      Text007@1006 : TextConst 'DEU=Diese Methode muss g�ltig sein, da sie als %1 definiert ist.;ENU=This method must be valid because it is defined as the %1.';

    LOCAL PROCEDURE ValidateModification@3();
    VAR
      JobWIPEntry@1001 : Record 1004;
      JobWIPGLEntry@1000 : Record 1005;
    BEGIN
      IF "System Defined" THEN
        ERROR(Text002,FIELDCAPTION("System Defined"));
      JobWIPEntry.SETRANGE("WIP Method Used",Code);
      JobWIPGLEntry.SETRANGE("WIP Method Used",Code);
      IF NOT (JobWIPEntry.ISEMPTY AND JobWIPGLEntry.ISEMPTY) THEN
        ERROR(Text005,JobWIPEntry.TABLECAPTION,JobWIPGLEntry.TABLECAPTION);
    END;

    BEGIN
    END.
  }
}

