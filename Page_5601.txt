OBJECT Page 5601 Fixed Asset List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Anlagen�bersicht;
               ENU=Fixed Asset List];
    SourceTable=Table5600;
    PageType=List;
    CardPageID=Fixed Asset Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 30      ;1   ;ActionGroup;
                      CaptionML=[DEU=A&nlage;
                                 ENU=Fixed &Asset];
                      Image=FixedAssets }
      { 32      ;2   ;Action    ;
                      CaptionML=[DEU=&AfA-B�cher;
                                 ENU=Depreciation &Books];
                      ToolTipML=[DEU=Zum Anzeigen oder Bearbeiten der AfA-B�cher, die f�r die einzelnen Anlagen verwendet werden. Hier legen Sie auch fest, auf welche Art die Abschreibung berechnet wird.;
                                 ENU=View or edit the depreciation book or books that must be used for each of the fixed assets. Here you also specify the way depreciation must be calculated.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5619;
                      RunPageLink=FA No.=FIELD(No.);
                      Image=DepreciationBooks }
      { 46      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[DEU=Statistik;
                                 ENU=Statistics];
                      ToolTipML=[DEU=Zeigt detaillierte historische Informationen �ber die Anlage an.;
                                 ENU=View detailed historical information about the fixed asset.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5602;
                      RunPageLink=FA No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 49      ;2   ;ActionGroup;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      Image=Dimensions }
      { 41      ;3   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Zuordnung f�r aktuellen Datensatz;
                                 ENU=Dimensions-Single];
                      ToolTipML=[DEU=Zeigen Sie den f�r den ausgew�hlten Datensatz eingerichteten einzelnen Dimensionssatz an, oder bearbeiten Sie ihn.;
                                 ENU=View or edit the single set of dimensions that are set up for the selected record.];
                      ApplicationArea=#Suite;
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(5600),
                                  No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Dimensions;
                      PromotedCategory=Process }
      { 50      ;3   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      CaptionML=[DEU=Zuordnung f�r &markierte Datens�tze;
                                 ENU=Dimensions-&Multiple];
                      ToolTipML=[DEU=Zeigt oder bearbeitet Standarddimensionen f�r eine Gruppe von Datens�tzen. Sie k�nnen Transaktionen Dimensionscodes zuweisen, um die Kosten zu verteilen und historische Informationen zu analysieren.;
                                 ENU=View or edit dimensions for a group of records. You can assign dimension codes to transactions to distribute costs and analyze historical information.];
                      ApplicationArea=#Suite;
                      Promoted=Yes;
                      Image=DimensionSets;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 FA@1001 : Record 5600;
                                 DefaultDimMultiple@1002 : Page 542;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(FA);
                                 DefaultDimMultiple.SetMultiFA(FA);
                                 DefaultDimMultiple.RUNMODAL;
                               END;
                                }
      { 39      ;2   ;Action    ;
                      CaptionML=[DEU=Wartu&ngsposten;
                                 ENU=Main&tenance Ledger Entries];
                      ToolTipML=[DEU="Zeigt alle Wartungsposten f�r eine Anlage an. ";
                                 ENU="View all the maintenance ledger entries for a fixed asset. "];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5641;
                      RunPageView=SORTING(FA No.);
                      RunPageLink=FA No.=FIELD(No.);
                      Image=MaintenanceLedgerEntries }
      { 42      ;2   ;Action    ;
                      CaptionML=[DEU=Bild;
                                 ENU=Picture];
                      ToolTipML=[DEU=Zum Hinzuf�gen oder Anzeigen eines Bilds der Anlage.;
                                 ENU=Add or view a picture of the fixed asset.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5620;
                      RunPageLink=No.=FIELD(No.);
                      Image=Picture }
      { 48      ;2   ;Action    ;
                      CaptionML=[DEU=Anlagenbuchungsgruppen �bers.;
                                 ENU=FA Posting Types Overview];
                      ToolTipML=[DEU=Zeigt die Gesamtbetr�ge f�r jedes Feld wie etwa Buchwert, Anschaffungskosten und Abschreibung sowie f�r alle Anlagen an. Bei jeder Anlage wird f�r jedes verkn�pfte AfA-Buch eine separate Zeile angezeigt.;
                                 ENU=View accumulated amounts for each field, such as book value, acquisition cost, and depreciation, and for each fixed asset. For every fixed asset, a separate line is shown for each depreciation book linked to the asset.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5662;
                      Promoted=Yes;
                      Image=ShowMatrix;
                      PromotedCategory=Process }
      { 40      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkungen;
                                 ENU=Co&mments];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(Fixed Asset),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 3       ;1   ;ActionGroup;
                      CaptionML=[DEU=Hauptanlage;
                                 ENU=Main Asset];
                      Image=Components }
      { 44      ;2   ;Action    ;
                      CaptionML=[DEU=&Unteranlagen;
                                 ENU=M&ain Asset Components];
                      ToolTipML=[DEU=Zeigen Sie die Anlagenkomponenten der Hauptkomponente an, die durch die Anlagenkarte dargestellt wird, oder bearbeiten Sie diese.;
                                 ENU=View or edit fixed asset components of the main fixed asset that is represented by the fixed asset card.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5658;
                      RunPageLink=Main Asset No.=FIELD(No.);
                      Image=Components }
      { 47      ;2   ;Action    ;
                      CaptionML=[DEU=Hauptanlagenstat&istik;
                                 ENU=Ma&in Asset Statistics];
                      ToolTipML=[DEU=Zeigt detaillierte historische Informationen �ber alle Komponenten an, aus denen die Hauptanlage besteht.;
                                 ENU=View detailed historical information about all the components that make up the main asset.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5603;
                      RunPageLink=FA No.=FIELD(No.);
                      Image=StatisticsDocument }
      { 45      ;2   ;Separator ;
                      CaptionML=[DEU="";
                                 ENU=""] }
      { 5       ;1   ;ActionGroup;
                      CaptionML=[DEU=Historie;
                                 ENU=History];
                      Image=History }
      { 37      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=&Posten;
                                 ENU=Ledger E&ntries];
                      ToolTipML=[DEU=Zeigt detaillierte Informationen zu f�r die Anlage get�tigten Transaktionen an.;
                                 ENU=View detailed information about transactions made for the fixed asset.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5604;
                      RunPageView=SORTING(FA No.)
                                  ORDER(Descending);
                      RunPageLink=FA No.=FIELD(No.);
                      Image=FixedAssetLedger }
      { 38      ;2   ;Action    ;
                      CaptionML=[DEU=Stornoposten;
                                 ENU=Error Ledger Entries];
                      ToolTipML=[DEU=Zeigt die Posten an, die gebucht wurden, nachdem die Abbrechen-Funktion zum Abbrechen eines Postens verwendet wurde.;
                                 ENU=View the entries that have been posted as a result of you using the Cancel function to cancel an entry.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5605;
                      RunPageView=SORTING(Canceled from FA No.)
                                  ORDER(Descending);
                      RunPageLink=Canceled from FA No.=FIELD(No.);
                      Image=ErrorFALedgerEntries }
      { 43      ;2   ;Action    ;
                      CaptionML=[DEU=&Wartungsregistrierung;
                                 ENU=Maintenance &Registration];
                      ToolTipML=[DEU=Zum Anzeigen oder Bearbeiten von Wartungscodes f�r verschiedene Arten von Wartung, Reparaturen und Services in Bezug auf Ihre Anlagen. Sie k�nnen den Code in das Feld "Wartungscode" auf Buch.-Bl�ttern eingeben.;
                                 ENU=View or edit maintenance codes for the various types of maintenance, repairs, and services performed on your fixed assets. You can then enter the code in the Maintenance Code field on journals.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5625;
                      RunPageLink=FA No.=FIELD(No.);
                      Image=MaintenanceRegistrations }
      { 7       ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;Action    ;
                      CaptionML=[DEU=Anlagen Buch.-Blatt;
                                 ENU=Fixed Asset Journal];
                      ToolTipML=[DEU="Bucht Anlagentransaktionen mit einem AfA-Buch, das nicht in die Finanzbuchhaltung integriert ist, f�r die interne Verwaltung. Nur Anlagenposten werden erstellt. ";
                                 ENU="Post fixed asset transactions with a depreciation book that is not integrated with the general ledger, for internal management. Only fixed asset ledger entries are created. "];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5629;
                      Image=Journal }
      { 10      ;1   ;Action    ;
                      CaptionML=[DEU=Anlagen-Fibu Buch.-Blatt;
                                 ENU=Fixed Asset G/L Journal];
                      ToolTipML=[DEU="Bucht Anlagentransaktionen mit einem AfA-Buch, das nicht in die Finanzbuchhaltung integriert ist, f�r Finanzberichte. Sowohl Anlagenposten als auch Sachposten werden erstellt. ";
                                 ENU="Post fixed asset transactions with a depreciation book that is integrated with the general ledger, for financial reporting. Both fixed asset ledger entries are general ledger entries are created. "];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5628;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Journal;
                      PromotedCategory=Process }
      { 61      ;1   ;Action    ;
                      CaptionML=[DEU=Anlagen Umbuch.-Blatt;
                                 ENU=Fixed Asset Reclassification Journal];
                      ToolTipML=[DEU=�bertragen, Teilen oder Kombinieren von Anlagen.;
                                 ENU=Transfer, split, or combine fixed assets.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5636;
                      Image=Journal }
      { 60      ;1   ;Action    ;
                      CaptionML=[DEU=Wiederk. Anl. Buch.-Bl.;
                                 ENU=Recurring Fixed Asset Journal];
                      ToolTipML=[DEU=Bucht wiederkehrende Posten auf ein AfA-Buch ohne eine Integration in die Finanzbuchhaltung.;
                                 ENU=Post recurring entries to a depreciation book without integration with general ledger.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5634;
                      Image=Journal }
      { 11      ;1   ;Action    ;
                      Name=CalculateDepreciation;
                      Ellipsis=Yes;
                      CaptionML=[DEU=AfA berechnen;
                                 ENU=Calculate Depreciation];
                      ToolTipML=[DEU=Berechnet die Abschreibung unter Ber�cksichtigung der von Ihnen festgelegten Bedingungen. Falls das verkn�pfte Abschreibungsbuch so eingerichtet ist, dass es in die Finanzbuchhaltung integriert wird, werden die berechneten Eintr�ge an das Anlagen Fibu Buch.-Blatt �bertragen. Andernfalls werden die berechneten Posten an das Anlagen Buch.-Blatt �bertragen. Sie k�nnen dann die Posten �berpr�fen und das Buch.-Blatt buchen.;
                                 ENU=Calculate depreciation according to conditions that you specify. If the related depreciation book is set up to integrate with the general ledger, then the calculated entries are transferred to the fixed asset general ledger journal. Otherwise, the calculated entries are transferred to the fixed asset journal. You can then review the entries and post the journal.];
                      ApplicationArea=#FixedAssets;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CalculateDepreciation;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 REPORT.RUNMODAL(REPORT::"Calculate Depreciation",TRUE,FALSE,Rec);
                               END;
                                }
      { 13      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Anlage k&opieren;
                                 ENU=C&opy Fixed Asset];
                      ToolTipML=[DEU=Erstellen Sie mindestens eine neue Anlage, indem Sie eine vorhandene Anlage mit �hnlichen Informationen kopieren.;
                                 ENU=Create one or more new fixed assets by copying from an existing fixed asset that has similar information.];
                      ApplicationArea=#FixedAssets;
                      Image=CopyFixedAssets;
                      OnAction=VAR
                                 CopyFA@1000 : Report 5685;
                               BEGIN
                                 CopyFA.SetFANo("No.");
                                 CopyFA.RUNMODAL;
                               END;
                                }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1907091306;1 ;Action    ;
                      CaptionML=[DEU=Anlagen - Liste;
                                 ENU=Fixed Assets List];
                      ToolTipML=[DEU=Zeigt eine Liste der im System vorhandenen Anlagen an.;
                                 ENU=View the list of fixed assets that exist in the system .];
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5601;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1903109606;1 ;Action    ;
                      CaptionML=[DEU=Anschaffungs�bersicht;
                                 ENU=Acquisition List];
                      ToolTipML=[DEU=Zeigt die verkn�pften Anschaffungen an.;
                                 ENU=View the related acquisitions.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5608;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1901902606;1 ;Action    ;
                      CaptionML=[DEU=Details;
                                 ENU=Details];
                      ToolTipML=[DEU=Zeigt detaillierte Informationen zu den Anlagenposten an, die f�r jede Anlage auf ein bestimmtes AfA-Buch gebucht wurden.;
                                 ENU=View detailed information about the fixed asset ledger entries that have been posted to a specified depreciation book for each fixed asset.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5604;
                      Promoted=Yes;
                      Image=View;
                      PromotedCategory=Report }
      { 1905598506;1 ;Action    ;
                      CaptionML=[DEU=Anlagenspiegel;
                                 ENU=FA Book Value];
                      ToolTipML=[DEU=Zeigt detaillierte Informationen �ber Anschaffungskosten, Abschreibungen und Buchwerte sowohl f�r einzelne Anlagen als auch f�r Gruppen von Anlagen an. F�r jede dieser drei Betragsarten werden die Betr�ge zu Beginn und am Ende der Periode sowie w�hrend der Periode selbst berechnet.;
                                 ENU=View detailed information about acquisition cost, depreciation and book value for both individual assets and groups of assets. For each of these three amount types, amounts are calculated at the beginning and at the end of a specified period as well as for the period itself.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5605;
                      Image=Report }
      { 1905598606;1 ;Action    ;
                      CaptionML=[DEU=Anlagenspiegel - Zuschr. & erh�hte AfA;
                                 ENU=FA Book Val. - Appr. & Write-D];
                      ToolTipML=[DEU=Zeigt detaillierte Informationen �ber die Anschaffungskosten, Abschreibungen, Zuschreibung, erh�hte Abschreibung und Buchwerte obwohl f�r einzelne Anlagen als auch f�r Gruppen von Anlagen an. F�r jede der Kategorien werden die Betr�ge zu Beginn und am Ende der Periode sowie w�hrend der Periode selbst berechnet.;
                                 ENU=View detailed information about acquisition cost, depreciation, appreciation, write-down and book value for both individual assets and groups of assets. For each of these categories, amounts are calculated at the beginning and at the end of a specified period, as well as for the period itself.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5606;
                      Image=Report }
      { 1901105406;1 ;Action    ;
                      CaptionML=[DEU=Analyseansicht;
                                 ENU=Analysis];
                      ToolTipML=[DEU=Zeigt eine Analyse Ihrer Anlagen f�r verschiedene Arten von Daten (sowohl f�r individuelle Anlagen als auch f�r Gruppen von Anlagen) an.;
                                 ENU=View an analysis of your fixed assets with various types of data for both individual assets and groups of fixed assets.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5600;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1902048606;1 ;Action    ;
                      CaptionML=[DEU=Vorschau;
                                 ENU=Projected Value];
                      ToolTipML=[DEU=Zeigt die berechnete zuk�nftige Abschreibung und den Buchwert an. Sie k�nnen den Bericht f�r jeweils ein AfA-Buch anzeigen.;
                                 ENU=View the calculated future depreciation and book value. You can print the report for one depreciation book at a time.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5607;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1903345906;1 ;Action    ;
                      CaptionML=[DEU=Fibu-Analyse;
                                 ENU=G/L Analysis];
                      ToolTipML=[DEU=Zeigt eine Analyse Ihrer Anlagen f�r verschiedene Arten von Daten (f�r individuelle Anlagen und/oder f�r Gruppen von Anlagen) an.;
                                 ENU=View an analysis of your fixed assets with various types of data for individual assets and/or groups of fixed assets.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5610;
                      Image=Report }
      { 1903807106;1 ;Action    ;
                      CaptionML=[DEU=Journal;
                                 ENU=Register];
                      ToolTipML=[DEU=Zeigt Journale an, die alle erstellten Anlagenposten enthalten. Jedes Journal zeigt die erste und die letzte Nummer der Posten an.;
                                 ENU=View registers containing all the fixed asset entries that are created. Each register shows the first and last entry number of its entries.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5603;
                      Image=Confirm }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Nummer f�r die Anlage an.;
                           ENU=Specifies a number for the fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Anlage an.;
                           ENU=Specifies a description of the fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr=Description }

    { 23  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kreditors an, von dem Sie die Anlage gekauft haben.;
                           ENU=Specifies the number of the vendor from which you purchased this fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Vendor No.";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kreditors an, der Reparaturen und Wartungen an der Anlage durchf�hrt.;
                           ENU=Specifies the number of the vendor who performs repairs and maintenance on the fixed asset.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Maintenance Vendor No.";
                Visible=FALSE }

    { 35  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, welcher Mitarbeiter f�r die Anlage verantwortlich ist.;
                           ENU=Specifies which employee is responsible for the fixed asset.];
                SourceExpr="Responsible Employee" }

    { 25  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Klasse an, zu der die Anlage geh�rt.;
                           ENU=Specifies the class that the fixed asset belongs to.];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Class Code" }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachgruppe der Klasse an, zu der die Anlage geh�rt.;
                           ENU=Specifies the subclass of the class that the fixed asset belongs to.];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Subclass Code" }

    { 33  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ort an (z.�B. ein Geb�ude), in dem sich die Anlage befindet.;
                           ENU=Specifies the location, such as a building, where the fixed asset is located.];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Location Code" }

    { 17  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Anlage f�r die Budgetierung verwendet werden soll.;
                           ENU=Specifies if the asset is for budgeting purposes.];
                ApplicationArea=#Suite;
                SourceExpr="Budgeted Asset";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Suchbegriff f�r die Anlage an.;
                           ENU=Specifies a search description for the fixed asset.];
                SourceExpr="Search Description" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass die Anlage erworben wurde.;
                           ENU=Specifies that the fixed asset has been acquired.];
                ApplicationArea=#FixedAssets;
                SourceExpr=Acquired }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

