OBJECT Table 5942 Service Item Log
{
  OBJECT-PROPERTIES
  {
    Date=07.09.12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Service Item No.;
    OnInsert=BEGIN
               ServItemLog.LOCKTABLE;
               ServItemLog.RESET;
               ServItemLog.SETRANGE("Service Item No.","Service Item No.");
               IF ServItemLog.FINDLAST THEN
                 "Entry No." := ServItemLog."Entry No." + 1
               ELSE
                 "Entry No." := 1;

               "Change Date" := TODAY;
               "Change Time" := TIME;
               "User ID" := USERID;
             END;

    CaptionML=[DEU=Serviceartikelprotokoll;
               ENU=Service Item Log];
    LookupPageID=Page5989;
    DrillDownPageID=Page5989;
  }
  FIELDS
  {
    { 1   ;   ;Service Item No.    ;Code20        ;TableRelation="Service Item";
                                                   CaptionML=[DEU=Serviceartikelnr.;
                                                              ENU=Service Item No.];
                                                   NotBlank=Yes }
    { 2   ;   ;Entry No.           ;Integer       ;CaptionML=[DEU=Lfd. Nr.;
                                                              ENU=Entry No.] }
    { 3   ;   ;Event No.           ;Integer       ;CaptionML=[DEU=Ereignisnr.;
                                                              ENU=Event No.] }
    { 4   ;   ;Document No.        ;Code20        ;TableRelation=IF (Document Type=CONST(Quote)) "Service Header".No. WHERE (Document Type=CONST(Quote))
                                                                 ELSE IF (Document Type=CONST(Order)) "Service Header".No. WHERE (Document Type=CONST(Order))
                                                                 ELSE IF (Document Type=CONST(Contract)) "Service Contract Header"."Contract No." WHERE (Contract Type=CONST(Contract));
                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Belegnr.;
                                                              ENU=Document No.] }
    { 5   ;   ;After               ;Text50        ;CaptionML=[DEU=Neuer Inhalt;
                                                              ENU=After] }
    { 6   ;   ;Before              ;Text50        ;CaptionML=[DEU=Alter Inhalt;
                                                              ENU=Before] }
    { 7   ;   ;Change Date         ;Date          ;CaptionML=[DEU=Korrigiert am;
                                                              ENU=Change Date] }
    { 8   ;   ;Change Time         ;Time          ;CaptionML=[DEU=Korrigiert um;
                                                              ENU=Change Time] }
    { 9   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Benutzer-ID;
                                                              ENU=User ID] }
    { 10  ;   ;Document Type       ;Option        ;CaptionML=[DEU=Belegart;
                                                              ENU=Document Type];
                                                   OptionCaptionML=[DEU=" ,Angebot,Auftrag,Vertrag";
                                                                    ENU=" ,Quote,Order,Contract"];
                                                   OptionString=[ ,Quote,Order,Contract] }
  }
  KEYS
  {
    {    ;Service Item No.,Entry No.              ;Clustered=Yes }
    {    ;Change Date                              }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ServItemLog@1000 : Record 5942;

    BEGIN
    END.
  }
}

