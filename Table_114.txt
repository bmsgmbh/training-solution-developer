OBJECT Table 114 Sales Cr.Memo Header
{
  OBJECT-PROPERTIES
  {
    Date=23.02.17;
    Time=12:00:00;
    Version List=NAVW110.00.00.15601;
  }
  PROPERTIES
  {
    DataCaptionFields=No.,Sell-to Customer Name;
    OnDelete=VAR
               PostedDeferralHeader@1002 : Record 1704;
               PostSalesDelete@1000 : Codeunit 363;
               DeferralUtilities@1001 : Codeunit 1720;
             BEGIN
               PostSalesDelete.IsDocumentDeletionAllowed("Posting Date");
               TESTFIELD("No. Printed");
               LOCKTABLE;
               PostSalesDelete.DeleteSalesCrMemoLines(Rec);

               SalesCommentLine.SETRANGE("Document Type",SalesCommentLine."Document Type"::"Posted Credit Memo");
               SalesCommentLine.SETRANGE("No.","No.");
               SalesCommentLine.DELETEALL;

               ApprovalsMgmt.DeletePostedApprovalEntries(RECORDID);
               PostedDeferralHeader.DeleteForDoc(DeferralUtilities.GetSalesDeferralDocType,'','',
                 SalesCommentLine."Document Type"::"Posted Credit Memo","No.");
             END;

    CaptionML=[DEU=Verkaufsgutschriftskopf;
               ENU=Sales Cr.Memo Header];
    LookupPageID=Page144;
    DrillDownPageID=Page144;
  }
  FIELDS
  {
    { 2   ;   ;Sell-to Customer No.;Code20        ;TableRelation=Customer;
                                                   CaptionML=[DEU=Verk. an Deb.-Nr.;
                                                              ENU=Sell-to Customer No.];
                                                   NotBlank=Yes }
    { 3   ;   ;No.                 ;Code20        ;CaptionML=[DEU=Nr.;
                                                              ENU=No.] }
    { 4   ;   ;Bill-to Customer No.;Code20        ;TableRelation=Customer;
                                                   CaptionML=[DEU=Rech. an Deb.-Nr.;
                                                              ENU=Bill-to Customer No.];
                                                   NotBlank=Yes }
    { 5   ;   ;Bill-to Name        ;Text50        ;CaptionML=[DEU=Rech. an Name;
                                                              ENU=Bill-to Name] }
    { 6   ;   ;Bill-to Name 2      ;Text50        ;CaptionML=[DEU=Rech. an Name 2;
                                                              ENU=Bill-to Name 2] }
    { 7   ;   ;Bill-to Address     ;Text50        ;CaptionML=[DEU=Rech. an Adresse;
                                                              ENU=Bill-to Address] }
    { 8   ;   ;Bill-to Address 2   ;Text50        ;CaptionML=[DEU=Rech. an Adresse 2;
                                                              ENU=Bill-to Address 2] }
    { 9   ;   ;Bill-to City        ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Rech. an Ort;
                                                              ENU=Bill-to City] }
    { 10  ;   ;Bill-to Contact     ;Text50        ;CaptionML=[DEU=Rech. an Kontakt;
                                                              ENU=Bill-to Contact] }
    { 11  ;   ;Your Reference      ;Text35        ;CaptionML=[DEU=Ihre Referenz;
                                                              ENU=Your Reference] }
    { 12  ;   ;Ship-to Code        ;Code10        ;TableRelation="Ship-to Address".Code WHERE (Customer No.=FIELD(Sell-to Customer No.));
                                                   CaptionML=[DEU=Lief. an Code;
                                                              ENU=Ship-to Code] }
    { 13  ;   ;Ship-to Name        ;Text50        ;CaptionML=[DEU=Lief. an Name;
                                                              ENU=Ship-to Name] }
    { 14  ;   ;Ship-to Name 2      ;Text50        ;CaptionML=[DEU=Lief. an Name 2;
                                                              ENU=Ship-to Name 2] }
    { 15  ;   ;Ship-to Address     ;Text50        ;CaptionML=[DEU=Lief. an Adresse;
                                                              ENU=Ship-to Address] }
    { 16  ;   ;Ship-to Address 2   ;Text50        ;CaptionML=[DEU=Lief. an Adresse 2;
                                                              ENU=Ship-to Address 2] }
    { 17  ;   ;Ship-to City        ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Lief. an Ort;
                                                              ENU=Ship-to City] }
    { 18  ;   ;Ship-to Contact     ;Text50        ;CaptionML=[DEU=Lief. an Kontakt;
                                                              ENU=Ship-to Contact] }
    { 20  ;   ;Posting Date        ;Date          ;CaptionML=[DEU=Buchungsdatum;
                                                              ENU=Posting Date] }
    { 21  ;   ;Shipment Date       ;Date          ;CaptionML=[DEU=Warenausg.-Datum;
                                                              ENU=Shipment Date] }
    { 22  ;   ;Posting Description ;Text50        ;CaptionML=[DEU=Buchungsbeschreibung;
                                                              ENU=Posting Description] }
    { 23  ;   ;Payment Terms Code  ;Code10        ;TableRelation="Payment Terms";
                                                   CaptionML=[DEU=Zlg.-Bedingungscode;
                                                              ENU=Payment Terms Code] }
    { 24  ;   ;Due Date            ;Date          ;CaptionML=[DEU=F�lligkeitsdatum;
                                                              ENU=Due Date] }
    { 25  ;   ;Payment Discount %  ;Decimal       ;CaptionML=[DEU=Skonto %;
                                                              ENU=Payment Discount %];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 26  ;   ;Pmt. Discount Date  ;Date          ;CaptionML=[DEU=Skontodatum;
                                                              ENU=Pmt. Discount Date] }
    { 27  ;   ;Shipment Method Code;Code10        ;TableRelation="Shipment Method";
                                                   CaptionML=[DEU=Lieferbedingungscode;
                                                              ENU=Shipment Method Code] }
    { 28  ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[DEU=Lagerortcode;
                                                              ENU=Location Code] }
    { 29  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[DEU=Shortcutdimensionscode 1;
                                                              ENU=Shortcut Dimension 1 Code];
                                                   CaptionClass='1,2,1' }
    { 30  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[DEU=Shortcutdimensionscode 2;
                                                              ENU=Shortcut Dimension 2 Code];
                                                   CaptionClass='1,2,2' }
    { 31  ;   ;Customer Posting Group;Code10      ;TableRelation="Customer Posting Group";
                                                   CaptionML=[DEU=Debitorenbuchungsgruppe;
                                                              ENU=Customer Posting Group];
                                                   Editable=No }
    { 32  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[DEU=W�hrungscode;
                                                              ENU=Currency Code];
                                                   Editable=No }
    { 33  ;   ;Currency Factor     ;Decimal       ;CaptionML=[DEU=W�hrungsfaktor;
                                                              ENU=Currency Factor];
                                                   DecimalPlaces=0:15;
                                                   MinValue=0 }
    { 34  ;   ;Customer Price Group;Code10        ;TableRelation="Customer Price Group";
                                                   CaptionML=[DEU=Debitorenpreisgruppe;
                                                              ENU=Customer Price Group] }
    { 35  ;   ;Prices Including VAT;Boolean       ;CaptionML=[DEU=Preise inkl. MwSt.;
                                                              ENU=Prices Including VAT] }
    { 37  ;   ;Invoice Disc. Code  ;Code20        ;CaptionML=[DEU=Rechnungsrabattcode;
                                                              ENU=Invoice Disc. Code] }
    { 40  ;   ;Customer Disc. Group;Code20        ;TableRelation="Customer Discount Group";
                                                   CaptionML=[DEU=Debitorenrabattgruppe;
                                                              ENU=Customer Disc. Group] }
    { 41  ;   ;Language Code       ;Code10        ;TableRelation=Language;
                                                   CaptionML=[DEU=Sprachcode;
                                                              ENU=Language Code] }
    { 43  ;   ;Salesperson Code    ;Code10        ;TableRelation=Salesperson/Purchaser;
                                                   CaptionML=[DEU=Verk�ufercode;
                                                              ENU=Salesperson Code] }
    { 46  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Sales Comment Line" WHERE (Document Type=CONST(Posted Credit Memo),
                                                                                                 No.=FIELD(No.),
                                                                                                 Document Line No.=CONST(0)));
                                                   CaptionML=[DEU=Bemerkung;
                                                              ENU=Comment];
                                                   Editable=No }
    { 47  ;   ;No. Printed         ;Integer       ;CaptionML=[DEU=Anzahl gedruckt;
                                                              ENU=No. Printed];
                                                   Editable=No }
    { 51  ;   ;On Hold             ;Code3         ;CaptionML=[DEU=Abwarten;
                                                              ENU=On Hold] }
    { 52  ;   ;Applies-to Doc. Type;Option        ;CaptionML=[DEU=Ausgleich mit Belegart;
                                                              ENU=Applies-to Doc. Type];
                                                   OptionCaptionML=[DEU=" ,Zahlung,Rechnung,Gutschrift,Zinsrechnung,Mahnung,Erstattung";
                                                                    ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 53  ;   ;Applies-to Doc. No. ;Code20        ;OnLookup=BEGIN
                                                              CustLedgEntry.SETCURRENTKEY("Document No.");
                                                              CustLedgEntry.SETRANGE("Document Type","Applies-to Doc. Type");
                                                              CustLedgEntry.SETRANGE("Document No.","Applies-to Doc. No.");
                                                              PAGE.RUN(0,CustLedgEntry);
                                                            END;

                                                   CaptionML=[DEU=Ausgleich mit Belegnr.;
                                                              ENU=Applies-to Doc. No.] }
    { 55  ;   ;Bal. Account No.    ;Code20        ;TableRelation=IF (Bal. Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Bal. Account Type=CONST(Bank Account)) "Bank Account";
                                                   CaptionML=[DEU=Gegenkontonr.;
                                                              ENU=Bal. Account No.] }
    { 60  ;   ;Amount              ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Sales Cr.Memo Line".Amount WHERE (Document No.=FIELD(No.)));
                                                   CaptionML=[DEU=Betrag;
                                                              ENU=Amount];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 61  ;   ;Amount Including VAT;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Sales Cr.Memo Line"."Amount Including VAT" WHERE (Document No.=FIELD(No.)));
                                                   CaptionML=[DEU=Betrag inkl. MwSt.;
                                                              ENU=Amount Including VAT];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 70  ;   ;VAT Registration No.;Text20        ;CaptionML=[DEU=USt-IdNr.;
                                                              ENU=VAT Registration No.] }
    { 73  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[DEU=Ursachencode;
                                                              ENU=Reason Code] }
    { 74  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[DEU=Gesch�ftsbuchungsgruppe;
                                                              ENU=Gen. Bus. Posting Group] }
    { 75  ;   ;EU 3-Party Trade    ;Boolean       ;CaptionML=[DEU=EU-Dreiecksgesch�ft;
                                                              ENU=EU 3-Party Trade] }
    { 76  ;   ;Transaction Type    ;Code10        ;TableRelation="Transaction Type";
                                                   CaptionML=[DEU=Art des Gesch�ftes;
                                                              ENU=Transaction Type] }
    { 77  ;   ;Transport Method    ;Code10        ;TableRelation="Transport Method";
                                                   CaptionML=[DEU=Verkehrszweig;
                                                              ENU=Transport Method] }
    { 78  ;   ;VAT Country/Region Code;Code10     ;TableRelation=Country/Region;
                                                   CaptionML=[DEU=MwSt.-L�nder-/Regionscode;
                                                              ENU=VAT Country/Region Code] }
    { 79  ;   ;Sell-to Customer Name;Text50       ;CaptionML=[DEU=Verk. an Name;
                                                              ENU=Sell-to Customer Name] }
    { 80  ;   ;Sell-to Customer Name 2;Text50     ;CaptionML=[DEU=Verk. an Name 2;
                                                              ENU=Sell-to Customer Name 2] }
    { 81  ;   ;Sell-to Address     ;Text50        ;CaptionML=[DEU=Verk. an Adresse;
                                                              ENU=Sell-to Address] }
    { 82  ;   ;Sell-to Address 2   ;Text50        ;CaptionML=[DEU=Verk. an Adresse 2;
                                                              ENU=Sell-to Address 2] }
    { 83  ;   ;Sell-to City        ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Verk. an Ort;
                                                              ENU=Sell-to City] }
    { 84  ;   ;Sell-to Contact     ;Text50        ;CaptionML=[DEU=Verk. an Kontakt;
                                                              ENU=Sell-to Contact] }
    { 85  ;   ;Bill-to Post Code   ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Rech. an PLZ-Code;
                                                              ENU=Bill-to Post Code] }
    { 86  ;   ;Bill-to County      ;Text30        ;CaptionML=[DEU=Rech. an Bundesregion;
                                                              ENU=Bill-to County] }
    { 87  ;   ;Bill-to Country/Region Code;Code10 ;TableRelation=Country/Region;
                                                   CaptionML=[DEU=Rech. an L�nder-/Regionscode;
                                                              ENU=Bill-to Country/Region Code] }
    { 88  ;   ;Sell-to Post Code   ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Verk. an PLZ-Code;
                                                              ENU=Sell-to Post Code] }
    { 89  ;   ;Sell-to County      ;Text30        ;CaptionML=[DEU=Verk. an Bundesregion;
                                                              ENU=Sell-to County] }
    { 90  ;   ;Sell-to Country/Region Code;Code10 ;TableRelation=Country/Region;
                                                   CaptionML=[DEU=Verk. an L�nder-/Regionscode;
                                                              ENU=Sell-to Country/Region Code] }
    { 91  ;   ;Ship-to Post Code   ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Lief. an PLZ-Code;
                                                              ENU=Ship-to Post Code] }
    { 92  ;   ;Ship-to County      ;Text30        ;CaptionML=[DEU=Lief. an Bundesregion;
                                                              ENU=Ship-to County] }
    { 93  ;   ;Ship-to Country/Region Code;Code10 ;TableRelation=Country/Region;
                                                   CaptionML=[DEU=Lief. an L�nder-/Regionscode;
                                                              ENU=Ship-to Country/Region Code] }
    { 94  ;   ;Bal. Account Type   ;Option        ;CaptionML=[DEU=Gegenkontoart;
                                                              ENU=Bal. Account Type];
                                                   OptionCaptionML=[DEU=Sachkonto,Bankkonto;
                                                                    ENU=G/L Account,Bank Account];
                                                   OptionString=G/L Account,Bank Account }
    { 97  ;   ;Exit Point          ;Code10        ;TableRelation="Entry/Exit Point";
                                                   CaptionML=[DEU=Einladehafen;
                                                              ENU=Exit Point] }
    { 98  ;   ;Correction          ;Boolean       ;CaptionML=[DEU=Storno;
                                                              ENU=Correction] }
    { 99  ;   ;Document Date       ;Date          ;CaptionML=[DEU=Belegdatum;
                                                              ENU=Document Date] }
    { 100 ;   ;External Document No.;Code35       ;CaptionML=[DEU=Externe Belegnummer;
                                                              ENU=External Document No.] }
    { 101 ;   ;Area                ;Code10        ;TableRelation=Area;
                                                   CaptionML=[DEU=Ursprungsregion;
                                                              ENU=Area] }
    { 102 ;   ;Transaction Specification;Code10   ;TableRelation="Transaction Specification";
                                                   CaptionML=[DEU=Verfahren;
                                                              ENU=Transaction Specification] }
    { 104 ;   ;Payment Method Code ;Code10        ;TableRelation="Payment Method";
                                                   CaptionML=[DEU=Zahlungsformcode;
                                                              ENU=Payment Method Code] }
    { 107 ;   ;Pre-Assigned No. Series;Code10     ;TableRelation="No. Series";
                                                   CaptionML=[DEU=Zugeordnete Nr.-Serie;
                                                              ENU=Pre-Assigned No. Series] }
    { 108 ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[DEU=Nummernserie;
                                                              ENU=No. Series];
                                                   Editable=No }
    { 111 ;   ;Pre-Assigned No.    ;Code20        ;CaptionML=[DEU=Zugeordnete Nr.;
                                                              ENU=Pre-Assigned No.] }
    { 112 ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Benutzer-ID;
                                                              ENU=User ID] }
    { 113 ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[DEU=Herkunftscode;
                                                              ENU=Source Code] }
    { 114 ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[DEU=Steuergebietscode;
                                                              ENU=Tax Area Code] }
    { 115 ;   ;Tax Liable          ;Boolean       ;CaptionML=[DEU=Steuerpflichtig;
                                                              ENU=Tax Liable] }
    { 116 ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[DEU=MwSt.-Gesch�ftsbuchungsgruppe;
                                                              ENU=VAT Bus. Posting Group] }
    { 119 ;   ;VAT Base Discount % ;Decimal       ;CaptionML=[DEU=MwSt.-Bemessungsgr. Skonto %;
                                                              ENU=VAT Base Discount %];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 134 ;   ;Prepmt. Cr. Memo No. Series;Code10 ;TableRelation="No. Series";
                                                   CaptionML=[DEU=Vorauszahlungsgutschrift Nummernserie;
                                                              ENU=Prepmt. Cr. Memo No. Series] }
    { 136 ;   ;Prepayment Credit Memo;Boolean     ;CaptionML=[DEU=Vorauszahlungsgutschrift;
                                                              ENU=Prepayment Credit Memo] }
    { 137 ;   ;Prepayment Order No.;Code20        ;CaptionML=[DEU=Vorauszahlungsauftragsnr.;
                                                              ENU=Prepayment Order No.] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=[DEU=Dimensionssatz-ID;
                                                              ENU=Dimension Set ID];
                                                   Editable=No }
    { 710 ;   ;Document Exchange Identifier;Text50;CaptionML=[DEU=Belegaustauschsbezeichner;
                                                              ENU=Document Exchange Identifier] }
    { 711 ;   ;Document Exchange Status;Option    ;CaptionML=[DEU=Belegaustauschsstatus;
                                                              ENU=Document Exchange Status];
                                                   OptionCaptionML=[DEU=Nicht gesendet,An Belegaustauschdienst gesendet,An Empf�nger geliefert,Fehler bei der �bermittlung,Verbindung zu Empf�nger steht aus;
                                                                    ENU=Not Sent,Sent to Document Exchange Service,Delivered to Recipient,Delivery Failed,Pending Connection to Recipient];
                                                   OptionString=Not Sent,Sent to Document Exchange Service,Delivered to Recipient,Delivery Failed,Pending Connection to Recipient }
    { 712 ;   ;Doc. Exch. Original Identifier;Text50;
                                                   CaptionML=[DEU=Originalbezeichner Belegaustausch;
                                                              ENU=Doc. Exch. Original Identifier] }
    { 1302;   ;Paid                ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=-Exist("Cust. Ledger Entry" WHERE (Entry No.=FIELD(Cust. Ledger Entry No.),
                                                                                                  Open=FILTER(Yes)));
                                                   CaptionML=[DEU=Bezahlt;
                                                              ENU=Paid];
                                                   Editable=No }
    { 1303;   ;Remaining Amount    ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Detailed Cust. Ledg. Entry".Amount WHERE (Cust. Ledger Entry No.=FIELD(Cust. Ledger Entry No.)));
                                                   CaptionML=[DEU=Restbetrag;
                                                              ENU=Remaining Amount];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 1304;   ;Cust. Ledger Entry No.;Integer     ;TableRelation="Cust. Ledger Entry"."Entry No.";
                                                   TestTableRelation=No;
                                                   CaptionML=[DEU=Debitorenposten Lfd. Nr.;
                                                              ENU=Cust. Ledger Entry No.];
                                                   Editable=No }
    { 1305;   ;Invoice Discount Amount;Decimal    ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Sales Cr.Memo Line"."Inv. Discount Amount" WHERE (Document No.=FIELD(No.)));
                                                   CaptionML=[DEU=Rechnungsrab.-Betrag;
                                                              ENU=Invoice Discount Amount];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 1310;   ;Cancelled           ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Cancelled Document" WHERE (Source ID=CONST(114),
                                                                                                 Cancelled Doc. No.=FIELD(No.)));
                                                   CaptionML=[DEU=Storniert;
                                                              ENU=Cancelled];
                                                   Editable=No }
    { 1311;   ;Corrective          ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Cancelled Document" WHERE (Source ID=CONST(112),
                                                                                                 Cancelled By Doc. No.=FIELD(No.)));
                                                   CaptionML=[DEU=Korrektur;
                                                              ENU=Corrective];
                                                   Editable=No }
    { 5050;   ;Campaign No.        ;Code20        ;TableRelation=Campaign;
                                                   CaptionML=[DEU=Kampagnennr.;
                                                              ENU=Campaign No.] }
    { 5052;   ;Sell-to Contact No. ;Code20        ;TableRelation=Contact;
                                                   CaptionML=[DEU=Verk. an Kontaktnr.;
                                                              ENU=Sell-to Contact No.] }
    { 5053;   ;Bill-to Contact No. ;Code20        ;TableRelation=Contact;
                                                   CaptionML=[DEU=Rech. an Kontaktnr.;
                                                              ENU=Bill-to Contact No.] }
    { 5055;   ;Opportunity No.     ;Code20        ;TableRelation=Opportunity;
                                                   CaptionML=[DEU=Verkaufschancennr.;
                                                              ENU=Opportunity No.] }
    { 5700;   ;Responsibility Center;Code10       ;TableRelation="Responsibility Center";
                                                   CaptionML=[DEU=Zust�ndigkeitseinheitencode;
                                                              ENU=Responsibility Center] }
    { 6601;   ;Return Order No.    ;Code20        ;AccessByPermission=TableData 6660=R;
                                                   CaptionML=[DEU=Reklamationsnr.;
                                                              ENU=Return Order No.] }
    { 6602;   ;Return Order No. Series;Code10     ;TableRelation="No. Series";
                                                   CaptionML=[DEU=Reklamationsnr.-Serie;
                                                              ENU=Return Order No. Series] }
    { 7001;   ;Allow Line Disc.    ;Boolean       ;CaptionML=[DEU=Zeilenrabatt zulassen;
                                                              ENU=Allow Line Disc.] }
    { 7200;   ;Get Return Receipt Used;Boolean    ;CaptionML=[DEU=Verwendete R�cksend. abrufen;
                                                              ENU=Get Return Receipt Used] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Pre-Assigned No.                         }
    {    ;Return Order No.                         }
    {    ;Sell-to Customer No.                     }
    {    ;Prepayment Order No.                     }
    {    ;Bill-to Customer No.                     }
    {    ;Posting Date                             }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,Sell-to Customer No.,Bill-to Customer No.,Posting Date,Posting Description }
    { 2   ;Brick               ;No.,Sell-to Customer Name,Amount,Due Date,Amount Including VAT }
  }
  CODE
  {
    VAR
      SalesCommentLine@1001 : Record 44;
      CustLedgEntry@1002 : Record 21;
      ApprovalsMgmt@1004 : Codeunit 1535;
      DimMgt@1005 : Codeunit 408;
      UserSetupMgt@1006 : Codeunit 5700;
      DocTxt@1000 : TextConst 'DEU=Gutschrift;ENU=Credit Memo';

    PROCEDURE SendRecords@12();
    VAR
      DocumentSendingProfile@1001 : Record 60;
      DummyReportSelections@1000 : Record 77;
    BEGIN
      DocumentSendingProfile.SendCustomerRecords(
        DummyReportSelections.Usage::"S.Cr.Memo",Rec,DocTxt,"Bill-to Customer No.","No.",
        FIELDNO("Bill-to Customer No."),FIELDNO("No."));
    END;

    PROCEDURE SendProfile@6(VAR DocumentSendingProfile@1000 : Record 60);
    VAR
      DummyReportSelections@1002 : Record 77;
    BEGIN
      DocumentSendingProfile.Send(
        DummyReportSelections.Usage::"S.Cr.Memo",Rec,"No.","Bill-to Customer No.",
        DocTxt,FIELDNO("Bill-to Customer No."),FIELDNO("No."));
    END;

    PROCEDURE PrintRecords@1(ShowRequestForm@1000 : Boolean);
    VAR
      DocumentSendingProfile@1002 : Record 60;
      DummyReportSelections@1001 : Record 77;
    BEGIN
      DocumentSendingProfile.TrySendToPrinter(
        DummyReportSelections.Usage::"S.Cr.Memo",Rec,"Bill-to Customer No.",ShowRequestForm);
    END;

    PROCEDURE EmailRecords@17(ShowRequestForm@1000 : Boolean);
    VAR
      DocumentSendingProfile@1002 : Record 60;
      DummyReportSelections@1001 : Record 77;
    BEGIN
      DocumentSendingProfile.TrySendToEMail(
        DummyReportSelections.Usage::"S.Cr.Memo",Rec,FIELDNO("No."),DocTxt,FIELDNO("Bill-to Customer No."),ShowRequestForm);
    END;

    PROCEDURE Navigate@2();
    VAR
      NavigateForm@1000 : Page 344;
    BEGIN
      NavigateForm.SetDoc("Posting Date","No.");
      NavigateForm.RUN;
    END;

    PROCEDURE LookupAdjmtValueEntries@3();
    VAR
      ValueEntry@1000 : Record 5802;
    BEGIN
      ValueEntry.SETCURRENTKEY("Document No.");
      ValueEntry.SETRANGE("Document No.","No.");
      ValueEntry.SETRANGE("Document Type",ValueEntry."Document Type"::"Sales Credit Memo");
      ValueEntry.SETRANGE(Adjustment,TRUE);
      PAGE.RUNMODAL(0,ValueEntry);
    END;

    PROCEDURE GetCustomerVATRegistrationNumber@14() : Text;
    BEGIN
      EXIT("VAT Registration No.");
    END;

    PROCEDURE GetCustomerVATRegistrationNumberLbl@15() : Text;
    BEGIN
      EXIT(FIELDCAPTION("VAT Registration No."));
    END;

    PROCEDURE GetCustomerGlobalLocationNumber@8() : Text;
    BEGIN
      EXIT('');
    END;

    PROCEDURE GetCustomerGlobalLocationNumberLbl@10() : Text;
    BEGIN
      EXIT('');
    END;

    PROCEDURE GetLegalStatement@60() : Text;
    VAR
      SalesSetup@1000 : Record 311;
    BEGIN
      SalesSetup.GET;
      EXIT(SalesSetup.GetLegalStatement);
    END;

    PROCEDURE ShowDimensions@4();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"No."));
    END;

    PROCEDURE SetSecurityFilterOnRespCenter@5();
    BEGIN
      IF UserSetupMgt.GetSalesFilter <> '' THEN BEGIN
        FILTERGROUP(2);
        SETRANGE("Responsibility Center",UserSetupMgt.GetSalesFilter);
        FILTERGROUP(0);
      END;
    END;

    PROCEDURE GetDocExchStatusStyle@13() : Text;
    BEGIN
      CASE "Document Exchange Status" OF
        "Document Exchange Status"::"Not Sent":
          EXIT('Standard');
        "Document Exchange Status"::"Sent to Document Exchange Service":
          EXIT('Ambiguous');
        "Document Exchange Status"::"Delivered to Recipient":
          EXIT('Favorable');
        ELSE
          EXIT('Unfavorable');
      END;
    END;

    PROCEDURE ShowActivityLog@116();
    VAR
      ActivityLog@1000 : Record 710;
    BEGIN
      ActivityLog.ShowEntries(RECORDID);
    END;

    PROCEDURE DocExchangeStatusIsSent@112() : Boolean;
    BEGIN
      EXIT("Document Exchange Status" <> "Document Exchange Status"::"Not Sent");
    END;

    PROCEDURE ShowCanceledOrCorrInvoice@18();
    BEGIN
      CALCFIELDS(Cancelled,Corrective);
      CASE TRUE OF
        Cancelled:
          ShowCorrectiveInvoice;
        Corrective:
          ShowCancelledInvoice;
      END;
    END;

    PROCEDURE ShowCorrectiveInvoice@16();
    VAR
      CancelledDocument@1000 : Record 1900;
      SalesInvHeader@1001 : Record 112;
    BEGIN
      CALCFIELDS(Cancelled);
      IF NOT Cancelled THEN
        EXIT;

      IF CancelledDocument.FindSalesCancelledCrMemo("No.") THEN BEGIN
        SalesInvHeader.GET(CancelledDocument."Cancelled By Doc. No.");
        PAGE.RUN(PAGE::"Posted Sales Invoice",SalesInvHeader);
      END;
    END;

    PROCEDURE ShowCancelledInvoice@37();
    VAR
      CancelledDocument@1000 : Record 1900;
      SalesInvHeader@1001 : Record 112;
    BEGIN
      CALCFIELDS(Corrective);
      IF NOT Corrective THEN
        EXIT;

      IF CancelledDocument.FindSalesCorrectiveCrMemo("No.") THEN BEGIN
        SalesInvHeader.GET(CancelledDocument."Cancelled Doc. No.");
        PAGE.RUN(PAGE::"Posted Sales Invoice",SalesInvHeader);
      END;
    END;

    BEGIN
    END.
  }
}

