OBJECT Page 423 Customer Bank Account Card
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Debitor Bankkontokarte;
               ENU=Customer Bank Account Card];
    SourceTable=Table287;
    PageType=Card;
    ActionList=ACTIONS
    {
      { 3       ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 5       ;1   ;Action    ;
                      CaptionML=[DEU=Lastschrift-Mandate;
                                 ENU=Direct Debit Mandates];
                      ToolTipML=[DEU=Zeigen Sie die Lastschrift-Mandate an, die Sie eingerichtet haben, um Vereinbarungen mit Debitoren zum direkten Einzug der Zahlungen von ihrem Bankkonto widerzuspiegeln, oder bearbeiten Sie diese.;
                                 ENU=View or edit direct-debit mandates that you set up to reflect agreements with customers to collect invoice payments from their bank account.];
                      ApplicationArea=#Suite;
                      RunObject=Page 1230;
                      RunPageLink=Customer No.=FIELD(Customer No.),
                                  Customer Bank Account Code=FIELD(Code);
                      Promoted=Yes;
                      Image=MakeAgreement;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Legt einen Code zur Identifizierung dieses Debitor Bankkontos fest.;
                           ENU=Specifies a code to identify this customer bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Bank an, bei der das Bankkonto des Debitors gef�hrt wird.;
                           ENU=Specifies the name of the bank where the customer has the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Adresse der Bank an, bei der das Bankkonto des Debitors gef�hrt wird.;
                           ENU=Specifies the address of the bank where the customer has the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Address }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt zus�tzliche Adressinformationen an.;
                           ENU=Specifies additional address information.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Address 2" }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Postleitzahl an.;
                           ENU=Specifies the postal code.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Post Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ort der Bank an, bei der das Bankkonto des Debitors gef�hrt wird.;
                           ENU=Specifies the city of the bank where the customer has the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=City }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Land/die Region der Adresse an.;
                           ENU=Specifies the country/region of the address.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Country/Region Code" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Telefonnummer der Bank an, bei der das Konto des Debitors gef�hrt wird.;
                           ENU=Specifies the telephone number of the bank where the customer has the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Phone No." }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Bankangestellten an, mit dem Sie in Zusammenhang mit diesem Bankkonto regelm��ig Kontakt aufnehmen.;
                           ENU=Specifies the name of the bank employee regularly contacted in connection with this bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Contact }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den entsprechenden W�hrungscode f�r das Bankkonto fest.;
                           ENU=Specifies the relevant currency code for the bank account.];
                ApplicationArea=#Suite;
                SourceExpr="Currency Code" }

    { 64  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Bankleitzahl an.;
                           ENU=Specifies the number of the bank branch.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Branch No." }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer an, die die Bank f�r das Bankkonto verwendet.;
                           ENU=Specifies the number used by the bank for the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No." }

    { 50  ;2   ;Field     ;
                ToolTipML=[DEU=Legt eine beliebige Bank ID-Nummer fest.;
                           ENU=Specifies a bank identification number of your own choice.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Transit No." }

    { 1902768601;1;Group  ;
                CaptionML=[DEU=Kommunikation;
                           ENU=Communication] }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Faxnummer der Bank an, bei der das Konto des Debitors gef�hrt wird.;
                           ENU=Specifies the fax number of the bank where the customer has the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Fax No." }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die mit dem Bankkonto verbundene E-Mail-Adresse an.;
                           ENU=Specifies the email address associated with the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="E-Mail" }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die mit dem Bankkonto verbundene Homepage an.;
                           ENU=Specifies the home page address associated with the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Home Page" }

    { 1905090301;1;Group  ;
                CaptionML=[DEU=Transfer;
                           ENU=Transfer] }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den SWIFT-Code (internationale Bankidentifikationsnummer) der Bank an, bei der der Debitor sein Konto hat.;
                           ENU=Specifies the SWIFT code (international bank identifier code) of the bank where the customer has the account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="SWIFT Code" }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die internationale Bankkontonummer des Bankkontos an.;
                           ENU=Specifies the bank account's international bank account number.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=IBAN }

    { 13  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den in Bank�berweisungen zu verwendenden Formatstandard fest, wenn Sie das Feld "Bank-Clearing-Code" verwenden, um sich als Absender zu kennzeichnen.;
                           ENU=Specifies the format standard to be used in bank transfers if you use the Bank Clearing Code field to identify you as the sender.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Clearing Standard" }

    { 9   ;2   ;Field     ;
                ToolTipML=[DEU=Legt den Code f�r Bank-Clearing fest, der laut dem von Ihnen ausgew�hlten Formatstandard im Feld "Bank-Clearing-Standard" erforderlich ist.;
                           ENU=Specifies the code for bank clearing that is required according to the format standard you selected in the Bank Clearing Standard field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Clearing Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

