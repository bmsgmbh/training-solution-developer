OBJECT Page 394 Entry/Exit Points
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=H�fen;
               ENU=Entry/Exit Points];
    SourceTable=Table282;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r den Lieferhafen (Einlade-/Entladehafen) an.;
                           ENU=Specifies the code for the shipping location (Entry/Exit Point).];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung f�r den Lieferhafen (Einlade-/Entladehafen) an.;
                           ENU=Specifies a description of the shipping location (Entry/Exit Point).];
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

