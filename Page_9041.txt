OBJECT Page 9041 Shop Supervisor Activities
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Aktivit�ten;
               ENU=Activities];
    SourceTable=Table9056;
    PageType=CardPart;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 8   ;1   ;Group     ;
                CaptionML=[DEU=Fertigungsauftr�ge;
                           ENU=Production Orders];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 22      ;0   ;Action    ;
                                  CaptionML=[DEU=FA-Status �ndern;
                                             ENU=Change Production Order Status];
                                  RunObject=Page 99000914;
                                  Image=ChangeStatus }
                  { 2       ;0   ;Action    ;
                                  CaptionML=[DEU=Einstandspreis aktualisieren;
                                             ENU=Update Unit Cost];
                                  RunObject=Report 99001014;
                                  Image=UpdateUnitCost }
                  { 27      ;0   ;Action    ;
                                  CaptionML=[DEU=Navigate;
                                             ENU=Navigate];
                                  ToolTipML=[DEU=Zeigt alle Posten an, die f�r die Belegnummer in der ausgew�hlten Zeile vorhanden sind, und stellt eine Verkn�pfung zu ihnen her.;
                                             ENU=View and link to all entries that exist for the document number on the selected line.];
                                  RunObject=Page 344;
                                  Image=Navigate }
                }
                 }

    { 1   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl geplanter Fertigungsauftr�ge an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of planned production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Planned Prod. Orders - All";
                DrillDownPageID=Planned Production Orders }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl fest geplanter Fertigungsauftr�ge an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of firm planned production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Firm Plan. Prod. Orders - All";
                DrillDownPageID=Firm Planned Prod. Orders }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl freigegebener Fertigungsauftr�ge an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of released production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Released Prod. Orders - All";
                DrillDownPageID=Released Production Orders }

    { 9   ;1   ;Group     ;
                CaptionML=[DEU=Arbeitsg�nge;
                           ENU=Operations];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 19      ;0   ;Action    ;
                                  CaptionML=[DEU=FA-Verb. Buch.-Blatt;
                                             ENU=Consumption Journal];
                                  RunObject=Page 99000846;
                                  Image=ConsumptionJournal }
                  { 20      ;0   ;Action    ;
                                  CaptionML=[DEU=FA-Istmld. Buch.-Bl.;
                                             ENU=Output Journal];
                                  RunObject=Page 99000823;
                                  Image=OutputJournal }
                }
                 }

    { 11  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl von FA-Arbeitspl�nen in Warteschlange an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of production order routings in queue that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Prod. Orders Routings-in Queue";
                DrillDownPageID=Prod. Order Routing }

    { 13  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl inaktiver Serviceauftr�ge an, die im Rollencenter im Servicestapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of inactive service orders that are displayed in the Service Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Prod. Orders Routings-in Prog.";
                DrillDownPageID=Prod. Order Routing }

    { 10  ;1   ;Group     ;
                CaptionML=[DEU=Logistikbelege;
                           ENU=Warehouse Documents];
                GroupType=CueGroup }

    { 21  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl von Lagerkommissionierungen an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of inventory picks that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Invt. Picks to Production";
                DrillDownPageID=Inventory Picks }

    { 17  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl von Lagereinlagerungen von Fertigung an, die im Rollencenter im Produktionsstapel angezeigt werden. Die Belege werden nach dem heutigen Datum gefiltert.;
                           ENU=Specifies the number of inventory put-always from production that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.];
                SourceExpr="Invt. Put-aways from Prod.";
                DrillDownPageID=Inventory Put-aways }

  }
  CODE
  {

    BEGIN
    END.
  }
}

