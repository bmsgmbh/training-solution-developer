OBJECT Table 324 VAT Product Posting Group
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00,NAVDACH10.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Description;
    CaptionML=[DEU=MwSt.-Produktbuchungsgrp.;
               ENU=VAT Product Posting Group];
    LookupPageID=Page471;
    DrillDownPageID=Page471;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[DEU=Code;
                                                              ENU=Code];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[DEU=Beschreibung;
                                                              ENU=Description] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;Brick               ;Code,Description                         }
  }
  CODE
  {

    PROCEDURE LookupVATProductPostingGroupFilter@1140000(VAR FilterString@1140000 : Text) : Boolean;
    VAR
      VATProductPostingGroups@1140001 : Page 471;
    BEGIN
      VATProductPostingGroups.LOOKUPMODE(TRUE);
      IF VATProductPostingGroups.RUNMODAL = ACTION::LookupOK THEN BEGIN
        FilterString := VATProductPostingGroups.GetSelectionFilter;
        EXIT(TRUE);
      END;
      EXIT(FALSE)
    END;

    BEGIN
    END.
  }
}

