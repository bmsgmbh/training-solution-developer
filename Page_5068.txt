OBJECT Page 5068 Industry Group Contacts
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Branchenkontakte;
               ENU=Industry Group Contacts];
    SourceTable=Table5058;
    DataCaptionFields=Industry Group Code;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kontaktunternehmens an, dem Sie Branchen zuordnen.;
                           ENU=Specifies the number of the contact company you are assigning industry groups.];
                ApplicationArea=#All;
                SourceExpr="Contact No." }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[DEU=Gibt den Namen des Kontaktunternehmens an, dem Sie eine Branche zuordnen.;
                           ENU=Specifies the name of the contact company you are assigning an industry group.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact Name" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

