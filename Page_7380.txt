OBJECT Page 7380 Phys. Invt. Item Selection
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Inventurartikelauswahl;
               ENU=Phys. Invt. Item Selection];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table7380;
    PageType=List;
    OnQueryClosePage=BEGIN
                       IF CloseAction = ACTION::LookupOK THEN
                         LookupOKOnPush;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 23      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 24      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Artikelkarte;
                                 ENU=Item Card];
                      RunObject=Page 30;
                      RunPageLink=No.=FIELD(Item No.);
                      Image=Item }
      { 25      ;2   ;Action    ;
                      CaptionML=[DEU=Lagerhaltungsdatenkarte;
                                 ENU=SKU Card];
                      RunObject=Page 5701;
                      RunPageLink=Item No.=FIELD(Item No.),
                                  Variant Code=FIELD(Variant Code),
                                  Location Code=FIELD(Location Code);
                      Image=SKU }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Artikels an, f�r den eine zyklische Inventur vorgenommen werden kann.;
                           ENU=Specifies the number of the item for which the cycle counting can be performed.];
                SourceExpr="Item No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung des Artikels an.;
                           ENU=Specifies the description of the item.];
                SourceExpr=Description }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Variante des Artikels an, f�r den eine zyklische Inventur vorgenommen werden kann.;
                           ENU=Specifies the variant of the item for which the cycle counting can be performed.];
                SourceExpr="Variant Code" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Lagerorts an, an dem die zyklische Inventur ausgef�hrt werden soll.;
                           ENU=Specifies the code of the location where the cycle counting is performed.];
                SourceExpr="Location Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Regalnummer des Artikels zu Informationszwecken an.;
                           ENU=Specifies the shelf number of the item for informational use.];
                SourceExpr="Shelf No.";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den Code der Inventurh�ufigkeit fest, der angibt, wie oft der Artikel bzw. die Lagerhaltungsdaten �ber eine Inventur erfasst werden sollen.;
                           ENU=Specifies the code of the counting period that indicates how often you want to count the item or stockkeeping unit in a physical inventory.];
                SourceExpr="Phys Invt Counting Period Code" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das letzte Datum an, an dem die Inventurh�ufigkeit f�r den Artikel oder die Lagerhaltungsdaten aktualisiert wurde.;
                           ENU=Specifies the last date when the counting period for the item or stockkeeping unit was updated.];
                SourceExpr="Last Counting Date" }

    { 16  ;2   ;Field     ;
                SourceExpr="Next Counting Start Date" }

    { 3   ;2   ;Field     ;
                SourceExpr="Next Counting End Date" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie oft der Artikel oder die Lagerhaltungsdaten pro Jahr gez�hlt werden sollen.;
                           ENU=Specifies the number of times you want the item or stockkeeping unit to be counted each year.];
                SourceExpr="Count Frequency per Year";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    LOCAL PROCEDURE LookupOKOnPush@19031339();
    BEGIN
      CurrPage.SETSELECTIONFILTER(Rec);
      MODIFYALL(Selected,TRUE);
    END;

    BEGIN
    END.
  }
}

