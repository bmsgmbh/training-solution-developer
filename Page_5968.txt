OBJECT Page 5968 Service Document Registers
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Servicebelegjournale;
               ENU=Service Document Registers];
    SourceTable=Table5936;
    DataCaptionFields=Source Document Type,Source Document No.;
    PageType=List;
    OnAfterGetRecord=BEGIN
                       CASE "Destination Document Type" OF
                         "Destination Document Type"::Invoice:
                           IF ServHeader.GET(ServHeader."Document Type"::Invoice,"Destination Document No.") THEN BEGIN
                             CustNo := ServHeader."Bill-to Customer No.";
                             CustName := ServHeader."Bill-to Name";
                           END;
                         "Destination Document Type"::"Credit Memo":
                           IF ServHeader.GET(ServHeader."Document Type"::"Credit Memo","Destination Document No.") THEN BEGIN
                             CustNo := ServHeader."Bill-to Customer No.";
                             CustName := ServHeader."Bill-to Name";
                           END;
                         "Destination Document Type"::"Posted Invoice":
                           IF ServInvHeader.GET("Destination Document No.") THEN BEGIN
                             CustNo := ServInvHeader."Bill-to Customer No.";
                             CustName := ServInvHeader."Bill-to Name";
                           END;
                         "Destination Document Type"::"Posted Credit Memo":
                           IF ServCrMemoHeader.GET("Destination Document No.") THEN BEGIN
                             CustNo := ServCrMemoHeader."Bill-to Customer No.";
                             CustName := ServCrMemoHeader."Bill-to Name";
                           END;
                       END;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 13      ;1   ;ActionGroup;
                      CaptionML=[DEU=B&eleg;
                                 ENU=&Document];
                      Image=Document }
      { 14      ;2   ;Action    ;
                      Name=Card;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Karte;
                                 ENU=Card];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=EditLines;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CASE "Destination Document Type" OF
                                   "Destination Document Type"::Invoice:
                                     BEGIN
                                       ServHeader.GET(ServHeader."Document Type"::Invoice,"Destination Document No.");
                                       PAGE.RUN(PAGE::"Service Invoice",ServHeader);
                                     END;
                                   "Destination Document Type"::"Credit Memo":
                                     BEGIN
                                       ServHeader.GET(ServHeader."Document Type"::"Credit Memo","Destination Document No.");
                                       PAGE.RUN(PAGE::"Service Credit Memo",ServHeader);
                                     END;
                                   "Destination Document Type"::"Posted Invoice":
                                     BEGIN
                                       ServInvHeader.GET("Destination Document No.");
                                       PAGE.RUN(PAGE::"Posted Service Invoice",ServInvHeader);
                                     END;
                                   "Destination Document Type"::"Posted Credit Memo":
                                     BEGIN
                                       ServCrMemoHeader.GET("Destination Document No.");
                                       PAGE.RUN(PAGE::"Posted Service Credit Memo",ServCrMemoHeader);
                                     END;
                                 END;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Serviceauftrags oder des Servicevertrags an.;
                           ENU=Specifies the number of the service order or service contract.];
                SourceExpr="Source Document No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Belegs an, der aus dem Serviceauftrag oder dem Vertrag erstellt wurde, der in "Herkunftsbelegnr." angegeben wurde.;
                           ENU=Specifies the type of document created from the service order or contract specified in the Source Document No.];
                SourceExpr="Destination Document Type" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Rechnung oder Gutschrift auf Grundlage der Inhalte des Felds "Zielbelegart" an.;
                           ENU=Specifies the number of the invoice or credit memo, based on the contents of the Destination Document Type field.];
                SourceExpr="Destination Document No." }

    { 15  ;2   ;Field     ;
                CaptionML=[DEU=Rech. an Deb.-Nr.;
                           ENU=Bill-to Customer No.];
                SourceExpr=CustNo;
                TableRelation=Customer;
                Editable=FALSE }

    { 19  ;2   ;Field     ;
                CaptionML=[DEU=Rech. an Name;
                           ENU=Bill-to Customer Name];
                SourceExpr=CustName;
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ServHeader@1000 : Record 5900;
      ServInvHeader@1001 : Record 5992;
      ServCrMemoHeader@1002 : Record 5994;
      CustNo@1003 : Code[20];
      CustName@1004 : Text[50];

    BEGIN
    END.
  }
}

