OBJECT Page 5005371 Post. Exp. Ph. In. Track. List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVDACH10.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Geb. erw. Inv.-Verfolg.-�bers.;
               ENU=Post. Exp. Ph. In. Track. List];
    SourceTable=Table5005362;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1140000;1;Group     ;
                GroupType=Repeater }

    { 1140001;2;Field     ;
                ToolTipML=[DEU=Zeigt die erwartete Seriennummer.;
                           ENU=Shows the expected Serial No.];
                SourceExpr="Serial No." }

    { 1140003;2;Field     ;
                ToolTipML=[DEU=Zeigt die erwartete Chargennummer.;
                           ENU=Shows the expected Lot No.];
                SourceExpr="Lot No." }

    { 1140005;2;Field     ;
                ToolTipML=[DEU=Zeigt die erwartete Menge f�r eine Seriennummer und Chargennummer, die sich auf den Basiseinheitencode in der gebuchten Inventurauftragszeile bezieht.;
                           ENU=Shows the expected quantity of Serial No. and Lot No. that relates to the Base Unit of Measure Code, in the Posted Inventory Order Line.];
                SourceExpr="Quantity (Base)" }

    { 1140007;2;Field     ;
                ToolTipML=[DEU=Zeigt die Belegnummer des gebuchten Inventurauftrags an.;
                           ENU=Shows the document number of the Posted Inventory Order.];
                SourceExpr="Order No" }

    { 1140009;2;Field     ;
                ToolTipML=[DEU=Zeigt die Zeilennummer der gebuchten Inventurauftragszeile an.;
                           ENU=Shows the line number of the Posted Inventory Order Line.];
                SourceExpr="Order Line No." }

  }
  CODE
  {

    BEGIN
    END.
  }
}

