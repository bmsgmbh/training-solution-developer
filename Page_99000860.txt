OBJECT Page 99000860 Planning Worksheet Line List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Planungsvorschlagzeilen�bers.;
               ENU=Planning Worksheet Line List];
    SourceTable=Table246;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 20      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 6500    ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[DEU=Artikel&verfolgungszeilen;
                                 ENU=Item &Tracking Lines];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Aktion an, die unternommen werden soll, um die Bedarfssituation auszugleichen.;
                           ENU=Specifies an action to take to rebalance the demand-supply situation.];
                SourceExpr="Action Message" }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos oder Artikels an, die in der Zeile eingegeben wird.;
                           ENU=Specifies the number of the general ledger account or item to be entered on the line.];
                SourceExpr="No." }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Variantencode f�r den Artikel an.;
                           ENU=Specifies a variant code for the item.];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt beschreibenden Text f�r den Posten an.;
                           ENU=Specifies text that describes the entry.];
                SourceExpr=Description }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt zus�tzlichen Text an, der den Posten oder die Bemerkung �ber die Bestellvorschlagszeile beschreibt.;
                           ENU=Specifies additional text describing the entry, or a remark about the requisition worksheet line.];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Bestellvorschlagszeile verkn�pft werden soll.;
                           ENU=Specifies the dimension value code to be linked to the requisition worksheet line.];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 53  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Dimensionswertcode an, der mit der Bestellvorschlagszeile verkn�pft werden soll.;
                           ENU=Specifies the dimension value code to be linked to the requisition worksheet line.];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 55  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code f�r den Lagerort an, an dem die bestellten Artikel registriert werden.;
                           ENU=Specifies a code for an inventory location where the items that are being ordered will be registered.];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Einheiten des Artikels an.;
                           ENU=Specifies the number of units of the item.];
                SourceExpr=Quantity;
                Visible=FALSE }

    { 57  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ausschuss in Prozess f�r den Artikel in der jeweiligen Arbeitsblattzeile an.;
                           ENU=Specifies the scrap percentage of the item to be included on the worksheet line.];
                SourceExpr="Scrap %";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das erwartete Lieferdatum der Artikel an.;
                           ENU=Specifies the date when you can expect to receive the items.];
                SourceExpr="Due Date" }

    { 59  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Startzeit des Fertigungsprozesses an.;
                           ENU=Specifies the starting time of the manufacturing process.];
                SourceExpr="Starting Time";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Startdatum des Fertigungsprozesses an, wenn es sich bei der geplanten Beschaffung um einen Fertigungsauftrag handelt.;
                           ENU=Specifies the starting date of the manufacturing process, if the planned supply is a production order.];
                SourceExpr="Starting Date" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Endzeit f�r den Fertigungsprozess an.;
                           ENU=Specifies the ending time for the manufacturing process.];
                SourceExpr="Ending Time";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Enddatum des Fertigungsprozesses an, wenn es sich bei dem geplanten Vorrat um einen Fertigungsauftrag handelt.;
                           ENU=Specifies the ending date of the manufacturing process, if the planned supply is a production order.];
                SourceExpr="Ending Date" }

    { 61  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die die Nummer der Fertigungsst�ckliste f�r diesen Fertigungsauftrag an.;
                           ENU=Specifies the production BOM number for this production order.];
                SourceExpr="Production BOM No.";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Arbeitsplans an.;
                           ENU=Specifies the routing number.];
                SourceExpr="Routing No.";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

