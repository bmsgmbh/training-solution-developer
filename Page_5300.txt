OBJECT Page 5300 Outlook Synch. Entity
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Outlook-Synch.-Einheit;
               ENU=Outlook Synch. Entity];
    SourceTable=Table5300;
    PageType=ListPlus;
    OnOpenPage=VAR
                 OutlookSynchSetupDefaults@1000 : Codeunit 5312;
               BEGIN
                 OutlookSynchSetupDefaults.InsertOSynchDefaults;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[DEU=S&ynch.-Einheit;
                                 ENU=S&ynch. Entity];
                      Image=OutlookSyncFields }
      { 16      ;2   ;Action    ;
                      CaptionML=[DEU=Felder;
                                 ENU=Fields];
                      Image=OutlookSyncFields;
                      OnAction=BEGIN
                                 ShowEntityFields;
                               END;
                                }
      { 21      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Auf Standards zur�cksetzen;
                                 ENU=Reset to Defaults];
                      Image=Restore;
                      OnAction=VAR
                                 OutlookSynchSetupDefaults@1000 : Codeunit 5312;
                               BEGIN
                                 OutlookSynchSetupDefaults.ResetEntity(Code);
                               END;
                                }
      { 19      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=In der Einrichtung des �nderung&sprotokolls erfassen;
                                 ENU=Register in Change Log &Setup];
                      Image=ImportLog;
                      OnAction=VAR
                                 OSynchEntity@1000 : Record 5300;
                               BEGIN
                                 OSynchEntity := Rec;
                                 OSynchEntity.SETRECFILTER;
                                 REPORT.RUN(REPORT::"Outlook Synch. Change Log Set.",TRUE,FALSE,OSynchEntity);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen eindeutigen Bezeichner f�r jeden Posten in der Tabelle "Outlook-Synch.-Einheit" an.;
                           ENU=Specifies a unique identifier for each entry in the Outlook Synch. Entity table.];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine kurze Beschreibung der von Ihnen erstellten Synchronisierungseinheit an.;
                           ENU=Specifies a short description of the synchronization entity that you create.];
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Microsoft Dynamics NAV-Tabelle an, die mit einem Outlook-Element synchronisiert werden soll.;
                           ENU=Specifies the number of the Microsoft Dynamics NAV table that is to be synchronized with an Outlook item.];
                SourceExpr="Table No.";
                OnValidate=BEGIN
                             TableNoOnAfterValidate;
                           END;
                            }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Microsoft Dynamics NAV-Tabelle an, die synchronisiert werden soll. Dieses Feld wird automatisch ausgef�llt, wenn Sie eine Tabellennummer im Feld "Tabellennr." angeben.;
                           ENU=Specifies the name of the Microsoft Dynamics NAV table to synchronize. The program fills in this field every time you specify a table number in the Table No. field.];
                SourceExpr="Table Caption" }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Kriterien zum Definieren eines Satzes bestimmter Posten an, die im Synchronisierungsprozess verwendet werden sollen. Dieser Filter wird auf die Tabelle angewendet, die Sie im Feld "Tabellennr." angegeben haben. F�r diesen Filtertyp k�nnen Sie nur Microsoft Dynamics NAV-Filter vom Typ CONST und FILTER definieren.;
                           ENU=Specifies the criteria for defining a set of specific entries to use in the synchronization process. This filter is applied to the table you specified in the Table No. field. For this filter type, you will only be able to define Microsoft Dynamics NAV filters of the types CONST and FILTER.];
                SourceExpr=Condition;
                OnAssistEdit=BEGIN
                               Condition := COPYSTR(OSynchSetupMgt.ShowOSynchFiltersForm("Record GUID","Table No.",0),1,MAXSTRLEN(Condition));
                             END;
                              }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen des Outlook-Elements an, das der Microsoft Dynamics NAV-Tabelle entspricht, die Sie im Feld "Tabellennr." angegeben haben.;
                           ENU=Specifies the name of the Outlook item that corresponds to the Microsoft Dynamics NAV table which you specified in the Table No. field.];
                SourceExpr="Outlook Item";
                OnValidate=BEGIN
                             OutlookItemOnAfterValidate;
                           END;
                            }

    { 17  ;1   ;Part      ;
                Name=SynchEntityElements;
                SubPageLink=Synch. Entity Code=FIELD(Code);
                PagePartID=Page5301 }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      OSynchSetupMgt@1000 : Codeunit 5300;

    LOCAL PROCEDURE TableNoOnAfterValidate@19056468();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE OutlookItemOnAfterValidate@19055591();
    BEGIN
      CurrPage.UPDATE;
    END;

    BEGIN
    END.
  }
}

