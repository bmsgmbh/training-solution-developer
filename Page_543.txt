OBJECT Page 543 Default Dimension Priorities
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Vorgabedimension Priorit�ten;
               ENU=Default Dimension Priorities];
    SaveValues=Yes;
    SourceTable=Table354;
    DelayedInsert=Yes;
    SourceTableView=SORTING(Source Code,Priority);
    PageType=Worksheet;
    OnOpenPage=BEGIN
                 OpenSourceCode(CurrentSourceCode,Rec);
               END;

    OnAfterGetRecord=BEGIN
                       PriorityOnFormat(FORMAT(Priority));
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                CaptionML=[DEU=Allgemein;
                           ENU=General] }

    { 15  ;2   ;Field     ;
                Lookup=Yes;
                CaptionML=[DEU=Herkunftscode;
                           ENU=Source Code];
                SourceExpr=CurrentSourceCode;
                TableRelation="Source Code".Code;
                OnValidate=VAR
                             SourceCode@1001 : Record 230;
                           BEGIN
                             SourceCode.GET(CurrentSourceCode);
                             CurrentSourceCodeOnAfterValida;
                           END;

                OnLookup=BEGIN
                           CurrPage.SAVERECORD;
                           LookupSourceCode(CurrentSourceCode,Rec);
                           CurrPage.UPDATE(FALSE);
                         END;
                          }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Tabellen-ID f�r die Kontoart fest, wenn Sie einer Kontenart eine Priorit�t zuordnen m�chten.;
                           ENU=Specifies the table ID for the account type, if you want to prioritize an account type.];
                SourceExpr="Table ID";
                OnValidate=BEGIN
                             TableIDOnAfterValidate;
                           END;
                            }

    { 6   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[DEU=Gibt den Tabellennamen der Kontenart an, der eine Priorit�t zugeordnet werden soll.;
                           ENU=Specifies the table name for the account type you wish to prioritize.];
                SourceExpr="Table Caption" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Priorit�t einer Kontenart fest. Die h�chste Priorit�t lautet 1.;
                           ENU=Specifies the priority of an account type, with the highest priority being 1.];
                SourceExpr=Priority;
                OnValidate=BEGIN
                             PriorityOnAfterValidate;
                           END;
                            }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'DEU=<auto>;ENU=<auto>';
      Text001@1001 : TextConst 'DEU=Sie m�ssen einen Herkunftscode angeben.;ENU=You need to define a source code.';
      CurrentSourceCode@1002 : Code[20];

    LOCAL PROCEDURE OpenSourceCode@2(VAR CurrentSourceCode@1000 : Code[20];VAR DefaultDimPriority@1001 : Record 354);
    BEGIN
      CheckSourceCode(CurrentSourceCode);
      WITH DefaultDimPriority DO BEGIN
        FILTERGROUP := 2;
        SETRANGE("Source Code",CurrentSourceCode);
        FILTERGROUP := 0;
      END;
    END;

    LOCAL PROCEDURE CheckSourceCode@7(VAR CurrentSourceCode@1000 : Code[20]);
    VAR
      SourceCode@1001 : Record 230;
    BEGIN
      IF NOT SourceCode.GET(CurrentSourceCode) THEN
        IF SourceCode.FINDFIRST THEN
          CurrentSourceCode := SourceCode.Code
        ELSE
          ERROR(Text001);
    END;

    PROCEDURE SetSourceCode@5(CurrentSourceCode@1000 : Code[20];VAR DefaultDimPriority@1001 : Record 354);
    BEGIN
      WITH DefaultDimPriority DO BEGIN
        FILTERGROUP := 2;
        SETRANGE("Source Code",CurrentSourceCode);
        FILTERGROUP := 0;
        IF FIND('-') THEN;
      END;
    END;

    LOCAL PROCEDURE LookupSourceCode@6(VAR CurrentSourceCode@1000 : Code[20];VAR DefaultDimPriority@1001 : Record 354);
    VAR
      SourceCode@1002 : Record 230;
    BEGIN
      COMMIT;
      SourceCode.Code := DefaultDimPriority.GETRANGEMAX("Source Code");
      IF PAGE.RUNMODAL(0,SourceCode) = ACTION::LookupOK THEN BEGIN
        CurrentSourceCode := SourceCode.Code;
        SetSourceCode(CurrentSourceCode,DefaultDimPriority);
      END;
    END;

    LOCAL PROCEDURE TableIDOnAfterValidate@19033357();
    BEGIN
      CALCFIELDS("Table Caption");
    END;

    LOCAL PROCEDURE PriorityOnAfterValidate@19076862();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE CurrentSourceCodeOnAfterValida@19035286();
    BEGIN
      CurrPage.SAVERECORD;
      SetSourceCode(CurrentSourceCode,Rec);
      CurrPage.UPDATE(FALSE);
    END;

    LOCAL PROCEDURE PriorityOnFormat@19057707(Text@19033532 : Text[1024]);
    BEGIN
      IF Priority = 0 THEN
        Text := Text000;
    END;

    BEGIN
    END.
  }
}

