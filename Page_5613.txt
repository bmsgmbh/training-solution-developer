OBJECT Page 5613 FA Posting Groups
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Anlagenbuchungsgruppen;
               ENU=FA Posting Groups];
    SourceTable=Table5606;
    PageType=List;
    CardPageID=FA Posting Group Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 57      ;1   ;ActionGroup;
                      CaptionML=[DEU=B&uchungsgruppe;
                                 ENU=P&osting Gr.];
                      Image=Group }
      { 78      ;2   ;ActionGroup;
                      CaptionML=[DEU=Verteilungen;
                                 ENU=Allocations];
                      Image=Allocate }
      { 73      ;3   ;Action    ;
                      Name=Acquisition;
                      CaptionML=[DEU=&Anschaffung;
                                 ENU=&Acquisition];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilung, die auf Anschaffungen zutrifft.;
                                 ENU=View or edit the FA allocation that apply to acquisitions.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST(Acquisition);
                      Image=Allocate }
      { 79      ;3   ;Action    ;
                      Name=Depreciation;
                      CaptionML=[DEU=Normal-A&fA;
                                 ENU=&Depreciation];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilung, die auf Abschreibungen zutrifft.;
                                 ENU=View or edit the FA allocation that apply to depreciations.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST(Depreciation);
                      Image=Allocate }
      { 80      ;3   ;Action    ;
                      Name=WriteDown;
                      CaptionML=[DEU=&Erh�hte AfA;
                                 ENU=&Write-Down];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilung, die auf AfA zutrifft.;
                                 ENU=View or edit the FA allocation that apply to write-downs.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST(Write-Down);
                      Image=Allocate }
      { 74      ;3   ;Action    ;
                      Name=Appreciation;
                      CaptionML=[DEU=&Zuschreibung;
                                 ENU=Appr&eciation];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilungen, die auf Abschreibungen zutreffen.;
                                 ENU=View or edit the FA allocations that apply to appreciations.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST(Appreciation);
                      Image=Allocate }
      { 81      ;3   ;Action    ;
                      Name=Custom1;
                      CaptionML=[DEU=&Sonder-AfA;
                                 ENU=&Custom 1];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilung, die auf benutzerdefinierte Werte zutrifft.;
                                 ENU=View or edit the FA allocation that apply to custom values.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST(Custom 1);
                      Image=Allocate }
      { 82      ;3   ;Action    ;
                      Name=Custom2;
                      CaptionML=[DEU=&Benutzerdef. 2;
                                 ENU=C&ustom 2];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilung, die auf benutzerdefinierte Werte zutrifft.;
                                 ENU=View or edit the FA allocation that apply to custom values.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST(Custom 2);
                      Image=Allocate }
      { 83      ;3   ;Action    ;
                      Name=Disposal;
                      CaptionML=[DEU=&Verkauf;
                                 ENU=Disp&osal];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilung, die auf Abg�nge zutrifft.;
                                 ENU=View or edit the FA allocation that apply to disposals.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST(Disposal);
                      Image=Allocate }
      { 96      ;3   ;Action    ;
                      Name=Maintenance;
                      CaptionML=[DEU=Wartung;
                                 ENU=Maintenance];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilungen, die auf die Wartung zutreffen.;
                                 ENU=View or edit the FA allocations that apply to maintenance.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST(Maintenance);
                      Image=Allocate }
      { 97      ;3   ;Action    ;
                      Name=Gain;
                      CaptionML=[DEU=Gewinn;
                                 ENU=Gain];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilungen, die auf Gewinne zutreffen.;
                                 ENU=View or edit the FA allocations that apply to gains.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST(Gain);
                      Image=Allocate }
      { 98      ;3   ;Action    ;
                      Name=Loss;
                      CaptionML=[DEU=Verlust;
                                 ENU=Loss];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilungen, die auf Verluste zutreffen.;
                                 ENU=View or edit the FA allocations that apply to losses.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST(Loss);
                      Image=Allocate }
      { 99      ;3   ;Action    ;
                      Name=BookValueGain;
                      CaptionML=[DEU=Buchwert (Gewinn);
                                 ENU=Book Value (Gain)];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilungen, die auf Buchwertgewinne zutreffen.;
                                 ENU=View or edit the FA allocations that apply to book value gains.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST("Book Value (Gain)");
                      Image=Allocate }
      { 100     ;3   ;Action    ;
                      Name=BookValueLoss;
                      CaptionML=[DEU=Bu&chwert (Verlust);
                                 ENU=Book &Value (Loss)];
                      ToolTipML=[DEU=Zeigt oder bearbeitet die Anlagenverteilungen, die auf Buchwertverluste zutreffen.;
                                 ENU=View or edit the FA allocations that apply to book value losses.];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5623;
                      RunPageLink=Code=FIELD(Code),
                                  Allocation Type=CONST("Book Value (Loss)");
                      Image=Allocate }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code f�r die Anlagenbuchungsgruppe an.;
                           ENU=Specifies a fixed asset posting group code.];
                ApplicationArea=#FixedAssets;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen der Anschaffungskosten f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post acquisition cost for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Acquisition Cost Account" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer an, auf die kumulierte Abschreibungen gebucht werden sollen, wenn Sie Abschreibungen f�r Anlagen buchen.;
                           ENU=Specifies the general ledger account number to post accumulated depreciation to when you post depreciation for fixed assets.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Accum. Depreciation Account" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen s�mtlicher AfA f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post any write-downs for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Write-Down Account";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von Abschreibungstransaktionen f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post appreciation transactions for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Appreciation Account";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von Sonderabschreibungstransaktionen f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post Custom-1 transactions for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Custom 1 Account";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von benutzerdefinierten Abschreibungstransaktionen f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post Custom-2 transactions for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Custom 2 Account";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen der Anschaffungskosten beim Verkauf von Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post acquisition cost to when you dispose of fixed assets in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Acq. Cost Acc. on Disposal" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen der kumulierten Abschreibungen beim Verkauf von Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post accumulated depreciation to when you dispose of fixed assets in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Accum. Depr. Acc. on Disposal" }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen der AfA von Anlagen beim Verkauf von Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post write-downs of fixed assets to when you dispose of fixed assets in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Write-Down Acc. on Disposal";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen der Zuschreibungen beim Verkauf von Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post appreciation to when you dispose of fixed assets in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Appreciation Acc. on Disposal";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen der Sonderabschreibungstransaktionen beim Verkauf von Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post Custom-1 transactions to when you dispose of fixed assets in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Custom 1 Account on Disposal";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen der benutzerdefinierten Abschreibungstransaktionen beim Verkauf von Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post Custom-2 transactions to when you dispose of fixed assets in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Custom 2 Account on Disposal";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von Gewinnen beim Verkauf von Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post any gains to when you dispose of fixed assets in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Gains Acc. on Disposal" }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen von Verlusten beim Verkauf von Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post any losses to when you dispose of fixed assets in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Losses Acc. on Disposal" }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer an, unter die Anwendung den Buchwert von Anlagen beim Verkauf von Anlagen als Gewinn auf den Buchwert buchen soll.;
                           ENU=Specifies the G/L account number you want the program to post assets' book value to when you dispose of fixed assets at a gain on book value.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Book Val. Acc. on Disp. (Gain)";
                Visible=FALSE }

    { 103 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer an, unter der der Buchwert der Anlagen beim Verkauf von Anlagen als Verlust auf den Buchwert gebucht werden soll.;
                           ENU=Specifies the G/L account number to which to post assets' book value, when you dispose of fixed assets at a loss on book value.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Book Val. Acc. on Disp. (Loss)";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer an, unter der Erl�se beim Verkauf von Anlagen als Gewinn auf den Buchwert gebucht werden sollen.;
                           ENU=Specifies the G/L account number you want to post sales to when you dispose of fixed assets at a gain on book value.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Sales Acc. on Disp. (Gain)";
                Visible=FALSE }

    { 101 ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer an, unter der Erl�se beim Verkauf von Anlagen als Verlust auf den Buchwert gebucht werden sollen.;
                           ENU=Specifies the G/L account number to which you want to post sales, when you dispose of fixed assets at a loss on book value.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Sales Acc. on Disp. (Loss)";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gegenkontonummer zum Buchen der AfA von Anlagen beim Verkauf von Anlagen an.;
                           ENU=Specifies the general ledger balancing account number to post write-downs of fixed assets to when you dispose of fixed assets.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Write-Down Bal. Acc. on Disp.";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gegenkontonummer zum Buchen der Zuschreibungstransaktionen von Anlagen beim Verkauf von Anlagen an.;
                           ENU=Specifies the general ledger balancing account number to post appreciation transactions of fixed assets to when you dispose of fixed assets.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Apprec. Bal. Acc. on Disp.";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gegenkontonummer zum Buchen der Sonderabschreibungstransaktionen von Anlagen beim Verkauf von Anlagen an.;
                           ENU=Specifies the general ledger balancing account number to post custom-1 transactions of fixed assets to when you dispose of fixed assets.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Custom 1 Bal. Acc. on Disposal";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gegenkontonummer zum Buchen der benutzerdefinierten Abschreibungstransaktionen von Anlagen beim Verkauf von Anlagen an.;
                           ENU=Specifies the general ledger balancing account number to post custom-2 transactions of fixed assets to when you dispose of fixed assets.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Custom 2 Bal. Acc. on Disposal";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen der Wartungskosten f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post maintenance expenses for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Maintenance Expense Account" }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gegenkontonummer zum Buchen der Wartungskosten f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger balancing account number to post maintenance expenses for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Maintenance Bal. Acc.";
                Visible=FALSE }

    { 55  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Gegenkontos zum Buchen der Anschaffungskosten f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger balancing account number to post acquisition cost for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Acquisition Cost Bal. Acc." }

    { 59  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Sachkontonummer zum Buchen der Abschreibungsausgaben f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger account number to post depreciation expense for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Depreciation Expense Acc." }

    { 61  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gegenkontonummer zum Buchen der AfA f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger balancing account number to post write-downs for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Write-Down Expense Acc.";
                Visible=FALSE }

    { 63  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gegenkontonummer zum Buchen der Zuschreibungen f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger balancing account number to post appreciation for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Appreciation Bal. Account";
                Visible=FALSE }

    { 65  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gegenkontonummer zum Buchen von Sonderabschreibungstransaktionen f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger balancing account number to post custom-1 transactions for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Custom 1 Expense Acc.";
                Visible=FALSE }

    { 67  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gegenkontonummer zum Buchen von benutzerdefinierten Abschreibungstransaktionen f�r Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger balancing account number to post custom-2 transactions for fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Custom 2 Expense Acc.";
                Visible=FALSE }

    { 69  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Gegenkonto zum Buchen von Erl�sen beim Verkauf von Anlagen in dieser Buchungsgruppe an.;
                           ENU=Specifies the general ledger balancing account to post sales when you dispose of fixed assets to in this posting group.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Sales Bal. Acc.";
                Visible=FALSE }

    { 71  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtprozentsatz der Anschaffungskosten an, der beim Buchen der Anschaffungskosten verteilt werden kann.;
                           ENU=Specifies the total percentage of acquisition cost that can be allocated when acquisition cost is posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allocated Acquisition Cost %";
                Visible=FALSE }

    { 75  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtprozentsatz der Abschreibungen an, der beim Buchen der Abschreibungen verteilt werden kann.;
                           ENU=Specifies the total percentage of depreciation that can be allocated when depreciation is posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allocated Depreciation %";
                Visible=FALSE }

    { 84  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtprozentsatz der AfA-Transaktionen an, der beim Buchen der AfA-Transaktionen verteilt werden kann.;
                           ENU=Specifies the total percentage for write-down transactions that can be allocated when write-down transactions are posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allocated Write-Down %";
                Visible=FALSE }

    { 86  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtprozentsatz der Zuschreibungstransaktionen an, der beim Buchen der Zuschreibungstransaktionen verteilt werden kann.;
                           ENU=Specifies the total percentage for appreciation transactions that can be allocated when appreciation transactions are posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allocated Appreciation %";
                Visible=FALSE }

    { 88  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtprozentsatz der Sonderabschreibungstransaktionen an, der beim Buchen der Sonderabschreibungstransaktionen verteilt werden kann.;
                           ENU=Specifies the total percentage for custom-1 transactions that can be allocated when custom-1 transactions are posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allocated Custom 1 %";
                Visible=FALSE }

    { 90  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtprozentsatz der benutzerdefinierten Abschreibungstransaktionen an, der beim Buchen der benutzerdefinierten Abschreibungstransaktionen verteilt werden kann.;
                           ENU=Specifies the total percentage for custom-2 transactions that can be allocated when custom-2 transactions are posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allocated Custom 2 %";
                Visible=FALSE }

    { 92  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtprozentsatz des Verkaufspreises an, der beim Buchen der Erl�se verteilt werden kann.;
                           ENU=Specifies the total percentage of sales price that can be allocated when sales are posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allocated Sales Price %";
                Visible=FALSE }

    { 94  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Gesamtprozentsatz der Wartungstransaktionen an, der beim Buchen der Wartungstransaktionen verteilt werden kann.;
                           ENU=Specifies the total percentage for maintenance transactions that can be allocated when maintenance transactions are posted.];
                ApplicationArea=#FixedAssets;
                SourceExpr="Allocated Maintenance %";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

