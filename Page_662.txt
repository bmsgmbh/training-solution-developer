OBJECT Page 662 Approval Request Entries
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Genehmigungsanforderungsposten;
               ENU=Approval Request Entries];
    SourceTable=Table454;
    PageType=List;
    OnOpenPage=BEGIN
                 IF Usersetup.GET(USERID) THEN
                   IF NOT Usersetup."Approval Administrator" THEN BEGIN
                     FILTERGROUP(2);
                     SETCURRENTKEY("Sender ID");
                     SETFILTER("Sender ID",'=%1',Usersetup."User ID");
                     FILTERGROUP(0);
                   END;

                 SETRANGE(Status);
                 SETRANGE("Due Date");
               END;

    OnAfterGetRecord=BEGIN
                       Overdue := Overdue::" ";
                       IF FormatField(Rec) THEN
                         Overdue := Overdue::Yes;

                       RecordIDText := FORMAT("Record ID to Approve",0,1);
                     END;

    OnAfterGetCurrRecord=VAR
                           RecRef@1000 : RecordRef;
                         BEGIN
                           ShowRecCommentsEnabled := RecRef.GET("Record ID to Approve");
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 33      ;1   ;ActionGroup;
                      CaptionML=[DEU=An&zeigen;
                                 ENU=&Show];
                      Image=View }
      { 35      ;2   ;Action    ;
                      CaptionML=[DEU=Datensatz;
                                 ENU=Record];
                      ToolTipML=[DEU=�ffnet den Beleg, die Buch.-Blattzeile oder Karte, f�r den/die die Genehmigungsanforderung ist.;
                                 ENU=Open the document, journal line, or card that the approval request is for.];
                      ApplicationArea=#Suite;
                      Promoted=Yes;
                      Enabled=ShowRecCommentsEnabled;
                      PromotedIsBig=Yes;
                      Image=Document;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowRecord;
                               END;
                                }
      { 36      ;2   ;Action    ;
                      Name=Comments;
                      CaptionML=[DEU=Bemerkungen;
                                 ENU=Comments];
                      ToolTipML=[DEU=Zeigt Bemerkungen an oder f�gt diese hinzu.;
                                 ENU=View or add comments.];
                      ApplicationArea=#Suite;
                      Promoted=Yes;
                      Enabled=ShowRecCommentsEnabled;
                      PromotedIsBig=Yes;
                      Image=ViewComments;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ApprovalsMgmt@1000 : Codeunit 1535;
                                 RecRef@1001 : RecordRef;
                               BEGIN
                                 RecRef.GET("Record ID to Approve");
                                 ApprovalsMgmt.GetApprovalComment(RecRef);
                               END;
                                }
      { 39      ;2   ;Action    ;
                      CaptionML=[DEU=&F�llige Posten;
                                 ENU=O&verdue Entries];
                      ToolTipML=[DEU=Zeigt an, dass Genehmigungsanforderungen �berf�llig sind.;
                                 ENU=View approval requests that are overdue.];
                      ApplicationArea=#Suite;
                      Image=OverdueEntries;
                      OnAction=BEGIN
                                 SETFILTER(Status,'%1|%2',Status::Created,Status::Open);
                                 SETFILTER("Due Date",'<%1',TODAY);
                               END;
                                }
      { 40      ;2   ;Action    ;
                      CaptionML=[DEU=Alle Posten;
                                 ENU=All Entries];
                      ToolTipML=[DEU=Zeigen Sie alle Genehmigungsposten an.;
                                 ENU=View all approval entries.];
                      ApplicationArea=#Suite;
                      Image=Entries;
                      OnAction=BEGIN
                                 SETRANGE(Status);
                                 SETRANGE("Due Date");
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 25      ;1   ;Action    ;
                      CaptionML=[DEU=&Delegieren;
                                 ENU=&Delegate];
                      ToolTipML=[DEU=Delegieren Sie die Genehmigungsanfrage an einen anderen Genehmiger, der als Ihr Ersatzgenehmiger eingerichtet ist.;
                                 ENU=Delegate the approval request to another approver that has been set up as your substitute approver.];
                      ApplicationArea=#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Delegate;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ApprovalEntry@1001 : Record 454;
                                 ApprovalsMgmt@1000 : Codeunit 1535;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(ApprovalEntry);
                                 ApprovalsMgmt.DelegateApprovalRequests(ApprovalEntry)
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 37  ;2   ;Field     ;
                CaptionML=[DEU=�berf�llig;
                           ENU=Overdue];
                ToolTipML=[DEU=Legt fest, dass die Genehmigung �berf�llig ist.;
                           ENU=Specifies that the approval is overdue.];
                ApplicationArea=#Suite;
                SourceExpr=Overdue;
                Editable=False }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Dieses Feld wird intern verwendet.;
                           ENU=This field is used internally.];
                SourceExpr="Table ID";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt ist die Art des Belegs an, f�r den ein Genehmigungsposten erstellt wurde. Genehmigungsposten k�nnen f�r die folgenden sechs unterschiedlichen Arten von Verkaufs- oder Einkaufsbelegen erstellt werden:;
                           ENU=Specifies the type of document that an approval entry has been created for. Approval entries can be created for six different types of sales or purchase documents:];
                SourceExpr="Document Type";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Belegnummer fest, die aus dem entsprechenden Verkaufs- oder Einkaufsbeleg (zum Beispiel eine Bestellung oder ein Verkaufsangebot) kopiert wird.;
                           ENU=Specifies the document number copied from the relevant sales or purchase document, such as a purchase order or a sales quote.];
                SourceExpr="Document No.";
                Visible=FALSE }

    { 3   ;2   ;Field     ;
                CaptionML=[DEU=Zu genehmigen;
                           ENU=To Approve];
                ToolTipML=[DEU=Gibt den Datensatz an, den Sie genehmigen sollen.;
                           ENU=Specifies the record that you are requested to approve.];
                ApplicationArea=#Suite;
                SourceExpr=RecordIDText }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Legt die Reihenfolge der Genehmiger fest, wenn mehrere Genehmiger an einem Genehmigungsworkflow beteiligt sind.;
                           ENU=Specifies the order of approvers when an approval workflow involves more than one approver.];
                ApplicationArea=#Suite;
                SourceExpr="Sequence No." }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der die Genehmigungsanforderung f�r den Beleg gesendet hat.;
                           ENU=Specifies the ID of the user who sent the approval request for the document to be approved.];
                ApplicationArea=#Suite;
                SourceExpr="Sender ID" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Legt den Code des Verk�ufers oder Eink�ufers fest, der in dem zu genehmigenden Beleg angegeben ist. Dies ist kein obligatorisches Feld. Es ist jedoch n�tzlich, wenn ein f�r den Debitor/Kreditor verantwortlicher Verk�ufer oder Eink�ufer einen Beleg genehmigen muss, bevor dieser verarbeitet wird.;
                           ENU=Specifies the code for the salesperson or purchaser that was in the document to be approved. It is not a mandatory field, but is useful if a salesperson or a purchaser responsible for the customer/vendor needs to approve the document before it is processed.];
                ApplicationArea=#Suite;
                SourceExpr="Salespers./Purch. Code" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der den Beleg genehmigen muss (der Genehmiger).;
                           ENU=Specifies the ID of the user who must approve the document (the Approver).];
                ApplicationArea=#Suite;
                SourceExpr="Approver ID" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Genehmigungsstatus f�r den Posten an:;
                           ENU=Specifies the approval status for the entry:];
                ApplicationArea=#Suite;
                SourceExpr=Status }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum und die Uhrzeit der Versendung des Belegs zur Genehmigung an.;
                           ENU=Specifies the date and the time that the document was sent for approval.];
                ApplicationArea=#Suite;
                SourceExpr="Date-Time Sent for Approval" }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum der letzten �nderung des Genehmigungspostens an. Wurde die Beleggenehmigung beispielsweise storniert, wird das Feld entsprechend aktualisiert.;
                           ENU=Specifies the date when the approval entry was last modified. If, for example, the document approval is canceled, this field will be updated accordingly.];
                ApplicationArea=#Suite;
                SourceExpr="Last Date-Time Modified" }

    { 27  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der den Genehmigungsposten zuletzt ge�ndert hat. Wurde die Beleggenehmigung beispielsweise storniert, wird das Feld entsprechend aktualisiert.;
                           ENU=Specifies the ID of the user who last modified the approval entry. If, for example, the document approval is canceled, this field will be updated accordingly.];
                ApplicationArea=#Suite;
                SourceExpr="Last Modified By User ID" }

    { 29  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob Bemerkungen bez�glich der Genehmigung des Datensatzes vorhanden sind. Wenn Sie die Bemerkungen lesen m�chten, w�hlen Sie das Feld, um das Fenster "Genehmigung Bemerkungen" zu �ffnen.;
                           ENU=Specifies whether there are comments relating to the approval of the record. If you want to read the comments, choose the field to open the Approval Comment Sheet window.];
                ApplicationArea=#Suite;
                SourceExpr=Comment }

    { 31  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wann der Datensatz von mindestens einem Genehmiger genehmigt werden muss.;
                           ENU=Specifies when the record must be approved, by one or more approvers.];
                ApplicationArea=#Suite;
                SourceExpr="Due Date" }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den verbleibenden Kredit (in MW) f�r den Debitor an.;
                           ENU=Specifies the remaining credit (in LCY) that exists for the customer.];
                ApplicationArea=#Suite;
                SourceExpr="Available Credit Limit (LCY)" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Usersetup@1000 : Record 91;
      Overdue@1005 : 'Yes, ';
      RecordIDText@1001 : Text;
      ShowRecCommentsEnabled@1002 : Boolean;

    LOCAL PROCEDURE FormatField@1(Rec@1000 : Record 454) : Boolean;
    BEGIN
      IF Status IN [Status::Created,Status::Open] THEN BEGIN
        IF Rec."Due Date" < TODAY THEN
          EXIT(TRUE);

        EXIT(FALSE);
      END;
    END;

    BEGIN
    END.
  }
}

