OBJECT Page 290 Recurring Resource Jnl.
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Wiederk. Res. Buch.-Blatt;
               ENU=Recurring Resource Jnl.];
    SaveValues=Yes;
    SourceTable=Table207;
    DelayedInsert=Yes;
    DataCaptionFields=Journal Batch Name;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnOpenPage=VAR
                 JnlSelected@1000 : Boolean;
               BEGIN
                 IF IsOpenedFromBatch THEN BEGIN
                   CurrentJnlBatchName := "Journal Batch Name";
                   ResJnlManagement.OpenJnl(CurrentJnlBatchName,Rec);
                   EXIT;
                 END;
                 ResJnlManagement.TemplateSelection(PAGE::"Recurring Resource Jnl.",TRUE,Rec,JnlSelected);
                 IF NOT JnlSelected THEN
                   ERROR('');
                 ResJnlManagement.OpenJnl(CurrentJnlBatchName,Rec);
               END;

    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                     END;

    OnNewRecord=BEGIN
                  SetUpNewLine(xRec);
                  CLEAR(ShortcutDimCode);
                END;

    OnAfterGetCurrRecord=BEGIN
                           ResJnlManagement.GetRes("Resource No.",ResName);
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 65      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 66      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Zeigt Dimensionen an oder bearbeitet sie, wie etwa einen Bereich, ein Projekt oder eine Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      ApplicationArea=#Jobs;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 49      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Ressource;
                                 ENU=&Resource];
                      Image=Resource }
      { 53      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Karte;
                                 ENU=Card];
                      ToolTipML=[DEU=Zeigt detaillierte Informationen �ber den in der Buch.-Blattzeile verarbeiteten Datensatz an oder �ndert sie.;
                                 ENU=View or change detailed information about the record that is being processed on the journal line.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 76;
                      RunPageLink=No.=FIELD(Resource No.);
                      Image=EditLines }
      { 54      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=&Posten;
                                 ENU=Ledger E&ntries];
                      ToolTipML=[DEU=Zeigt die Historie der Transaktionen an, die f�r den ausgew�hlten Datensatz gebucht wurden.;
                                 ENU=View the history of transactions that have been posted for the selected record.];
                      ApplicationArea=#Jobs;
                      RunObject=Page 202;
                      RunPageView=SORTING(Resource No.);
                      RunPageLink=Resource No.=FIELD(Resource No.);
                      Promoted=No;
                      Image=ResourceLedger;
                      PromotedCategory=Process }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 48      ;1   ;ActionGroup;
                      CaptionML=[DEU=Bu&chung;
                                 ENU=P&osting];
                      Image=Post }
      { 50      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Testbericht;
                                 ENU=Test Report];
                      ToolTipML=[DEU=Zeigt einen Testbericht an, um m�gliche Fehler vor dem tats�chlichen Buchen des Buch.-Blatts oder Belegs finden und korrigieren zu k�nnen.;
                                 ENU=View a test report so that you can find and correct any errors before you perform the actual posting of the journal or document.];
                      ApplicationArea=#Jobs;
                      Image=TestReport;
                      OnAction=BEGIN
                                 ReportPrint.PrintResJnlLine(Rec);
                               END;
                                }
      { 51      ;2   ;Action    ;
                      ShortCutKey=F9;
                      CaptionML=[DEU=Bu&chen;
                                 ENU=P&ost];
                      ToolTipML=[DEU=Schlie�t den Beleg oder das Buch.-Blatt durch Buchen der Betr�ge und Mengen auf den entsprechenden Konten in den Firmenb�chern ab.;
                                 ENU=Finalize the document or journal by posting the amounts and quantities to the related accounts in your company books.];
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Res. Jnl.-Post",Rec);
                                 CurrentJnlBatchName := GETRANGEMAX("Journal Batch Name");
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 52      ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      CaptionML=[DEU=Buchen und d&rucken;
                                 ENU=Post and &Print];
                      ToolTipML=[DEU=Schlie�t den Beleg oder das Buch.-Blatt ab und bereitet das Drucken vor. Die Werte und Mengen werden auf den entsprechenden Konten gebucht. Ein Berichtanforderungsfenster wird ge�ffnet, in dem Sie angeben k�nnen, was auf dem Ausdruck enthalten sein soll.;
                                 ENU=Finalize and prepare to print the document or journal. The values and quantities are posted to the related accounts. A report request window where you can specify what to include on the print-out.];
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Res. Jnl.-Post+Print",Rec);
                                 CurrentJnlBatchName := GETRANGEMAX("Journal Batch Name");
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 39  ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[DEU=Buch.-Blattname;
                           ENU=Batch Name];
                ToolTipML=[DEU=Gibt den Namen des Buch.-Blattnamens an.;
                           ENU=Specifies the name of the journal batch.];
                ApplicationArea=#Jobs;
                SourceExpr=CurrentJnlBatchName;
                OnValidate=BEGIN
                             ResJnlManagement.CheckName(CurrentJnlBatchName,Rec);
                             CurrentJnlBatchNameOnAfterVali;
                           END;

                OnLookup=BEGIN
                           CurrPage.SAVERECORD;
                           ResJnlManagement.LookupName(CurrentJnlBatchName,Rec);
                           CurrPage.UPDATE(FALSE);
                         END;
                          }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, was mit den Mengenangaben in der Buch.-Blattzeile nach der Buchung geschieht.;
                           ENU=Specifies what happens to the quantity on the journal line after posting.];
                ApplicationArea=#Jobs;
                SourceExpr="Recurring Method" }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Wiederholungsrate an, wenn Sie im Feld "Wiederkehrend" in der Res. Buch.-Blattvorlage angegeben haben, dass das Buch.-Blatt wiederkehrend ist.;
                           ENU=Specifies a recurring frequency if you have indicated in the Recurring field of the Res. Journal Template the journal is recurring.];
                ApplicationArea=#Jobs;
                SourceExpr="Recurring Frequency" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum der Zuweisung an.;
                           ENU=Specifies the date when you want to assign.];
                ApplicationArea=#Jobs;
                SourceExpr="Posting Date" }

    { 59  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum des Belegs an, der als Grundlage f�r den Posten in der Ressourcen Buch.-Blattzeile dient.;
                           ENU=Specifies the date on the document that provides the basis for the entry on the resource journal line.];
                ApplicationArea=#Jobs;
                SourceExpr="Document Date";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Postenart f�r jede Zeile an.;
                           ENU=Specifies an entry type for each line.];
                ApplicationArea=#Jobs;
                SourceExpr="Entry Type" }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Belegnummer f�r die Buch.-Blattzeile an.;
                           ENU=Specifies a document number for the journal line.];
                ApplicationArea=#Jobs;
                SourceExpr="Document No." }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Ressource an, f�r die ein Posten gebucht werden soll.;
                           ENU=Specifies the number of the resource that you want to post an entry for.];
                ApplicationArea=#Jobs;
                SourceExpr="Resource No.";
                OnValidate=BEGIN
                             ResJnlManagement.GetRes("Resource No.",ResName);
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 55  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Ressourcengruppe an, der diese Ressource zugewiesen ist.;
                           ENU=Specifies the resource group that this resource is assigned to.];
                ApplicationArea=#Jobs;
                SourceExpr="Resource Group No.";
                Visible=FALSE;
                OnValidate=BEGIN
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung oder den Namen der im Feld "Ressourcennr." ausgew�hlten Ressource an.;
                           ENU=Specifies the description or name of the resource you chose in the Resource No. field.];
                ApplicationArea=#Jobs;
                SourceExpr=Description }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Projektnummer an, die mit dem Posten verkn�pft ist.;
                           ENU=Specifies the job number associated with the entry.];
                ApplicationArea=#Jobs;
                SourceExpr="Job No.";
                Visible=FALSE;
                OnValidate=BEGIN
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 30  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Shortcutdimension 1 an.;
                           ENU=Specifies the code for Shortcut Dimension 1.];
                ApplicationArea=#Jobs;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Shortcutdimension 2 an.;
                           ENU=Specifies the code for Shortcut Dimension 2.];
                ApplicationArea=#Jobs;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(3,ShortcutDimCode[3]);
                           END;
                            }

    { 302 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(4,ShortcutDimCode[4]);
                           END;
                            }

    { 304 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(5,ShortcutDimCode[5]);
                           END;
                            }

    { 306 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(6,ShortcutDimCode[6]);
                           END;
                            }

    { 308 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(7,ShortcutDimCode[7]);
                           END;
                            }

    { 310 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(8,ShortcutDimCode[8]);
                           END;
                            }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, zu welchem Arbeitstyp die Ressource geh�rt. Die Preise werden auf Grundlage dieses Postens aktualisiert.;
                           ENU=Specifies which work type the resource applies to. Prices are updated based on this entry.];
                ApplicationArea=#Jobs;
                SourceExpr="Work Type Code" }

    { 61  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Gesch�ftsbuchungsgruppe an, die bei der Buchung des Postens in der Buch.-Blattzeile verwendet wird.;
                           ENU=Specifies the general business posting group that will be used when you post the entry on the journal line.];
                ApplicationArea=#Jobs;
                SourceExpr="Gen. Bus. Posting Group";
                Visible=FALSE }

    { 63  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Produktbuchungsgruppe an, die bei der Buchung des Postens in der Buch.-Blattzeile verwendet wird.;
                           ENU=Specifies the general product posting group that will be used when you post the entry on the journal line.];
                ApplicationArea=#Jobs;
                SourceExpr="Gen. Prod. Posting Group";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie viele Einheiten Sie buchen m�chten.;
                           ENU=Specifies the quantity of units you intend to post.];
                ApplicationArea=#Jobs;
                SourceExpr=Quantity }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, wie die Ressource gemessen wird.;
                           ENU=Specifies how the resource is measured.];
                ApplicationArea=#Jobs;
                SourceExpr="Unit of Measure Code" }

    { 57  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den EK-Preis an. Als Vorgabewert wird der Betrag aus der Ressourcenkarte oder aus der Tabelle "Ressourcen-EK-Preis" abgerufen.;
                           ENU=Specifies the direct unit cost. As a default value, the amount is retrieved from the resource card or Resource Cost table.];
                ApplicationArea=#Jobs;
                SourceExpr="Direct Unit Cost" }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einstandspreis an.;
                           ENU=Specifies the unit cost.];
                ApplicationArea=#Jobs;
                SourceExpr="Unit Cost" }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einstandsbetrag f�r diese Buch.-Blattzeile an.;
                           ENU=Specifies the total cost for this journal line.];
                ApplicationArea=#Jobs;
                SourceExpr="Total Cost" }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den VK-Preis an.;
                           ENU=Specifies the unit price.];
                ApplicationArea=#Jobs;
                SourceExpr="Unit Price" }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Verkaufsbetrag in der Buch.-Blattzeile an.;
                           ENU=Specifies the total price on the journal line.];
                ApplicationArea=#Jobs;
                SourceExpr="Total Price" }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ursachencode an, der in die Buch.-Blattzeilen eingegeben wurde.;
                           ENU=Specifies the reason code that has been entered on the journal lines.];
                ApplicationArea=#Jobs;
                SourceExpr="Reason Code";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das letzte Datum an, an dem ein Wiederk. Buch.-Blatt gebucht werden kann.;
                           ENU=Specifies the last date when a recurring journal can be posted.];
                ApplicationArea=#Jobs;
                SourceExpr="Expiration Date" }

    { 41  ;1   ;Group      }

    { 1903222401;2;Group  ;
                GroupType=FixedLayout }

    { 1901652401;3;Group  ;
                CaptionML=[DEU=Ressourcenname;
                           ENU=Resource Name] }

    { 43  ;4   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ResName;
                Editable=FALSE;
                ShowCaption=No }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ResJnlManagement@1000 : Codeunit 270;
      ReportPrint@1001 : Codeunit 228;
      CurrentJnlBatchName@1002 : Code[10];
      ResName@1003 : Text[50];
      ShortcutDimCode@1004 : ARRAY [8] OF Code[20];

    LOCAL PROCEDURE CurrentJnlBatchNameOnAfterVali@19002411();
    BEGIN
      CurrPage.SAVERECORD;
      ResJnlManagement.SetName(CurrentJnlBatchName,Rec);
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

