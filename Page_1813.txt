OBJECT Page 1813 Cust. Approval WF Setup Wizard
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Einrichtung Debitorengenehmigungsworkflow;
               ENU=Customer Approval Workflow Setup];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table1804;
    PageType=NavigatePage;
    SourceTableTemporary=Yes;
    ShowFilter=No;
    OnInit=BEGIN
             IF NOT GET THEN BEGIN
               INIT;
               SetDefaultValues;
               INSERT;
             END;
             LoadTopBanners;
           END;

    OnOpenPage=BEGIN
                 ShowIntroStep;
               END;

    OnQueryClosePage=VAR
                       AssistedSetup@1001 : Record 1803;
                     BEGIN
                       IF CloseAction = ACTION::OK THEN
                         IF AssistedSetup.GetStatus(PAGE::"Cust. Approval WF Setup Wizard") = AssistedSetup.Status::"Not Completed" THEN
                           IF NOT CONFIRM(NAVNotSetUpQst,FALSE) THEN
                             EXIT(FALSE);
                     END;

    ActionList=ACTIONS
    {
      { 8       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 15      ;1   ;Action    ;
                      Name=PreviousPage;
                      CaptionML=[DEU=Zur�ck;
                                 ENU=Back];
                      ApplicationArea=#Suite;
                      Enabled=BackEnabled;
                      InFooterBar=Yes;
                      Image=PreviousRecord;
                      OnAction=BEGIN
                                 NextStep(TRUE);
                               END;
                                }
      { 14      ;1   ;Action    ;
                      Name=NextPage;
                      CaptionML=[DEU=Weiter;
                                 ENU=Next];
                      ApplicationArea=#Suite;
                      Enabled=NextEnabled;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 NextStep(FALSE);
                               END;
                                }
      { 9       ;1   ;Action    ;
                      Name=Finish;
                      CaptionML=[DEU=Fertig stellen;
                                 ENU=Finish];
                      ApplicationArea=#Suite;
                      Enabled=FinishEnabled;
                      InFooterBar=Yes;
                      Image=Approve;
                      OnAction=VAR
                                 AssistedSetup@1004 : Record 1803;
                                 ApprovalWorkflowSetupMgt@1001 : Codeunit 1804;
                               BEGIN
                                 ApprovalWorkflowSetupMgt.ApplyCustomerWizardUserInput(Rec);
                                 AssistedSetup.SetStatus(PAGE::"Cust. Approval WF Setup Wizard",AssistedSetup.Status::Completed);

                                 CurrPage.CLOSE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 96  ;1   ;Group     ;
                Visible=TopBannerVisible AND NOT DoneVisible;
                Editable=FALSE;
                GroupType=Group }

    { 97  ;2   ;Field     ;
                ApplicationArea=#Suite;
                SourceExpr=MediaRepositoryStandard.Image;
                Editable=FALSE;
                ShowCaption=No }

    { 98  ;1   ;Group     ;
                Visible=TopBannerVisible AND DoneVisible;
                Editable=FALSE;
                GroupType=Group }

    { 99  ;2   ;Field     ;
                ApplicationArea=#Suite;
                SourceExpr=MediaRepositoryDone.Image;
                Editable=FALSE;
                ShowCaption=No }

    { 11  ;1   ;Group     ;
                Name=Step1;
                CaptionML=[DEU="";
                           ENU=""];
                Visible=IntroVisible;
                GroupType=Group }

    { 18  ;2   ;Group     ;
                Name=Para1.1;
                CaptionML=[DEU=Willkommen bei der Einrichtung des Debitorengenehmigungsworkflows;
                           ENU=Welcome to Customer Approval Workflow Setup];
                GroupType=Group }

    { 19  ;3   ;Group     ;
                Name=Para1.1.1;
                CaptionML=[DEU="";
                           ENU=""];
                GroupType=Group;
                InstructionalTextML=[DEU=Sie k�nnen Genehmigungsworkflows erstellen, die einen Genehmiger automatisch benachrichtigen, wenn ein Benutzer versucht, eine Debitorenkarte zu erstellen oder zu �ndern.;
                                     ENU=You can create approval workflows that automatically notify an approver when a user tries to create or change a customer card.] }

    { 21  ;2   ;Group     ;
                Name=Para1.2;
                CaptionML=[DEU=Los geht's!;
                           ENU=Let's go!];
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie 'Weiter', um grundlegende Genehmigungsworkfloweinstellungen f�r das �ndern einer Debitorenkarte festzulegen.;
                                     ENU=Choose Next to specify basic approval workflow settings for changing a customer card.] }

    { 67  ;1   ;Group     ;
                Name=Step2;
                CaptionML=[DEU="";
                           ENU=""];
                Visible=CustomerApproverSetupVisible;
                GroupType=Group }

    { 5   ;2   ;Group     ;
                Name=Para2.1;
                CaptionML=[DEU="";
                           ENU=""];
                GroupType=Group }

    { 4   ;3   ;Group     ;
                Name=Para2.1.1;
                CaptionML=[DEU="";
                           ENU=""];
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie, wer f�r das Genehmigen oder Ablehnen neuer oder ge�nderter Debitorenkarten autorisiert ist.;
                                     ENU=Choose who is authorized to approve or reject new or changed customer cards.] }

    { 2   ;4   ;Field     ;
                CaptionML=[DEU=Genehmiger;
                           ENU=Approver];
                ApplicationArea=#Suite;
                SourceExpr="Approver ID";
                LookupPageID=Approval User Setup;
                OnValidate=BEGIN
                             CanEnableNext;
                           END;
                            }

    { 66  ;2   ;Group     ;
                Name=Para2.2;
                CaptionML=[DEU="";
                           ENU=""];
                GroupType=Group }

    { 3   ;3   ;Group     ;
                Name=Para2.2.1;
                CaptionML=[DEU=W�hlen Sie, ob der Genehmigungsprozess automatisch gestartet wird oder ob der Benutzer den Prozess starten muss.;
                           ENU=Choose if the approval process starts automatically or if the user must start the process.];
                GroupType=Group }

    { 65  ;4   ;Field     ;
                CaptionML=[DEU=Der Workflow startet, wenn;
                           ENU=The workflow starts when];
                ApplicationArea=#Suite;
                SourceExpr="App. Trigger";
                OnValidate=BEGIN
                             CanEnableNext;
                           END;
                            }

    { 12  ;1   ;Group     ;
                Name=Step3;
                CaptionML=[DEU="";
                           ENU=""];
                Visible=CustomerAutoAppDetailsVisible;
                GroupType=Group }

    { 62  ;2   ;Group     ;
                Name=Para3.1;
                CaptionML=[DEU="";
                           ENU=""];
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie die Kriterien, wann der Genehmigungsvorgang automatisch gestartet wird.;
                                     ENU=Choose criteria for when the approval process starts automatically.] }

    { 61  ;3   ;Group     ;
                Name=Para3.1.1;
                CaptionML=[DEU="";
                           ENU=""];
                GroupType=GridLayout;
                Layout=Rows }

    { 60  ;4   ;Group     ;
                Name=Para3.1.1.1;
                CaptionML=[DEU="";
                           ENU=""];
                GroupType=Group;
                InstructionalTextML=[DEU=Der Workflow startet, wenn:;
                                     ENU=The workflow starts when:] }

    { 59  ;5   ;Field     ;
                Name=CustomerFieldCap;
                CaptionML=[DEU=Feld;
                           ENU=Field];
                ApplicationArea=#Suite;
                SourceExpr=CustomerFieldCaption;
                OnValidate=VAR
                             FieldRec@1000 : Record 2000000041;
                           BEGIN
                             IF CustomerFieldCaption = '' THEN BEGIN
                               SetCustomerField(0);
                               EXIT;
                             END;

                             IF NOT FindAndFilterToField(FieldRec,CustomerFieldCaption) THEN
                               ERROR(FieldNotExistErr,CustomerFieldCaption);

                             IF FieldRec.COUNT = 1 THEN BEGIN
                               SetCustomerField(FieldRec."No.");
                               EXIT;
                             END;

                             IF PAGE.RUNMODAL(PAGE::"Field List",FieldRec) = ACTION::LookupOK THEN
                               SetCustomerField(FieldRec."No.")
                             ELSE
                               ERROR(FieldNotExistErr,CustomerFieldCaption);
                           END;

                OnLookup=VAR
                           FieldRec@1000 : Record 2000000041;
                         BEGIN
                           FindAndFilterToField(FieldRec,Text);
                           FieldRec.SETRANGE("Field Caption");
                           FieldRec.SETRANGE("No.");

                           IF PAGE.RUNMODAL(PAGE::"Field List",FieldRec) = ACTION::LookupOK THEN
                             SetCustomerField(FieldRec."No.");
                         END;

                ShowCaption=No }

    { 58  ;5   ;Field     ;
                CaptionML=[DEU=ist gleich;
                           ENU=is];
                ApplicationArea=#Suite;
                ShowCaption=No }

    { 57  ;5   ;Field     ;
                CaptionML=[DEU=Operator;
                           ENU=Operator];
                ApplicationArea=#Suite;
                SourceExpr="Field Operator";
                ShowCaption=No }

    { 56  ;3   ;Group     ;
                Name=Para3.1.2;
                CaptionML=[DEU=Legen Sie die Nachricht fest, die angezeigt wird, wenn der Workflow gestartet wird.;
                           ENU=Specify the message to display when the workflow starts.];
                GroupType=Group }

    { 54  ;4   ;Field     ;
                CaptionML=[DEU=Nachricht;
                           ENU=Message];
                ApplicationArea=#Suite;
                SourceExpr="Custom Message";
                ShowCaption=No }

    { 10  ;1   ;Group     ;
                Name=Step10;
                CaptionML=[DEU="";
                           ENU=""];
                Visible=DoneVisible;
                GroupType=Group }

    { 6   ;2   ;Group     ;
                Name=Para10.1;
                CaptionML=[DEU="";
                           ENU=""];
                GroupType=Group;
                InstructionalTextML=[DEU=�bersicht Debitorengenehmigungsworkflow;
                                     ENU=Customer Approval Workflow Overview] }

    { 7   ;3   ;Field     ;
                Name=Overview;
                ApplicationArea=#Suite;
                SourceExpr=SummaryText;
                Editable=FALSE;
                MultiLine=Yes;
                Style=StrongAccent;
                StyleExpr=TRUE;
                ShowCaption=No }

    { 25  ;2   ;Group     ;
                Name=Para10.2;
                CaptionML=[DEU=Das war's schon!;
                           ENU=That's it!];
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie 'Fertig stellen', um den Workflow mit den festgelegten Einstellungen zu aktivieren.;
                                     ENU=Choose Finish to enable the workflow with the specified settings.] }

  }
  CODE
  {
    VAR
      MediaRepositoryStandard@1040 : Record 9400;
      MediaRepositoryDone@1041 : Record 9400;
      Step@1015 : 'Intro,Customer Approver Setup,Automatic Approval Setup,Done';
      BackEnabled@1014 : Boolean;
      NextEnabled@1013 : Boolean;
      FinishEnabled@1010 : Boolean;
      TopBannerVisible@1042 : Boolean;
      IntroVisible@1001 : Boolean;
      CustomerApproverSetupVisible@1011 : Boolean;
      CustomerAutoAppDetailsVisible@1027 : Boolean;
      DoneVisible@1004 : Boolean;
      NAVNotSetUpQst@1000 : TextConst 'DEU=Die Debitorengenehmigung wurde noch nicht eingerichtet.\\M�chten Sie den Assistenten wirklich beenden?;ENU=Customer Approval has not been set up.\\Are you sure that you want to exit?';
      MandatoryApproverErr@1002 : TextConst '@@@="%1 = User Name";DEU=Sie m�ssen einen Genehmiger ausw�hlen, bevor Sie fortfahren.;ENU=You must select an approver before continuing.';
      CustomerFieldCaption@1021 : Text[250];
      FieldNotExistErr@1028 : TextConst '@@@="%1 = Field Caption";DEU=Feld %1 existiert nicht.;ENU=Field %1 does not exist.';
      ManualTriggerTxt@1006 : TextConst '@@@="%1 = User Name (eg. An approval request will be sent to the user Domain/Username when the user sends the request manually.)";DEU=Eine Genehmigungsanforderung wird an den Benutzer %1 gesendet, wenn der Benutzer die Anforderung manuell sendet.;ENU=An approval request will be sent to the user %1 when the user sends the request manually.';
      AutoTriggerTxt@1020 : TextConst '@@@="%1 = User Name, %2 = Field caption, %3 = Of of this 3 values: Increased, Decreased, Changed (eg. An approval request will be sent to the user Domain/Username when the value in the Credit Limit (LCY) field is Increased.)";DEU=Eine Genehmigungsanforderung wird an den Benutzer %1 gesendet, wenn der Wert im %2-Feld %3 lautet.;ENU=An approval request will be sent to the user %1 when the value in the %2 field is %3.';
      SummaryText@1007 : Text;

    LOCAL PROCEDURE NextStep@3(Backwards@1000 : Boolean);
    BEGIN
      IF Backwards THEN
        Step := Step - 1
      ELSE BEGIN
        IF CustomerApproverSetupVisible THEN
          ValidateApprover;
        IF CustomerAutoAppDetailsVisible THEN
          ValidateFieldSelection;
        Step := Step + 1;
      END;

      CASE Step OF
        Step::Intro:
          ShowIntroStep;
        Step::"Customer Approver Setup":
          ShowApprovalUserSetupDetailsStep;
        Step::"Automatic Approval Setup":
          IF "App. Trigger" = "App. Trigger"::"The user changes a specific field"
          THEN
            ShowCustomerApprovalDetailsStep
          ELSE
            NextStep(Backwards);
        Step::Done:
          ShowDoneStep;
      END;
      CurrPage.UPDATE(TRUE);
    END;

    LOCAL PROCEDURE ShowIntroStep@1();
    BEGIN
      ResetWizardControls;
      IntroVisible := TRUE;
      BackEnabled := FALSE;
    END;

    LOCAL PROCEDURE ShowApprovalUserSetupDetailsStep@9();
    BEGIN
      ResetWizardControls;
      CustomerApproverSetupVisible := TRUE;
    END;

    LOCAL PROCEDURE ShowCustomerApprovalDetailsStep@25();
    BEGIN
      ResetWizardControls;
      CustomerAutoAppDetailsVisible := TRUE;
      SetCustomerField(Field);
    END;

    LOCAL PROCEDURE ShowDoneStep@6();
    BEGIN
      ResetWizardControls;
      DoneVisible := TRUE;
      NextEnabled := FALSE;
      FinishEnabled := TRUE;

      IF "App. Trigger" = "App. Trigger"::"The user sends an approval requests manually" THEN
        SummaryText := STRSUBSTNO(ManualTriggerTxt,"Approver ID");
      IF "App. Trigger" = "App. Trigger"::"The user changes a specific field"
      THEN BEGIN
        CALCFIELDS("Field Caption");
        SummaryText := STRSUBSTNO(AutoTriggerTxt,"Approver ID","Field Caption","Field Operator");
      END;

      SummaryText := CONVERTSTR(SummaryText,'\','/');
    END;

    LOCAL PROCEDURE ResetWizardControls@10();
    BEGIN
      // Buttons
      BackEnabled := TRUE;
      NextEnabled := TRUE;
      FinishEnabled := FALSE;

      // Tabs
      IntroVisible := FALSE;
      CustomerApproverSetupVisible := FALSE;
      CustomerAutoAppDetailsVisible := FALSE;
      DoneVisible := FALSE;
    END;

    LOCAL PROCEDURE SetDefaultValues@8();
    VAR
      Workflow@1000 : Record 1501;
      WorkflowRule@1007 : Record 1524;
      WorkflowStep@1003 : Record 1502;
      WorkflowStepArgument@1006 : Record 1523;
      WorkflowSetup@1002 : Codeunit 1502;
      WorkflowResponseHandling@1005 : Codeunit 1521;
      WorkflowCode@1001 : Code[20];
    BEGIN
      TableNo := DATABASE::Customer;
      WorkflowCode := WorkflowSetup.GetWorkflowTemplateCode(WorkflowSetup.CustomerCreditLimitChangeApprovalWorkflowCode);
      IF Workflow.GET(WorkflowCode) THEN BEGIN
        WorkflowStep.SETRANGE("Workflow Code",WorkflowCode);
        WorkflowStep.SETRANGE("Function Name",WorkflowResponseHandling.ShowMessageCode);
        IF WorkflowStep.FINDFIRST THEN BEGIN
          WorkflowStepArgument.GET(WorkflowStep.Argument);
          "Custom Message" := WorkflowStepArgument.Message;
        END;
        WorkflowRule.SETRANGE("Workflow Code",WorkflowCode);
        IF WorkflowRule.FINDFIRST THEN BEGIN
          Field := WorkflowRule."Field No.";
          "Field Operator" := WorkflowRule.Operator;
        END;
      END;
    END;

    LOCAL PROCEDURE ValidateApprover@11();
    BEGIN
      IF "Approver ID" = '' THEN
        ERROR(MandatoryApproverErr);
    END;

    LOCAL PROCEDURE ValidateFieldSelection@7();
    BEGIN
    END;

    LOCAL PROCEDURE CanEnableNext@32();
    BEGIN
      NextEnabled := TRUE;
    END;

    LOCAL PROCEDURE SetCustomerField@54(FieldNo@1000 : Integer);
    BEGIN
      Field := FieldNo;
      CALCFIELDS("Field Caption");
      CustomerFieldCaption := "Field Caption";
    END;

    LOCAL PROCEDURE FindAndFilterToField@57(VAR FieldRec@1000 : Record 2000000041;CaptionToFind@1001 : Text) : Boolean;
    BEGIN
      FieldRec.FILTERGROUP(2);
      FieldRec.SETRANGE(TableNo,DATABASE::Customer);
      FieldRec.SETFILTER(Type,STRSUBSTNO('%1|%2|%3|%4|%5|%6|%7|%8|%9|%10|%11|%12|%13',
          FieldRec.Type::Boolean,
          FieldRec.Type::Text,
          FieldRec.Type::Code,
          FieldRec.Type::Decimal,
          FieldRec.Type::Integer,
          FieldRec.Type::BigInteger,
          FieldRec.Type::Date,
          FieldRec.Type::Time,
          FieldRec.Type::DateTime,
          FieldRec.Type::DateFormula,
          FieldRec.Type::Option,
          FieldRec.Type::Duration,
          FieldRec.Type::RecordID));
      FieldRec.SETRANGE(Class,FieldRec.Class::Normal);

      IF CaptionToFind = "Field Caption" THEN
        FieldRec.SETRANGE("No.",Field)
      ELSE
        FieldRec.SETFILTER("Field Caption",'%1','@' + CaptionToFind + '*');

      EXIT(FieldRec.FINDFIRST);
    END;

    LOCAL PROCEDURE LoadTopBanners@40();
    BEGIN
      IF MediaRepositoryStandard.GET('AssistedSetup-NoText-400px.png',FORMAT(CURRENTCLIENTTYPE)) AND
         MediaRepositoryDone.GET('AssistedSetupDone-NoText-400px.png',FORMAT(CURRENTCLIENTTYPE))
      THEN
        TopBannerVisible := MediaRepositoryDone.Image.HASVALUE;
    END;

    BEGIN
    END.
  }
}

