OBJECT Page 5530 Item Availability by Event
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Artikelverf�gbarkeit nach Ereignis;
               ENU=Item Availability by Event];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    LinksAllowed=No;
    SourceTable=Table5531;
    DataCaptionExpr=PageCaption;
    SourceTableView=SORTING(Period Start,Line No.)
                    ORDER(Ascending);
    PageType=Worksheet;
    SourceTableTemporary=Yes;
    OnOpenPage=BEGIN
                 IF ItemIsSet THEN
                   InitAndCalculatePeriodEntries
                 ELSE
                   InitItemRequestFields;
               END;

    OnAfterGetRecord=BEGIN
                       Emphasize := EmphasizeLine;
                       EnableShowDocumentAction := HasSourceDocument;
                     END;

    OnQueryClosePage=BEGIN
                       IF CloseAction = ACTION::LookupOK THEN
                         SelectedDate := "Period Start";
                     END;

    ActionList=ACTIONS
    {
      { 3       ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 6       ;1   ;Action    ;
                      CaptionML=[DEU=Neu berechnen;
                                 ENU=Recalculate];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Refresh;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 InitAndCalculatePeriodEntries;
                               END;
                                }
      { 17      ;1   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Beleg anzeigen;
                                 ENU=Show Document];
                      ToolTipML=[DEU=�ffnet den Beleg, in dem die ausgew�hlte Zeile vorhanden ist.;
                                 ENU=Open the document that the selected line exists on.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Enabled=EnableShowDocumentAction;
                      PromotedIsBig=Yes;
                      Image=View;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CalcInventoryPageData.ShowDocument("Source Document ID");
                               END;
                                }
      { 24      ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 23      ;1   ;ActionGroup;
                      CaptionML=[DEU=Art&ikel;
                                 ENU=&Item];
                      Image=Item }
      { 22      ;2   ;ActionGroup;
                      CaptionML=[DEU=Art&ikelverf�gbarkeit nach;
                                 ENU=&Item Availability by];
                      Image=ItemAvailability }
      { 20      ;3   ;Action    ;
                      CaptionML=[DEU=Ereignis;
                                 ENU=Event];
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromItem(Item,ItemAvailFormsMgt.ByEvent);
                               END;
                                }
      { 18      ;3   ;Action    ;
                      CaptionML=[DEU=Periode;
                                 ENU=Period];
                      ToolTipML=[DEU=Zeigt die tats�chliche und die voraussichtliche Menge eines Artikels im Lauf der Zeit gem�� einem angegebenen Zeitintervall an, wie etwa nach Tag, Woche oder Monat.;
                                 ENU=Show the actual and projected quantity of an item over time according to a specified time interval, such as by day, week or month.];
                      ApplicationArea=#Basic,#Suite;
                      Image=Period;
                      OnAction=BEGIN
                                 PAGE.RUN(PAGE::"Item Availability by Periods",Item,Item."No.");
                               END;
                                }
      { 11      ;3   ;Action    ;
                      CaptionML=[DEU=Variante;
                                 ENU=Variant];
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 PAGE.RUN(PAGE::"Item Availability by Variant",Item,Item."No.");
                               END;
                                }
      { 7       ;3   ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[DEU=Lagerort;
                                 ENU=Location];
                      Image=Warehouse;
                      OnAction=BEGIN
                                 PAGE.RUN(PAGE::"Item Availability by Location",Item,Item."No.");
                               END;
                                }
      { 25      ;3   ;Action    ;
                      CaptionML=[DEU=St�cklistenebene;
                                 ENU=BOM Level];
                      Image=BOMLevel;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromItem(Item,ItemAvailFormsMgt.ByBOM);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[DEU=Optionen;
                           ENU=Options];
                GroupType=Group }

    { 30  ;2   ;Field     ;
                CaptionML=[DEU=Artikelnr.;
                           ENU=Item No.];
                SourceExpr=ItemNo;
                TableRelation=Item;
                OnValidate=BEGIN
                             IF ItemNo <> Item."No." THEN BEGIN
                               Item.GET(ItemNo);
                               IF LocationFilter <> '' THEN
                                 Item.SETFILTER("Location Filter",LocationFilter);
                               IF VariantFilter <> '' THEN
                                 Item.SETFILTER("Variant Filter",VariantFilter);
                               InitAndCalculatePeriodEntries;
                             END;
                           END;
                            }

    { 35  ;2   ;Field     ;
                CaptionML=[DEU=Variantenfilter;
                           ENU=Variant Filter];
                SourceExpr=VariantFilter;
                OnValidate=BEGIN
                             IF VariantFilter <> Item.GETFILTER("Variant Filter") THEN BEGIN
                               Item.SETRANGE("Variant Filter");
                               IF VariantFilter <> '' THEN
                                 Item.SETFILTER("Variant Filter",VariantFilter);
                               InitAndCalculatePeriodEntries;
                             END;
                           END;

                OnLookup=VAR
                           ItemVariant@1001 : Record 5401;
                           ItemVariants@1000 : Page 5401;
                         BEGIN
                           ItemVariant.SETFILTER("Item No.",ItemNo);
                           ItemVariants.SETTABLEVIEW(ItemVariant);
                           ItemVariants.LOOKUPMODE := TRUE;
                           IF ItemVariants.RUNMODAL = ACTION::LookupOK THEN BEGIN
                             ItemVariants.GETRECORD(ItemVariant);
                             Text := ItemVariant.Code;
                             EXIT(TRUE);
                           END;
                           EXIT(FALSE);
                         END;
                          }

    { 4   ;2   ;Field     ;
                CaptionML=[DEU=Lagerortfilter;
                           ENU=Location Filter];
                SourceExpr=LocationFilter;
                OnValidate=BEGIN
                             IF LocationFilter <> Item.GETFILTER("Location Filter") THEN BEGIN
                               Item.SETRANGE("Location Filter");
                               IF LocationFilter <> '' THEN
                                 Item.SETFILTER("Location Filter",LocationFilter);
                               InitAndCalculatePeriodEntries;
                             END;
                           END;

                OnLookup=VAR
                           Location@1001 : Record 14;
                           LocationList@1000 : Page 15;
                         BEGIN
                           LocationList.SETTABLEVIEW(Location);
                           LocationList.LOOKUPMODE := TRUE;
                           IF LocationList.RUNMODAL = ACTION::LookupOK THEN BEGIN
                             LocationList.GETRECORD(Location);
                             Text := Location.Code;
                             EXIT(TRUE);
                           END;
                           EXIT(FALSE);
                         END;
                          }

    { 15  ;2   ;Field     ;
                CaptionML=[DEU=Anzeigen nach;
                           ENU=View by];
                ToolTipML=[DEU=Gibt an, in welchen Zeitintervallen die Verf�gbarkeitszahlen gruppiert und angezeigt werden sollen.;
                           ENU=Specifies which time intervals to group and view the availability figures.];
                OptionCaptionML=[DEU=Tag,Woche,Monat,Quartal,Jahr;
                                 ENU=Day,Week,Month,Quarter,Year];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PeriodType;
                Importance=Promoted;
                OnValidate=BEGIN
                             CalculatePeriodEntries;
                           END;
                            }

    { 13  ;2   ;Field     ;
                CaptionML=[DEU=Letzte Aktualisierung;
                           ENU=Last Updated];
                ToolTipML=[DEU=Gibt an, wann die Verf�gbarkeitszahlen im Fenster "Artikelverf�gbarkeit nach Ereignis" zuletzt aktualisiert wurden.;
                           ENU=Specifies when the availability figures in the Item Availability by Event window were last updated.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=LastUpdateTime;
                Importance=Additional;
                Editable=FALSE }

    { 9   ;2   ;Field     ;
                CaptionML=[DEU=Planungsname;
                           ENU=Forecast Name];
                ToolTipML=[DEU=Gibt eine Absatzplanung an, die Sie als Bedarf ber�cksichtigen sollten, wenn Sie die Verf�gbarkeitszahlen des Artikels anzeigen.;
                           ENU=Specifies a production forecast you want to include as demand, when showing the item's availability figures.];
                SourceExpr=ForecastName;
                TableRelation="Production Forecast Name";
                Importance=Promoted;
                OnValidate=BEGIN
                             InitAndCalculatePeriodEntries;
                           END;
                            }

    { 32  ;2   ;Field     ;
                CaptionML=[DEU=Planungsvorschl�ge einschlie�en;
                           ENU=Include Planning Suggestions];
                ToolTipML=[DEU=Gibt an, ob Vorschl�ge in Planungs- oder Bestellvorschl�gen in den Verf�gbarkeitszahlen enthalten sein sollen.;
                           ENU=Specifies whether to include suggestions in planning or requisition worksheets, in the availability figures.];
                SourceExpr=IncludePlanningSuggestions;
                OnValidate=BEGIN
                             IF IncludePlanningSuggestions THEN
                               IncludeBlanketOrders := TRUE;

                             InitAndCalculatePeriodEntries;
                           END;
                            }

    { 16  ;2   ;Field     ;
                CaptionML=[DEU=Rahmenauftr�ge einschlie�en;
                           ENU=Include Blanket Sales Orders];
                ToolTipML=[DEU=Umfasst voraussichtlichen Bedarf aus Rahmenauftr�gen in Verf�gbarkeitszahlen.;
                           ENU=Includes anticipated demand from blanket sales orders in availability figures.];
                SourceExpr=IncludeBlanketOrders;
                Editable=NOT IncludePlanningSuggestions;
                OnValidate=BEGIN
                             InitAndCalculatePeriodEntries;
                           END;
                            }

    { 5   ;1   ;Group     ;
                Editable=FALSE;
                IndentationColumnName=Level;
                IndentationControls=Description;
                ShowAsTree=Yes;
                GroupType=Repeater }

    { 39  ;2   ;Field     ;
                CaptionML=[DEU=Periode;
                           ENU=Period];
                ToolTipML=[DEU=Gibt das erste Datum in der ausgew�hlten Periode an, an dem ein Beschaffungs- oder Bedarfsereignis auftritt, das die Verf�gbarkeitszahlen des Artikels �ndert.;
                           ENU=Specifies the first date in the selected period where a supply or demand event occurs that changes the item's availability figures.];
                SourceExpr=Code;
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, an welchem Datum die Periode beginnt, z. B. am ersten Tag im M�rz, wenn die Periode "Monat" ist.;
                           ENU=Specifies on which date the period starts, such as the first day of March, if the period is Month.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Period Start";
                Visible=TRUE;
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung der Verf�gbarkeitszeile an.;
                           ENU=Specifies the description of the availability line.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description;
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 19  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art des Herkunftsbelegs oder der Herkunftszeile an.;
                           ENU=Specifies the type of the source document or source line.];
                SourceExpr=Type;
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 28  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, auf welcher Art von Beleg oder Zeile die Verf�gbarkeitszahl basiert.;
                           ENU=Specifies which type of document or line the availability figure is based on.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Source;
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 21  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Belegs an, auf dem die Verf�gbarkeitszahl basiert.;
                           ENU=Specifies the number of the document that the availability figure is based on.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No.";
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Variantencode an, wenn nur Verf�gbarkeitszahlen f�r diese Variante des Artikels angezeigt werden sollen.;
                           ENU=Specifies a variant code if you only want to view availability figures for that variant of the item.];
                SourceExpr="Variant Code";
                Visible=FALSE;
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Speicherort des Bedarfsbelegs an, aus dem das Fenster "Artikelverf�gbarkeit nach Ereignis" ge�ffnet wurde.;
                           ENU=Specifies the location of the demand document, from which the Item Availability by Event window was opened.];
                SourceExpr="Location Code";
                Visible=FALSE;
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 41  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den gesamten Bedarf des Artikels an.;
                           ENU=Specifies the item's total demand.];
                SourceExpr="Gross Requirement";
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 26  ;2   ;Field     ;
                SourceExpr="Reserved Requirement";
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 43  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Summe der Artikel in bestehenden Beschaffungsauftr�gen an.;
                           ENU=Specifies the sum of items on existing supply orders.];
                SourceExpr="Scheduled Receipt";
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 27  ;2   ;Field     ;
                SourceExpr="Reserved Receipt";
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Differenz zwischen fertig gestellter Menge und geplanter Menge im Fertigungsauftrag an.;
                           ENU=Specifies the difference between the finished quantity and the planned quantity on the production order.];
                SourceExpr="Remaining Quantity (Base)";
                Visible=FALSE;
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 33  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Verf�gbarkeit des Artikels an.;
                           ENU=Specifies the item's availability.];
                ApplicationArea=#Basic,#Suite;
                DecimalPlaces=0:5;
                SourceExpr="Projected Inventory";
                Enabled=FALSE;
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 45  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge an, die bei der Absatzplanung angefordert wird und auf der die Verf�gbarkeitszahl basiert.;
                           ENU=Specifies the quantity that is demanded on the production forecast that the availability figure is based on.];
                SourceExpr=Forecast;
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 37  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerbestand des Artikels an, einschlie�lich des voraussichtlichen Bedarfs aus Absatzplanungen oder Rahmenauftr�gen.;
                           ENU=Specifies the item's inventory, including anticipated demand from production forecasts or blanket sales orders.];
                DecimalPlaces=0:5;
                SourceExpr="Forecasted Projected Inventory";
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 49  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge an, die f�r die Absatzplanung verbleibt, nachdem die Planungsmenge in der Verf�gbarkeitszeile verbraucht wurde.;
                           ENU=Specifies the quantity that remains on the production forecast, after the forecast quantity on the availability line has been consumed.];
                SourceExpr="Remaining Forecast";
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 53  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Ereignismeldung der Planungs- oder Bestellvorschlagszeile an, auf der diese Verf�gbarkeitszahl basiert.;
                           ENU=Specifies the action message of the planning or requisition line that this availability figure is based on.];
                SourceExpr="Action Message";
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 51  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Menge an, die in der Planungs- oder Bestellvorschlagszeile vorgeschlagen wird, auf der diese Verf�gbarkeitszahl basiert.;
                           ENU=Specifies the quantity that is suggested in the planning or requisition line that this availability figure is based on.];
                SourceExpr="Action Message Qty.";
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 47  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Lagerbestand des Artikels an, einschlie�lich der vorgeschlagenen Lieferungen in Planungs- oder Bestellvorschlagszeilen.;
                           ENU=Specifies the item's inventory, including the suggested supplies that occur in planning or requisition worksheet lines.];
                DecimalPlaces=0:5;
                SourceExpr="Suggested Projected Inventory";
                Editable=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

  }
  CODE
  {
    VAR
      Item@1004 : Record 27;
      TempInvtPageData@1015 : TEMPORARY Record 5531;
      CalcInventoryPageData@1010 : Codeunit 5531;
      ItemAvailFormsMgt@1021 : Codeunit 353;
      ItemNo@1003 : Code[20];
      LocationFilter@1009 : Text;
      VariantFilter@1008 : Text;
      ForecastName@1005 : Code[10];
      PeriodType@1000 : 'Day,Week,Month,Quarter,Year';
      LastUpdateTime@1016 : DateTime;
      SelectedDate@1001 : Date;
      IncludePlanningSuggestions@1011 : Boolean INDATASET;
      IncludeBlanketOrders@1014 : Boolean INDATASET;
      Emphasize@1020 : Boolean INDATASET;
      EnableShowDocumentAction@1007 : Boolean INDATASET;

    LOCAL PROCEDURE InitAndCalculatePeriodEntries@14();
    BEGIN
      Initialize;
      CalculatePeriodEntries;
    END;

    LOCAL PROCEDURE CalculatePeriodEntries@8();
    BEGIN
      TempInvtPageData.RESET;
      TempInvtPageData.DELETEALL;
      TempInvtPageData.SETCURRENTKEY("Period Start","Line No.");
      CalcInventoryPageData.CreatePeriodEntries(TempInvtPageData,PeriodType);

      RESET;
      DELETEALL;
      SETCURRENTKEY("Period Start","Line No.");

      TempInvtPageData.SETRANGE(Level,0);
      IF TempInvtPageData.FIND('-') THEN
        REPEAT
          CalcInventoryPageData.DetailsForPeriodEntry(TempInvtPageData,TRUE);
          CalcInventoryPageData.DetailsForPeriodEntry(TempInvtPageData,FALSE);
        UNTIL TempInvtPageData.NEXT = 0;
      TempInvtPageData.SETRANGE(Level);

      ExpandAll;
    END;

    LOCAL PROCEDURE Initialize@1();
    BEGIN
      Item.SETRANGE("Drop Shipment Filter",FALSE);
      CalcInventoryPageData.Initialize(Item,ForecastName,IncludeBlanketOrders,0D,IncludePlanningSuggestions);
      LastUpdateTime := CURRENTDATETIME;
    END;

    LOCAL PROCEDURE ExpandAll@9();
    VAR
      RunningInventory@1000 : Decimal;
      RunningInventoryForecast@1002 : Decimal;
      RunningInventoryPlan@1001 : Decimal;
    BEGIN
      RESET;
      DELETEALL;
      SETCURRENTKEY("Period Start","Line No.");

      IF TempInvtPageData.FIND('-') THEN
        REPEAT
          Rec := TempInvtPageData;
          UpdateInventorys(RunningInventory,RunningInventoryForecast,RunningInventoryPlan);
          INSERT;
        UNTIL TempInvtPageData.NEXT = 0;

      IF FIND('-') THEN;
    END;

    LOCAL PROCEDURE EmphasizeLine@4() : Boolean;
    BEGIN
      EXIT(Level = 0);
    END;

    LOCAL PROCEDURE HasSourceDocument@7() : Boolean;
    BEGIN
      EXIT((Level > 0) AND (FORMAT("Source Document ID") <> ''));
    END;

    LOCAL PROCEDURE InitItemRequestFields@10();
    BEGIN
      CLEAR(Item);
      CLEAR(ItemNo);
      CLEAR(LocationFilter);
      CLEAR(VariantFilter);
      CLEAR(LastUpdateTime);
    END;

    LOCAL PROCEDURE UpdateItemRequestFields@11(VAR Item@1000 : Record 27);
    BEGIN
      ItemNo := Item."No.";
      LocationFilter := '';
      IF Item.GETFILTER("Location Filter") <> '' THEN
        LocationFilter := Item.GETFILTER("Location Filter");
      VariantFilter := '';
      IF Item.GETFILTER("Variant Filter") <> '' THEN
        VariantFilter := Item.GETFILTER("Variant Filter");
    END;

    LOCAL PROCEDURE ItemIsSet@16() : Boolean;
    BEGIN
      EXIT(Item."No." <> '');
    END;

    LOCAL PROCEDURE PageCaption@5() : Text[250];
    BEGIN
      EXIT(STRSUBSTNO('%1 %2',Item."No.",Item.Description));
    END;

    PROCEDURE SetItem@3(VAR NewItem@1000 : Record 27);
    BEGIN
      Item.COPY(NewItem);
      UpdateItemRequestFields(Item);
    END;

    PROCEDURE SetForecastName@2(NewForcastName@1000 : Code[10]);
    BEGIN
      ForecastName := NewForcastName;
    END;

    PROCEDURE SetIncludePlan@6(NewIncludePlanningSuggestions@1000 : Boolean);
    BEGIN
      IncludePlanningSuggestions := NewIncludePlanningSuggestions;
    END;

    PROCEDURE GetSelectedDate@13() : Date;
    BEGIN
      EXIT(SelectedDate);
    END;

    BEGIN
    END.
  }
}

