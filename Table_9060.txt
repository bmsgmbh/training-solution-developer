OBJECT Table 9060 SB Owner Cue
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=SB-Besitzerstapel;
               ENU=SB Owner Cue];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[DEU=Prim�rschl�ssel;
                                                              ENU=Primary Key] }
    { 2   ;   ;Released Sales Quotes;Integer      ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=CONST(Quote),
                                                                                           Status=FILTER(Released)));
                                                   CaptionML=[DEU=Freigegebene Verkaufsangebote;
                                                              ENU=Released Sales Quotes] }
    { 3   ;   ;Open Sales Orders   ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=CONST(Order),
                                                                                           Status=FILTER(Open)));
                                                   AccessByPermission=TableData 6660=R;
                                                   CaptionML=[DEU=Offene Verkaufsauftr�ge;
                                                              ENU=Open Sales Orders] }
    { 4   ;   ;Released Sales Orders;Integer      ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=CONST(Order),
                                                                                           Status=FILTER(Released)));
                                                   AccessByPermission=TableData 6660=R;
                                                   CaptionML=[DEU=Freigegebene Verkaufsauftr�ge;
                                                              ENU=Released Sales Orders] }
    { 5   ;   ;Released Purchase Orders;Integer   ;FieldClass=FlowField;
                                                   CalcFormula=Count("Purchase Header" WHERE (Document Type=CONST(Order),
                                                                                              Status=FILTER(Released)));
                                                   AccessByPermission=TableData 120=R;
                                                   CaptionML=[DEU=Freigegebene Bestellungen;
                                                              ENU=Released Purchase Orders] }
    { 6   ;   ;Overdue Sales Documents;Integer    ;FieldClass=FlowField;
                                                   CalcFormula=Count("Cust. Ledger Entry" WHERE (Document Type=FILTER(Invoice|Credit Memo),
                                                                                                 Due Date=FIELD(Overdue Date Filter),
                                                                                                 Open=CONST(Yes)));
                                                   CaptionML=[DEU=�berf�llige Verkaufsbelege;
                                                              ENU=Overdue Sales Documents] }
    { 7   ;   ;SOs Shipped Not Invoiced;Integer   ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=CONST(Order),
                                                                                           Completely Shipped=CONST(Yes),
                                                                                           Shipped Not Invoiced=CONST(Yes)));
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[DEU=Verkaufsauftr�ge - Nicht fakt. Lieferungen;
                                                              ENU=SOs Shipped Not Invoiced] }
    { 8   ;   ;Customers - Blocked ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count(Customer WHERE (Blocked=FILTER(<>' ')));
                                                   CaptionML=[DEU=Debitoren - Gesperrt;
                                                              ENU=Customers - Blocked] }
    { 9   ;   ;Purchase Documents Due Today;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Vendor Ledger Entry" WHERE (Document Type=FILTER(Invoice|Credit Memo),
                                                                                                  Due Date=FIELD(Due Date Filter),
                                                                                                  Open=CONST(Yes)));
                                                   CaptionML=[DEU=Heute f�llige Einkaufsbelege;
                                                              ENU=Purchase Documents Due Today] }
    { 10  ;   ;Vendors - Payment on Hold;Integer  ;FieldClass=FlowField;
                                                   CalcFormula=Count(Vendor WHERE (Blocked=FILTER(Payment)));
                                                   CaptionML=[DEU=Kreditoren - Zahlung abwarten;
                                                              ENU=Vendors - Payment on Hold] }
    { 11  ;   ;Sales Invoices      ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=FILTER(Invoice)));
                                                   CaptionML=[DEU=Verkaufsrechnungen;
                                                              ENU=Sales Invoices] }
    { 12  ;   ;Unpaid Sales Invoices;Integer      ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Invoice Header" WHERE (Closed=FILTER(No)));
                                                   CaptionML=[DEU=Nicht bezahlte Verkaufsrechnungen;
                                                              ENU=Unpaid Sales Invoices] }
    { 13  ;   ;Overdue Sales Invoices;Integer     ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Invoice Header" WHERE (Due Date=FIELD(Overdue Date Filter),
                                                                                                   Closed=FILTER(No)));
                                                   CaptionML=[DEU=�berf�llige Verkaufsrechnungen;
                                                              ENU=Overdue Sales Invoices] }
    { 14  ;   ;Sales Quotes        ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=FILTER(Quote)));
                                                   CaptionML=[DEU=Verkaufsangebote;
                                                              ENU=Sales Quotes] }
    { 20  ;   ;Due Date Filter     ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[DEU=F�lligkeitsdatumsfilter;
                                                              ENU=Due Date Filter];
                                                   Editable=No }
    { 21  ;   ;Overdue Date Filter ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[DEU=�berf�lligkeits-Datumsfilter;
                                                              ENU=Overdue Date Filter] }
    { 30  ;   ;Purchase Invoices   ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Purchase Header" WHERE (Document Type=FILTER(Invoice)));
                                                   CaptionML=[DEU=Einkaufsrechnungen;
                                                              ENU=Purchase Invoices] }
    { 31  ;   ;Unpaid Purchase Invoices;Integer   ;FieldClass=FlowField;
                                                   CalcFormula=Count("Purch. Inv. Header" WHERE (Closed=FILTER(No)));
                                                   CaptionML=[DEU=Nicht bezahlte Einkaufsrechnungen;
                                                              ENU=Unpaid Purchase Invoices] }
    { 32  ;   ;Overdue Purchase Invoices;Integer  ;FieldClass=FlowField;
                                                   CalcFormula=Count("Purch. Inv. Header" WHERE (Due Date=FIELD(Overdue Date Filter),
                                                                                                 Closed=FILTER(No)));
                                                   CaptionML=[DEU=�berf�llige Einkaufsrechnungen;
                                                              ENU=Overdue Purchase Invoices] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE CountSalesOrdersShippedNotInvoiced@1() : Integer;
    VAR
      CountSalesOrders@1000 : Query 9060;
    BEGIN
      CountSalesOrders.SETRANGE(Completely_Shipped,TRUE);
      CountSalesOrders.SETRANGE(Invoice,FALSE);
      CountSalesOrders.OPEN;
      CountSalesOrders.READ;
      EXIT(CountSalesOrders.Count_Orders);
    END;

    PROCEDURE ShowSalesOrdersShippedNotInvoiced@2();
    VAR
      SalesHeader@1000 : Record 36;
    BEGIN
      SalesHeader.SETRANGE("Document Type",SalesHeader."Document Type"::Order);
      SalesHeader.SETRANGE("Completely Shipped",TRUE);
      SalesHeader.SETRANGE(Invoice,FALSE);
      PAGE.RUN(PAGE::"Sales Order List",SalesHeader);
    END;

    BEGIN
    END.
  }
}

