OBJECT Page 9991 Code Coverage Setup
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Code Coverage-Einrichtung;
               ENU=Code Coverage Setup];
    SaveValues=Yes;
    OnOpenPage=BEGIN
                 SetDefaultValues;
                 CodeCoverageMgt.StartAutomaticBackup(TimeInterval,BackupPath,SummaryPath);
               END;

  }
  CONTROLS
  {
    { 1000;    ;Container ;
                Name=Container;
                ContainerType=ContentArea }

    { 1001;1   ;Field     ;
                Name=<TimeInterval>;
                CaptionML=[DEU=Zeitintervall (Minuten);
                           ENU=Time Interval (minutes)];
                SourceExpr=TimeInterval;
                OnValidate=VAR
                             DefaultTimeIntervalInt@1001 : Integer;
                           BEGIN
                             EVALUATE(DefaultTimeIntervalInt,DefaultTimeIntervalInMinutesTxt);
                             IF TimeInterval < DefaultTimeIntervalInt THEN
                               ERROR(TimeIntervalErr);

                             CodeCoverageMgt.UpdateAutomaticBackupSettings(TimeInterval,BackupPath,SummaryPath);
                             MESSAGE(AppliedSettingsSuccesfullyMsg);
                           END;
                            }

    { 1002;1   ;Field     ;
                Name=<BackupPath>;
                CaptionML=[DEU=Sicherungspfad;
                           ENU=Backup Path];
                SourceExpr=BackupPath;
                OnValidate=BEGIN
                             IF BackupPath = '' THEN
                               ERROR(BackupPathErr);

                             CodeCoverageMgt.UpdateAutomaticBackupSettings(TimeInterval,BackupPath,SummaryPath);
                             MESSAGE(AppliedSettingsSuccesfullyMsg);
                           END;

                OnAssistEdit=BEGIN
                               BackupPath := GetFolder;
                               IF BackupPath = '' THEN
                                 ERROR(BackupPathErr);

                               CodeCoverageMgt.UpdateAutomaticBackupSettings(TimeInterval,BackupPath,SummaryPath);
                               MESSAGE(AppliedSettingsSuccesfullyMsg);
                             END;
                              }

    { 1   ;1   ;Field     ;
                Name=<SummaryPath>;
                CaptionML=[DEU=Pfad Zusammenfassung;
                           ENU=Summary Path];
                SourceExpr=SummaryPath;
                OnValidate=BEGIN
                             IF SummaryPath = '' THEN
                               ERROR(SummaryPathErr);

                             CodeCoverageMgt.UpdateAutomaticBackupSettings(TimeInterval,BackupPath,SummaryPath);
                             MESSAGE(AppliedSettingsSuccesfullyMsg);
                           END;

                OnAssistEdit=BEGIN
                               SummaryPath := GetFolder;
                               IF SummaryPath = '' THEN
                                 ERROR(SummaryPathErr);

                               CodeCoverageMgt.UpdateAutomaticBackupSettings(TimeInterval,BackupPath,SummaryPath);
                               MESSAGE(AppliedSettingsSuccesfullyMsg);
                             END;
                              }

  }
  CODE
  {
    VAR
      CodeCoverageMgt@1003 : Codeunit 9990;
      TimeInterval@1000 : Integer;
      BackupPath@1001 : Text[1024];
      SummaryPath@1002 : Text[1024];
      AppliedSettingsSuccesfullyMsg@1007 : TextConst 'DEU=Automatische Sicherungseinstellungen erfolgreich �bernommen.;ENU=Automatic Backup settings applied successfully.';
      BackupPathErr@1006 : TextConst 'DEU=Der Sicherungspfad muss einen Wert aufweisen.;ENU=Backup Path must have a value.';
      DefaultTimeIntervalInMinutesTxt@1008 : TextConst 'DEU=10;ENU=10';
      SummaryPathErr@1005 : TextConst 'DEU=Der Zusammenfassungspfad muss einen Wert aufweisen.;ENU=Summary Path must have a value.';
      TimeIntervalErr@1004 : TextConst 'DEU=Das Zeitintervall muss gr��er als oder gleich 10 sein.;ENU=The time interval must be greater than or equal to 10.';

    PROCEDURE GetFolder@1000() : Text[1024];
    VAR
      FolderBrowserDialog@1000 : DotNet "'System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Windows.Forms.FolderBrowserDialog" RUNONCLIENT;
    BEGIN
      FolderBrowserDialog := FolderBrowserDialog.FolderBrowserDialog;
      FolderBrowserDialog.ShowNewFolderButton(TRUE);
      FolderBrowserDialog.Description('Select Folder');
      FolderBrowserDialog.ShowDialog;
      IF FolderBrowserDialog.SelectedPath <> '' THEN
        EXIT(FolderBrowserDialog.SelectedPath + '\')
    END;

    PROCEDURE SetDefaultValues@2();
    BEGIN
      // Set default values for automatic backups settings, in case they don't exist
      IF TimeInterval < 10 THEN
        EVALUATE(TimeInterval,DefaultTimeIntervalInMinutesTxt);
      IF BackupPath = '' THEN
        BackupPath := APPLICATIONPATH;
      IF SummaryPath = '' THEN
        SummaryPath := APPLICATIONPATH;
    END;

    BEGIN
    END.
  }
}

