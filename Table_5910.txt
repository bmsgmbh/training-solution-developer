OBJECT Table 5910 Service Hour
{
  OBJECT-PROPERTIES
  {
    Date=15.09.15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               CheckTime;
             END;

    OnModify=BEGIN
               CheckTime;
             END;

    CaptionML=[DEU=Servicezeit;
               ENU=Service Hour];
    LookupPageID=Page5916;
  }
  FIELDS
  {
    { 1   ;   ;Service Contract No.;Code20        ;TableRelation=IF (Service Contract Type=CONST(Contract)) "Service Contract Header"."Contract No." WHERE (Contract Type=CONST(Contract))
                                                                 ELSE IF (Service Contract Type=CONST(Quote)) "Service Contract Header"."Contract No." WHERE (Contract Type=CONST(Quote));
                                                   CaptionML=[DEU=Servicevertragsnr.;
                                                              ENU=Service Contract No.] }
    { 2   ;   ;Starting Date       ;Date          ;CaptionML=[DEU=Startdatum;
                                                              ENU=Starting Date] }
    { 3   ;   ;Day                 ;Option        ;CaptionML=[DEU=Tag;
                                                              ENU=Day];
                                                   OptionCaptionML=[DEU=Montag,Dienstag,Mittwoch,Donnerstag,Freitag,Samstag,Sonntag;
                                                                    ENU=Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday];
                                                   OptionString=Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday }
    { 4   ;   ;Starting Time       ;Time          ;OnValidate=BEGIN
                                                                IF "Ending Time" <> 0T THEN
                                                                  IF "Starting Time" >= "Ending Time" THEN
                                                                    ERROR(Text001,FIELDCAPTION("Starting Time"),FIELDCAPTION("Ending Time"));
                                                              END;

                                                   CaptionML=[DEU=Startzeit;
                                                              ENU=Starting Time] }
    { 5   ;   ;Ending Time         ;Time          ;OnValidate=BEGIN
                                                                IF "Ending Time" <> 0T THEN
                                                                  IF "Ending Time" <= "Starting Time" THEN
                                                                    ERROR(Text000,FIELDCAPTION("Ending Time"),FIELDCAPTION("Starting Time"));
                                                              END;

                                                   CaptionML=[DEU=Endzeit;
                                                              ENU=Ending Time] }
    { 6   ;   ;Valid on Holidays   ;Boolean       ;CaptionML=[DEU=An Feiertagen g�ltig;
                                                              ENU=Valid on Holidays] }
    { 7   ;   ;Service Contract Type;Option       ;CaptionML=[DEU=Servicevertragsart;
                                                              ENU=Service Contract Type];
                                                   OptionCaptionML=[DEU=" ,Angebot,Vertrag";
                                                                    ENU=" ,Quote,Contract"];
                                                   OptionString=[ ,Quote,Contract] }
  }
  KEYS
  {
    {    ;Service Contract Type,Service Contract No.,Day,Starting Date;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'DEU=%1 muss sp�ter als %2 sein.;ENU=%1 must be later than %2.';
      Text001@1001 : TextConst 'DEU=Die %1 muss vor der %2 liegen.;ENU=%1 must be earlier than %2.';
      Text002@1002 : TextConst 'DEU=Sie m�ssen die %1 angeben.;ENU=You must specify %1.';
      Text003@1003 : TextConst 'DEU=M�chten Sie den Standardservicekalender kopieren?;ENU=Do you want to copy the default service calendar?';

    LOCAL PROCEDURE CheckTime@1();
    BEGIN
      IF "Starting Time" = 0T THEN
        ERROR(Text002,FIELDCAPTION("Starting Time"));
      IF "Ending Time" = 0T THEN
        ERROR(Text002,FIELDCAPTION("Ending Time"));
    END;

    PROCEDURE CopyDefaultServiceHours@2();
    VAR
      ServHour@1000 : Record 5910;
      ServHour2@1001 : Record 5910;
    BEGIN
      IF NOT CONFIRM(Text003) THEN
        EXIT;

      ServHour.RESET;
      ServHour.SETRANGE("Service Contract No.",'');
      IF ServHour.FINDSET THEN
        REPEAT
          ServHour2.TRANSFERFIELDS(ServHour);
          EVALUATE(ServHour2."Service Contract Type",GETFILTER("Service Contract Type"));
          ServHour2.VALIDATE("Service Contract No.",GETFILTER("Service Contract No."));
          IF NOT ServHour2.INSERT THEN
            ServHour2.MODIFY;
        UNTIL ServHour.NEXT = 0;
    END;

    BEGIN
    END.
  }
}

