OBJECT Page 296 Recurring Req. Worksheet
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Wiederk. Bestellvorschl�ge;
               ENU=Recurring Req. Worksheet];
    SaveValues=Yes;
    SourceTable=Table246;
    DelayedInsert=Yes;
    DataCaptionFields=Journal Batch Name;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnOpenPage=VAR
                 JnlSelected@1000 : Boolean;
               BEGIN
                 OpenedFromBatch := ("Journal Batch Name" <> '') AND ("Worksheet Template Name" = '');
                 IF OpenedFromBatch THEN BEGIN
                   CurrentJnlBatchName := "Journal Batch Name";
                   ReqJnlManagement.OpenJnl(CurrentJnlBatchName,Rec);
                   EXIT;
                 END;
                 ReqJnlManagement.TemplateSelection(PAGE::"Recurring Req. Worksheet",TRUE,0,Rec,JnlSelected);
                 IF NOT JnlSelected THEN
                   ERROR('');
                 ReqJnlManagement.OpenJnl(CurrentJnlBatchName,Rec);
               END;

    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                     END;

    OnNewRecord=BEGIN
                  ReqJnlManagement.SetUpNewLine(Rec,xRec);
                  CLEAR(ShortcutDimCode);
                END;

    OnAfterGetCurrRecord=BEGIN
                           ReqJnlManagement.GetDescriptionAndRcptName(Rec,Description2,BuyFromVendorName);
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 37      ;1   ;ActionGroup;
                      CaptionML=[DEU=&Zeile;
                                 ENU=&Line];
                      Image=Line }
      { 38      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[DEU=Karte;
                                 ENU=Card];
                      ToolTipML=[DEU=Zeigt oder �ndert detaillierte Informationen �ber den Artikel oder die Ressource.;
                                 ENU=View or change detailed information about the item or resource.];
                      ApplicationArea=#Jobs;
                      RunObject=Codeunit 335;
                      Image=EditLines }
      { 74      ;2   ;ActionGroup;
                      CaptionML=[DEU=Artikelverf�gbarkeit nach;
                                 ENU=Item Availability by];
                      Image=ItemAvailability }
      { 5       ;3   ;Action    ;
                      CaptionML=[DEU=Ereignis;
                                 ENU=Event];
                      ToolTipML=[DEU=Zeigt an, wie sich die tats�chliche und die voraussichtliche Lagerebene eines Artikels im Zeitverlauf gem�� den Angebots- und Nachfrageereignissen entwickelt.;
                                 ENU=View how the actual and projected inventory level of an item will develop over time according to supply and demand events.];
                      ApplicationArea=#Jobs;
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromReqLine(Rec,ItemAvailFormsMgt.ByEvent)
                               END;
                                }
      { 75      ;3   ;Action    ;
                      CaptionML=[DEU=Periode;
                                 ENU=Period];
                      ToolTipML=[DEU=Zeigt die tats�chliche und die voraussichtliche Menge eines Artikels im Lauf der Zeit gem�� einem angegebenen Zeitintervall an, wie etwa nach Tag, Woche oder Monat.;
                                 ENU=Show the actual and projected quantity of an item over time according to a specified time interval, such as by day, week or month.];
                      ApplicationArea=#Jobs;
                      Image=Period;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromReqLine(Rec,ItemAvailFormsMgt.ByPeriod)
                               END;
                                }
      { 76      ;3   ;Action    ;
                      CaptionML=[DEU=Variante;
                                 ENU=Variant];
                      ToolTipML=[DEU=Zeigt die Attribute des Artikels an oder bearbeitet sie. Anstatt f�r jede Farbe einen separaten Artikel anzulegen, k�nnen Sie einen Artikel und die verschiedenen Farben als Varianten zu dem Artikel einrichten.;
                                 ENU=View or edit the item's variants. Instead of setting up each color of an item as a separate item, you can set up the various colors as variants of the item.];
                      ApplicationArea=#Jobs;
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromReqLine(Rec,ItemAvailFormsMgt.ByVariant)
                               END;
                                }
      { 44      ;3   ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[DEU=Lagerort;
                                 ENU=Location];
                      Image=Warehouse;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromReqLine(Rec,ItemAvailFormsMgt.ByLocation)
                               END;
                                }
      { 3       ;3   ;Action    ;
                      CaptionML=[DEU=St�cklistenebene;
                                 ENU=BOM Level];
                      ToolTipML=[DEU=Zeigt, wie sich die Lagerebene eines Artikels im Zeitverlauf je nach ausgew�hlter St�cklistenebene entwickelt.;
                                 ENU=View how the inventory level of an item develops over time according to the bill of materials level that you select.];
                      ApplicationArea=#Jobs;
                      Image=BOMLevel;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromReqLine(Rec,ItemAvailFormsMgt.ByBOM)
                               END;
                                }
      { 69      ;2   ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[DEU=Reservierungsposten;
                                 ENU=Reservation Entries];
                      ToolTipML=[DEU=Zeigen Sie alle Reservierungen f�r den Artikel an. Die Elemente k�nnen beispielsweise f�r Fertigungsauftr�ge reserviert werden.;
                                 ENU=View all reservations for the item. For example, items can be reserved for production orders or production orders.];
                      ApplicationArea=#Jobs;
                      Image=ReservationLedger;
                      OnAction=BEGIN
                                 ShowReservationEntries(TRUE);
                               END;
                                }
      { 81      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[DEU=Dimensionen;
                                 ENU=Dimensions];
                      ToolTipML=[DEU=Zeigt Dimensionen an oder bearbeitet sie, wie etwa einen Bereich, ein Projekt oder eine Abteilung, die Sie Verkaufs- oder Einkaufsbelegen zuweisen k�nnen, um die Kosten zu verteilen und den Transaktionsverlauf zu analysieren.;
                                 ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.];
                      ApplicationArea=#Jobs;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 6500    ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[DEU=Artikel&verfolgungszeilen;
                                 ENU=Item &Tracking Lines];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 39      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 40      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Planung berechnen;
                                 ENU=Calculate Plan];
                      ToolTipML=[DEU=Verwendet eine Stapelverarbeitung zur Berechnung eines Beschaffungsplans f�r Artikel und Lagerhaltungsdaten, deren Feld "Beschaffungsmethode" auf "Einkauf" oder "Umlagerung" gesetzt ist.;
                                 ENU=Use a batch job to help you calculate a supply plan for items and stockkeeping units that have the Replenishment System field set to Purchase or Transfer.];
                      ApplicationArea=#Jobs;
                      Image=CalculatePlan;
                      OnAction=BEGIN
                                 ReorderItems.SetTemplAndWorksheet("Worksheet Template Name","Journal Batch Name");
                                 ReorderItems.RUNMODAL;
                                 CLEAR(ReorderItems);
                               END;
                                }
      { 41      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Ereignismeldung durch&f�hren;
                                 ENU=Carry &Out Action Message];
                      ToolTipML=[DEU=Verwendet eine Stapelverarbeitung, um aus den Bestellvorschl�gen tats�chliche Beschaffungsauftr�ge zu erstellen.;
                                 ENU=Use a batch job to help you create actual supply orders from the order proposals.];
                      ApplicationArea=#Jobs;
                      Image=CarryOutActionMessage;
                      OnAction=VAR
                                 MakePurchOrder@1001 : Report 493;
                               BEGIN
                                 MakePurchOrder.SetReqWkshLine(Rec);
                                 MakePurchOrder.RUNMODAL;
                                 MakePurchOrder.GetReqWkshLine(Rec);
                                 CurrentJnlBatchName := GETRANGEMAX("Journal Batch Name");
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 43      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Re&servieren;
                                 ENU=&Reserve];
                      ToolTipML=[DEU=Reserviert mindestens eine Einheit des Artikels in der Projektplanungszeile, entweder aus dem Lagerbestand oder aus der eingehenden Lieferung.;
                                 ENU=Reserve one or more units of the item on the job planning line, either from inventory or from incoming supply.];
                      ApplicationArea=#Jobs;
                      Image=Reserve;
                      OnAction=BEGIN
                                 CurrPage.SAVERECORD;
                                 ShowReservation;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 33  ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[DEU=Name;
                           ENU=Name];
                ToolTipML=[DEU=Gibt den Namen des Datensatzes an.;
                           ENU=Specifies the name of the record.];
                ApplicationArea=#Jobs;
                SourceExpr=CurrentJnlBatchName;
                OnValidate=BEGIN
                             ReqJnlManagement.CheckName(CurrentJnlBatchName,Rec);
                             CurrentJnlBatchNameOnAfterVali;
                           END;

                OnLookup=BEGIN
                           CurrPage.SAVERECORD;
                           ReqJnlManagement.LookupName(CurrentJnlBatchName,Rec);
                           CurrPage.UPDATE(FALSE);
                         END;
                          }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Wiederholungsart an, wenn Sie im Feld "Wiederkehrend" angegeben haben, dass es sich um einen wiederkehrenden Bestellvorschlag handelt.;
                           ENU=Specifies a recurring method, if you have indicated in the Recurring field that the worksheet is a recurring requisition worksheet.];
                ApplicationArea=#Jobs;
                SourceExpr="Recurring Method" }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Wiederholungsrate an, wenn im Feld "Wiederkehrend" angegeben ist, dass es sich um einen wiederkehrenden Bestellvorschlag handelt.;
                           ENU=Specifies a recurring frequency, if it is indicated in the Recurring field that the worksheet is a recurring requisition worksheet.];
                ApplicationArea=#Jobs;
                SourceExpr="Recurring Frequency" }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Bestellvorschlagszeile an, die Sie erstellen.;
                           ENU=Specifies the type of requisition worksheet line you are creating.];
                ApplicationArea=#Jobs;
                SourceExpr=Type;
                OnValidate=BEGIN
                             ReqJnlManagement.GetDescriptionAndRcptName(Rec,Description2,BuyFromVendorName);
                           END;
                            }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Sachkontos oder Artikels an, die in der Zeile eingegeben wird.;
                           ENU=Specifies the number of the general ledger account or item to be entered on the line.];
                ApplicationArea=#Jobs;
                SourceExpr="No.";
                OnValidate=BEGIN
                             ReqJnlManagement.GetDescriptionAndRcptName(Rec,Description2,BuyFromVendorName);
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 49  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Variantencode f�r den Artikel an.;
                           ENU=Specifies a variant code for the item.];
                ApplicationArea=#Jobs;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 61  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Aktion an, die unternommen werden soll, um die Bedarfssituation auszugleichen.;
                           ENU=Specifies an action to take to rebalance the demand-supply situation.];
                ApplicationArea=#Jobs;
                SourceExpr="Action Message" }

    { 63  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die f�r die Zeile vorgeschlagene Ereignismeldung akzeptiert werden soll.;
                           ENU=Specifies whether to accept the action message proposed for the line.];
                ApplicationArea=#Jobs;
                SourceExpr="Accept Action Message" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt beschreibenden Text f�r den Posten an.;
                           ENU=Specifies text that describes the entry.];
                ApplicationArea=#Jobs;
                SourceExpr=Description }

    { 59  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt zus�tzlichen Text an, der den Posten oder die Bemerkung �ber die Bestellvorschlagszeile beschreibt.;
                           ENU=Specifies additional text describing the entry, or a remark about the requisition worksheet line.];
                ApplicationArea=#Jobs;
                SourceExpr="Description 2";
                Visible=FALSE }

    { 53  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Code f�r den Lagerort an, an dem die bestellten Artikel registriert werden.;
                           ENU=Specifies a code for an inventory location where the items that are being ordered will be registered.];
                SourceExpr="Location Code";
                Visible=TRUE }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Anzahl der Einheiten des Artikels an.;
                           ENU=Specifies the number of units of the item.];
                ApplicationArea=#Jobs;
                SourceExpr=Quantity }

    { 67  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Einheitencode an, der zum Ermitteln des VK-Preises verwendet wird.;
                           ENU=Specifies the unit of measure code used to determine the unit price.];
                ApplicationArea=#Jobs;
                SourceExpr="Unit of Measure Code" }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kreditors an, der die Artikel in der Bestellung liefern wird.;
                           ENU=Specifies the number of the vendor who will ship the items in the purchase order.];
                ApplicationArea=#Jobs;
                SourceExpr="Vendor No.";
                OnValidate=BEGIN
                             ReqJnlManagement.GetDescriptionAndRcptName(Rec,Description2,BuyFromVendorName);
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Artikelnummer des Kreditors f�r diesen Artikel an.;
                           ENU=Specifies the vendor's item number for this item.];
                ApplicationArea=#Jobs;
                SourceExpr="Vendor Item No." }

    { 57  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Shortcutdimension 1 an.;
                           ENU=Specifies the code for Shortcut Dimension 1.];
                ApplicationArea=#Jobs;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 55  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code f�r die Shortcutdimension 2 an.;
                           ENU=Specifies the code for Shortcut Dimension 2.];
                ApplicationArea=#Jobs;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(3,ShortcutDimCode[3]);
                           END;
                            }

    { 302 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(4,ShortcutDimCode[4]);
                           END;
                            }

    { 304 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(5,ShortcutDimCode[5]);
                           END;
                            }

    { 306 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(6,ShortcutDimCode[6]);
                           END;
                            }

    { 308 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(7,ShortcutDimCode[7]);
                           END;
                            }

    { 310 ;2   ;Field     ;
                ApplicationArea=#Jobs;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(8,ShortcutDimCode[8]);
                           END;
                            }

    { 35  ;2   ;Field     ;
                AssistEdit=Yes;
                ToolTipML=[DEU=Gibt den W�hrungscode f�r die Bestellvorschlagszeilen an.;
                           ENU=Specifies the currency code for the requisition lines.];
                ApplicationArea=#Jobs;
                SourceExpr="Currency Code";
                Visible=FALSE;
                OnAssistEdit=BEGIN
                               ChangeExchangeRate.SetParameter("Currency Code","Currency Factor",WORKDATE);
                               IF ChangeExchangeRate.RUNMODAL = ACTION::OK THEN
                                 VALIDATE("Currency Factor",ChangeExchangeRate.GetParameter);

                               CLEAR(ChangeExchangeRate);
                             END;
                              }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den EK-Preis dieses Artikels an.;
                           ENU=Specifies the direct unit cost of this item.];
                ApplicationArea=#Jobs;
                SourceExpr="Direct Unit Cost" }

    { 51  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Rabattprozentsatz zum Berechnen des Einkaufszeilenrabatts an.;
                           ENU=Specifies the discount percentage used to calculate the purchase line discount.];
                ApplicationArea=#Jobs;
                SourceExpr="Line Discount %";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Bestelldatum an, das f�r die Bestellvorschlagszeile gilt.;
                           ENU=Specifies the order date that will apply to the requisition worksheet line.];
                ApplicationArea=#Jobs;
                SourceExpr="Order Date";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das erwartete Lieferdatum der Artikel an.;
                           ENU=Specifies the date when you can expect to receive the items.];
                ApplicationArea=#Jobs;
                SourceExpr="Due Date" }

    { 47  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Benutzers an, der die Artikel in der Zeile bestellt.;
                           ENU=Specifies the ID of the user who is ordering the items on the line.];
                ApplicationArea=#Jobs;
                SourceExpr="Requester ID";
                Visible=FALSE }

    { 65  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt einen Wert an, wenn Sie den Fertigungsauftrag berechnen.;
                           ENU=Specifies a value when you calculate the production order.];
                ApplicationArea=#Jobs;
                SourceExpr="Prod. Order No.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Artikel in der Zeile f�r den Einkauf genehmigt wurden.;
                           ENU=Specifies whether the items on the line have been approved for purchase.];
                ApplicationArea=#Jobs;
                SourceExpr=Confirmed;
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das letzte Datum an, an dem der wiederkehrende Bestellvorschlag in eine Bestellung �bernommen wird.;
                           ENU=Specifies the last date on which the recurring requisition worksheet will be converted to a purchase order.];
                ApplicationArea=#Jobs;
                SourceExpr="Expiration Date" }

    { 28  ;1   ;Group      }

    { 1902205001;2;Group  ;
                GroupType=FixedLayout }

    { 1903866901;3;Group  ;
                CaptionML=[DEU=Beschreibung;
                           ENU=Description] }

    { 29  ;4   ;Field     ;
                ToolTipML=[DEU=Gibt einen zus�tzlichen Teil der Beschreibung des Arbeitsblatts an.;
                           ENU=Specifies an additional part of the worksheet description.];
                ApplicationArea=#Jobs;
                SourceExpr=Description2;
                Editable=FALSE;
                ShowCaption=No }

    { 1902759701;3;Group  ;
                CaptionML=[DEU=Eink. von Name;
                           ENU=Buy-from Vendor Name] }

    { 31  ;4   ;Field     ;
                CaptionML=[DEU=Eink. von Name;
                           ENU=Buy-from Vendor Name];
                ToolTipML=[DEU=Legt den Kreditor den Werten in den Feldern "Belegnr." und "Belegart" entsprechend fest.;
                           ENU=Specifies the vendor according to the values in the Document No. and Document Type fields.];
                ApplicationArea=#Jobs;
                SourceExpr=BuyFromVendorName;
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ReorderItems@1000 : Report 699;
      ReqJnlManagement@1002 : Codeunit 330;
      ItemAvailFormsMgt@1008 : Codeunit 353;
      ChangeExchangeRate@1001 : Page 511;
      CurrentJnlBatchName@1003 : Code[10];
      Description2@1004 : Text[50];
      BuyFromVendorName@1005 : Text[50];
      ShortcutDimCode@1006 : ARRAY [8] OF Code[20];
      OpenedFromBatch@1007 : Boolean;

    LOCAL PROCEDURE CurrentJnlBatchNameOnAfterVali@19002411();
    BEGIN
      CurrPage.SAVERECORD;
      ReqJnlManagement.SetName(CurrentJnlBatchName,Rec);
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

