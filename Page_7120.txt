OBJECT Page 7120 Sales Analysis Lines
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Verkaufsanalysezeilen;
               ENU=Sales Analysis Lines];
    MultipleNewLines=Yes;
    SourceTable=Table7114;
    DelayedInsert=Yes;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnOpenPage=VAR
                 GLSetup@1001 : Record 98;
                 AnalysisLineTemplate@1000 : Record 7112;
               BEGIN
                 AnalysisReportMgt.OpenAnalysisLines(CurrentAnalysisLineTempl,Rec);

                 GLSetup.GET;

                 IF AnalysisLineTemplate.GET(GETRANGEMAX("Analysis Area"),CurrentAnalysisLineTempl) THEN
                   IF AnalysisLineTemplate."Item Analysis View Code" <> '' THEN
                     ItemAnalysisView.GET(GETRANGEMAX("Analysis Area"),AnalysisLineTemplate."Item Analysis View Code")
                   ELSE BEGIN
                     CLEAR(ItemAnalysisView);
                     ItemAnalysisView."Dimension 1 Code" := GLSetup."Global Dimension 1 Code";
                     ItemAnalysisView."Dimension 2 Code" := GLSetup."Global Dimension 2 Code";
                   END;
               END;

    OnAfterGetRecord=BEGIN
                       DescriptionIndent := 0;
                       DescriptionOnFormat;
                     END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 27      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 28      ;2   ;Action    ;
                      CaptionML=[DEU=&Artikel einf�gen;
                                 ENU=Insert &Item];
                      ToolTipML=[DEU=F�gt einen oder mehrere Artikel ein, die im Verkaufsanalysebericht ber�cksichtigt werden sollen.;
                                 ENU=Insert one or more items that you want to include in the sales analysis report.];
                      Image=Item;
                      OnAction=BEGIN
                                 InsertLine(0);
                               END;
                                }
      { 29      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=&Debitoren einf�gen;
                                 ENU=Insert &Customers];
                      ToolTipML=[DEU=F�gt einen oder mehrere Debitoren ein, die im Verkaufsanalysebericht ber�cksichtigt werden sollen.;
                                 ENU=Insert one or more customers that you want to include in the sales analysis report.];
                      Image=Customer;
                      OnAction=BEGIN
                                 InsertLine(1);
                               END;
                                }
      { 36      ;2   ;Separator  }
      { 30      ;2   ;Action    ;
                      CaptionML=[DEU=Arti&kelgruppen einf�gen;
                                 ENU=Insert Ite&m Groups];
                      ToolTipML=[DEU=F�gt eine oder mehrere Artikelgruppen ein, die im Verkaufsanalysebericht ber�cksichtigt werden sollen.;
                                 ENU=Insert one or more item groups that you want to include in the sales analysis report.];
                      Image=ItemGroup;
                      OnAction=BEGIN
                                 InsertLine(3);
                               END;
                                }
      { 32      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=Debitoren&gruppen einf�gen;
                                 ENU=Insert Customer &Groups];
                      ToolTipML=[DEU=F�gt eine oder mehrere Debitorengruppen ein, die im Verkaufsanalysebericht ber�cksichtigt werden sollen.;
                                 ENU=Insert one or more customer groups that you want to include in the sales analysis report.];
                      Image=CustomerGroup;
                      OnAction=BEGIN
                                 InsertLine(4);
                               END;
                                }
      { 33      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[DEU=&Verk�ufer/Eink�ufer einf�gen;
                                 ENU=Insert &Sales/Purchase Persons];
                      ToolTipML=[DEU=F�gt einen oder mehrere Verk�ufer von Eink�ufern ein, die im Verkaufsanalysebericht ber�cksichtigt werden sollen.;
                                 ENU=Insert one or more sales people of purchasers that you want to include in the sales analysis report.];
                      Image=SalesPurchaseTeam;
                      OnAction=BEGIN
                                 InsertLine(5);
                               END;
                                }
      { 48      ;2   ;Separator  }
      { 49      ;2   ;Action    ;
                      CaptionML=[DEU=Zeilen neu nummerieren;
                                 ENU=Renumber Lines];
                      ToolTipML=[DEU=Nummeriert Zeilen im Analysebericht fortlaufend ab einer Nummer, die Sie angeben, neu.;
                                 ENU=Renumber lines in the analysis report sequentially from a number that you specify.];
                      Image=Refresh;
                      OnAction=VAR
                                 AnalysisLine@1000 : Record 7114;
                                 RenAnalysisLines@1001 : Report 7110;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(AnalysisLine);
                                 RenAnalysisLines.Init(AnalysisLine);
                                 RenAnalysisLines.RUNMODAL;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 25  ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[DEU=Name;
                           ENU=Name];
                ToolTipML=[DEU=Gibt den Namen des Datensatzes an.;
                           ENU=Specifies the name of the record.];
                SourceExpr=CurrentAnalysisLineTempl;
                OnValidate=BEGIN
                             AnalysisReportMgt.CheckAnalysisLineTemplName(CurrentAnalysisLineTempl,Rec);
                             CurrentAnalysisLineTemplOnAfte;
                           END;

                OnLookup=BEGIN
                           CurrPage.SAVERECORD;
                           AnalysisReportMgt.LookupAnalysisLineTemplName(CurrentAnalysisLineTempl,Rec);
                           CurrPage.UPDATE(FALSE);
                         END;
                          }

    { 1   ;1   ;Group     ;
                IndentationColumnName=DescriptionIndent;
                IndentationControls=Description;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Zeilenreferenznummer f�r die Analysezeile an.;
                           ENU=Specifies a row reference number for the analysis line.];
                SourceExpr="Row Ref. No.";
                StyleExpr='Strong';
                OnValidate=BEGIN
                             RowRefNoOnAfterValidate;
                           END;
                            }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung f�r die Analysezeile an.;
                           ENU=Specifies a description for the analysis line.];
                SourceExpr=Description;
                StyleExpr='Strong' }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Zusammenz�hlung f�r die Analysezeile an. Die Art bestimmt, welche Artikel innerhalb des Zusammenz�hlungsbereichs, den Sie im Feld "Bereich" angeben, zusammengez�hlt werden.;
                           ENU=Specifies the type of totaling for the analysis line. The type determines which items within the totaling range that you specify in the Range field will be totaled.];
                OptionCaptionML=[DEU=Artikel,Artikelgruppe,Debitor,Debitorengruppe,,Verk�ufer/Eink�ufer,Formel;
                                 ENU=Item,Item Group,Customer,Customer Group,,Sales/Purchase person,Formula];
                SourceExpr=Type }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer oder Formel der Art an, die zur Berechnung der Summe f�r diese Zeile verwendet werden soll.;
                           ENU=Specifies the number or formula of the type to use to calculate the total for this line.];
                SourceExpr=Range;
                OnLookup=BEGIN
                           EXIT(LookupTotalingRange(Text));
                         END;
                          }

    { 42  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Dimensionswertbetr�ge an, die in dieser Zeile zusammengez�hlt werden.;
                           ENU=Specifies the dimension value amounts that are totaled on this line.];
                SourceExpr="Dimension 1 Totaling";
                OnLookup=BEGIN
                           EXIT(LookupDimTotalingRange(Text,ItemAnalysisView."Dimension 1 Code"));
                         END;
                          }

    { 44  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, welche Dimensionswertbetr�ge in dieser Zeile zusammengez�hlt werden. Wenn die Art in der Zeile "Formel" lautet, muss dieses Feld leer sein. Wenn die Betr�ge in der Zeile nicht nach Dimensionen gefiltert werden sollen, muss dieses Feld ebenfalls leer sein.;
                           ENU=Specifies which dimension value amounts will be totaled on this line. If the type on the line is Formula, this field must be blank. Also, if you do not want the amounts on the line to be filtered by dimensions, this field must be blank.];
                SourceExpr="Dimension 2 Totaling";
                OnLookup=BEGIN
                           EXIT(LookupDimTotalingRange(Text,ItemAnalysisView."Dimension 2 Code"));
                         END;
                          }

    { 46  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, welche Dimensionswertbetr�ge in dieser Zeile zusammengez�hlt werden. Wenn die Art in der Zeile "Formel" lautet, muss dieses Feld leer sein. Wenn die Betr�ge in der Zeile nicht nach Dimensionen gefiltert werden sollen, muss dieses Feld ebenfalls leer sein.;
                           ENU=Specifies which dimension value amounts will be totaled on this line. If the type on the line is Formula, this field must be blank. Also, if you do not want the amounts on the line to be filtered by dimensions, this field must be blank.];
                SourceExpr="Dimension 3 Totaling";
                OnLookup=BEGIN
                           EXIT(LookupDimTotalingRange(Text,ItemAnalysisView."Dimension 3 Code"));
                         END;
                          }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob beim Drucken des Analyseberichts nach der aktuellen Zeile ein Seitenumbruch eingef�gt werden soll.;
                           ENU=Specifies if you want a page break after the current line when you print the analysis report.];
                SourceExpr="New Page" }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Analysezeile beim Drucken des Berichts ber�cksichtigt werden soll.;
                           ENU=Specifies whether you want the analysis line to be included when you print the report.];
                SourceExpr=Show }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Betr�ge in dieser Zeile fett gedruckt werden sollen.;
                           ENU=Specifies if you want the amounts in this line to be printed in bold.];
                SourceExpr=Bold }

    { 31  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Einr�ckung der Zeile an.;
                           ENU=Specifies the indentation of the line.];
                SourceExpr=Indentation;
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Betr�ge in dieser Zeile kursiv gedruckt werden sollen.;
                           ENU=Specifies if you want the amounts in this line to be printed in italics.];
                SourceExpr=Italic }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob die Betr�ge in dieser Zeile beim Drucken unterstrichen werden sollen.;
                           ENU=Specifies if you want the amounts in this line to be underlined when printed.];
                SourceExpr=Underline }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, ob Verk�ufe und Abg�nge als positive Betr�ge und Eink�ufe und Zug�nge als negative Betr�ge angezeigt werden sollen.;
                           ENU=Specifies if you want sales and negative adjustments to be shown as positive amounts and purchases and positive adjustments to be shown as negative amounts.];
                SourceExpr="Show Opposite Sign" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ItemAnalysisView@1000 : Record 7152;
      AnalysisReportMgt@1001 : Codeunit 7110;
      CurrentAnalysisLineTempl@1002 : Code[10];
      DescriptionIndent@19057867 : Integer INDATASET;

    LOCAL PROCEDURE InsertLine@1(Type@1001 : 'Item,Customer,Vendor,ItemGroup,CustGroup,SalespersonGroup');
    VAR
      AnalysisLine@1003 : Record 7114;
      InsertAnalysisLine@1002 : Codeunit 7111;
    BEGIN
      CurrPage.UPDATE(TRUE);
      AnalysisLine.COPY(Rec);
      IF "Line No." = 0 THEN BEGIN
        AnalysisLine := xRec;
        IF AnalysisLine.NEXT = 0 THEN
          AnalysisLine."Line No." := xRec."Line No." + 10000;
      END;
      CASE Type OF
        Type::Item:
          InsertAnalysisLine.InsertItems(AnalysisLine);
        Type::Customer:
          InsertAnalysisLine.InsertCust(AnalysisLine);
        Type::Vendor:
          InsertAnalysisLine.InsertVend(AnalysisLine);
        Type::ItemGroup:
          InsertAnalysisLine.InsertItemGrDim(AnalysisLine);
        Type::CustGroup:
          InsertAnalysisLine.InsertCustGrDim(AnalysisLine);
        Type::SalespersonGroup:
          InsertAnalysisLine.InsertSalespersonPurchaser(AnalysisLine);
      END;
    END;

    PROCEDURE SetCurrentAnalysisLineTempl@2(AnalysisLineTemlName@1000 : Code[10]);
    BEGIN
      CurrentAnalysisLineTempl := AnalysisLineTemlName;
    END;

    LOCAL PROCEDURE RowRefNoOnAfterValidate@19011265();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE CurrentAnalysisLineTemplOnAfte@19019881();
    VAR
      ItemSchedName@1001 : Record 7112;
    BEGIN
      CurrPage.SAVERECORD;
      AnalysisReportMgt.SetAnalysisLineTemplName(CurrentAnalysisLineTempl,Rec);
      IF ItemSchedName.GET(GETRANGEMAX("Analysis Area"),CurrentAnalysisLineTempl) THEN
        CurrPage.UPDATE(FALSE);
    END;

    LOCAL PROCEDURE DescriptionOnFormat@19023855();
    BEGIN
      DescriptionIndent := Indentation;
    END;

    BEGIN
    END.
  }
}

