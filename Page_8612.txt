OBJECT Page 8612 Config. Question Subform
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Zeilen;
               ENU=Lines];
    LinksAllowed=No;
    SourceTable=Table8612;
    PageType=ListPart;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Nummer f�r die Frage an. F�r manuell erstellte Fragen beginnt die Nummerierung mit 0, es sei denn, Sie geben einen anderen Ausgangspunkt an. Andernfalls ist die Nummerierung fortlaufend und beginnt mit 1.;
                           ENU=Specifies a number for the question. For manually created questions, numbering starts from 0 unless you specify another starting point. Otherwise, numbering is sequential and starts from 1.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No.";
                MinValue=1 }

    { 3   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die ID des Felds aus der Tabelle an, die der Fragenbereich verwaltet.;
                           ENU=Specifies the ID of the field from the table that the question area manages.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Field ID" }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Frage an, die im Einrichtungsfragebogen beantwortet werden muss. W�hlen Sie auf der Registerkarte "Aktionen" in der Gruppe "Frage" die Option "Fragen aktualisieren", um die Frageliste auf Grundlage der Felder in der Tabelle, auf der der Fragenbereich basiert, automatisch auszuf�llen. Sie k�nnen den Text so �ndern, dass er f�r die Person, die f�r das Ausf�llen des Fragebogens verantwortlich ist, mehr Sinn ergibt. Sie k�nnen z. B. die Frage "Name?" in "Wie lautet der Name Ihres Unternehmens?" umschreiben.;
                           ENU=Specifies a question that is to be answered on the setup questionnaire. On the Actions tab, in the Question group, choose Update Questions to autopopulate the question list based on the fields in the table on which the question area is based. You can modify the text to be more meaningful to the person responsible for filling out the questionnaire. For example, you could rewrite the Name? question as What is the name of your company?];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Question }

    { 16  ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[DEU=Gibt das Format an, dem die Antwort auf die Frage entsprechen muss. Wenn Sie beispielsweise eine Frage zu einem Namen haben, die beantwortet werden muss, kann die Antwortoption entsprechend dem in der Datenbank festgelegten Namensfeldformat und Datentyp "Text" lauten.;
                           ENU=Specifies the format that the answer to the question needs to meet. For example, if you have a question about a name that needs to be answered, according to the name field format and data type set up in the database, the answer option can specify Text.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Answer Option" }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Antwort auf die Frage an. Die Antwort auf die Frage muss dem Format der Antwortoption entsprechen und ein Wert sein, den die Datenbank unterst�tzt. Andernfalls tritt beim �bernehmen der Antwort ein Fehler auf.;
                           ENU=Specifies the answer to the question. The answer to the question should match the format of the answer option and must be a value that the database supports. If it does not, then there will be an error when you apply the answer.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Answer }

    { 5   ;2   ;Field     ;
                Name=Field Value;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=LookupValue }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine URL-Adresse an. Verwenden Sie dieses Feld, um eine URL-Adresse f�r einen Speicherort anzugeben, der Informationen �ber die Frage angibt. Sie k�nnen z. B. die Adresse einer Seite mit Informationen �ber die Einrichtungs�berlegungen angeben, die die Person, die den Fragebogen beantwortet, ber�cksichtigen soll.;
                           ENU=Specifies a url address. Use this field to provide a url address to a location that Specifies information about the question. For example, you could provide the address of a page that Specifies information about setup considerations that the person answering the questionnaire should consider.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Reference }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[DEU=Gibt den Namen des Felds an, das den Einrichtungsfragebogenbereich unterst�tzt. Der Name wird aus der Eigenschaft "Name" des Felds �bernommen.;
                           ENU=Specifies the name of the field that is supporting the setup questionnaire area. The name comes from the Name property of the field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Field Name" }

    { 6   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[DEU=Gibt die Caption des Felds an, das den Einrichtungsfragebogenbereich unterst�tzt. Die Caption wird aus der Eigenschaft "Caption" des Felds �bernommen.;
                           ENU=Specifies the caption of the field that is supporting the setup questionnaire area. The caption comes from the Caption property of the field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Field Caption" }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Ursprung der Frage an.;
                           ENU=Specifies the origin of the question.];
                SourceExpr="Question Origin";
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

