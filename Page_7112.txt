OBJECT Page 7112 Analysis Line Templates
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Analysezeilenvorlagen;
               ENU=Analysis Line Templates];
    SourceTable=Table7112;
    DataCaptionFields=Analysis Area;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 11      ;1   ;Action    ;
                      Name=Lines;
                      CaptionML=[DEU=&Zeilen;
                                 ENU=&Lines];
                      ToolTipML=[DEU=Gibt die Zeilen in der Analyseansicht an, die Daten anzeigt.;
                                 ENU=Specifies the lines in the analysis view that shows data.];
                      Promoted=Yes;
                      Image=AllLines;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 AnalysisLine@1001 : Record 7114;
                                 AnalysisReportMngt@1000 : Codeunit 7110;
                               BEGIN
                                 AnalysisLine.FILTERGROUP := 2;
                                 AnalysisLine.SETRANGE("Analysis Area","Analysis Area");
                                 AnalysisLine.FILTERGROUP := 0;
                                 AnalysisReportMngt.OpenAnalysisLinesForm(AnalysisLine,Name);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Analysezeilenvorlage an.;
                           ENU=Specifies the name of the analysis line template.];
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt eine Beschreibung der Analysezeilenvorlage an.;
                           ENU=Specifies a description of the analysis line template.];
                SourceExpr=Description }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den f�r diesen Analysebericht eingerichteten Spaltenvorlagennamen an.;
                           ENU=Specifies the column template name that you have set up for this analysis report.];
                SourceExpr="Default Column Template Name";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Namen der Analyseansicht an, auf der der Analysebericht basiert.;
                           ENU=Specifies the name of the analysis view that the analysis report is based on.];
                SourceExpr="Item Analysis View Code";
                OnLookup=VAR
                           ItemAnalysisView@1000 : Record 7152;
                         BEGIN
                           ItemAnalysisView.FILTERGROUP := 2;
                           ItemAnalysisView.SETRANGE("Analysis Area","Analysis Area");
                           ItemAnalysisView.FILTERGROUP := 0;
                           ItemAnalysisView."Analysis Area" := "Analysis Area";
                           ItemAnalysisView.Code := Text;
                           IF PAGE.RUNMODAL(0,ItemAnalysisView) = ACTION::LookupOK THEN BEGIN
                             Text := ItemAnalysisView.Code;
                             EXIT(TRUE);
                           END;
                         END;
                          }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

