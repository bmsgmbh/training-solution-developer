OBJECT Page 5096 To-do List
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[DEU=Aufgaben�bersicht;
               ENU=To-do List];
    SourceTable=Table5080;
    DataCaptionExpr=Caption;
    PageType=List;
    CardPageID=To-do Card;
    OnFindRecord=BEGIN
                   RecordsFound := FIND(Which);
                   EXIT(RecordsFound);
                 END;

    OnAfterGetRecord=BEGIN
                       ContactNoOnFormat(FORMAT("Contact No."));
                     END;

    OnAfterGetCurrRecord=BEGIN
                           CALCFIELDS("Contact Name","Contact Company Name");
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 31      ;1   ;ActionGroup;
                      Name=Todo;
                      CaptionML=[DEU=Auf&gabe;
                                 ENU=To-&do];
                      Image=Task }
      { 34      ;2   ;Action    ;
                      CaptionML=[DEU=Be&merkung;
                                 ENU=Co&mment];
                      ToolTipML=[DEU=Zeigt Bemerkungen an oder f�gt diese hinzu.;
                                 ENU=View or add comments.];
                      RunObject=Page 5072;
                      RunPageLink=Table Name=CONST(To-do),
                                  No.=FIELD(Organizer To-do No.),
                                  Sub No.=CONST(0);
                      Image=ViewComments }
      { 35      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[DEU=Aktivit&�tenprotokollposten;
                                 ENU=Interaction Log E&ntries];
                      ToolTipML=[DEU=Zeigt Aktivit�tenprotokollposten f�r die Aufgabe an.;
                                 ENU=View interaction log entries for the to-do.];
                      RunObject=Page 5076;
                      RunPageView=SORTING(To-do No.);
                      RunPageLink=To-do No.=FIELD(Organizer To-do No.);
                      Image=InteractionLog }
      { 50      ;2   ;Action    ;
                      CaptionML=[DEU=Zur�ckgestellte &Aktivit�ten;
                                 ENU=Postponed &Interactions];
                      ToolTipML=[DEU=Zeigt zur�ckgestellte Aktivit�ten f�r die Aufgabe an.;
                                 ENU=View postponed interactions for the to-do.];
                      RunObject=Page 5082;
                      RunPageView=SORTING(To-do No.);
                      RunPageLink=To-do No.=FIELD(Organizer To-do No.);
                      Image=PostponedInteractions }
      { 52      ;2   ;Action    ;
                      CaptionML=[DEU=&Teilnehmerplanung;
                                 ENU=A&ttendee Scheduling];
                      ToolTipML=[DEU=Zeigt den Status einer geplanten Besprechung an.;
                                 ENU=View the status of a scheduled meeting.];
                      Image=ProfileCalender;
                      OnAction=VAR
                                 Todo@1001 : Record 5080;
                               BEGIN
                                 Todo.GET("Organizer To-do No.");
                                 PAGE.RUNMODAL(PAGE::"Attendee Scheduling",Todo)
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 32      ;1   ;ActionGroup;
                      CaptionML=[DEU=F&unktion;
                                 ENU=F&unctions];
                      Image=Action }
      { 46      ;2   ;Action    ;
                      CaptionML=[DEU=Aktionen zuordnen;
                                 ENU=Assign Activities];
                      ToolTipML=[DEU=Zeigt alle Aufgaben an, die Verk�ufern und Teams zugewiesen wurden. Eine Aufgabe kann darin bestehen, Besprechungen zu organisieren, Anrufe zu t�tigen usw.;
                                 ENU=View all the to-dos that have been assigned to salespeople and teams. A to-do can be organizing meetings, making phone calls, and so on.];
                      Image=Allocate;
                      OnAction=VAR
                                 TempToDo@1001 : TEMPORARY Record 5080;
                               BEGIN
                                 TempToDo.AssignActivityFromToDo(Rec);
                               END;
                                }
      { 42      ;2   ;Action    ;
                      CaptionML=[DEU=&Telefonverbindung herstellen;
                                 ENU=Make &Phone Call];
                      ToolTipML=[DEU=Ruft den ausgew�hlten Kontakt an.;
                                 ENU=Call the selected contact.];
                      Image=Calls;
                      OnAction=VAR
                                 SegLine@1001 : Record 5077;
                                 ContactNo@1002 : Code[10];
                                 ContCompanyNo@1003 : Code[10];
                               BEGIN
                                 IF "Contact No." <> '' THEN
                                   ContactNo := "Contact No."
                                 ELSE
                                   ContactNo := GETFILTER("Contact No.");
                                 IF "Contact Company No." <> '' THEN
                                   ContCompanyNo := "Contact Company No."
                                 ELSE
                                   ContCompanyNo := GETFILTER("Contact Company No.");
                                 IF ContactNo = '' THEN BEGIN
                                   IF (Type = Type::Meeting) AND ("Team Code" = '') THEN
                                     ERROR(Text004);
                                   ERROR(Text005);
                                 END;
                                 SegLine."To-do No." := "No.";
                                 SegLine."Contact No." := ContactNo;
                                 SegLine."Contact Company No." := ContCompanyNo;
                                 SegLine."Campaign No." := "Campaign No.";

                                 SegLine.CreateCall;
                               END;
                                }
      { 33      ;1   ;Action    ;
                      CaptionML=[DEU=Aufgabe &erstellen;
                                 ENU=&Create To-do];
                      ToolTipML=[DEU=Erstellt eine neue Aufgabe.;
                                 ENU=Create a new to-do.];
                      ApplicationArea=#RelationshipMgmt;
                      Promoted=Yes;
                      Image=NewToDo;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 TempToDo@1001 : TEMPORARY Record 5080;
                               BEGIN
                                 TempToDo.CreateToDoFromToDo(Rec);
                               END;
                                }
      { 1034    ;1   ;Action    ;
                      CaptionML=[DEU=Organisatoraufgabe bearbeiten;
                                 ENU=Edit Organizer To-Do];
                      ToolTipML=[DEU=Zeigt allgemeine Informationen �ber die Aufgaben an wie Art, Beschreibung, Priorit�t und Status der Aufgabe sowie welchem Verk�ufer oder welchem Team die Aufgabe zugewiesen ist.;
                                 ENU=View general information about the to-dos such as type, description, priority and status of the to-do, as well as the salesperson or team the to-do is assigned to.];
                      ApplicationArea=#RelationshipMgmt;
                      RunObject=Page 5098;
                      RunPageLink=No.=FIELD(Organizer To-do No.);
                      Promoted=No;
                      Image=Edit;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                Editable=FALSE;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass die Aufgabe geschlossen ist.;
                           ENU=Specifies that the to-do is closed.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Closed }

    { 4   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Aufgabe beginnen soll. Es gibt bestimmte Regeln f�r die Eingabe von Datumsangaben. Diese finden Sie unter "Vorgehensweise: Eingeben von Datums- und Uhrzeitwerten".;
                           ENU=Specifies the date when the to-do should be started. There are certain rules for how dates should be entered found in How to: Enter Dates and Times.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Date }

    { 6   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Art der Aufgabe an.;
                           ENU=Specifies the type of the to-do.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Type }

    { 8   ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Beschreibung der Aufgabe an.;
                           ENU=Specifies the description of the to-do.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Description }

    { 10  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Priorit�t der Aufgabe an. Es sind drei Optionen verf�gbar:;
                           ENU=Specifies the priority of the to-do. There are three options:];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Priority }

    { 12  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Status der Aufgabe an. Es sind f�nf Optionen verf�gbar: "Nicht begonnen", "In Bearbeitung", "Abgeschlossen", "Wartet" und "Zur�ckgestellt".;
                           ENU=Specifies the status of the to-do. There are five options: Not Started, In Progress, Completed, Waiting and Postponed.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Status }

    { 45  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Aufgabe des Organisators an. Dieses Feld kann nicht bearbeitet werden.;
                           ENU=Specifies the number of the organizer's to-do. The field is not editable.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Organizer To-do No." }

    { 16  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt das Datum an, an dem die Aufgabe geschlossen wurde.;
                           ENU=Specifies the date the to-do was closed.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Date Closed" }

    { 43  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass die Aufgabe storniert wurde.;
                           ENU=Specifies that the to-do has been canceled.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Canceled }

    { 18  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt an, dass der Aufgabe eine Bemerkung zugewiesen wurde.;
                           ENU=Specifies that a comment has been assigned to the to-do.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Comment }

    { 20  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer des Kontakts an, der mit der Aufgabe verkn�pft ist.;
                           ENU=Specifies the number of the contact linked to the to-do.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact No.";
                OnLookup=VAR
                           Todo@1000 : Record 5080;
                           Cont@1004 : Record 5050;
                         BEGIN
                           IF Type = Type::Meeting THEN BEGIN
                             Todo.SETRANGE("No.","No.");
                             PAGE.RUNMODAL(PAGE::"Attendee Scheduling",Todo);
                           END ELSE BEGIN
                             IF Cont.GET("Contact No.") THEN;
                             IF PAGE.RUNMODAL(0,Cont) = ACTION::LookupOK THEN;
                           END;
                         END;
                          }

    { 47  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Kontaktnummer des Unternehmens an, f�r das der Kontakt arbeitet, auf den sich die Aufgabe bezieht.;
                           ENU=Specifies the contact number of the company for which the contact involved in the to-do works.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact Company No.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Verk�ufers an, der der Aufgabe zugewiesen ist.;
                           ENU=Specifies the code of the salesperson assigned to the to-do.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Salesperson Code" }

    { 22  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt den Code des Teams an, dem die Aufgabe zugewiesen ist.;
                           ENU=Specifies the code of the team to which the to-do is assigned.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Team Code" }

    { 26  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Kampagne an, mit der die Aufgabe verkn�pft ist.;
                           ENU=Specifies the number of the campaign to which the to-do is linked.];
                SourceExpr="Campaign No." }

    { 36  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Verkaufschance an, mit der die Aufgabe verkn�pft ist.;
                           ENU=Specifies the number of the opportunity to which the to-do is linked.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Opportunity No." }

    { 14  ;2   ;Field     ;
                ToolTipML=[DEU=Gibt die Nummer der Aufgabe an.;
                           ENU=Specifies the number of the to-do.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No." }

    { 55  ;1   ;Group      }

    { 56  ;2   ;Field     ;
                DrillDown=No;
                CaptionML=[DEU=Kontaktname;
                           ENU=Contact Name];
                ToolTipML=[DEU=Gibt den Namen des Kontakts an, dem diese Aufgabe zugewiesen wurde.;
                           ENU=Specifies the name of the contact to which this to-do has been assigned.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact Name" }

    { 58  ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[DEU=Gibt den Namen des Unternehmens an, f�r das der Kontakt arbeitet, auf den sich die Aufgabe bezieht.;
                           ENU=Specifies the name of the company for which the contact involved in the to-do works.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact Company Name" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Cont@1000 : Record 5050;
      Contact1@1009 : Record 5050;
      SalesPurchPerson@1001 : Record 13;
      Campaign@1002 : Record 5071;
      Team@1003 : Record 5083;
      Opp@1004 : Record 5092;
      SegHeader@1005 : Record 5076;
      RecordsFound@1008 : Boolean;
      Text000@1007 : TextConst 'DEU=(Mehrere);ENU=(Multiple)';
      Text001@1006 : TextConst 'DEU=unbenannt;ENU=untitled';
      Text004@1010 : TextConst 'DEU=F�r diese Aufgabe kann nur innerhalb des Teilnehmerplanungsfensters eine Telefonverbindung hergestellt werden.;ENU=The Make Phone Call function for this to-do is available only on the Attendee Scheduling window.';
      Text005@1011 : TextConst 'DEU=Sie m�ssen eine Aufgabe zusammen mit einem dieser Aufgabe zugewiesenen Kontakt ausw�hlen, bevor Sie eine Telefonverbindung herstellen k�nnen.;ENU=You must select a to-do with a contact assigned to it before you can use the Make Phone Call function.';

    LOCAL PROCEDURE Caption@1() : Text[260];
    VAR
      CaptionStr@1000 : Text[260];
    BEGIN
      IF Cont.GET(GETFILTER("Contact Company No.")) THEN BEGIN
        Contact1.GET(GETFILTER("Contact Company No."));
        IF Contact1."No." <> Cont."No." THEN
          CaptionStr := COPYSTR(Cont."No." + ' ' + Cont.Name,1,MAXSTRLEN(CaptionStr));
      END;
      IF Cont.GET(GETFILTER("Contact No.")) THEN
        CaptionStr := COPYSTR(CaptionStr + ' ' + Cont."No." + ' ' + Cont.Name,1,MAXSTRLEN(CaptionStr));
      IF SalesPurchPerson.GET(GETFILTER("Salesperson Code")) THEN
        CaptionStr := COPYSTR(CaptionStr + ' ' + SalesPurchPerson.Code + ' ' + SalesPurchPerson.Name,1,MAXSTRLEN(CaptionStr));
      IF Team.GET(GETFILTER("Team Code")) THEN
        CaptionStr := COPYSTR(CaptionStr + ' ' + Team.Code + ' ' + Team.Name,1,MAXSTRLEN(CaptionStr));
      IF Campaign.GET(GETFILTER("Campaign No.")) THEN
        CaptionStr := COPYSTR(CaptionStr + ' ' + Campaign."No." + ' ' + Campaign.Description,1,MAXSTRLEN(CaptionStr));
      IF Opp.GET(GETFILTER("Opportunity No.")) THEN
        CaptionStr := COPYSTR(CaptionStr + ' ' + Opp."No." + ' ' + Opp.Description,1,MAXSTRLEN(CaptionStr));
      IF SegHeader.GET(GETFILTER("Segment No.")) THEN
        CaptionStr := COPYSTR(CaptionStr + ' ' + SegHeader."No." + ' ' + SegHeader.Description,1,MAXSTRLEN(CaptionStr));
      IF CaptionStr = '' THEN
        CaptionStr := Text001;

      EXIT(CaptionStr);
    END;

    LOCAL PROCEDURE ContactNoOnFormat@19025756(Text@19019593 : Text[1024]);
    BEGIN
      IF Type = Type::Meeting THEN
        Text := Text000;
    END;

    BEGIN
    END.
  }
}

