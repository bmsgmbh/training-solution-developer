OBJECT Table 7344 Registered Invt. Movement Hdr.
{
  OBJECT-PROPERTIES
  {
    Date=23.09.13;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Reg. Lagerbestandsumlagerungskopf;
               ENU=Registered Invt. Movement Hdr.];
    LookupPageID=Page7386;
  }
  FIELDS
  {
    { 2   ;   ;No.                 ;Code20        ;CaptionML=[DEU=Nr.;
                                                              ENU=No.] }
    { 3   ;   ;Location Code       ;Code10        ;TableRelation=Location.Code;
                                                   CaptionML=[DEU=Lagerortcode;
                                                              ENU=Location Code];
                                                   NotBlank=Yes }
    { 4   ;   ;Assigned User ID    ;Code50        ;TableRelation="Warehouse Employee" WHERE (Location Code=FIELD(Location Code));
                                                   CaptionML=[DEU=Zugewiesene Benutzer-ID;
                                                              ENU=Assigned User ID] }
    { 5   ;   ;Assignment Date     ;Date          ;CaptionML=[DEU=Zuweisungsdatum;
                                                              ENU=Assignment Date] }
    { 6   ;   ;Assignment Time     ;Time          ;CaptionML=[DEU=Zuweisungszeit;
                                                              ENU=Assignment Time] }
    { 8   ;   ;Registering Date    ;Date          ;CaptionML=[DEU=Registriert am;
                                                              ENU=Registering Date] }
    { 9   ;   ;No. Series          ;Code10        ;TableRelation="No. Series".Code;
                                                   CaptionML=[DEU=Nummernserie;
                                                              ENU=No. Series];
                                                   Editable=No }
    { 10  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Warehouse Comment Line" WHERE (Table Name=CONST(Registered Invt. Movement),
                                                                                                     Type=CONST(" "),
                                                                                                     No.=FIELD(No.)));
                                                   CaptionML=[DEU=Bemerkung;
                                                              ENU=Comment];
                                                   Editable=No }
    { 11  ;   ;Invt. Movement No.  ;Code20        ;CaptionML=[DEU=Lagebestandsumlagerungsnr.;
                                                              ENU=Invt. Movement No.] }
    { 12  ;   ;No. Printed         ;Integer       ;CaptionML=[DEU=Anzahl gedruckt;
                                                              ENU=No. Printed];
                                                   Editable=No }
    { 20  ;   ;Posting Date        ;Date          ;CaptionML=[DEU=Buchungsdatum;
                                                              ENU=Posting Date] }
    { 7306;   ;Source No.          ;Code20        ;TableRelation=IF (Source Type=CONST(120)) "Purch. Rcpt. Header" WHERE (No.=FIELD(Source No.))
                                                                 ELSE IF (Source Type=CONST(110)) "Sales Shipment Header" WHERE (No.=FIELD(Source No.))
                                                                 ELSE IF (Source Type=CONST(6650)) "Return Shipment Header" WHERE (No.=FIELD(Source No.))
                                                                 ELSE IF (Source Type=CONST(6660)) "Return Receipt Header" WHERE (No.=FIELD(Source No.))
                                                                 ELSE IF (Source Type=CONST(5744)) "Transfer Shipment Header" WHERE (No.=FIELD(Source No.))
                                                                 ELSE IF (Source Type=CONST(5746)) "Transfer Receipt Header" WHERE (No.=FIELD(Source No.))
                                                                 ELSE IF (Source Type=CONST(5405)) "Production Order".No. WHERE (Status=FILTER(Released|Finished),
                                                                                                                                 No.=FIELD(Source No.))
                                                                                                                                 ELSE IF (Source Type=CONST(900)) "Assembly Header".No. WHERE (Document Type=CONST(Order),
                                                                                                                                                                                               No.=FIELD(Source No.));
                                                   CaptionML=[DEU=Herkunftsnr.;
                                                              ENU=Source No.] }
    { 7307;   ;Source Document     ;Option        ;CaptionML=[DEU=Herkunftsbeleg;
                                                              ENU=Source Document];
                                                   OptionCaptionML=[DEU=" ,Auftrag,,,Verkaufsreklamation,Bestellung,,,Einkaufsreklamation,Eingeh. Umlagerung,Ausgeh. Umlagerung,FA-Verbrauch,FA-Istmeldung,,,,,,,,Verbrauch f�r Montage,Montageauftrag";
                                                                    ENU=" ,Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,Inbound Transfer,Outbound Transfer,Prod. Consumption,Prod. Output,,,,,,,,Assembly Consumption,Assembly Order"];
                                                   OptionString=[ ,Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,Inbound Transfer,Outbound Transfer,Prod. Consumption,Prod. Output,,,,,,,,Assembly Consumption,Assembly Order];
                                                   BlankZero=Yes }
    { 7308;   ;Source Type         ;Integer       ;CaptionML=[DEU=Herkunftsart;
                                                              ENU=Source Type] }
    { 7309;   ;Source Subtype      ;Option        ;CaptionML=[DEU=Herkunftsunterart;
                                                              ENU=Source Subtype];
                                                   OptionCaptionML=[DEU=0,1,2,3,4,5,6,7,8,9,10;
                                                                    ENU=0,1,2,3,4,5,6,7,8,9,10];
                                                   OptionString=0,1,2,3,4,5,6,7,8,9,10;
                                                   Editable=No }
    { 7310;   ;Destination Type    ;Option        ;CaptionML=[DEU=F�r Art;
                                                              ENU=Destination Type];
                                                   OptionCaptionML=[DEU=" ,Debitor,Kreditor,Lagerort,Artikel,Fertigungsfamilie,Auftrag";
                                                                    ENU=" ,Customer,Vendor,Location,Item,Family,Sales Order"];
                                                   OptionString=[ ,Customer,Vendor,Location,Item,Family,Sales Order] }
    { 7311;   ;Destination No.     ;Code20        ;TableRelation=IF (Destination Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Destination Type=CONST(Customer)) Customer
                                                                 ELSE IF (Destination Type=CONST(Location)) Location
                                                                 ELSE IF (Destination Type=CONST(Item)) Item
                                                                 ELSE IF (Destination Type=CONST(Family)) Family
                                                                 ELSE IF (Destination Type=CONST(Sales Order)) "Sales Header".No. WHERE (Document Type=CONST(Order));
                                                   CaptionML=[DEU=F�r Nr.;
                                                              ENU=Destination No.] }
    { 7312;   ;External Document No.;Code35       ;CaptionML=[DEU=Externe Belegnummer;
                                                              ENU=External Document No.] }
    { 7314;   ;Shipment Date       ;Date          ;CaptionML=[DEU=Warenausg.-Datum;
                                                              ENU=Shipment Date] }
    { 7315;   ;External Document No.2;Code35      ;CaptionML=[DEU=Externe Belegnummer 2;
                                                              ENU=External Document No.2] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Invt. Movement No.                       }
    {    ;Location Code                            }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

