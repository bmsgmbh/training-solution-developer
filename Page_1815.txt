OBJECT Page 1815 Pmt. App. Workflow Setup Wzrd.
{
  OBJECT-PROPERTIES
  {
    Date=25.10.16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[DEU=Einrichtung des Genehmigungsworkflows;
               ENU=Approval Workflow Setup];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table1804;
    PageType=NavigatePage;
    SourceTableTemporary=Yes;
    ShowFilter=No;
    OnInit=BEGIN
             IF NOT GET THEN BEGIN
               INIT;
               INSERT;
             END;
             LoadTopBanners;
             CurrentBatchIsLabel := STRSUBSTNO(CurrentBatchTxt,"Journal Batch Name");
           END;

    OnOpenPage=BEGIN
                 ShowIntroStep;
                 IF "For All Batches" THEN
                   BatchSelection := BatchSelection::"All Batches"
                 ELSE
                   BatchSelection := BatchSelection::"Current Batch Only";

                 ShowCurrentBatch := NOT "For All Batches";
               END;

    OnQueryClosePage=VAR
                       AssistedSetup@1001 : Record 1803;
                     BEGIN
                       IF CloseAction = ACTION::OK THEN
                         IF AssistedSetup.GetStatus(PAGE::"Pmt. App. Workflow Setup Wzrd.") = AssistedSetup.Status::"Not Completed" THEN
                           IF NOT CONFIRM(NAVNotSetUpQst,FALSE) THEN
                             ERROR('');
                     END;

    ActionList=ACTIONS
    {
      { 8       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 15      ;1   ;Action    ;
                      Name=PreviousPage;
                      CaptionML=[DEU=Zur�ck;
                                 ENU=Back];
                      ApplicationArea=#Suite;
                      Enabled=BackEnabled;
                      InFooterBar=Yes;
                      Image=PreviousRecord;
                      OnAction=BEGIN
                                 NextStep(TRUE);
                               END;
                                }
      { 14      ;1   ;Action    ;
                      Name=NextPage;
                      CaptionML=[DEU=Weiter;
                                 ENU=Next];
                      ApplicationArea=#Suite;
                      Enabled=NextEnabled;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 NextStep(FALSE);
                               END;
                                }
      { 9       ;1   ;Action    ;
                      Name=Finish;
                      CaptionML=[DEU=Fertig stellen;
                                 ENU=Finish];
                      ApplicationArea=#Suite;
                      Enabled=FinishEnabled;
                      InFooterBar=Yes;
                      Image=Approve;
                      OnAction=VAR
                                 AssistedSetup@1000 : Record 1803;
                                 ApprovalWorkflowSetupMgt@1001 : Codeunit 1804;
                               BEGIN
                                 ApprovalWorkflowSetupMgt.ApplyPaymantJrnlWizardUserInput(Rec);
                                 AssistedSetup.SetStatus(PAGE::"Pmt. App. Workflow Setup Wzrd.",AssistedSetup.Status::Completed);

                                 CurrPage.CLOSE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 96  ;1   ;Group     ;
                Visible=TopBannerVisible AND NOT DoneVisible;
                Editable=FALSE;
                GroupType=Group }

    { 97  ;2   ;Field     ;
                ApplicationArea=#Suite;
                SourceExpr=MediaRepositoryStandard.Image;
                Editable=FALSE;
                ShowCaption=No }

    { 98  ;1   ;Group     ;
                Visible=TopBannerVisible AND DoneVisible;
                Editable=FALSE;
                GroupType=Group }

    { 99  ;2   ;Field     ;
                ApplicationArea=#Suite;
                SourceExpr=MediaRepositoryDone.Image;
                Editable=FALSE;
                ShowCaption=No }

    { 11  ;1   ;Group     ;
                Name=Step1;
                CaptionML=[DEU="";
                           ENU=""];
                Visible=IntroVisible;
                GroupType=Group }

    { 18  ;2   ;Group     ;
                Name=Para1.1;
                CaptionML=[DEU=Willkommen bei der Einrichtung des Genehmigungsworkflows f�r Zahlungs-Buch.-Blattzeilen;
                           ENU=Welcome to Payment Journal Line Approval Workflow Setup];
                GroupType=Group }

    { 19  ;3   ;Group     ;
                Name=Para1.1.1;
                CaptionML=[DEU="";
                           ENU=""];
                GroupType=Group;
                InstructionalTextML=[DEU=Sie k�nnen einen Genehmigungsworkflow erstellen, der einen Genehmiger benachrichtigt, wenn ein Benutzer Zahlungs-Buch.-Blattzeilen sendet.;
                                     ENU=You can create approval workflow that notifies an approver when a user sends payment journal lines for approval.] }

    { 21  ;2   ;Group     ;
                Name=Para1.2;
                CaptionML=[DEU=Los geht's!;
                           ENU=Let's go!];
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie 'Weiter', um den Genehmiger und andere grundlegende Einstellungen festzulegen.;
                                     ENU=Choose Next to specify the approver and other basic settings.] }

    { 67  ;1   ;Group     ;
                Name=Step2;
                CaptionML=[DEU="";
                           ENU=""];
                Visible=ApproverSelectionVisible;
                GroupType=Group }

    { 5   ;2   ;Group     ;
                Name=Para2.1;
                CaptionML=[DEU=" ";
                           ENU=" "];
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie, wer zum Genehmigen oder Ablehnen der Zahlungs-Buch.-Blattzeilen autorisiert ist.;
                                     ENU=Choose who is authorized to approve or reject the payment journal lines.] }

    { 2   ;3   ;Field     ;
                CaptionML=[DEU=Genehmiger;
                           ENU=Approver];
                ApplicationArea=#Suite;
                SourceExpr="Approver ID";
                LookupPageID=Approval User Setup;
                OnValidate=BEGIN
                             CanEnableNext;
                           END;
                            }

    { 16  ;2   ;Group     ;
                Name=Para2.2;
                CaptionML=[DEU=" ";
                           ENU=" "];
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie, ob der Genehmigungsworkflow f�r alle Buch.-Blattnamen oder nur f�r den aktuellen Buch.-Blattnamen gilt.;
                                     ENU=Choose if the approval workflow applies to all journal batches or to the current journal batch only.] }

    { 12  ;3   ;Field     ;
                CaptionML=[DEU=Genehmigungsworkflow gilt f�r;
                           ENU=Approval workflow applies to];
                ApplicationArea=#Suite;
                SourceExpr=BatchSelection;
                OnValidate=BEGIN
                             "For All Batches" := BatchSelection = BatchSelection::"All Batches";
                             ShowCurrentBatch := NOT "For All Batches";
                           END;
                            }

    { 3   ;3   ;Group     ;
                Visible=ShowCurrentBatch;
                GroupType=Group }

    { 4   ;4   ;Field     ;
                DrillDown=Yes;
                ApplicationArea=#Suite;
                SourceExpr=CurrentBatchIsLabel;
                Editable=FALSE;
                Style=StandardAccent;
                StyleExpr=TRUE;
                OnDrillDown=BEGIN
                              CurrPage.UPDATE;
                            END;

                ShowCaption=No }

    { 10  ;1   ;Group     ;
                Name=Step10;
                CaptionML=[DEU="";
                           ENU=""];
                Visible=DoneVisible;
                GroupType=Group }

    { 13  ;2   ;Group     ;
                Name=Para10.1;
                CaptionML=[DEU=�bersicht �ber Genehmigungsworkfl. Zahlung Buch.-Blatt;
                           ENU=Payment Journal Approval Workflow Overview];
                GroupType=Group }

    { 7   ;3   ;Field     ;
                Name=Overview;
                ApplicationArea=#Suite;
                SourceExpr=SummaryText;
                Editable=FALSE;
                MultiLine=Yes;
                Style=StrongAccent;
                StyleExpr=TRUE;
                ShowCaption=No }

    { 6   ;2   ;Group     ;
                Name=Para10.2;
                CaptionML=[DEU=Das war's schon!;
                           ENU=That's it!];
                GroupType=Group;
                InstructionalTextML=[DEU=W�hlen Sie 'Fertig stellen', um den Workflow mit den festgelegten Einstellungen zu aktivieren.;
                                     ENU=Choose Finish to enable the workflow with the specified settings.] }

  }
  CODE
  {
    VAR
      MediaRepositoryStandard@1040 : Record 9400;
      MediaRepositoryDone@1041 : Record 9400;
      Step@1015 : 'Intro,Approver Selection,Done';
      BatchSelection@1008 : 'Current Batch Only,All Batches';
      BackEnabled@1014 : Boolean;
      NextEnabled@1013 : Boolean;
      FinishEnabled@1010 : Boolean;
      TopBannerVisible@1042 : Boolean;
      IntroVisible@1001 : Boolean;
      ApproverSelectionVisible@1000 : Boolean;
      BatchSelectionVisible@1003 : Boolean;
      DoneVisible@1004 : Boolean;
      NAVNotSetUpQst@1007 : TextConst 'DEU=Die Zahlung Buch.-Blatt-Genehmigung wurde noch nicht eingerichtet.\\M�chten Sie den Assistenten wirklich beenden?;ENU=Payment Journal Approval has not been set up.\\Are you sure that you want to exit?';
      MandatoryApproverErr@1002 : TextConst '@@@="%1 = User Name";DEU=Sie m�ssen einen Genehmiger ausw�hlen, bevor Sie fortfahren.;ENU=You must select an approver before continuing.';
      MandatoryBatchErr@1006 : TextConst '@@@="%1 = User Name";DEU=Sie m�ssen eine Stapelverarbeitung ausw�hlen, bevor Sie fortfahren.;ENU=You must select a batch before continuing.';
      CurrentBatchTxt@1009 : TextConst '@@@="%1 = Batch name. Example - Current Batch is BANK.";DEU=Die aktuelle Stapelverarbeitung ist %1.;ENU=Current Batch is %1.';
      ShowCurrentBatch@1005 : Boolean;
      CurrentBatchIsLabel@1011 : Text;
      SummaryText@1012 : Text;
      OverviewTemplateTxt@1016 : TextConst '@@@="%1 = User Name, %2 = batch name or all batches. Example - An approval request will be sent to the user NAVUSER when the approval request is snt to all batches. ";DEU=Eine Genehmigungsanforderung wird an den Benutzer %1 gesendet, um Buch.-Blattzeilen in %2 zu genehmigen.;ENU=An approval request will be sent to the user %1 for approving journal lines in %2.';
      AllBatchesTxt@1017 : TextConst 'DEU=Alle Stapelverarbeitungen;ENU=all batches';
      BatchNameTxt@1018 : TextConst '@@@="%1 = Batch name";DEU=Stapel %1;ENU=batch %1';

    LOCAL PROCEDURE NextStep@3(Backwards@1000 : Boolean);
    BEGIN
      IF Backwards THEN
        Step := Step - 1
      ELSE BEGIN
        IF ApproverSelectionVisible THEN
          ValidateApprover;
        IF BatchSelectionVisible THEN
          ValidateBatchSelection;
        Step := Step + 1;
      END;

      CASE Step OF
        Step::Intro:
          ShowIntroStep;
        Step::"Approver Selection":
          ShowApprovalUserSelectionStep;
        Step::Done:
          ShowDoneStep;
      END;
      CurrPage.UPDATE(TRUE);
    END;

    LOCAL PROCEDURE ShowIntroStep@1();
    BEGIN
      ResetWizardControls;
      IntroVisible := TRUE;
      BackEnabled := FALSE;
    END;

    LOCAL PROCEDURE ShowApprovalUserSelectionStep@9();
    BEGIN
      ResetWizardControls;
      ApproverSelectionVisible := TRUE;
    END;

    LOCAL PROCEDURE ShowDoneStep@6();
    BEGIN
      ResetWizardControls;
      DoneVisible := TRUE;
      NextEnabled := FALSE;
      FinishEnabled := TRUE;

      IF "For All Batches" THEN
        SummaryText := STRSUBSTNO(OverviewTemplateTxt,"Approver ID",AllBatchesTxt)
      ELSE
        SummaryText := STRSUBSTNO(OverviewTemplateTxt,"Approver ID",STRSUBSTNO(BatchNameTxt,"Journal Batch Name"));

      SummaryText := CONVERTSTR(SummaryText,'\','/');
    END;

    LOCAL PROCEDURE ResetWizardControls@10();
    BEGIN
      // Buttons
      BackEnabled := TRUE;
      NextEnabled := TRUE;
      FinishEnabled := FALSE;

      // Tabs
      IntroVisible := FALSE;
      ApproverSelectionVisible := FALSE;
      BatchSelectionVisible := FALSE;
      DoneVisible := FALSE;
    END;

    LOCAL PROCEDURE ValidateApprover@11();
    BEGIN
      IF "Approver ID" = '' THEN
        ERROR(MandatoryApproverErr);
    END;

    LOCAL PROCEDURE ValidateBatchSelection@7();
    BEGIN
      IF NOT "For All Batches" THEN
        IF "Journal Batch Name" = '' THEN
          ERROR(MandatoryBatchErr);
    END;

    LOCAL PROCEDURE CanEnableNext@32();
    BEGIN
      NextEnabled := TRUE;
    END;

    LOCAL PROCEDURE LoadTopBanners@40();
    BEGIN
      IF MediaRepositoryStandard.GET('AssistedSetup-NoText-400px.png',FORMAT(CURRENTCLIENTTYPE)) AND
         MediaRepositoryDone.GET('AssistedSetupDone-NoText-400px.png',FORMAT(CURRENTCLIENTTYPE))
      THEN
        TopBannerVisible := MediaRepositoryDone.Image.HASVALUE;
    END;

    BEGIN
    END.
  }
}

